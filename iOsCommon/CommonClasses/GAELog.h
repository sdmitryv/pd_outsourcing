//
//  GAELog.h
//  ProductBuilder
//
//  Created by Alexander Martyshko on 10/20/14.
//  Copyright (c) 2014 Cloudworks. All rights reserved.
//

#define AREA_MONERIS_PAYMENT @"Teamwork.POS.Moneris"
#define AREA_APPLICATION_STATES @"Teamwork.POS.States"

#define AREA_HTML_REPORT_GENERATOR      @"HTML report generator"
#define AREA_HTML_REPORT_PARSER         @"HTML report parser"
#define AREA_PRINTING_PROCESSOR         @"Printing processor"

#import "CEntity.h"

typedef NS_ENUM(NSUInteger, GAELogType) {
    GAELogTypeCritical = 1,
    GAELogTypeError = 2,
    GAELogTypeWarning = 4,
    GAELogTypeInfo = 8,
    GAELogTypeVerbose = 16
};

@interface GAELog : CEntity

@property (nonatomic, retain) NSString *serverName;
@property (nonatomic, retain) NSString *appName;
@property (nonatomic, retain) NSString *dateTimeStr;
@property (nonatomic, retain) NSString *threadNo;
@property (nonatomic, assign) GAELogType type;
@property (nonatomic, retain) NSString *area;
@property (nonatomic, retain) NSString *message;
@property (nonatomic, retain) NSString *stackTrace;
@property (nonatomic, retain) BPUUID *locationId;
@property (nonatomic, retain) BPUUID *workstationId;
@property (nonatomic, retain) NSString *deviceNo;
@property (nonatomic, retain) NSString *appShortVersion;
@property (nonatomic, retain) NSString *appVersion;
@property (nonatomic, retain) NSString *systemVersion;
@property (nonatomic, retain) NSString *additionalInfo;

@property (nonatomic, readonly) NSString *severity;
@property (nonatomic, readonly) NSDictionary *dictionatyRepresentation;

+ (void)logfWithType:(GAELogType)type area:(NSString *)area message:(NSString *)message stackTrace:(NSString *)stackTrace;
+ (NSArray *)listOfLogs;
+(BOOL)deleteFromList:(NSArray*)list error:(NSError**)error;

+(void)startPublicLog:(GAELogType)type area:(NSString *)area message:(NSString *)message ;
+(void)addStackTraceToPublicLog:(NSString *)stackTrace;
+(void)addStackTraceToPublicLog:(NSString *)stackTrace calcDelta:(BOOL)calcDelta;
+(void)addStackTraceToPublicLog:(NSString *)stackTrace startCalcDelta:(BOOL)startCalcDelta;
+(void)addStackTraceToPublicLog:(NSString *)stackTrace calcDelta:(BOOL)calcDelta startCalcDelta:(BOOL)startCalcDelta;
+(void)pushPublicLog:(BOOL)calcTotalTime;

@end
