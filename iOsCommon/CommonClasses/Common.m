//
//  Common.m
//  StoreManager
//
//  Created by  on 12/13/11.
//  Copyright (c) 2011 Home. All rights reserved.
//

#import "Common.h"
#import <CommonCrypto/CommonDigest.h>
#import "NSData+Base64.h"
#include <sys/xattr.h>
#import <mach/mach.h>
#import <mach/mach_host.h>

@implementation Common

////////////////////////////////////////////////////////////
+(NSString*)getMD5Base64:(NSString*)txt {
    
    const char *str = [txt UTF8String];
    unsigned char result[CC_MD5_DIGEST_LENGTH];
    CC_MD5(str, (CC_LONG)strlen(str), result);

    NSData *resData = [NSData dataWithBytes:result length:CC_MD5_DIGEST_LENGTH];
    
    return [resData base64Encoding];
}


////////////////////////////////////////////////////////////
+ (NSUInteger)memoryAvailable {
    
    mach_port_t hostPort = mach_host_self(); 
    mach_msg_type_number_t hostSize = sizeof(vm_statistics_data_t) / sizeof(integer_t); 

    vm_size_t pageSize; 
    host_page_size(hostPort, &pageSize); 
    vm_statistics_data_t vmStats; 

    if (host_statistics(hostPort, HOST_VM_INFO, (host_info_t) &vmStats, &hostSize) == KERN_SUCCESS) { 

        natural_t freeMemory = vmStats.free_count * (natural_t)pageSize;
        return freeMemory/1024; 
    } 
    
    return 0; 
} 


////////////////////////////////////////////////////////////
+ (BOOL)addSkipBackupAttributeToItemAtPath:(NSString *)path {
   
    NSURL *URL = [NSURL fileURLWithPath:path];
    return [Common addSkipBackupAttributeToItemAtURL:URL];
}


////////////////////////////////////////////////////////////
+ (BOOL)addSkipBackupAttributeToItemAtURL:(NSURL *)URL {
    
    const char* filePath = [[URL path] fileSystemRepresentation];
    
    const char* attrName = "com.apple.MobileBackup";
    u_int8_t attrValue = 1;
    
    int result = setxattr(filePath, attrName, &attrValue, sizeof(attrValue), 0, 0);
    return result == 0;
}

@end
