//
//  DeviceStats.h
//  ProductBuilder
//
//  Created by Alexander Martyshko on 10/20/14.
//  Copyright (c) 2014 Cloudworks. All rights reserved.
//

#import "CEntity.h"

@interface DeviceStats : CEntity

@property (nonatomic, retain) NSString *key;
@property (nonatomic, retain) NSString *value;
@property (nonatomic, assign) int state;

-(id)initWithKey:(NSString *)key value:(NSString *)value;

+(void)statsKey:(NSString *)key value:(NSString *)value;

+(void)statsLastAppRun;
+(void)statsLastBecomeActive;
+(void)statsLastSyncStart;
+(void)statsLastSyncEnd;
+(void)statsLastInit;
+(void)statsSyncErrorTable:(NSString *)table;
+(void)statsSyncErrorMessage:(NSString *)message;
+(void)statsLastForceSyncStart;

+(NSDictionary*)getStatsList;
+(NSDate *)getLastRun;

@end
