//
//  DeviceStats.m
//  ProductBuilder
//
//  Created by Alexander Martyshko on 10/20/14.
//  Copyright (c) 2014 Cloudworks. All rights reserved.
//

#import "DeviceStats.h"
#import "DataUtils.h"
#import "NSDate+ISO8601Unparsing.h"

@implementation DeviceStats

- (void)dealloc {
    
    [_key release];
    [_value release];
    
    [super dealloc];
}


-(id)initWithKey:(NSString *)key value:(NSString *)value {
    
    if (self = [super init]) {
        
        _key = [key retain];
        _value = [value retain];
    }

    return self;
}

+(void)statsKey:(NSString *)key value:(NSString *)value {
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT,0), ^{
        
        DeviceStats *stats = [[DeviceStats alloc] initWithKey:key value:value];
        stats.state = 1;
        [stats save];
        [stats release];
        
    });
    
}


static NSDate *_lastRun;
+(NSDate *)getLastRun {
    
    @synchronized(self) {
        
        if (!_lastRun)
            _lastRun = [[NSDate date] retain];
    }
    
    return _lastRun;
}

+(void)statsLastAppRun {
    
    @synchronized(self) {
        
        [_lastRun release];
        _lastRun = [[NSDate date] retain];
        [DeviceStats statsKey:@"LastRun" value:[_lastRun ISO8601DateString]];
    }
}

+(void)statsLastBecomeActive {
    
    [DeviceStats statsKey:@"LastBecomeActive" value:[[NSDate date] ISO8601DateString]];
}

+(void)statsLastSyncStart {
    
    [DeviceStats statsKey:@"LastFullSyncStart" value:[[NSDate date] ISO8601DateString]];
}

+(void)statsLastSyncEnd {
    
    [DeviceStats statsKey:@"LastFullSyncEnd" value:[[NSDate date] ISO8601DateString]];
}

+(void)statsLastInit {
    
    [DeviceStats statsKey:@"LastInit" value:[[NSDate date] ISO8601DateString]];
}

+(void)statsSyncErrorTable:(NSString *)table {
    
    [DeviceStats statsKey:@"SyncErrorTable" value:table];
}

+(void)statsSyncErrorMessage:(NSString *)message {
    
    [DeviceStats statsKey:@"SyncErrorMessage" value:message];
}

+(void)statsLastForceSyncStart {
    
    [DeviceStats statsKey:@"LastForceSyncStart" value:[[NSDate date] ISO8601DateString]];
}

+(NSDictionary*)getStatsList { return nil; }

@end
