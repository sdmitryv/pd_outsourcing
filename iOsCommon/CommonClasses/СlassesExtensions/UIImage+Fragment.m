//
//  UIImage+Fragment.m
//  ProductBuilder
//
//  Created by Sergey Lugovoy on 4/27/15.
//  Copyright (c) 2015 Cloudworks. All rights reserved.
//

#import "UIImage+Fragment.h"

@implementation UIImage(Fragment)

- (UIImage *)fragment:(CGRect)rect {
    CGImageRef imageRef = CGImageCreateWithImageInRect([self CGImage], rect);
    UIImage *croppedImage = [UIImage imageWithCGImage:imageRef];
    CGImageRelease(imageRef);
    return croppedImage;
}

@end
