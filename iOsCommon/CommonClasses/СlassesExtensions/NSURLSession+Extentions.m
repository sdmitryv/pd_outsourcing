//
//  NSURLSession+Extentions.m
//  ProductBuilder
//
//  Created by valery on 11/18/15.
//  Copyright © 2015 Cloudworks. All rights reserved.
//

#import "NSURLSession+Extentions.h"

@implementation NSURLSession(Extentions)


+ (NSData *)sendSynchronousRequest:(NSURLRequest *)request returningResponse:(NSURLResponse **)response error:(NSError **)error{
    
    NSURLSessionConfiguration* sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
    if (request.timeoutInterval){
        sessionConfig.timeoutIntervalForRequest = request.timeoutInterval;
    }
    NSURLSession *session = [NSURLSession sessionWithConfiguration:sessionConfig];
    __block NSData* theData = nil;
    __block NSURLResponse* theResponse = nil;
    __block NSError* theError = nil;
    dispatch_semaphore_t waitForRequestSemaphore = dispatch_semaphore_create(0);
    [[session dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable anError) {
        
        if (anError.code==NSURLErrorCancelled){
            
            [session invalidateAndCancel];
            dispatch_semaphore_signal(waitForRequestSemaphore);
            return;
        }
        [session finishTasksAndInvalidate];
        theData = [data retain];
        if (response){
            theResponse = [response retain];
        }
        if (error){
            theError = [anError retain];
        }
        dispatch_semaphore_signal(waitForRequestSemaphore);
        
    }] resume];
    
    dispatch_semaphore_wait(waitForRequestSemaphore, DISPATCH_TIME_FOREVER);
    dispatch_release(waitForRequestSemaphore);
    
    if (error){
        *error = [theError autorelease];
    }
    if (response){
        *response = [theResponse autorelease];
    }
    return [theData autorelease];
}

- (void)defaultHandlerForDidReceiveChallenge:(NSURLAuthenticationChallenge *)challenge
                           completionHandler:(void (^)(NSURLSessionAuthChallengeDisposition disposition, NSURLCredential * credential))completionHandler userName:(NSString*)userName password:(NSString*)password errorHandler:(void(^)(NSError*))errorHandler{
    if ([challenge previousFailureCount] == 0) {
        NSURLCredential *credential = nil;
        BOOL ignoreChallenge = FALSE;
        if([challenge.protectionSpace.authenticationMethod isEqualToString:NSURLAuthenticationMethodServerTrust]){
            //Ignoring SSL
            credential = [NSURLCredential credentialForTrust:challenge.protectionSpace.serverTrust];
            ignoreChallenge = TRUE;
        }
        else if (userName && password){
            credential = [NSURLCredential credentialWithUser:userName password:password persistence:NSURLCredentialPersistenceForSession];
        }
        if (credential){
            [challenge.sender useCredential:credential forAuthenticationChallenge:challenge];
            if (ignoreChallenge){
                [challenge.sender continueWithoutCredentialForAuthenticationChallenge:challenge];
            }
        }
        if (completionHandler){
            completionHandler(credential ? NSURLSessionAuthChallengeUseCredential : NSURLSessionAuthChallengePerformDefaultHandling, credential);
        }
    } else {
        [[challenge sender] cancelAuthenticationChallenge:challenge];
        if (completionHandler){
            completionHandler(NSURLSessionAuthChallengeCancelAuthenticationChallenge, nil);
        }
        if (errorHandler){
            NSDictionary *userInfo = @{NSLocalizedDescriptionKey: @"Authentication Error"};
            NSError *authError = [NSError errorWithDomain:@"Connection Authentication" code:0 userInfo:userInfo];
            errorHandler(authError);
        }
    }
}
@end
