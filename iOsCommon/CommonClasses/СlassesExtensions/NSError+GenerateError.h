//
//  NSError+GenerateError.h
//  Lessons
//
//  Created by Dmitry Smirnov on 5/16/12.
//  Copyright (c) 2012 Home. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSError(GenerateError)

+(NSError *)errorWithDomain:(NSString *)domain code:(NSInteger)code description:(NSString *)description;

#ifdef IsTestMode

+(void)generateSignal;
+(void)generateError;

#endif

@end
