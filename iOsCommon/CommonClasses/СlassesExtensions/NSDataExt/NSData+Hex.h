//
//  NSData+Hex.h
//  ProductBuilder
//
//  Created by Viacheslav Lulakov on 8/28/15.
//  Copyright (c) 2015 Cloudworks. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSData(Hex)

+(NSString *)hexStringWithData:(NSData *)data;
-(NSString *)hexString;

@end
