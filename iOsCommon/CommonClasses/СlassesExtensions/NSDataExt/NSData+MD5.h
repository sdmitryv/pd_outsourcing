//
//  NSData+MD5.h
//  Lessons
//
//  Created by Dmitry Smirnov on 5/15/12.
//  Copyright (c) 2012 Home. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSData(MD5)

+(NSString *)MD5FromData:(NSData *)data;
-(NSString *)MD5;
-(NSString *)sha256WithRSA:(SecKeyRef)key;

@end
