//
//  NSData+Bytes.h
//  Lessons
//
//  Created by Dmitry Smirnov on 5/16/12.
//  Copyright (c) 2012 Home. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSData(Bytes)

+(NSArray *)bytesArrayFromData:(NSData *)data;
-(NSArray *)bytesArray;

@end
