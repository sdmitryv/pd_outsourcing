//
//  NSData+Bytes.m
//  Lessons
//
//  Created by Dmitry Smirnov on 5/16/12.
//  Copyright (c) 2012 Home. All rights reserved.
//

#import "NSData+Bytes.h"

@implementation NSData(Bytes)

////////////////////////////////////////////////////////////
+(NSArray *)bytesArrayFromData:(NSData *)data {
#ifdef ShowLog
    NSLog(@" %s", __FUNCTION__);
#endif
    
    NSMutableArray *arr = nil;
    
    if (data) {
    
        NSUInteger len = [data length];
        arr = [[NSMutableArray alloc] initWithCapacity:len];
        
        Byte *byteData = (Byte*)malloc(len);
        memcpy(byteData, [data bytes], len);
        
        for (NSInteger i = 0; i < len; i++)
            [arr addObject:@(byteData[i])];
        
        free(byteData);
    }
    
    return [arr autorelease];
}


////////////////////////////////////////////////////////////
-(NSArray *)bytesArray {
#ifdef ShowLog
    NSLog(@" %s", __FUNCTION__);
#endif
    
    return [NSData bytesArrayFromData:self];
}

@end
