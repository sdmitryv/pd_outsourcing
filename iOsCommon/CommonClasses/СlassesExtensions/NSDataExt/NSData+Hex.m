//
//  NSData+Hex.m
//  ProductBuilder
//
//  Created by Viacheslav Lulakov on 8/28/15.
//  Copyright (c) 2015 Cloudworks. All rights reserved.
//

#import "NSData+Hex.h"

@implementation NSData(Hex)

+(NSString *)hexStringWithData:(NSData *)data
{
    NSMutableString * tmp = [NSMutableString stringWithCapacity:data.length * 2];
    for (NSUInteger i = 0; i < data.length; i++) {
        [tmp appendFormat: @"%02x", ((unsigned char *)data.bytes)[i]];
    }
    return [NSString stringWithString:tmp];
}

-(NSString *)hexString
{
    return [NSData hexStringWithData:self];
}

@end
