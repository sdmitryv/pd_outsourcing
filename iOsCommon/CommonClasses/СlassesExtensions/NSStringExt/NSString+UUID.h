//
//  NSString+UUID.h
//  Lessons
//
//  Created by Dmitry Smirnov on 5/16/12.
//  Copyright (c) 2012 Home. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString(UUID)

+(NSString *)UUID;

@end
