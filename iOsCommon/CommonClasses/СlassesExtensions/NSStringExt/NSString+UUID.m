//
//  NSString+UUID.m
//  Lessons
//
//  Created by Dmitry Smirnov on 5/16/12.
//  Copyright (c) 2012 Home. All rights reserved.
//

#import "NSString+UUID.h"

@implementation NSString(UUID)

////////////////////////////////////////////////////////////
+(NSString *)UUID {
#ifdef ShowLog
    NSLog(@" %s", __FUNCTION__);
#endif
    
    CFUUIDRef theUUID = CFUUIDCreate(NULL);
    CFStringRef string = CFUUIDCreateString(NULL, theUUID);
    CFRelease(theUUID);
    return [(NSString *)string autorelease];
}

@end
