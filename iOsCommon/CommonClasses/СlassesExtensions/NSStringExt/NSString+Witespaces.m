//
//  NSString+Witespaces.m
//  Lessons
//
//  Created by Dmitry Smirnov on 5/16/12.
//  Copyright (c) 2012 Home. All rights reserved.
//

#import "NSString+Witespaces.h"

@implementation NSString(Witespaces)

////////////////////////////////////////////////////////////
+(NSString *)removeWitespaces:(NSString *)str {
#ifdef ShowLog
    NSLog(@" %s", __FUNCTION__);
#endif
    
    NSString *result = @"";
    
    if (!str || [str length] == 0)
        return @"";
    
    NSArray *arr = [str componentsSeparatedByString:@"\n"];
    for (NSString *s1 in arr) {
        
        NSString *s2 = [s1 stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        if ([s2 length] == 0)
            continue;
        
        if ([result length] > 0) 
            result = [result stringByAppendingString:@"\n"];
        
        result = [result stringByAppendingString:s2];
    }
    
    return result;
}


////////////////////////////////////////////////////////////
-(NSString *)removeWitespaces {
#ifdef ShowLog
    NSLog(@" %s", __FUNCTION__);
#endif
    
    return [NSString removeWitespaces:self];
}

@end
