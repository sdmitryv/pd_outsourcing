//
//  NSString+Encoding.m
//  Lessons
//
//  Created by Dmitry Smirnov on 5/16/12.
//  Copyright (c) 2012 Home. All rights reserved.
//

#import "NSString+Encoding.h"

@implementation NSString(Encoding)

////////////////////////////////////////////////////////////
+(NSString *)encodeForWebStr:(NSString *)str {
    
    if(!str || [str isEqualToString:@""])
		return @"";
    
	return [[str stringByReplacingOccurrencesOfString:@"&" withString:@"%26"] stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLHostAllowedCharacterSet]];
}


////////////////////////////////////////////////////////////
-(NSString *)encodeForWeb {
    
    return [NSString encodeForWebStr:self];
}

@end
