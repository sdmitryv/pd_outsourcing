//
//  UIImage+Fragment.m
//  ProductBuilder
//
//  Created by Sergey Lugovoy on 4/27/15.
//  Copyright (c) 2015 Cloudworks. All rights reserved.
//

#import "UIImage+BlackWhite.h"

@implementation UIImage(BlackWhite)

- (UIImage *)bwImage {
    return [self bwImageWithThreshold:0.6];
}

- (UIImage *)bwImageWithThreshold:(CGFloat)threshold {
    int kRed = 1;
    int kGreen = 2;
    int kBlue = 4;
    
    int colors = kGreen | kBlue | kRed;
    int m_width = self.size.width;
    int m_height = self.size.height;
    
    uint32_t *rgbImage = (uint32_t *) malloc(m_width * m_height * sizeof(uint32_t));
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    CGContextRef context = CGBitmapContextCreate(rgbImage, m_width, m_height, 8, m_width * 4, colorSpace, kCGBitmapByteOrder32Little | kCGImageAlphaNoneSkipLast);
    CGContextSetInterpolationQuality(context, kCGInterpolationHigh);
    CGContextSetShouldAntialias(context, NO);
    CGContextDrawImage(context, CGRectMake(0, 0, m_width, m_height), [self CGImage]);
    CGContextRelease(context);
    CGColorSpaceRelease(colorSpace);
    
    // now convert to grayscale
    uint32_t *result = (uint32_t *) malloc(m_width * m_height *sizeof(uint32_t));
    for(int y = 0; y < m_height; y++) {
        for(int x = 0; x < m_width; x++) {
            uint32_t rgbPixel = rgbImage[y*m_width+x];
            uint32_t sum = 0, count = 0;
            if (colors & kRed) {sum += (rgbPixel >> 24) & 255; count++;}
            if (colors & kGreen) {sum += (rgbPixel >> 16) & 255; count++;}
            if (colors & kBlue) {sum += (rgbPixel >> 8) & 255; count++;}
            uint32_t val = 1.0f * sum / count < (threshold * 255) ? 0 : 255;
            
            uint32_t bwPixel = (val << 24) + (val << 16) + (val << 8);
            
            result[y*m_width+x] = bwPixel;
        }
    }
    free(rgbImage);
    
    // create a UIImage
    colorSpace = CGColorSpaceCreateDeviceRGB();
    context = CGBitmapContextCreate(result, m_width, m_height, 8, m_width * sizeof(uint32_t), colorSpace, kCGBitmapByteOrder32Little | kCGImageAlphaNoneSkipLast);
    CGImageRef image = CGBitmapContextCreateImage(context);
    CGContextRelease(context);
    CGColorSpaceRelease(colorSpace);
    UIImage *resultUIImage = [UIImage imageWithCGImage:image];
    CGImageRelease(image);
    
    // make sure the data will be released by giving it to an autoreleased NSData
    [NSData dataWithBytesNoCopy:result length:m_width * m_height];
    
    return resultUIImage;
}

@end
