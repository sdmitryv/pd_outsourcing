//
//  NSError+GenerateError.m
//  Lessons
//
//  Created by Dmitry Smirnov on 5/16/12.
//  Copyright (c) 2012 Home. All rights reserved.
//

#import "NSError+GenerateError.h"

@implementation NSError(GenerateError)

////////////////////////////////////////////////////////////
+(NSError *)errorWithDomain:(NSString *)domain code:(NSInteger)code description:(NSString *)description {
#ifdef ShowLog
    NSLog(@" %s: %@", __FUNCTION__, description);
#endif
    
    NSDictionary *errDict = @{NSLocalizedDescriptionKey: description};
    NSError *err = [[[NSError alloc] initWithDomain:domain code:code userInfo:errDict] autorelease];
    return err;
}


#ifdef IsTestMode

static void (^nullFunction)();

////////////////////////////////////////////////////////////
+(void)generateSignal {
#ifdef ShowLog
    NSLog(@" %s", __FUNCTION__);
#endif
    
    nullFunction();
}


////////////////////////////////////////////////////////////
+(void)generateError {
#ifdef ShowLog
    NSLog(@" %s", __FUNCTION__);
#endif
    
    NSMutableArray *ma = [[NSMutableArray alloc] init];
    [ma addObject:nil];    
    [ma release];
}

#endif

@end
