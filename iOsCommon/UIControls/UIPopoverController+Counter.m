//
//  UIPopoverController+Counter.m
//  ProductBuilder
//
//  Created by DSM on 10/28/13.
//  Copyright (c) 2013 Cloudworks. All rights reserved.
//

#import "UIPopoverController+Counter.h"
#import "NSObject+MethodExchange.h"
#import <objc/runtime.h>
#import "RTDatePickerViewController.h"
#import "RTSelectorButton.h"
#import "DDPopoverBackgroundView.h"
#import "Global.h"

static NSMutableArray *popovers;

const char *popoverProxyDelegateKey = "popoverProxyDelegateTag";


typedef struct{

    BOOL shouldDismiss;
    BOOL didDismiss;
} PopoverDelegateImplementedMethods;


@interface UIPopoverDelegateProxy : NSObject<UIPopoverControllerDelegate>{
    PopoverDelegateImplementedMethods delegateMethods;
}
@property (nonatomic, assign) id<UIPopoverControllerDelegate> delegate;
@end



@implementation UIPopoverDelegateProxy

-(void)setDelegate:(id<UIPopoverControllerDelegate>)aDelegate{
    _delegate = aDelegate;
    delegateMethods.shouldDismiss = [self.delegate respondsToSelector:@selector(popoverControllerShouldDismissPopover:)];
    delegateMethods.didDismiss = [self.delegate respondsToSelector:@selector(popoverControllerDidDismissPopover:)];
}


- (BOOL)popoverControllerShouldDismissPopover:(UIPopoverController *)popoverController{

    if (_delegate && delegateMethods.shouldDismiss)
        return [self.delegate popoverControllerShouldDismissPopover:popoverController];
    
    return YES;
}


- (void)popoverControllerDidDismissPopover:(UIPopoverController *)popoverController{

    [popovers removeObject:popoverController];
    
    if (_delegate && delegateMethods.didDismiss)
        [self.delegate popoverControllerDidDismissPopover:popoverController];
}

@end



@implementation UIPopoverController(Couter)

+(void)load{
    [[self class] exchangeMethod:@selector(presentPopoverFromRect:inView:permittedArrowDirections:animated:) withNewMethod:@selector(presentPopoverFromRectImpl:inView:permittedArrowDirections:animated:)];
    [[self class] exchangeMethod:@selector(presentPopoverFromBarButtonItem:permittedArrowDirections:animated:) withNewMethod:@selector(presentPopoverFromBarButtonItemImpl:permittedArrowDirections:animated:)];
    [[self class] exchangeMethod:@selector(dismissPopoverAnimated:) withNewMethod:@selector(dismissPopoverAnimatedImpl:)];
    
    [[self class] exchangeMethod:@selector(setDelegate:) withNewMethod:@selector(setDelegateImpl:)];
    [[self class] exchangeMethod:@selector(delegate) withNewMethod:@selector(delegateImpl)];
}


+ (void) initialize {
    
    popovers = [[NSMutableArray alloc] init];
}


- (void)initProxyDelegate {
    
    [self getProxyDelegate];
}


- (UIPopoverDelegateProxy *)getProxyDelegate {
    
    UIPopoverDelegateProxy* proxyDelegate = objc_getAssociatedObject(self, popoverProxyDelegateKey);
    
    if (proxyDelegate)
        return proxyDelegate;
        
    proxyDelegate = [[UIPopoverDelegateProxy alloc] init];
    objc_setAssociatedObject(self, popoverProxyDelegateKey, proxyDelegate, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    [self setDelegateImpl:proxyDelegate];
    [proxyDelegate release];
    
    return proxyDelegate;
}


- (void)presentPopoverFromRectImpl:(CGRect)rect inView:(UIView *)view permittedArrowDirections:(UIPopoverArrowDirection)arrowDirections animated:(BOOL)animated{
    
    [self presentPopoverFromRectImpl:rect inView:view permittedArrowDirections:arrowDirections animated:animated];
    
    [self initProxyDelegate];
    [popovers addObject:self];
}



- (void)presentPopoverFromBarButtonItemImpl:(UIBarButtonItem *)item permittedArrowDirections:(UIPopoverArrowDirection)arrowDirections animated:(BOOL)animated {
    
    [self presentPopoverFromBarButtonItemImpl:item permittedArrowDirections:arrowDirections animated:animated];
    
    [self initProxyDelegate];
    [popovers addObject:self];
}



- (void)dismissPopoverAnimatedImpl:(BOOL)animated {
    
    [popovers removeObject:self];
        
    [self dismissPopoverAnimatedImpl:animated];
}



-(void)setDelegateImpl:(id<UIPopoverControllerDelegate>)delegate {
    
    [self getProxyDelegate].delegate = delegate;
}


-(id<UIPopoverControllerDelegate>)delegateImpl {
    
    return [self getProxyDelegate].delegate;
}



+(NSInteger)popoversViewCount{
    
    return popovers.count;
}



+(void)closeAlerts{
    
    while (popovers.count > 0) {
        
        UIPopoverController *pc = popovers[0];
        
        NSObject *del = pc.delegate;
        UIViewController *cont = pc.contentViewController;
        
        if ([cont isKindOfClass:[RTDatePickerViewController class]] && [del isKindOfClass:[RTDateEditViewUnderlined class]]) {

            ((RTDateEditViewUnderlined *)del).delegate = nil;
            [((RTDatePickerViewController *)cont) close];
        }
        else {
            
            pc.delegate = nil;
            
            [pc dismissPopoverAnimated:NO];
            [popovers removeObject:pc];
        }
    }
}

@end
