//
//  MagazinePage.m
//  Lessons
//
//  Created by  on 1/17/12.
//  Copyright (c) 2012 Home. All rights reserved.
//

#import "MagazinePage.h"
#import <QuartzCore/QuartzCore.h>

@implementation MagazinePage

@synthesize maxScale = _maxScale, isCurrent = _isCurrent, visible = _visible, scale = _scale, contentView = _contentView;

////////////////////////////////////////////////////////////
- (id)init {

    self = [super init];

    if (self) {
#ifdef ShowLog
        NSLog(@" %s", __FUNCTION__);
#endif
        
        _scale = 1;
    }

    return self;
}


////////////////////////////////////////////////////////////
-(void)setFrame:(CGRect)frame {
    
    BOOL isChanged = (super.frame.size.width != frame.size.width || super.frame.size.height != frame.size.height);
    
    super.frame = frame;
    
    if (isChanged)
        [self updateContentPossitions];
}


////////////////////////////////////////////////////////////
-(BOOL)canZoom:(int)sign {

    if (!_canZoom || !_contentView || !_visible)
        return NO;
    
    CGSize contentSize = _contentView.frame.size;

    
    if (sign > 0) {
        
        CGSize maxSize = CGSizeMake(super.frame.size.width *_maxScale, super.frame.size.height *_maxScale);
        if (maxSize.width <= _contentView.frame.size.width || maxSize.height <= _contentView.frame.size.height)
            return NO;
    }
    else if (sign < 0)  {
        
        CGSize minSize = super.frame.size;
        if (minSize.width > contentSize.width || minSize.height > contentSize.height)
            return NO;
    }
        
    return YES;
}


////////////////////////////////////////////////////////////
-(void)scaleOn:(float)scale {
#ifdef ShowLog
    NSLog(@" %s", __FUNCTION__);
#endif
    
    self.scale = _scale + scale;
}


////////////////////////////////////////////////////////////
-(void)setScale:(float)scale {
    
    if (!_contentView || ![self canZoom:scale - _scale] || !_visible)
        return;
    
    if (scale > _maxScale)
        scale = _maxScale;
    else if (scale < 1)
        scale = 1;

    if (_scale == scale)
        return;
    
    _scale = scale;
    
    CGSize newSize = CGSizeMake(_originalSize.width*_scale, _originalSize.height*_scale);
    
    _contentView.frame = CGRectMake(_contentView.frame.origin.x - (newSize.width - _contentView.frame.size.width)/2, 
                                    _contentView.frame.origin.y - (newSize.height - _contentView.frame.size.height)/2,
                                    newSize.width, 
                                    newSize.height);
    _containerFrame = _contentView.frame;
    
    if (_containerFrame.size.width > self.frame.size.width)
        _containerFrame = CGRectMake(0, _containerFrame.origin.y, self.frame.size.width, _containerFrame.size.height);
    if (_containerFrame.size.height > self.frame.size.height)
        _containerFrame = CGRectMake(_containerFrame.origin.x, 0, _containerFrame.size.width, self.frame.size.height);
}


////////////////////////////////////////////////////////////
-(CGSize)moveTo:(CGSize)move {
#ifdef ShowLog
    NSLog(@" %s", __FUNCTION__);
#endif
    
    CGSize delta = CGSizeZero;
    
    CGFloat tempX = 0;
    
    if (move.width > 0 && self.frame.origin.x > 0) {
        
        if (self.frame.origin.x < move.width) {
            
            tempX = move.width - self.frame.origin.x;
            move = CGSizeMake(move.width - self.frame.origin.x, move.height);
        }
        else {
            
            tempX = move.width;
            move = CGSizeMake(0, move.height);
        }
    }    
    else if (move.width < 0 && self.frame.origin.x < 0) {
        
        if (self.frame.origin.x > move.width) {
            
            tempX = self.frame.origin.x - move.width;
            move = CGSizeMake(self.frame.origin.x - move.width, move.height);
        }
        else {
            
            tempX = move.width;
            move = CGSizeMake(0, move.height);
        }
    }
    
    
    if (!_contentView)
        return move;
    
    int moveX = move.width, 
        moveY = move.height;
    int toRightXMaxMove = (_containerFrame.origin.x + _contentView.frame.origin.x) < 0 ? -(_containerFrame.origin.x + _contentView.frame.origin.x) : 0, 
    toLeftXMaxMove = (_contentView.frame.size.width - self.frame.size.width) - toRightXMaxMove, 
    toBottomYMaxMove = (_contentView.frame.origin.y + _containerFrame.origin.y) < 0 ? -(_contentView.frame.origin.y + _containerFrame.origin.y) : 0, 
    toTopYMaxMove = (_contentView.frame.size.height - self.frame.size.height) - toBottomYMaxMove;
    
    if (toTopYMaxMove < 0)
        toTopYMaxMove = 0;
    if (toLeftXMaxMove < 0)
        toLeftXMaxMove = 0;
    
    CGPoint newPoint = _contentView.frame.origin;
    
    if (move.width > 0) {
        
        delta = CGSizeMake(toLeftXMaxMove > moveX ? 0 : moveX - toLeftXMaxMove, delta.height);
        newPoint = CGPointMake(toLeftXMaxMove > moveX ? newPoint.x - moveX : newPoint.x - toLeftXMaxMove, newPoint.y);
    }
    else if (move.width < 0){
        
        delta = CGSizeMake(toRightXMaxMove > -moveX ? 0 : moveX + toRightXMaxMove, delta.height);
        newPoint = CGPointMake(toRightXMaxMove > -moveX ? newPoint.x - moveX : newPoint.x, newPoint.y);
    }
    
    if (move.height > 0) {
        
        delta = CGSizeMake(delta.width, toTopYMaxMove > moveY ? 0 : moveY - toTopYMaxMove);
        newPoint = CGPointMake(newPoint.x, toTopYMaxMove > moveY ? newPoint.y - moveY : newPoint.y - toTopYMaxMove);
    }
    else if (move.height < 0){

        delta = CGSizeMake(delta.width, toBottomYMaxMove > -moveY ? 0 : -toBottomYMaxMove - moveY);
        newPoint = CGPointMake(newPoint.x, toBottomYMaxMove > -moveY ? newPoint.y - moveY : newPoint.y);
    }
    
    _contentView.frame = CGRectMake(newPoint.x, newPoint.y, _contentView.frame.size.width, _contentView.frame.size.height);
    
    return CGSizeMake(delta.width + tempX, delta.height);
}


////////////////////////////////////////////////////////////
-(void)setVisible:(BOOL)visible {
#ifdef ShowLog
    NSLog(@" %s", __FUNCTION__);
#endif
    
    if (_visible == visible)
        return;

    _visible = visible;
    
    if (!_visible && _contentView) {

        [_contentView removeFromSuperview];
        [_contentView release];
        _contentView = nil;
    }
    else if (_visible)
        [self updateContentPossitions];
}


////////////////////////////////////////////////////////////
-(void)updateContentPossitions {
#ifdef ShowLog
    NSLog(@" %s", __FUNCTION__);
#endif
    
    if (!_contentView || !_visible)
        return;
        
    _contentView.frame = self.bounds;
}


////////////////////////////////////////////////////////////
-(void)checkContentPossitions {
#ifdef ShowLog
    NSLog(@" %s", __FUNCTION__);
#endif
    
    if (!_contentView)
        return;
    
   
    if (_contentView.frame.size.width > self.frame.size.width) {
        
        if (_contentView.frame.origin.x > 0)
            _contentView.frame = CGRectMake(0, 
                                            _contentView.frame.origin.y,
                                            _contentView.frame.size.width, 
                                            _contentView.frame.size.height);
        if (_contentView.frame.origin.x + _contentView.frame.size.width < self.frame.size.width)
            _contentView.frame = CGRectMake(self.frame.size.width - _contentView.frame.size.width, 
                                            _contentView.frame.origin.y,
                                            _contentView.frame.size.width, 
                                            _contentView.frame.size.height);
    }
    else
        _contentView.frame = CGRectMake((self.frame.size.width - _contentView.frame.size.width)/2, 
                                        _contentView.frame.origin.y,
                                        _contentView.frame.size.width, 
                                        _contentView.frame.size.height);


    if (_contentView.frame.size.height > self.frame.size.height) {
        
        if (_contentView.frame.origin.y > 0)
            _contentView.frame = CGRectMake(_contentView.frame.origin.x, 
                                            0,
                                            _contentView.frame.size.width, 
                                            _contentView.frame.size.height);
        if (_contentView.frame.origin.y + _contentView.frame.size.height < self.frame.size.height)
            _contentView.frame = CGRectMake(_contentView.frame.origin.x, 
                                            self.frame.size.height - _contentView.frame.size.height,
                                            _contentView.frame.size.width, 
                                            _contentView.frame.size.height);
    }
    else
        _contentView.frame = CGRectMake(_contentView.frame.origin.x, 
                                        (self.frame.size.height - _contentView.frame.size.height)/2,
                                        _contentView.frame.size.width, 
                                        _contentView.frame.size.height);
    
    _containerFrame = _contentView.frame;
    
    if (_containerFrame.size.width > self.frame.size.width)
        _containerFrame = CGRectMake(0, _containerFrame.origin.y, self.frame.size.width, _containerFrame.size.height);
    if (_containerFrame.size.height > self.frame.size.height)
        _containerFrame = CGRectMake(_containerFrame.origin.x, 0, _containerFrame.size.width, self.frame.size.height);
}


////////////////////////////////////////////////////////////
-(UIImage*)imageRepresentation {
#ifdef ShowLog
    NSLog(@" %s", __FUNCTION__);
#endif
    
    UIGraphicsBeginImageContextWithOptions(CGSizeMake(self.frame.size.width, self.frame.size.height), 
                                           NO, 
                                           [UIScreen mainScreen].scale);
    [self.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *viewImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();

    return viewImage;
}


////////////////////////////////////////////////////////////
-(void)dealloc {
#ifdef ShowLog
    NSLog(@" %s", __FUNCTION__);
#endif
    
    [_contentView release];
    
    [super dealloc];
}

@end
