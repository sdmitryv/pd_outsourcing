//
//  MagazineAVVideoPage.h
//  ProductBuilder
//
//  Created by dev1 on 5/22/12.
//  Copyright (c) 2012 Cloudworks. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MagazinePage.h"
#import "CWMovieViewController.h"

@interface MagazineAVVideoPage : MagazinePage {
    NSString *_filePath; 
    CWMovieViewController *theMovie;
}

@property (nonatomic, retain) NSString *filePath;
@property (nonatomic, retain) NSString *fileServerPath;
@property (nonatomic, readonly) CWMovieViewController *theMovie;
@property (nonatomic, assign) BOOL supportsPseudoFullScreen;

@end
