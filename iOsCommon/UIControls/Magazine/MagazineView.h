//
//  MagazineView.h
//  Lessons
//
//  Created by  on 1/17/12.
//  Copyright (c) 2012 Home. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MagazinePage.h"

@protocol MagazineViewDelegate
  
    -(void)magazinPageChanged:(NSInteger)index;
    -(void)magazinAnimationBegined;
    -(void)magazinAnimationEnded;
    -(void)magazinOneClick;

@end


@interface MagazineView : UIView {
    
    NSMutableArray *_pages;
    
    CGSize _pageSize;
    NSInteger _pagesPadding;
    
    NSInteger _currentIndex;
    NSInteger _priorIndex;
    
    MagazinePage *leftPage, *currentPage, *rightPage;
    
    BOOL isMoving, swiping, isStoped, animationEnded;
    float startDistance, swipeStartX, swipeStartY;
        
    id<MagazineViewDelegate> delegate;
}

@property (nonatomic, retain) NSMutableArray *pages;

@property (nonatomic, assign) CGSize pageSize;
@property (nonatomic, assign) NSInteger pagesPadding;
@property (nonatomic, readonly) CGRect currentPageFrame;

@property (nonatomic, assign) NSInteger currentIndex;

@property (nonatomic, readonly) MagazinePage *currentPage;

@property (nonatomic, assign) id<MagazineViewDelegate> delegate;

-(void)updateContentPossitions;

-(void)nextPage;
-(void)priorPage;


@end
