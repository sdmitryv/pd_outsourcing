//
//  MagazineImagePage.m
//  
//
//  Created by  on 1/20/12.
//  Copyright (c) 2012 Home. All rights reserved.
//

#import "MagazineImagePage.h"
#import <QuartzCore/QuartzCore.h>

@implementation MagazineImagePage

@synthesize filePath = _filePath, image = _image, scaleType, widthForScale = _widthForScale;

////////////////////////////////////////////////////////////
- (id)init {
    
    self = [super init];
    if (self) {
        
        _canZoom = YES;
        _maxScale = 3;
        scaleType = scaleByPageWidthOrOriginal;
    }
    return self;
}


////////////////////////////////////////////////////////////
-(void)setFilePath:(NSString *)filePath {
    
    if (_contentView) {
        
        [_contentView removeFromSuperview];
        [_contentView release];
        _contentView = nil;
    }
    
    if (_filePath)
        [_filePath release];
    
    _filePath = [filePath retain];
    
    [self updateContentPossitions];
}


////////////////////////////////////////////////////////////
-(void)updateContentPossitions {
#ifdef ShowLog
    NSLog(@" %s", __FUNCTION__);
#endif
    
    if (!_contentView) {
        
        if (!_filePath && !_image)
            return;
        
        UIImageView *iv;
        if (_filePath) {

            UIImage *im = [[UIImage alloc] initWithContentsOfFile:_filePath];
            if (!im) {
                return;
            }
            iv = [[UIImageView alloc] initWithImage:im];
            [im release];
        }
        else
            iv = [[UIImageView alloc] initWithImage:_image];
        iv.contentMode = UIViewContentModeScaleAspectFit;
        [self addSubview:iv];
        _contentView = iv;
        _contentView.backgroundColor = [UIColor whiteColor];
    }
    
    _originalSize = ((UIImageView *)_contentView).image.size;
    if (scaleType == scaleByPageWidth)
        _originalSize = CGSizeMake(self.frame.size.width, _originalSize.height*self.frame.size.width/_originalSize.width);
    if (scaleType == scaleByPageWidthOrOriginal) {
     
        if (_originalSize.width > self.frame.size.width)
            _originalSize = CGSizeMake(self.frame.size.width, _originalSize.height*self.frame.size.width/_originalSize.width);
    }
    else if (scaleType == scaleAuto) {
        
        if (self.frame.size.height/_originalSize.height > self.frame.size.width/_originalSize.width)
            _originalSize = CGSizeMake(self.frame.size.width, _originalSize.height*self.frame.size.width/_originalSize.width);
        else
            _originalSize = CGSizeMake(_originalSize.width*self.frame.size.height/_originalSize.height, self.frame.size.height);
    }
    else if (scaleType == scaleByWidth) {
        
        _originalSize = CGSizeMake(_widthForScale, _originalSize.height*_widthForScale/_originalSize.width);
    }
    
    
    _containerFrame = CGRectMake((self.frame.size.width - _originalSize.width)/2,
                                 (self.frame.size.height - _originalSize.height)/2, 
                                 _originalSize.width, 
                                 _originalSize.height);
    _containerFrame = CGRectMake(_containerFrame.origin.x < 0 ? 0 : _containerFrame.origin.x, 
                                _containerFrame.origin.y < 0 ? 0 : _containerFrame.origin.y,
                                _containerFrame.size.width > self.frame.size.width ? self.frame.size.width : _containerFrame.size.width,
                                _containerFrame.size.height > self.frame.size.height ? self.frame.size.height : _containerFrame.size.height);

    _contentView.frame = CGRectMake(_containerFrame.origin.x, 
                                    _containerFrame.origin.y, 
                                    _originalSize.width, 
                                    _originalSize.height);
    [super setScale:1.0];
    
}


////////////////////////////////////////////////////////////
-(void)dealloc {
#ifdef ShowLog
    NSLog(@" %s", __FUNCTION__);
#endif
    
    [_filePath release];
    [_image release];
    
    [super dealloc];
}


@end
