//
//  UIPopoverController+Counter.h
//  ProductBuilder
//
//  Created by DSM on 10/28/13.
//  Copyright (c) 2013 Cloudworks. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UIPopoverController(Couter)


+(NSInteger)popoversViewCount;
+(void)closeAlerts;

@end
