//
//  Bar.h
//  Restaurant
//
//   on 8/16/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BarButton.h"
#import <QuartzCore/QuartzCore.h>

@protocol BarDelegate <NSObject>

@required
-(void)buttonPressed:(NSInteger)index sender:(id)sender;
-(void)buttonChecked:(NSInteger)index sender:(id)sender;

@optional
-(void)barAnimationBegined;
-(void)barAnimationEnded;
@end


@interface Bar:UIView<UIScrollViewDelegate, BarButtonDelegate> {
    
    CAGradientLayer *gradient;
    
    UIScrollView *_scroll;
    
    NSMutableArray *_buttons;
    
    NSInteger _checkedButtonIndex;
    
    id<BarDelegate> delegate;
    
    CGSize _buttonSize;
    
    BOOL _centerFirstAndLast;
    
    BOOL _centerChecked;
    
    NSInteger _offset;
}

@property (nonatomic, assign) id<BarDelegate> delegate;

@property (nonatomic, readonly) NSInteger checkedButtonIndex;

@property (nonatomic, assign) CGSize buttonSize;

@property (nonatomic, assign) BOOL centerFirstAndLast;

@property (nonatomic, assign) BOOL centerChecked;

@property (nonatomic, assign) NSInteger offset;

@property (nonatomic, readonly) CAGradientLayer *gradient;


-(void)addCheck:(UIView *)backgroundView
   disabledView:(UIView *)disabledView
      checkView:(UIView *)checkView
        padding:(int)padding;

-(void)clear;

-(void)checkItemAtIndex:(NSInteger)index;
-(BarButton *)barViewAtIndex:(NSInteger)index;

-(NSInteger)getItemsCount;

@end
