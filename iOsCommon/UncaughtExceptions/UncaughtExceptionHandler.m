//
//  UncaughtExceptionHandler.m
//  UncaughtExceptions
//
//  Created by Matt Gallagher on 2010/05/25.
//  Copyright 2010 Matt Gallagher. All rights reserved.
//
//  Permission is given to use this source code file, free of charge, in any
//  project, commercial or otherwise, entirely at your risk, with the condition
//  that any redistribution (in part or whole) of source code must retain
//  this copyright and permission notice. Attribution in compiled projects is
//  appreciated but not required.
//

#import "UncaughtExceptionHandler.h"
#include <libkern/OSAtomic.h>
#include <execinfo.h>

#import "Reporter.h"
#import "CrashInfo.h"

NSString * const UncaughtExceptionHandlerSignalExceptionName = @"UncaughtExceptionHandlerSignalExceptionName";
NSString * const UncaughtExceptionHandlerSignalKey = @"UncaughtExceptionHandlerSignalKey";
NSString * const UncaughtExceptionHandlerAddressesKey = @"UncaughtExceptionHandlerAddressesKey";
NSString * const UncaughtExceptionHandlerExceptionTypeKey = @"UncaughtExceptionHandlerExceptionTypeKey";

volatile int32_t UncaughtExceptionCount = 0;
const int32_t UncaughtExceptionMaximum = 10;

const NSInteger UncaughtExceptionHandlerSkipAddressCount = 4; // was 4!
const NSInteger UncaughtExceptionHandlerReportAddressCount = 5;

static UncaughtExceptionHandler *handlerInstance = nil;

@implementation UncaughtExceptionHandler

+ (void)initExceptionHandlerWithAppId:(NSString *)appId logServerUrl:(NSString *)logServerUrl {
    if (!handlerInstance) {
        handlerInstance = [[UncaughtExceptionHandler alloc] init];
        [Reporter sharedInstance].appId = appId;
        [Reporter sharedInstance].logServerUrl = logServerUrl;
    }
    
    [handlerInstance performSelector:@selector(install) withObject:nil afterDelay:0];
}

- (void)install {
    InstallUncaughtExceptionHandler();
}

+ (NSArray *)backtrace
{
    void* callstack[128];
    int frames = backtrace(callstack, 128);
    char **strs = backtrace_symbols(callstack, frames);
    
    int i;
    NSMutableArray *backtrace = [NSMutableArray arrayWithCapacity:frames];
    //    for (
    //         i = UncaughtExceptionHandlerSkipAddressCount;
    //         i < UncaughtExceptionHandlerSkipAddressCount +
    //         UncaughtExceptionHandlerReportAddressCount;
    //         i++)
    //    {
    //	 	[backtrace addObject:[NSString stringWithUTF8String:strs[i]]];
    //    }
    for (i = 0; i < frames; i++)
    {
	 	[backtrace addObject:@(strs[i])];
    }
    free(strs);
    
    return backtrace;
}

+ (NSString *)composeStackTraceStringFromArray:(NSArray *)arr reason:(NSString *)reason {
    if (arr) {
        NSMutableString *val = [[NSMutableString alloc] initWithFormat:@"%@\n", reason];
        for (int i=0; i<[arr count]; i++) {
            NSString *str = arr[i];
            [val appendFormat:@"%@\n", str];
        }
        
        return [val autorelease];
    }
    return @"";
}

- (void)alertView:(UIAlertView *)anAlertView clickedButtonAtIndex:(NSInteger)anIndex
{
	if (anIndex == 0)
	{
		dismissed = YES;
	}
}

- (void)validateAndSaveCriticalApplicationData
{
	
}

- (void)handleException:(NSException *)exception
{
    
    CrashInfo *info = [[CrashInfo alloc] init];
    info.stackTrace = [UncaughtExceptionHandler composeStackTraceStringFromArray:[exception userInfo][UncaughtExceptionHandlerAddressesKey] reason:[exception reason]];
    info.date = [NSDate date];
    info.logType = [[exception userInfo][UncaughtExceptionHandlerExceptionTypeKey] intValue];
    [[Reporter sharedInstance] insertCrashInfo:info];
    [info release];

    
	
//    UIAlertView *alert =
//    [[[UIAlertView alloc]
//      initWithTitle:NSLocalizedString(@"Unhandled exception", nil)
//      message:[NSString stringWithFormat:NSLocalizedString(
//                                                           @"You can try to continue but the application may be unstable.\n\n"
//                                                           @"Debug details follow:\n%@\n%@", nil),
//               [exception reason],
//               [[exception userInfo] objectForKey:UncaughtExceptionHandlerAddressesKey]]
//      delegate:self
//      cancelButtonTitle:NSLocalizedString(@"Quit", nil)
//      otherButtonTitles:NSLocalizedString(@"Continue", nil), nil]
//     autorelease];
//    [alert show];
    NSLog(@"%@", [exception reason]);
    NSLog(@"%@", [exception userInfo][UncaughtExceptionHandlerAddressesKey]);
    
    [self performSelector:@selector(crash) withObject:nil afterDelay:5.0f];
	
	CFRunLoopRef runLoop = CFRunLoopGetCurrent();
	CFArrayRef allModes = CFRunLoopCopyAllModes(runLoop);
	
	while (!dismissed)
	{
		for (NSString *mode in (NSArray *)allModes)
		{
			CFRunLoopRunInMode((CFStringRef)mode, 0.001, false);
		}
	}
	
	CFRelease(allModes);
    
	NSSetUncaughtExceptionHandler(NULL);
//	signal(SIGABRT, SIG_DFL);
//	signal(SIGILL, SIG_DFL);
//	signal(SIGSEGV, SIG_DFL);
//	signal(SIGFPE, SIG_DFL);
//	signal(SIGBUS, SIG_DFL);
//	signal(SIGPIPE, SIG_DFL);
    struct sigaction mySigAction;
    mySigAction.sa_sigaction = NULL;
    mySigAction.sa_flags = SA_SIGINFO;
    sigemptyset(&mySigAction.sa_mask);
    sigaction(SIGILL, &mySigAction, NULL); 
    sigaction(SIGTRAP, &mySigAction, NULL);
    sigaction(SIGABRT, &mySigAction, NULL); 
    sigaction(SIGFPE, &mySigAction, NULL); 
    sigaction(SIGBUS, &mySigAction, NULL); 
    sigaction(SIGSEGV, &mySigAction, NULL); 
    sigaction(SIGSYS, &mySigAction, NULL); 
    sigaction(SIGPIPE, &mySigAction, NULL); 
	
	if ([[exception name] isEqual:UncaughtExceptionHandlerSignalExceptionName])
	{
		kill(getpid(), [[exception userInfo][UncaughtExceptionHandlerSignalKey] intValue]);
	}
	else
	{
		[exception raise];
	}
}

- (void)crash {
    dismissed = YES;
}

@end

void HandleException(NSException *exception)
{
	int32_t exceptionCount = OSAtomicIncrement32(&UncaughtExceptionCount);
	if (exceptionCount > UncaughtExceptionMaximum)
	{
		return;
	}
	
	NSArray *callStack = [exception callStackSymbols];//[UncaughtExceptionHandler backtrace];
	NSMutableDictionary *userInfo =
    [NSMutableDictionary dictionaryWithDictionary:[exception userInfo]];
	userInfo[UncaughtExceptionHandlerAddressesKey] = callStack;
    userInfo[UncaughtExceptionHandlerExceptionTypeKey] = @(LogTypeException);
	
	[handlerInstance
     performSelectorOnMainThread:@selector(handleException:)
     withObject:
     [NSException
      exceptionWithName:[exception name]
      reason:[exception reason]
      userInfo:userInfo]
     waitUntilDone:YES];
}

void SignalHandler(int signal)
{
	int32_t exceptionCount = OSAtomicIncrement32(&UncaughtExceptionCount);
	if (exceptionCount > UncaughtExceptionMaximum)
	{
		return;
	}
	
	NSMutableDictionary *userInfo =
    [NSMutableDictionary
     dictionaryWithObject:@(signal)
     forKey:UncaughtExceptionHandlerSignalKey];
    
	NSArray *callStack = [UncaughtExceptionHandler backtrace];
	userInfo[UncaughtExceptionHandlerAddressesKey] = callStack;
    userInfo[UncaughtExceptionHandlerExceptionTypeKey] = @(LogTypeSignal);
	
	[handlerInstance
     performSelectorOnMainThread:@selector(handleException:)
     withObject:
     [NSException
      exceptionWithName:UncaughtExceptionHandlerSignalExceptionName
      reason:
      [NSString stringWithFormat:
       NSLocalizedString(@"Signal %d was raised.", nil),
       signal]
      userInfo:userInfo]
     waitUntilDone:YES];
}

void mysighandler(int signal, siginfo_t *info, void *context) {
    int32_t exceptionCount = OSAtomicIncrement32(&UncaughtExceptionCount);
	if (exceptionCount > UncaughtExceptionMaximum)
	{
		return;
	}
	
	NSMutableDictionary *userInfo =
    [NSMutableDictionary
     dictionaryWithObject:@(signal)
     forKey:UncaughtExceptionHandlerSignalKey];
    
	NSArray *callStack = [UncaughtExceptionHandler backtrace];
	userInfo[UncaughtExceptionHandlerAddressesKey] = callStack;
    userInfo[UncaughtExceptionHandlerExceptionTypeKey] = @(LogTypeSignal);
	
	[handlerInstance
     performSelectorOnMainThread:@selector(handleException:)
     withObject:
     [NSException
      exceptionWithName:UncaughtExceptionHandlerSignalExceptionName
      reason:
      [NSString stringWithFormat:
       NSLocalizedString(@"Signal %d was raised.", nil),
       signal]
      userInfo:userInfo]
     waitUntilDone:YES];
}



void InstallUncaughtExceptionHandler()
{
	NSSetUncaughtExceptionHandler(&HandleException);
//    signal(SIGABRT, SignalHandler);
//    signal(SIGILL, SignalHandler);
//    signal(SIGSEGV, SignalHandler);
//    signal(SIGFPE, SignalHandler);
//    signal(SIGBUS, SignalHandler);
//    signal(SIGPIPE, SignalHandler);
//    signal(SIGSYS, SignalHandler);
//    signal(SIGTRAP, SignalHandler);
    
    struct sigaction mySigAction;
    mySigAction.sa_sigaction = mysighandler;
    mySigAction.sa_flags = SA_SIGINFO;
    sigemptyset(&mySigAction.sa_mask);
    sigaction(SIGILL, &mySigAction, NULL); 
    sigaction(SIGTRAP, &mySigAction, NULL);
    sigaction(SIGABRT, &mySigAction, NULL); 
    sigaction(SIGFPE, &mySigAction, NULL); 
    sigaction(SIGBUS, &mySigAction, NULL); 
    sigaction(SIGSEGV, &mySigAction, NULL); 
    sigaction(SIGSYS, &mySigAction, NULL); 
    sigaction(SIGPIPE, &mySigAction, NULL); 
    
    [[Reporter sharedInstance] startService];
    
}

