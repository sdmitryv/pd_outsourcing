 //
//  main.m
//  CloudworksPOS
//
//  Created by Lulakov Viacheslav on 6/22/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "DecimalHelper.h"
#import "Currency.h"

#import "Receipt.h"
#import "ReceiptPayment.h"
#import "NSDate+ISO8601Parsing.h"
#import "MathUtils.h"
#import "SOAPResponseParser.h"
#import "SyncOperation.h"
#import "BackupDbOperation.h"
#import "SyncTableOperation.h"
#import "ReverseOrphanedCCPaymentsOperation.h"
#import "RTEncryption.h"
#import "NSMutableDictionaryFast.h"
#import "UploadReceiptsOperation.h"
#import "NSURLSesstion+Extentions.h"

@interface CDummy : NSObject
@property (nonatomic, retain)NSString* name;
@end

@implementation CDummy
@synthesize name;
-(id)init{
    if ((self=[super init])){
        [NSNotificationCenter defaultCenter];
        
        [[NSNotificationCenter defaultCenter] addObserver : self
                                                 selector : @selector(threadWillExit:)
                                                     name : NSThreadWillExitNotification
                                                   object : [NSThread currentThread]];
    }
    return self;
}

+(CDummy*)currentDummyTLS{
    CDummy* dummy = [[NSThread currentThread].threadDictionary objectForKey:@"dummy"];
    if (!dummy){
        dummy = [[CDummy alloc]init];
        [[NSThread currentThread].threadDictionary setObject:dummy forKey:@"dummy"];
        [dummy release];
    }
    return dummy;
}

-(void)dealloc{
    NSLog(@"dummy dealloc");
    [name release];
    [super dealloc];
}

-(void)threadWillExit:(NSNotification*)notification{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    NSLog(@"thread will exit");
}


@end

void myFinalizerFunction(void *context)
{
    CDummy* theData = (CDummy*)context;
    NSLog(@"%@",theData.name);
    
}

void testReceiptXML(){
    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
    xmlDocPtr doc;
    NSData* faultData = [NSData dataWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"soapResponse" ofType:@"xml"]];
    doc = xmlParseMemory([faultData bytes], [faultData length]);
    xmlNodePtr node = doc->children;
    if (xmlStrEqual(node->name,(const xmlChar*)"GetOriginalReceiptResult")){
        
        XmlElement* result = [[[XmlElement alloc]initWithXmlNodePtr:node]autorelease];
        NSArray* receipts = [result childrenWithNameByPath:@"Receipt"];
        for (XmlElement* receipt in receipts){
            
            NSArray* receiptItems = [receipt childrenWithNameByPath:@"ReceiptItems\\ReceiptItem"];
            
            EGODatabaseResult*aResult = [receipt childrenToEGODatabaseResultByPath:@"ReceiptItems\\ReceiptItem"];
            for(EGODatabaseRow* row in aResult.rows){
                ReceiptItem* ri = [[ReceiptItem alloc]initWithRow:row];
                NSLog(@"%@",ri);
                [ri release];
                
            }
            
            for (XmlElement* receiptItem in receiptItems){
                NSArray* receiptItemTaxes = [receiptItem childrenWithNameByPath:@"ReceiptItemTaxes\\ReceiptItemTax"];    
                for (XmlElement* receiptItemTax in receiptItemTaxes){
                    NSLog(@"%@",receiptItemTax.children);    
                }
            }
            NSArray* receiptPayments = [receipt childrenWithNameByPath:@"ReceiptPayments\\ReceiptPayment"];
            for (XmlElement* receiptPayment in receiptPayments){
                NSLog(@"%@",receiptPayment.children);
            }
        }
    }
    xmlFreeDoc(doc);
    [pool release];
}

void testDecimals(){
    //NSDecimal d1 = CPDecimalFromInt(100);
    NSDecimal d = CPDecimalFromFloat(-13.825);
    //d = CPDecimalABS(d);
    //d._isNegative = !d._isNegative;
    //NSDecimal d = CPDecimalFromFloat(22.5);
    // NSDecimalNumber* dn = [NSDecimalNumber decimalNumberWithDecimal:d];
    NSDecimalNumber* dn = [NSDecimalNumber decimalNumberWithDecimal:[MathUtils roundMoney:d]];
    //NSDecimal result;
	//NSDecimalRound(&result, &d, 0, NSRoundBankers);
    //NSDecimalNumber* dn = [NSDecimalNumber decimalNumberWithDecimal:result];
    
    NSLog(@"%@",dn);
}

void testHTTPRequests(){
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init] ;
    [request setURL:[NSURL URLWithString:@"http://10.0.0.1"]];
    [request setTimeoutInterval:10.0];
    [request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    //[request setHTTPMethod:@"GET"];
    //set headers
    //[request addValue:@"text/xml" forHTTPHeaderField: @"Content-Type"];
    //[request setHTTPBody:requestData];
    
    NSURLResponse* httpResponse;
    NSError* responseError;
    NSData* responseData = [NSURLSesstion sendSynchronousRequest:request returningResponse:&httpResponse error:&responseError];
    if (responseError){
        NSLog(@"%@",responseError);
    }
    else{
        NSLog(@"%@",responseData);
    }
    [request release];
}

void testQueues(){
    dispatch_queue_t q = dispatch_queue_create("myqueue", NULL);
    
    //    dispatch_queue_t q1 = dispatch_queue_create("myqueue1", NULL);
    //    dispatch_queue_t q2 = dispatch_queue_create("myqueue2", NULL);
    //
    //    dispatch_async(q1,^{
    //        NSLog(@"queue1 thread:%@",[NSThread currentThread]);
    //        dispatch_async(q2,^{
    //            NSLog(@"queue2 thread:%@",[NSThread currentThread]);
    //            });
    //        [NSThread sleepForTimeInterval:2];
    //        });
    NSLog(@"m:current thread:%@",[NSThread currentThread]);
    dispatch_async(q, ^{
        NSLog(@"q:current thread:%@",[NSThread currentThread]);
        //void *context = dispatch_get_context(dispatch_get_current_queue());
        //if (!context){
        CDummy* dummy = [CDummy currentDummyTLS];
        NSLog(@"%@",dummy.name);
        //dispatch_set_context(dispatch_get_current_queue(), dummy);
        //dispatch_set_finalizer_f(dispatch_get_current_queue(), &myFinalizerFunction);
        // }
        
        [NSThread sleepForTimeInterval:2];
        ;});
    dispatch_release(q);
    [NSThread sleepForTimeInterval:20];
}

void testDates(){
    NSString* dateStr = @"1974-12-31T13:50:18.553";
    //NSString* dateStr = @"1753-12-31T13:50:18.553";
    //    NSTimeInterval ti1;
    //    BOOL result = [NSDate parseISO8601String:dateStr inTimeIntervalSince1970:&ti1];
    //    NSDate* date2 = [NSDate dateWithISO8601String:dateStr];
    //    NSTimeInterval ti2 = [date2 timeIntervalSince1970];
    
    int count = 100000;
    NSTimeInterval start = [NSDate timeIntervalSinceReferenceDate];
    NSTimeInterval ti;
    for (NSInteger i = 0;i<count;i++){
        [NSDate parseISO8601String:dateStr inTimeIntervalSince1970:&ti];
    }
    NSLog(@"Parse time using c runtime only is: %f",  [NSDate timeIntervalSinceReferenceDate] - start);
    
    start = [NSDate timeIntervalSinceReferenceDate];
    for (NSInteger i = 0;i<count;i++){
        [[NSDate dateWithISO8601String:dateStr] timeIntervalSince1970];
    }
    NSLog(@"Parse time using NSDate components is: %f",  [NSDate timeIntervalSinceReferenceDate] - start);
}

void testCurrency(){
    NSString *localCurID = [[Currency localCurrency].id description];
    NSLog(@"Local Currency ID: %@", localCurID);
    
    
    Receipt *receipt = [[Receipt alloc] init];
    BPUUID *ID = [[BPUUID alloc] initWithString:@"D02F8E7C-6184-4F2F-935E-E00C3C990030"];
    PaymentMethod *paymentMethod = [PaymentMethod getInstanceById:ID];
    ReceiptPayment *receiptPayment = [[ReceiptPayment alloc] initWithinReceipt:receipt paymentMethod:paymentMethod];
    receiptPayment.paymentAmount = CPDecimalFromInteger(23);
    NSLog(@"Foreign currency amount: %@", CPDecimalStringValue(receiptPayment.foreignCurrencyAmount));
    
    receiptPayment.foreignCurrencyAmount = CPDecimalFromInteger(43);
    NSLog(@"Payment amount: %@", CPDecimalStringValue(receiptPayment.paymentAmount));
    //int retVal = UIApplicationMain(argc, argv, nil, nil);
    [receiptPayment release];
    [ID release];
    [receipt release];
}

void testMTOM(){
    BackupDbOperation* op = [[BackupDbOperation alloc]init];
    SyncOperationQueue * queue = [[SyncOperationQueue alloc]init] ;
    [queue addOperation:op];
    [queue waitUntilAllOperationsAreFinished];
    [queue release];
    if ([op.operationResult isKindOfClass:[NSNumber class]]){
        NSInteger result = [(NSNumber*)op.operationResult intValue];
        NSLog(@"result=%i",result);
    }
    [op release];
}

void testSAX(){
    
    //    for (NSInteger i = 0; i<100000000; i++){
    //        NSObject* v = [[NSObject alloc]init];
    //        [v release];
    //    }
    //    return;
    @autoreleasepool {
        xmlInitParser();
        [[DataManager instance] enableDatabaseAccess];
        SyncTableOperation* op = [SyncTableOperation syncTableOperation:@"InvenItem" mode:SyncOperationModeInitialization];
        SyncOperationQueue * queue = [[SyncOperationQueue alloc]init] ;
        NSTimeInterval start = [NSDate timeIntervalSinceReferenceDate];
        [queue addOperation:op];
        [queue waitUntilAllOperationsAreFinished];
        if (op.lastError){
            NSLog(@"%@", op.lastError);    
        }
        NSLog(@"Running time is: %f",  [NSDate timeIntervalSinceReferenceDate] - start);
        [queue release];
        xmlCleanupParser();
    }
}

static char firstNamePages[] =
{
    0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x00,
    0x00, 0x09, 0x0A, 0x0B, 0x0C, 0x0D, 0x0E, 0x0F,
    0x10, 0x11, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x12, 0x13,
    0x00, 0x14, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x15, 0x16, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x01,
    0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01,
    0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01,
    0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01,
    0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01,
    0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01,
    0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01,
    0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01,
    0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01,
    0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01,
    0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x17,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x01, 0x01, 0x01, 0x01,
    0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01,
    0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01,
    0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01,
    0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01,
    0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x18,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
};

static NSUInteger nameBitmap[] =
{
    0x00000000, 0x00000000, 0x00000000, 0x00000000,
    0x00000000, 0x00000000, 0x00000000, 0x00000000,
    0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF,
    0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF,
    0x00000000, 0x04000000, 0x87FFFFFE, 0x07FFFFFE,
    0x00000000, 0x00000000, 0xFF7FFFFF, 0xFF7FFFFF,
    0xFFFFFFFF, 0x7FF3FFFF, 0xFFFFFDFE, 0x7FFFFFFF,
    0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFE00F, 0xFC31FFFF,
    0x00FFFFFF, 0x00000000, 0xFFFF0000, 0xFFFFFFFF,
    0xFFFFFFFF, 0xF80001FF, 0x00000003, 0x00000000,
    0x00000000, 0x00000000, 0x00000000, 0x00000000,
    0xFFFFD740, 0xFFFFFFFB, 0x547F7FFF, 0x000FFFFD,
    0xFFFFDFFE, 0xFFFFFFFF, 0xDFFEFFFF, 0xFFFFFFFF,
    0xFFFF0003, 0xFFFFFFFF, 0xFFFF199F, 0x033FCFFF,
    0x00000000, 0xFFFE0000, 0x027FFFFF, 0xFFFFFFFE,
    0x0000007F, 0x00000000, 0xFFFF0000, 0x000707FF,
    0x00000000, 0x07FFFFFE, 0x000007FE, 0xFFFE0000,
    0xFFFFFFFF, 0x7CFFFFFF, 0x002F7FFF, 0x00000060,
    0xFFFFFFE0, 0x23FFFFFF, 0xFF000000, 0x00000003,
    0xFFF99FE0, 0x03C5FDFF, 0xB0000000, 0x00030003,
    0xFFF987E0, 0x036DFDFF, 0x5E000000, 0x001C0000,
    0xFFFBAFE0, 0x23EDFDFF, 0x00000000, 0x00000001,
    0xFFF99FE0, 0x23CDFDFF, 0xB0000000, 0x00000003,
    0xD63DC7E0, 0x03BFC718, 0x00000000, 0x00000000,
    0xFFFDDFE0, 0x03EFFDFF, 0x00000000, 0x00000003,
    0xFFFDDFE0, 0x03EFFDFF, 0x40000000, 0x00000003,
    0xFFFDDFE0, 0x03FFFDFF, 0x00000000, 0x00000003,
    0x00000000, 0x00000000, 0x00000000, 0x00000000,
    0xFFFFFFFE, 0x000D7FFF, 0x0000003F, 0x00000000,
    0xFEF02596, 0x200D6CAE, 0x0000001F, 0x00000000,
    0x00000000, 0x00000000, 0xFFFFFEFF, 0x000003FF,
    0x00000000, 0x00000000, 0x00000000, 0x00000000,
    0x00000000, 0x00000000, 0x00000000, 0x00000000,
    0x00000000, 0xFFFFFFFF, 0xFFFF003F, 0x007FFFFF,
    0x0007DAED, 0x50000000, 0x82315001, 0x002C62AB,
    0x40000000, 0xF580C900, 0x00000007, 0x02010800,
    0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF,
    0x0FFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0x03FFFFFF,
    0x3F3FFFFF, 0xFFFFFFFF, 0xAAFF3F3F, 0x3FFFFFFF,
    0xFFFFFFFF, 0x5FDFFFFF, 0x0FCF1FDC, 0x1FDC1FFF,
    0x00000000, 0x00004C40, 0x00000000, 0x00000000,
    0x00000007, 0x00000000, 0x00000000, 0x00000000,
    0x00000080, 0x000003FE, 0xFFFFFFFE, 0xFFFFFFFF,
    0x001FFFFF, 0xFFFFFFFE, 0xFFFFFFFF, 0x07FFFFFF,
    0xFFFFFFE0, 0x00001FFF, 0x00000000, 0x00000000,
    0x00000000, 0x00000000, 0x00000000, 0x00000000,
    0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF,
    0xFFFFFFFF, 0x0000003F, 0x00000000, 0x00000000,
    0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF,
    0xFFFFFFFF, 0x0000000F, 0x00000000, 0x00000000,
    0x00000000, 0x07FF6000, 0x87FFFFFE, 0x07FFFFFE,
    0x00000000, 0x00800000, 0xFF7FFFFF, 0xFF7FFFFF,
    0x00FFFFFF, 0x00000000, 0xFFFF0000, 0xFFFFFFFF,
    0xFFFFFFFF, 0xF80001FF, 0x00030003, 0x00000000,
    0xFFFFFFFF, 0xFFFFFFFF, 0x0000003F, 0x00000003,
    0xFFFFD7C0, 0xFFFFFFFB, 0x547F7FFF, 0x000FFFFD,
    0xFFFFDFFE, 0xFFFFFFFF, 0xDFFEFFFF, 0xFFFFFFFF,
    0xFFFF007B, 0xFFFFFFFF, 0xFFFF199F, 0x033FCFFF,
    0x00000000, 0xFFFE0000, 0x027FFFFF, 0xFFFFFFFE,
    0xFFFE007F, 0xBBFFFFFB, 0xFFFF0016, 0x000707FF,
    0x00000000, 0x07FFFFFE, 0x0007FFFF, 0xFFFF03FF,
    0xFFFFFFFF, 0x7CFFFFFF, 0xFFEF7FFF, 0x03FF3DFF,
    0xFFFFFFEE, 0xF3FFFFFF, 0xFF1E3FFF, 0x0000FFCF,
    0xFFF99FEE, 0xD3C5FDFF, 0xB080399F, 0x0003FFCF,
    0xFFF987E4, 0xD36DFDFF, 0x5E003987, 0x001FFFC0,
    0xFFFBAFEE, 0xF3EDFDFF, 0x00003BBF, 0x0000FFC1,
    0xFFF99FEE, 0xF3CDFDFF, 0xB0C0398F, 0x0000FFC3,
    0xD63DC7EC, 0xC3BFC718, 0x00803DC7, 0x0000FF80,
    0xFFFDDFEE, 0xC3EFFDFF, 0x00603DDF, 0x0000FFC3,
    0xFFFDDFEC, 0xC3EFFDFF, 0x40603DDF, 0x0000FFC3,
    0xFFFDDFEC, 0xC3FFFDFF, 0x00803DCF, 0x0000FFC3,
    0x00000000, 0x00000000, 0x00000000, 0x00000000,
    0xFFFFFFFE, 0x07FF7FFF, 0x03FF7FFF, 0x00000000,
    0xFEF02596, 0x3BFF6CAE, 0x03FF3F5F, 0x00000000,
    0x03000000, 0xC2A003FF, 0xFFFFFEFF, 0xFFFE03FF,
    0xFEBF0FDF, 0x02FE3FFF, 0x00000000, 0x00000000,
    0x00000000, 0x00000000, 0x00000000, 0x00000000,
    0x00000000, 0x00000000, 0x1FFF0000, 0x00000002,
    0x000000A0, 0x003EFFFE, 0xFFFFFFFE, 0xFFFFFFFF,
    0x661FFFFF, 0xFFFFFFFE, 0xFFFFFFFF, 0x77FFFFFF
};

static char namePages[] =
{
    0x19, 0x03, 0x1A, 0x1B, 0x1C, 0x1D, 0x1E, 0x00,
    0x00, 0x1F, 0x20, 0x21, 0x22, 0x23, 0x24, 0x25,
    0x10, 0x11, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x12, 0x13,
    0x26, 0x14, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x27, 0x16, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x01,
    0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01,
    0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01,
    0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01,
    0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01,
    0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01,
    0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01,
    0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01,
    0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01,
    0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01,
    0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x17,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x01, 0x01, 0x01, 0x01,
    0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01,
    0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01,
    0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01,
    0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01,
    0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x18,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
};

BOOL IsNameChar (int ch)
{
    if ((ch >= 'a' && ch <= 'z') || (ch >= 'A' && ch <= 'Z')) {
        return true;
    } else if ((uint) ch <= 0xFFFF) {
        return (nameBitmap[(namePages[ch >> 8] << 3) + ((ch & 0xFF) >> 5)] & (1 << (ch & 0x1F))) != 0;
    } else {
        return false;
    }
}

BOOL IsFirstNameChar (int ch)
{
    if ((ch >= 'a' && ch <= 'z') || (ch >= 'A' && ch <= 'Z')) {
        return true;
    } else if ((uint) ch <= 0xFFFF) {
        return (nameBitmap[(firstNamePages[ch >> 8] << 3) + ((ch & 0xFF) >> 5)] & (1 << (ch & 0x1F))) != 0;
    }
    
    return false;
}

BOOL IsInvalid (char c, bool firstOnlyLetter)
{
    if (c == ':') // Special case. allowed in EncodeName, but encoded in EncodeLocalName
        return false;
    
    if (firstOnlyLetter)
        return !IsFirstNameChar (c);
    else
        return !IsNameChar (c);
}

NSString*encodeName(NSString* name){
    if (!name|| !name.length)
        return name;
    BOOL nmtoken = FALSE;
    NSMutableString* str = [[NSMutableString alloc]init];
    int length = name.length;
    for (NSInteger i = 0; i < length; i++) {
        char c = [name characterAtIndex:i];
        if (IsInvalid (c, i == 0 && !nmtoken))
            [str appendFormat:@"_x%04x_", (int) c];
        else if (c == '_' && i + 6 < length && [name characterAtIndex:i+1] == 'x' && [name characterAtIndex:i + 6] == '_')
            [str appendString:@"_x005F_"];
        else
            [str appendString:[NSString stringWithFormat:@"%c",c]];
    }
    return [str autorelease];
}

#import "XmlConvert.h"
void textXML(){
    
    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
    NSString *c, *c2, *c3;
    @autoreleasepool {
        c = [[XmlConvert encodeName:@"item__x005F_"] retain];
        c2 =  [[XmlConvert decodeName:c] retain];
        c3 = [[XmlConvert decodeNameFromUTF8String:[c UTF8String]] retain];
        
        [c release];
        [c2 release];
        [c3 release];
    }
    
    
    int count = 1;
    NSTimeInterval start = [NSDate timeIntervalSinceReferenceDate];
    for (NSInteger i = 0;i<count;i++){
        //NSString* c2 = 
        [XmlConvert decodeName:c];
    }
    NSLog(@"Parse time using c NSString only is: %f",  [NSDate timeIntervalSinceReferenceDate] - start);
    
    
    start = [NSDate timeIntervalSinceReferenceDate];
    for (NSInteger i = 0;i<count;i++){
        //NSString* c3 = 
        [XmlConvert decodeNameFromUTF8String:[c UTF8String]];
    }
    NSLog(@"Parse time using UTF8String is: %f",  [NSDate timeIntervalSinceReferenceDate] - start);
    
    
    
    xmlDocPtr doc = xmlNewDoc((const xmlChar*)XML_DEFAULT_VERSION);
	xmlNodePtr root = xmlNewDocNode(doc, NULL, (const xmlChar*)"Envelope", NULL);
	xmlDocSetRootElement(doc, root);
    NSString* name = encodeName(@"MobilePrintingDesign");
	xmlNodePtr bodyNode = xmlNewDocNode(doc, nil, (const xmlChar*)[name UTF8String], NULL);
    xmlAddChild(root, bodyNode);
    NSString* body = @"&amp;amp;";
    xmlNodePtr bodyNodeContent =  [body xmlNodeForDoc:doc elementName:@"key" elementNSPrefix:nil];
	xmlAddChild(bodyNode, bodyNodeContent);
    
    //NSString* s = [NSString deserializeNode:bodyNodeContent];
    
	
	xmlChar *buf;
	int size;
	xmlDocDumpFormatMemory(doc, &buf, &size, 1);
	
	NSString *serializedForm = [NSString stringWithCString:(const char*)buf encoding:NSUTF8StringEncoding];
	xmlFree(buf);
	
	xmlFreeDoc(doc);	
    NSLog(@"%@", serializedForm);
    [pool release];
}


void testReverseRRP(){
    
    //    for (NSInteger i = 0; i<100000000; i++){
    //        NSObject* v = [[NSObject alloc]init];
    //        [v release];
    //    }
    //    return;
    xmlInitParser();
    ReverseOrphanedCCPaymentsOperation* op = [[ReverseOrphanedCCPaymentsOperation alloc]init];
    SyncOperationQueue * queue = [[SyncOperationQueue alloc]init] ;
    [queue addOperation:op];
    [queue waitUntilAllOperationsAreFinished];
    [queue release];
    xmlCleanupParser();
    [op release];
}

@interface CPoint : NSObject<NSCoding>{
    NSInteger i;
}
@property (nonatomic, assign)NSInteger i;
@end

@implementation CPoint
@synthesize i;
#pragma mark NSCoding

- (void)encodeWithCoder:(NSCoder *)coder{
    [coder encodeInteger:i forKey:@"i"];
}
- (id)initWithCoder:(NSCoder *)decoder{
    self = [super init];
    i = [decoder decodeIntegerForKey:@"i"];
    return self;
}
@end

@interface CLine : NSObject<NSCoding>{
    CPoint* point1;
    CPoint* point2;
}
@property (nonatomic,retain)CPoint* point1;
@property (nonatomic,retain)CPoint* point2;
@end

@implementation CLine

@synthesize point1, point2;

#pragma mark NSCoding

- (void)encodeWithCoder:(NSCoder *)coder{
    [coder encodeObject:point1 forKey:@"point1"];
    [coder encodeObject:point2 forKey:@"point2"];
}
- (id)initWithCoder:(NSCoder *)decoder{
    self = [super init];
    self.point1 = [decoder decodeObjectForKey:@"point1"];
    self.point2 = [decoder decodeObjectForKey:@"point2"];
    return self;
}
-(void)dealloc{
    [point1 release];
    [point2 release];
    [super dealloc];
}
@end

void testSerializer(){
    @autoreleasepool {
     CLine* line1 = [[[CLine alloc] init] autorelease];
    line1.point1 = [[[CPoint alloc] init] autorelease];
    line1.point1.i = 10;
    line1.point2 = line1.point1;
    NSData *archivedData = [NSKeyedArchiver archivedDataWithRootObject:line1];
    CLine* line2 = [NSKeyedUnarchiver unarchiveObjectWithData:archivedData];
    NSLog(@"%@, %@",line1.point1, line1.point2);
    NSLog(@"%@, %@",line2.point1, line2.point2);
    }
}

void testQuoteName(){
    int count = 1000000;
    NSTimeInterval start = [NSDate timeIntervalSinceReferenceDate];
    for (NSInteger i = 0;i<count;i++){
        [DataManager quoteName:@"CustomField1"];
    }
    NSLog(@"quoteName run time: %f",  [NSDate timeIntervalSinceReferenceDate] - start);
}

#import "RTIntegerArray.h"

 void testObjectCreation(){
    long long collectionSize = 10000000;

    NSTimeInterval start = 0;
    
//    NSMutableArray* array = [[NSMutableArray alloc]init];
//    for (long long i = 0; i < collectionSize; i++) {
//        NSNumber *number = @(i);
//        [array addObject:number];
//    }
//    [array release];
//    NSLog(@"number creation run time: %f",  [NSDate timeIntervalSinceReferenceDate] - start);
    start = [NSDate timeIntervalSinceReferenceDate];
    //NSUInteger ii;
    RTIntegerArray* arrayInt = [[RTIntegerArray alloc]init];
    for (long long i = 0; i < collectionSize; i++) {
        [arrayInt addInteger:i];
        //ii = i; 
    }
    [arrayInt release];
    NSLog(@"int creation run time: %f",  [NSDate timeIntervalSinceReferenceDate] - start);
    
    start = [NSDate timeIntervalSinceReferenceDate];
    //NSUInteger ii;
    NSIntegerArray* arrayInt2 = [[NSIntegerArray alloc]init];
    for (long long i = 0; i < collectionSize; i++) {
        [arrayInt2 addInteger:i];
        //ii = i; 
    }
    NSLog(@"int creation run time: %f",  [NSDate timeIntervalSinceReferenceDate] - start);
    int index = [arrayInt2 indexOfObject:[NSNumber numberWithInt:100]];
    NSArray* array = [arrayInt2 subarrayWithRange:NSMakeRange(200, 50)];
    if (index==[array count]){
        
    }
    [arrayInt2 release];
    
}


void testEncryption(){
    NSString* string = [RTEncryption encrypt:@"hi"];
    NSLog(@"%@", string);
    string = [RTEncryption decrypt:string];
    NSLog(@"%@", string);
}

// Add support for subscripting to the iOS 5 SDK.
#if __IPHONE_OS_VERSION_MAX_ALLOWED < 60000
@interface NSObject (PSPDFSubscriptingSupport)

- (id)objectAtIndexedSubscript:(NSUInteger)idx;
- (void)setObject:(id)obj atIndexedSubscript:(NSUInteger)idx;
- (void)setObject:(id)obj forKeyedSubscript:(id <NSCopying>)key;
- (id)objectForKeyedSubscript:(id)key;

@end
#endif

void testMembershipCode(){
    [[DataManager instance] enableDatabaseAccess];
    NSLog(@"%@",[CustomerBase generateMembershipCode]);
}

@interface CTestValue : CEntity{
    NSString* text;
}
@property (nonatomic, retain)NSString* text;
@end

@implementation CTestValue
@synthesize text;

-(void)dealloc{
    [text release];
    text = nil;
    [super dealloc];
}
//
//- (void)encodeWithCoder:(NSCoder *)aCoder{
//    [aCoder encodeObject:text forKey:@"text"];
//}
//- (id)initWithCoder:(NSCoder *)aDecoder{
//    self=[super init];
//    text = [[aDecoder decodeObjectForKey:@"text"] retain];
//    return self;
//}

@end



@interface CTest : CEntity{
    NSString* text;
    NSInteger listOrder;
    CTestValue* testValue;
}
@property (nonatomic, retain)NSString* text;
@property (nonatomic, retain)CTestValue* testValue;
@property (nonatomic, assign)NSInteger listOrder;
@end

@implementation CTest
@synthesize text, listOrder, testValue;

-(void)dealloc{
    self.testValue = nil;
    self.text = nil;
    [super dealloc];
}

@end


void testBeginEdit(){
    NSAutoreleasePool* pool = [[NSAutoreleasePool alloc]init];
    CTest* testObject = [[CTest alloc]init];
    NSMutableString* text1 = [[NSMutableString alloc]initWithString:@"hi"];
    CTestValue* testValue = [[CTestValue alloc]init];
    testValue.text = @"text";
    testObject.listOrder = 11;
    testObject.testValue = testValue;
    [testValue release];
    NSMutableString* text2 = [[NSMutableString alloc]initWithString:@"bye"];
    testObject.text = text1;
    [testObject beginEdit];
    testObject.listOrder = 20;
    testObject.text = text2;
    testObject.testValue.text = @"foo";
    [testObject cancelEdit];
    [text1 release];
    [text2 release];
    [testObject release];
    [pool release];
    return;
}

void testEditFees(){
    @autoreleasepool {
        [[DataManager instance] enableDatabaseAccess];
        Receipt* receipt = [[Receipt alloc]init];
        NSArray* feeList = [Fee getFeeList:FeeAreaSalesGlobal active:TRUE];
        if (feeList.count){
            [receipt addGlobalFee:[feeList objectAtIndex:0]];
        }
        [receipt.globalFeesList beginEdit];
        [receipt.globalFeesList endEdit];
        [receipt release];
    }
    NSLog(@"");
}

@interface NSDummy : NSObject

@property (nonatomic, retain)NSString* name;
@end

@implementation NSDummy

-(void)dealloc{
    //NSLog(@"dealloc NSDummy=%@",self);
    self.name = nil;
    [super dealloc];
}

-(id)retain{
    return [super retain];
}

@end

@interface NSDummyKey : NSObject<NSCopying>

@property (nonatomic, retain)NSString* name;

@end

@implementation NSDummyKey
-(id)init{
    if ((self=[super init])){
        //NSLog(@"created NSDummyKey=%@",self);
    }
    return self;
}
- (id)copyWithZone:(NSZone *)zone{
    NSDummyKey* c = [[[self class]alloc]init];
    c->_name = [self.name copy];
    return c;
}

-(id)retain{
    return [super retain];
}
-(void)dealloc{
    //NSLog(@"dealloc NSDummyKey=%@",self);
    self.name = nil;
    [super dealloc];
}
@end

void testFastDictionary(){

    NSMutableDictionaryFast* dict = [[NSMutableDictionaryFast alloc]init];
    NSDummy* obj1 = [[NSDummy alloc]init];
    obj1.name = @"name1";
    NSString* key = @"key1";
    [dict setObject:obj1 forKey:key];
    NSDummy* obj = [dict objectForKey:key];
    NSLog(@"%@", obj.name);
    NSDummy* obj2 = [[NSDummy alloc]init];
    obj2.name = @"name2";
    [dict setObject:obj2 forKey:key];
    obj = [dict objectForKey:key];
    NSLog(@"%@", obj.name);
    [dict removeObjectForKey:key];
    obj = [dict objectForKey:key];
    NSLog(@"%@", obj.name);
    [dict removeObjectForKey:key];
    NSLog(@"%@", obj.name);
    [key release];
    [obj1 release];
    [obj2 release];
    [dict release];
    
    NSInteger count = 100000;
    NSTimeInterval start;
    {
        NSMutableDictionary*dictionary = [[NSMutableDictionary alloc]initWithCapacity:count];
        start = [NSDate timeIntervalSinceReferenceDate];
        for (int i=0; i<count; i++){
            NSDummy*object = [[NSDummy alloc]init];
            NSString* name = [[NSString alloc] initWithFormat:@"obj%iName", i];
            object.name = name;
            [name release];
            NSString*aKey = [[NSString alloc]initWithFormat:@"key%i", i];
            [dictionary setObject:object forKey:[NSString stringWithFormat:aKey, i]];
            [aKey release];
            [object release];
            //[key1 release];
        }
        NSLog(@"Fill time using NSDictionary is: %f",  [NSDate timeIntervalSinceReferenceDate] - start);
        
        start = [NSDate timeIntervalSinceReferenceDate];
        for (int i=0; i<count; i++){
            [dictionary objectForKey:[NSString stringWithFormat:@"key%i", i]];
        }
        NSLog(@"Access time using NSDictionary is: %f",  [NSDate timeIntervalSinceReferenceDate] - start);

        start = [NSDate timeIntervalSinceReferenceDate];
        [dictionary enumerateKeysAndObjectsUsingBlock:^(id key, id o, BOOL *stop) {
            [o retain];
            [o release];
        }];
        NSLog(@"Enumerate using NSDictionary is: %f",  [NSDate timeIntervalSinceReferenceDate] - start);
        
        [dictionary release];
    }
    
    {
        NSMutableDictionaryFast* dict = [[NSMutableDictionaryFast alloc]initWithCapacity:count];
        start = [NSDate timeIntervalSinceReferenceDate];
        for (int i=0; i<count; i++){
            NSDummy*dummyObj = [[NSDummy alloc]init];
            NSString* name = [[NSString alloc] initWithFormat:@"obj%iName", i];
            dummyObj.name = name;
            [name release];
            //NSDummyKey* key1 = [[NSDummyKey alloc]init];
            //key1.name = [NSString stringWithFormat:@"key%i", i];
            //[dict setObject:dummyObj forKey:key1];
            NSString*aKey = [[NSString alloc]initWithFormat:@"key%i", i];
            [dict setObject:dummyObj forKey:aKey];
            [aKey release];
            [dummyObj release];
            //[key1 release];
        }
        NSLog(@"Fill time using hashtable is: %f",  [NSDate timeIntervalSinceReferenceDate] - start);
        start = [NSDate timeIntervalSinceReferenceDate];
        for (int i=0; i<count; i++){
            [dict objectForKey:[NSString stringWithFormat:@"key%i", i]];
        }
        NSLog(@"Access time using hashtable is: %f",  [NSDate timeIntervalSinceReferenceDate] - start);
        
        start = [NSDate timeIntervalSinceReferenceDate];
        [dict enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
            [obj retain];
            [obj release];
        }];
        NSLog(@"Enumerate using hashtable is: %f",  [NSDate timeIntervalSinceReferenceDate] - start);
        
        [dict release];
    }
    
    {
        NSMutableArray* arr = [[NSMutableArray alloc]initWithCapacity:count];
        start = [NSDate timeIntervalSinceReferenceDate];
        for (int i=0; i<count; i++){
            NSDummy*anObj1 = [[NSDummy alloc]init];
            NSString* name = [[NSString alloc] initWithFormat:@"obj%iName", i];
            anObj1.name = name;
            [name release];
            [arr addObject:anObj1];
            [anObj1 release];
        }
        NSLog(@"Fill time using NSArray is: %f",  [NSDate timeIntervalSinceReferenceDate] - start);
        start = [NSDate timeIntervalSinceReferenceDate];
        for (int i=0; i<count; i++){
            [arr objectAtIndex:i];
        }
        NSLog(@"Access time using NSArray is: %f",  [NSDate timeIntervalSinceReferenceDate] - start);
        
        start = [NSDate timeIntervalSinceReferenceDate];
        for (id arr1 in arr) {
            [arr1 retain];
            [arr1 release];
        }
        NSLog(@"Enumerate using NSArray is: %f",  [NSDate timeIntervalSinceReferenceDate] - start);
        
        [arr release];
    }
}


void textXMLEncoding(){
    
    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
    
    xmlDocPtr doc = xmlNewDoc((const xmlChar*)XML_DEFAULT_VERSION);
	xmlNodePtr root = xmlNewDocNode(doc, NULL, (const xmlChar*)"Envelope", NULL);
	xmlDocSetRootElement(doc, root);
	xmlNodePtr bodyNode = xmlNewDocNode(doc, nil, (const xmlChar*)"name", NULL);
    xmlAddChild(root, bodyNode);
    NSString* body = @"&amp;amp;";
    //NSString* body = @"<hi></hi>";
    xmlNodePtr bodyNodeContent =  [body xmlNodeForDoc:doc elementName:@"key" elementNSPrefix:nil];
	xmlAddChild(bodyNode, bodyNodeContent);
    
    NSString* s = [NSString deserializeNode:bodyNodeContent];
    NSLog(@"%@", s);
    
	xmlChar *buf;
	int size;
	xmlDocDumpFormatMemory(doc, &buf, &size, 1);
	
	NSString *serializedForm = [NSString stringWithCString:(const char*)buf encoding:NSUTF8StringEncoding];
	xmlFree(buf);
	
	xmlFreeDoc(doc);
    NSLog(@"%@", serializedForm);
    [pool release];
}

void testProxyStream(){
    dispatch_queue_t writer = dispatch_queue_create("writer", nil);
    dispatch_queue_t reader = dispatch_queue_create("reader", nil);
    RTProxyStream* stream = [[RTProxyStream alloc]init];
    dispatch_async(writer, ^{
        for (int i =0; i < 1000; i++){
            NSData* str =[[NSString stringWithFormat:@"%@_",[[NSNumber numberWithInt:i]stringValue]] dataUsingEncoding:NSUTF8StringEncoding];
            [stream append:str];
        }
        [stream close];
    });
    
    dispatch_async(reader, ^{
        NSMutableData* mdata = [[NSMutableData dataWithLength:10] retain];
        NSMutableData* resultData = [[NSMutableData data] retain];
        int len = 0;
        [stream open];
        while((len = [stream read:mdata.mutableBytes maxLength:mdata.length])){
            [resultData appendBytes:mdata.bytes length:len];
        }
        [stream close];
        NSString* resultString = [[NSString alloc]initWithData:resultData encoding:NSUTF8StringEncoding];
        NSLog(@"%@", resultString);
        [resultString release];
        [mdata release];
        [resultData release];
    });
    dispatch_sync(writer, ^{});
    dispatch_sync(reader, ^{});
    [stream release];
}


@interface NSThreadRunner: NSObject
-(void)runRequests;
@end

@implementation NSThreadRunner

-(void)runRequests{
    [[DataManager instance] enableDatabaseAccess];
    [[[DataManager instance] currentDatabase] beginTransaction];
    [[[DataManager instance] currentDatabase] executeQuery:@"select 1"];
}

@end


void testEGODbTLS(){
//    dispatch_queue_t dbQueue = dispatch_queue_create(nil, nil);
//    dispatch_async(dbQueue, ^{
//        [[DataManager instance] enableDatabaseAccess];
//        [[[DataManager instance] currentDatabase] beginTransaction];
//        [[[DataManager instance] currentDatabase] executeQuery:@"select 1"];
//    });
//    dispatch_sync(dbQueue, ^{});
//    dispatch_release(dbQueue);
    
    NSThreadRunner* t = [[NSThreadRunner alloc]init];
    NSThread* networkThread = [[NSThread alloc] initWithTarget:t selector:@selector(runRequests) object:nil];
    [networkThread start];
    [NSThread sleepForTimeInterval:200];
    [networkThread release];
    [t release];
}

void reverseStr(char *p)
{
    char *q = p;
    while(q && *q) ++q;
    for(--q; p < q; ++p, --q)
        *p = *p ^ *q,
        *q = *p ^ *q,
        *p = *p ^ *q;
}

unsigned int double_dabble(unsigned short n, unsigned short *mantissa, char **result){
    
    int maxLen = (16*n)/3;
    int len = 0;
    char *digits = calloc(1 + maxLen, sizeof(char));
    unsigned short *a = malloc(n * sizeof(unsigned short));
    memcpy(a, mantissa, n * sizeof *mantissa);
    
//        do
//        {
//            unsigned int d, r;
//            BOOL stop = TRUE;
//    
//            r = a [0];
//            for (NSInteger i = 0; i < n; i++){
//                d = r / 10;
//                if (i==n-1){
//                    r = r - d * 10;
//                    a [i] = d;
//                }
//                else{
//                    r = ((r - d * 10) << 16) + a [i + 1];
//                    a [i] = d;
//                }
//                if (d){
//                    stop = FALSE;
//                }
//            }
//            
//
//            digits[len++] = '0' + r;
//            if (stop) break;
//            
//        }
//        while (TRUE);

    
    BOOL done = FALSE;
    unsigned int d = 0;
    unsigned int carry = 0;
//    const unsigned int i10 = 10;
//    const unsigned short s10 = 10;
    int j;
    int lastWorld = n - 1;
    while (!done){
        done = 1;
        {
            carry = 0;
            for (j=lastWorld; j>=0; j--)
            {
                d = (carry << 16) + a[j];
                a[j] = d / 10;
                carry = d - a[j] * 10;
                if (a[j]) done = 0;
            }
            digits[len++] = '0' + carry;
        }
    }
    
    free(a);
    //*result = realloc(digits, len+1);
    *result = digits;
    reverseStr(*result);
    return len;

}

char* NSDecimalUTF8String(const NSDecimal* number)
{
    if (!number) return NULL;
	if (NSDecimalIsNotANumber(number)){
        char* string = malloc(4);
        memcpy(string, "NaN", 4);
		return string;
    }
	if (!number->_length)    {
        char* string = malloc(2);
        memcpy(string, "0", 2);
		return string;
    }
    
    char* digits = NULL;

    unsigned int numOfDigits = double_dabble((unsigned short)number->_length, number->_mantissa, &digits);
    
    int l = ((number->_exponent < 0) ? number->_exponent : 0) + numOfDigits;
    uint totalLength = (number->_isNegative ? 1 : 0) + (l <= 0 ? (2 + ABS(l)) : (l < numOfDigits ? 1 : 0))  + numOfDigits  + (number->_exponent > 0 ? number->_exponent  : 0) + 1;
    char* string = calloc(totalLength, sizeof(char));
    char* stringResult = string;
    if (number->_isNegative)
        *string++ = '-';
    
    if (l < 0)
    {
        // add leading zeros
        *string++ = '0';
        *string++ = '.';
        unsigned int labs = ABS(l);
        memset(string, '0', labs);
        string+=labs;
        l = numOfDigits;
    }
    else if (l == 0){
        memset(string++, '0', 1);
    }
    
    if (l > 0){
        memcpy(string, digits, l);
        string+=l;
    }
    
    if (l < numOfDigits){
        *string++ = '.';
        memcpy(string, digits + l, numOfDigits);
        string+=numOfDigits;
    }
    if (number->_exponent > 0){
        memset(string, '0', number->_exponent);
    }
    free(digits);
    return stringResult;
}


void testDecimalString(){
    NSDecimal dec;
    for (NSInteger i = 0; i < 8; i++){
        dec._mantissa[i] = 65535;
    }
    dec._exponent = 0;
    dec._length = 8;
    //NSDecimalNumber* decimalNumber = [NSDecimalNumber decimalNumberWithString:@"-0.0012844099005569976"];
    
//    NSDecimalNumber* decimalNumber = [NSDecimalNumber decimalNumberWithDecimal:dec];
//    NSString* s = [decimalNumber stringValue];
//    NSDecimal d = [decimalNumber decimalValue];
//    char* s2 = NSDecimalUTF8String(&d);
//    NSLog(@"\n%@\n%s", s ,s2);
//    free(s2);
    
    
    //const char* utf8String1;
    char* utf8String2 = nil;
    NSTimeInterval start = [NSDate timeIntervalSinceReferenceDate];
    for (NSInteger i = 0; i < 1000000; i ++){
        @autoreleasepool {
            //NSDecimalNumber* decimalNumber = [NSDecimalNumber decimalNumberWithString:@"-0.0012844099005569976"];
            NSDecimalNumber* decimalNumber = [NSDecimalNumber decimalNumberWithDecimal:dec];
            [decimalNumber description];
            //utf8String1 = [[decimalNumber stringValue] UTF8String];
        }
    }
    NSLog(@"stringValue.UTF8String: %f",  [NSDate timeIntervalSinceReferenceDate] - start);
    [NSThread sleepForTimeInterval:1];
    start = [NSDate timeIntervalSinceReferenceDate];
    for (NSInteger i = 0; i < 1000000; i ++){
        @autoreleasepool {
            NSDecimalNumber* decimalNumber = [NSDecimalNumber decimalNumberWithDecimal:dec];
            NSDecimal d = [decimalNumber decimalValue];
            utf8String2 = NSDecimalUTF8String(&d);
            free(utf8String2);
        }
    }
    NSLog(@"NSDecimalUTF8String: %f",  [NSDate timeIntervalSinceReferenceDate] - start);
    
}

@interface CDummyObj : CEntity
@property (nonatomic, assign) NSDecimal d;
@end

@implementation CDummyObj

@end

void testSaveRestoreObject(){
    CDummyObj* o = [[CDummyObj alloc]init];
    o.d = D0;
    [o beginEdit];
    o.d = CPDecimalFromDouble(0.4);
    [o cancelEdit];
    [o release];
}


void testUploadReceipts(){
    [[DataManager instance]enableDatabaseAccess];
    EGODatabaseResult* result = [[DataManager instance].currentDatabase executeQuery:@"select * from Receipt where SyncState is not null limit 1"];
    if( !result.rows.count) return;
    Receipt* _receipt = [[[Receipt alloc]initWithRow:[result.rows objectAtIndex:0]]autorelease];
    UploadReceiptsOperation * operation = [UploadReceiptsOperation uploadReceiptsOperationWithReceipt:_receipt];
    SyncOperationQueue * queue = [[[SyncOperationQueue alloc] init] autorelease];
    [queue addOperations:[NSArray arrayWithObject:operation] waitUntilFinished:NO breakOnError:YES];
    [NSThread sleepForTimeInterval:2];
    [[SyncOperationQueue sharedInstance] startReceiptSync];
    
    UploadReceiptsOperation * operation2 = [UploadReceiptsOperation uploadReceiptsOperationWithReceipt:_receipt];
    [queue addOperations:[NSArray arrayWithObject:operation2] waitUntilFinished:YES breakOnError:YES];
}

void testCurrencyRounding(){
    Currency* currency = [[[Currency alloc]init]autorelease];
    currency.amountRoundingPrecision = CPDecimalFromFloat(0.05);
    currency.displayDecimalPrecision = 2;
    currency.roundingType = RoundingTypeNearest;
    NSDecimal value = CPDecimalFromFloat(100.32);
    NSDecimal valueRounded = [currency roundAmount:value];
    NSLog(@"new=%@", [NSDecimalNumber decimalNumberWithDecimal:valueRounded]);
}

void testBackgroundOperationViewController(){
    BackgroundOperationViewController* backgroundOperationViewController = [[BackgroundOperationViewController alloc]init];
    backgroundOperationViewController.title = @"Test 1";
    [backgroundOperationViewController startTask:^{
        [NSThread sleepForTimeInterval:0.5f];
        int i = 100;
        while (i>0){
            dispatch_async(dispatch_get_main_queue(), ^{
                [backgroundOperationViewController hide];
            });
            u_int32_t interval = arc4random_uniform(35);
            CGFloat intervalFloat = interval/100.0;
            [NSThread sleepForTimeInterval:intervalFloat];
            dispatch_async(dispatch_get_main_queue(), ^{
                backgroundOperationViewController.title = [NSString stringWithFormat:@"iteration=%@, interval = %@",  [NSNumber numberWithFloat:i], [NSNumber numberWithFloat:intervalFloat]];
                [backgroundOperationViewController show];
            });
            interval = arc4random_uniform(35);
            intervalFloat = interval/100.0;
            [NSThread sleepForTimeInterval:intervalFloat];
            i--;
        }
        
    } completed:^{
        [ModalAlert show:@"Completed"];
    }];
    [backgroundOperationViewController release];
}

int main(int argc, char *argv[])
{
    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
    
    if(getenv("NSZombieEnabled"))
        NSLog(@"NSZombieEnabled enabled!");
    if(getenv("NSAutoreleaseFreedObjectCheckEnabled"))
        NSLog(@"NSAutoreleaseFreedObjectCheckEnabled enabled!");
    
    //NSArray* a = @[@"A",@"B",@"C"];
    //NSLog(@"Obj is %@", a[0]);
    
    //testSAX();
    //testProxyStream();
    //testFastDictionary();
    //textXMLEncoding();
    
    //testDecimalString();
    //testUploadReceipts();
    testCurrencyRounding();
    [pool release];
    return 0;
}
