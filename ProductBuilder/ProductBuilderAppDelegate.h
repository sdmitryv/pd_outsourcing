//
//  ProductBuilderAppDelegate.h
//  ProductBuilder
//
//  Created by Lulakov Viacheslav on 6/22/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
@class SuperViewController50;

@interface ProductBuilderAppDelegate : NSObject <UIApplicationDelegate> {
}

@property (nonatomic, retain) IBOutlet UIWindow *window;
@property (nonatomic, retain) IBOutlet SuperViewController50 * superViewController;

@end

