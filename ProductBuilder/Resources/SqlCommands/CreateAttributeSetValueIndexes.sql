INSERT INTO AttributeSetValueFTS(AttributeSetValueFTS) VALUES('rebuild');

DROP TRIGGER IF EXISTS AttributeSetValue_bu;
CREATE TRIGGER AttributeSetValue_bu BEFORE UPDATE ON AttributeSetValue BEGIN
DELETE FROM AttributeSetValueFTS WHERE docid = old.rowid;
END;

DROP TRIGGER IF EXISTS AttributeSetValue_bd;
CREATE TRIGGER AttributeSetValue_bd BEFORE DELETE ON AttributeSetValue BEGIN
DELETE FROM AttributeSetValueFTS WHERE docid = old.rowid;
END;

DROP TRIGGER IF EXISTS AttributeSetValue_au;
CREATE TRIGGER AttributeSetValue_au AFTER UPDATE ON AttributeSetValue BEGIN
INSERT INTO AttributeSetValueFTS(docid, Value) VALUES (new.rowid, new.Value);
END;

DROP TRIGGER IF EXISTS AttributeSetValue_ai;
CREATE TRIGGER AttributeSetValue_ai AFTER INSERT ON AttributeSetValue BEGIN
INSERT INTO AttributeSetValueFTS(docid, Value) VALUES (new.rowid, new.Value);
END;
