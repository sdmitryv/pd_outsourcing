INSERT INTO CustomerFTS(CustomerFTS) VALUES('rebuild');

DELETE FROM CustomerFTSInvert;
INSERT INTO CustomerFTSInvert(docid,Phone1,Phone2) SELECT rowid, strinvert(Phone1), strinvert(Phone2) from Customer;

DROP TRIGGER IF EXISTS Customer_bu;
CREATE TRIGGER Customer_bu BEFORE UPDATE ON Customer BEGIN
DELETE FROM CustomerFTS WHERE docid = old.rowid;
DELETE FROM CustomerFTSInvert WHERE docid = old.rowid;
END;

DROP TRIGGER IF EXISTS Customer_bd;
CREATE TRIGGER Customer_bd BEFORE DELETE ON Customer BEGIN
DELETE FROM CustomerFTS WHERE docid = old.rowid;
DELETE FROM CustomerFTSInvert WHERE docid = old.rowid;
END;

DROP TRIGGER IF EXISTS Customer_au;
CREATE TRIGGER Customer_au AFTER UPDATE ON Customer BEGIN
INSERT INTO CustomerFTS(docid, FirstName,LastName,Organization,EMail1,Phone1Digits,Phone2Digits,Phone1,Phone2,MembershipCode) VALUES(new.rowid, new.FirstName, new.LastName, new.Organization,new.EMail1,new.Phone1Digits,new.Phone2Digits,new.Phone1,new.Phone2,new.MembershipCode);
INSERT INTO CustomerFTSInvert(docid,Phone1,Phone2) VALUES (new.rowid, strinvert(new.Phone1), strinvert(new.Phone2));
END;

DROP TRIGGER IF EXISTS Customer_ai;
CREATE TRIGGER Customer_ai AFTER INSERT ON Customer BEGIN
INSERT INTO CustomerFTS(docid, FirstName,LastName,Organization,EMail1,Phone1Digits,Phone2Digits,Phone1,Phone2,MembershipCode) VALUES(new.rowid, new.FirstName, new.LastName, new.Organization,new.EMail1,new.Phone1Digits,new.Phone2Digits,new.Phone1,new.Phone2,new.MembershipCode);
INSERT INTO CustomerFTSInvert(docid,Phone1,Phone2) VALUES (new.rowid, strinvert(new.Phone1), strinvert(new.Phone2));
END;

