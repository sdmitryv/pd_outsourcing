//
//  NSDecimal+Operators.m
//  NSDecimalOperators
//
//  Created by Markus Gasser on 26.11.13.
//  Copyright (c) 2013 konoma GmbH. All rights reserved.
//
//  Modified by Valery Fomenko
//  Copyright (c) 2014 Cloudworks. All rights reserved.


#import "NSDecimal+Operators.h"
#import "DecimalHelper.h"

NSDecimal operator + (const NSDecimal& lhs, const NSDecimal& rhs) {
    NSDecimal result;
    if (NSDecimalAdd(&result, &lhs, &rhs, NSRoundBankers) > NSCalculationLossOfPrecision) {
        result = D0;
    }
    return result;
}

NSDecimal operator + (const NSDecimal& lhs, const NSInteger& rhs) {
    return lhs + @(rhs).decimalValue;
}

NSDecimal& operator += (NSDecimal& lhs, const NSDecimal& rhs){
    if (NSDecimalAdd(&lhs, &lhs, &rhs, NSRoundBankers) > NSCalculationLossOfPrecision) {
        lhs = D0;
    }
    return lhs;
}

NSDecimal& operator += (NSDecimal& lhs, const NSInteger& rhs){
    lhs = lhs + @(rhs).decimalValue;
    return lhs;
}

NSDecimal& operator ++ (NSDecimal& lhs){
    if (NSDecimalAdd(&lhs, &lhs, &D1, NSRoundBankers)> NSCalculationLossOfPrecision){
        lhs = D0;
    }
    return lhs;
}

NSDecimal operator ++ (NSDecimal& lhs, int){
    NSDecimal tmp(lhs);
    ++lhs;
    return tmp;
}

NSDecimal& operator -- (NSDecimal& lhs){
    if (NSDecimalSubtract(&lhs, &lhs, &D1, NSRoundBankers)> NSCalculationLossOfPrecision){
        lhs = D0;
    }
    return lhs;
}

NSDecimal operator -- (NSDecimal& lhs, int){
    NSDecimal tmp(lhs);
    ++lhs;
    return tmp;
}

NSDecimal operator-(const NSDecimal& lhs) {
    NSDecimal tmp(lhs);
    if (NSDecimalCompare(&tmp, &D0) != NSOrderedSame){
        tmp._isNegative = !tmp._isNegative;
    }
    return tmp;
}

NSDecimal operator - (const NSDecimal& lhs, const NSDecimal& rhs) {
    NSDecimal result;
    if (NSDecimalSubtract(&result, &lhs, &rhs, NSRoundBankers) > NSCalculationLossOfPrecision) {
        result = D0;
    }
    return result;
}

NSDecimal& operator -= (NSDecimal& lhs, const NSDecimal& rhs) {
    if (NSDecimalSubtract(&lhs, &lhs, &rhs, NSRoundBankers) > NSCalculationLossOfPrecision) {
        lhs = D0;
    }
    return lhs;
}

NSDecimal& operator -= (NSDecimal& lhs, const NSInteger& rhs) {
    lhs = lhs - @(rhs).decimalValue;
    return lhs;
}

NSDecimal operator * (const NSDecimal& lhs, const NSDecimal& rhs) {
    NSDecimal result;
    if (NSDecimalMultiply(&result, &lhs, &rhs, NSRoundBankers)> NSCalculationLossOfPrecision){
        return D0;
    }
    if (NSDecimalIsNotANumber(&result))
        result = D0;
    return result;
}

NSDecimal operator * (const NSDecimal& lhs, const NSInteger& rhs) {
    return lhs * @(rhs).decimalValue;
}

NSDecimal operator / (const NSDecimal& lhs, const NSDecimal& rhs) {
    NSDecimal result;
    if (NSDecimalDivide(&result, &lhs, &rhs, NSRoundBankers)> NSCalculationLossOfPrecision){
        return D0;
    }
    if (NSDecimalIsNotANumber(&result))
        result = D0;
    return result;
}

NSDecimal operator / (const NSDecimal& lhs, const NSInteger& rhs) {
    return lhs / @(rhs).decimalValue;
}

BOOL operator == (const NSDecimal& lhs, const NSDecimal& rhs) {
    return (NSDecimalCompare(&lhs, &rhs) == NSOrderedSame);
}

BOOL operator != (const NSDecimal& lhs, const NSDecimal& rhs) {
    return (NSDecimalCompare(&lhs, &rhs) != NSOrderedSame);
}

BOOL operator < (const NSDecimal& lhs, const NSDecimal& rhs) {
    return (NSDecimalCompare(&lhs, &rhs) == NSOrderedAscending);
}

BOOL operator > (const NSDecimal& lhs, const NSDecimal& rhs) {
    return (NSDecimalCompare(&lhs, &rhs) == NSOrderedDescending);
}

BOOL operator <= (const NSDecimal& lhs, const NSDecimal& rhs) {
    NSComparisonResult result = NSDecimalCompare(&lhs, &rhs);
    return (result == NSOrderedSame || result == NSOrderedAscending);
}

BOOL operator >= (const NSDecimal& lhs, const NSDecimal& rhs) {
    NSComparisonResult result = NSDecimalCompare(&lhs, &rhs);
    return (result == NSOrderedSame || result == NSOrderedDescending);
}

NSDecimal operator"" DECIMAL_SUFFIX (const char *value) {
    NSDecimal result;
    if (![[NSScanner scannerWithString:@(value)] scanDecimal:&result]) {
        result = D0;
    }
    return result;
}

BOOL operator == (const NSDecimal& lhs, const NSInteger& rhs){
    return lhs== (rhs ? @(rhs).decimalValue : D0);
}

BOOL operator != (const NSDecimal& lhs, const NSInteger& rhs){
    return lhs != (rhs ? @(rhs).decimalValue : D0);
}

BOOL operator < (const NSDecimal& lhs, const NSInteger& rhs){
    return lhs < (rhs ? @(rhs).decimalValue : D0);
}

BOOL operator > (const NSDecimal& lhs, const NSInteger& rhs){
    return lhs > (rhs ? @(rhs).decimalValue : D0);
}

BOOL operator <= (const NSDecimal& lhs, const NSInteger& rhs){
    return lhs <= (rhs ? @(rhs).decimalValue : D0);
}

BOOL operator >= (const NSDecimal& lhs, const NSInteger& rhs){
    return lhs >= (rhs ? @(rhs).decimalValue : D0);
}
