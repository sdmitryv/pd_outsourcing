//
//  AutoLogOutManager.m
//  ProductBuilder
//
//  Created by DSM on 8/8/13.
//  Copyright (c) 2013 Cloudworks. All rights reserved.
//

#import "AutoLogOutManager.h"

@implementation AutoLogOutManager

@synthesize isStarted;

AutoLogOutManager *instance = nil;

////////////////////////////////////////////////////////////
-(id)init {
#ifdef ShowLog
    NSLog(@" %s", __FUNCTION__);
#endif
    
    if (self = [super init]) {
        
        autoLogOut = 0;
        autoLogOutAction = LogOutActionLogOut;
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(textChanged)
                                                     name:UITextFieldTextDidChangeNotification
                                                   object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(textChanged)
                                                     name:UITextViewTextDidChangeNotification
                                                   object:nil];
    }
    
    return self;
}


////////////////////////////////////////////////////////////
+(AutoLogOutManager *)instance {
    
    if (!instance) {
#ifdef ShowLog
        NSLog(@" %s", __FUNCTION__);
#endif
        
        instance = [[AutoLogOutManager alloc] init];
    }
    
    return instance;
}


////////////////////////////////////////////////////////////
-(NSInteger)autoLogOut {
#ifdef ShowLog
    NSLog(@" %s", __FUNCTION__);
#endif
    
    return autoLogOut;
}


////////////////////////////////////////////////////////////
-(void)reset {
#ifdef ShowLog
    NSLog(@" %s", __FUNCTION__);
#endif
    
    autoLogOut = [SettingManager instance].autoLogout;
    
    autoLogOutAction = LogOutActionLogOut;
    if ([[SettingManager instance].autoLogoutAction isEqualToString:@"Main Menu"])
        autoLogOutAction = LogOutActionGoToMainMenu;
    else if ([[SettingManager instance].autoLogoutAction isEqualToString:@"Main Menu and Logout"])
        autoLogOutAction = LogOutActionGoToMainMenuWithLogOut;
}


////////////////////////////////////////////////////////////
-(void)userActivityEvent {
#ifdef ShowLog
    NSLog(@" %s", __FUNCTION__);
#endif
    
    @synchronized(self){
        
        if (!isStarted)
            return;
        
        self.eventDate = [NSDate date];
    }
}


////////////////////////////////////////////////////////////
-(void)setEventDate:(NSDate *)date {
#ifdef ShowLog
    NSLog(@" %s", __FUNCTION__);
#endif
    
    if (eventDate)
        [eventDate release];
    
    eventDate = [date retain];
}


////////////////////////////////////////////////////////////
-(void)textChanged {
#ifdef ShowLog
    NSLog(@" %s", __FUNCTION__);
#endif
    
    [self userActivityEvent];
}


////////////////////////////////////////////////////////////
-(void)timerFun {
#ifdef ShowLog
    NSLog(@" %s", __FUNCTION__);
#endif
    
    [self checkState];
    
    if (isStarted)
        [self performSelector:@selector(timerFun) withObject:nil afterDelay:2];
}


////////////////////////////////////////////////////////////
-(void)checkState{
#ifdef ShowLog
    NSLog(@" %s", __FUNCTION__);
#endif
    
    @synchronized(self){
        
        [self checkState:[NSDate date]];
    }
}


////////////////////////////////////////////////////////////
-(void)checkState:(NSDate *)currentDate{
#ifdef ShowLog
    NSLog(@" %s", __FUNCTION__);
#endif
    
    if (!isStarted || isSuspended || autoLogOut == 0)
        return;
    
    int interval = [currentDate timeIntervalSinceDate:eventDate];
    
    if (interval < autoLogOut)
        return;
    
    
    [[NSNotificationCenter defaultCenter] postNotificationName:LOG_OUT_SATE_CHANGED
                                                        object:self
                                                      userInfo:@{@"state": @(autoLogOutAction)}];
}


////////////////////////////////////////////////////////////
-(void)Start {
#ifdef ShowLog
    NSLog(@" %s", __FUNCTION__);
#endif
    
    @synchronized(self){
        
        [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(timerFun) object:nil];
        
        self.eventDate = [NSDate date];
        
        [self reset];
        isStarted = !(autoLogOut == 0);

        if (isStarted) {
            
            [[NSNotificationCenter defaultCenter] postNotificationName:LOG_OUT_START object:self userInfo:nil];
            [self performSelector:@selector(timerFun) withObject:nil afterDelay:1];
        }
    }
}


////////////////////////////////////////////////////////////
-(void)Stop {
#ifdef ShowLog
    NSLog(@" %s", __FUNCTION__);
#endif
    
    @synchronized(self){
        
        self.eventDate = nil;
    
        isStarted = NO;
        
        [[NSNotificationCenter defaultCenter] postNotificationName:LOG_OUT_STOP object:self userInfo:nil];
        [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(timerFun) object:nil];
    }
}


////////////////////////////////////////////////////////////
-(void)Suspend {
#ifdef ShowLog
    NSLog(@" %s", __FUNCTION__);
#endif
    
    @synchronized(self){
        isSuspended = YES;
    }
}


////////////////////////////////////////////////////////////
-(void)Resume {
#ifdef ShowLog
    NSLog(@" %s", __FUNCTION__);
#endif
    
    if (!isSuspended) {
        return;
    }
    @synchronized(self){
        if (isStarted) {
            [self Stop];
            [self Start];
        }

        isSuspended = NO;
    }
}


////////////////////////////////////////////////////////////
-(void)dealloc {
#ifdef ShowLog
    NSLog(@" %s", __FUNCTION__);
#endif
    
    [eventDate release];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    [super dealloc];
}

@end
