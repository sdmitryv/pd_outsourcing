//
//  UIImage+Color.h
//  ProductBuilder
//
//  Created by valery on 1/5/15.
//  Copyright (c) 2015 Cloudworks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage(Color)
+ (UIImage *)imageWithColor:(UIColor *)color;
@end
