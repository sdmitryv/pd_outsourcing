//
//  NSVectorArray.m
//  ProductBuilder
//
//  Created by Valery Fomenko on 7/12/12.
//  Copyright (c) 2012 Cloudworks. All rights reserved.
//

#import "NSIntegerArray.h"
#include <vector>
#include <algorithm>

//struct NSIntegerArrayImpl : std::vector<int> { };

@interface NSIntegerArray(){
    std::vector<NSInteger>* vector;
}
@end

@implementation NSIntegerArray

-(id)init
{
    if ((self = [super init])){
        //vector = new NSIntegerArrayImpl;
        vector = new std::vector<NSInteger>;
    }
    
    return self;
}

- (void)addInteger:(NSInteger)value{
    vector->push_back(value);
}

- (NSUInteger)indexOfInteger:(NSInteger)value{
    std::vector<NSInteger>::iterator iter = std::find(vector->begin(), vector->end(),
          value);
    NSUInteger index = std::distance(vector->begin(), iter);
    if(index == vector->size())
    {
        return NSNotFound;
    }
    return index;
}

- (NSInteger)integerAtIndex:(NSInteger) index{
    return vector->at(index);
}

- (void)setInteger:(NSInteger) integer atIndex:(NSInteger) index{
    vector->at(index)=integer;
}

-(void)removeLast{
    if (vector->size())
        vector->pop_back();
}

- (NSUInteger)count{
    return vector->size();
}


-(NSArray*)subarrayWithRange:(NSRange)range{
    NSMutableArray* a = [[NSMutableArray alloc]init];
    for(NSInteger i=range.location; i<range.location + range.length;i++){
        [a addObject:@([self integerAtIndex:i])];  
    }
    return [a autorelease];
}


- (NSNumber*)objectAtIndex:(NSUInteger)index{
    NSUInteger value = [self integerAtIndex:index];
    return @(value);
}

- (NSNumber*)objectAtIndexedSubscript:(NSUInteger)index{
    return [self objectAtIndex:index];
}

- (NSUInteger)indexOfObject:(NSNumber*)object{
    if (!object) return NSNotFound;
    return [self indexOfInteger:[object intValue]];
}

- (void)removeObject:(NSNumber*)object{
    if (!object) return;
    NSInteger value = [object integerValue];
    std::vector<NSInteger>::iterator iter = std::remove(vector->begin(), vector->end(), value);
    vector->erase(iter, vector->end());
}

- (void)removeObjectAtIndex:(NSUInteger)index{
    vector->erase(vector->begin() + index);
}

- (void)dealloc {
    delete vector;
    [super dealloc];
}

@end
