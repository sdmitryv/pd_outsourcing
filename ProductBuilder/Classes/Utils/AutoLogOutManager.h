//
//  AutoLogOutManager.h
//  ProductBuilder
//
//  Created by DSM on 8/8/13.
//  Copyright (c) 2013 Cloudworks. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SettingManager.h"

#define LOG_OUT_SATE_CHANGED        @"LogOutSateChanged"
#define LOG_OUT_START               @"LogOutStart"
#define LOG_OUT_STOP                @"LogOutStop"

typedef NS_ENUM(NSUInteger, LogOutAction) {
    LogOutActionLogOut = 0,
    LogOutActionGoToMainMenu = 1,
    LogOutActionGoToMainMenuWithLogOut = 2,
};

@interface AutoLogOutManager : NSObject {
    
    NSInteger autoLogOut;
    LogOutAction autoLogOutAction;

    NSDate *eventDate;
    BOOL isStarted;
    BOOL isSuspended;
}

+(AutoLogOutManager *)instance;
-(void)userActivityEvent;

@property(nonatomic, readonly) BOOL isStarted;

-(void)Start;
-(void)Stop;
-(void)Suspend;
-(void)Resume;


@end
