//
//  NumberpadInputViewController.h
//  ProductBuilder
//
//  Created by Lulakov Viacheslav on 1/15/13.
//  Copyright (c) 2013 Cloudworks. All rights reserved.
//

#import "NumpadKeyboardController.h"

@interface NumberpadInputViewController : NumpadKeyboardController

@end
