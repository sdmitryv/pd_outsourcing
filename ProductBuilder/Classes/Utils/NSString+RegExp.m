//
//  NSString+RegExp.m
//  iPadPOS
//
//  Created by valera on 5/12/11.
//  Copyright 2011 DBBestTechnologies. All rights reserved.
//

#import "NSString+RegExp.h"


@implementation NSString(RegExp)

-(BOOL)isMatchedByRegex:(NSString*)pattern{
    if (!pattern) return FALSE;
    BOOL result = FALSE;
    NSError* error = nil;
    NSRegularExpression* regexp = [[NSRegularExpression alloc]initWithPattern:pattern options:NSRegularExpressionCaseInsensitive error:&error];
    if (!error){
        NSArray* matches = [regexp matchesInString:self options:0 range:NSMakeRange(0, self.length)];
        if (matches.count==1){
            NSTextCheckingResult* match = matches[0];
            if (match.range.location==0 && match.range.length==self.length)
                result = TRUE;
        }
    }
    [regexp release];
    return result;
}
@end
