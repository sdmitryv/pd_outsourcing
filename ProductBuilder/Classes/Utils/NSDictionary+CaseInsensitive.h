//
//  NSDictionary+CaseInsensitive.h
//  ProductBuilder
//
//  Created by valery on 4/3/14.
//  Copyright (c) 2014 Cloudworks. All rights reserved.
//

#import <Foundation/Foundation.h>

// Utility for building case-insensitive NSDictionary objects.
@interface NSDictionary (CaseInsensitive)

/// Initializes an NSDictionary with a case-insensitive comparison function
/// for NSString keys, while non-NSString keys are treated normally.
///
/// The case for NSString keys is preserved, though duplicate keys (when
/// compared in a case-insensitive fashion) have one of their values dropped
/// arbitrarily.
///
- (id)initWithDictionaryCaseInsensitive:(NSDictionary *)dictionary;

/// Returns a newly created and autoreleased NSDictionary object as above.
+ (id)dictionaryWithDictionaryCaseInsensitive:(NSDictionary *)dictionary;

@end
