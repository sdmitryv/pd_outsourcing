//
//  NSDate+System.h
//  ProductBuilder
//
//  Created by Roman on 3/13/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDate (System)

-(NSDate*)toTimeZone:(NSTimeZone*)timeZone;
-(NSDate*)toSystemTimeZone;
+(NSDate*)systemDate;
-(NSDate*)toUTCTimeZone;

@end
