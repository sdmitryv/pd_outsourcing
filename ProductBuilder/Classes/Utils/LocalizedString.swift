//
//  LocalizedString.swift
//  ProductBuilder
//
//  Created by valery on 2/9/17.
//  Copyright © 2017 Cloudworks. All rights reserved.
//

import Foundation

func L(_ key: String, comment: String = "") -> String {
    return NSLocalizedString(key, comment: comment)
}
