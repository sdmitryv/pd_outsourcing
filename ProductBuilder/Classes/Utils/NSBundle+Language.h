//
//  NSBundle+Language.h
//  ProductBuilder
//
//  Created by Olga Ivchenko on 6/9/17.
//  Copyright © 2017 Cloudworks. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSBundle (Language)

+ (void)setLanguage:(NSString *)language;

@end
