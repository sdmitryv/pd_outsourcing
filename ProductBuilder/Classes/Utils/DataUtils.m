//
//  DataUtils.m
//  ProductBuilder
//
//  Created by Lulakov Viacheslav on 10/5/10.
//  Copyright 2010 DBBestTechnologies. All rights reserved.
//

#import "DataUtils.h"
#import "NSDate+ISO8601Parsing.h"
#import "SettingManager.h"
#import "NBPhoneNumberUtil.h"
#import "Location.h"

static NSDateFormatter *_dateFormatter = nil;
static NSDateFormatter *_dateFormatterLocalized = nil;

@implementation DataUtils

+(dispatch_queue_t)sync_queue {

    static dispatch_once_t pred;
    static dispatch_queue_t sync_queue = nil;

    dispatch_once(&pred, ^{
        sync_queue = dispatch_queue_create("com.cloudwk.dataformatter", DISPATCH_QUEUE_CONCURRENT);
    });
    return sync_queue;
}

+(dispatch_queue_t)sync_queue_localized {
    
    static dispatch_once_t pred;
    static dispatch_queue_t sync_queue = nil;
    
    dispatch_once(&pred, ^{
        sync_queue = dispatch_queue_create("com.cloudwk.dataformatterLocalized", DISPATCH_QUEUE_CONCURRENT);
    });
    return sync_queue;
}

+(void)resetFormatters{
    if (_dateFormatter){
        dispatch_barrier_async(self.sync_queue, ^{
            if (_dateFormatter){
                [_dateFormatter release];
                _dateFormatter = nil;
            }
        });
    }
    if (_dateFormatterLocalized){
        dispatch_barrier_async(self.sync_queue_localized, ^{
            if (_dateFormatterLocalized){
                [_dateFormatterLocalized release];
                _dateFormatterLocalized = nil;
            }
        });
    }
}
+(NSString*)stringFromDate:(NSDate*)date handleBlock:(void (^)(NSDateFormatter* dateFormatter))block{
    if (!date) return nil;
    __block NSString* value = nil;
    dispatch_sync(self.sync_queue, ^{
        if (!_dateFormatter) {
            _dateFormatter = [[NSDateFormatter alloc] init];
            [_dateFormatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
            [_dateFormatterLocalized setLocale:[NSLocale currentLocale]];
        }
        if (block){
            block(_dateFormatter);
        }
        value = [[_dateFormatter stringFromDate:date] retain];
    });
	return [value autorelease];
}

+(NSString*)localizedStringFromDate:(NSDate*)date handleBlock:(void (^)(NSDateFormatter* dateFormatter))block{
    __block NSString* value = nil;
        dispatch_sync(self.sync_queue_localized, ^{
            if (!_dateFormatterLocalized) {
                _dateFormatterLocalized = [[NSDateFormatter alloc] init];
                [_dateFormatterLocalized setTimeZone:[NSTimeZone localTimeZone]];
                [_dateFormatterLocalized setLocale:[NSLocale currentLocale]];
            }
            if (block){
                block(_dateFormatterLocalized);
            }
            value = [[_dateFormatterLocalized stringFromDate:date] retain];
        });
	return [value autorelease];
}

+(NSString *)stringFromDate:(NSDate *)date {
    return [DataUtils stringFromDate:date handleBlock:^(NSDateFormatter *dateFormatter) {
        [dateFormatter setDateFormat:@"yyyyMMdd HH:mm:ss"];
    }];
}

+(NSString *)readableDateTimeStringFromDate:(NSDate *)date {
    return [self readableDateTimeStringFromDate:date format:nil];
}

+(NSString *)readableDateTimeStringFromDate:(NSDate *)date format:(NSString*)format{
    return [DataUtils stringFromDate:date handleBlock:^(NSDateFormatter *dateFormatter) {
        [dateFormatter setDateFormat:format ? format : @"MM/dd/yyyy HH:mm:ss"];
    }];
}

+(NSString *)readableUTCDateTimeStringFromDate:(NSDate *)date {
    return [self readableUTCDateTimeStringFromDate:date format:@"MM/dd/yyyy HH:mm:ss"];
}

+(NSString *)readableUTCDateTimeStringFromDate:(NSDate *)date format:(NSString*)format{
    return [DataUtils localizedStringFromDate:date handleBlock:^(NSDateFormatter *dateFormatter) {
        [dateFormatter setDateFormat:format ? format : @"MM/dd/yyyy HH:mm:ss"];
    }];
}

+(NSString *)readableUTCDateTimeStringFromDate:(NSDate *)date dateFormatterStyle:(NSDateFormatterStyle)dateFormatterStyle timeFormatterStyle:(NSDateFormatterStyle)timeFormatterStyle{
    return [DataUtils localizedStringFromDate:date handleBlock:^(NSDateFormatter *dateFormatter) {
        [dateFormatter setDateStyle:dateFormatterStyle];
        [dateFormatter setTimeStyle:timeFormatterStyle];
    }];

}


+(NSString *)readableDateStringFromDate:(NSDate *)date {
    return [[self class]readableDateStringFromDate:date formatterStyle:NSDateFormatterShortStyle];
}

+(NSString *)readableDateStringFromDate:(NSDate *)date format:(NSString*)format {
    return [DataUtils stringFromDate:date handleBlock:^(NSDateFormatter *dateFormatter) {
        [dateFormatter setDateFormat:format ? format : @"MM/dd/yyyy"];
    }];
}

+(NSString *)readableDateStringFromDate:(NSDate *)date formatterStyle:(NSDateFormatterStyle)formatterStyle {
    return [DataUtils stringFromDate:date handleBlock:^(NSDateFormatter *dateFormatter) {
        [dateFormatter setDateStyle:formatterStyle];
        [dateFormatter setTimeStyle:NSDateFormatterNoStyle];
    }];
}

+(NSString *)readableDateTimeStringFromDate:(NSDate *)date dateFormatterStyle:(NSDateFormatterStyle)dateFormatterStyle timeFormatterStyle:(NSDateFormatterStyle)timeFormatterStyle {
    return [DataUtils stringFromDate:date handleBlock:^(NSDateFormatter *dateFormatter) {
        [dateFormatter setDateStyle:dateFormatterStyle];
        [dateFormatter setTimeStyle:timeFormatterStyle];
    }];
}


+(NSDate *)dateFromString:(NSString *)string {
	if (string == nil) {
		return nil;
	}
    return [NSDate dateWithISO8601String:string];
}

+(NSDate *)dateWithYear:(NSInteger)year month:(NSInteger)month day:(NSInteger)day {
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *components = [[[NSDateComponents alloc] init] autorelease];
    calendar.timeZone = components.timeZone = [NSTimeZone timeZoneForSecondsFromGMT:0];
    [components setYear:year];
    [components setMonth:month];
    [components setDay:day];
    return [calendar dateFromComponents:components];
}

+(NSString *)fileNameFromDate:(NSDate *)date {
    return [DataUtils stringFromDate:date handleBlock:^(NSDateFormatter *dateFormatter) {
        [dateFormatter setDateFormat:@"yyyyMMdd-HHmmssFFFFFFF"];
    }];
}

+(NSString *)fileNameFromCurrentDate {
	return [DataUtils fileNameFromDate:[NSDate date]];
}

+(NSString *)stringFromBool:(BOOL)data {
	return data ? @"True" : @"False";
}

+(BOOL)BoolFromString:(NSString *)string {
	if (string == nil) {
		return NO;
	}
	return [[string uppercaseString] isEqual:@"TRUE"];
}

static int indexes[16] = {3, 2, 1, 0, 5, 4, 7, 6, 8, 9, 10, 11, 12, 13, 14, 15};
+(BPUUID *)CodeToGuid:(NSString *)code {
	UInt8 bytes[16];
	const char *str = [code UTF8String];
	for (NSInteger i = 0; i < 16; i++) {
		int index = indexes[i];
		if (i < [code length]) {
			bytes[index] = str[i];
		}
		else {
			bytes[index] = i * 2;
		}
	}
	
	return [BPUUID UUIDWithBytes:[NSData dataWithBytes:bytes length:16]];
}

+(NSString *)stringFromXmlString:(NSString *)string {
	if (string == nil) {
		return nil;
	}
	NSString *strWithoutPlus = [string stringByReplacingOccurrencesOfString:@"+" withString:@" "];
    NSString *result = [strWithoutPlus stringByRemovingPercentEncoding];
	return result;
}

+(NSNumber *)intFromString:(NSString *)string {
	if (string == nil) {
		return nil;
	}
	return @([string intValue]);
}

+(BOOL)isValidEmail:(NSString *)string {
    if (!string.length) return FALSE;
    NSString *emailRegEx =
    @"(?:[a-z0-9!#$%\\&'*+/=?\\^_`{|}~-]+(?:\\.[a-z0-9!#$%\\&'*+/=?\\^_`{|}"
    @"~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\"
    @"x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-"
    @"z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5"
    @"]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-"
    @"9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21"
    @"-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegEx]; 
    return [emailTest evaluateWithObject:[string lowercaseString]];
}

+(BOOL)validateEmail:(NSString *)string error:(NSError **)error {
    NSString * message = nil;
    NSRange range = [string rangeOfCharacterFromSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    if (range.location != NSNotFound) {
        message = NSLocalizedString(@"CUSTOMER_EMAIL_CONTAINS_SPICE_ERROR", nil);
    }
    else if (![string containsString:@"@"]) {
        message = NSLocalizedString(@"CUSTOMER_EMAIL_ABSENT_AT_ERROR", nil);
    }
    else if ([string hasPrefix:@"@"]) {
        message = NSLocalizedString(@"CUSTOMER_EMAIL_ABSENT_NAME_ERROR", nil);
    }
    else {
        message = NSLocalizedString(@"CUSTOMER_EMAIL_INVALIDE_EXT_ERROR", nil);
        NSArray * words = [string componentsSeparatedByString:@"."];
        if (words.count > 0) {
            NSString * extention = words.lastObject;
            NSInteger length = extention.length;
            if (length >= 2 && length <= 64 && NSEqualRanges([extention rangeOfCharacterFromSet:[NSCharacterSet alphanumericCharacterSet]], NSMakeRange(0, length - 1))) {
                message = nil;
            }
        }
    }
    if (message != nil) {
        if (error != nil) {
            NSDictionary *userInfo = @{NSLocalizedDescriptionKey:message};
            (*error) = [NSError errorWithDomain:@"email.validation" code:0 userInfo:userInfo];
        }
        return NO;
    }
    return YES;
}

+(BOOL)validateAndFormatPhone:(UITextField*)textField{
    return YES;
//    if ( textField.text.length>0) {
//        
//        //if validation not needed
//        if ([[SettingManager instance] phoneFormate]== PhoneNumberFormatNoValidation) {
//            return YES;
//        }
//        //else
//        [textField retain];
//        BOOL valid=NO;
//        NBPhoneNumberUtil *phoneUtil = [[NBPhoneNumberUtil alloc] init];
//        NSError *anError = nil;
//        
//        NSString *countryCode=[Location localLocation].country?[Location localLocation].country:[[SettingManager instance] phoneNumberDefaultCountry];
//        NBPhoneNumber *myNumber = [phoneUtil parse:textField.text defaultRegion:countryCode error:&anError];
//        valid=[phoneUtil isValidNumber:myNumber];
//        
//        if (valid){
//            //get regionCode
//            NSString *code=[phoneUtil getRegionCodeForNumber:myNumber];
//            switch ([[SettingManager instance] phoneFormate]) {
//                case PhoneNumberFormatE164:
//                    textField.text=[phoneUtil format:myNumber numberFormat:NBEPhoneNumberFormatE164 error:&anError];
//                    break;
//                case PhoneNumberFormatInternational:
//                    textField.text=[phoneUtil format:myNumber numberFormat:NBEPhoneNumberFormatINTERNATIONAL error:&anError];
//                    break;
//                case PhoneNumberFormatNational:
//                    if ([code caseInsensitiveCompare: countryCode] == NSOrderedSame)
//                        textField.text=[phoneUtil format:myNumber numberFormat:NBEPhoneNumberFormatNATIONAL error:&anError];
//                    else
//                        textField.text=[phoneUtil format:myNumber numberFormat:NBEPhoneNumberFormatINTERNATIONAL error:&anError];
//                    break;
//                    
//                default:
//                    break;
//            }
//        }
//        else{
//        }
//        [phoneUtil release];
//        [textField release];
//        return valid;
//    }
//    return YES;
}

@end
