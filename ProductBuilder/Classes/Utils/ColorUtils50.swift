//
//  ColorUtils.swift
//  RPlusTeamwork
//
//  Created by Alexander Martyshko on 6/29/16.
//  Copyright © 2016 Cloudworks. All rights reserved.
//

import UIKit

class ColorUtils: NSObject {
//    #define MO_RGBCOLOR(r,g,b) [UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:1]
//    #define MO_RGBACOLOR(r,g,b,a) [UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:a]
    
    class func ColorWithRGB(red : Float, green : Float, blue : Float) -> UIColor {
        return UIColor(red: CGFloat(red)/255.0, green: CGFloat(green)/255.0, blue: CGFloat(blue)/255.0, alpha: 1.0)
    }
    
    class func mainRedColor() -> UIColor {
        return ColorUtils.ColorWithRGB(red: 220, green: 68, blue: 71)
    }
    
    class func mainBlueColor() -> UIColor {
        return ColorUtils.ColorWithRGB(red: 0, green: 139, blue: 232)
    }
    
    class func infoLabelTextColor() -> UIColor {
        return ColorUtils.ColorWithRGB(red: 70, green: 70, blue: 70)
    }
    
    class func tableCellGrayTextColor() -> UIColor {
        return ColorUtils.ColorWithRGB(red: 144, green: 144, blue: 144)
    }
}

extension UIColor {
    convenience init( r: CGFloat, g: CGFloat, b: CGFloat) {
        self.init(red: r/255.0, green: g/255.0, blue: b/255.0, alpha: 1.0)
    }
    
    convenience init(_ rgb: CGFloat) {
        self.init(red: rgb/255.0, green: rgb/255.0, blue:rgb/255.0, alpha: 1.0)
    }
    
    open class var barButtonHighlighted: UIColor {
        get{
            //UIColor(r: 49, g: 172, b: 234)
            return UIColor(r: 62, g: 162, b: 233)
        }
    }
    
    open class var  barButtonNormal: UIColor {
        get{
            return .white
        }
    }
}
