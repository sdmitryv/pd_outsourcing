//
//  NSObject+NSCoding.m
//  OpenStack
//
//  Created by Michael Mayo on 3/4/11.
//  The OpenStack project is provided under the Apache 2.0 license.
//

#import "NSObject+NSCoding.h"
#import <objc/runtime.h>
#import <libkern/OSAtomic.h>
#import "os/lock.h"

static NSString* typeEncodingDecimal;
static NSString* typeEncodingDecimalFake;

typedef struct {
    unsigned int _length;     // length == 0 && isNegative -> NaN
    unsigned short _mantissa[NSDecimalMaxSize];
} NSDecimalFake;

@interface NSDecimalFakeObject :NSObject{
    NSDecimalFake df;
    NSDecimal d;
}
@end

@implementation NSDecimalFakeObject

@end


@implementation NSObject (NSCoding)

- (NSMutableDictionary *)propertiesForClass:(Class)klass {
    
    NSMutableDictionary *results = [[[NSMutableDictionary alloc] init] autorelease];
    
    unsigned int outCount, i;
    objc_property_t *properties = class_copyPropertyList(klass, &outCount);
    for(i = 0; i < outCount; i++) {
        objc_property_t property = properties[i];
        
        NSString *pname = @(property_getName(property));
        NSString *pattrs = @(property_getAttributes(property));
        
        pattrs = [pattrs componentsSeparatedByString:@","][0];
        pattrs = [pattrs substringFromIndex:1];
        
        results[pname] = pattrs;
    }
    free(properties);
    
    if ([klass superclass] != [NSObject class]) {
        [results addEntriesFromDictionary:[self propertiesForClass:[klass superclass]]];
    }
    
    return results;
}

//static OSSpinLock _lock;
static os_unfair_lock _lock;

+ (NSMutableDictionary*)classIvarsDictoinary {
    static NSMutableDictionary *_classIvarsDictoinary = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _classIvarsDictoinary = [[NSMutableDictionary alloc]init];
    });
    
    return _classIvarsDictoinary;
}

-(void)lockDict{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _lock = OS_UNFAIR_LOCK_INIT;
    });
    os_unfair_lock_lock(&_lock);
}

-(void)unlockDict{
    os_unfair_lock_unlock(&_lock);
}

- (NSDictionary *)iVarsForClass:(Class)class {
    NSString* className = NSStringFromClass(class);
    NSMutableDictionary* result = nil;
    NSMutableDictionary* classIvarsDictoinary = self.class.classIvarsDictoinary;
    //OSSpinLockLock(&_lock);
    [self lockDict];
    result = classIvarsDictoinary[className];
    //OSSpinLockUnlock(&_lock);
    [self unlockDict];
    if (!result){
    
        result = [[NSMutableDictionary alloc] init];
        
        unsigned int outCount, i;
        Ivar* ivars = class_copyIvarList([class class], &outCount);
        
        for(i = 0; i < outCount; i++) {
            NSString *pname = @(ivar_getName(ivars[i]));
            NSString *pattrs = @(ivar_getTypeEncoding(ivars[i]));
            result[pname] = pattrs;
        }
        free(ivars);
        
        if ([class superclass] != [NSObject class]) {
            [result addEntriesFromDictionary:[self iVarsForClass:[class superclass]]];
        }
        //OSSpinLockLock(&_lock);
        [self lockDict];
        classIvarsDictoinary[className] = result;
        //OSSpinLockUnlock(&_lock);
        [self unlockDict];
        [result release];
    }
    return result;
}

- (NSDictionary *)properties {
    return [self propertiesForClass:[self class]];
}

- (NSDictionary *)iVars {
    return [self iVarsForClass:[self class]];
}

-(NSString*)encodeType:(NSString*)typeEncoding{
    if (!typeEncodingDecimal){
        @synchronized(self){
            if (!typeEncodingDecimal){
                Ivar ivarDecimal = class_getInstanceVariable([NSDecimalFakeObject class], [@"d"UTF8String]);
                typeEncodingDecimal = [@(ivar_getTypeEncoding(ivarDecimal))retain];
            }
        }
    }
    if (!typeEncodingDecimalFake){
        @synchronized(self){
            if (!typeEncodingDecimalFake){
                Ivar ivarDecimalFake = class_getInstanceVariable([NSDecimalFakeObject class], [@"df"UTF8String]);
                typeEncodingDecimalFake = [@(ivar_getTypeEncoding(ivarDecimalFake))retain];
            }
        }
    }
    if (typeEncodingDecimal){
        NSRange range = [typeEncoding rangeOfString:typeEncodingDecimal];
        if (range.length > 0){
            return [typeEncoding stringByReplacingOccurrencesOfString:typeEncodingDecimal withString:typeEncodingDecimalFake];
        }
    }
    return typeEncoding;
}

-(void*)getValue:(Ivar)ivar{
    void* val = nil;
    val = (void *)((uint8_t *)self + ivar_getOffset(ivar));
    return val;
}

-(void)setVariable:(Ivar)ivar value:(void*)value size:(size_t)size{
    if (!ivar) return;
    uint8_t* pointer = (uint8_t *)self + ivar_getOffset(ivar);
    //*pointer = *((uint8_t*)value);
    memcpy(pointer, value, size);
}

- (void)saveToObject:(NSObject *)object {
    NSDictionary *iVars = [self iVars];
    for (NSString *key in iVars) {
        if (![self shouldEncodeProperty:key]) continue;
        NSString *type = iVars[key];
        Ivar ivar = class_getInstanceVariable([self class], [key UTF8String]);
        if (!ivar) continue;
        id value = nil;
        BOOL shouldRetain = TRUE;
        switch ([type characterAtIndex:0]){
            case '{':
            {
                void* val = nil;
                val = [self getValue:ivar];
                NSString* typeEncoding = [self encodeType:type];
                NSUInteger size = 0;
                NSGetSizeAndAlignment([typeEncoding UTF8String], &size, NULL);
                value = [NSData dataWithBytes:val length:size];
            }
                break;
            case 'i':
            {
                NSInteger* i = [self getValue:ivar];
                value = @(*i);
            }
                break;
            case 'I':
            {
                NSUInteger* i = [self getValue:ivar];
                value = @(*i);
            }
                break;
            case 'f':
            {
                float* f = [self getValue:ivar];
                value = @(*f);
            }
                break;
            case 'd':
            {
                double* d = [self getValue:ivar];
                value = @(*d);
            }
                break;
            case 'c':
            {
                char* c = [self getValue:ivar];
                value = @(*c);
            }
                break;
            case 'C':
            {
                unsigned char* c = [self getValue:ivar];
                value = @(*c);
            }
                break;
            case 'l':{
                long* l = [self getValue:ivar];
                value = @(*l);
            }
                break;
            case 'L':{
                unsigned long* l = [self getValue:ivar];
                value = @(*l);
            }
                break;
            case 'q':
            {
                long long* l = [self getValue:ivar];
                value = @(*l);
            }
                break;
            case 'Q':
            {
                unsigned long long* l = [self getValue:ivar];
                value = @(*l);
            }
                break;
            case 's':
            {
                short* s = [self getValue:ivar];
                value = @(*s);
            }
                break;
            case 'S':
            {
                unsigned short* s = [self getValue:ivar];
                value = @(*s);
            }
                break;
            case '@':
                value = object_getIvar(self, ivar);
                if ([type hasPrefix:@"@\"NSMutable"]){
                    value = [[value mutableCopy]autorelease];
                }
                else{
                    shouldRetain = FALSE;
                }
                break;
            default:
                break;
        }
        if (value){
            objc_setAssociatedObject(object, ivar, value,
                                     shouldRetain || [self shouldRetainProperty:key] ? OBJC_ASSOCIATION_RETAIN_NONATOMIC : OBJC_ASSOCIATION_ASSIGN);
        }
        
    }
}


- (void)restoreFromObject:(NSObject*)object {
    NSDictionary *iVars = [self iVars];
    for (NSString *key in iVars) {
        if (![self shouldEncodeProperty:key]) continue;
        NSString *type = iVars[key];
        Ivar ivar = class_getInstanceVariable([self class], [key UTF8String]);
        if (!ivar) continue;
        id value = objc_getAssociatedObject(object, ivar);
        switch ([type characterAtIndex:0]) {
            case '@':   // object
            {
                NSInteger countOfQuatation = 0;
                for(NSInteger i=0; i<type.length;i++){
                    if ([type characterAtIndex:i]== '"'){
                        countOfQuatation++;
                        if (countOfQuatation > 1) break;
                    }
                }
                //if ([[type componentsSeparatedByString:@"\""] count] > 1) {
                if (countOfQuatation > 1) {
                    id oldValue = object_getIvar(self, ivar);
                    object_setIvar(self, ivar, value);
                    if ([self shouldRetainProperty:key]){
                        [oldValue release];
                        [value retain];
                    }
                }
            }
                break;
            case '{':
            {
                if ([value isKindOfClass:[NSData class]]){
                    NSData* data = (NSData*)value;
                    NSString* typeEncoding = [self encodeType:type];
                    NSUInteger size = 0;
                    NSGetSizeAndAlignment([typeEncoding UTF8String], &size, NULL);
                    void* vv = calloc(size, sizeof(Byte));
                    if (data){
                        NSValue* v= [[NSValue alloc ]initWithBytes:data.bytes objCType:[typeEncoding UTF8String]];
                        [v getValue:vv];
                        [v release];
                    }
                    [self setVariable:ivar value:vv size:size];
                    free(vv);
                }
            }
                break;
            case 'i':
            {
                NSInteger v = [(NSNumber*)value integerValue];
                [self setVariable:ivar value:&v size:sizeof(NSInteger)];
            }
                break;
            case 'I':
            {
                NSUInteger v = [(NSNumber*)value unsignedIntegerValue];
                [self setVariable:ivar value:&v size:sizeof(NSUInteger)];
            }
                break;
            case 'f':
            {
                float v = [(NSNumber*)value floatValue];
                [self setVariable:ivar value:&v size:sizeof(float)];
            }
                break;
            case 'd':
            {
                double v = [(NSNumber*)value doubleValue];
                [self setVariable:ivar value:&v size:sizeof(double)];
            }
                break;
            case 'c':
            {
                char v = [(NSNumber*)value charValue];
                [self setVariable:ivar value:&v size:sizeof(char)];
            }
                break;
            case 'C':
            {
                unsigned char v = [(NSNumber*)value unsignedCharValue];
                [self setVariable:ivar value:&v size:sizeof(unsigned char)];
            }
                break;
            case 'l':
            {
                long v = [(NSNumber*)value longValue];
                [self setVariable:ivar value:&v size:sizeof(long)];
            }
                break;
            case 'L':
            {
                unsigned long v = [(NSNumber*)value unsignedLongValue];
                [self setVariable:ivar value:&v size:sizeof(unsigned long)];
            }
                break;
            case 'q':
            {
                long long v = [(NSNumber*)value longLongValue];
                [self setVariable:ivar value:&v size:sizeof(long long)];
            }
                break;
            case 'Q':
            {
                unsigned long long v = [(NSNumber*)value unsignedLongLongValue];
                [self setVariable:ivar value:&v size:sizeof(unsigned long long)];
            }
                break;
            case 's':
            {
                short s = [(NSNumber*)value shortValue];
                [self setVariable:ivar value:&s size:sizeof(short)];

            }
                break;
            case 'S':
            {
                unsigned short s = [(NSNumber*)value unsignedShortValue];
                [self setVariable:ivar value:&s size:sizeof(unsigned short)];
            }
                break;
            default:
                //object_setIvar(self, ivar, value);
                break;
        }
    }
}

- (BOOL)shouldEncodeProperty:(NSString*)name{
    return true;
}

- (BOOL)shouldRetainProperty:(NSString*)name{
    return TRUE;
}

@end
