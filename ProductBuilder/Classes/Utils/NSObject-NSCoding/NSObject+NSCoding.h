//
//  NSObject+NSCoding.h
//  OpenStack
//
//  Created by Michael Mayo on 3/4/11.
//  The OpenStack project is provided under the Apache 2.0 license.
//

#import <Foundation/Foundation.h>

@interface NSObject (NSCoding)

///- (void)autoEncodeWithCoder: (NSCoder *)coder;
//- (void)autoDecode:(NSCoder *)coder;
- (NSDictionary *)properties;
- (NSDictionary *)iVars;
- (BOOL)shouldEncodeProperty:(NSString*)name;
- (BOOL)shouldRetainProperty:(NSString*)name;
- (void)saveToObject:(NSObject *)object;
- (void)restoreFromObject:(NSObject*)object;
@end
