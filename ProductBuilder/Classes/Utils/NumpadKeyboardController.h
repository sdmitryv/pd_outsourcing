//
//  NumkeyboardController.h
//  StockCount
//
//  Created by Valera on 12/1/10.
//  Copyright 2010 DBBest Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KBNotificationsHelper.h"

typedef NS_ENUM(NSUInteger, NumpadKeyboardContent) {
    NumpadKeyboardContentsNone       = 0,
    NumpadKeyboardContentsDot        = 1 << 0,
    NumpadKeyboardContentsKeyboard   = 1 << 1,
    NumpadKeyboardContentsMinus      = 1 << 2,
    NumpadKeyboardContentsTab        = 1 << 3
};

@class NumpadKeyboardController;

@protocol NumpadKeyboardControllerDelegate

@optional

-(void)keyboardButtonClick:(NumpadKeyboardController *)numpadKeyboardController;
-(void)enterClick:(NumpadKeyboardController *)numpadKeyboardController;
-(void)tabButtonClick:(NumpadKeyboardController *)numpadKeyboardController;

@end

@interface NumpadKeyboardController : UIViewController<KBNotificationsHelperDelegate> {
	
	IBOutlet UIButton* btn1;
	IBOutlet UIButton* btn2;
	IBOutlet UIButton* btn3;
	IBOutlet UIButton* btn4;
	IBOutlet UIButton* btn5;
	IBOutlet UIButton* btn6;
	IBOutlet UIButton* btn7;
	IBOutlet UIButton* btn8;
	IBOutlet UIButton* btn9;
	IBOutlet UIButton* btn0;
	IBOutlet UIButton* btnDot;
	IBOutlet UIButton* btnBs;
	IBOutlet UIButton* btnKb;
	IBOutlet UIButton* btnMinus;
	IBOutlet UIButton* btnTab;
	IBOutlet UIButton* btnEnter;
	IBOutlet UIButton* btnSmallEnter;
	IBOutlet UIButton* btnBigEnter;
	IBOutlet UIButton* btnWide0;
	//IBOutletCollection(UIButton*) NSArray *buttons;
	UITextField* _currentResponder;
	id delegate;
    IBOutlet UIView* contentView;
    NumpadKeyboardContent _contents;
}

@property (nonatomic, assign) id delegate;
@property (nonatomic, assign) NumpadKeyboardContent contents;

-(IBAction)btnClick:(UIButton*)button;

-(void)adjustContents;

@end
