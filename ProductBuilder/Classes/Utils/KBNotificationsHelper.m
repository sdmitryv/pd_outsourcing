//
// IQNotificationsHelper.m
// iQueue
//
// Created by Seivan Heidari on 7/14/10.
// Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "KBNotificationsHelper.h"


@implementation KBNotificationsHelper

@synthesize delegate;

+(void)setKeyboardNotificationsToDelegate:(id<KBNotificationsHelperDelegate>)delegate{
	
	if ([delegate respondsToSelector:@selector(keyboardDidShow:)])
		[[NSNotificationCenter defaultCenter] addObserver: delegate
											 selector: @selector(keyboardDidShow:)
												 name: UIKeyboardDidShowNotification object:nil];
	if ([delegate respondsToSelector:@selector(keyboardWillHide:)])
		[[NSNotificationCenter defaultCenter] addObserver: delegate
											 selector: @selector(keyboardWillHide:)
												 name: UIKeyboardWillHideNotification object:nil];
	if ([delegate respondsToSelector:@selector(keyboardWillShow:)])
		[[NSNotificationCenter defaultCenter] addObserver: delegate
												selector: @selector(keyboardWillShow:)
													name: UIKeyboardWillShowNotification object:nil];
	if ([delegate respondsToSelector:@selector(keyboardDidHide:)])
		[[NSNotificationCenter defaultCenter] addObserver: delegate selector: @selector(keyboardDidHide:)
													name: UIKeyboardDidHideNotification object:nil];
	
}
+(void)removeKeyboardNotificationsFromDelegate:(id<KBNotificationsHelperDelegate>)delegate {
	
	[[NSNotificationCenter defaultCenter] removeObserver:delegate name:UIKeyboardDidShowNotification object:nil];
	[[NSNotificationCenter defaultCenter] removeObserver:delegate name:UIKeyboardWillShowNotification object:nil];
	[[NSNotificationCenter defaultCenter] removeObserver:delegate name:UIKeyboardWillHideNotification object:nil];
	[[NSNotificationCenter defaultCenter] removeObserver:delegate name:UIKeyboardDidHideNotification object:nil];
}
@end