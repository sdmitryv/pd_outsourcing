//
//  UIImage+Arrow.m
//  ProductBuilder
//
//  Created by Alexander Martyshko on 7/10/15.
//  Copyright (c) 2015 Cloudworks. All rights reserved.
//

#import "UIImage+Arrow.h"

@implementation UIImage (Arrow)

+ (UIImage *)arrowImageWithSize:(CGSize)size color:(UIColor *)color direction:(ArrowDirection)direction {
    UIGraphicsBeginImageContextWithOptions(size, NO, 0.0);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetStrokeColorWithColor(context, color.CGColor);
    CGContextSetFillColorWithColor(context, color.CGColor);
    
    CGContextSaveGState(context);
    
    CGContextBeginPath(context);
    switch (direction) {
        case ArrowDirectionRight: {
            CGContextMoveToPoint(context, 0, 0);
            CGContextAddLineToPoint(context, size.width, size.height/2);
            CGContextAddLineToPoint(context, 0, size.height);
            CGContextAddLineToPoint(context, 0, 0);
        }
            break;
        case ArrowDirectionLeft:{
            CGContextMoveToPoint(context, size.width, 0);
            CGContextAddLineToPoint(context, 0, size.height/2);
            CGContextAddLineToPoint(context, size.width, size.height);
            CGContextAddLineToPoint(context, size.width, 0);
        }
            break;
        case ArrowDirectionUp:{
            CGContextMoveToPoint(context, 0, size.height);
            CGContextAddLineToPoint(context, size.width/2, 0);
            CGContextAddLineToPoint(context, size.width, size.height);
            CGContextAddLineToPoint(context, 0, size.height);
        }
            break;
        case ArrowDirectionDown:{
            CGContextMoveToPoint(context, 0, 0);
            CGContextAddLineToPoint(context, size.width, 0);
            CGContextAddLineToPoint(context, size.width/2, size.height);
            CGContextAddLineToPoint(context, 0, 0);
        }
            break;
        default:
            break;
    }
    
    CGContextFillPath(context);
    
    CGContextRestoreGState(context);
    
    UIImage *resultImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return resultImage;
}

@end
