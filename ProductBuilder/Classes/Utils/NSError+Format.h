//
//  NSError+Format.h
//  ProductBuilder
//
//  Created by valera on 1/4/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSError(Format)

-(NSString*)descriptionExtended;
@end
