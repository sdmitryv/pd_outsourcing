//
//  NSString+RegExp.h
//  iPadPOS
//
//  Created by valera on 5/12/11.
//  Copyright 2011 DBBestTechnologies. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface NSString(RegExp)
-(BOOL)isMatchedByRegex:(NSString*)pattern;
@end
