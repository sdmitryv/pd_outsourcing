//
//  NSDate+System.m
//  ProductBuilder
//
//  Created by Roman on 3/13/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "NSDate+System.h"

@implementation NSDate (System)

-(NSDate*)toTimeZone:(NSTimeZone*)timeZone {
    return [[[NSDate alloc] initWithTimeInterval:[timeZone secondsFromGMTForDate:self]
                                       sinceDate:self] autorelease];
}

-(NSDate*)toSystemTimeZone {
    return [self toTimeZone:[NSTimeZone systemTimeZone]];
}

+(NSDate*)systemDate {
    return [[NSDate date] toSystemTimeZone];
}

-(NSDate*)toUTCTimeZone {
    //return [self toTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
    
    return [[[NSDate alloc] initWithTimeInterval:[[NSTimeZone timeZoneWithName:@"UTC"] secondsFromGMTForDate:self] - [[NSTimeZone systemTimeZone] secondsFromGMTForDate:self]
                                       sinceDate:self] autorelease];
}

@end
