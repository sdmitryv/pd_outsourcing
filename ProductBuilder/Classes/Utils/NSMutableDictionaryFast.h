//
//  NSMutableDictionaryFast.h
//  ProductBuilder
//
//  Created by valera on 1/17/13.
//  Copyright (c) 2013 Cloudworks. All rights reserved.
//

#import <Foundation/Foundation.h>
@interface NSEnumeratorFast : NSObject
-(id)nextObject;
@end

@interface NSMutableDictionaryFast : NSObject
-(id)initWithCapacity:(NSUInteger)numItems;
- (void)setObject: (id)obj forKey: (id)key;
- (void)setObject:(id)value forKeyedSubscript:key;
- (void)removeObjectForKey:(id)key;
-(void)removeAllObjects;
- (NSUInteger)count;
- (id)objectForKey: (id)key;
- (id)objectForKeyedSubscript:(id)key;
- (void)enumerateKeysAndObjectsUsingBlock:(void (^)(id key, id obj, BOOL *stop))block NS_AVAILABLE(10_6, 4_0);
@end
