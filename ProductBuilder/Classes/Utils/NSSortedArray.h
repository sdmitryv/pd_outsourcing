//
//  NSSortedArray.h
//  StockCount
//
//  Created by Valera on 12/7/10.
//  Copyright 2010 DBBest Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CEntityList.h"

@interface NSArray(FunctionalStyle)
- (NSArray*)filter:(BOOL(^)(id elt))filterBlock;
@end

@interface NSMutableArray(CaseInsensitiveSort)
- (void)insensitiveSortByKey:(NSString*)key ascending:(BOOL)ascending;
@end

@interface NSSortedArray : NSObject<NSFastEnumeration> {
    id	arrayOriginal;
	NSMutableArray*	arraySorted;
	NSMutableArray*	arrayFiltered;
	BOOL			filtered;
	BOOL			sorted;
	NSString*		sortingKey;
	BOOL			sortAscending;
	BOOL(^filteringBlock)(id elt);
    NSPredicate*    filteringPredicate;
}
- (id)initWithArray:(NSArray*)array;
- (id)initWithCEntityList:(CEntityList*)array;
- (id)originalArray;
- (void)setOriginalArray:(id)value;
- (NSUInteger)count;
- (id)objectAtIndex:(NSUInteger)index;
- (id)objectAtIndexedSubscript:(NSUInteger)index;
- (NSUInteger)indexOfObject:(id)anObject;
- (void)sortByKey:(NSString*)key ascending:(BOOL)ascending;
- (void)sortStringByKey:(NSString*)key ascending:(BOOL)ascending;
- (void)sortByKey:(NSString*)key ascending:(BOOL)ascending comparator:(NSComparator)comparator useDescriptor:(BOOL)useDescriptor;
- (void)removeSort;
- (void)resort;
- (void)applyFilter:(BOOL(^)(id elt))filterBlock;
- (void)applyFilterWithPredicate:(NSPredicate*)predicate;
- (void)reApplyFilter;
- (void)removeFilter;
- (BOOL)filtered;
- (BOOL)sorted;
- (void)refresh;
- (NSArray*)filteredArray;
@end
    
@interface NSMutableSortedArray : NSSortedArray {
}
- (id)initWithMutableArray:(NSMutableArray*)array;
- (void)addObject:(id)anObject;
- (void)removeObjectAtIndex:(NSUInteger)index;
- (void)insertObject:(id)anObject atIndex:(NSUInteger)index;
- (void)replaceObjectAtIndex:(NSUInteger)index withObject:(id)object;
- (void)setObject:(id)anObject atIndexedSubscript:(NSUInteger)index;
- (void)removeObjectsAtIndexes:(NSIndexSet *)indexes;
- (void)removeAllObjects;
@end
