//
//  ImageHelper.swift
//  RPlus
//
//  Created by Alexander Martyshko on 7/28/16.
//  Copyright © 2016 Cloudworks. All rights reserved.
//

import UIKit
import Photos

class ImageHelper: NSObject {
    
    static let tableCellArrowImage = ImageHelper.tableViewCellArrowImage(CGSize(width: 10, height: 16), color: ColorUtils.ColorWithRGB(red: 199, green: 199, blue: 204))

    class func tableViewCellArrowImage(_ size: CGSize, color: UIColor) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(size, false, 0.0);
        let context = UIGraphicsGetCurrentContext()
        
        let imageOffset = CGFloat(2.0);
        
        context?.setShouldAntialias(true );
        context?.setStrokeColor(color.cgColor);
        context?.setFillColor(color.cgColor);
        context?.setLineWidth(2);
        
        context?.saveGState();
        context?.beginPath();
        
        context?.move(to: CGPoint(x: imageOffset, y: imageOffset));
        context?.addLine(to: CGPoint(x: size.width-imageOffset, y: size.height/2));
        context?.addLine(to: CGPoint(x: imageOffset, y: size.height - imageOffset));
        
        context?.strokePath();
        context?.restoreGState();
        
        let resultImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        return resultImage!;
    }
    
    class func circleImage(_ withSize: CGSize, color: UIColor) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(withSize, false, 0.0);
        let context = UIGraphicsGetCurrentContext()
        
        context?.setShouldAntialias(true );
        context?.setStrokeColor(color.cgColor);
        context?.setFillColor(color.cgColor);
        context?.setLineWidth(2);
        
        context?.saveGState();
        context?.beginPath();
        
        context?.fillEllipse(in: CGRect(x: 0, y: 0, width: withSize.width, height: withSize.height))
        
        context?.strokePath();
        context?.restoreGState();
        
        let resultImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        return resultImage!;
    }
    
    class func rectImage(_ withSize: CGSize, color: UIColor) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(withSize, false, 0.0);
        let context = UIGraphicsGetCurrentContext()
        
        context?.setShouldAntialias(true );
        context?.setStrokeColor(color.cgColor);
        context?.setFillColor(color.cgColor);
        
        context?.saveGState();
        context?.beginPath();
        
        context?.fill(CGRect(origin: CGPoint(x: 0, y: 0), size: withSize))
        
        context?.strokePath();
        context?.restoreGState();
        
        let resultImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        return resultImage!;
    }
    
    class func segmentImage(_ withImage: UIImage?, string: String, color: UIColor) -> UIImage {
        let font = UIFont.systemFont(ofSize: 14)
        let expectedTextSize = (string as NSString).size(attributes: [NSFontAttributeName: font])
        
        var imageSize = CGSize.zero
        if withImage != nil {
            imageSize = withImage!.size
        }
        let size  = CGSize(width: imageSize.width + 5 + expectedTextSize.width, height: max(expectedTextSize.height, imageSize.height))
        
        UIGraphicsBeginImageContextWithOptions(size, false, 0.0);
        let context = UIGraphicsGetCurrentContext()
        context?.setFillColor(color.cgColor)
        let fontTopPosition = (size.height - expectedTextSize.height) / 2
        let textPoint = CGPoint(x: imageSize.width + 5, y: fontTopPosition)
        (string as NSString).draw(at: textPoint, withAttributes: [NSFontAttributeName : font])
        
        if let image = withImage {
            context?.concatenate(CGAffineTransform(a: 1, b: 0, c: 0, d: -1, tx: 0, ty: size.height))
            context?.draw(image.cgImage!, in: CGRect(x: 0, y: (size.height - image.size.height) / 2, width: imageSize.width, height: imageSize.height))
        }
        
        let resultImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        return resultImage!;
    }
    
    class func crossImage(_ size: CGSize, color: UIColor) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(size, false, 0.0);
        let context = UIGraphicsGetCurrentContext()
        
        //let imageOffset = CGFloat(2.0);
        
        context?.setShouldAntialias(true);
        context?.setStrokeColor(color.cgColor);
        context?.setFillColor(color.cgColor);
        context?.setLineWidth(1.5);
        
        context?.saveGState();
        context?.beginPath();
        
        context?.move(to: CGPoint(x: 0, y: 0));
        context?.addLine(to: CGPoint(x: size.width, y: size.height));
        
        context?.move(to: CGPoint(x: size.width, y: 0));
        context?.addLine(to: CGPoint(x: 0, y: size.height));
        
        context?.strokePath();
        context?.restoreGState();
        
        let resultImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        return resultImage!;
    }
    
    class func resizedImageFromImage(_ inImage: UIImage?, maxWidth: CGFloat, maxHeight: CGFloat) -> UIImage? {
        
        if let image = inImage {
            let tempWidth = maxWidth;
            let tempHeight = maxHeight;
            
            var height = tempHeight;
            var width = tempHeight * image.size.width / image.size.height;
            
            if (width > tempWidth) {
                
                width = tempWidth;
                height = tempWidth * image.size.height / image.size.width;
            }
            
            let itemSize = CGSize(width: width, height: height);
            UIGraphicsBeginImageContextWithOptions(itemSize, false, inImage?.scale ?? 0.0);
            let imageRect = CGRect(x: 0.0, y: 0.0, width: itemSize.width, height: itemSize.height);
            image.draw(in: imageRect)
            let result = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            return result
        }
        else {
            return nil
        }
    }
    
    static func getLastTakenPhoto(completion: @escaping (UIImage?)->Void) {
        
        let imgManager = PHImageManager.default()
        
        // Note that if the request is not set to synchronous
        // the requestImageForAsset will return both the image
        // and thumbnail; by setting synchronous to true it
        // will return just the thumbnail
        let requestOptions = PHImageRequestOptions()
        requestOptions.isSynchronous = true
        
        // Sort the images by creation date
        let fetchOptions = PHFetchOptions()
        fetchOptions.sortDescriptors = [NSSortDescriptor(key:"creationDate", ascending: true)]
        
        let fetchResult: PHFetchResult<PHAsset> = PHAsset.fetchAssets(with: .image, options: fetchOptions)
        if fetchResult.count > 0 {
            // Perform the image request
            imgManager.requestImage(for: fetchResult.object(at: fetchResult.count - 1) as PHAsset, targetSize: PHImageManagerMaximumSize, contentMode: PHImageContentMode.aspectFill, options: requestOptions, resultHandler: { (image, _) in
                completion(image)
            })
        } else {
            completion(nil)
        }

    }
//    + (id) imageFromImage:(UIImage*)image string:(NSString*)string color:(UIColor*)color
//    {
//    UIFont *font = [UIFont systemFontOfSize:16.0];
//    CGSize expectedTextSize = [string sizeWithAttributes:@{NSFontAttributeName: font}];
//    int width = expectedTextSize.width + image.size.width + 5;
//    int height = MAX(expectedTextSize.height, image.size.width);
//    CGSize size = CGSizeMake((float)width, (float)height);
//    UIGraphicsBeginImageContextWithOptions(size, NO, 0);
//    CGContextRef context = UIGraphicsGetCurrentContext();
//    CGContextSetFillColorWithColor(context, color.CGColor);
//    int fontTopPosition = (height - expectedTextSize.height) / 2;
//    CGPoint textPoint = CGPointMake(image.size.width + 5, fontTopPosition);
//    
//    [string drawAtPoint:textPoint withAttributes:@{NSFontAttributeName: font}];
//    // Images upside down so flip them
//    CGAffineTransform flipVertical = CGAffineTransformMake(1, 0, 0, -1, 0, size.height);
//    CGContextConcatCTM(context, flipVertical);
//    CGContextDrawImage(context, (CGRect){ {0, (height - image.size.height) / 2}, {image.size.width, image.size.height} }, [image CGImage]);
//    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
//    UIGraphicsEndImageContext();
//    return newImage;
//    }
}
