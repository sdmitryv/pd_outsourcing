//
//  SecurityManager.h
//  CloudworksPOS
//
//  Created by Vitaliy Gervazuk on 8/9/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Employee.h"


extern NSString* const SecurityManagerCurrentEmployeeDidChange;

@interface SecurityManager : NSObject {
}

+(NSString*)getPaswordHash:(NSString*)password;
+(Employee*)authorizeUser:(NSString*)loginName password:(NSString*)password;
+(Employee*)findEmployeeByLoginName:(NSString*)loginName;
+(Employee*)findEmployeeByLoginName:(NSString*)loginName isAll:(BOOL)isAll;
+(Employee*)currentEmployee;
+(void)setCurrentEmployee:(Employee*)employee;
+(BOOL)isUserInRole:(BPUUID*)employeeId role:(NSString*)role;

@end

@class UserRole;

// customer
extern NSString* const UserRoleCustomersAdd; // Customers - Add
extern NSString* const UserRoleCustomersEdit; // Customers - Edit
extern NSString* const UserRoleCustomersAdjustStoreCredit; // Customers - Adjust Store Credit
extern NSString* const UserRoleCustomerDeactivate; // Customer - Deactivate
extern NSString* const UserRoleCustomersAdjustGiftCard; // Customers - Adjust Gift Card
extern NSString* const UserRoleCustomersLoyaltyRewardsAdj; // Customers - Allow Loyalty Rewards adjustments
extern NSString* const UserRoleCustomersEditSecureCustomField; // Customers - Allow Edit Secure Custom Field
// drawer memos
extern NSString* const UserRoleDrawerMemosEditMedia; // Drawer Memos - Edit media
extern NSString* const UserRoleDrawerMemosEditOpenAmt; // Drawer Memos - Edit Open Amt
extern NSString* const UserRoleDrawerMemosFinalize; // Drawer Memos - Finalize Drawer
extern NSString* const UserRoleDrawerMemosOpenNew; // Drawer Memos - Open new
extern NSString* const UserRoleDrawerMemosAddPaidInOut; // Drawer Memos - Add Paid in/out
extern NSString* const UserRoleDrawerMemosAddVoidInOut; // Drawer Memos-Void Paid in/out
extern NSString* const UserRoleDrawerMemosTakeOffline; // Drawer Memos - Take Dr Offline
extern NSString* const UserRoleDrawerMemosManageOtherWs; // Drawer Memos - Allow to manage other workstations drawer memos
// items
extern NSString* const UserRoleItemsAddEdit; // Inventory Items - Add/Edit
extern NSString* const UserRoleItemsDelete; // Inventory Items - Delete
extern NSString* const UserRoleViewCosts; // View Costs
extern NSString* const UserRoleItemsPrintTags; // Inventory Items - Print tags
extern NSString* const UserRoleItemsManualWeight; // Inventory Items - Allow manual weight entry
// physical inventory
extern NSString* const UserRolePIAdd; // PI - Add
extern NSString* const UserRolePIDeleteScanCount; // PI - Delete scan count
extern NSString* const UserRolePIEditScanCount; // PI - Edit scan count
extern NSString* const UserRolePIPrepareForHQ; // PI - Prepare for HQ
// purchase orders
extern NSString* const UserRolePOAddEdit; // Purchase Orders - Add/Edit
// prices
extern NSString* const UserRoleItemPricesAddEdit; // Inven Item Prices - Add/Edit
extern NSString* const UserRoleChangePriceLevel; // SR/Custmr - Change price level
extern NSString* const UserRoleChangeOfferPrice; //SR - Change offer price
// purchase receipts
extern NSString* const UserRolePurchReceiptsAdd; // Purchase Receipts - Add
extern NSString* const UserRolePurchReceiptsCancelDate; // Prch Rcpts-Past PO canc date
extern NSString* const UserRolePurchReceiptsChangeAssociate; // Prch Rcpts - Change associate
extern NSString* const UserRolePurchReceiptsReprint; // Purchase Receipts - Reprint
extern NSString* const UserRolePurchReceiptsVoid;// Purchase Rcpts - Void/Reverse
// sales receipts
extern NSString* const UserRoleSalesReceiptsAdd; // Sales Rcpts - Add
extern NSString* const UserRoleSalesReceiptsOpenCashDrawer; // Sales Rcpts - Open cash drawer
extern NSString* const UserRoleSalesReceiptsOpenCashDrawerWithSale; // Open the cash drawer when a sale is made that requires the drawer to open
extern NSString* const UserRoleSalesReceiptsAddAnotherLocation; //Allow the user to create a sales receipt for another location
extern NSString* const UserRoleSalesReceiptsOverrideCashDrawerCloseRestriction; // Override the restriction of not printing until the cash drawer is closed
extern NSString* const UserRoleSalesReceiptsReprint;
extern NSString* const UserRoleSalesReceiptsChangeAssociate; // Sales Rcpts - Change associate
extern NSString* const UserRoleSalesReceiptsChangeCashier; // Sales Rcpts - Change cashier
extern NSString* const UserRoleSalesReceiptsHold; // Sales Rcpts - Hold
extern NSString* const UserRoleSalesReceiptsVoid; // Sales Rcpts - Void/Reverse
extern NSString* const UserRoleSalesReceiptsUpdTradeInPrice; // Sales Receipts - Update Trade-in Price
extern NSString* const UserRoleSalesReceiptsOriginalReceiptNotRequiredForReturn; // Sales Receipts - Original Receipts Not Required for Return
extern NSString* const UserRoleAllowReturnWithoutCustomer; // Sales Receipts - OAllow return without customer
extern NSString* const UserRoleSalesReceiptsApplyMemberPricing; //Sales Receipts - Manually apply Member Pricing
extern NSString* const UserRoleSalesReceiptsAllowCardCredit; // Allow credit card 'Credit' transaction
extern NSString* const UserRoleSalesReceiptsAllowMOTO; // Allow credit card MOTO transaction processing
extern NSString* const UserRoleSalesReceiptsAllowVoiceAuth; // Allow voice Authorization for CC payments
extern NSString* const UserRoleSalesReceiptsAllowKeyedAuth; // Allow Keyed Authorization for CC payments
extern NSString* const UserRoleSalesReceiptCardOnFileSaveAllowed; // Allows to save the card on file by processed payment
extern NSString* const UserRoleSalesReceiptCardOnFileUseAllowed; // Allows to use saved cards on files for payment processing
extern NSString* const UserRoleSalesReceipOverrideCustomerCreditLimit; // Allows override customer credit limit

//extern NSString* const UserRoleSalesReceiptsChangeTax; // Sales Receipts - Change Tax on Receipt
extern NSString* const UserRoleSalesChangeTaxArea; //Allows user to change Tax Area for Sale Items on the Sales Receipt and Sales Order
extern NSString* const UserRoleSalesChangeTaxPercent;// Allows user to edit Tax percentage for Tax Jurisdiction for Sale Items on the Sales Receipt and Sales Order
extern NSString* const UserRoleSalesChangeTaxExempt; //Allows user to change Tax Exempt flag for Sale Items on the Sales Receipt and Sales Order
extern NSString* const UserRoleSalesChangeTaxAreaForReturns;// Allows user to change Tax Area for Return Items on the Sales Receipt
extern NSString* const UserRoleSalesChangeTaxPercentForReturns;// Allows user to edit Tax percentage for Tax Jurisdiction for Return Items on the Sales Receipt
extern NSString* const UserRoleSalesChangeTaxExemptForReturns;// Allows user to change Tax Exempt flag for Return Items on the Sales Receipt
extern NSString* const UserRoleSalesAllowChangeDefaultDiscount;// Allow changing default discount percent/amount in sales receipt and sales order
extern NSString* const UserRoleSalesReceiptsDiscardAllHeld; // Sales Receipts - Discard All held receipts
extern NSString* const UserRoleSalesReceiptsChangeExchangeRate; // Sales Receipts - Allows user to change exchange rate for Foreign Currency payment 
// sales orders
extern NSString* const UserRoleSalesOrdersAddEdit; // Sales Orders - Add/Edit
extern NSString* const UserRoleOverrideSalesOrderDeposit; // Sales Orders - Override SO Item Deposit
extern NSString* const UserRoleAccessSalesOrders; // Sales Orders - User may access, create and edit sales orders
extern NSString* const UserRoleSalesOrderChangeAssociate; // Sales Orders - User can change associate
extern NSString* const UserRoleSalesOrderChangeCashier; // Sales Orders - User can change cashier
extern NSString* const UserRoleSalesOrderChangeSellFromLocation; // Sales Orders - User may change Sell/Fulfill From Location on the order
extern NSString* const UserRoleSalesOrderChangeSaleCreditLocation; // Sales Orders - User may change Sale Credit Location on the order
extern NSString* const UserRoleSalesOrderRefundDeposit; // Sales Orders - User may refund deposit on a SalesOrder
extern NSString* const UserRoleSalesOrderRefundDepositArchived; // Sales Orders - User may refund deposit on a SalesOrder which is in Archive state
extern NSString* const UserRoleSalesOrderRefundDepositExternalOMS; // Sales Orders - User may refund deposit on a SalesOrder which is in External OMS state
extern NSString* const UserRoleSalesOrderAllowsIgnoreQtyCheck; // Sales Orders - Allows to ignore Qty check,
extern NSString* const UserRoleSalesOrderAllowsEditLinkedToPOItemWithDropShip; // Sales Orders -  Allows user to cancel or change quantity of sales order item which refers purchase order with Drop Ship option selected.
// repair orders
extern NSString* const UserRoleRepairOrdersAddEdit; // Repair Orders - Add/Edit
// wish lists
extern NSString* const UserRoleWishListsAddEdit; // Wish Lists - Add/Edit
//delivery
extern NSString* const UserRoleDeliveryOrdersAddEdit; // Delivery Orders - Add/Edit
//layaway
extern NSString* const UserRoleLayawaysAddEdit; // Layaway - Add/Edit
//retnal
extern NSString* const UserRoleRentalAddEdit; // Rental - Add/Edit
// system
extern NSString* const UserRoleCustomDesignAdmin; 
extern NSString* const UserRoleSystemAccessCustomer; // System - Customer Access
extern NSString* const UserRoleSystemAccessDrawerMemos; // System - Access Drawer Memos
extern NSString* const UserRoleSystemAccessItems; // System - Access Items
extern NSString* const UserRoleSystemAccessPurchaseOrders; // System - Access Purch. Orders
extern NSString* const UserRoleSystemAccessPurchaseReceipts; // System - Access Purch. Rcpts
extern NSString* const UserRoleSystemAccessSalesReceipts; // System - Access Sales Receipts
extern NSString* const UserRoleSystemAccessTransferMemos; // System - Access Transfer Memos
extern NSString* const UserRoleSystemAccessTransferNotices; // System - Access Trans. Notices
extern NSString* const UserRoleSystemAccessInternalMessaging; // System - Access Internal Messaging
extern NSString* const UserRoleSystemAccessMenuDesigner; // System - Access Menu Designer
extern NSString* const UserRoleSystemAccessPPN; // System - Access Price Plan Notices
extern NSString* const UserRoleSystemAccessRepairOrders; // System - Access Repair Orders
extern NSString* const UserRoleSystemAccessVendors; // System - Access Vendors
extern NSString* const UserRoleSystemAccessWishList; // System - Access Wish Lists
extern NSString* const UserRoleSystemAccessLayawayOrder; // System - Access Layaway Orders
extern NSString* const UserRoleSystemAccessRental; // System - Access Rental Orders
extern NSString* const UserRoleSystemAccessSalesOrder; // System - Sales Orders
extern NSString* const UserRoleSystemAccessDeliveryOrders; // System - Delivery Orders
extern NSString* const UserRoleSystemAccessQtyAdjustment; // System - Qty Adjustments
extern NSString* const UserRoleSystemAccessInventoryCatalogMemos; // System - Inventory Catalog Memos
extern NSString* const UserRoleSystemAccessAdministrationMenu; // System - Administration Menu
// transfer memos
extern NSString* const UserRoleTransferMemosChangeAssociate; // Transfer Memos - Change assoc.
extern NSString* const UserRoleTransferMemosHold; // Transfer Memos - Hold
extern NSString* const UserRoleTransferMemosVoid; // Transfer Memos - Void/Reverse
// vendors
extern NSString* const UserRoleVendorAddEdit; // Vendor - Add/Edit
// Call center
extern NSString* const UserRoleCallCenterPractice;
// inventory catalog memos
extern NSString* const UserRoleInventoryCatalogMemoChangeAssociate; // Inventory Catalog Memos - Change Associate
extern NSString* const UserRoleInventoryCatalogMemoAddEditCorporative;
//stock count
extern NSString* const UserRoleStockCountAdd; //Stock Count -New Stock Count
extern NSString* const UserRoleStockCountEdit; //Stock Count - Edit Stock Count
extern NSString* const UserRoleStockCountArchive; //Stock Count - Archive Stock Count
extern NSString* const UserRoleStockCountFinalize; //Stock Count - Finalize Stock Count
extern NSString* const UserRoleStockCountViewOHQ; //Stock Count - View On Hand Quantities
//stored value service
extern NSString* const UserRoleGiftCardsOffline; // Gift Cards - allow working offline
extern NSString* const UserRoleLinkGiftCardsToCustomer; // Gift Cards - allow link gift card to customer
extern NSString* const UserRoleGiftCardOverrideSellAmount; // Gift Cards - allow to sell gift card on amount less then minimum or greater tham maximum
extern NSString* const UserRoleUseGiftCardWithoutPassword; // Gift Cards - allow to select a gift card from the payment dialog without requiring a customer password
extern NSString* const UserRoleStoreCreditOffline; // Store Credit - making offline transactions
// Tokens
extern NSString* const UserRoleTokenAdjustment; // Tokens - adjusting token of customer.
extern NSString* const UserRoleTokenRedeem; // Tokens - redeem.
extern NSString* const UserRoleTokenNegative; // Tokens - take balance to negative.
extern NSString* const UserRoleTokenOffline; // Tokens - process tokens in offline mode.

//Membership
extern NSString* const UserRoleSalesReceiptMembershipOverride;//apply a temporary membership on a sales receipt SALESRCPTS-MEMOVER
extern NSString* const UserRoleAccessCustomerMembershipLevel;//manually change the membership level OR Membership Code on the customer record CUSTOMER-MEMLEVEL
extern NSString* const UserRoleAccessCustomerMembershipExpiresDate;//manually change the expiration date on the customer record CUSTOMER-MEMEXP
extern NSString* const UserRoleAccessCustomerMembershipNewCode; //"CUSTOMER-MEMNEWCODE" – allows to change membership code if Customer does not have Membership Level
extern NSString* const UserRoleAccessCustomerMembershipChangeCode; //"CUSTOMER-MEMCHGCODE" – allows to change membership code if Customer HAS a Membership Level.
extern NSString* const UserCustomerHistoryReport; //"CUSTOMER-HISTORY" – print customer report 

// Frequent Buyer Program
extern NSString* const UserRoleFrequentBuyerAdjustment; // Frequent Buyer Program - balance adjustment

// House Charge Program
extern NSString* const UserRoleHouseChargeOffline; // House Charge - making offline transactions
extern NSString* const UserRoleHouseChargeChangeLimit; // House Charge settings changing


extern NSString* const UserRoleLRPCheckGCBalance; // LRP - allow to check Gift card balance received as award.

extern NSString* const ShipMemoAcces; // User may access and process Ship Memos.
extern NSString* const ShipMemoArchive; // User may archive Ship Memos

//Reject Appointments
extern NSString* const UserRoleRejectAppointments; // User may reject appointment
