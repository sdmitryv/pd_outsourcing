//
//  NSDate+Compare.h
//  iPadPOS
//
//  Created by valera on 3/24/11.
//  Copyright 2011 DBBestTechnologies. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface NSDate(Compare)
- (BOOL)isEqualToDay:(NSDate*)anotherDate;
- (BOOL)lessThanDate:(NSDate*)anotherDate;
- (BOOL)lessOrEqualToDate:(NSDate*)anotherDate;
- (BOOL)isFutureDate;
- (id)dateByAddingDays:(NSInteger)days;
- (id)dateOnly;
@end
