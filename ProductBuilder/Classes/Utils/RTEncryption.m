//
//  RTEncryption.m
//  ProductBuilder
//
//  Created by Valery Fomenko on 5/18/12.
//  Copyright (c) 2012 Cloudworks. All rights reserved.
//

#import "RTEncryption.h"
#import <CommonCrypto/CommonCryptor.h>
#import <CommonCrypto/CommonDigest.h>
#import "USAdditions.h"

NSString* const rtKey = @"Retail Teamwork, LLC";

@implementation RTEncryption

+ (NSData*)crypt3DES:(NSData*)data withKey:(NSData*)key operation:(CCOperation)operation
{
    unsigned char *input = (unsigned char*)[data bytes];
    NSUInteger inLength = [data length];
    NSInteger outLength = ((inLength + kCCBlockSize3DES) & ~(kCCBlockSize3DES - 1));
    unsigned char *output =(unsigned char *)calloc(outLength, sizeof(unsigned char));
    bzero(output, outLength*sizeof(unsigned char));
    size_t movedBytes = 0;
    NSMutableData* key3DES = [[NSMutableData alloc]init];
    while (key3DES.length < kCCKeySize3DES){
        [key3DES appendData:[key subdataWithRange:NSMakeRange(0,((key3DES.length + key.length) > kCCKeySize3DES) ? key3DES.length + key.length - kCCKeySize3DES : key.length)]];
    }

    CCCryptorStatus err = CCCrypt(operation, kCCAlgorithm3DES, 
                                  kCCOptionECBMode | kCCOptionPKCS7Padding,
                                  [key3DES bytes], 
                                  [key3DES length],
                                  0, input, inLength, output, outLength, &movedBytes);
    [key3DES release];
    if(err != kCCSuccess){
        free(output);
        return nil;
    }
    
    return [NSData dataWithBytesNoCopy:output length:movedBytes freeWhenDone:YES];
}

+(NSData*)getKeyDataFromKey:(NSString*)key useHash:(BOOL)useHash{
    NSData* keyData = nil;
    if (useHash){
        const char *keyBytes = [key UTF8String];
        unsigned char result[CC_MD5_DIGEST_LENGTH];
        CC_MD5(keyBytes, (CC_LONG)strlen(keyBytes), result);
        
        keyData = [NSData dataWithBytes:result length:CC_MD5_DIGEST_LENGTH];
    }
    else{
        keyData = [key dataUsingEncoding:NSUTF8StringEncoding];
    }
    return keyData;
}

+(NSString*)encrypt:(NSString*)string key:(NSString*)key useHash:(BOOL)useHash{
    NSData* resultData = [self crypt3DES:[string dataUsingEncoding:NSUTF8StringEncoding]withKey:[self getKeyDataFromKey:key useHash:useHash] operation:kCCEncrypt];
    return [resultData base64Encoding];
}

+(NSString*)decrypt:(NSString*)string key:(NSString*)key useHash:(BOOL)useHash{
    NSData* resultData = [self crypt3DES:[NSData dataWithBase64EncodedString:string]withKey:[self getKeyDataFromKey:key useHash:useHash] operation:kCCDecrypt];
    if (!resultData) return nil;
    return [[[NSString alloc] initWithBytes:[resultData bytes]
                                     length:[resultData length] encoding: NSUTF8StringEncoding]autorelease];
}

+(NSString*)encrypt:(NSString*)string{
    return [self encrypt:string key:rtKey useHash:TRUE];
}

+(NSString*)decrypt:(NSString*)string{
    return [self decrypt:string key:rtKey useHash:TRUE];
}

@end
