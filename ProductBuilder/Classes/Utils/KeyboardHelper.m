//
//  KeyboardHelper.m
//  StockCount
//
//  Created by Valera on 12/10/10.
//  Copyright 2010 DBBest Technologies. All rights reserved.
//

#import "KeyboardHelper.h"
#import "UIViewControllerExtended.h"
#import <QuartzCore/QuartzCore.h>
#import "UITextField+Input.h"
#import "UIView+FirstResponder.h"
#import "NumberpadInputViewController.h"
#import <objc/runtime.h>
#import <objc/message.h>

char* const updatingShiftStateKey = "updatingShiftState";

@implementation KeyboardHelper

UIView* inputAccessoryView;

-(id)init{
	if ((self = [super init])){
	}
    return self;
}

+(KeyboardHelper *)sharedInstance {
    static dispatch_once_t pred;
    static KeyboardHelper *instance = nil;
    
    dispatch_once(&pred, ^{
        instance = [[KeyboardHelper alloc] init];
    });
    return instance;
}

//+(UIView *)numberpadInputView {
//    return self.numberpadInputViewController.view;
//}
//
//+(NumberpadInputViewController *)numberpadInputViewController {
//    static dispatch_once_t pred;
//    static NumberpadInputViewController * numberpadInputViewController;
//    dispatch_once(&pred, ^{
//        numberpadInputViewController = [[NumberpadInputViewController alloc] init];
//    });
//    return numberpadInputViewController;
//}

BOOL shouldMinimizeForHWKeyboard(id self, SEL _cmd){
    [self setValue:@(0) forKey:@"m_hardwareKeyboardAttached"];
    return FALSE;
}

BOOL automaticMinimizationEnabled(id self, SEL _cmd){
    return FALSE;
}

void setInHWKeyboardMode(id self, SEL _cmd, BOOL arg){
    
}

void updateTouchProcessingForKeyplaneChange(id self, SEL _cmd){
    
}

IMP org_updateShiftState;
IMP org_notifyShiftState;
IMP org_displayImagesForViewFromLayoutImageFlags;

void notifyShiftState(id self, SEL _cmd){
    NSNumber* isUpdatingShiftStateKey = objc_getAssociatedObject(self, updatingShiftStateKey);
    if (!isUpdatingShiftStateKey || ([isUpdatingShiftStateKey isKindOfClass:NSNumber.class] && !isUpdatingShiftStateKey.boolValue)){
        return;
    }
    org_notifyShiftState(self, _cmd);
}

void updateShiftState(id self, SEL _cmd){
    objc_setAssociatedObject(self, updatingShiftStateKey, @(YES), OBJC_ASSOCIATION_RETAIN);
    org_updateShiftState(self, _cmd);
    objc_setAssociatedObject(self, updatingShiftStateKey, nil, OBJC_ASSOCIATION_RETAIN);
}



-(void)install{
    
    Class class = NSClassFromString(@"UIKeyboardImpl");
    SEL selectorToOverride = NSSelectorFromString(@"_shouldMinimizeForHardwareKeyboard");
    Method m = class_getInstanceMethod(class, selectorToOverride);
    class_replaceMethod(class, selectorToOverride, (IMP)shouldMinimizeForHWKeyboard, method_getTypeEncoding(m));
    
    selectorToOverride = NSSelectorFromString(@"automaticMinimizationEnabled");
    m = class_getInstanceMethod(class, selectorToOverride);
    class_replaceMethod(class, selectorToOverride, (IMP)automaticMinimizationEnabled, method_getTypeEncoding(m));
    
    selectorToOverride = NSSelectorFromString(@"setInHardwareKeyboardMode:");
    m = class_getInstanceMethod(class, selectorToOverride);
    class_replaceMethod(class, selectorToOverride, (IMP)setInHWKeyboardMode, method_getTypeEncoding(m));
    
    class = NSClassFromString(@"UIKeyboardLayoutStar");
    selectorToOverride = NSSelectorFromString(@"updateTouchProcessingForKeyplaneChange");
    m = class_getInstanceMethod(class, selectorToOverride);
    class_replaceMethod(class, selectorToOverride, (IMP)updateTouchProcessingForKeyplaneChange, method_getTypeEncoding(m));
    
    class = NSClassFromString(@"UIKeyboardImpl");
    selectorToOverride = NSSelectorFromString(@"notifyShiftState");
    m = class_getInstanceMethod(class, selectorToOverride);
    org_notifyShiftState = class_replaceMethod(class, selectorToOverride, (IMP)notifyShiftState, method_getTypeEncoding(m));
    
    selectorToOverride = NSSelectorFromString(@"updateShiftState");
    m = class_getInstanceMethod(class, selectorToOverride);
    org_updateShiftState = class_replaceMethod(class, selectorToOverride, (IMP)updateShiftState, method_getTypeEncoding(m));

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textFieldTextDidBeginEditingNotification:) name:UITextFieldTextDidBeginEditingNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(willEnterForeground) name:UIApplicationWillEnterForegroundNotification object:nil];
}

+(void)installNumpadKeyboardForTextField:(UITextField*)textField{

    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"10.0")) {
        return;
    }

    UIView* inputView = textField.inputView;
    NumberpadInputViewController * numberpadInputViewController = [[NumberpadInputViewController alloc] init];
    if (inputView && inputView.tag==9){
        //do nothing just save numpad keyboard as original input view
        objc_setAssociatedObject(textField, "inputViewTag", numberpadInputViewController.view, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    }
    else{
        textField.inputView = numberpadInputViewController.view;
    }
    objc_setAssociatedObject(textField, "numberpadInputViewControllerTag", numberpadInputViewController, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    [numberpadInputViewController release];
}

+(void)uninstallNumpadKeyboardForTextField:(UITextField*)textField{
    
    textField.inputView = nil;
    objc_setAssociatedObject(textField, "inputViewTag", nil, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    objc_setAssociatedObject(textField, "numberpadInputViewControllerTag", nil, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

+(void)install{
    [[[self class]sharedInstance]install];
}

- (void)dealloc {
	[[NSNotificationCenter defaultCenter] removeObserver:self];
	[super dealloc];
}

-(void) textFieldTextDidBeginEditingNotification: (NSNotification *) theNotification{
    UITextField *theTextField = [theNotification object];
    theTextField.inputAccessoryView = inputAccessoryView;
    NumberpadInputViewController * numberpadInputViewController = objc_getAssociatedObject(theTextField, "numberpadInputViewControllerTag");
    if (numberpadInputViewController != nil) {
        numberpadInputViewController.contents = theTextField.numpadKeyboardContent;
    }
    //    if (theTextField.inputView==[[self class]numberpadInputView]){
    //        [self class].numberpadInputViewController.contents = theTextField.numpadKeyboardContent;
    //    }
}

+ (UIView *)keyboardWindow
{
    // Locate non-UIWindow.
    UIView *keyboardWindow = nil;
    for (UIWindow *testWindow in [[UIApplication sharedApplication] windows]) {
        if ([[testWindow description] hasPrefix:@"<UITextEffectsWindow"]) {
            keyboardWindow = testWindow;
            break;
        }
    }
    if ( SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8")){
        for (UIView *view in [keyboardWindow subviews]) {
            if ([view.class.description hasPrefix:@"UIInputSetContainerView"]){
                keyboardWindow = view;
                break;
            }
        }
    }
    
    NSString* kbClassName = SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8") ?@"UIInputSetHostView" :  @"UIPeripheralHostView";
    for (UIView *possibleKeyboard in [keyboardWindow subviews]) {
        if ([possibleKeyboard.class.description hasPrefix:kbClassName]) {
            return possibleKeyboard;
            break;
        }
    }
    
    return nil;
}


-(void)willEnterForeground{
    #ifdef __IPHONE_8_0
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8")){
        UIResponder* responder = [[UIApplication sharedApplication].keyWindow getFirstResponder];
        if ([responder isKindOfClass:UITextView.class]){
            [responder reloadInputViews];
        }
    }
    #endif
}

@end
