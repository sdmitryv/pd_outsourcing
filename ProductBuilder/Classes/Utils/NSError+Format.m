//
//  NSError+Format.m
//  ProductBuilder
//
//  Created by valera on 1/4/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "NSError+Format.h"

@implementation NSError(Format)

-(NSString*)descriptionExtended{
    NSMutableString* description = [[NSMutableString alloc]initWithString:[self localizedDescription]];
    id object = [self userInfo][NSUnderlyingErrorKey];
    if (object && [object isKindOfClass:[NSError class]]){
        NSString* underlyingErrorDescription = [(NSError*)object descriptionExtended];
        if (![underlyingErrorDescription isEqualToString:description]){
            [description appendString:@"\n"];
            [description appendString:underlyingErrorDescription]; 
        }
    }
    return [description autorelease];
}

@end
