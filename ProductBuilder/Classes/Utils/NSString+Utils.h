//
//  NSString(Removing).h
//  ProductBuilder
//
//  Created by valera on 10/21/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString(Utils)

- (NSString *)stringByRemovingCharactersInSet:(NSCharacterSet*)characterSet;
- (NSString *)stringByReplacingCharactersInSet:(NSCharacterSet*)characterSet
                                    withString:(NSString*)string;
- (NSString *)stringByRemovingCharactersInSet:(NSCharacterSet*)characterSet inRange:(NSRange)range;
- (NSString *)stringByReplacingCharactersInSet:(NSCharacterSet*)characterSet
                                    withString:(NSString*)string inRange:(NSRange)range;
- (NSString *)stringByLeavingOnlyDigits;
- (NSMutableAttributedString*)stringByHighlightingString:(NSString*)highlightingString normalFont:(UIFont*)normalFont highlightFont:(UIFont*)highlightFont color:(UIColor*)color;
- (NSMutableAttributedString*)stringByHighlightingString:(NSString*)highlightingString normalFont:(UIFont*)normalFont highlightFont:(UIFont*)highlightFont color:(UIColor*)color backgroundColor:(UIColor*)backgroundColor;
- (NSString *)whitespaceReplacementWithSystemAttributes:(NSDictionary *)systemAttributes newAttributes:(NSDictionary *)newAttributes;
@end

@interface NSMutableString(NewLine)

- (void)appendStringOnNewLine:(NSString *)string;

@end
