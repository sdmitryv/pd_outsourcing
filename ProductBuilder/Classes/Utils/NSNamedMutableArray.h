//
//  NSNamedMutableArray.h
//  CloudworksPOS
//
//  Created by Lulakov Viacheslav on 7/6/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface NSNamedMutableArray : NSObject<NSFastEnumeration> {
    NSMutableArray  * data;
    NSString        * name;    
}

@property (nonatomic, copy) NSString    * name;
@property (nonatomic, readonly) NSMutableArray * data;
- (id)initWithArray:(NSArray*)array;
- (NSUInteger)count;
- (id)objectAtIndex:(NSUInteger)index;
- (id)objectAtIndexedSubscript:(NSUInteger)index;
- (void)addObject:(id)anObject;
- (void)addObjectsFromArray:(NSArray *)otherArray;

@end
