//
//  NSDictionary+CaseInsensitive.m
//  ProductBuilder
//
//  Created by valery on 4/3/14.
//  Copyright (c) 2014 Cloudworks. All rights reserved.
//

#import "NSDictionary+CaseInsensitive.h"

@interface NSMutableDictionary (CaseInsensitive)

// Returns a mutable equivalent to GTMNSDictionaryCaseInsensitiveAdditions.
- (id)initWithDictionaryCaseInsensitive:(NSDictionary *)dictionary;

@end

static Boolean CaseInsensitiveEqualCallback(const void *a, const void *b) {
    id idA = (id)a;
    id idB = (id)b;
    Boolean ret = FALSE;
    if ([idA isKindOfClass:[NSString class]] &&
        [idB isKindOfClass:[NSString class]]) {
        ret = ([idA compare:idB options:NSCaseInsensitiveSearch|NSLiteralSearch]
               == NSOrderedSame);
    } else {
        ret = [idA isEqual:idB];
    }
    return ret;
}

static CFHashCode CaseInsensitiveHashCallback(const void *value) {
    id idValue = (id)value;
    CFHashCode ret = 0;
    if ([idValue isKindOfClass:[NSString class]]) {
        ret = [[idValue lowercaseString] hash];
    } else {
        ret = [idValue hash];
    }
    return ret;
}

@implementation NSDictionary (CaseInsensitive)

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wobjc-designated-initializers"

- (id)initWithDictionaryCaseInsensitive:(NSDictionary *)dictionary {
    [self release];
    self = nil;
    
    CFIndex count = 0;
    void *keys = NULL;
    void *values = NULL;
    
    if (dictionary && ![dictionary isKindOfClass:[NSNull class]]) {
        count = CFDictionaryGetCount((CFDictionaryRef)dictionary);
        
        if (count) {
            keys = malloc(count * sizeof(void *));
            values = malloc(count * sizeof(void *));
            if (!keys || !values) {
                free(keys);
                free(values);
                return self;
            }
            
            CFDictionaryGetKeysAndValues((CFDictionaryRef)dictionary, keys, values);
        }
    }
    
    CFDictionaryKeyCallBacks keyCallbacks = kCFCopyStringDictionaryKeyCallBacks;
    keyCallbacks.equal = CaseInsensitiveEqualCallback;
    keyCallbacks.hash = CaseInsensitiveHashCallback;
    
    self = (NSDictionary*)CFDictionaryCreate(kCFAllocatorDefault,
                                                   keys, values, count, &keyCallbacks,
                                                   &kCFTypeDictionaryValueCallBacks);
    
    free(keys);
    free(values);
    
    return self;
}

+ (id)dictionaryWithDictionaryCaseInsensitive:(NSDictionary *)dictionary {
    return [[[self alloc] initWithDictionaryCaseInsensitive:dictionary] autorelease];
}

@end

@implementation NSMutableDictionary (GTMNSMutableDictionaryCaseInsensitiveAdditions)

- (id)initWithDictionaryCaseInsensitive:(NSDictionary *)dictionary {
    if ((self = [super initWithDictionaryCaseInsensitive:dictionary])) {
        id copy = (NSMutableDictionary*)CFDictionaryCreateMutableCopy(kCFAllocatorDefault, 0,(CFDictionaryRef)self);
        [self release];
        self = copy;
    }
    return self;
}

#pragma clang diagnostic pop


@end
