//
//  KeyboardHelper.h
//  StockCount
//
//  Created by Valera on 12/10/10.
//  Copyright 2010 DBBest Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "KBNotificationsHelper.h"

@interface KeyboardHelper : NSObject {
}

+(KeyboardHelper *)sharedInstance;
+(void)install;
+(void)installNumpadKeyboardForTextField:(UITextField*)textField;
+(void)uninstallNumpadKeyboardForTextField:(UITextField*)textField;
+ (UIView *)keyboardWindow;
@end
