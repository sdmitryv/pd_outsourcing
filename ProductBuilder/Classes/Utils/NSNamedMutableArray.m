//
//  NSNamedMutableArray.m
//  CloudworksPOS
//
//  Created by Lulakov Viacheslav on 7/6/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "NSNamedMutableArray.h"

@implementation NSNamedMutableArray

@synthesize name;
@synthesize data;

- (NSUInteger)count {
    return [data count];
}

- (id)objectAtIndex:(NSUInteger)index {
    return data[index];
}

- (id)objectAtIndexedSubscript:(NSUInteger)index{
    return data[index];
}

- (id)init {
	if((self = [super init])) {
		data = [[NSMutableArray alloc] init];
	}
	return self;
}

- (id)initWithArray:(NSArray*)array {
	if((self = [super init])) {
		data = [[NSMutableArray alloc] initWithArray:array];
	}
	return self;
}



- (NSUInteger)countByEnumeratingWithState:(NSFastEnumerationState *)state objects:(id __unsafe_unretained [])stackbuf count:(NSUInteger)len {
	return [data countByEnumeratingWithState:state objects:stackbuf count:len];
}

- (void)addObject:(id)anObject {
    [data addObject:anObject];
}

- (void)addObjectsFromArray:(NSArray *)otherArray {
    [data addObjectsFromArray:otherArray];
}

- (void)dealloc {
	[data release];
    [name release];
    [super dealloc];
}

@end
