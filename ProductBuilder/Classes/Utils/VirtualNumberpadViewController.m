//
//  VirtualNumberpadViewController.m
//  ProductBuilder
//
//  Created by Lulakov Viacheslav on 12/6/12.
//  Copyright (c) 2012 Cloudworks. All rights reserved.
//

#import "VirtualNumberpadViewController.h"
#import "UIView+FirstResponder.h"

@interface VirtualNumberpadViewController ()

@end

@implementation VirtualNumberpadViewController
@synthesize popoverController, textField;

- (id)initWithTextField:(UITextField *)aTextField
{
    self = [super initWithNibName:@"VirtualNumberpadView" bundle:nil];
    if (self) {
        _numpadKeyboardController = [[NumpadKeyboardController alloc] init];
        popoverController = [[RTPopoverController alloc] initWithContentViewController:self];
        self.textField = aTextField;
        popoverController.delegate = self;
    }
    return self;
}

-(void)setTextField:(UITextField *)aTextField{
    if (!aTextField) {
        textField = nil;
        popoverController.passthroughViews = nil;
        return;
    }
    
    textField = aTextField;
    popoverController.passthroughViews = @[aTextField];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [_keyboardView addSubview:_numpadKeyboardController.view];
    self.preferredContentSize = self.view.bounds.size;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc
{
    [_numpadKeyboardController release];
    [popoverController release];
    
    [_keyboardView release];
    
    [super dealloc];
}

#pragma mark - Actions

- (IBAction)cancelButtonClick
{
	id currentResponder = [self.view.window getFirstResponder];
    [self dismissPopover];
	if ([currentResponder isKindOfClass:[UITextField class]]) {
		//UITextField * textField = (UITextField *)currentResponder;
        if ([currentResponder canResignFirstResponder])
            [currentResponder resignFirstResponder];
//        if ((textField.delegate != nil) && ([textField.delegate respondsToSelector:@selector(textFieldShouldReturn:)])) {
//            [textField.delegate performSelector:@selector(textFieldShouldReturn:) withObject:textField];
//        }
    }
}

#pragma mark - Show/Hide popover

- (void)presentPopoverFromRect:(CGRect)rect inView:(UIView *)view permittedArrowDirections:(UIPopoverArrowDirection)arrowDirections animated:(BOOL)animated
{
	//force viewDidLoad
	if (self.view) {
		[popoverController presentPopoverFromRect:rect inView:view
                         permittedArrowDirections:arrowDirections animated:animated];
	}
}

- (void)dismissPopover {
    [popoverController dismissPopoverAnimated:NO];
}

#pragma mark - RTPopoverControllerDelegate methods

- (BOOL)popoverControllerShouldDismissPopover:(RTPopoverController *)popoverController
{
	return NO;
}

- (void)popoverControllerDidDismissPopover:(RTPopoverController *)popoverController
{
}

#pragma mark - Properties

- (void)setContents:(NumpadKeyboardContent)value
{
    _numpadKeyboardController.contents = value;
}

- (NumpadKeyboardContent)contents
{
    return _numpadKeyboardController.contents;
}

@end
