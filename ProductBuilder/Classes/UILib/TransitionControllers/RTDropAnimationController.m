//
//  RTDropAnimationController.m
//  ModalView.Test
//
//  Created by valery on 11/5/13.
//  Copyright (c) 2013 valera. All rights reserved.
//

#import "RTDropAnimationController.h"
#import <objc/runtime.h>

#ifndef __IPHONE_8_0
#define UIModalPresentationOverCurrentContext 6
#endif

#ifdef __IPHONE_7_0

@interface RTDimmingViewController : UIViewController{

}

@end

@implementation RTDimmingViewController

-(void)loadView{
    self.view = [[[UIView alloc]initWithFrame:CGRectZero]autorelease];
    self.view.backgroundColor = [UIColor blackColor];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
//    return (interfaceOrientation == UIInterfaceOrientationLandscapeLeft || interfaceOrientation == UIInterfaceOrientationLandscapeRight);
    return TRUE;
}

@end

@interface RTDropShadowView : UIView{
    BOOL defaultsInitited;
}

@end

@implementation RTDropShadowView

-(void)dealloc{
    [super dealloc];
}

+(void)initDefaults:(UIView*)view{
    view.layer.masksToBounds = NO;
    view.layer.shadowColor = [[UIColor blackColor] CGColor];
    view.layer.shadowOpacity = 0.2f;
    view.layer.shadowOffset = CGSizeMake(10, 10);
    view.layer.contentsScale = [UIScreen mainScreen].scale;
}

+(void)setShadow:(UIView*)view{
    UIRectCorner rectCorners = UIRectCornerAllCorners;
    CGFloat radius = 5.0f;
    UIBezierPath* beizerPath = [UIBezierPath bezierPathWithRoundedRect:view.bounds
                                                     byRoundingCorners:rectCorners
                                                           cornerRadii:CGSizeMake(radius, radius)];
    view.layer.shadowPath = beizerPath.CGPath;

}

-(void)initDefaults{
    if (defaultsInitited)
        return;
    [[self class] initDefaults:self];
    defaultsInitited = TRUE;
}

- (id)initWithFrame:(CGRect)frame
{
    if ((self = [super initWithFrame:frame])) {
        [self initDefaults];
    }
    return self;
}

-(id)initWithCoder:(NSCoder *)aDecoder{
    if ((self = [super initWithCoder:aDecoder])) {
        [self initDefaults];
    }
    return self;
}


-(void)layoutSubviews{
    [super layoutSubviews];
    [[self class]setShadow:self];
    
}
@end


@interface RTDropAnimationController(){
    RTDimmingViewController* dimmimgViewController;
    CGRect parentViewFrame;
}

@end
@implementation RTDropAnimationController

-(id)init{
    self = [super init];
    
    if(self){
        self.presentationDuration = 0.4f;
        self.dismissalDuration = 0.4f;
    }
    
    return self;
}

-(NSTimeInterval)transitionDuration:(id<UIViewControllerContextTransitioning>)transitionContext{
    return self.isPresenting ? self.presentationDuration : self.dismissalDuration;
}

-(void)animateTransition:(id<UIViewControllerContextTransitioning>)transitionContext{
    
    if(self.isPresenting){
        [self executePresentationAnimation:transitionContext];
    }
    else{
        [self executeDismissalAnimation:transitionContext];
    }
    
}

-(void)executePresentationAnimation:(id<UIViewControllerContextTransitioning>)transitionContext{
    
    UIView* inView = [transitionContext containerView];
    UIViewController* fromViewController = [transitionContext viewControllerForKey:UITransitionContextFromViewControllerKey];
    UIViewController* toViewController = [transitionContext viewControllerForKey:UITransitionContextToViewControllerKey];
    UIView* toView = toViewController.view;
    
    if (transitionContext.presentationStyle==UIModalPresentationFormSheet ||transitionContext.presentationStyle==UIModalPresentationPageSheet){
        if ([[[toViewController.view.superview class] description] hasPrefix:@"UIDropShadowView"]){
            toViewController.view = toViewController.view.superview;
            toViewController.view.autoresizingMask = UIViewAutoresizingNone;
        }
    }
    
    if (transitionContext.presentationStyle!=UIModalPresentationCustom){
        CGRect toViewControllerFinalFrame = [transitionContext finalFrameForViewController:toViewController];
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone){
                if (self.parentViewController){
                    if (self.parentViewController!=fromViewController){
                        for (UIView* v in inView.subviews){
                            [v removeFromSuperview];
                            [inView.superview addSubview:v];
                        }
                        [self.parentViewController.view addSubview:inView];
                        inView.frame  = self.parentViewController.view.bounds;
                        toViewControllerFinalFrame = self.parentViewController.view.bounds;
                    }
                    else{
                        toViewControllerFinalFrame = self.parentViewController.view.frame;
                        if (self.parentViewController==fromViewController){
                            parentViewFrame = self.parentViewController.view.frame;
                        }
                    }
                }
        }
        if (!CGRectEqualToRect(toViewControllerFinalFrame, CGRectZero)){
            toView.frame = toViewControllerFinalFrame;
        }
    }
    else{
        [dimmimgViewController.view removeFromSuperview];
        [dimmimgViewController release];
        dimmimgViewController = [[RTDimmingViewController alloc]init];
        dimmimgViewController.view.alpha = 0.0f;
        dimmimgViewController.view.frame = inView.bounds;
        dimmimgViewController.view.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        [inView addSubview:dimmimgViewController.view];
    }
    
    if (transitionContext.presentationStyle!=UIModalPresentationCurrentContext && transitionContext.presentationStyle!=UIModalPresentationOverCurrentContext && transitionContext.presentationStyle!=UIModalPresentationFullScreen
        && ![toView isKindOfClass:[RTDropShadowView class]]){
        toView.layer.cornerRadius = 5.0f;
        toView.layer.masksToBounds = TRUE;
    }
    
    if (toView.superview && ![toView.superview isKindOfClass:[RTDropShadowView class]]){
        //aply shadow to superview
        UIView* view = toView.superview;
        [RTDropShadowView initDefaults:view];
        [RTDropShadowView setShadow:view];
        view.backgroundColor = [UIColor clearColor];
        //fixes iOs8 beta bug
        view.bounds = toView.bounds;
        //prev statement changes toView bounds so revert it back
        toView.frame = view.bounds;
        toView = view;
    }
    
    if (transitionContext.presentationStyle==UIModalPresentationCustom && ![toView isKindOfClass:[RTDropShadowView class]] && !toView.superview){
        RTDropShadowView* dropShadowView = [[[RTDropShadowView alloc]init]autorelease];
        dropShadowView.transform = toView.transform;
        dropShadowView.frame = toView.frame;
        toView.transform = CGAffineTransformIdentity;
        toView.frame = toView.bounds;
        dropShadowView.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin;
        toView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
        [dropShadowView addSubview:toView];
        toView = dropShadowView;
        toViewController.view = toView;
    }
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad && transitionContext.presentationStyle!=UIModalPresentationCurrentContext && transitionContext.presentationStyle!=UIModalPresentationOverCurrentContext && transitionContext.presentationStyle!=UIModalPresentationFullScreen){
        while (toView.motionEffects.count) {
            [toView removeMotionEffect:toView.motionEffects[0]];
        }
        
        UIInterpolatingMotionEffect *verticalMotionEffect = [[UIInterpolatingMotionEffect alloc] initWithKeyPath:@"center.y" type:UIInterpolatingMotionEffectTypeTiltAlongHorizontalAxis];
        verticalMotionEffect.minimumRelativeValue = @(-10);
        verticalMotionEffect.maximumRelativeValue = @(10);
        
        UIInterpolatingMotionEffect *horizontalMotionEffect = [[UIInterpolatingMotionEffect alloc] initWithKeyPath:@"center.x" type:UIInterpolatingMotionEffectTypeTiltAlongVerticalAxis];
        horizontalMotionEffect.minimumRelativeValue = @(10);
        horizontalMotionEffect.maximumRelativeValue = @(-10);
        
        UIMotionEffectGroup *group = [UIMotionEffectGroup new];
        group.motionEffects = @[horizontalMotionEffect, verticalMotionEffect];
        [verticalMotionEffect release];
        [horizontalMotionEffect release];
        [toView addMotionEffect:group];
        [group release];
    }
    //[inView addSubview:toView];
    
    UIView* superviewOfInView = [inView superview];
    
    if (transitionContext.presentationStyle==UIModalPresentationCustom){
        CGPoint centerInView = inView.center;
        centerInView = [inView convertPoint:centerInView fromView:inView.superview];
        //it will be the final position
        toView.center = centerInView;
        //round coordinates to avoid blur
        CGRect finalFrame = toView.frame;
        finalFrame.origin.x = roundf(finalFrame.origin.x);
        finalFrame.origin.y = roundf(finalFrame.origin.y);
        toView.frame = finalFrame;
    }
    [inView addSubview:toView];
    //get the original transform
    CGAffineTransform completionTranslationTransform = toView.transform;
    
    CGRect screenBounds = [[UIScreen mainScreen] bounds];
    //screen size converted to view's transformation
    CGSize screenSize = CGSizeApplyAffineTransform(screenBounds.size,toView.transform);
    toView.transform = CGAffineTransformTranslate(toView.transform, 0,fabs(screenSize.height) - toView.frame.origin.y);
    void(^animations)(void) = ^{
        if (dimmimgViewController.view.superview){
            dimmimgViewController.view.alpha = 0.5f;
        }
        toView.transform = completionTranslationTransform;
    };
    void(^completion)(BOOL) = ^(BOOL finished){
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone){
            if (fromViewController.view.superview==inView){
                //[fromViewController.view removeFromSuperview];
                if (superviewOfInView==toViewController.view.superview){
                    [superviewOfInView insertSubview:fromViewController.view belowSubview:toViewController.view];
                }
                else{
                    [superviewOfInView insertSubview:fromViewController.view belowSubview:inView];
                }
            }
        }
        [transitionContext completeTransition:YES];
        UIView* fromView = fromViewController.view;
        if ([fromView.superview.class isKindOfClass:[RTDropShadowView class]]){
            fromView = fromView.superview;
        }
        if (fromView.superview==toView.superview){
            [toView.superview bringSubviewToFront:toView];
        }
    };
    if (transitionContext.presentationStyle==UIModalPresentationFullScreen || transitionContext.presentationStyle == UIModalPresentationCurrentContext ||transitionContext.presentationStyle == UIModalPresentationOverCurrentContext)
    {
        [UIView animateWithDuration:self.presentationDuration delay:0.0f options:UIViewAnimationOptionCurveEaseIn animations:animations completion:completion];
    }
    else{
        [UIView animateWithDuration:self.presentationDuration delay:0.0f usingSpringWithDamping:0.65f initialSpringVelocity:0.8f options:UIViewAnimationOptionCurveEaseIn animations:animations completion:completion];
    }
}

-(void)executeDismissalAnimation:(id<UIViewControllerContextTransitioning>)transitionContext{
    
    UIView* inView = [transitionContext containerView];
    UIViewController* toViewController = [transitionContext viewControllerForKey:UITransitionContextToViewControllerKey];
    UIViewController* fromViewController = [transitionContext viewControllerForKey:UITransitionContextFromViewControllerKey];
    UIView* fromView = fromViewController.view;
    UIView* toView = toViewController.view;
    if ([[fromView.superview description] hasPrefix:@"<UIDropShadow"] || [fromView.superview isKindOfClass:[RTDropShadowView class]]){
        fromView = fromView.superview;
    }
    UIView* superviewOfInView = [inView superview];
    
    if (!toView.superview){
        [inView insertSubview:toView belowSubview:fromView];
    }
    if (!CGRectEqualToRect(parentViewFrame, CGRectZero)){
        toViewController.view.frame = parentViewFrame;
    }
    
    CGRect screenBounds = [[UIScreen mainScreen] bounds];
    CGSize screenSize = CGSizeApplyAffineTransform(screenBounds.size,fromView.transform);
    CGAffineTransform completionTranslationTransform1 = CGAffineTransformTranslate(fromView.transform, 0,-20);
    
    CGAffineTransform completionTranslationTransform2 = CGAffineTransformTranslate(fromView.transform, 0,fabs(screenSize.height) - toView.frame.origin.y + 20);
    if (transitionContext.presentationStyle==UIModalPresentationFullScreen || transitionContext.presentationStyle == UIModalPresentationCurrentContext ||transitionContext.presentationStyle == UIModalPresentationOverCurrentContext){
        [UIView animateWithDuration:self.dismissalDuration delay:0.0f options:UIViewAnimationOptionCurveEaseIn animations:^{
            fromView.transform = completionTranslationTransform2;
        } completion:^(BOOL finished) {
            [transitionContext completeTransition:YES];
            if (!toView.superview){
                [superviewOfInView addSubview:toView];
            }
                         }];
    }
    else
        [UIView animateKeyframesWithDuration:self.dismissalDuration delay:0.0f options:UIViewKeyframeAnimationOptionCalculationModeLinear animations:^{
            
            [UIView addKeyframeWithRelativeStartTime:0 relativeDuration:1.0 animations:^{
                dimmimgViewController.view.alpha = 0.0;
            }];
            
            [UIView addKeyframeWithRelativeStartTime:0 relativeDuration:0.5 animations:^{
                fromView.transform = completionTranslationTransform1;
            }];
            
            [UIView addKeyframeWithRelativeStartTime:0.5 relativeDuration:0.5 animations:^{
                fromView.transform = completionTranslationTransform2;
        }];
        
        
    } completion:^(BOOL finished) {
        
        [transitionContext completeTransition:YES];
        [dimmimgViewController.view removeFromSuperview];
        if (!toView.superview){
            [superviewOfInView addSubview:toView];
        }
        if ([fromView isKindOfClass:RTDropShadowView.class] && fromView.subviews.count==1){
            fromViewController.view = fromView.subviews[0];
        }
    }];
}

- (void)dealloc {
    [dimmimgViewController release];
    [super dealloc];
}

@end

#endif