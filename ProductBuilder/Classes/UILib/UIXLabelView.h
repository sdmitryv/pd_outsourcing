//
//  UIXLabelView.h
//  GlassButtonTest
//
//  Created by valera on 10/31/11.
//  Copyright (c) 2011 Retail Teamwork Ukraine. All rights reserved.
//

#import <UIKit/UIKit.h>

//draws x sign using view's background color

@interface UIXLabelView : UIView{
    UIColor* labelColor;
}

@end
