//
//  UITextField+FormatCase.h
//  ProductBuilder
//
//  Created by Roman on 3/16/12.
//  Copyright (c) 2012 Cloudworks. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomRequiredField.h"

@interface UITextField (FormatCase)

@property (nonatomic, assign) ConvertEntry textCaseType;

@end
