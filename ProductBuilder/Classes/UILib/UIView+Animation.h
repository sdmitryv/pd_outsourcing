//
//  UIView+UIView_Animation.h
//  ProductBuilder
//
//  Created by valery on 5/7/14.
//  Copyright (c) 2014 Cloudworks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView(Animation)
-(BOOL)animateFading:(BOOL)fadeOut;
@end
