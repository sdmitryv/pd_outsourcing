//
//  SDCAlertView.h
//  SDCAlertView
//
//  Created by Scott Berrevoets on 9/20/13.
//  Copyright (c) 2013 Scotty Doesn't Code. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SDCAlertView;

@protocol SDCAlertViewDelegate <NSObject>
@optional

// Called when a button is clicked. The view will be automatically dismissed after this call returns
- (void)alertView:(SDCAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex;

// Called when we cancel a view (eg. the user clicks the Home button). This is not called when the user clicks the cancel button.
// If not defined in the delegate, we simulate a click in the cancel button
- (void)alertViewCancel:(SDCAlertView *)alertView;

- (void)willPresentAlertView:(SDCAlertView *)alertView;  // before animation and showing view
- (void)didPresentAlertView:(SDCAlertView *)alertView;  // after animation

- (void)alertView:(SDCAlertView *)alertView willDismissWithButtonIndex:(NSInteger)buttonIndex; // before animation and hiding view
- (void)alertView:(SDCAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex;  // after animation

// Called after edits in any of the default fields added by the style
- (BOOL)alertViewShouldEnableFirstOtherButton:(SDCAlertView *)alertView;

@end

@interface SDCAlertView : UIView

@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *message;

/*
 * SDCAlertView has a "bug" that was intentionally not duplicated in SDCAlertView.
 * This code:
 *
 *		SDCAlertView *alert = [[RTAlertView alloc] initWithTitle:@"Title" message:@"This is a message" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"B1", @"B2", nil];
 *		NSLog(@"%d", alert.firstOtherButtonIndex);
 *
 * will display 1 in the console, which is correct. When setting alert.cancelButtonIndex = 1, both cancelButtonIndex and firstOtherButtonIndex are 1 (B1).
 *
 * Though it doesn't make much sense, it's not a big deal. However, when returning NO from the delegate method -alertViewShouldEnableFirstOtherButton:, the
 * button with Cancel on it will be disabled. So, alert.firstOtherButtonIndex refers to B1, while the delegate method disables the button with title Cancel.
 *
 * That makes no sense, so when you do the same thing on SDCAlertView, firstOtherButtonIndex will return 0 and the delegate method will disable the Cancel button.
 * In other words, the former Cancel button now got demoted to a normal button at index 0.
 */

@property (nonatomic) NSInteger cancelButtonIndex;
@property (nonatomic, readonly) NSInteger firstOtherButtonIndex;
@property (nonatomic, readonly) NSInteger numberOfButtons;
@property (nonatomic, assign)NSInteger numberOfRows;
@property (nonatomic, readonly, getter = isVisible) BOOL visible;

@property (nonatomic) UIAlertViewStyle alertViewStyle;

/**
 *  The contentView property can be used to display any arbitrary view in an alert view by adding these views to the contentView.
 *  SDCAlertView uses auto-layout to layout all its subviews, including the contentView. That means that you should not modify
 *  the contentView's frame property, as it will do nothing. Use NSLayoutConstraint or helper methods included in SDCAutoLayout
 *  to modify the contentView's dimensions.
 *
 *  The contentView will take up the entire width of the alert. The height cannot be automatically determined and will need to be
 *  explicitly defined.
 *
 *  If there are no subviews, the contentView will not be added to the alert.
 */
@property (nonatomic, readonly) UIView *contentView;

@property (nonatomic, readonly) NSMutableArray *buttonTitles;

@property (nonatomic, weak) id <SDCAlertViewDelegate> delegate;

/*
 *  The following properties are blocks as alternatives to using delegate methods.
 *  It's possible to implement both the delegate and set its corresponding block. In
 *  that case, the delegate will be called before the block will be executed.
 *
 *  In the case of alertView:shouldDismissWithButtonIndex:/shouldDismissHandler and 
 *  alertView:shouldDeselectButtonAtIndex:/shouldDeselectButtonHandler, the
 *  delegate will always have precedence. That means that if the delegate is set,
 *  the block will NOT be executed.
 */

/// Alternative property for \c alertView:clickedButtonAtIndex:
@property (nonatomic, copy) void (^clickedButtonHandler)(NSInteger buttonIndex);

/// Alternative property for \c alertView:shouldDismissWithButtonIndex:
//@property (nonatomic, copy) BOOL (^shouldDismissHandler)(NSInteger buttonIndex);

/// Alternative property for \c alertView:willDismissWithButtonIndex:
@property (nonatomic, copy) void (^willDismissHandler)(NSInteger buttonIndex);

/// Alternative property for \c alertView:didDismissWithButtonIndex:
@property (nonatomic, copy) void (^didDismissHandler)(NSInteger buttonIndex);

/// Alternative property for \c alertView:shouldDeselectButtonAtIndex:
//@property (nonatomic, copy) BOOL (^shouldDeselectButtonHandler)(NSInteger buttonIndex);

@property (nonatomic, assign) CGFloat bottomSpacing;

- (instancetype)initWithTitle:(NSString *)title message:(NSString *)message delegate:(id)delegate cancelButtonTitle:(NSString *)cancelButtonTitle otherButtonTitle:(NSString *)otherButtonTitle;

- (instancetype)initWithTitle:(NSString *)title message:(NSString *)message delegate:(id)delegate cancelButtonTitle:(NSString *)cancelButtonTitle otherButtonTitles:(NSString *)otherButtonTitles, ... NS_REQUIRES_NIL_TERMINATION;

- (void)show;

/**
 *  Convenience method that sets the dismiss block while simultaneously showing the alert.
*/
- (void)showWithDismissHandler:(void(^)(NSInteger buttonIndex))dismissHandler;

- (NSInteger)addButtonWithTitle:(NSString *)title;
- (NSString *)buttonTitleAtIndex:(NSInteger)index;

- (void)dismissWithClickedButtonIndex:(NSInteger)buttonIndex animated:(BOOL)animated;

- (UITextField *)textFieldAtIndex:(NSInteger)textFieldIndex;

+(CGFloat)SDCAlertViewCornerRadius;
@end
