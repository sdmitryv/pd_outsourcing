//
//  SDCAlertViewContentView.m
//  SDCAlertView
//
//  Created by Scott Berrevoets on 11/5/13.
//  Copyright (c) 2013 Scotty Doesn't Code. All rights reserved.
//
#import "SDCAlertView.h"
#import "SDCAlertViewContentView.h"
#import "UIView+SDCAutoLayout.h"
#import "SDCAlertViewButton.h"
#import "SDCAlertViewCell.h"
#import "SDCAlertViewController.h"
#import <objc/runtime.h>
#import <objc/message.h>


@interface SDCAlertView(Private)

- (BOOL)alertContentViewShouldUseSecureEntryForPrimaryTextField:(SDCAlertViewContentView *)sender;
- (CGFloat)maximumHeightForAlertContentView:(SDCAlertViewContentView *)sender;
- (void)alertContentView:(SDCAlertViewContentView *)sender didTapButtonAtIndex:(NSUInteger)index;
- (BOOL)alertContentView:(SDCAlertViewContentView *)sender shouldEnableButtonAtIndex:(NSUInteger)index;
-(CGFloat)SDCAlertViewWidth;
-(UIEdgeInsets) SDCAlertViewPadding;
@end

@interface SDCAlertViewTextField : UITextField
@property (nonatomic) UIEdgeInsets textInsets;
@end

@interface SDCAlertViewContentView ()
@property (nonatomic, strong) UIScrollView *contentScrollView;
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UILabel *messageLabel;

@property (nonatomic, strong) UIView *textFieldBackgroundView;
@property (nonatomic, strong) SDCAlertViewTextField *primaryTextField;
@property (nonatomic, strong) UIView *textFieldSeparatorView;
@property (nonatomic, strong) SDCAlertViewTextField *secondaryTextField;

//@property (nonatomic, strong) UIView *buttonTopSeparatorView;
@property (nonatomic, strong) UIView *buttonSeparatorView;
@property (nonatomic, strong) UITableView *suggestedButtonTableView;
//@property (nonatomic, strong) UITableView *otherButtonsTableView;
@end

@implementation SDCAlertViewContentView

#pragma mark - Getter

- (NSArray *)textFields {
	NSArray *elements = [self alertViewElementsToDisplay];
	
	NSMutableArray *textFields = [NSMutableArray array];
	
	if ([elements containsObject:self.primaryTextField])	[textFields addObject:self.primaryTextField];
	if ([elements containsObject:self.secondaryTextField])	[textFields addObject:self.secondaryTextField];
	
	return textFields;
}

#pragma mark - Setters

- (void)setTitle:(NSString *)title {
	_title = title;
    if (!title){
        self.titleLabel.text = title;
    }
    else{
        NSMutableParagraphStyle *paragrahStyle = [[NSMutableParagraphStyle alloc] init];
        paragrahStyle.lineSpacing = 2.3;
        paragrahStyle.alignment = NSTextAlignmentCenter;
        NSAttributedString* string = [[NSAttributedString alloc]initWithString:title attributes:@{NSParagraphStyleAttributeName:paragrahStyle}];
        self.titleLabel.attributedText = string;
    }
    
}

- (void)setMessage:(NSString *)message {
	_message = message;
    if (!message){
        self.messageLabel.text = message;
    }
    else{
        NSMutableParagraphStyle *paragrahStyle = [[NSMutableParagraphStyle alloc] init];
        paragrahStyle.lineSpacing = 2.3;
        paragrahStyle.alignment = NSTextAlignmentCenter;
        NSAttributedString* string = [[NSAttributedString alloc]initWithString:message attributes:@{NSParagraphStyleAttributeName:paragrahStyle}];
        self.messageLabel.attributedText = string;
    }
}

#pragma mark - Initialization

- (instancetype)initAlertView:(SDCAlertView*)alertView{
	self = [super init];
	
	if (self) {
		_alertView = alertView;
        [self initLayer];
		[self initializeSubviews];
		
		[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textFieldTextChanged:) name:UITextFieldTextDidChangeNotification object:nil];
	}
	
	return self;
}

-(void)initLayer{
}


- (void)initializeSubviews {
	[self initializeTitleLabel];
	[self initializeMessageLabel];
	[self initializeContentScrollView];
	[self initializeTextFieldBackgroundView];
	[self initializePrimaryTextField];
	[self initializeTextFieldSeparatorView];
	[self initializeSecondaryTextField];
	[self initializeCustomContentView];
	[self initializeMainTableView];
}

- (void)initializeTitleLabel {
	self.titleLabel = [[UILabel alloc] init];
	[self.titleLabel setTranslatesAutoresizingMaskIntoConstraints:NO];
	self.titleLabel.font = [[self class] sdc_titleLabelFont];
    self.titleLabel.backgroundColor = [UIColor clearColor];
    self.titleLabel.textColor = [[self class] sdc_alertTitleColor];
	self.titleLabel.textAlignment = NSTextAlignmentCenter;
	self.titleLabel.numberOfLines = 0;
	self.titleLabel.preferredMaxLayoutWidth = self.alertView.SDCAlertViewWidth - self.class.SDCAlertViewContentPadding.left - self.class.SDCAlertViewContentPadding.right - self.alertView.SDCAlertViewPadding.right - self.alertView.SDCAlertViewPadding.left;
}

- (void)initializeMessageLabel {
	self.messageLabel = [[UILabel alloc] init];
	[self.messageLabel setTranslatesAutoresizingMaskIntoConstraints:NO];
	self.messageLabel.font = [[self class] sdc_messageLabelFont];
    self.messageLabel.backgroundColor = [UIColor clearColor];
    self.messageLabel.textColor = [[self class] sdc_alertMessageColor];
	self.messageLabel.textAlignment = NSTextAlignmentCenter;
	self.messageLabel.numberOfLines = 0;
    self.messageLabel.lineBreakMode = NSLineBreakByWordWrapping;
	self.messageLabel.preferredMaxLayoutWidth = self.alertView.SDCAlertViewWidth - self.class.SDCAlertViewContentPadding.left - self.class.SDCAlertViewContentPadding.right - self.alertView.SDCAlertViewPadding.right - self.alertView.SDCAlertViewPadding.left;
}

- (void)initializeContentScrollView {
	self.contentScrollView = [[UIScrollView alloc] init];
	[self.contentScrollView setTranslatesAutoresizingMaskIntoConstraints:NO];
}

- (void)initializeTextFieldBackgroundView {
	self.textFieldBackgroundView = [[UIView alloc] init];
	[self.textFieldBackgroundView setTranslatesAutoresizingMaskIntoConstraints:NO];
	self.textFieldBackgroundView.backgroundColor = [UIColor whiteColor];
	self.textFieldBackgroundView.layer.borderColor = [[[self class] sdc_textFieldBackgroundViewColor] CGColor];
	self.textFieldBackgroundView.layer.borderWidth = [[self class]SDCAlertViewGetSeparatorThickness];
	self.textFieldBackgroundView.layer.masksToBounds = YES;
	self.textFieldBackgroundView.layer.cornerRadius = [[self class]SDCAlertViewTextFieldBackgroundViewCornerRadius];
}

- (void)initializePrimaryTextField {
	self.primaryTextField = [[SDCAlertViewTextField alloc] init];
	[self.primaryTextField setTranslatesAutoresizingMaskIntoConstraints:NO];
	self.primaryTextField.font = [[self class] sdc_textFieldFont];
	self.primaryTextField.textInsets = [[self class]SDCAlertViewTextFieldTextInsets];
	self.primaryTextField.secureTextEntry = [self.alertView alertContentViewShouldUseSecureEntryForPrimaryTextField:self];
	//[self.primaryTextField becomeFirstResponder];
}

- (void)initializeTextFieldSeparatorView {
	self.textFieldSeparatorView = [self separatorView];
}


- (void)initializeCustomContentView {
	self.customContentView = [[UIView alloc] init];
	[self.customContentView setTranslatesAutoresizingMaskIntoConstraints:NO];
}

- (void)initializeSecondaryTextField {
	self.secondaryTextField = [[SDCAlertViewTextField alloc] init];
	[self.secondaryTextField setTranslatesAutoresizingMaskIntoConstraints:NO];
	self.secondaryTextField.font = [[self class] sdc_textFieldFont];
	self.secondaryTextField.textInsets = [[self class]SDCAlertViewTextFieldTextInsets];
	self.secondaryTextField.secureTextEntry = YES;
}

- (UIView *)separatorView {
	UIView *separatorView = [[UIView alloc] init];
	[separatorView setTranslatesAutoresizingMaskIntoConstraints:NO];
	separatorView.backgroundColor = [[self class] sdc_alertSeparatorColor];
	return separatorView;
}


- (UITableView *)buttonTableView {
	UITableView *tableView = [[UITableView alloc] init];
	[tableView setTranslatesAutoresizingMaskIntoConstraints:NO];
	tableView.backgroundColor = [UIColor clearColor];
    if (self.useSeparator){
        tableView.separatorInset = UIEdgeInsetsZero;
        tableView.separatorColor = [[self class] sdc_alertSeparatorColor];
        UIView* tableHeaderView = [[UIView alloc]init];
        tableHeaderView.backgroundColor = [[self class] sdc_alertSeparatorColor];
        tableHeaderView.bounds = (CGRect){.size = {.height = [[self class]SDCAlertViewGetSeparatorThickness]}};
        tableHeaderView.autoresizingMask = UIViewAutoresizingFlexibleWidth;
        //[tableHeaderView setTranslatesAutoresizingMaskIntoConstraints:NO];
        //[tableHeaderView sdc_pinHeight: self.class.SDCAlertViewGetSeparatorThickness];
        tableView.tableHeaderView = tableHeaderView;
    }
    else{
        tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    }
	tableView.scrollEnabled = NO;
    tableView.delegate = self;
	tableView.dataSource = self;
	return tableView;
}

- (void)initializeMainTableView {
	self.suggestedButtonTableView = [self buttonTableView];
}

#pragma mark geometry constraints

+(UIEdgeInsets)SDCAlertViewContentPadding{
    return (UIEdgeInsets){19, 15, 18.5, 15};
}

+(CGFloat)SDCAlertViewLabelSpacing{
    return 4.0f;
};

+(CGFloat)SDCAlertViewTextFieldBackgroundViewCornerRadius{
    return 5.0f;
};

+(UIEdgeInsets)SDCAlertViewTextFieldBackgroundViewPadding{
    return (UIEdgeInsets){20, 15, 0, 15};
}

+(UIEdgeInsets)SDCAlertViewTextFieldBackgroundViewInsets{
    return (UIEdgeInsets){0, 2, 0, 2};
}

+(UIEdgeInsets)SDCAlertViewTextFieldTextInsets{
    return (UIEdgeInsets){0, 4, 0, 4};
}

+(CGFloat)SDCAlertViewPrimaryTextFieldHeight{
    return 30;
};

+(CGFloat) SDCAlertViewSecondaryTextFieldHeight{
    return 29;
};


+(CGFloat) SDCAlertViewGetSeparatorThickness{
	return 1.0f / [[UIScreen mainScreen] scale];
}

#pragma  mark Colors and Fonts

+ (UIColor *)sdc_textFieldBackgroundViewColor {
	return [UIColor colorWithWhite:0.5 alpha:0.5];
}

+ (UIColor *)sdc_alertButtonTextColor {
	return [UIColor colorWithRed:0/255.0 green:122/255.0 blue:255/255.0 alpha:1];
}

+ (UIColor *)sdc_alertButtonDisabledTextColor {
	return [UIColor grayColor];
}

+ (UIColor *)sdc_alertMessageColor {
    return [UIColor blackColor];
}

+ (UIColor *)sdc_alertTitleColor {
    return [UIColor blackColor];
}

+ (UIColor *)sdc_alertSeparatorColor {
	return [UIColor colorWithWhite:0.5 alpha:0.5];
}

+ (UIFont *)sdc_titleLabelFont {
	return [UIFont boldSystemFontOfSize:17];
}

+ (UIFont *)sdc_messageLabelFont {
	return [UIFont systemFontOfSize:14];
}

+ (UIFont *)sdc_textFieldFont {
	return [UIFont systemFontOfSize:13];
}

+ (UIFont *)sdc_suggestedButtonFont {
	return [UIFont boldSystemFontOfSize:17];
}

+ (UIFont *)sdc_normalButtonFont {
	return [UIFont systemFontOfSize:17];
}

+(CGFloat)heightForButtonView:(BOOL)isDefault{
    return 44.0f;
}

#pragma mark - UITableViewDataSource/Delegate

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    [SDCAlertViewController removeAnimationsFromUIView:tableView];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    BOOL isDefaultButton = !self.showButtonInOneRow && indexPath.row== ([self.alertView.buttonTitles count] - 1);
    return [self.class heightForButtonView:isDefaultButton];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.showButtonInOneRow ? 1 : self.alertView.buttonTitles.count;
}

-(SDCAlertViewCell*)initializeAlertViewCellWithNumberOfButtons:(NSUInteger)buttonsCount defaultButtonIndex:(NSUInteger)defaultButtonIndex{
    return [[SDCAlertViewCell alloc] initWithNumberOfButtons:buttonsCount defaultButtonIndex:defaultButtonIndex reuseIdentifier:nil];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSUInteger buttonsCount = self.alertView.buttonTitles.count;
    //for one row buttons there is no default index at this moment because it has special insets
    SDCAlertViewCell *cell = [self initializeAlertViewCellWithNumberOfButtons:(self.showButtonInOneRow ? buttonsCount : 1) defaultButtonIndex:buttonsCount==1 || self.showButtonInOneRow ? NSNotFound : (buttonsCount - 1) == indexPath.row ? 0 : NSNotFound];
    if (cell.buttons.count > 1){
        for (NSInteger i =  0; i < cell.buttons.count; i++ ){
            [self prepareButton:cell.buttons[i] forButtonIndex:i];
        }
    }
    else if (cell.buttons.count == 1){
        NSUInteger rowIndex = indexPath.row;
        if ((buttonsCount - 1) == rowIndex){
            rowIndex = self.lastButtonIndex;
        }
        else if (indexPath.row >= self.lastButtonIndex){
            rowIndex++;
        }
        [self prepareButton:cell.buttons[0] forButtonIndex:rowIndex];
    }
	return cell;
}

-(NSUInteger)lastButtonIndex{
    if (!self.showButtonInOneRow){
        NSInteger cancelButtonIndex = self.alertView.cancelButtonIndex;
        if (cancelButtonIndex >=0 && cancelButtonIndex < self.alertView.buttonTitles.count)
            return cancelButtonIndex;
    }
    return self.alertView.buttonTitles.count - 1;
}

-(BOOL)useSeparator{
    return TRUE;
}

-(void)prepareButton:(SDCAlertViewButton*)button forButtonIndex:(NSUInteger)buttonIndex{
    button.enabled = [self.alertView alertContentView:self shouldEnableButtonAtIndex:buttonIndex];
    button.index = buttonIndex;
    [button addTarget:self action:@selector(buttonClick:) forControlEvents:UIControlEventTouchUpInside];
    [button setTitle:self.alertView.buttonTitles[buttonIndex] forState:UIControlStateNormal];
    if (buttonIndex== self.lastButtonIndex){
        button.titleLabel.font = [[self class] sdc_suggestedButtonFont];
        button.isDefault = TRUE;
    }
    else{
        button.titleLabel.font = [[self class] sdc_normalButtonFont];
    }
    button.backgroundColor = [UIColor clearColor];
    button.tintColor = [UIColor grayColor];
    [button setTitleColor:[[self class] sdc_alertButtonTextColor] forState:UIControlStateNormal];
    [button setTitleColor:[[self class] sdc_alertButtonDisabledTextColor] forState:UIControlStateDisabled];
    if (self.useSeparator && self.showButtonInOneRow && buttonIndex!=(self.alertView.buttonTitles.count-1)){
        UIView* deviderView = [[UIView alloc]init];
        deviderView.translatesAutoresizingMaskIntoConstraints = NO;
        deviderView.backgroundColor = [[self class] sdc_alertSeparatorColor];
        [button.superview addSubview:deviderView];
        [deviderView sdc_pinWidth:self.class.SDCAlertViewGetSeparatorThickness];
        [deviderView sdc_alignEdges:UIRectEdgeTop | UIRectEdgeBottom withView:button];
        [deviderView sdc_alignEdges:UIRectEdgeRight withView:button insets:UIEdgeInsetsMake(0, 0, 0, self.class.SDCAlertViewGetSeparatorThickness/2)];
    }
}

-(void)buttonClick:(SDCAlertViewButton*)button{
    self.alertView.userInteractionEnabled = NO;
	[self.alertView alertContentView:self didTapButtonAtIndex:button.index];
    self.alertView.userInteractionEnabled = YES;
}

#pragma mark - Layout

- (NSArray *)alertViewElementsToDisplay {
	NSMutableArray *elements = [NSMutableArray array];
	
	if ([self.titleLabel.text length] > 0)		[elements addObject:self.titleLabel];
	if ([self.messageLabel.text length] > 0)	[elements addObject:self.messageLabel];
	if ([elements count] > 0)					[elements addObject:self.contentScrollView];
	
	if (self.numberOfTextFields > 0) {
		[elements addObject:self.textFieldBackgroundView];
		[elements addObject:self.primaryTextField];
		
		if (self.numberOfTextFields == 2) {
			[elements addObject:self.textFieldSeparatorView];
			[elements addObject:self.secondaryTextField];
		}
	}
	
	if ([[self.customContentView subviews] count] > 0)		[elements addObject:self.customContentView];
	
	if ([self.alertView.buttonTitles count] > 0) {
		[elements addObject:self.suggestedButtonTableView];
	}
    
	return elements;
}

- (void)updateConstraints {
	NSArray *elements = [self alertViewElementsToDisplay];
	
	if ([elements containsObject:self.contentScrollView])			[self positionContentScrollView];
	if ([elements containsObject:self.textFieldBackgroundView])		[self positionTextFields];
	if ([elements containsObject:self.customContentView])			[self positionCustomContentView];
	if ([elements containsObject:self.suggestedButtonTableView])	[self positionButtons];
	
	[self positionAlertElements];
	
	[super updateConstraints];
}

#pragma mark - Custom Behavior

-(UIResponder*)firstResponder{
    if ([self.primaryTextField isFirstResponder])
        return self.primaryTextField;
    if ([self.secondaryTextField isFirstResponder])
        return self.secondaryTextField;
    if ([self.customContentView isFirstResponder])
        return self.customContentView;
    return nil;
}

-(BOOL)isFirstResponder{
    return [self.primaryTextField isFirstResponder] || [self.secondaryTextField isFirstResponder] || [self.customContentView isFirstResponder];
}

-(BOOL)canBecomeFirstResponder{
    return (self.primaryTextField.superview && [self.primaryTextField canBecomeFirstResponder]) || (self.secondaryTextField.superview && [self.secondaryTextField canBecomeFirstResponder]) || (self.customContentView.superview && (self.customContentView.superview && [self.customContentView canBecomeFirstResponder]));
}

-(BOOL)canResignFirstResponder{
    return [[self firstResponder] canResignFirstResponder];
}

- (BOOL)becomeFirstResponder {
    if (self.primaryTextField.superview && self.primaryTextField){
        return [self.primaryTextField becomeFirstResponder];
    }
    else if (self.secondaryTextField.superview && self.secondaryTextField){
        return [self.secondaryTextField becomeFirstResponder];
    }
    return self.customContentView.superview && [self.customContentView becomeFirstResponder];
}

- (BOOL)resignFirstResponder {
    [super resignFirstResponder];
    return [[self firstResponder] resignFirstResponder];
}


- (void)willMoveToSuperview:(UIView *)newSuperview {
	[super willMoveToSuperview:newSuperview];
	if (!newSuperview) return;
	NSArray *elements = [self alertViewElementsToDisplay];
	
	if ([elements containsObject:self.titleLabel])			[self.contentScrollView addSubview:self.titleLabel];
	if ([elements containsObject:self.messageLabel])		[self.contentScrollView addSubview:self.messageLabel];
	if ([elements containsObject:self.contentScrollView])	[self addSubview:self.contentScrollView];
	
	if ([elements containsObject:self.primaryTextField]) {
		[self addSubview:self.textFieldBackgroundView];
		[self.textFieldBackgroundView addSubview:self.primaryTextField];
		
		self.primaryTextField.secureTextEntry = [self.alertView alertContentViewShouldUseSecureEntryForPrimaryTextField:self];
		
		if ([elements containsObject:self.secondaryTextField]) {
			[self.textFieldBackgroundView addSubview:self.textFieldSeparatorView];
			[self.textFieldBackgroundView addSubview:self.secondaryTextField];
			
			self.primaryTextField.placeholder = NSLocalizedString(@"Login", nil);
			self.secondaryTextField.placeholder = NSLocalizedString(@"Password", nil);
		}
	}
	
	if ([elements containsObject:self.customContentView])
		[self addSubview:self.customContentView];
	
	if ([elements containsObject:self.suggestedButtonTableView]) {
		[self addSubview:self.suggestedButtonTableView];
	}
	
}

- (void)textFieldTextChanged:(NSNotification *)notification {
	if (notification.object == self.primaryTextField || notification.object == self.secondaryTextField)
		[self.suggestedButtonTableView reloadData];
}

- (void)dealloc {
	[[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - Content View Layout

- (void)positionContentScrollView {
	NSMutableString *verticalVFL = [@"V:|-(==topSpace)" mutableCopy];
	NSArray *elements = [self alertViewElementsToDisplay];
	
	CGFloat topSpace = self.class.SDCAlertViewContentPadding.top;
	if (![elements containsObject:self.titleLabel]) topSpace += self.class.SDCAlertViewLabelSpacing;
	
	if ([elements containsObject:self.titleLabel]) {
		[self.titleLabel sdc_pinWidthToWidthOfView:self.contentScrollView offset:-(self.class.SDCAlertViewContentPadding.left + self.class.SDCAlertViewContentPadding.right)];
		[self.titleLabel sdc_horizontallyCenterInSuperview];
		[verticalVFL appendString:@"-[titleLabel]"];
	}
	
	if ([elements containsObject:self.messageLabel]) {
		[self.messageLabel sdc_pinWidthToWidthOfView:self.contentScrollView offset:-(self.class.SDCAlertViewContentPadding.left + self.class.SDCAlertViewContentPadding.right)];
		[self.messageLabel sdc_horizontallyCenterInSuperview];
		
		if ([elements containsObject:self.titleLabel])
			[verticalVFL appendString:@"-(==labelSpace)"];
		
		[verticalVFL appendString:@"-[messageLabel]"];
	}
	
	[verticalVFL appendString:@"|"];
	
	NSDictionary *mapping = @{@"titleLabel": self.titleLabel, @"messageLabel": self.messageLabel};
	NSDictionary *metrics = @{@"topSpace": @(topSpace), @"labelSpace": @(self.class.SDCAlertViewLabelSpacing)};
	[self.contentScrollView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:verticalVFL options:0 metrics:metrics views:mapping]];
}

- (void)positionTextFields {
	NSDictionary *mapping = @{@"primaryTextField": self.primaryTextField, @"textFieldSeparator": self.textFieldSeparatorView, @"secondaryTextField": self.secondaryTextField};
	NSDictionary *metrics = @{@"primaryTextFieldHeight": @(self.class.SDCAlertViewPrimaryTextFieldHeight), @"secondaryTextFieldHeight": @(self.class.SDCAlertViewSecondaryTextFieldHeight), @"separatorHeight": @(self.class.SDCAlertViewGetSeparatorThickness)};
	
	UIEdgeInsets insets = self.class.SDCAlertViewTextFieldBackgroundViewInsets;
	insets.right = -insets.right;
	
	[self.primaryTextField sdc_pinWidthToWidthOfView:self.textFieldBackgroundView offset:insets.left + insets.right];
	[self.primaryTextField sdc_horizontallyCenterInSuperview];
	
	NSMutableString *verticalVFL = [@"V:|[primaryTextField(==primaryTextFieldHeight)]" mutableCopy];
	
	if ([[self alertViewElementsToDisplay] containsObject:self.secondaryTextField]) {
		[self.secondaryTextField sdc_pinWidthToWidthOfView:self.textFieldBackgroundView offset:insets.left + insets.right];
		[self.secondaryTextField sdc_horizontallyCenterInSuperview];
		
		[self.textFieldSeparatorView sdc_pinHeight:self.class.SDCAlertViewGetSeparatorThickness];
		[self.textFieldSeparatorView sdc_alignEdges:UIRectEdgeLeft|UIRectEdgeRight withView:self.textFieldBackgroundView];
		[self.textFieldSeparatorView sdc_alignEdges:UIRectEdgeTop withView:self.secondaryTextField];
		
		[verticalVFL appendString:@"[secondaryTextField(==secondaryTextFieldHeight)]"];
	}
	
	[verticalVFL appendString:@"|"];
	[self.textFieldBackgroundView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:verticalVFL options:0 metrics:metrics views:mapping]];
}

- (void)positionCustomContentView {
	[self.customContentView sdc_pinWidthToWidthOfView:self];
	[self.customContentView sdc_horizontallyCenterInSuperview];
}

- (void)setNumberOfRows:(NSInteger)numberOfRows
{
    if (_numberOfRows == numberOfRows) return;
	_numberOfRows = numberOfRows;
}


- (BOOL)showButtonInOneRow {
    return self.alertView.buttonTitles.count < 2 || (self.alertView.buttonTitles.count == 2 && _numberOfRows < 2);
}

-(CGFloat)tableViewHeight{
    if (!self.alertView.buttonTitles.count) return 0.0f;
    return self.showButtonInOneRow ? [self.class heightForButtonView:FALSE] : [self.class heightForButtonView:FALSE]*(self.alertView.buttonTitles.count - 1) + [self.class heightForButtonView:TRUE] + (self.useSeparator ? self.class.SDCAlertViewGetSeparatorThickness : 0);
}

- (void)positionButtons {
    [self.suggestedButtonTableView sdc_pinHeight:self.tableViewHeight];
    [self.suggestedButtonTableView sdc_alignEdges:UIRectEdgeLeft|UIRectEdgeRight withView:self];
}

- (CGFloat)heightForContentScrollView {
	NSArray *elements = [self alertViewElementsToDisplay];
	
	CGFloat scrollViewHeight = self.class.SDCAlertViewContentPadding.top + [self.titleLabel intrinsicContentSize].height + [self.messageLabel intrinsicContentSize].height;
	if ([[self alertViewElementsToDisplay] containsObject:self.messageLabel])	scrollViewHeight += self.class.SDCAlertViewLabelSpacing;
    
	CGFloat maximumScrollViewHeight = [self.alertView maximumHeightForAlertContentView:self] - self.class.SDCAlertViewContentPadding.bottom;
	if ([elements containsObject:self.suggestedButtonTableView])
		maximumScrollViewHeight -= self.tableViewHeight;
	
	if ([elements containsObject:self.primaryTextField])
		maximumScrollViewHeight -= (self.class.SDCAlertViewTextFieldBackgroundViewPadding.top + self.class.SDCAlertViewTextFieldBackgroundViewPadding.bottom + self.class.SDCAlertViewPrimaryTextFieldHeight);
	
	return MIN(scrollViewHeight, maximumScrollViewHeight);
}

- (void)positionAlertElements {
	NSArray *elements = [self alertViewElementsToDisplay];
    if (!elements.count) return;
	NSMutableString *verticalVFL = [@"V:|" mutableCopy];
    BOOL hasContentOtherThanButtons = NO;
	
	if ([elements containsObject:self.contentScrollView]) {
		[self.contentScrollView sdc_pinHeight:[self heightForContentScrollView]];
		[self.contentScrollView sdc_alignEdges:UIRectEdgeLeft|UIRectEdgeRight withView:self];
		
		[verticalVFL appendString:@"[scrollView]"];
        hasContentOtherThanButtons = YES;
	}
	
	if ([elements containsObject:self.textFieldBackgroundView]) {
		UIEdgeInsets insets = self.class.SDCAlertViewTextFieldBackgroundViewPadding;
		insets.right = -insets.right;
		
		[self.textFieldBackgroundView sdc_alignEdges:UIRectEdgeLeft|UIRectEdgeRight withView:self insets:insets];
		[verticalVFL appendString:@"-(==textFieldBackgroundViewTopSpacing)-[textFieldBackgroundView]"];
        hasContentOtherThanButtons = YES;
	}
	
	if ([elements containsObject:self.customContentView]) {
		[verticalVFL appendString:@"-[customContentView]"];
        hasContentOtherThanButtons = YES;
	}
	
    if ([elements containsObject:self.suggestedButtonTableView]) {
        if (hasContentOtherThanButtons){
            [verticalVFL appendString:@"-(==bottomSpacing)-[suggestedButtonTableView]"];
        }
        else{
            [verticalVFL appendString:@"-[suggestedButtonTableView]"];
        }
    }
    else{
        if (hasContentOtherThanButtons){
            //use topSpace instead of bottom to center conent
            [verticalVFL appendString:@"-(==topSpacing)-"];
        }
    }

    [verticalVFL appendString:@"|"];
	
	NSDictionary *metrics = @{@"textFieldBackgroundViewTopSpacing": @(self.class.SDCAlertViewTextFieldBackgroundViewPadding.top), @"bottomSpacing": @(self.class.SDCAlertViewContentPadding.bottom), @"topSpacing": @(self.class.SDCAlertViewContentPadding.top)};
	
    NSDictionary *views = @{@"scrollView": self.contentScrollView, @"textFieldBackgroundView": self.textFieldBackgroundView, @"customContentView": self.customContentView, @"suggestedButtonTableView": self.suggestedButtonTableView};
    
	[self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:verticalVFL options:0 metrics:metrics views:views]];
}

@end

@implementation SDCAlertViewTextField

- (CGRect)textRectForBounds:(CGRect)bounds {
	return [super textRectForBounds:UIEdgeInsetsInsetRect(bounds, self.textInsets)];
}

- (CGRect)editingRectForBounds:(CGRect)bounds {
	return [super textRectForBounds:UIEdgeInsetsInsetRect(bounds, self.textInsets)];
}

@end
