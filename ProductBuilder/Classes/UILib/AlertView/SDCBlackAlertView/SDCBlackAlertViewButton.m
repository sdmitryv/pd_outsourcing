//
//  SDCBlackAlertViewButton.m
//  SDCAlertView
//
//  Created by valery on 2/14/14.
//  Copyright (c) 2014 Scotty Doesn't Code. All rights reserved.
//

#import "SDCBlackAlertViewButton.h"

@implementation SDCBlackAlertViewButton

//-(UIEdgeInsets)insets{
//    return
//    self.isDefault ? (UIEdgeInsets){.top = 15,.left = 4,.bottom = 5,.right = 4} :
//    (UIEdgeInsets){.top = 3,.left = 4,.bottom = 3,.right = 4};
//}

//+(UIImage*)normalBackgroundImage{
//    static UIImage* normalBackgroundImage = nil;
//    static dispatch_once_t pred;
//    dispatch_once(&pred, ^{
//        normalBackgroundImage = [self.class resizableImage:[UIImage imageNamed:@"UIPopupAlertSheetButton.png"]];
//    });
//    return normalBackgroundImage;
//}
//
//+(UIImage*)highlightedBackgroundImage{
//    static UIImage* highlightedBackgroundImage = nil;
//    static dispatch_once_t pred;
//    dispatch_once(&pred, ^{
//        highlightedBackgroundImage = [self.class resizableImage:[UIImage imageNamed:@"UIPopupAlertSheetButtonPress.png"]];
//    });
//    return highlightedBackgroundImage;
//}

//
//+(UIImage*)normalDefaultBackgroundImage{
//    static UIImage* normalDefaultBackgroundImage = nil;
//    static dispatch_once_t pred;
//    dispatch_once(&pred, ^{
//        normalDefaultBackgroundImage = [self.class resizableImage:[UIImage imageNamed:@"UIPopupAlertSheetDefaultButton.png"]];
//    });
//    return normalDefaultBackgroundImage;
//}


+(UIImage*)highlightedBackgroundImage{
    static UIImage* highlightedBackgroundImage = nil;
    static dispatch_once_t pred;
    dispatch_once(&pred, ^{
        highlightedBackgroundImage = [self.class imageWithColor:[UIColor colorWithRed:50/255.0 green:50/255.0 blue:50/255.0 alpha:1] size:CGSizeMake(1, 1)];
    });
    return highlightedBackgroundImage;
}


+ (UIImage*)resizableImage:(UIImage*)image
{
    const CGFloat capWidth = image.size.width  /  2;
    const CGFloat capHeight = image.size.height / 2;
    UIEdgeInsets capInsets = UIEdgeInsetsMake(capHeight, capWidth, capHeight, capWidth);
    return [image resizableImageWithCapInsets:capInsets];
}

-(void)initializeDefaults{
    [super initializeDefaults];
    self.alpha = 0.7;
}


@end
