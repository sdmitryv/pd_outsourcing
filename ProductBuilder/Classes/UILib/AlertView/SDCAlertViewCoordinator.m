//
//  SDCAlertViewCoordinator.m
//  SDCAlertView
//
//  Created by Scott Berrevoets on 1/25/14.
//  Copyright (c) 2014 Scotty Doesn't Code. All rights reserved.
//
#import "SDCAlertView.h"
#import "SDCAlertViewCoordinator.h"
#import "SDCAlertViewController.h"
#import <objc/runtime.h>

@interface SDCAlertWindow : UIWindow

@end

@implementation SDCAlertWindow

@end

@interface UIWindow(level)

@end

@implementation UIWindow(level)

+(void)load{
    Class class = [self class];
    Method origMethod = class_getInstanceMethod(class,  @selector(setWindowLevel:));
    Method newMethod = class_getInstanceMethod(class,  @selector(setWindowLevelHack:));
    method_exchangeImplementations(origMethod, newMethod);
}

-(void)setWindowLevelHack:(UIWindowLevel)windowLevel{
    if (windowLevel >= 10000000 && ![self isKindOfClass:SDCAlertWindow.class]){
        windowLevel = 10000000 - 1;
    }
    [self setWindowLevelHack:windowLevel];
}
@end



@interface SDCAlertView (Private)
- (void)willBePresented;
- (void)wasPresented;
- (void)willBeDismissedWithButtonIndex:(NSInteger)buttonIndex;
- (void)wasDismissedWithButtonIndex:(NSInteger)buttonIndex;
@end

@interface SDCAlertViewCoordinator ()
@property (nonatomic, strong) UIWindow *userWindow;
@property (nonatomic, strong) UIWindow *alertWindow;
@property (nonatomic, strong) NSMutableArray *alerts;
//@property (nonatomic, weak) SDCAlertView *visibleAlert;
@end

@implementation SDCAlertViewCoordinator

- (UIWindow *)alertWindow {
	if (!_alertWindow) {
		_alertWindow = [[SDCAlertWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
		_alertWindow.backgroundColor = [UIColor clearColor];
        SDCAlertViewController* controller = [SDCAlertViewController currentController];
		_alertWindow.rootViewController = controller;
        if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"9")) {
            _alertWindow.windowLevel =  10000000;
        }
        else {
            _alertWindow.windowLevel =  UIWindowLevelAlert;
        }
	}
	
	return _alertWindow;
}

- (NSMutableArray *)alerts {
	if (!_alerts)
		_alerts = [NSMutableArray array];
	return _alerts;
}

- (id)init {
	self = [super init];
	
	if (self)
		_userWindow = [[UIApplication sharedApplication] keyWindow];
	
	return self;
}

+ (instancetype)sharedCoordinator {
	static SDCAlertViewCoordinator *sharedCoordinator;
	static dispatch_once_t onceToken;
	dispatch_once(&onceToken, ^{
		sharedCoordinator = [[self alloc] init];
	});
	
	return sharedCoordinator;
}

-(SDCAlertView*)visibleAlert{
    return [self.alerts lastObject];
}

- (void)presentAlert:(SDCAlertView *)alert {
    SDCAlertView *oldAlert = [self.alerts lastObject];
    if (oldAlert==alert) return;
    NSInteger idx = [self.alerts indexOfObject:alert];
    if (idx!=NSNotFound){
        [self.alerts removeObjectAtIndex:idx];
    }
    [self.alerts addObject:alert];
	
	[alert willBePresented];
    [self makeAlertWindowKeyWindow];
	[self showAlert:[self.alerts lastObject] replacingAlert:self.alerts.count>1 ?  self.alerts[self.alerts.count - 2] : nil animated:TRUE showNewCompletion:^{
        [alert wasPresented];
    } hideOldCompletion:nil];
}

- (void)dismissAlert:(SDCAlertView *)alert withButtonIndex:(NSInteger)buttonIndex animated:(BOOL)animated{
	[self.alerts removeObject:alert];
	[alert willBeDismissedWithButtonIndex:buttonIndex];
    
    [self showAlert:[self.alerts lastObject] replacingAlert:alert animated:animated showNewCompletion:nil hideOldCompletion:^{
        [alert wasDismissedWithButtonIndex:buttonIndex];
    }];
}

- (void)showAlert:(SDCAlertView *)newAlert replacingAlert:(SDCAlertView *)oldAlert animated:(BOOL)animated showNewCompletion:(void (^)(void))showNewCompletionHandler hideOldCompletion:(void (^)(void))hideOldCompletion{
	SDCAlertViewController *alertViewController = [SDCAlertViewController currentController];
	[alertViewController replaceAlert:oldAlert
							withAlert:newAlert
                             animated:animated
					  showDimmingView:newAlert != nil
					hideOldCompletion:^{
                        if (hideOldCompletion){
                            hideOldCompletion();
                        }
                        if (!self.alerts.count){
							[self returnToUserWindow];
                        }
                    }
					showNewCompletion:^{
                        if (showNewCompletionHandler){
                            showNewCompletionHandler();
                        }
					}];
}

- (void)resaturateUI {

}

- (void)makeAlertWindowKeyWindow {
    if (self.alertWindow.isKeyWindow) return;
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7")){
	    self.userWindow.tintAdjustmentMode = UIViewTintAdjustmentModeDimmed;
    }
	[self.alertWindow makeKeyAndVisible];
}

- (void)returnToUserWindow {
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7")){
  	    self.userWindow.tintAdjustmentMode = UIViewTintAdjustmentModeAutomatic;
    }
	[self.userWindow makeKeyAndVisible];
    self.alertWindow.rootViewController = nil;
	self.alertWindow = nil;
}

@end
