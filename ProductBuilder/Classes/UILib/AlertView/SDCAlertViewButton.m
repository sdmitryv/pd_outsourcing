//
//  SDCAlertViewButton.m
//  SDCAlertView
//
//  Created by valery on 2/13/14.
//  Copyright (c) 2014 Scotty Doesn't Code. All rights reserved.
//

#import "SDCAlertViewButton.h"
#import "UIView+SDCAutoLayout.h"

@interface SDCAlertViewButton(){
    BOOL highlightedDelay;
}

@end
@implementation SDCAlertViewButton

-(id)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self){
        [self initializeDefaults];
    }
    return self;
}

-(id)init{
    self = [super init];
    if (self){
        [self initializeDefaults];
    }
    return self;
}

-(void)setIsDefault:(BOOL)value{
    if (_isDefault==value) return;
    _isDefault = value;
    if (_isDefault){
        [self setBackgroundImage:self.class.normalDefaultBackgroundImage forState:UIControlStateNormal];
        [self setBackgroundImage:self.class.highlightedDefaultBackgroundImage forState:UIControlStateHighlighted];

    }
}

-(UIEdgeInsets)insets{
    return (UIEdgeInsets){0,0,0,0};
}

-(void)initializeDefaults{
    self.exclusiveTouch = YES;
    [self setBackgroundImage:self.class.normalBackgroundImage forState:UIControlStateNormal];
    [self setBackgroundImage:self.class.highlightedBackgroundImage forState:UIControlStateHighlighted];
}

-(void)setHighlighted:(BOOL)highlighted{
    if (self.highlighted ==highlighted) return;
    if (!highlighted && !highlightedDelay){
        highlightedDelay = TRUE;
        [super performSelector:@selector(setHighlighted:) withObject:@(highlighted) afterDelay:0.5f];
    }
    else{
        highlightedDelay = FALSE;
        [super setHighlighted:highlighted];
    }
}

+(UIImage*)normalDefaultBackgroundImage{
    return nil;
}

+(UIImage*)highlightedDefaultBackgroundImage{
    return self.class.highlightedBackgroundImage;
}

+(UIImage*)normalBackgroundImage{
    return nil;
}

+(UIImage*)highlightedBackgroundImage{
    static UIImage* highlightedBackgroundImage = nil;
    static dispatch_once_t pred;
    dispatch_once(&pred, ^{
        highlightedBackgroundImage = [self.class imageWithColor:[UIColor colorWithRed:217/255.0 green:217/255.0 blue:217/255.0 alpha:1] size:CGSizeMake(1, 1)];
    });
    return highlightedBackgroundImage;
}


+ (UIImage *)imageWithColor:(UIColor *)color size:(CGSize)size
{
    UIGraphicsBeginImageContext(size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(context, color.CGColor);
    CGContextFillRect(context, (CGRect){.size = size});
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}

@end
