//
//  SDCAlertViewContentView.h
//  SDCAlertView
//
//  Created by Scott Berrevoets on 11/5/13.
//  Copyright (c) 2013 Scotty Doesn't Code. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SDCAlertView;

@interface SDCAlertViewContentView : UIView <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, assign)SDCAlertView* alertView;
@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *message;
@property (nonatomic) NSInteger numberOfTextFields;
@property (nonatomic, readonly) NSArray *textFields;
@property (nonatomic, strong) UIView *customContentView;
@property (nonatomic, assign)NSInteger numberOfRows;
@property (nonatomic, readonly)BOOL showsTableViewsSideBySide;

- (instancetype)initAlertView:(SDCAlertView*)alertView;

@end
