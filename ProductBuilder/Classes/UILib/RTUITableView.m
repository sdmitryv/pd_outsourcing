//
//  RTTableView.m
//  ProductBuilder
//
//  Created by Alexander Martyshko on 7/16/15.
//  Copyright (c) 2015 Cloudworks. All rights reserved.
//

#import "RTUITableView.h"
#import "Global.h"

@interface RTUITableView () {
    BOOL keyboardIsShown;
    CGRect lastKeyboardRect;
}

@end



@implementation RTUITableView

- (void)awakeFromNib {
    [super awakeFromNib];
    [self initDefaults];
}

- (id)init {
    self = [super init];
    if (self) {
        [self initDefaults];
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self initDefaults];
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame style:(UITableViewStyle)style {
    self = [super initWithFrame:frame style:style];
    if (self) {
        [self initDefaults];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self initDefaults];
    }
    return self;
}

- (void)initDefaults {
    self.separatorColor = MO_RGBCOLOR(192, 192, 192);
}

- (void)didMoveToSuperview{
    [super didMoveToSuperview];
    [self removeObservers];
    if (!self.superview) return;
    // register for keyboard notifications
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardDidShow:)
                                                 name:UIKeyboardDidShowNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
}

- (void)willMoveToSuperview:(UIView *)newSuperview{
    [super willMoveToSuperview:nil];
//    self.separatorStyle = UITableViewCellSeparatorStyleNone;
    //self.layoutMargins = UIEdgeInsetsZero;
    //self.separatorInset = UIEdgeInsetsZero;
    //self.tableFooterView = [[UIView new] autorelease];
    if (!newSuperview){
        [self removeObservers];
    }
}

-(void)removeObservers{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

- (void)dealloc {
    [self removeObservers];
    [super dealloc];
}

- (void)keyboardWillHide:(NSNotification *)n{
    if (!keyboardIsShown) {
        return;
    }
    //restore originals
    self.contentInset = UIEdgeInsetsMake(0, 0, 0, 0);
    lastKeyboardRect = CGRectZero;
    keyboardIsShown = NO;
}

- (void)keyboardDidShow:(NSNotification *)n
{
    NSDictionary* userInfo = [n userInfo];
    
    // get the size of the keyboard
    CGRect keyboardEndFrame;
    [userInfo[UIKeyboardFrameEndUserInfoKey] getValue:&keyboardEndFrame];
    
    CGRect keyboardRect = [self.superview convertRect:keyboardEndFrame fromView:nil];
    
    UIViewController *appRootVC = [[UIApplication sharedApplication].windows[0] rootViewController];
    CGRect keyboardRectInRootView = [appRootVC.view convertRect:keyboardEndFrame fromView:nil];
    if (keyboardRectInRootView.origin.y >= appRootVC.view.bounds.size.height-1) {
        return;
    }
    
    lastKeyboardRect = keyboardRect;
    
    if (CGRectIntersectsRect(self.frame, keyboardRect)) {
        [UIView animateWithDuration:0.3f animations:^{
            [self adjustToKeyboardRect:keyboardRect];
        }];
        keyboardIsShown = YES;
    }
}

-(void)adjustToKeyboardRect:(CGRect)keyboardRect{
    if (!CGRectIntersectsRect(self.frame, keyboardRect)) return;
    CGFloat deltaY = (self.frame.origin.y + self.frame.size.height) - keyboardRect.origin.y + self.additionalOffset;
    self.contentInset = UIEdgeInsetsMake(0, 0, deltaY, 0);
}


@end



@implementation RTUITableCellBackView

+(CGFloat)RTUITableViewCellStandardInset {
    return 0.5f;
}

- (void)dealloc {
    [_backgroundColor release];
    [_separatorColor release];
    [super dealloc];
}

- (void)awakeFromNib {
    [super awakeFromNib];
    [self initDefaults];
}

- (id)init {
    self = [super init];
    if (self) {
        [self initDefaults];
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self initDefaults];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self initDefaults];
    }
    return self;
}

- (void)initDefaults {
    self.backgroundColor = [UIColor whiteColor];
    self.separatorColor = MO_RGBCOLOR(38, 38, 38);
}

- (void)drawRect:(CGRect)rect {
    
    rect = UIEdgeInsetsInsetRect(rect, _insets);
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetShouldAntialias(context, NO);
    CGContextSetFillColorWithColor(context, self.backgroundColor.CGColor);
    CGContextFillRect(context, rect);
    
    CGFloat lineWidth = 1.0f;
    
    CGContextSetLineWidth(context, lineWidth);
    CGContextSetStrokeColorWithColor( context, self.separatorColor.CGColor );
    CGContextBeginPath(context);
    CGContextMoveToPoint(context, 0, rect.size.height - lineWidth + [RTUITableCellBackView RTUITableViewCellStandardInset]);
    CGContextAddLineToPoint(context, rect.size.width,  rect.size.height - lineWidth + [RTUITableCellBackView RTUITableViewCellStandardInset]);
    CGContextStrokePath( context );
}

@end
