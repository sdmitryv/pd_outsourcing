//
//  UICopyLabel.m
//  ProductBuilder
//
//  Created by valery on 5/7/14.
//  Copyright (c) 2014 Cloudworks. All rights reserved.
//

#import "UICopyLabel.h"

@implementation UICopyLabel

- (void) attachTapHandler
{
    [self setUserInteractionEnabled:YES];
    UILongPressGestureRecognizer *touchy = [[UILongPressGestureRecognizer alloc]
                                   initWithTarget:self action:@selector(handleTap:)];
    touchy.minimumPressDuration = 0.8f;
    touchy.cancelsTouchesInView = NO;
    [self addGestureRecognizer:touchy];
    [touchy release];
}

- (id) initWithFrame: (CGRect) frame
{
    if ((self=[super initWithFrame:frame])){
        [self attachTapHandler];
    }
    return self;
}

- (void) awakeFromNib
{
    [super awakeFromNib];
    [self attachTapHandler];
}

#pragma mark Clipboard

- (void) copy: (id) sender{
    UIPasteboard *pboard = [UIPasteboard generalPasteboard];
    if (self.text)
        pboard.string = self.text;
}

- (BOOL) canPerformAction: (SEL) action withSender: (id) sender
{
    return (action == @selector(copy:));
}

- (void) handleTap: (UIGestureRecognizer*) recognizer
{
    if (recognizer.state != UIGestureRecognizerStateBegan) return;
    [self becomeFirstResponder];
    UIMenuController *menu = [UIMenuController sharedMenuController];
    CGPoint touchOrigin = [recognizer locationInView:self.superview];
    [menu setTargetRect:CGRectMake(touchOrigin.x, touchOrigin.y, 0, 0) inView:self.superview];
    [menu setMenuVisible:YES animated:YES];
}

- (BOOL) canBecomeFirstResponder
{
    return YES;
}

@end
