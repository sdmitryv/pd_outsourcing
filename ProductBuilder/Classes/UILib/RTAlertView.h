//
//  RTAlertView.h
//  Key Chain
//

#import <Foundation/Foundation.h>
//#import "SDCBlackAlertView.h"

extern NSString* _Nonnull const RTAlertViewDidPresentNotification;
extern NSString* _Nonnull const RTAlertViewDidDismissNotification;

@interface RTAlertView : NSObject
- (void)disableDismissForIndex:(int)index;
- (void)dismissAlert;
- (void)hideAfter:(float)seconds;
-(UILabel* _Nullable)messageLabel;
//@property (nonatomic, copy, nullable) void(^completedBlock)(NSInteger buttonIndex);
//-(void(^completedBlock)(NSInteger buttonIndex))
- (void(^ _Nullable)(NSInteger)) completedBlock;
-(void)setCompletedBlock:(void (^ _Nullable)(NSInteger buttonIndex))aCompletedBlock;
-(void)showOnMainThread;
-(NSInteger)showOnMainThreadAndWait;
@property(nonatomic, assign)CGFloat angle;
//@property(nonatomic, retain, nullable)UIResponder* formerResponder;
+(NSObject* _Nullable)topMostAlertView;
+(NSInteger)alertViewCount;
+(BOOL)alertViewShown;
+(void)closeAlerts;
@property (nonatomic, assign)NSInteger numberOfRows;

//original methods
- (instancetype _Nonnull)initWithTitle:(NSString * _Nullable)title message:(NSString * _Nullable)message delegate:(id _Nullable)delegate cancelButtonTitle:(NSString * _Nullable)cancelButtonTitle otherButtonTitle:(NSString * _Nullable)otherButtonTitle;

- (instancetype _Nonnull)initWithTitle:(NSString * _Nullable)title message:(NSString * _Nullable)message delegate:(id _Nullable)delegate cancelButtonTitle:(NSString * _Nullable)cancelButtonTitle otherButtonTitles:(NSString * _Nullable)otherButtonTitles, ... NS_REQUIRES_NIL_TERMINATION;
@property(nullable,nonatomic,copy) id /*<UIAlertViewDelegate>*/ delegate;
@property(nullable,nonatomic,copy) NSString *title;
@property(nullable,nonatomic,copy) NSString *message;   // secondary explanation text
- (NSInteger)addButtonWithTitle:(nullable NSString *)title;
- (nullable NSString *)buttonTitleAtIndex:(NSInteger)buttonIndex;
@property(nonatomic,readonly) NSInteger numberOfButtons;
@property(nonatomic) NSInteger cancelButtonIndex;
@property(nonatomic,readonly) NSInteger firstOtherButtonIndex;
@property(nonatomic,readonly) BOOL visible;
- (void)show;
- (void)dismissWithClickedButtonIndex:(NSInteger)buttonIndex animated:(BOOL)animated;
//@property(nonatomic,assign) UIAlertViewStyle alertViewStyle NS_AVAILABLE_IOS(5_0);
- (nullable UITextField *)textFieldAtIndex:(NSInteger)textFieldIndex NS_AVAILABLE_IOS(5_0);
//fake UIView props
@property(nonatomic,assign) NSInteger tag;
-(UIView* _Nullable)viewWithTag:(NSInteger)tag;
-(CGRect)bounds;
-(void)setTransform:(CGAffineTransform)transform;
@end


@protocol RTAlertViewDelegate <NSObject>
@optional

// Called when a button is clicked. The view will be automatically dismissed after this call returns
- (void)alertView:(RTAlertView * _Nullable)alertView clickedButtonAtIndex:(NSInteger)buttonIndex;

// Called when we cancel a view (eg. the user clicks the Home button). This is not called when the user clicks the cancel button.
// If not defined in the delegate, we simulate a click in the cancel button
- (void)alertViewCancel:(RTAlertView * _Nullable)alertView;

- (void)willPresentAlertView:(RTAlertView * _Nullable)alertView;  // before animation and showing view
- (void)didPresentAlertView:(RTAlertView * _Nullable)alertView;  // after animation

- (void)alertView:(RTAlertView * _Nullable)alertView willDismissWithButtonIndex:(NSInteger)buttonIndex; // before animation and hiding view
- (void)alertView:(RTAlertView * _Nullable)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex;  // after animation

// Called after edits in any of the default fields added by the style
- (BOOL)alertViewShouldEnableFirstOtherButton:(RTAlertView * _Nullable)alertView;

@end


#define SDCAlertViewDelegate RTAlertViewDelegate
#define SDCAlertView RTAlertView
