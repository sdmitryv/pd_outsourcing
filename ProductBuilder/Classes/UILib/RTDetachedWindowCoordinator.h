//
//  RTDetachedWindowCoordinator.h
//  ProductBuilder
//
//  Created by valery on 3/21/14.
//  Copyright (c) 2014 Cloudworks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RTDetachedWindowCoordinator : UIViewController

@property (nonatomic, readonly)UIWindow* window;
@property (nonatomic, readonly)UIWindow* userWindow;
@property (nonatomic, readonly)UIViewController* childViewController;

-(void)presentViewControllerInDetachedWindow:(UIViewController*)childController completion:(void (^)(void))completion;
+(void)presentViewControllerInDetachedWindow:(UIViewController*)childController completion:(void (^)(void))completion;
@end
