#import "RTSelectorButton.h"
#import "Global.h"
#import "RTPopoverController.h"
#import "UIView+FirstResponder.h"
#import "DataUtils.h"
#import "NSAttributedString+Attributes.h"
#import "RTDialogViewController.h"

@interface RTDateEditContentViewController : RTDialogContentViewController

@end

@implementation RTDateEditContentViewController

-(void)loadView{
    UIView* view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 240, 320)];
    self.view = view;
    [view release];
}

-(NSString*)title{
    return NSLocalizedString(@"DATE_PICKER_SELECT_DATE_TITLE", nil);
}

-(void)setDialogViewController:(RTDialogViewController *)aDialogViewController{
    [super setDialogViewController:aDialogViewController];
    [self.dialogViewController setSaveHidden:TRUE];
}

@end


@interface RTDateEditViewUnderlined(){
    RTDateEditContentViewController* _dateEditContentViewController;
}

@end

@implementation RTDateEditViewUnderlined
@synthesize defaultTitleValue;
@synthesize dateDelegate;

-(id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self initDefault];
    }
    return self;
}

-(id)init {
    self = [super init];
    if (self) {
        [self initDefault];
    }
    return self;
}

-(void)awakeFromNib {
    
    [super awakeFromNib];
    
    [self initDefault];
}

-(void)initDefault {
    _dateMode = UIDatePickerModeDate;
    _useDateOnly = YES;
    [self addTarget:self action:@selector(clickButton:) forControlEvents:UIControlEventTouchUpInside];
}

-(void)setDateMode:(UIDatePickerMode)dateMode {
    _dateMode = dateMode;
    if (_pickerControl) {
        _pickerControl.datePicker.datePickerMode = dateMode;
    }
}

-(void)setUseDateOnly:(BOOL)useDateOnly {
    _useDateOnly = useDateOnly;
    if (_pickerControl)
        _pickerControl.useDateOnly = useDateOnly;
}

-(void) handleState{
    
    [super handleState];
    
    if (self.highlighted)
        self.titleLabel.transform = CGAffineTransformMakeScale(1.0,0.9);
    else
        self.titleLabel.transform = CGAffineTransformIdentity;
}

-(void)setTitle:(NSString *)title forState:(UIControlState)state {
    
    NSString* label = @"";
    if (title .length > 0) {
        label = title;
    }
    
    NSMutableAttributedString * str = [[NSMutableAttributedString alloc] initWithString:label];
    
    [str setTextAlignment:NSTextAlignmentCenter lineBreakMode:NSLineBreakByWordWrapping];
    [str setTextIsUnderlined:YES];
    [str setFont:self.titleLabel.font range:NSMakeRange(0, str.length)];
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        CGSize possibleSize = str.size;
        possibleSize.width = ceilf(possibleSize.width + 5);
        
        if (self.maxWidth > 0)
            possibleSize.width = MIN(possibleSize.width, self.maxWidth);
        
        if (self.titleLabel.textAlignment == NSTextAlignmentRight) {
            [self setFrame:CGRectMake(self.frame.origin.x + self.frame.size.width - possibleSize.width, self.frame.origin.y , possibleSize.width, self.frame.size.height)];
        }
        else {
            [self setFrame:CGRectMake(self.frame.origin.x, self.frame.origin.y , possibleSize.width, self.frame.size.height)];
        }
        CGRect stringRect = [str boundingRectWithSize:CGSizeMake(self.bounds.size.width, CGFLOAT_MAX) options:0 context:nil];
        if (stringRect.size.height > self.bounds.size.height || stringRect.size.width > self.bounds.size.width){
            [str setTextAlignment:NSTextAlignmentCenter lineBreakMode:NSLineBreakByTruncatingTail];
        }
    
    }
    [super setAttributedTitle:str forState:UIControlStateNormal];
    [super setAttributedTitle:str forState:UIControlStateDisabled];
    

    if (!self.enabled)
        [str setTextColor:[UIColor blackColor]];
    else
        [str setTextColor:global_blue_color];
    
    [str release];
}

-(void)setupLayers {
    [self setBackgroundColor:[UIColor clearColor] forState:UIControlStateNormal];
    [self setBackgroundColor:[UIColor clearColor] forState:UIControlStateHighlighted];
    [self setBackgroundColor:[UIColor clearColor] forState:UIControlStateSelected];
    [self setBackgroundColor:[UIColor clearColor] forState:UIControlStateDisabled];
    
    [self setTitleColor:[UIColor colorWithRed:14/255.0 green:62/255.0 blue:158/255.0 alpha:1] forState:UIControlStateNormal];
    [self setTitleColor:[UIColor colorWithRed:14/255.0 green:62/255.0 blue:158/255.0 alpha:1] forState:UIControlStateDisabled];
    [self setTitleColor:[UIColor colorWithRed:14/255.0 green:62/255.0 blue:158/255.0 alpha:1] forState:UIControlStateHighlighted];
    [self setTitleColor:[UIColor colorWithRed:14/255.0 green:62/255.0 blue:158/255.0 alpha:1] forState:UIControlStateSelected];
}

-(void)clickButton:(id)sender {
    
    UIView* responder = [self.window getFirstResponder];
    if (responder && ![responder canResignFirstResponder])
        return;
    
    [responder resignFirstResponder];
    
    if (!_pickercontrol){
        _pickercontrol = [[RTDatePickerViewController alloc] initWithMode:_dateMode];
        _pickercontrol.preferredContentSize = CGSizeEqualToSize(CGSizeZero, self.popoverContentSize) ? CGSizeMake(320, 273) : self.popoverContentSize;
        _pickercontrol.useDateOnly = _useDateOnly;
    }
    
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone) {
        if (!_dateEditContentViewController){
            _dateEditContentViewController = [[RTDateEditContentViewController alloc]init];
            _pickercontrol.view.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin;
            _pickercontrol.view.center = _dateEditContentViewController.view.center;
            _pickercontrol.contentViewController = _dateEditContentViewController;
            [_dateEditContentViewController.view addSubview:_pickercontrol.view];
        }
        [_dateEditContentViewController showModal:^{
            [self handlePickerControlSelection];
        }];
    }
    else{
        if (!_popoverCtrl){
            _popoverCtrl = [[RTPopoverController alloc]initWithContentViewController:_pickercontrol];
            _popoverCtrl.delegate = self;
        }
        _pickercontrol.popover = _popoverCtrl;
        _pickercontrol.selectedDate = _date;
        
        // Show calendar
        [_popoverCtrl presentPopoverFromRect:self.frame inView:self.superview
                    permittedArrowDirections:UIPopoverArrowDirectionAny animated:TRUE];
    }
    
}

-(void)setDate:(NSDate *)datevalue {
    
    [_date release];
    _date = [datevalue retain];
    
    NSString* text = nil;
    
    if (_date) {
        if (_dateMode == UIDatePickerModeDate) {
            text = [DataUtils readableDateStringFromDate:_date];
        }
        else if (_dateMode == UIDatePickerModeTime) {
            text = [DataUtils readableUTCDateTimeStringFromDate:_date dateFormatterStyle:NSDateFormatterNoStyle timeFormatterStyle:NSDateFormatterShortStyle];
        }
        
        [self setTitle:text forState:UIControlStateNormal];
    }
    else if(defaultTitleValue.length>0)
        [self setTitle:defaultTitleValue forState:UIControlStateNormal];
    else if(self.titleForNullValue.length>0)
        [self setTitle:self.titleForNullValue forState:UIControlStateNormal];
    else
        [self setTitle:@"" forState:UIControlStateNormal];
}

-(void)setDefaultTitleValue:(NSString *)defaultTitleVal {
    [defaultTitleValue release];
    defaultTitleValue = [defaultTitleVal retain];
    
    if (!_date)
        [self setTitle:defaultTitleValue forState:UIControlStateNormal];
}

- (void)setReadonly:(BOOL)readonly{
    
    self.enabled = !readonly;
}

-(void)setEnabled:(BOOL)value{
 
    if (value==self.enabled) return;
    
    [super setEnabled:value];
    _readonly = !value;
    [self setTitle:self.titleLabel.attributedText.string forState:UIControlStateNormal];
}

-(void)dealloc{
    
    [_dateEditContentViewController release];
    [_popoverCtrl release];
    [_date release];
    [_pickercontrol release];
    [defaultTitleValue release];
    [super dealloc];
}

-(void)handlePickerControlSelection{
    if ((!_pickercontrol.selectedDate && !self.date) || [_pickercontrol.selectedDate isEqualToDate:self.date])
    return;
    
    [self setDate:_pickercontrol.selectedDate];
    if (dateDelegate)
        [dateDelegate valueChanged:self];
}


#pragma mark - RTPopoverControllerDelegate
-(BOOL)popoverControllerShouldDismissPopover:(RTPopoverController *)popoverController {
	return TRUE;
}

-(void)popoverControllerDidDismissPopover:(RTPopoverController *)popoverController {
    
    if (popoverController != _popoverCtrl)
        return;
    [self handlePickerControlSelection];
    [_popoverCtrl release];
    _popoverCtrl = nil;
}

@end
