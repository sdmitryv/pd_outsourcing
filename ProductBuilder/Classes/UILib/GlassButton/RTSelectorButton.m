//
//  RTSelectorButton.m
//  CloudworksPOS
//
//  Created by Vitaliy Gervazuk on 7/25/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "RTSelectorButton.h"
#import "Global.h"
#import "RTPopoverController.h"
#import "UIView+FirstResponder.h"
#import "DataUtils.h"
#import "NSAttributedString+Attributes.h"
#import "UIViewControllerExtended.h"

@implementation RTSelectorButton

@synthesize delegate, titleFormat, columns, popoverArrowDirection, presentingController;

-(id)init{
    if (self = [super init]) {
        self.style = MOGlassButtonStyleFlat;
    }
    return self;
}

-(id)initWithCoder:(NSCoder *)aDecoder{
    if (self = [super initWithCoder:aDecoder]) {
        self.style = MOGlassButtonStyleFlat;
    }
    return self;
}

-(id)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        self.style = MOGlassButtonStyleFlat;
    }
    return self;
}


-(void)setTitleForNullValue:(NSString *)value{
    [_titleForNullValue release];
    _titleForNullValue = [value retain];
    if (!self.value){
        [self setTitle:_titleForNullValue ? : @"" forState:UIControlStateNormal];
    }
}

-(NSObject*)itemValue:(NSObject*)item{
    if (!_dataMember) return nil;
    if ([_dataMember isEqualToString:@"indexOfObject"]){
        NSInteger index = [_dataSource indexOfObject:item];
        return index!=NSNotFound ? @(index) : nil;
    }
    return [item valueForKey:_dataMember];
}

-(NSObject*)value {
    return _value;
}

//value defined by _dataMember property in _selectedItem
-(void)setValue:(NSObject *)value {
    if ([value isEqual:_value]) return;
    [_selectedItem release];
    _selectedItem  = nil;
    if (value){
        for (NSObject* item in _dataSource) {
            NSObject* theValue = [self itemValue:item];
            if ([theValue isEqual:value] || theValue == value) {
                _selectedItem = [item retain];
                break;
            }
        }
    }
    [_value release];
    _value = [value retain];
    
    [self updateTitleFromValue];
}

-(void)updateTitleFromItem{
    NSString * title = nil;
    if (_selectedItem && _buttonDisplayMember) {
        NSString * description = [[_selectedItem valueForKey:_buttonDisplayMember] description];
        if (description.length > 0) {
            title = titleFormat.length ? [NSString stringWithFormat:titleFormat, description] : description;
        }
    }
    [self setTitle:title
                    ?: (_value && _valueTitle ? _valueTitle : self.titleForNullValue)
          forState:UIControlStateNormal];
}

-(void)updateTitleFromValue{
    NSString * title = nil;
    if (_buttonDisplayMember && _dataMember && [_buttonDisplayMember isEqualToString:_dataMember]) {
        NSString * description = [_value description];
        if (description.length > 0) {
            title = titleFormat.length ? [NSString stringWithFormat:titleFormat, description] : description;
        }
        [self setTitle:title
                        ?: (_value && _valueTitle ? _valueTitle : self.titleForNullValue)
              forState:UIControlStateNormal];
        return;
    }
    [self updateTitleFromItem];
}


-(NSObject*)selectedItem {
    return _selectedItem;
}

-(void)setSelectedItem:(NSObject*)item {
    [_selectedItem release];
    _selectedItem  = [item retain];
    [_value release];
    _value = [[self itemValue:item] retain];
    [self updateTitleFromItem];
}

-(NSInteger)selectedIndex {
    return [_dataSource indexOfObject:_selectedItem];
}

-(void)setSelectedIndex:(NSInteger)index{
    if (index==NSNotFound || index < 0 || index >= _dataSource.count){
        self.selectedItem = nil;
    }
    else{
        self.selectedItem = _dataSource[index];
    }
}

- (void)setupLayers {
    [super setupLayers];
    self.contentEdgeInsets = UIEdgeInsetsMake(0, 10, 0, 0);
    self.contentMode = UIViewContentModeLeft;
    self.titleLabel.textAlignment = NSTextAlignmentCenter;
    self.titleLabel.lineBreakMode = NSLineBreakByTruncatingTail;
    self.titleLabel.numberOfLines = 1;

}

-(void)initializeControl:(NSString*)member dataSource:(NSArray*)dataSource currentValue:(NSObject*)currentValue title:(NSString*)title {
    [self initializeControl:member dataMember:member dataSource:dataSource currentValue:currentValue title:title];
}

-(void)initializeControl:(NSString*)displayMember dataMember:(NSString*)dataMember dataSource:(NSArray*)dataSource currentValue:(NSObject*)currentValue title:(NSString*)title {
    
    [self initializeControl:displayMember listDisplayMember:displayMember dataMember:dataMember dataSource:dataSource currentValue:currentValue currentDisplayValue:nil title:title];
}

-(void)initializeControl:(NSString*)buttonDisplayMember dataMember:(NSString*)dataMember columns:(NSArray*)columnsPar dataSource:(NSArray*)dataSource currentValue:(NSObject*)currentValue title:(NSString*)title {
    
    [self initializeControl:buttonDisplayMember listDisplayMember:nil dataMember:dataMember dataSource:dataSource currentValue:currentValue currentDisplayValue:nil title:title];
    
    [columns release];
    columns = [columnsPar retain];
}

-(void)initializeControl:(NSString*)buttonDisplayMember listDisplayMember:(NSString*)listDisplayMember dataMember:(NSString*)dataMember dataSource:(NSArray*)dataSource currentValue:(NSObject*)currentValue title:(NSString*)title {
    
    [self initializeControl:buttonDisplayMember listDisplayMember:listDisplayMember dataMember:dataMember dataSource:dataSource currentValue:currentValue currentDisplayValue:nil title:title];
}


-(void)initializeControl:(NSString*)buttonDisplayMember listDisplayMember:(NSString*)listDisplayMember dataMember:(NSString*)dataMember dataSource:(NSArray*)dataSource currentValue:(NSObject*)currentValue currentDisplayValue:(NSString*)currentDisplayValue title:(NSString*)title {
    [_buttonDisplayMember release];
    _buttonDisplayMember = [buttonDisplayMember retain];
   [_listDisplayMember release];
    _listDisplayMember = [listDisplayMember retain];
    [_dataMember release];
    _dataMember = [dataMember retain];
    [_dataSource release];
    _dataSource = [dataSource retain];
    [_title release];
    _title = [title retain];
    
    self.valueTitle = currentDisplayValue;
    
    self.value = currentValue;
    
    popoverArrowDirection = UIPopoverArrowDirectionAny;
    
    [self addTarget:self action:@selector(clickButton:) forControlEvents:UIControlEventTouchUpInside];
    
    return;
}

-(void)setDataSource:(NSArray*)value{
    if ([_dataSource isEqual:value]) return;
    [_dataSource release];
    _dataSource = [value retain];
    self.value = _value;
}

-(NSArray*)dataSource{
    return _dataSource;
}

-(SimpleTableListViewController*)itemsListViewController{
    if (!_itemsListViewController){
        _itemsListViewController = [[SimpleTableListViewController alloc] init];
        _itemsListViewController.items = _dataSource;
        _itemsListViewController.displayMember = _listDisplayMember;
        _itemsListViewController.delegate = self;
        _itemsListViewController.titleText = _title;
        _itemsListViewController.columns = columns;
        _itemsListViewController.preferredContentSize = CGSizeEqualToSize(CGSizeZero, self.popoverContentSize) ? CGSizeMake(300.0, 300.0) : self.popoverContentSize;
    }
    NSArray* displayedDataSorce = _dataSource;
    if (self.dataSourceDisplayPredicate){
        displayedDataSorce = [_dataSource filteredArrayUsingPredicate:self.dataSourceDisplayPredicate];
    }
    _itemsListViewController.items = displayedDataSorce;
    [_itemsListViewController setSelectedIndex:[displayedDataSorce indexOfObject:_selectedItem] ignoreSelection:TRUE];
    return _itemsListViewController;
}

-(void)setPopoverContentSize:(CGSize)popoverContentSize{
    _popoverContentSize = popoverContentSize;
    if (_itemsListViewController){
        _itemsListViewController.preferredContentSize = _popoverContentSize;
    }
}


- (void) click{
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad) {
        [self becomeFirstResponder];
        [self.itemsListViewController presentPopoverFromRect:self.frame inView:self.superview
                                permittedArrowDirections:popoverArrowDirection animated:YES];
    }
    else {
        [self performSelector:@selector(presentSimpleTableListView) withObject:nil afterDelay:0.0];
    }
}

- (void)presentSimpleTableListView {
    [self.itemsListViewController presentInController:presentingController?:[UIViewController topmostModalViewController]];
}

- (void) performClick{
    [self clickButton:self];
}

- (void) clickButton:(id)sender {

    UIView *responder = [self.window getFirstResponder];
    if (responder && ![responder canResignFirstResponder])
        return;

    if (delegate && [(id)delegate respondsToSelector:@selector(selectorButton:validateAndCallback:)]){
        [delegate selectorButton:self validateAndCallback:^(BOOL stop) {
            if (!stop){
                [self click];
            }
        }];
    }
    else{
        [self click];
    }
}

-(void)dealloc{
    [_titleForNullValue release];
    [_title release];
    [_value release];
    [_valueTitle release];
    [_itemsListViewController release];
    [_listDisplayMember release];
    [_buttonDisplayMember release];
    [_dataMember release];
    [_selectedItem release];
    [_dataSource release];
    [titleFormat release];
    [columns release];
    [_dataSourceDisplayPredicate release];
    [super dealloc];
}

-(BOOL)canBecomeFirstResponder {
    return YES;
}

#pragma mark -
#pragma mark SimpleTableListViewControllerDelegate

-(void)simpleTableListViewController:(SimpleTableListViewController*)controller didSelectValue:(id)value{
    if (_buttonDisplayMember){
        NSString * title = [[value valueForKey:_buttonDisplayMember] description];
        if (!title){
            title = self.titleForNullValue;
        }
        else if (title.length && titleFormat) {
            title = [NSString stringWithFormat:titleFormat, title];
        }
        [self setTitle:title forState:UIControlStateNormal];
    }
    [_selectedItem release];
    _selectedItem = [value retain];
    [_value release];
    _value = [[self itemValue:value]retain];
    if (delegate && !insideDidSelectValue){
        insideDidSelectValue++;
        if ([(id)delegate respondsToSelector:@selector(selectorButton:didSelectValue:)])
            [delegate selectorButton:self didSelectValue:_value];
        if ([(id)delegate respondsToSelector:@selector(selectorButton:didSelectItem:)])
            [delegate selectorButton:self didSelectItem:value];
        insideDidSelectValue--;
    }
}

-(void)simpleTableListViewController:(SimpleTableListViewController *)controller didDismissPopover:(UIPopoverPresentationController*)popover{
    [self resignFirstResponder];
}

@end

