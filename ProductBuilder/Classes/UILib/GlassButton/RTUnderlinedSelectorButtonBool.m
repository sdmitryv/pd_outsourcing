//
//  RTUnderlinedSelectorButtonBool.m
//  ProductBuilder
//
//  Created by Julia Korevo on 6/23/14.
//  Copyright (c) 2014 Cloudworks. All rights reserved.
//

#import "RTSelectorButton.h"

@implementation RTUnderlinedSelectorButtonBool

-(void)initializeControlAsBool:(NSString*)title currentValue:(BOOL)value{
    NSArray *array = @[NSLocalizedString(@"NOBTN_TITLE", nil), NSLocalizedString(@"YESBTN_TITLE", nil)];
//    self.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [super initializeControl:@"description" dataMember:@"indexOfObject" dataSource:array currentValue:@(value?TRUE:FALSE) title:title];
}
-(void)setBoolValue:(BOOL)value{
    [self setSelectedIndex:value ?TRUE:FALSE];
}
-(BOOL)boolValue{
    return self.selectedIndex;
}
@end
