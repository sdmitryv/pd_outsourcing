//
//  RTSelectorButton.h
//  CloudworksPOS
//
//  Created by Vitaliy Gervazuk on 7/25/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MOGlassButton.h"
#import "SimpleTableListViewController.h"
#import "RTDatePickerViewController.h"

@class RTSelectorButton;

@protocol RTSelectorButtonDelegate
@optional
-(void)selectorButton:(RTSelectorButton*)button didSelectValue:(id)value;
-(void)selectorButton:(RTSelectorButton*)button didSelectItem:(id)item;
-(void)selectorButton:(RTSelectorButton*)button validateAndCallback:(void (^)(BOOL stop))callback;
@end

@interface RTSelectorButton : MOGlassButtonLightGray<SimpleTableListViewControllerDelegate> {
    @protected
    NSString* _buttonDisplayMember;
    NSString* _listDisplayMember;
    NSString* _dataMember;
    NSObject* _value;
    NSObject* _selectedItem;
    NSArray* _dataSource;
    SimpleTableListViewController* _itemsListViewController;
    id<RTSelectorButtonDelegate> delegate;
    NSArray* columns;
    NSInteger insideDidSelectValue;
    UIViewController* presentingController;
    NSString* _title;
}
@property (nonatomic, assign) CGSize popoverContentSize;
@property (nonatomic, assign) NSInteger selectedIndex;
@property (nonatomic, retain) NSObject* value;
@property (nonatomic, retain) NSString *valueTitle;
@property (nonatomic, retain) NSObject* selectedItem;
@property (nonatomic, assign) id<RTSelectorButtonDelegate> delegate;
@property (nonatomic, retain) NSString* titleFormat;
@property (nonatomic, retain) NSArray* columns;
@property (nonatomic, assign) UIPopoverArrowDirection popoverArrowDirection;
@property (nonatomic, retain)NSString* titleForNullValue;
@property (nonatomic, assign) UIViewController *presentingController;
@property (nonatomic, retain) NSArray *dataSource;
@property (nonatomic, retain) NSPredicate *dataSourceDisplayPredicate;
@property (nonatomic, readonly) SimpleTableListViewController* itemsListViewController;

-(void)initializeControl:(NSString*)member dataSource:(NSArray*)dataSource currentValue:(NSObject*)currentValue title:(NSString*)title;
-(void)initializeControl:(NSString*)displayMember dataMember:(NSString*)dataMember dataSource:(NSArray*)dataSource currentValue:(NSObject*)currentValue title:(NSString*)title;
-(void)initializeControl:(NSString*)buttonDisplayMember listDisplayMember:(NSString*)listDisplayMember dataMember:(NSString*)dataMember dataSource:(NSArray*)dataSource currentValue:(NSObject*)currentValue currentDisplayValue:(NSString*)currentDisplayValue title:(NSString*)title;
-(void)initializeControl:(NSString*)buttonDisplayMember listDisplayMember:(NSString*)listDisplayMember dataMember:(NSString*)dataMember dataSource:(NSArray*)dataSource currentValue:(NSObject*)currentValue title:(NSString*)title;

-(void)initializeControl:(NSString*)buttonDisplayMember dataMember:(NSString*)dataMember columns:(NSArray*)columns dataSource:(NSArray*)dataSource currentValue:(NSObject*)currentValue title:(NSString*)title;
- (void) performClick;
@end

@interface RTUnderlinedSelectorButton :  RTSelectorButton

@property(nonatomic, assign) BOOL noLineForReadOnly;
@property(nonatomic, assign) int maxWidth;
@property(nonatomic, assign) BOOL isReadOnly;
@property(nonatomic, retain) UIColor *enabledColor;
@end

@interface RTUnderlinedSelectorButtonBool :  RTUnderlinedSelectorButton
-(void)initializeControlAsBool:(NSString*)title currentValue:(BOOL)value;
@property(nonatomic,assign)BOOL boolValue;
@end

@interface RTSelectorButtonBlack :  RTSelectorButton
@end

@protocol RTDateEditViewUnderlinedDelegate
-(void)valueChanged:(RTSelectorButton*)button;
@end

@interface RTDateEditViewUnderlined : RTSelectorButton<RTPopoverControllerDelegate, IRTDatePickerViewController> {
    RTPopoverController* _popoverCtrl;
    RTDatePickerViewController* _pickercontrol;
    NSDate* _date;
    id<RTDateEditViewUnderlinedDelegate> dateDelegate;
}

@property (nonatomic, retain) NSDate* date;
@property (nonatomic, assign) BOOL readonly;
@property (nonatomic, readonly) RTDatePickerViewController* pickerControl;
@property (nonatomic, assign) NSInteger maxWidth;
@property (nonatomic, assign) UIDatePickerMode dateMode;
@property (nonatomic, retain) NSString *defaultTitleValue;
@property (nonatomic, assign) id<RTDateEditViewUnderlinedDelegate> dateDelegate;
@property (nonatomic, assign) BOOL useDateOnly;

@end

@interface RTSelectorButton50 :  RTSelectorButton
@property(nonatomic, assign) BOOL fitPopoverSize;
@property (nonatomic, readonly) UIColor *defaultTitleColor;
@end
