//
//  MOButton.m
//  Licensed under the terms of the BSD License, as specified below.
//
//  Created by Hwee-Boon Yar on Feb/13/2010.
//
/*
 Copyright 2010 Yar Hwee Boon. All rights reserved.
 
 All rights reserved.
 
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 
 * Redistributions of source code must retain the above copyright
 notice, this list of conditions and the following disclaimer.
 
 * Redistributions in binary form must reproduce the above copyright
 notice, this list of conditions and the following disclaimer in the
 documentation and/or other materials provided with the distribution.
 
 * Neither the name of MotionObj nor the names of its
 contributors may be used to endorse or promote products derived from
 this software without specific prior written permission.
 
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
 TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#import "MOButton.h"
#import "Global.h"
#import <QuartzCore/QuartzCore.h>
#import "UIRoundedCornersView.h"
#import "UIColor+HSL.h"

@implementation MOButton

@synthesize normalBackgroundColor;
@synthesize highlightedBackgroundColor;
@synthesize disabledBackgroundColor;
@synthesize selectedBackgroundColor;
@synthesize normalBorderColor;
@synthesize highlightedBorderColor;
@synthesize disabledBorderColor;
@synthesize selectedBorderColor;
@synthesize forceLayerWithImage;

+ (id)buttonWithType:(UIButtonType)buttonType{
    return [super buttonWithType:UIButtonTypeCustom];
}

- (id)initWithFrame:(CGRect)aRect {
	if ((self = [super initWithFrame:aRect])) {
        [self applyDefaultProperties];
		[self setupLayers];
	}

	return self;
}

-(id)initWithCoder:(NSCoder *)aDecoder{
    if ((self =[super initWithCoder:aDecoder])){
        [self applyDefaultProperties];
        [self setupLayers];
    }
    return self;
}


- (void)awakeFromNib {
    [super awakeFromNib];
    [self applyDefaultProperties];
    [self setupLayers];
}


- (void)dealloc {
	self.normalBackgroundColor = nil;
	self.highlightedBackgroundColor = nil;
	self.disabledBackgroundColor = nil;
    self.selectedBackgroundColor = nil;
    [normalBorderColor release];
    [highlightedBorderColor release];
    [selectedBorderColor release];
    [disabledBorderColor release];
	[super dealloc];
}

-(void)setBackgroundColor:(UIColor *)backgroundColor{
    //do nothing
}

- (void)setupLayers {
    self.titleLabel.textAlignment = NSTextAlignmentCenter;
    self.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    self.titleLabel.numberOfLines = 0;
}

- (void)applyDefaultProperties {
    [self setValue:@(UIButtonTypeCustom) forKey:@"buttonType"];
    self.exclusiveTouch = TRUE;
}


- (void)setBackgroundColor:(UIColor*)aColor forState:(UIControlState)aState {
	switch (aState) {
		case UIControlStateNormal:
			self.normalBackgroundColor = aColor;
            self.layer.borderColor = [normalBorderColor?:aColor CGColor];
			if (self.enabled) self.layer.backgroundColor = self.normalBackgroundColor.CGColor;
			break;
		case UIControlStateHighlighted:
            self.highlightedBackgroundColor = aColor;
        case UIControlStateSelected:
			self.selectedBackgroundColor = aColor;
			break;
		case UIControlStateDisabled:
			self.disabledBackgroundColor = aColor;
			if (!self.enabled) self.layer.backgroundColor = self.disabledBackgroundColor.CGColor;
			break;
		default:
			break;
	}
}


-(void)handleState{
    if ([self imageForState:UIControlStateNormal] && !forceLayerWithImage) {
        self.backgroundColor = [UIColor clearColor];
        self.layer.backgroundColor = [UIColor clearColor].CGColor;
        self.layer.borderColor = [[UIColor clearColor] CGColor];
        self.layer.borderWidth = 0;
        return;
    }
    
    if (!self.enabled){
        self.layer.backgroundColor = (self.disabledBackgroundColor ?: MO_RGBCOLOR(90, 90, 90)).CGColor;
        self.layer.borderColor = self.disabledBorderColor.CGColor ?: self.layer.backgroundColor;
    }
    else{
        if (self.highlighted){
            self.layer.backgroundColor = [self colorOrTransofrmedNormalColor:self.highlightedBackgroundColor].CGColor;
            self.layer.borderColor = self.highlightedBorderColor.CGColor ?: [self colorOrTransofrmedNormalColor:self.normalBorderColor].CGColor ? : self.layer.backgroundColor;
        }
        else if (self.selected){
            self.layer.backgroundColor = [self colorOrTransofrmedNormalColor:self.selectedBackgroundColor].CGColor;
            self.layer.borderColor = self.selectedBorderColor.CGColor ?: [self colorOrTransofrmedNormalColor:self.normalBorderColor].CGColor ? : self.layer.backgroundColor;
        }
        else{
            self.layer.backgroundColor = self.normalBackgroundColor.CGColor;
            self.layer.borderColor = self.normalBorderColor.CGColor ?: self.layer.backgroundColor;
        }
    }
}

-(UIColor*)colorOrTransofrmedNormalColor:(UIColor*)color{
    if (color) return color;
    CGFloat h,s,l,a;
    [self.normalBackgroundColor getHue:&h saturation:&s lightness:&l alpha:&a];
    return l < 0.8 ?  [self.normalBackgroundColor lighten:0.1] : [self.normalBackgroundColor darken:0.1];
}


- (void)setEnabled:(BOOL)value {
    if (self.enabled ==value)return;
	[super setEnabled:value];
    [self handleState];
}

- (void)setHighlighted:(BOOL)value {
    if (self.highlighted ==value)return;
    [super setHighlighted:value];
    [self handleState];
}

-(void)setSelected:(BOOL)value{
    if (self.selected ==value)return;
    [super setSelected:value];
    [self handleState];
}

//-(void)layoutSubviews{
//    [super layoutSubviews];
//    [self handleState];
//}

@end

@implementation MORoundedGrayButton

- (void)setupLayers
{
    [UIRoundedCornersView drawRoundedCorners:self radius:5.0];
	[super setupLayers];
  	[self setBackgroundColor:MO_RGBCOLOR(205, 203, 203) forState:UIControlStateNormal];
	[self setBackgroundColor:MO_RGBCOLOR(0, 0, 125) forState:UIControlStateHighlighted];
    [self setBackgroundColor:MO_RGBCOLOR(0, 0, 125) forState:UIControlStateSelected];
	[self setBackgroundColor:MO_RGBCOLOR(227, 226, 225) forState:UIControlStateDisabled];
	self.titleLabel.shadowOffset = CGSizeMake(0, 0);
}

- (void)setHighlighted:(BOOL)highlight {
    if (self.selected && highlight) {
        return;
    }
    [super setHighlighted:highlight];
    for (UIView * view in self.subviews) {
        if ([view isKindOfClass:[UILabel class]]) {
            [(UILabel *)view setHighlighted:highlight || self.isSelected];
        }
    }
}

- (void)setSelected:(BOOL)selected {
    if (self.isEnabled) {
        [super setSelected:selected];
        for (UIView * view in self.subviews) {
            if ([view isKindOfClass:[UILabel class]]) {
                [(UILabel *)view setHighlighted:selected || self.isHighlighted];
            }
        }
    }
}

@end

@implementation MORoundedRedButton

-(void)dealloc {
    //[_disabledBorderColor release];
    [super dealloc];
}

- (void)setupLayers
{
    [UIRoundedCornersView drawRoundedCorners:self radius:5.0];
    [super setupLayers];
    [self setBackgroundColor:MO_RGBCOLOR(220, 68, 71) forState:UIControlStateNormal];
    [self setBackgroundColor:MO_RGBCOLOR(255, 50, 70) forState:UIControlStateHighlighted];
    [self setBackgroundColor:MO_RGBCOLOR(255, 50, 70) forState:UIControlStateSelected];
    [self setBackgroundColor:MO_RGBCOLOR(255, 255, 255) forState:UIControlStateDisabled];
    self.disabledBorderColor = MO_RGBCOLOR(137, 143, 156);
    [self setTitleColor:self.disabledBorderColor forState:UIControlStateDisabled];
    [self setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    self.titleLabel.shadowOffset = CGSizeMake(0, 0);
    
    [self setContentEdgeInsets:UIEdgeInsetsMake(0, 20, 0, 20)];
}

- (void)setHighlighted:(BOOL)highlight {
    if (self.selected && highlight) {
        return;
    }
    [super setHighlighted:highlight];
    for (UIView * view in self.subviews) {
        if ([view isKindOfClass:[UILabel class]]) {
            [(UILabel *)view setHighlighted:highlight || self.isSelected];
        }
    }
}

- (void)setSelected:(BOOL)selected {
    if (self.isEnabled) {
        [super setSelected:selected];
        for (UIView * view in self.subviews) {
            if ([view isKindOfClass:[UILabel class]]) {
                [(UILabel *)view setHighlighted:selected || self.isHighlighted];
            }
        }
    }
}

-(void)handleState{
    
    [super handleState];
    
    if (!self.enabled && self.disabledBorderColor) {
        self.layer.borderColor = self.disabledBorderColor.CGColor;
        self.layer.borderWidth = 1.0f;
    }
    else {
        self.layer.borderWidth = 0.0f;
    }
}

@end
