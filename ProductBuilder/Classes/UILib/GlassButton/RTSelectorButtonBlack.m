#import "RTSelectorButton.h"
#import <objc/runtime.h>
#import <objc/message.h>

@implementation RTSelectorButtonBlack

+(void)load{
    Method newMethod = class_getInstanceMethod([MOGlassButtonBlack class], @selector(addShineLayer));
    class_replaceMethod([self class], @selector(addShineLayer), method_getImplementation(newMethod), method_getTypeEncoding(newMethod));
    newMethod = class_getInstanceMethod([MOGlassButtonBlack class], @selector(setupLayers));
    class_replaceMethod([self class], @selector(setupLayers), method_getImplementation(newMethod), method_getTypeEncoding(newMethod));
}

@end