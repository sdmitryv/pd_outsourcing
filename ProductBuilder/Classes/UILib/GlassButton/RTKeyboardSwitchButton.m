//
//  RTKeyboardSwitchButton.m
//  CloudworksPOS
//
//  Created by Vitaliy Gervazuk on 8/30/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "RTKeyboardSwitchButton.h"
#import "AppSettingManager.h"
#import "SystemVer.h"

@implementation RTKeyboardSwitchButton

-(void)boundSearchBar:(UISearchBar*)sbar selected:(BOOL)selected {
    [_sBar setKeyboardVisible:YES];
    [_sBar release];
    _sBar = [sbar retain];
    self.selected = selected;
    [_sBar setKeyboardVisible:!self.selected];
    [self removeTarget:self action:@selector(clickButton:) forControlEvents:UIControlEventTouchUpInside];
    [self addTarget:self action:@selector(clickButton:) forControlEvents:UIControlEventTouchUpInside];
}

- (void)applyDefaultProperties {
    [super applyDefaultProperties];
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad){
        forceLayerWithImage = TRUE;
        [self setImage:[UIImage imageNamed:@"keyboard_down7.png"] forState:UIControlStateNormal];
        [self setImage:[UIImage imageNamed:@"keyboard_down7.png"] forState:UIControlStateHighlighted];
        [self setImage:[UIImage imageNamed:@"keyboard_up7.png"] forState:UIControlStateSelected];
        [self setImage:[UIImage imageNamed:@"keyboard_up7.png"] forState:UIControlStateSelected | UIControlStateHighlighted];
    }
    else{
        if (IS_IPHONE_6_OR_MORE) {
            forceLayerWithImage = TRUE;
            [self setImage:[UIImage imageNamed:@"keyboard_down7_iphone.png"] forState:UIControlStateNormal];
            [self setImage:[UIImage imageNamed:@"keyboard_down7_iphone.png"] forState:UIControlStateHighlighted];
            [self setImage:[UIImage imageNamed:@"keyboard_up7_iphone.png"] forState:UIControlStateSelected];
            [self setImage:[UIImage imageNamed:@"keyboard_up7_iphone.png"] forState:UIControlStateSelected | UIControlStateHighlighted];
        }
        else {
            [self setBackgroundImage:[UIImage imageNamed:@"dial_btn_normal.png"] forState:UIControlStateNormal];
            [self setBackgroundImage:[UIImage imageNamed:@"dial_btn_normal.png"] forState:UIControlStateSelected];
            [self setImage:[UIImage imageNamed:@"keyboard_down_icon.png"] forState:UIControlStateNormal];
            [self setImage:[UIImage imageNamed:@"keyboard_down_icon.png"] forState:UIControlStateHighlighted];
            [self setImage:[UIImage imageNamed:@"keyboard_up_icon.png"] forState:UIControlStateSelected];
            [self setImage:[UIImage imageNamed:@"keyboard_up_icon.png"] forState:UIControlStateSelected | UIControlStateHighlighted];
        }
    }
}

-(void)setupLayers{
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad || IS_IPHONE_6_OR_MORE){
        [super setupLayers];
        self.selectedBackgroundColor = self.normalBackgroundColor;
    }
}

-(void)dealloc {
    [_sBar release];
    [super dealloc];
}

- (void) clickButton:(id)sender {
    [self handleKeyboardButton];
}

-(void)handleKeyboardButton{
    [_sBar setKeyboardVisible:!_sBar.isKeyboardVisible];
    self.selected = !_sBar.isKeyboardVisible;
}

@end
