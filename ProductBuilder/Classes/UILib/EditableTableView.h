//
//  EditableTableView.h
//  iPadPOS
//
//  Created by Lulakov Viacheslav on 3/16/11.
//  Copyright 2011 DBBestTechnologies. All rights reserved.
//

#import <Foundation/Foundation.h>

@class EditableTableView;

@protocol EditableTableViewProtocol

- (id)editableTableView:(EditableTableView *)tableView addItem:(NSString *)name;
- (void)editableTableView:(EditableTableView *)tableView renameItemAtIndex:(NSInteger)index newName:(NSString *)name;
- (BOOL)editableTableView:(EditableTableView *)tableView deleteItemAtIndex:(NSInteger)index;

@end

@interface EditableTableView : UIView <UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate> {
	UITableView		* itemTableView;
	NSMutableArray	* itemList;
	NSObject<EditableTableViewProtocol>	* dataDelegate;
	int maxItems;
}
	
@property (nonatomic, retain) UITableView		* itemTableView;
@property (nonatomic, retain) NSMutableArray	* itemList;
@property (nonatomic, assign) NSObject<EditableTableViewProtocol>	* dataDelegate;
@property (nonatomic, assign) int maxItems;

@end
