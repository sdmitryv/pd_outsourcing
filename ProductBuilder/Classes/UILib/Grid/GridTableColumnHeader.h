//
//  ColumnHeader.h
//  ProductBuilder
//
//  Created by Roman on 12/12/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@class GridTableColumnDescription;

typedef NS_ENUM(NSUInteger, GridTableColumnHeaderHighlightedSide) { GridTableColumnHeaderHighlightedSideNone = 0, GridTableColumnHeaderHighlightedSideLeft = 1, GridTableColumnHeaderHighlightedSideRight = 2 };

@interface GridTableColumnHeader : UIButton {
    GridTableColumnDescription       *_columnDescription;
    GridTableColumnHeaderHighlightedSide   _highlightedSide;
    UIView                  *_highlightedBorderView;
    CGFloat                _touchableBorderWidth;
}

@property (nonatomic, retain) GridTableColumnDescription* columnDescription;
@property (nonatomic, assign) GridTableColumnHeaderHighlightedSide highlightedSide;
@property (nonatomic, assign) CGFloat touchableBorderWidth;

-(UIImage *)createDruggingImage;

@end
