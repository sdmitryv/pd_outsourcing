//
//  GridTable.m
//  ProductBuilder
//
//  Created by Lulakov Viacheslav on 11/10/10.
//  Copyright 2010 DBBestTechnologies. All rights reserved.
//

#import "GridTable.h"
#import <QuartzCore/QuartzCore.h>
#import "DataUtils.h"
#import "DecimalHelper.h"
#import "UITextFieldNumeric.h"
#import "GridColumnsEditorViewController.h"
#import "GridTableColumnHeader.h"
#import "UITextField+Input.h"
#import "RTAlertView.h"
#import "GridTableCellTextField.h"
#import "GridTableCellLabel.h"
#import "GridTableCellBoolean.h"
#import "GridTableCellColoredTextColumn.h"
#import "GridTableCellButton.h"
#import "GridTableCellImageWithText.h"
#import "GridTableCellList.h"
#import "GridTableCellImage.h"
#import "UIView+FirstResponder.h"
#import "Global.h"

@interface RTTableView : UITableView

@end

@implementation RTTableView

-(void)layoutSubviews{
    [super layoutSubviews];
    if (self.delegate && [self.delegate respondsToSelector:@selector(tableViewDidEndLayoutSubviews:)]){
        [self.delegate performSelector:@selector(tableViewDidEndLayoutSubviews:) withObject:self];
    }
}

@end

#define DEFAULT_HEADER_HEIGHT 40
#define DEFAULT_ROW_HEIGHT 40

NSString* const gridTableColumnHeaderFrameChangedNotification = @"gridTableColumnHeaderFrameChangedNotification";
NSString* const gridTableColumnHeaderHighLightBorderNotification = @"gridTableColumnHeaderHighLightBorderNotification";
NSString* const gridTableColumnDescriptionKey = @"gridTableColumnDescriptionKey";

@interface GridTable (){
    //resing stuff
    GridTableColumnHeader *resizedLeftColumnHeader;
    GridTableColumnHeader *resizedRightColumnHeader;
    //reordering stuff
    GridTableColumnHeader *candidateReorderingColumn;
    GridTableColumnHeader *candidateNextToReorderingColumn;
    UIView* movingView;
    UIView* hightlightVerticalLine;
    CGRect lastLayoutedRect;
}

@end

@implementation GridTable

@synthesize delegate = _delegate, tableView =_tableView, headerView=_headerView, footerView = _footerView, scrollView = _scrollView, headerFont, cellFont, allowCustomization, allowMoveRows, defaultColumns = _defaultColumns, yesImage = _yesImage, noImage = _noImage, selectRowOnReload,
scrollPosition, selectedRowBackgroundColor,
rowTextColorSelected,
rowTextColor,
rowBackgroundColorEven,
rowBackgroundColorOdd,
sortingColumnName = _sortingColumnName;

- (void)initDefaults {
    
    _footerViewHiden = YES;
    defferedSelectedRow = NSNotFound;
    self.selectRowOnReload = TRUE;
    self.rowTextColorSelected = [UIColor colorWithRed:1.0 green:1.0 blue:1.0 alpha:1.0];
    self.rowTextColor = [UIColor colorWithRed:0.2 green:0.2 blue:0.2 alpha:1.0];
    self.rowBackgroundColorEven = [UIColor colorWithRed:0.867 green:0.867 blue:0.867 alpha:1.0];
    self.rowBackgroundColorOdd = [UIColor whiteColor];
    self.headerFont = [UIFont systemFontOfSize:14];
    self.cellFont = [UIFont systemFontOfSize:14];
    
    _sortingMode = NotSorted;
    self.sortingColumnName = nil;
    _upArrow = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"bt_sort_up.png"]];
    _downArrow = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"bt_sort_down.png"]];
    _backgroundImage = [[UIImage imageNamed:@"gradient.png"] retain];
    _yesImage = [[UIImage imageNamed:@"yesImage.png"] retain];
    _noImage = [[UIImage imageNamed:@"noImage.png"] retain];
    _headerView = [[UIRoundedCornersView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, DEFAULT_HEADER_HEIGHT)];
    _headerView.clipsToBounds = YES;
    _headerView.autoresizingMask = UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleWidth;
    _headerView.autoresizesSubviews = TRUE;
    _headerView.backgroundColor = [UIColor clearColor];
    
    _footerView = [[UIRoundedCornersView alloc] initWithFrame:CGRectMake(0, self.frame.size.height - _headerView.bounds.size.height, self.frame.size.width, _headerView.bounds.size.height)];
    _footerView.clipsToBounds = YES;
    _footerView.autoresizingMask = UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleWidth;
    _footerView.autoresizesSubviews = TRUE;
    _footerView.backgroundColor = [UIColor colorWithRed:0.45 green:0.45 blue:0.45 alpha:1.0];
    [_footerView setRoundedCorners:UIViewRoundedCornerLowerRight | UIViewRoundedCornerLowerLeft radius:5];
    _footerView.hidden = _footerViewHiden;
    
    _tableView = [[RTTableView alloc] initWithFrame:CGRectMake(0, _headerView.bounds.size.height, self.frame.size.width, self.frame.size.height - _headerView.bounds.size.height) style:UITableViewStylePlain];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.sectionHeaderHeight = 0.0;
    _tableView.rowHeight = DEFAULT_ROW_HEIGHT;
    _tableView.backgroundColor = self.backgroundColor = nil;
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    _tableView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth |UIViewAutoresizingFlexibleRightMargin;
    
    
    UIView* backgroundView =[[UIRoundedCornersView alloc]initWithFrame:CGRectZero];
    _tableView.tableFooterView = backgroundView;
    backgroundView.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    backgroundView.backgroundColor = [UIColor whiteColor];
    [backgroundView release];
    
    _scrollView = [[UIScrollView alloc]initWithFrame:self.bounds];
    [self addSubview:_scrollView];
    _scrollView.autoresizesSubviews = YES;
    _scrollView.backgroundColor = nil;
    _scrollView.clipsToBounds = TRUE;
    _scrollView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth |UIViewAutoresizingFlexibleRightMargin;
    _scrollView.showsVerticalScrollIndicator = NO;
    _scrollView.showsHorizontalScrollIndicator = YES;
    _scrollView.bounces = FALSE;
    _scrollView.pagingEnabled = NO;
    [_scrollView addSubview:_headerView];
    [_scrollView addSubview:_tableView];
    [_scrollView addSubview:_footerView];
    
    
    allowCustomization = NO;
    [self setAllowMoveRows:NO];
    
    //self.showsHorizontalScrollIndicator = YES;
    UILongPressGestureRecognizer *lpgr = [[UILongPressGestureRecognizer alloc]
                                          initWithTarget:self action:@selector(handleLongPress:)];
    lpgr.minimumPressDuration = 2.0; //seconds
    [_headerView addGestureRecognizer:lpgr];
    [lpgr release];
    self.scrollPosition = UITableViewScrollPositionMiddle;
    
    _rowIdentifier = [[NSString stringWithFormat:@"Init%@", [[BPUUID UUID]description]] retain];
    selectedRowBackgroundColor = [global_blue_color retain];
    
    updateCounter = 0;
}

-(void)rebuildFooterView{
    
    if (_tableView.tableFooterView) {
        
        [self.tableView layoutIfNeeded];
        UIView* tableFooterView = _tableView.tableFooterView;
        CGFloat contentHeight = _tableView.contentSize.height;
        //ios 10 sets wrong contentSize
        CGFloat contentHeightActual = updateCounter > 0 ? 0 : _tableView.rowHeight * self.rowCount;
        if (contentHeight > contentHeightActual){
            contentHeight = contentHeightActual;
            _tableView.contentSize = CGSizeMake(_tableView.contentSize.width, contentHeight);
        }
        CGRect tableFooterViewFrame = tableFooterView.frame;
        tableFooterViewFrame.size.height =  _tableView.bounds.size.height;
        if (contentHeight < self.bounds.size.height){
            tableFooterViewFrame.size.height = self.bounds.size.height - contentHeight;
            if (tableFooterViewFrame.size.height < _tableView.rowHeight){
                tableFooterViewFrame.size.height = _tableView.rowHeight;
            }
        }
        else{
            tableFooterViewFrame.size.height = _tableView.rowHeight;
        }
        
        tableFooterView.frame = tableFooterViewFrame;
        
        if ([_tableView.tableFooterView isKindOfClass:[UIRoundedCornersView class]]){
            UIRoundedCornersView* tableFooterViewRC = (UIRoundedCornersView*)_tableView.tableFooterView;
            if (_footerViewHiden)
                [tableFooterViewRC setRoundedCorners:UIViewRoundedCornerAll radius:5];
            else
                [tableFooterViewRC setRoundedCorners:UIViewRoundedCornerUpperLeft | UIViewRoundedCornerUpperRight radius:5];
        }
        
    }
}

- (void)layoutSubviews {
    [super layoutSubviews];
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    _tableView.backgroundColor = nil;
    if (_headerView.frame.size.width!=lastLayoutedRect.size.width){
        if ([_delegate respondsToSelector:@selector(gridTableLayoutSubViews:)]){
            [_delegate gridTableLayoutSubViews:self];
        }
    }
    if (_headerView.frame.size.width!=lastLayoutedRect.size.width){
        [self rebuildColumnHeaders];
    }
    [self rebuildFooterView];
    _footerView.frame = CGRectMake(0, self.frame.size.height - _headerView.bounds.size.height, _tableView.bounds.size.width, _headerView.bounds.size.height);
    BOOL headerViewHidden = !self.headerView || self.headerView.superview!=_scrollView || self.headerView.hidden;
    _tableView.frame = CGRectMake(0,
                                  (headerViewHidden ? 0 : _headerView.bounds.size.height),
                                  _tableView.bounds.size.width,
                                  self.frame.size.height - (headerViewHidden ? 0 : _headerView.bounds.size.height) - (_footerViewHiden ? 0 : _headerView.bounds.size.height));
    _scrollView.contentSize = CGSizeMake(_headerView.bounds.size.width, _scrollView.bounds.size.height);
}

+ (UIControlContentHorizontalAlignment)convertTextAlignmentToContentHorizontalAlignment:(NSTextAlignment)alignment{
    switch (alignment) {
        case NSTextAlignmentLeft:
            return UIControlContentHorizontalAlignmentLeft;
        case NSTextAlignmentRight:
            return UIControlContentHorizontalAlignmentRight;
        default:
            return UIControlContentHorizontalAlignmentCenter;
    }
}

- (id)initWithCoder:(NSCoder *)aDecoder{
    if ((self=[super initWithCoder:aDecoder])){
        [self initDefaults];
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame {
    if ((self = [super initWithFrame:frame])) {
        [self initDefaults];
    }
    return self;
}


////////////////////////////////////////////////////////////
-(void)setFooterViewHiden:(BOOL)value {
#ifdef ShowLog
    NSLog(@" %s", __FUNCTION__);
#endif
    
    _footerViewHiden = value;
    _footerView.hidden = _footerViewHiden;
    [self layoutSubviews];
}


////////////////////////////////////////////////////////////
-(NSInteger)sortingColumnIndex {
    
    if (!self.sortingColumnName)
        return NSNotFound;
    
    int idx = 0;
    for (GridTableColumnDescription *descr in _columns) {
        if ([self.sortingColumnName isEqualToString:descr.name])
            return idx;
        idx++;
    }
    
    return NSNotFound;
}


////////////////////////////////////////////////////////////
-(void)setSortingColumnIndex:(NSInteger)value {
    
    if (value==NSNotFound || value < 0 || value >= _columns.count )
        self.sortingColumnName = nil;
    else
        self.sortingColumnName = ((GridTableColumnDescription *)_columns[value]).name;
}


////////////////////////////////////////////////////////////
-(BOOL)footerViewHiden {
#ifdef ShowLog
    NSLog(@" %s", __FUNCTION__);
#endif
    
    return _footerViewHiden;
}

-(BOOL)allowsMultipleSelection {
    return _tableView.allowsMultipleSelection;
}

-(void)setAllowsMultipleSelection:(BOOL)value {
    _tableView.allowsMultipleSelection = value;
}

-(NSIndexSet *)selectedRows {
    NSArray * indexPaths = _tableView.indexPathsForSelectedRows;
    if (indexPaths != nil) {
        NSMutableIndexSet * rows = [[[NSMutableIndexSet alloc] init] autorelease];
        for (NSIndexPath * path in indexPaths) {
            [rows addIndex:path.row];
        }
        return rows;
    }
    return nil;
}

- (void)dealloc {
    
    [_scrollView release];
    [_headerView release];
    [_tableView release];
    [_footerView release];
    [_upArrow release];
    [_downArrow release];
    [_backgroundImage release];
    [_yesImage release];
    [_noImage release];
    [_columns release];
    [_defaultColumns release];
    [_rowIdentifier release];
    [selectedRowBackgroundColor release];
    [rowTextColorSelected release];
    [rowTextColor release];
    [rowBackgroundColorOdd release];
    [rowBackgroundColorEven release];
    [headerFont release];
    [cellFont release];
    
    [_sortingColumnName release];
    [hightlightVerticalLine release];
    [_defaultHorizontalScrollEnabled release];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    [super dealloc];
}


#pragma mark -
#pragma mark HeaderView customization

- (void)handleLongPress:(UILongPressGestureRecognizer *)recognizer {
    if (allowCustomization && !movingView && !(resizedLeftColumnHeader && resizedRightColumnHeader) && recognizer.state == UIGestureRecognizerStateBegan) {
        RTAlertView* alert = [[RTAlertView alloc] initWithTitle:NSLocalizedString(@"GRID_TABLE_CUSTOMIZE_VIEW_TITLE", nil)
                                                        message:@""
                                                       delegate:self
                                              cancelButtonTitle:NSLocalizedString(@"CANCELBTN_TITLE", nil)
                                              otherButtonTitles:NSLocalizedString(@"GRID_TABLE_DEFAULTSBTN_TITLE", nil),
                              NSLocalizedString(@"GRID_TABLE_CUSTOMIZEBTN_TITLE", nil),
                              nil];
        alert.completedBlock = ^(NSInteger buttonIndex){
            if (buttonIndex == 1){
                //[(GridTable *)alertView.delegate resetToDefaults];
                [self resetToDefaults];
            }
            else if (buttonIndex == 2){
                GridColumnsEditorViewController *columnListViewController = [[GridColumnsEditorViewController alloc] initWithGridTable:self];
                [[self.window getFirstResponder] resignFirstResponder];
                CGRect presentRect = CGRectMake(self.center.x + 100, _headerView.center.y, columnListViewController.view.frame.size.width - self.center.x, columnListViewController.view.frame.size.height/2);
                [columnListViewController presentPopoverFromRect:presentRect inView:self permittedArrowDirections:UIPopoverArrowDirectionRight animated:YES];
                [columnListViewController release];
            }
        };
        [alert show];
        [alert release];
    }
    if (movingView){
        CGPoint tapPoint = [recognizer locationInView:nil];
        UIView *viewAtBottomOfHeirachy = [self.window hitTest:tapPoint withEvent:nil];
        
        if ([viewAtBottomOfHeirachy isKindOfClass:[GridTableColumnHeader class]]){
            [self performSelector:@selector(columnHeaderTouchUpInside:withEvent:) withObject:viewAtBottomOfHeirachy withObject:nil];
        }
    }
}

- (void)moveAllColumnsAfterIndex:(NSInteger) index onDistance:(CGFloat) distance {
    CGRect newframe;
    NSArray *visibleCols = [self getDisplayedColumns];
    for (NSInteger i = index+1; i<visibleCols.count; i++) {
        GridTableColumnHeader *header = [self getColumnHeaderByColumnDescription:visibleCols[i]];
        if (header){
            newframe = header.frame;
            newframe.origin.x += distance;
            header.frame = newframe;
        }
    }
    [self updateGridWidth];
}

-(void) highlightVerticalBorder{
    //[[NSNotificationCenter defaultCenter]postNotificationName:gridTableColumnHeaderHighLightBorderNotification object:self userInfo:nil];
    if (resizedLeftColumnHeader){
        [self createHighlightingVerticalBorder:resizedLeftColumnHeader];
    }
    else if (candidateReorderingColumn){
        GridTableColumnHeader* columnHeader = candidateReorderingColumn;
        if (candidateNextToReorderingColumn && candidateNextToReorderingColumn.frame.origin.x < candidateReorderingColumn.frame.origin.x){
            columnHeader =  candidateNextToReorderingColumn;
        }
        [self createHighlightingVerticalBorder:columnHeader];
    }
    else{
        [self createHighlightingVerticalBorder:nil];
    }
}

-(void)createHighlightingVerticalBorder:(GridTableColumnHeader*)columnHeader{
    [hightlightVerticalLine removeFromSuperview];
    [hightlightVerticalLine release];
    hightlightVerticalLine = nil;
    if (!columnHeader) return;
    CGFloat x = columnHeader.frame.origin.x + (columnHeader.highlightedSide == GridTableColumnHeaderHighlightedSideLeft ? 0.0f : columnHeader.frame.size.width);
    x = [self convertPoint:CGPointMake(x, 0) fromView:columnHeader.superview].x;
    if (x == 0){
        x = 1.0f;
    }
    else if (x == self.bounds.size.width){
        x -= 1.0f;
    }
    hightlightVerticalLine = [[UIView alloc]initWithFrame:CGRectMake(x, 0, 1, self.frame.size.height)];
    hightlightVerticalLine.backgroundColor = [UIColor blackColor];
    [self addSubview:hightlightVerticalLine];
}

-(NSInteger)highlightingBorderColumnIndex{
    if (resizedLeftColumnHeader){
        return [[self getDisplayedColumns] indexOfObject:resizedLeftColumnHeader.columnDescription];
    }
    else if (candidateReorderingColumn ||  candidateNextToReorderingColumn){
        NSArray* visibleColumns = [self getDisplayedColumns];
        NSInteger candidateReorderingColumnIndex = candidateReorderingColumn ? [visibleColumns indexOfObject:candidateReorderingColumn.columnDescription] : NSNotFound;
        if (candidateReorderingColumnIndex==0 || candidateReorderingColumnIndex==visibleColumns.count - 1) return NSNotFound;
        NSInteger candidateNextToReorderingColumnIndex = candidateNextToReorderingColumn ? [visibleColumns indexOfObject:candidateNextToReorderingColumn.columnDescription] : NSNotFound;
        return candidateNextToReorderingColumnIndex < candidateReorderingColumnIndex ? candidateNextToReorderingColumnIndex : candidateReorderingColumnIndex;
    }
    return NSNotFound;
}

-(BOOL)checkForResizing:(GridTableColumnHeader*) headerColumn forPoint:(CGPoint)point visibleColumns:(NSArray*)visibleColumns{
    if (!visibleColumns.count) return NO;
    CGFloat touchableBorderWidth = headerColumn.touchableBorderWidth;
    if (!movingView && (resizedLeftColumnHeader || resizedRightColumnHeader ||
                        //right side
                        (point.x >= headerColumn.frame.size.width - touchableBorderWidth ||
                         //left side
                         point.x <= touchableBorderWidth
                         )))
    {
        if (!resizedLeftColumnHeader && !resizedRightColumnHeader){
            if (point.x <= touchableBorderWidth){
                //find header at the left
                NSInteger index = [visibleColumns indexOfObject:headerColumn.columnDescription];
                if (index==NSNotFound || index==0){
                    return FALSE;
                }
                resizedRightColumnHeader = headerColumn;
                resizedLeftColumnHeader = [self getColumnHeaderByColumnDescription:visibleColumns[index - 1]];
            }
            else{
                resizedLeftColumnHeader = headerColumn;
                //find header at the right
                NSInteger index = [visibleColumns indexOfObject:headerColumn.columnDescription];
                if (index!=NSNotFound && index!=(visibleColumns.count - 1)){
                    resizedRightColumnHeader = [self getColumnHeaderByColumnDescription:visibleColumns[index + 1]];
                }
            }
            // highlight borders
            resizedLeftColumnHeader.highlightedSide = GridTableColumnHeaderHighlightedSideRight;
            resizedRightColumnHeader.highlightedSide = GridTableColumnHeaderHighlightedSideLeft;
            [self highlightVerticalBorder];
        }
        return TRUE;
    }
    return FALSE;
}

- (IBAction)columnHeaderTouchDrug:(id) sender withEvent:(UIEvent *) event {
    if (!allowCustomization) return;
    GridTableColumnHeader *headerColumn = sender;
    UITouch* touch = [[event allTouches] anyObject];
    CGPoint pointInHeader = [touch locationInView:headerColumn];
    
    NSArray *visibleColumns = [self getDisplayedColumns];
    // Resizing columns
    if ([self checkForResizing:headerColumn forPoint:pointInHeader visibleColumns:visibleColumns]){
        // get delta
        CGPoint point = (CGPoint)[headerColumn convertPoint:pointInHeader toView:_headerView];
        CGPoint previousPint = [touch previousLocationInView:_headerView];
        CGFloat delta_x = roundf(point.x - previousPint.x);
        
        if (delta_x < 0 && resizedLeftColumnHeader && resizedLeftColumnHeader.columnDescription==visibleColumns[0] && resizedLeftColumnHeader.frame.size.width < 40){
            return;
        }
        NSInteger touchableBorderWidth = headerColumn.touchableBorderWidth;
        if(!self.horizontalScrollEnabled){
            GridTableColumnHeader* mostRightColumnHeader = [self getColumnHeaderByColumnDescription:visibleColumns[visibleColumns.count - 1]];
            //disable resizing by dragging of last column's right side
            if (headerColumn == mostRightColumnHeader && pointInHeader.x >= headerColumn.frame.size.width - touchableBorderWidth){
                return;
            }
            //stop resizing to right of most right columns became small
            if (delta_x > 0 && mostRightColumnHeader.frame.size.width < 40){
                return;
            }
        }
        CGRect newframe = resizedLeftColumnHeader.frame;
        newframe.size.width+=delta_x;
        if (newframe.size.width > touchableBorderWidth){
            hightlightVerticalLine.center = CGPointMake(hightlightVerticalLine.center.x + delta_x,hightlightVerticalLine.center.y);
            resizedLeftColumnHeader.frame = newframe;
            resizedLeftColumnHeader.columnDescription.width = newframe.size.width;
            NSInteger index = [visibleColumns indexOfObject:resizedLeftColumnHeader.columnDescription];
            [self moveAllColumnsAfterIndex:index onDistance:delta_x];
        }
        return;
    }
    
    // Reordering column
    if (!movingView) {
        // Set fake image
        movingView = [[UIImageView alloc] initWithImage:[headerColumn createDruggingImage]];
        movingView.frame = headerColumn.frame;
        movingView.layer.borderColor = [UIColor darkGrayColor].CGColor;
        movingView.layer.borderWidth = 1.0f;
        movingView.layer.shadowOffset = CGSizeMake(3, 3);
        movingView.layer.shadowOpacity = 0.5f;
        movingView.alpha = 0.6f;
        [_headerView addSubview: movingView];
        [movingView release];
    }
    
    CGPoint point = (CGPoint)[headerColumn convertPoint:pointInHeader toView:_headerView];
    // Restrictions on movement
    point.y = _headerView.bounds.size.height/2;
    
    // get delta
    CGPoint previousPint = [touch previousLocationInView:_headerView];
    CGFloat delta_x = point.x - previousPint.x;
    movingView.center = CGPointMake(movingView.center.x + delta_x,movingView.center.y);
    
    UIView* candidateView = [_headerView hitTest:point withEvent:nil];
    BOOL resetCandidate = FALSE;
    if (candidateView==headerColumn){
        resetCandidate = TRUE;
    }
    else if ([candidateView isKindOfClass:[GridTableColumnHeader class]]){
        //left
        if (point.x < headerColumn.center.x){
            if (point.x < candidateView.center.x){
                if (candidateReorderingColumn!=candidateView){
                    //reset highlighting of old candidates
                    candidateReorderingColumn.highlightedSide = GridTableColumnHeaderHighlightedSideNone;
                    candidateNextToReorderingColumn.highlightedSide = GridTableColumnHeaderHighlightedSideNone;
                    
                    candidateReorderingColumn = (GridTableColumnHeader*)candidateView;
                    candidateReorderingColumn.highlightedSide = GridTableColumnHeaderHighlightedSideLeft;
                    NSInteger candidateIndex = [visibleColumns indexOfObject:candidateReorderingColumn.columnDescription];
                    if (candidateIndex!=NSNotFound && candidateIndex > 0){
                        candidateNextToReorderingColumn = [self getColumnHeaderByColumnDescription:visibleColumns[candidateIndex-1]];
                        candidateNextToReorderingColumn.highlightedSide = GridTableColumnHeaderHighlightedSideRight;
                    }
                    [self highlightVerticalBorder];
                }
            }
            else{
                if (candidateReorderingColumn==candidateView){
                    NSInteger currentIndex = [visibleColumns indexOfObject:headerColumn.columnDescription];
                    NSInteger candidateIndex = [visibleColumns indexOfObject:candidateReorderingColumn.columnDescription];
                    if (candidateIndex==currentIndex - 1){
                        resetCandidate = TRUE;
                    }
                }
            }
        }
        //right
        else if (point.x > headerColumn.center.x){
            if (point.x > candidateView.center.x){
                if (candidateReorderingColumn!=candidateView){
                    //reset highlighting of old candidates
                    candidateReorderingColumn.highlightedSide = GridTableColumnHeaderHighlightedSideNone;
                    candidateNextToReorderingColumn.highlightedSide = GridTableColumnHeaderHighlightedSideNone;
                    
                    candidateReorderingColumn = (GridTableColumnHeader*)candidateView;
                    candidateReorderingColumn.highlightedSide = GridTableColumnHeaderHighlightedSideRight;
                    NSInteger candidateIndex = [visibleColumns indexOfObject:candidateReorderingColumn.columnDescription];
                    if (candidateIndex!=NSNotFound && visibleColumns.count > candidateIndex + 1){
                        candidateNextToReorderingColumn = [self getColumnHeaderByColumnDescription:visibleColumns[candidateIndex+1]];
                        candidateNextToReorderingColumn.highlightedSide = GridTableColumnHeaderHighlightedSideLeft;
                    }
                    [self highlightVerticalBorder];
                }
            }
            else{
                if (candidateReorderingColumn==candidateView){
                    NSInteger currentIndex = [visibleColumns indexOfObject:headerColumn.columnDescription];
                    NSInteger candidateIndex = [visibleColumns indexOfObject:candidateReorderingColumn.columnDescription];
                    if (candidateIndex==currentIndex + 1){
                        resetCandidate = TRUE;
                    }
                }
            }
        }
    }
    if (resetCandidate){
        candidateReorderingColumn.highlightedSide = GridTableColumnHeaderHighlightedSideNone;
        candidateNextToReorderingColumn.highlightedSide = GridTableColumnHeaderHighlightedSideNone;
        candidateReorderingColumn = candidateNextToReorderingColumn = nil;
        [self highlightVerticalBorder];
    }
}

- (void)setAllowMoveRows:(BOOL)value {
    [_tableView setEditing:value];
}

- (BOOL)allowMoveRows {
    return _tableView.editing;
}

- (GridTableColumnHeader *)createColumnHeader: (GridTableColumnDescription *)columnDescription frame:(CGRect)frame {
    
    GridTableColumnHeader *columnHeader = [GridTableColumnHeader buttonWithType:UIButtonTypeCustom];
    columnHeader.frame = frame;
    columnHeader.backgroundColor = [UIColor colorWithRed:0.48 green:0.48 blue:0.48 alpha:1.0];
    columnHeader.titleLabel.adjustsFontSizeToFitWidth = YES;
    columnHeader.titleLabel.font = self.headerFont;
    columnHeader.titleLabel.minimumScaleFactor = 0.75;
    [columnHeader setTitle:columnDescription.title forState:UIControlStateNormal];
    columnHeader.adjustsImageWhenHighlighted = YES;
    columnHeader.enabled = YES;
    //columnHeader.userInteractionEnabled = column.sortable;
    columnHeader.contentHorizontalAlignment = [[self class] convertTextAlignmentToContentHorizontalAlignment:columnDescription.horizontalAlignment];
    columnHeader.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    columnHeader.contentEdgeInsets  = UIEdgeInsetsMake(5, 5, 5, 15);
    //	[columnHeader setBackgroundImage:_backgroundImage forState:UIControlStateNormal];
    columnHeader.columnDescription = columnDescription;
    columnHeader.touchableBorderWidth = 20;
    
    [columnHeader addTarget:self action:@selector(columnHeaderTouchDown:withEvent:) forControlEvents:UIControlEventTouchDown];
    [columnHeader addTarget:self action:@selector(columnHeaderTouchUpInside:withEvent:) forControlEvents:UIControlEventTouchUpInside];
    [columnHeader addTarget:self action:@selector(columnHeaderTouchUpOutside:) forControlEvents:UIControlEventTouchUpOutside | UIControlEventTouchCancel];
    [columnHeader addTarget:self action:@selector(columnHeaderTouchDrug:withEvent:) forControlEvents:UIControlEventTouchDragInside | UIControlEventTouchDragOutside];
    
    if (_sortingMode != NotSorted && self.sortingColumnName && [self.sortingColumnName isEqualToString:columnDescription.name]) {
        
        UIView * sortingSignView = _sortingMode == AscendingSorted ? _upArrow : _downArrow;
        CGRect frameSSV = sortingSignView.bounds;
        CGPoint center = columnHeader.center;
        center.x = columnHeader.frame.size.width - frameSSV.size.width - 1;
        sortingSignView.center = center;
        [columnHeader addSubview:sortingSignView];
        sortingSignView.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin;
    }
    
    return columnHeader;
}

-(CGFloat)spaceBetweenColumns{
    return 1.0f;
}

-(BOOL)restrictWidthToFrame{
    return TRUE;
}

-(void)updateGridWidth{
    GridTableColumnHeader* mostRightHeader = nil;
    CGFloat mostRightHeaderX = 0.0f;
    for (NSInteger i = 0; i < _headerView.subviews.count; i++) {
        GridTableColumnHeader* view = _headerView.subviews[i];
        if ([view isKindOfClass:[GridTableColumnHeader class]]) {
            CGFloat x = view.frame.size.width + view.frame.origin.x;
            if (x > mostRightHeaderX){
                mostRightHeaderX = x;
                mostRightHeader = view;
            }
        }
    }
    if (self.horizontalScrollEnabled){
        CGRect newHeaderFrame = _headerView.frame;
        newHeaderFrame.size.width = mostRightHeaderX;
        _headerView.frame = newHeaderFrame;
    }
    else{
        _headerView.frame = CGRectMake(0, 0, self.frame.size.width, DEFAULT_HEADER_HEIGHT);
        if (mostRightHeader){
            if (mostRightHeader.frame.origin.x + mostRightHeader.frame.size.width !=  _headerView.bounds.size.width){
                CGRect frame = mostRightHeader.frame;
                frame.size.width = _headerView.bounds.size.width - mostRightHeader.frame.origin.x;
                mostRightHeader.frame = frame;
                //mostLeftHeader.column.width = frame.size.width;
            }
        }
    }
    _tableView.frame = CGRectMake(_tableView.frame.origin.x, _tableView.frame.origin.y, _headerView.bounds.size.width, _tableView.frame.size.height);
    CGPoint oldContentOffset = _scrollView.contentOffset;
    _scrollView.contentSize = CGSizeMake(_headerView.bounds.size.width, _scrollView.bounds.size.height);
    _scrollView.scrollEnabled = self.horizontalScrollEnabled;
    //do not move grid content position since it being resized
    if (resizedLeftColumnHeader || resizedRightColumnHeader){
        _scrollView.contentOffset = oldContentOffset;
    }
    lastLayoutedRect = _headerView.frame;
    [[NSNotificationCenter defaultCenter]postNotificationName:gridTableColumnHeaderFrameChangedNotification object:self userInfo:nil];
}

- (void)rebuildColumnHeaders {
    NSMutableDictionary* currentVisibleHeaders = [[NSMutableDictionary alloc]init];
    for (NSInteger i = 0; i < _headerView.subviews.count; i++) {
        GridTableColumnHeader* view = _headerView.subviews[i];
        if ([view isKindOfClass:[GridTableColumnHeader class]]) {
            currentVisibleHeaders[view.columnDescription.id.description] = view;
        }
    }
    CGRect rect = CGRectMake(0,0, 0, _headerView.frame.size.height);
    CGFloat gridWidth =  _headerView.frame.size.width;
    NSArray *visibleColumns = [self getVisibleColumns];
    NSInteger lastColumnIndex = [visibleColumns count] - 1;
    BOOL stop = FALSE;
    for (NSInteger i = 0; i <= lastColumnIndex ; i++) {
        GridTableColumnDescription * col = visibleColumns[i];
        rect.size.width = col.width;
        if (!self.horizontalScrollEnabled && (i == lastColumnIndex ||  (rect.origin.x + rect.size.width) >= gridWidth)) {
            //adjust last column to right side and stop processing rest columns
            rect.size.width = gridWidth - rect.origin.x;
            stop = TRUE;
        }
        GridTableColumnHeader *columnHeader = currentVisibleHeaders[col.id.description];
        if (!columnHeader){
            columnHeader = [self createColumnHeader:col frame:rect];
            [_headerView addSubview:columnHeader];
        }
        else{
            columnHeader.frame = rect;
            columnHeader.columnDescription = col;
            [columnHeader setTitle:col.title forState:UIControlStateNormal];
        }
        [currentVisibleHeaders removeObjectForKey:col.id.description];
        columnHeader.highlightedSide = GridTableColumnHeaderHighlightedSideNone;
        if (stop){
            break;
        }
        if (i != lastColumnIndex){
            rect.origin.x += rect.size.width + self.spaceBetweenColumns;
        }
        if (!self.horizontalScrollEnabled && rect.origin.x >= gridWidth){
            break;
        }
    }
    //remove currently non visible columns
    for (UIView* view in currentVisibleHeaders.allValues){
        [view removeFromSuperview];
    }
    [currentVisibleHeaders release];
    
    [self updateGridWidth];
}

-(void)columnHeaderTouchUpOutside:(id)sender {
    GridTableColumnHeader *senderButton = (GridTableColumnHeader*)sender;
    if (resizedLeftColumnHeader || resizedRightColumnHeader) {
        resizedLeftColumnHeader.highlightedSide = GridTableColumnHeaderHighlightedSideNone;
        resizedRightColumnHeader.highlightedSide = GridTableColumnHeaderHighlightedSideNone;
        [self rebuildColumnHeaders];
        resizedLeftColumnHeader = resizedRightColumnHeader = nil;
        [self highlightVerticalBorder];
        return;
    }
    
    if (movingView) {
        //remove image view
        [movingView removeFromSuperview];
        movingView = nil;
        
        if (candidateReorderingColumn){
            GridTableColumnHeader* headerColumn = senderButton;
            NSInteger currentIndex = [_columns indexOfObject:headerColumn.columnDescription];
            NSInteger candidateIndex = [_columns indexOfObject:candidateReorderingColumn.columnDescription];
            
            if (_sortingMode != NotSorted) {
                if (self.sortingColumnName && [self.sortingColumnName isEqualToString:headerColumn.columnDescription.name])
                    self.sortingColumnName = candidateReorderingColumn.columnDescription.name;
                else if (self.sortingColumnName && [self.sortingColumnName isEqualToString:candidateReorderingColumn.columnDescription.name])
                    self.sortingColumnName = headerColumn.columnDescription.name;
            }
            
//            GridTableColumnDescription* neighborCol = candidateReorderingColumn.columnDescription;
//            _columns[candidateIndex] = headerColumn.columnDescription;
//            _columns[currentIndex] = neighborCol;
            
            [self moveArrayItemAtIndex:currentIndex afterIndex:candidateIndex array:_columns];
            
            candidateReorderingColumn = candidateNextToReorderingColumn = nil;
            [self highlightVerticalBorder];
            [UIView animateWithDuration:0.3f animations:^{
                [self rebuildColumnHeaders];
            }];
        }
    }
}

-(void)moveArrayItemAtIndex:(NSUInteger)startIndex afterIndex:(NSUInteger)endIndex array:(NSMutableArray*)array {
    NSObject* objtoDrag = array[startIndex];
    [array removeObject:objtoDrag];
    
    [array insertObject:objtoDrag atIndex:endIndex];
}

- (void)columnHeaderTouchDown:(id)sender  withEvent:(UIEvent *) event{
    if (!allowCustomization) return;
    GridTableColumnHeader *headerColumn = (GridTableColumnHeader*)sender;
    UITouch* touch = [[event allTouches] anyObject];
    CGPoint pointInHeader = [touch locationInView:headerColumn];
    
    if ([self checkForResizing:headerColumn forPoint:pointInHeader visibleColumns:[self getDisplayedColumns]]){
        return;
    }
}


- (void)columnHeaderTouchUpInside:(id)sender  withEvent:(UIEvent *) event{
    
    if (resizedLeftColumnHeader || resizedRightColumnHeader) {
        
        resizedLeftColumnHeader.highlightedSide = GridTableColumnHeaderHighlightedSideNone;
        resizedRightColumnHeader.highlightedSide = GridTableColumnHeaderHighlightedSideNone;
        [self rebuildColumnHeaders];
        resizedLeftColumnHeader = resizedRightColumnHeader = nil;
        [self highlightVerticalBorder];
        return;
    }
    
    GridTableColumnHeader *headerColumn = (GridTableColumnHeader*)sender;
    
    if (movingView) {
        //remove image view
        [movingView removeFromSuperview];
        movingView = nil;
        candidateReorderingColumn = candidateNextToReorderingColumn = nil;
        headerColumn.highlightedSide = GridTableColumnHeaderHighlightedSideNone;
    }
    else if (headerColumn.columnDescription.sortable){
        NSInteger indx = [_columns indexOfObject:headerColumn.columnDescription];
        [self sortByColumn:indx
                     order:(self.sortingColumnName != nil && [self.sortingColumnName isEqualToString:headerColumn.columnDescription.name])
         ? (_sortingMode + 1) % 3
                          : AscendingSorted ];
    }
}

- (void)setHeaderFont:(UIFont *)value{
    [headerFont release];
    headerFont = [value retain];
    for (UIView* view in _headerView.subviews){
        if ([view isKindOfClass:[GridTableColumnHeader class]]){
            ((GridTableColumnHeader*)view).titleLabel.font = self.headerFont;
        }
    }
    
}

- (GridTableColumnDescription*)sortingColumn{
    
    NSInteger idx = self.sortingColumnIndex;
    
    if (idx == NSNotFound || idx > (_columns.count - 1))
        return nil;
    
    return _columns[idx];
}

- (SortingMode)sortingMode{
    return _sortingMode;
}

- (void)sortByColumn:(NSInteger)columnIndex order:(SortingMode)order{
    
    GridTableColumnDescription *columDescr = nil;
    
    if (order!= NotSorted && (columnIndex >= _columns.count || columnIndex < 0))
        order = NotSorted;
    
    _sortingMode = order;
    [_upArrow removeFromSuperview];
    [_downArrow removeFromSuperview];
    
    if (_sortingMode != NotSorted) {
        
        columDescr = _columns[columnIndex];
        UIView * sortingSignView = _sortingMode == AscendingSorted ? _upArrow : _downArrow;
        self.sortingColumnName = columDescr.name;
        UIView* columnHeader = [self getColumnHeaderByColumnDescription:columDescr];
        CGRect frame = sortingSignView.bounds;
        CGPoint center = columnHeader.center;
        center.x = columnHeader.frame.size.width - frame.size.width - 1;
        sortingSignView.center = center;
        if (columDescr.width >= frame.size.width) {
            [columnHeader addSubview:sortingSignView];
            sortingSignView.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin;
        }
    }
    else
        self.sortingColumnName = nil;
    
    if (_delegate) {
        
        if ([_delegate respondsToSelector:@selector(gridTable:willSortByColumn:order:)]){
            [_delegate gridTable:self willSortByColumn:columDescr order:_sortingMode];
        }
        [_delegate gridTable:self sortByColumn:columDescr order:_sortingMode];
        UITableViewScrollPosition old_scrollPosition = scrollPosition;
        scrollPosition = UITableViewScrollPositionNone;
        [self reloadData];
        scrollPosition = old_scrollPosition;
        if ([_delegate respondsToSelector:@selector(gridTable:didSortByColumn:order:)]){
            [_delegate gridTable:self didSortByColumn:columDescr order:_sortingMode];
        }
    }
}

#pragma mark -

- (void)rebuildGrid {
    [_rowIdentifier release];
    _rowIdentifier = [[NSString stringWithFormat:@"ReloadGrid%@", [[BPUUID UUID]description]] retain];
    [self rebuildColumnHeaders];
    //NSLog(@"%@", _rowIdentifier);
    [self reloadData];
}


- (void)resetToDefaults {
    
    if (_sortingMode != NotSorted) {
        
        NSString *sortingCol = [self sortingColumn].name;
        bool flag = false;
        
        for (int i=0; i<_defaultColumns.count; i++) {
            
            GridTableColumnDescription *col = _defaultColumns[i];
            
            if ([col.name isEqualToString:sortingCol]) {
                
                self.sortingColumnName = col.name;
                flag = true;
                break;
            }
        }
        if (!flag) {
            
            self.sortingColumnName = nil;
            _sortingMode = NotSorted;
        }
    }
    
    if (_columns)
        [_columns release];
    
    _columns = [[NSMutableArray alloc] initWithArray:_defaultColumns copyItems:YES];
    _horizontalScrollEnabled = self.defaultHorizontalScrollEnabled;
    [self rebuildGrid];
}


-(void)selectRowInternal:(NSInteger)oldSelectedRowIndex{
    
    NSInteger idx = oldSelectedRowIndex;
    if (idx < 0 || idx == NSNotFound) idx = -1;
    NSInteger numberOfRows = [_delegate gridTableNumberOfRows:self];
    if (!numberOfRows){
        //do fake deselect
        NSIndexPath* indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
        if (indexPath){
            [self tableView:_tableView didDeselectRowAtIndexPath:indexPath];
        }
        return;
    }
    if (self.selectRowOnReload){
        if (idx >= numberOfRows){
            idx = numberOfRows - 1;
        }
        if (idx < 0 && numberOfRows)
            idx = 0;
        if (idx >= 0 && numberOfRows) {
            [self selectRow:idx animated:FALSE];
        }
    }
    else{
        if (idx>=0){
            [self tableView:_tableView didDeselectRowAtIndexPath:[NSIndexPath indexPathForRow:idx inSection:0]];
        }
    }
}

-(void)selectRowsInternal:(NSIndexSet*)oldSelectedRowIndexs{
    
    if (!oldSelectedRowIndexs)
        return;
    
    NSInteger numberOfRows = [_delegate gridTableNumberOfRows:self];
    if (!numberOfRows){
        
        //do fake deselect
        NSIndexPath* indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
        if (indexPath)
            [self tableView:_tableView didDeselectRowAtIndexPath:indexPath];
        return;
    }
    
    if (self.selectRowOnReload)
        [oldSelectedRowIndexs enumerateIndexesUsingBlock:^(NSUInteger idx, BOOL *stop) {
            
            if (idx >= numberOfRows)
                return;
            
            [self selectRow:idx animated:FALSE];
        }];
}


- (void)reloadData {
    
    if (updateCounter > 0)
        return;
    
    NSInteger idx = NSNotFound;
    NSIndexSet *idxs = nil;
    
    if (!self.allowsMultipleSelection)
        idx = [self selectedRowIndex];
    else
        idxs = [[self.selectedRows copy] autorelease];
    
    [_tableView reloadData];
    
    if (!self.allowsMultipleSelection)
        [self selectRowInternal:idx];
    else
        [self selectRowsInternal:idxs];
    [self rebuildFooterView];
    
}

-(void)reloadRow:(NSInteger)row {
    [_tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:row inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
}

- (void)endEditing {
    if (_activeField != nil) {
        [_activeField resignFirstResponder];
    }
}

#pragma mark -
#pragma mark Edit table rows

- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)sourceIndexPath toIndexPath:(NSIndexPath *)destinationIndexPath {
    if ([_delegate respondsToSelector:@selector(gridTable:reorderRowAtIndexPath:toIndexPath:)])
        [_delegate gridTable:self reorderRowAtIndexPath:sourceIndexPath toIndexPath:destinationIndexPath];
}

- (BOOL)tableView:(UITableView *)tableView shouldIndentWhileEditingRowAtIndexPath:(NSIndexPath *)indexPath{
    return FALSE;
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewCellEditingStyleNone;
}

#pragma mark -
-(NSArray*)columns{
    return _columns;
}

- (void)setColumns:(NSArray *)newColumns {
    if (_columns != newColumns) {
        [_columns release];
        _columns = [[NSMutableArray alloc]initWithCapacity:newColumns.count];
        [newColumns enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            id object = obj;
            if ([_columns indexOfObject:object]!=NSNotFound){
                object = [[obj copy]autorelease];
                ((GridTableColumnDescription*)object).id = [BPUUID UUID];
            }
            [_columns addObject:object];
            
        }];
        if (!_defaultColumns){
            [_defaultColumns release];
            _defaultColumns = [[NSArray alloc] initWithArray:_columns copyItems:YES];
        }
        [self rebuildColumnHeaders];
    }
}

-(void)clearDefaults {
    [_defaultColumns release];
    _defaultColumns = nil;
}

-(BOOL)defaultHorizontalScrollEnabled{
    return _defaultHorizontalScrollEnabled.boolValue;
}

-(void)setDefaultHorizontalScrollEnabled:(BOOL)defaultHorizontalScrollEnabled{
    [_defaultHorizontalScrollEnabled release];
    _defaultHorizontalScrollEnabled = [[NSNumber alloc]initWithBool:defaultHorizontalScrollEnabled];
}

- (NSArray *)getVisibleColumns {
    NSMutableArray *visibleCols = [[NSMutableArray alloc] init];
    for (GridTableColumnDescription *col in _columns) {
        if (col.visible) {
            [visibleCols addObject:col];
        }
    }
    return [visibleCols autorelease];
}

- (NSArray *)getDisplayedColumns {
    NSMutableArray *visibleCols = [[NSMutableArray alloc] init];
    NSMutableArray *displayedCols = [[NSMutableArray alloc] init];
    for (NSInteger i = 0; i < _headerView.subviews.count; i++) {
        GridTableColumnHeader* view = _headerView.subviews[i];
        if ([view isKindOfClass:[GridTableColumnHeader class]]) {
            [displayedCols addObject:view.columnDescription];
        }
    }
    
    for (GridTableColumnDescription *col in _columns) {
        if ([displayedCols indexOfObject:col]!=NSNotFound) {
            [visibleCols addObject:col];
        }
    }
    [displayedCols release];
    return [visibleCols autorelease];
}

- (void)setDelegate:(id<GridTableDelegate>)newDelegate {
    _delegate = newDelegate;
    [_delegate respondsToSelector:@selector(gridTable:heightForRow:)];
    //[self reloadData];
}

-(BOOL)isRowVisible:(NSInteger)row{
    if ([_tableView numberOfRowsInSection:0] <= row || row < 0) return FALSE;
    NSArray *visibleRows = [_tableView indexPathsForVisibleRows];
    for (NSIndexPath *index in visibleRows) {
        if (index.row == row) {
            return TRUE;
        }
    }
    return FALSE;
}

- (void)selectRow:(NSInteger)row {
    [self selectRow:row animated:YES];
}

-(void)deselectRow:(NSInteger)row {
    [self deselectRow:row animated:YES];
}

- (void)removeSelection:(BOOL)animated {
    NSInteger index = [self selectedRowIndex];
    if (NSNotFound==index) return;
    NSIndexPath* indexPath = [NSIndexPath indexPathForRow:index inSection:0];
    [self tableView:_tableView willDeselectRowAtIndexPath:indexPath];
    [_tableView deselectRowAtIndexPath:indexPath animated:animated];
    [self tableView:_tableView didDeselectRowAtIndexPath:indexPath];
}

- (void)selectRow:(NSInteger)row animated:(BOOL)animated {
    if (!firstReloadDataWithDelegateProcessed){
        defferedSelectedRow = row;
        return;
    }
    defferedSelectedRow = NSNotFound;
    if ([_tableView numberOfRowsInSection:0] <= row || row < 0) return;
    //if ([self selectedRowIndex]==row) return;
    NSIndexPath* indexPath = [NSIndexPath indexPathForRow:row inSection:0];
    indexPath = [self tableView:_tableView willSelectRowAtIndexPath:indexPath];
    if (!indexPath) return;
    [_tableView selectRowAtIndexPath:indexPath animated:animated scrollPosition:self.scrollPosition];
    [self tableView:_tableView didSelectRowAtIndexPath:indexPath];
}

-(void)deselectRow:(NSInteger)row animated:(BOOL)animated {
    if ([_tableView numberOfRowsInSection:0] <= row || row < 0) return;
    NSIndexPath* indexPath = [NSIndexPath indexPathForRow:row inSection:0];
    [self tableView:_tableView willDeselectRowAtIndexPath:indexPath];
    [_tableView deselectRowAtIndexPath:indexPath animated:animated];
    [self tableView:_tableView didDeselectRowAtIndexPath:indexPath];
}

- (NSInteger)rowCount {
    return [self tableView:_tableView numberOfRowsInSection:0];
}

- (GridTableRow *)cellForRow:(NSInteger)row {
    if ([_tableView numberOfRowsInSection:0] <= row) return nil;
    NSIndexPath* indexPath = [NSIndexPath indexPathForRow:row inSection:0];
    return [_tableView cellForRowAtIndexPath:indexPath];
}

- (UIView *)contentForRow:(NSInteger)row column:(NSString *)columnName {
    GridTableRow *gridRow = [self cellForRow:row];
    if (gridRow != nil) {
        GridTableCell *gridCell = [gridRow getCellViewByColumnName:columnName];
        if (gridCell != nil) {
            return [gridCell getSubView];
        }
    }
    return nil;
}

- (void)formatCellForRow:(NSInteger)row column:(GridTableColumnDescription *)column content:(id)content selected:(BOOL)selected initial:(BOOL)initial {
    if (_delegate && [_delegate respondsToSelector:@selector(gridTable:formatCellForRow:column:content:selected:initial:)]) {
        [_delegate gridTable:self formatCellForRow:row column:column content:content selected:selected initial:NO];
    }
}

#pragma mark -
#pragma mark Table view delegate

- (NSIndexPath *)tableView:(UITableView *)tableView willDeselectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (_delegate && [_delegate respondsToSelector:@selector(gridTable:willDeselectRow:)]) {
        if (![_delegate gridTable:self willDeselectRow:indexPath.row]) {
            return nil;
        }
    }
    return indexPath;
}

- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSIndexPath *selRow = [tableView indexPathForSelectedRow];
    if ([selRow isEqual:indexPath]) return indexPath;
    if (_delegate && [_delegate respondsToSelector:@selector(gridTable:willSelectRow:)]) {
        if (![_delegate gridTable:self willSelectRow:indexPath.row]) {
            return nil;
        }
    }
    return indexPath;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (_delegate && [_delegate respondsToSelector:@selector(gridTable:didSelectRow:)]) {
        [_delegate gridTable:self didSelectRow:indexPath.row];
    }
}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (_delegate && [_delegate respondsToSelector:@selector(gridTable:didDeselectRow:)]) {
        [_delegate gridTable:self didDeselectRow:indexPath.row];
    }
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if ((indexPath.row % 2) == 0) {
        cell.backgroundColor = self.rowBackgroundColorEven;
    }
    else {
        cell.backgroundColor = self.rowBackgroundColorOdd;
    }
    if (_delegate && [_delegate respondsToSelector:@selector(gridTable:willDisplayRow:gridTableRow:)]) {
        [_delegate gridTable:self willDisplayRow:indexPath.row gridTableRow:(GridTableRow*)cell];
    }
    if (_delegate && [_delegate respondsToSelector:@selector(gridTable:formatCellForRow:column:content:selected:initial:)]) {
        for (NSInteger i = 0; i < _columns.count; i++) {
            GridTableColumnDescription *column = _columns[i];
            [self formatCellForRow:indexPath.row column:column content:[(GridTableRow*)cell getCellSubViewByColumn:column] selected:cell.selected initial:NO];
        }
    }
}



#pragma mark -
#pragma mark Table view data source

-(void)tableViewDidEndLayoutSubviews:(UITableView*)tableView{
    _endingLayoutSubviews = TRUE;
    if (!firstLoadProcessed){
        if (self.selectedRowIndex==NSNotFound){
            [self selectRowInternal:0];
        }
        firstLoadProcessed = TRUE;
    }
    if (!firstReloadDataWithDelegateProcessed && _delegate){
        firstReloadDataWithDelegateProcessed = TRUE;
        if (defferedSelectedRow!=NSNotFound){
            [self selectRow:defferedSelectedRow animated:FALSE];
        }
    }
    _endingLayoutSubviews = FALSE;
}

//-(void)tableViewDidEndReloadData:(UITableView*)tableView{
//    if (!firstReloadDataWithDelegateProcessed && _delegate){
//        firstReloadDataWithDelegateProcessed = TRUE;
//        if (defferedSelectedRow!=NSNotFound){
//            [self selectRow:defferedSelectedRow animated:FALSE];
//        }
//    }
//}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (_delegate != nil) {
        return [_delegate gridTableNumberOfRows:self];
    }
    return 0;
}

- (NSArray*)getColumnHeadersByColumnName:(NSString*) name {
    if (!name) return nil;
    NSMutableArray* list = [[[NSMutableArray alloc]init]autorelease];
    NSArray *subviews = [_headerView subviews];
    for (GridTableColumnHeader* view in subviews) {
        if ([view isKindOfClass:[GridTableColumnHeader class]]) {
            if ([view.columnDescription.name isEqualToString:name]) {
                [list addObject:view];
            }
        }
    }
    return list;
}

- (GridTableColumnHeader *)getColumnHeaderByColumnDescription:(GridTableColumnDescription*)columnDescription {
    if (!columnDescription) return nil;
    NSArray *subviews = [_headerView subviews];
    for (GridTableColumnHeader* view in subviews) {
        if ([view isKindOfClass:[GridTableColumnHeader class]]) {
            if (view.columnDescription==columnDescription) {
                return view;
            }
        }
    }
    for (GridTableColumnHeader* view in subviews) {
        if ([view isKindOfClass:[GridTableColumnHeader class]]) {
            if ([view.columnDescription isEqual:columnDescription]) {
                return view;
            }
        }
    }
    return nil;
}

-(void)getRectsForColumn:(GridTableColumnDescription*)column cellRect:(CGRect*)cRect contentRect:(CGRect*)contentRect rowHeight:(CGFloat)rowHeight{
    if (!cRect && !contentRect) return;
    GridTableColumnHeader * columnHeader = [self getColumnHeaderByColumnDescription:column];
    CGRect cellRect = CGRectMake(columnHeader.frame.origin.x, 1, columnHeader.frame.size.width, rowHeight - 2);
    if (cRect){
        *cRect  = cellRect;
    }
    if (contentRect){
        *contentRect = CGRectMake(columnHeader.contentEdgeInsets.left, 0, cellRect.size.width - columnHeader.contentEdgeInsets.right - columnHeader.contentEdgeInsets.left, cellRect.size.height);
    }
}


-(CGRect)getDefaultContentRectForColumn:(GridTableColumnDescription*)column{
    CGRect rect = CGRectNull;
    [self getRectsForColumn:column cellRect:nil contentRect:&rect rowHeight:self.rowHeight];
    return rect;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    UIColor *backColor = self.rowBackgroundColorOdd;
    if ((indexPath.row % 2) == 0)
        backColor = self.rowBackgroundColorEven;
    
    GridTableRow *row = [tableView dequeueReusableCellWithIdentifier:_rowIdentifier];
    
    BOOL createCell = FALSE;
    if (!row) {
        createCell = TRUE;
        row = [[[GridTableRow alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:_rowIdentifier gridTable:self] autorelease];
        row.delegate = self;
    }
    else{
        [row didDequeue];
        if (!CGSizeEqualToSize(row.frame.size, row.lastDrawnFrame.size)){
            [row setNeedsDisplay];
        }
    }
    NSMutableArray *columns = [NSMutableArray arrayWithArray:[self getVisibleColumns]];
    for (NSInteger i = 0; i <= columns.count - 1 && columns.count!=0; i++) {
        GridTableColumnDescription *column = columns[i];
        CGRect cellRect = CGRectNull;
        CGRect contentRect = CGRectNull;
        CGFloat rowHeight = 0.0f;
        rowHeight = row.contentView.frame.size.height;
        [self getRectsForColumn:column cellRect:&cellRect contentRect:&contentRect rowHeight:rowHeight];
        
        GridTableCell* cellView = nil;
        UIView * columnView = nil;
        if (createCell){
            switch (column.type) {
                case TextColumn:
                    if (column.editable){
                        cellView = [GridTableCellTextField alloc];
                    }
                    else{
                        cellView = [GridTableCellLabel alloc];
                    }
                    break;
                case BoolColumn:
                    cellView = [GridTableCellBoolean alloc];
                    break;
                case ColoredTextColumn:
                    cellView = [GridTableCellColoredTextColumn alloc];
                    break;
                case ButtonColumn:
                    cellView = [GridTableCellButton alloc];
                    break;
                case ImageWithTextColumn:
                    cellView = [GridTableCellImageWithText alloc];
                    break;
                case CustomColumn:
                    cellView = [column.customCellClass alloc];
                    break;
                case ListColumn:
                    cellView = [GridTableCellList alloc];
                    break;
                case ImageColumn:
                    cellView = [GridTableCellImage alloc];
                    break;
                default:
                    break;
            }
            cellView = [cellView initWithFrame:cellRect dataFrame:contentRect row:row column:column];
            [row.contentView addSubview:cellView];
        }
        else{
            cellView = [row getCellViewByColumn:column];
        }
        row.backgroundColor = backColor;
        cellView.backgroundColor = [UIColor clearColor];
        [cellView applyDefaultProperties:contentRect];
        columnView = [cellView getSubView];
        columnView.backgroundColor = [UIColor clearColor];
        
        NSIndexPath* selectedIndexPath = [tableView indexPathForSelectedRow];
        BOOL rowSelected = [indexPath isEqual:selectedIndexPath];
        
        [cellView fillWithDataForRow:[indexPath row] dataFrame:contentRect];
        //        if (_delegate) {
        //            [_delegate gridTable:self setContentForRow:[indexPath row] column:column content:columnView];
        //        }
        if (_delegate && [_delegate respondsToSelector:@selector(gridTable:formatCellForRow:column:content:selected:initial:)]) {
            [self formatCellForRow:indexPath.row column:column content:columnView selected:rowSelected initial:TRUE];
        }
        
        if (createCell){
            [cellView release];
        }
    }
    
    return row;
}

#pragma mark -
#pragma mark UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    return [textField resignFirstResponder];
}


////////////////////////////////////////////////////////////
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
#ifdef ShowLog
    NSLog(@" %s", __FUNCTION__);
#endif
    
    CGPoint point = textField.center;
    point = [textField convertPoint:point toView:_tableView];
    NSIndexPath* indexPath = [_tableView indexPathForRowAtPoint:point];
    
    if ((_delegate != nil)
        && [_delegate respondsToSelector:@selector(gridTable:cellContentBeginChangeForRow:column:content:value:)]) {
        
        GridTableColumnDescription *column = ((GridTableCell*)[textField superview]).column;
        
        id value = textField.text;
        
        if ([textField isKindOfClass:[UITextFieldNumeric class]])
            value = [(UITextFieldNumeric *)textField decimalNumberValue];
        return [_delegate gridTable:self cellContentBeginChangeForRow:[indexPath row] column:column content:textField value:value];
    }
    
    return YES;
}


////////////////////////////////////////////////////////////
- (void)textFieldDidBeginEditing:(UITextField *)textField {
    
    CGPoint point = textField.center;
    point = [textField convertPoint:point toView:_tableView];
    NSIndexPath* indexPath = [_tableView indexPathForRowAtPoint:point];
    //[_tableView reloadData];
    [self selectRow:indexPath.row animated:FALSE];
    [textField selectAllNoMenu];
    _activeField = textField;
    if (_delegate && [_delegate respondsToSelector:@selector(gridTable:cellContentDidBeginEditingRow:column:content:)]){
        GridTableColumnDescription *column = ((GridTableCell*)[textField superview]).column;
        [_delegate gridTable:self cellContentDidBeginEditingRow:[indexPath row] column:column content:textField];
    }
}

////////////////////////////////////////////////////////////
- (BOOL)textFieldShouldEndEditing:(UITextField *)textField {
    
    CGPoint point = textField.center;
    point = [textField convertPoint:point toView:_tableView];
    NSIndexPath* indexPath = [_tableView indexPathForRowAtPoint:point];
    if ((_delegate != nil) && [_delegate respondsToSelector:@selector(gridTable:cellContentShouldEndChangeForRow:column:content:value:)]) {
        
        GridTableColumnDescription *column = ((GridTableCell*)[textField superview]).column;
        id value = textField.text;
        if ([textField isKindOfClass:[UITextFieldNumeric class]]) {
            [((UITextFieldNumeric*)textField)flushEditValue];
            value = [(UITextFieldNumeric *)textField decimalNumberValue];
        }
        return [_delegate gridTable:self cellContentShouldEndChangeForRow:[indexPath row] column:column content:textField value:value];
    }
    
    return YES;
}


////////////////////////////////////////////////////////////
- (void)textFieldDidEndEditing:(UITextField *)textField {
    
    _activeField = nil;
    
    CGPoint point = textField.center;
    point = [textField convertPoint:point toView:_tableView];
    NSIndexPath* indexPath = [_tableView indexPathForRowAtPoint:point];
    if ((_delegate != nil) && [_delegate respondsToSelector:@selector(gridTable:cellContentDidEndChangeForRow:column:content:value:)]) {
        GridTableColumnDescription *column = ((GridTableCell*)[textField superview]).column;
        id value = textField.text;
        if ([textField isKindOfClass:[UITextFieldNumeric class]]) {
            value = [(UITextFieldNumeric *)textField decimalNumberValue];
        }
        
        [_delegate gridTable:self cellContentDidEndChangeForRow:[indexPath row] column:column content:textField value:value];
    }
}


////////////////////////////////////////////////////////////
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
#ifdef ShowLog
    NSLog(@" %s", __FUNCTION__);
#endif
    
    CGPoint point = textField.center;
    point = [textField convertPoint:point toView:_tableView];
    NSIndexPath* indexPath = [_tableView indexPathForRowAtPoint:point];
    
    if ((_delegate != nil)
        && [_delegate respondsToSelector:@selector(gridTable:cellContentChangeForRow:column:content:)]) {
        
        NSMutableString *text = [[[NSMutableString alloc] initWithString:textField.text] autorelease];
        [text replaceCharactersInRange:range withString:string];
        
        GridTableColumnDescription *column = ((GridTableCell*)[textField superview]).column;
        
        return [_delegate gridTable:self cellContentChangeForRow:[indexPath row] column:column content:text];
    }
    
    return YES;
}



////////////////////////////////////////////////////////////
- (BOOL)textFieldShouldClear:(UITextField *)textField {
    
    CGPoint point = textField.center;
    point = [textField convertPoint:point toView:_tableView];
    NSIndexPath* indexPath = [_tableView indexPathForRowAtPoint:point];
    
    if ((_delegate != nil)
        && [_delegate respondsToSelector:@selector(gridTable:cellContentShouldClearForRow:column:)]) {
        
        GridTableColumnDescription *column = ((GridTableCell*)[textField superview]).column;
        
        return [_delegate gridTable:self cellContentShouldClearForRow:[indexPath row] column:column];
    }
    
    return YES;
}

-(NSInteger)selectedRowIndex{
    NSIndexPath* path = [_tableView indexPathForSelectedRow];
    if (!path) return NSNotFound;
    return path.row;
}

- (void)setContentForRowDefault:(NSInteger)row columnView:(UIView*)columnView column:(GridTableColumnDescription *)column value:(NSObject*)rowValue{
    if (!columnView || !column.name.length) return;
    id columnValue = [rowValue valueForKeyPath:column.name];
    if (column.type == BoolColumn){
        if ([columnView isKindOfClass:[UIButton class]]){
            UIButton* button = (UIButton*)columnView;
            BOOL boolValue = FALSE;
            if ([columnValue isKindOfClass:[NSValue class]]){
                [(NSValue*)columnValue getValue:&boolValue];
            }
            UIImage * image = boolValue ? _yesImage : _noImage;
            [button setImage:image forState:UIControlStateNormal];
            [button setImage:image forState:UIControlStateHighlighted];
        }
    }
    else{
        NSString* columnValueFormatted = nil;
        switch (column.format) {
            case GridColumnFormatDate:
                if ([columnValue isKindOfClass:[NSDate class]]) {
                    if (column.dateFormater)
                        columnValueFormatted = [column.dateFormater stringFromDate:columnValue];
                    else if (column.formatString)
                        columnValueFormatted = [DataUtils readableDateStringFromDate:columnValue format:column.formatString];
                    else if (column.dateFormatterStyle!=NSDateFormatterNoStyle)
                        columnValueFormatted = [DataUtils readableDateStringFromDate:columnValue formatterStyle:column.dateFormatterStyle];
                    else
                        columnValueFormatted = [DataUtils readableDateStringFromDate:columnValue];
                }
                else
                    columnValueFormatted = [columnValue description];
                break;
            case GridColumnFormatDateTime:
                if ([columnValue isKindOfClass:[NSDate class]]) {
                    
                    if (column.dateFormater)
                        columnValueFormatted = [column.dateFormater stringFromDate:columnValue];
                    else if (column.formatString)
                        columnValueFormatted = [DataUtils readableDateTimeStringFromDate:columnValue format:column.formatString];
                    else if (column.dateFormatterStyle!=NSDateFormatterNoStyle || column.timeFormatterStyle!=NSDateFormatterNoStyle)
                        columnValueFormatted = [DataUtils readableDateTimeStringFromDate:columnValue dateFormatterStyle:column.dateFormatterStyle timeFormatterStyle:column.timeFormatterStyle];
                    else
                        columnValueFormatted = [DataUtils readableDateTimeStringFromDate:columnValue];
                }
                else
                    columnValueFormatted = [columnValue description];
                break;
            case GridColumnFormatMoney:
                columnValueFormatted = [NSCurrencyFormatter formatCurrencyFromObject:columnValue];
                break;
            case GridColumnFormatPercent:
                columnValueFormatted = [NSCurrencyFormatter formatPercentFromObject:columnValue];
                break;
            case GridColumnFormatQty:
                columnValueFormatted = [NSCurrencyFormatter formatQtyFromObject:columnValue];
                break;
            case GridColumnFormatDecimal:
                if (column.clearZero && [[columnValue description] floatValue] == 0)
                    columnValueFormatted = @"";
                else if (column.decimalTextHandler)
                    columnValueFormatted = [column.decimalTextHandler stringValue:[NSDecimalNumber decimalNumberWithString:[columnValue description]]];
                else
                    columnValueFormatted = [NSCurrencyFormatter formatDecimalFromObject:columnValue];
                break;
            default:
                columnValueFormatted = [columnValue description];
                break;
        }
        
        if ([columnView respondsToSelector:@selector(setText:)])
            [columnView performSelector:@selector(setText:) withObject:columnValueFormatted];
    }
}

-(CGFloat)rowHeight{
    return _tableView.rowHeight - spaceBetweenRows;
}

-(void)setRowHeight:(CGFloat)rowHeight{
    _tableView.rowHeight = rowHeight + spaceBetweenRows;
}

-(void)setSpaceBetweenRows:(NSUInteger)value{
    NSUInteger oldSpaceBetweenRows = spaceBetweenRows;
    spaceBetweenRows = value;
    _tableView.rowHeight = _tableView.rowHeight - oldSpaceBetweenRows + spaceBetweenRows;
}

-(NSUInteger)spaceBetweenRows{
    return spaceBetweenRows;
}

- (void)resetScroll {
    if (![_tableView numberOfRowsInSection:0]) return;
    NSIndexPath* indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
    [_tableView scrollToRowAtIndexPath:indexPath atScrollPosition:self.scrollPosition animated:TRUE];
}

#pragma mark -
#pragma mark GridTableRowDelegate

- (void)gridTableRow:(GridTableRow *)gridTableRow didChangeSelectedState:(BOOL)selected{
    NSIndexPath* path = [_tableView indexPathForCell:gridTableRow];
    if (!path) return;
    if (_delegate && [_delegate respondsToSelector:@selector(gridTable:formatCellForRow:column:content:selected:initial:)]) {
        for (NSInteger i = 0; i < _columns.count; i++) {
            GridTableColumnDescription *column = _columns[i];
            [self formatCellForRow:path.row column:column content:[gridTableRow getCellSubViewByColumn:column] selected:selected initial:FALSE];
        }
    }
}

-(void)beginUpdate {
    
    updateCounter++;
}


-(void)endUpdate {
    
    updateCounter--;
    
    if (updateCounter == 0)
        [self reloadData];
    
    if (updateCounter < 0)
        updateCounter = 0;
}

-(void)setHorizontalScrollEnabled:(BOOL)value{
    if (_horizontalScrollEnabled!=value){
        _horizontalScrollEnabled = value;
        if (!_defaultHorizontalScrollEnabled){
            self.defaultHorizontalScrollEnabled = value;
        }
        if (!_horizontalScrollEnabled){
            _headerView.frame = CGRectMake(0, 0, self.frame.size.width, DEFAULT_HEADER_HEIGHT);
        }
        [self rebuildColumnHeaders];
    }
}

-(void)startBlinking{
    if (self.isBlinking) return;
    CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"borderColor"];
    animation.fromValue = (id)[UIColor clearColor].CGColor;
    animation.toValue   = (id)[UIColor redColor].CGColor;
    animation.duration = 1.0f;
    animation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    animation.repeatCount = HUGE_VALF;
    animation.autoreverses = YES;
    animation.removedOnCompletion =NO;
    
    self.layer.borderWidth = 4.0;
    self.layer.borderColor = [UIColor clearColor].CGColor;
    [self.layer addAnimation:animation forKey:@"border color"];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardDidHideNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidHide:) name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidHide:) name:UIKeyboardDidHideNotification object:nil];
}

- (void)keyboardDidHide:(NSNotification *)n{
    if (self.isBlinking) return;
    
    [self startBlinking];
}

-(BOOL)isBlinking{
    return ([self.layer animationForKey:@"border color"]!=nil);
}

@end
