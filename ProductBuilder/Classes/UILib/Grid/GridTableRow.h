//
//  GridTableRow.h
//  CloudStockCount
//
//  Created by Lulakov Viacheslav on 6/6/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GridTableCell.h"

@class GridTableRow;
@class GridTableColumnDescription;
@class GridTable;

@protocol GridTableRowDelegate
- (void)gridTableRow:(GridTableRow *)gridTableRow didChangeSelectedState:(BOOL)selected;
@end

@interface GridTableRow : UITableViewCell {
    id<GridTableRowDelegate> delegate;
    GridTable* _gridTable;
    BOOL changingSelection;
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier gridTable:(GridTable *)gridTable;

- (GridTableCell *)getCellViewByColumn:(GridTableColumnDescription *) column;
- (GridTableCell *)getCellViewByColumnName:(NSString *) columnName;
- (UIView *)getCellSubViewByColumn:(GridTableColumnDescription *) column;
- (NSArray*)getCellSubViewsByColumn:(GridTableColumnDescription*) column;
- (NSIndexPath*)indexPath;
-(void)didDequeue;
@property(nonatomic,readonly)NSArray *columns;
@property(nonatomic,assign)id<GridTableRowDelegate> delegate;
@property(nonatomic,readonly)GridTable* gridTable;
@property(nonatomic,readonly)BOOL changingSelection;
@property(nonatomic,readonly)CGRect lastDrawnFrame;
@end
