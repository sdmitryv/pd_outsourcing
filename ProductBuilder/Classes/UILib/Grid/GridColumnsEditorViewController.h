//
//  GridColumnsEditorViewController.h
//  ProductBuilder
//
//  Created by Roman on 12/20/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SimpleTableListViewController.h"


@interface GridColumnsEditorViewController :  SimpleTableListViewController{
    GridTable   *_gridTable;
    UIButton    *_applyButton;
    UISlider    *_useHorizontalScrollingSwitch;
    UILabel* _useHorizontalScrollingTitle;
    NSMutableArray     *_columns;
    BOOL _closing;
}
-(id)initWithGridTable:(GridTable*)gridTable;
@property (nonatomic, assign) GridTable *gridTable;

@end
