//
//  GridTableCellBoolean.m
//  ProductBuilder
//
//  Created by valera on 9/11/12.
//  Copyright (c) 2012 Cloudworks. All rights reserved.
//

#import "GridTableCellBoolean.h"
#import "GridTable.h"

@implementation GridTableCellBoolean

-(id)initWithFrame:(CGRect)frame dataFrame:(CGRect) dataFrame row:(GridTableRow*)row column:(GridTableColumnDescription*)columnDescription
{
    self = [super initWithFrame:frame dataFrame:dataFrame row:row column:columnDescription];
    if (self) {
        button = [[UIButton buttonWithType:UIButtonTypeCustom]retain];
        if (column.editable)
            [button addTarget:self action:@selector(boolValueChanged:) forControlEvents:UIControlEventTouchUpInside];
        
        button.userInteractionEnabled = column.editable;
        button.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
        button.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
        button.contentMode = UIViewContentModeScaleAspectFit;
        dataFrame.size.width += 10;
        CGFloat a = dataFrame.size.height - 8;
        [button setFrame:CGRectMake(dataFrame.origin.x + dataFrame.size.width / 2 - a / 2, 4, a, a)];
        button.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleLeftMargin;

        [self addSubview:button];
    }
    return self;
}

-(void)dealloc{
    [button release];
    [super dealloc];
}

-(void)applyDefaultProperties:(CGRect)dataFrame{

}

- (void)boolValueChanged:(id)sender {
	UIButton *btn = (UIButton *)sender;
    CGPoint point = btn.center;
	point = [button convertPoint:point toView:self.gridTable.tableView];
	NSIndexPath* indexPath = [self.gridTable.tableView indexPathForRowAtPoint:point];
	if (self.gridTable.delegate != nil) {
		[self.gridTable.delegate gridTable:self.gridTable changeBoolForRow:[indexPath row] column:column cell:self];
		BOOL boolValue = [self.gridTable.delegate gridTable:self.gridTable  getBoolForRow:[indexPath row] column:column];
		UIImage * image = boolValue ? self.gridTable.yesImage : self.gridTable.noImage;
		[button setImage:image forState:UIControlStateNormal];
		[button setImage:image forState:UIControlStateHighlighted];
	}
}

-(void)fillWithDataForRow:(NSInteger)row dataFrame:(CGRect)dataFrame{
    BOOL boolValue = NO;
    if (self.gridTable.delegate) {
        boolValue = [self.gridTable.delegate gridTable:self.gridTable getBoolForRow:row column:column];
    }
    UIImage * image = boolValue ? self.gridTable.yesImage : self.gridTable.noImage;
    [button setImage:image forState:UIControlStateNormal];
    [button setImage:image forState:UIControlStateHighlighted];

}

@end
