//
//  GridTableCellTextField.m
//  ProductBuilder
//
//  Created by valera on 9/11/12.
//  Copyright (c) 2012 Cloudworks. All rights reserved.
//

#import "GridTableCellTextField.h"
#import "UITextFieldNumeric.h"
#import "GridTable.h"

@implementation GridTableCellTextField

-(id)initWithFrame:(CGRect)frame dataFrame:(CGRect) dataFrame row:(GridTableRow*)row column:(GridTableColumnDescription*)columnDescription
{
    self = [super initWithFrame:frame dataFrame:dataFrame row:row column:columnDescription];
    if (self) {
        UITextFieldNumeric *textFieldNumeric = nil;
        switch (column.format) {
            case GridColumnFormatMoney:
                textFieldNumeric = [[UITextFieldNumeric alloc] initWithFrame:dataFrame];
                ((UITextFieldNumeric *)textField).textHandler.clearZero = column.clearZero;
                textFieldNumeric.style = NSTextFormatterCurrencyStyle;
                break;
            case GridColumnFormatQty:
                textFieldNumeric = [[UITextFieldNumeric alloc] initWithFrame:dataFrame];
                ((UITextFieldNumeric *)textField).textHandler.clearZero = column.clearZero;
                textFieldNumeric.style = NSTextFormatterQtyStyle;
                break;
            case GridColumnFormatPercent:
                textFieldNumeric = [[UITextFieldNumeric alloc] initWithFrame:dataFrame];
                ((UITextFieldNumeric *)textField).textHandler.clearZero = column.clearZero;
                textFieldNumeric.style = NSTextFormatterPercentStyle;
                break;
            case GridColumnFormatDecimal:
                textFieldNumeric = [[UITextFieldNumeric alloc] initWithFrame:dataFrame];
                ((UITextFieldNumeric *)textField).textHandler.clearZero = column.clearZero;
                textFieldNumeric.style = NSTextFormatterDecimalStyle;
                textFieldNumeric.keyboardType = UIKeyboardTypeNumberPad;
                
                if (column.decimalTextHandler)
                    textFieldNumeric.customFracationDigits = column.decimalTextHandler.customFracationDigits;
                
                break;
            default:
                textField = [[UITextField alloc] initWithFrame:dataFrame];
                break;
        }
        
        if (textField == nil) 
            textField = [textFieldNumeric retain];
        textField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
        textField.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
        textField.delegate = self.gridTable;
        [textField addTarget:self action:@selector(textFieldTextDidChange:) forControlEvents:UIControlEventEditingChanged];
        [self addSubview:textField];
        [textFieldNumeric release];
    }

    return self;
}


-(void)dealloc{
    textField.delegate = nil;
    [textField release];
    [super dealloc];
}

-(void)textFieldTextDidChange:(UITextField*)sender{
	if ((self.gridTable.delegate) && [self.gridTable.delegate respondsToSelector:@selector(gridTable:cellContentDidChangeForRow:column:content:value:)]) {
        CGPoint point = textField.center;
        point = [textField convertPoint:point toView:self.gridTable.tableView];
        NSIndexPath* indexPath = [self.gridTable.tableView indexPathForRowAtPoint:point];
        id value = textField.text;
        if ([textField isKindOfClass:[UITextFieldNumeric class]]) {
            [(UITextFieldNumeric *)textField flushEditValue];
            value = [(UITextFieldNumeric *)textField decimalNumberValue];
        }
        [self.gridTable.delegate gridTable:self.gridTable cellContentDidChangeForRow:[indexPath row] column:column content:textField value:value];
    }
}

-(void)applyDefaultProperties:(CGRect)dataFrame{
    textField.frame = dataFrame;
    textField.text = @"";
    textField.opaque = YES;
    textField.hidden = NO;
    textField.borderStyle = UITextBorderStyleNone;
    textField.clearButtonMode = UITextFieldViewModeWhileEditing;
    textField.font = self.gridTable.cellFont;
    textField.textAlignment = column.horizontalAlignment;
    textField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    textField.contentHorizontalAlignment = [GridTable convertTextAlignmentToContentHorizontalAlignment:column.horizontalAlignment];
    textField.textColor = self.gridTable.rowTextColor;
}

@end
