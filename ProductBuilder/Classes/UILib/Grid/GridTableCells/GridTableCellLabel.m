//
//  GridTableCellLabel.m
//  ProductBuilder
//
//  Created by valera on 9/11/12.
//  Copyright (c) 2012 Cloudworks. All rights reserved.
//

#import "GridTableCellLabel.h"
#import "GridTable.h"
#import "UICopyLabel.h"

@implementation GridTableCellLabel

-(id)initWithFrame:(CGRect)frame dataFrame:(CGRect) dataFrame row:(GridTableRow*)row column:(GridTableColumnDescription*)columnDescription
{
    self = [super initWithFrame:frame dataFrame:dataFrame row:row column:columnDescription];
    if (self) {
        label = [[UICopyLabel alloc] initWithFrame:dataFrame];
        label.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
        [self addSubview:label];
    }
    return self;
}

-(void)dealloc{
    [label release];
    [super dealloc];
}

-(void)applyDefaultProperties:(CGRect)dataFrame{
    label.frame = dataFrame;
    label.text = @"";
    label.opaque = YES;
    label.hidden = NO;
    label.font = self.gridTable.cellFont;
    label.textAlignment = column.horizontalAlignment;
    label.textColor = self.gridTable.rowTextColor;
    label.highlightedTextColor  = self.gridTable.rowTextColorSelected;
    label.numberOfLines = 0;
}

@end
