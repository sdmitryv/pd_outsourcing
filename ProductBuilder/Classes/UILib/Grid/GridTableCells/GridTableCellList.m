//
//  GridTableCellList.m
//  ProductBuilder
//
//  Created by Lulakov Viacheslav on 10/17/12.
//  Copyright (c) 2012 Cloudworks. All rights reserved.
//

#import "GridTableCellList.h"
#import "GridTable.h"

#define DEFAULT_ROW_HEIGTH 30

@implementation GridTableCellList

@synthesize dataArray;

-(id)initWithFrame:(CGRect)frame dataFrame:(CGRect)dataFrame row:(GridTableRow*)row column:(GridTableColumnDescription*)columnDescription
{
    self = [super initWithFrame:frame dataFrame:dataFrame row:row column:columnDescription];
    if (self) {
        listTableView = [[UITableView alloc] initWithFrame:dataFrame style:UITableViewStylePlain];
        listTableView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
//        listTableView.layer.borderColor = [UIColor redColor].CGColor;
//        listTableView.layer.borderWidth = 3;
        listTableView.hidden = NO;
        listTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        listTableView.allowsSelection = NO;
        listTableView.rowHeight = DEFAULT_ROW_HEIGTH;
        listTableView.userInteractionEnabled = NO;
        listTableView.dataSource = self;
        listTableView.delegate = self;
        listTableView.backgroundColor = [UIColor clearColor];
        [self addSubview:listTableView];
    }
    return self;
}

-(void)dealloc
{
    [listTableView release];
    [dataArray release];
    [super dealloc];
}

#pragma mark - UITableViewDataSource delegate methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return dataArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ListCell * cell = [tableView dequeueReusableCellWithIdentifier:@"ListCell"];
    if (cell == nil) {
        cell = [[[ListCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"ListCell"] autorelease];
        cell.titleLabel.font = self.gridTable.cellFont;
        cell.titleLabel.textAlignment = column.horizontalAlignment;
        cell.titleLabel.textColor = self.gridTable.rowTextColor;
        cell.titleLabel.highlightedTextColor  = self.gridTable.rowTextColorSelected;
        cell.titleLabel.lineBreakMode = NSLineBreakByTruncatingMiddle;
    }
    cell.titleLabel.text = [dataArray[indexPath.row] description];
    cell.titleLabel.highlighted = self.gridTableRow.selected;
    cell.backgroundColor = [UIColor clearColor];
    return cell;
}

#pragma mark - UITableViewDelegate delegate methods

#pragma mark - Properties

-(void)setDataArray:(NSArray *)value
{
    if (dataArray != value) {
        [dataArray release];
        dataArray = [value retain];
    }
    [listTableView reloadData];
}

@end


@implementation ListCell

@synthesize titleLabel = _titleLabel;

- (void)dealloc {
    [_titleLabel release];
    [super dealloc];
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        _titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.bounds.size.width, self.bounds.size.height)];
        _titleLabel.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        _titleLabel.backgroundColor = [UIColor clearColor];
        [self.contentView addSubview:_titleLabel];
    }
    return self;
}

@end
