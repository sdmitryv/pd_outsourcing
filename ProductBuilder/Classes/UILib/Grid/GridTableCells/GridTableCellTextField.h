//
//  GridTableCellTextField.h
//  ProductBuilder
//
//  Created by valera on 9/11/12.
//  Copyright (c) 2012 Cloudworks. All rights reserved.
//

#import "GridTableCell.h"

@interface GridTableCellTextField : GridTableCell{
    UITextField* textField;
}

@end
