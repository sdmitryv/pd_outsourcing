//
//  ColumnDescription.h
//  ProductBuilder
//
//  Created by Valery Fomenko on 4/25/12.
//  Copyright (c) 2012 Cloudworks. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UITextFieldHandler.h"
#import "BPUUID.h"

typedef NS_ENUM(NSUInteger, ColumnType) {
    TextColumn = 0, BoolColumn = 1, ColoredTextColumn = 2, ButtonColumn = 3, ImageWithTextColumn = 4, CustomColumn = 5, ListColumn = 6, ImageColumn=7
};

typedef NS_ENUM(NSUInteger, GridColumnFormat) {GridColumnFormatNone = 0, GridColumnFormatDate = 1, GridColumnFormatMoney = 2, GridColumnFormatQty = 3,
    GridColumnFormatPercent = 4, GridColumnFormatDateTime = 5, GridColumnFormatDecimal = 6};

@interface GridTableColumnDescription : NSObject <NSCoding, NSCopying> {
    ColumnType	_type;
    NSString	* _name;
    NSString	* _title;
    NSString	* _titleOriginal;
    CGFloat		  _width;
    BOOL		  _sortable;
    BOOL		  _editable;
    GridColumnFormat _format;
    NSString    * _formatString;
    NSTextAlignment horizontalAlignment;
    BOOL            hideSeparator;
    BOOL            visible;
    CGFloat         imageHeight;
    CGFloat         imageWidth;
    BOOL            heightToFit;
    Class           customCellClass;
    BOOL            multiline;
    BOOL            _isImageRight;
    BOOL            clearZero;
}
@property (nonatomic, retain) BPUUID* id;
@property (nonatomic, assign) BOOL hideSeparator;
@property (nonatomic, assign) ColumnType  type;
@property (nonatomic, retain) NSString	* name;
@property (nonatomic, retain) NSString	* title;
@property (nonatomic, retain) NSString	* titleOriginal;
@property (nonatomic, assign) CGFloat	  width;
@property (nonatomic, assign) BOOL		  sortable;
@property (nonatomic, assign) BOOL		  editable;
@property (nonatomic, assign) NSTextAlignment horizontalAlignment;
@property (nonatomic, assign)GridColumnFormat format;
@property (nonatomic, retain)NSString* formatString;
@property (nonatomic, assign)CGFloat imageWidth;
@property (nonatomic, assign)CGFloat imageHeight;
@property (nonatomic, assign)BOOL heightToFit;
@property (nonatomic, assign) BOOL visible;
@property (nonatomic, readonly) Class customCellClass;
@property (nonatomic, assign) BOOL multiline;
@property (nonatomic, assign)BOOL isImageRight;
@property (nonatomic, assign)BOOL clearZero;

@property (nonatomic, retain) UITextFieldHandler *decimalTextHandler;
@property (nonatomic, retain) NSDateFormatter *dateFormater;
@property (nonatomic, assign)NSDateFormatterStyle dateFormatterStyle;
@property (nonatomic, assign)NSDateFormatterStyle timeFormatterStyle;

-(void)assignProperties:(NSDictionary*)dict;
-(NSDictionary*)getProperties:(GridTableColumnDescription*)defaultColumnDescription;

- (NSString *)description;

- (id)initTextColumnWithName:(NSString *)name title:(NSString *)title width:(CGFloat)width sortable:(BOOL)sortable
                    editable: (BOOL)editable alignment :(NSTextAlignment)alignment;
- (id)initTextColumnWithName:(NSString *)name title:(NSString *)title width:(CGFloat)width sortable:(BOOL)sortable
                    editable: (BOOL)editable alignment :(NSTextAlignment)alignment format:(GridColumnFormat)format;
- (id)initTextColumnWithName:(NSString *)name title:(NSString *)title width:(CGFloat)width sortable:(BOOL)sortable
                    editable: (BOOL)editable alignment :(NSTextAlignment)alignment format:(GridColumnFormat)format formatString:(NSString*)formatString;
- (id)initTextColumnWithName:(NSString *)name title:(NSString *)title width:(CGFloat)width sortable:(BOOL)sortable
                    editable: (BOOL)editable alignment :(NSTextAlignment)alignment hideSeparator:(BOOL)hidesep;
- (id)initTextColumnWithName:(NSString *)name title:(NSString *)title width:(CGFloat)width sortable:(BOOL)sortable
                    editable: (BOOL)editable alignment :(NSTextAlignment)alignment format:(GridColumnFormat)format hideSeparator:(BOOL)hidesep;
- (id)initTextColumnWithName:(NSString *)name title:(NSString *)title width:(CGFloat)width sortable:(BOOL)sortable
                    editable: (BOOL)editable alignment :(NSTextAlignment)alignment format:(GridColumnFormat)format formatString:(NSString*)formatString hideSeparator:(BOOL)hidesep;
- (id)initTextColumnWithName:(NSString *)name title:(NSString *)title width:(CGFloat)width sortable:(BOOL)sortable
                    editable: (BOOL)editable alignment :(NSTextAlignment)alignment format:(GridColumnFormat)format formatString:(NSString*)formatString hideSeparator:(BOOL)hidesep
                   multilene:(BOOL)multiline;

- (id)initDateColumnWithName:(NSString *)name title:(NSString *)title width:(CGFloat)width sortable:(BOOL)sortable
                  alignment :(NSTextAlignment)alignment dateFormatStyle:(NSDateFormatterStyle)dateFormatStyle;
- (id)initDateTimeColumnWithName:(NSString *)name title:(NSString *)title width:(CGFloat)width sortable:(BOOL)sortable
                      alignment :(NSTextAlignment)alignment dateFormatStyle:(NSDateFormatterStyle)dateFormatStyle timeFormatStyle:(NSDateFormatterStyle)timeFormatStyle;

- (id)initBoolColumnWithName:(NSString *)name title:(NSString *)title width:(CGFloat)width sortable:(BOOL)sortable editable: (BOOL)editable hideSeparator:(BOOL)hidesep alignment :(NSTextAlignment)alignment;
- (id)initBoolColumnWithName:(NSString *)name title:(NSString *)title width:(CGFloat)width sortable:(BOOL)sortable editable: (BOOL)editable;

- (id)initButtonColumnWithName:(NSString *)name title:(NSString *)title width:(CGFloat)width;
- (id)initButtonColumnWithName:(NSString *)name title:(NSString *)title width:(CGFloat)width hideSeparator:(BOOL)hidesep alignment:(NSTextAlignment)alignment;

-(id)initImageTextColumnWithName:(NSString*)name title:(NSString*)title width:(CGFloat)width imageWidth:(CGFloat)imgWidth imageHeight:(CGFloat)imgHeight;
-(id)initImageTextColumnWithName:(NSString *)name title:(NSString *)title width:(CGFloat)width imageWidth:(CGFloat)imgWidth imageHeight:(CGFloat) imgHeight isImageRight:(BOOL)isImageRight;
- (id)initCustomColumnWithName:(NSString *)name title:(NSString *)title width:(CGFloat)width sortable:(BOOL)sortable alignment:(NSTextAlignment)alignment cellClass:(Class)cellClass;
- (id)initCustomColumnWithName:(NSString *)name title:(NSString *)title width:(CGFloat)width sortable:(BOOL)sortable cellClass:(Class)cellClass;
-(id)initImageColumnWithName:(NSString*)name title:(NSString*)title width:(CGFloat)width imageWidth:(CGFloat)imgWidth imageHeight:(CGFloat)imgHeight;
-(id)initImageColumnWithName:(NSString*)name title:(NSString*)title width:(CGFloat)width imageWidth:(CGFloat)imgWidth imageHeight:(CGFloat)imgHeight sortable:(BOOL)sortable;

- (id)initListColumnWithName:(NSString *)name title:(NSString *)title width:(CGFloat)width;

+ (id)textColumnWithName:(NSString *)name title:(NSString *)title width:(CGFloat)width sortable:(BOOL)sortable
                editable: (BOOL)editable alignment :(NSTextAlignment)alignment;
+ (id)textColumnWithName:(NSString *)name title:(NSString *)title width:(CGFloat)width sortable:(BOOL)sortable
                editable: (BOOL)editable alignment :(NSTextAlignment)alignment format:(GridColumnFormat)format;
+ (id)textColumnWithName:(NSString *)name title:(NSString *)title width:(CGFloat)width sortable:(BOOL)sortable
                editable: (BOOL)editable alignment :(NSTextAlignment)alignment format:(GridColumnFormat)format formatString:(NSString*)formatString;
+ (id)textColumnWithName:(NSString *)name title:(NSString *)title width:(CGFloat)width sortable:(BOOL)sortable
                editable: (BOOL)editable alignment :(NSTextAlignment)alignment format:(GridColumnFormat)format clearZero:(BOOL)clearZero;

+ (id)textColumnWithName:(NSString *)name title:(NSString *)title width:(CGFloat)width sortable:(BOOL)sortable
                editable: (BOOL)editable alignment :(NSTextAlignment)alignment hideSeparator:(BOOL)hidesep;
+ (id)textColumnWithName:(NSString *)name title:(NSString *)title width:(CGFloat)width sortable:(BOOL)sortable
                editable: (BOOL)editable alignment :(NSTextAlignment)alignment format:(GridColumnFormat)format hideSeparator:(BOOL)hidesep;
+ (id)textColumnWithName:(NSString *)name title:(NSString *)title width:(CGFloat)width sortable:(BOOL)sortable
                editable: (BOOL)editable alignment :(NSTextAlignment)alignment format:(GridColumnFormat)format formatString:(NSString*)formatString hideSeparator:(BOOL)hidesep;
+ (id)textColumnWithName:(NSString *)name title:(NSString *)title width:(CGFloat)width sortable:(BOOL)sortable
                editable: (BOOL)editable alignment :(NSTextAlignment)alignment format:(GridColumnFormat)format formatString:(NSString*)formatString hideSeparator:(BOOL)hidesep multiline:(BOOL)multiline;

+ (id)dateColumnWithName:(NSString *)name title:(NSString *)title width:(CGFloat)width sortable:(BOOL)sortable
              alignment :(NSTextAlignment)alignment dateFormatStyle:(NSDateFormatterStyle)dateFormatStyle;
+ (id)dateTimeColumnWithName:(NSString *)name title:(NSString *)title width:(CGFloat)width sortable:(BOOL)sortable
                  alignment :(NSTextAlignment)alignment dateFormatStyle:(NSDateFormatterStyle)dateFormatStyle timeFormatStyle:(NSDateFormatterStyle)timeFormatStyle;

+ (id)boolColumnWithName:(NSString *)name title:(NSString *)title width:(CGFloat)width sortable:(BOOL)sortable editable: (BOOL)editable;
+ (id)boolColumnWithName:(NSString *)name title:(NSString *)title width:(CGFloat)width sortable:(BOOL)sortable editable: (BOOL)editable alignment :(NSTextAlignment)alignment;
+ (id)boolColumnWithName:(NSString *)name title:(NSString *)title width:(CGFloat)width sortable:(BOOL)sortable editable: (BOOL)editable hideSeparator:(BOOL)hidesep;

+(id)imageTextColumnWithName:(NSString *)name title:(NSString *)title width:(CGFloat)width imageWidth:(CGFloat)imgWidth imageHeight:(CGFloat)imgHeight;
+(id)imageTextColumnWithName:(NSString *)name title:(NSString *)title width:(CGFloat)width imageWidth:(CGFloat)imgWidth imageHeight:(CGFloat)imgHeight isImageRight:(BOOL)isImageRight;

+ (id)buttonColumnWithName:(NSString *)name title:(NSString *)title width:(CGFloat)width;
+ (id)buttonColumnWithName:(NSString *)name title:(NSString *)title width:(CGFloat)width alignment:(NSTextAlignment)alignment;
+ (id)buttonColumnWithName:(NSString *)name title:(NSString *)title width:(CGFloat)width hideSeparator:(BOOL)hidesep;

+ (id)customColumnWithName:(NSString *)name title:(NSString *)title width:(CGFloat)width sortable:(BOOL)sortable cellClass:(Class)cellClass;
+ (id)customColumnWithName:(NSString *)name title:(NSString *)title width:(CGFloat)width sortable:(BOOL)sortable alignment:(NSTextAlignment)alignment cellClass:(Class)cellClass;
+ (id)listColumnWithName:(NSString *)name title:(NSString *)title width:(CGFloat)width;
+(id)imageColumnWithName:(NSString *)name title:(NSString *)title width:(CGFloat)width imageWidth:(CGFloat)imgWidth imageHeight:(CGFloat)imgHeight ;
+(id)imageColumnWithName:(NSString *)name title:(NSString *)title width:(CGFloat)width imageWidth:(CGFloat)imgWidth imageHeight:(CGFloat)imgHeight sortable:(BOOL)sortable;
@end
