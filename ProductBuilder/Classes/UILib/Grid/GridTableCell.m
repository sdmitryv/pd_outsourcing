//
//  MyCell.m
//  ProductBuilder
//
//  Created by Roman on 12/12/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import "GridTableCell.h"
#import "GridTable.h"
#import <QuartzCore/QuartzCore.h>

@interface GridTableCell()
@property (nonatomic, retain) GridTableColumnDescription* column;
@property (nonatomic, assign) GridTableRow* gridTableRow;
@end

@implementation GridTableCell

@synthesize column, gridTableRow;

-(void)initDefaults{
    column = nil;
    self.autoresizingMask = UIViewAutoresizingFlexibleHeight;
    self.autoresizesSubviews = YES;
    self.clipsToBounds = TRUE;
//    self.layer.borderColor = [UIColor greenColor].CGColor;
//    self.layer.borderWidth = 2;
}

-(id)initWithFrame:(CGRect)frame dataFrame:(CGRect) dataFrame row:(GridTableRow*)row column:(GridTableColumnDescription*)columnDescription{
    if (self=[super initWithFrame:frame]){
        [self initDefaults];
        self.gridTableRow = row;
        self.column = columnDescription;
        
    }
    return  self;
}

- (id)init {
	if ((self = [super init])) {
		[self initDefaults];
	}
	return self;
}

- (NSString *)description{
    return [column description];
}

-(UIView *) getSubView{
    if (!self.subviews.count) return nil;
    return (UIView *)[self subviews][0];
}

-(void)applyDefaultProperties:(CGRect)dataFrame{
    
}

//-(void)setFrame:(CGRect)frame{
//    [super setFrame:frame];
//}

- (void)dealloc {
	[column release];
    [super dealloc];
}

-(void)fillWithDataForRow:(NSInteger)row dataFrame:(CGRect)dataFrame{
    if (self.gridTable.delegate) {
        [self.gridTable.delegate gridTable:self.gridTable setContentForRow:row column:column content:[self getSubView]];
    }
}

-(GridTable*)gridTable {
    return gridTableRow.gridTable;
}

@end
