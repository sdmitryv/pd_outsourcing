//
//  GridTableVarHeight.m
//  ProductBuilder
//
//  Created by Valery Fomenko on 4/25/12.
//  Copyright (c) 2012 Cloudworks. All rights reserved.
//

#import "GridTableVarHeight.h"

@implementation GridTableVarHeight

#pragma mark -
#pragma mark Table view delegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if ([self.delegate respondsToSelector:@selector(gridTable:heightForRow:)]){
        return [self.delegate gridTable:self heightForRow:indexPath.row] + self.spaceBetweenRows;
    }
    return tableView.rowHeight + self.spaceBetweenRows;
}

@end
