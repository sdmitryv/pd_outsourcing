//
//  UITextFieldHandler.m
//  ProductBuilder
//
//  Created by Valera on 11/5/10.
//  Copyright 2010 DBBestTechnologies. All rights reserved.
//

#import "UITextFieldHandler.h"
#import "UITextFieldNumeric.h"
#import "DecimalHelper.h"
#import "UITextField+Input.h"
#import "VirtualNumberpadViewController.h"
//#import "NumberpadInputViewController.h"
#import "QuartzCore/QuartzCore.h"
#import "UIView+FirstResponder.h"
#import "KeyboardHelper.h"
#import <objc/runtime.h>

typedef struct {
    BOOL textFieldShouldBeginEditing;
    BOOL textFieldDidBeginEditing;
    BOOL textFieldShouldEndEditing;
    BOOL textFieldDidEndEditing;
    BOOL textFieldShouldChangeCharactersInRange;
    BOOL textFieldShouldClear;
    BOOL textFieldShouldReturn;
} UITextFieldDelegateFlags;

typedef struct{
    BOOL textFieldHandlerShouldChangeValue;
} UITextFieldHandlerDelegateFlags;

@interface UITextFieldHandler (){
    BOOL maxValueIsDefault;
    BOOL minValueIsDefault;
    UITextFieldDelegateFlags delegateFlags;
    UITextFieldHandlerDelegateFlags handlerDelegateFlags;
}
-(void)initDefaults;
@end

@implementation UITextFieldHandler

VirtualNumberpadViewController * virtualNumberpadViewController;

-(void)initDefaults{

    shouldProcessDidEndEditing = TRUE;
	forceSetStyle = TRUE;
	self.style = NSTextFormatterNumbersStyle;
	forceSetStyle = FALSE;
    internalTextAssign = FALSE;
    maxValue = [@999999999.99999 retain];
    minValue = [@-999999999.99999 retain];
    maxValueIsDefault = maxValueIsDefault = TRUE;
    //inputTextField.inputView = [self numberpadInputView];
    [inputTextField setNumpadKeyboardContent:NumpadKeyboardContentsMinus];
    //numpadKeyboardContent = NumpadKeyboardContentsMinus;
    [KeyboardHelper installNumpadKeyboardForTextField:inputTextField];
}

//-(UIView *)numberpadInputView {
//    if (numberpadInputViewController == nil) {
//        numberpadInputViewController = [[NumberpadInputViewController alloc] init];
//    }
//    return numberpadInputViewController.view;
//}

@synthesize formatter = _formatter, inputTextField, maxValue, minValue, enableNil, 
percentSymbol, shouldProcessDidEndEditing, internalTextAssign, delegate, customFracationDigits,maxTextLength, clearZero;

BOOL(^numberFormatterHandler)(NSNumberFormatter* formatter, NSTextFormatterStyle style);

+ (void)setNumberFormatterHandler:(BOOL(^)(NSNumberFormatter* formatter, NSTextFormatterStyle style))aNumberFormatterHandler{
    [numberFormatterHandler release];
    numberFormatterHandler = [aNumberFormatterHandler copy];
}

+ (BOOL(^)(NSNumberFormatter* formatter, NSTextFormatterStyle style))numberFormatterHandler{
    return  numberFormatterHandler;
}

-(void)setDelegate:(id<UITextFieldDelegate>)value{
    if (self.inputTextField.readOnly){
        if (![value isKindOfClass:[UITextFieldReadOnlyDelegate class]]){
            UITextFieldReadOnlyDelegate* readOnlyDelegate = objc_getAssociatedObject(self.inputTextField, readOnlyDelegateTagKey);
            readOnlyDelegate.originDelegate = value;
            return;
        }
    }
    delegate = value;
    delegateFlags.textFieldShouldBeginEditing = [delegate respondsToSelector:@selector(textFieldShouldBeginEditing:)];
    delegateFlags.textFieldDidBeginEditing = [delegate respondsToSelector:@selector(textFieldDidBeginEditing:)];
    delegateFlags.textFieldShouldEndEditing = [delegate respondsToSelector:@selector(textFieldShouldEndEditing:)];
    delegateFlags.textFieldDidEndEditing = [delegate respondsToSelector:@selector(textFieldDidEndEditing:)];
    delegateFlags.textFieldShouldChangeCharactersInRange = [delegate respondsToSelector:@selector(textField:shouldChangeCharactersInRange:replacementString:)];
    delegateFlags.textFieldShouldClear = [delegate respondsToSelector:@selector(textFieldShouldClear:)];
    delegateFlags.textFieldShouldReturn = [delegate respondsToSelector:@selector(textFieldShouldReturn:)];
}

-(void)setTextFieldHandlerDelegate:(id<UITextFieldHandlerDelegate>)value{
    _textFieldHandlerDelegate = value;
    handlerDelegateFlags.textFieldHandlerShouldChangeValue = [(id)value respondsToSelector:@selector(textFieldHandler:shouldChangeValue:)];
}

-(NSTextFormatterStyle)style{
	return style;
}

- (id)initWithTextField:(UITextField *)textField {
    if ((self = [super init])) {
        inputTextField = textField;
		[self initDefaults];
	}
    return self;
}


- (id)initWithTextField:(UITextField *)textField style:(NSTextFormatterStyle)value {
    if ((self = [self initWithTextField:textField])) {
		forceSetStyle = TRUE;
		[self setStyle:value];
		forceSetStyle = FALSE;
	}
    return self;
}

-(void)setStyle:(NSTextFormatterStyle)value{

	if (!forceSetStyle && style == value) return;

	[_formatter release];
	_formatter = [[NSNumberFormatter alloc] init];
	_formatter.generatesDecimalNumbers = YES;
	_formatter.formatterBehavior = NSNumberFormatterBehavior10_4;
	_formatter.locale = [NSLocale currentLocale];
	_formatter.roundingMode = NSNumberFormatterRoundHalfEven;

	style = value;

	[nonNumberSet release];
	NSMutableCharacterSet* numberSet = [[NSCharacterSet decimalDigitCharacterSet] mutableCopy];
	//[numberSet formUnionWithCharacterSet:[NSCharacterSet whitespaceCharacterSet]];
	
    //no negative by default
    //_formatter.minimum = @(0);
    _formatter.paddingPosition = NSNumberFormatterPadAfterPrefix;
    _formatter.paddingCharacter = @"0";
    
	switch (style) {
        case NSTextFormatterNumbersStyle:
            [_formatter setNumberStyle:NSNumberFormatterDecimalStyle];
            _formatter.usesGroupingSeparator = FALSE;
			_formatter.allowsFloats = FALSE;
            _formatter.maximumFractionDigits = 0;
            _formatter.minimumFractionDigits = 0;
            //no limits for numbers style
            if (maxValue && maxValueIsDefault){
                [maxValue release];
                maxValue = nil;
            }
            if (minValue && minValueIsDefault){
                [minValue release];
                minValue = nil;
            }
            break;
		case NSTextFormatterQtyStyle:
			[_formatter setNumberStyle:NSNumberFormatterDecimalStyle];
            _formatter.usesGroupingSeparator = TRUE;
			//_formatter.allowsFloats = FALSE;
            _formatter.maximumFractionDigits = 3;
            _formatter.minimumFractionDigits = 0;
			break;
		case NSTextFormatterCurrencyStyle:
            [_formatter setNumberStyle:NSNumberFormatterCurrencyStyle];
            _formatter.maximumFractionDigits = [NSCurrencyFormatter currencyFractionDigits];
            _formatter.minimumFractionDigits = [NSCurrencyFormatter currencyFractionDigits];
			break;
        case NSTextFormatterDecimalStyle:
            [_formatter setNumberStyle:NSNumberFormatterDecimalStyle];
            if (!minValue || [minValue intValue] < 0)
                [numberSet addCharactersInString: @"-"];
            if (customFracationDigits) {
                _formatter.maximumFractionDigits = [customFracationDigits intValue];
                _formatter.minimumFractionDigits = 0;
            }
            else {
                
                _formatter.maximumFractionDigits = 2;
                _formatter.minimumFractionDigits = 0;
            }
            break;
		case NSTextFormatterPercentStyle:
            [_formatter setNumberStyle:NSNumberFormatterPercentStyle];
			_formatter.multiplier  = @1;
            if (percentSymbol)
                _formatter.percentSymbol = percentSymbol;
            _formatter.maximumFractionDigits = 2;
            _formatter.minimumFractionDigits = 2;
			break;
        case NSTextFormatterWeightStyle:
            [_formatter setNumberStyle:NSNumberFormatterDecimalStyle];
            _formatter.maximumFractionDigits = 3;
            _formatter.minimumFractionDigits = 0;
            break;
		default:
			break;
	}
    if (minValue)
        _formatter.minimum = minValue;
    if(maxValue)
        _formatter.maximum = maxValue;

    if (style != NSTextFormatterNumbersStyle && (!minValue || [minValue intValue] < 0)){
        [numberSet addCharactersInString: @"-"];
        //[numberSet addCharactersInString: [_formatter negativePrefix]];
        //[numberSet addCharactersInString: [_formatter negativeSuffix]];
    }
	
    if (numberFormatterHandler)
        numberFormatterHandler(_formatter, style);

    NumpadKeyboardContent numpadKeyboardContent = inputTextField.numpadKeyboardContent;
    if (!_formatter.allowsFloats) {
        numpadKeyboardContent = numpadKeyboardContent & ~NumpadKeyboardContentsDot;
    }
    else {
        [numberSet addCharactersInString: _formatter.decimalSeparator];
        numpadKeyboardContent = numpadKeyboardContent | NumpadKeyboardContentsDot;
    }
    [inputTextField setNumpadKeyboardContent:numpadKeyboardContent];
    nonNumberSet = [(NSMutableCharacterSet*)[numberSet invertedSet]retain];
	[numberSet release];
}

-(void)setPercentSymbol:(NSString*)value{
    [percentSymbol release];
    percentSymbol = [value retain];
    _formatter.percentSymbol = percentSymbol;
}

-(void)setCustomFracationDigits:(NSNumber*)value {

    if (customFracationDigits && [customFracationDigits compare:value] == NSOrderedSame)
        return;
    
    [customFracationDigits release];
    customFracationDigits = [value retain];
    forceSetStyle = TRUE;
    [self setStyle:style];
    forceSetStyle = FALSE;
    NumpadKeyboardContent numpadKeyboardContent = inputTextField.numpadKeyboardContent;
    if ([customFracationDigits integerValue] == 0) {
        numpadKeyboardContent = numpadKeyboardContent & ~NumpadKeyboardContentsDot;
    }
    else {
        numpadKeyboardContent = numpadKeyboardContent | NumpadKeyboardContentsDot;
    }
    [inputTextField setNumpadKeyboardContent:numpadKeyboardContent];
}

-(void)setMaxValue:(NSNumber*)value{
    maxValueIsDefault = FALSE;
    [maxValue release];
    maxValue = [value retain];
    forceSetStyle = TRUE;
    [self setStyle:style];
    forceSetStyle = FALSE;
    
    if (/*self.style == NSTextFormatterCurrencyStyle && */self.useSmartAmount && self.maxValue && self.maxValue.floatValue == 0) {
        NumpadKeyboardContent numpadKeyboardContent = inputTextField.numpadKeyboardContent;
        numpadKeyboardContent = numpadKeyboardContent & ~NumpadKeyboardContentsMinus;
        [inputTextField setNumpadKeyboardContent:numpadKeyboardContent];
    }
}

-(void)setMinValue:(NSNumber*)value{
    minValueIsDefault = FALSE;
    [minValue release];
    minValue = [value retain];
    forceSetStyle = TRUE;
    [self setStyle:style];
    forceSetStyle = FALSE;
    NumpadKeyboardContent numpadKeyboardContent = inputTextField.numpadKeyboardContent;
    if ([minValue integerValue] >= 0) {
        numpadKeyboardContent = numpadKeyboardContent & ~NumpadKeyboardContentsMinus;
    }
    else {
        numpadKeyboardContent = numpadKeyboardContent | NumpadKeyboardContentsMinus;
    }
    [inputTextField setNumpadKeyboardContent:numpadKeyboardContent];
}

-(void)setFormatWidth:(NSUInteger)value{
    _formatter.formatWidth = value;
}

-(NSUInteger)formatWidth{
    return _formatter.formatWidth;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    BOOL result = YES;
    if (delegateFlags.textFieldShouldReturn) {
        result = [delegate textFieldShouldReturn:textField];
    }
    return result;
}

NSDecimalNumber* maxDoubleValue;

-(NSString*)stringValue:(NSDecimalNumber*)value {

	if (_formatter.numberStyle == NSNumberFormatterDecimalStyle && clearZero && [value doubleValue] == 0)
        return @"";
    if (!value || [value isEqualToNumber:[NSDecimalNumber notANumber]]){
        if (enableNil)
            return nil;
        else
            value = [NSDecimalNumber decimalNumberWithDecimal:D0];
    }
    NSUInteger orgFormatWidth = _formatter.formatWidth;
    NSString* str;
    if (style==NSTextFormatterNumbersStyle){

        if (!maxDoubleValue){
        	//don't know exactly the maximum value where rounding begins
            maxDoubleValue = (NSDecimalNumber*)[[NSDecimalNumber numberWithDouble:1000000000000000] retain];
        }

        //if value is huge then just format it with locale description
        //there is no way to format it whithout rounding
        if ([value compare:maxDoubleValue]==NSOrderedDescending){

        	str = [value stringValue];
            return str;
        }
        if (style==NSTextFormatterNumbersStyle && _formatter.formatWidth < inputTextField.text.length){
            _formatter.formatWidth=inputTextField.text.length;
        }
    }
    
    //edit string
    if (inputTextField && [inputTextField isFirstResponder]) {

        NSNumberFormatterStyle orgStyle = _formatter.numberStyle;
        
        NSString* oldPositiveFormat = [_formatter.positiveFormat retain];
        NSString* oldNegativeFormat = [_formatter.negativeFormat retain];
        NSString* oldPositivePrefix = [_formatter.positivePrefix retain];
        NSString* oldNegativePrefix = [_formatter.negativePrefix retain];
        NSString* oldPositiveSuffix =[_formatter.positiveSuffix retain];
        NSString* oldNegativeSuffix = [_formatter.negativeSuffix retain];

        BOOL orgUsesGroupingSeparator = _formatter.usesGroupingSeparator;
        NSUInteger oldFractionDigits = _formatter.maximumFractionDigits;

        if (customFracationDigits)
            _formatter.maximumFractionDigits = [customFracationDigits intValue];

        _formatter.positiveFormat =
        _formatter.negativeFormat =
        _formatter.positiveSuffix =
        _formatter.negativeSuffix =
        _formatter.positivePrefix =
        _formatter.negativePrefix = nil;
        
        _formatter.numberStyle = NSNumberFormatterDecimalStyle;
        _formatter.usesGroupingSeparator = NO;
        str = [_formatter stringFromNumber:value];
        _formatter.numberStyle = orgStyle;
        _formatter.usesGroupingSeparator = orgUsesGroupingSeparator;

        _formatter.positiveFormat = oldPositiveFormat;
        _formatter.negativeFormat = oldNegativeFormat;
        _formatter.positiveSuffix = oldPositiveSuffix;
        _formatter.negativeSuffix = oldNegativeSuffix;
        _formatter.positivePrefix = oldPositivePrefix;
        _formatter.negativePrefix = oldNegativePrefix;

        [oldPositiveFormat release];
        [oldNegativeFormat release];
        [oldPositivePrefix release];
        [oldNegativePrefix release];
        [oldPositiveSuffix release];
        [oldNegativeSuffix release];
        
        if (customFracationDigits)
            _formatter.maximumFractionDigits = oldFractionDigits;
    }
    else { //view string
        
//        if (style == NSTextFormatterCurrencyStyle)
//            str = [NSCurrencyFormatter formatCurrencyFromNumber:value];
//        else
            str = [_formatter stringFromNumber:value];
    }
    _formatter.formatWidth = orgFormatWidth;

	return str;
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    BOOL result = YES;
    if (delegateFlags.textFieldShouldBeginEditing) {
        result = [delegate textFieldShouldBeginEditing:textField];
    }
    if (!result) return FALSE;
    [prevResponder release];
    prevResponder = [[textField.window getFirstResponder] retain];
//    if(!textField.readOnly){
//        numberpadInputViewController.contents = numpadKeyboardContent;
//    }
    return TRUE;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField{
    if ([textField isKindOfClass:[UITextFieldNumeric class]] && ((UITextFieldNumeric*)textField).useVirtualNumericKeyboard && !textField.readOnly) {
        [self showVirtualNumpadKeyboard];
    }
    if (internalDidBeginEditing) return;
	NSDecimal decimal = D0;
    BOOL result = FALSE;
    //format value to edit style
    if ([textField isKindOfClass:[UITextFieldNumeric class]]){
        NSDecimalNumber* decimalNumber = ((UITextFieldNumeric*)textField).decimalNumberValue;
        if (decimalNumber){
            decimal = [decimalNumber decimalValue];
            result = TRUE;
        }
    }
    else {
        NSRange range = [textField.text rangeOfCharacterFromSet:nonNumberSet];
        //replace non numeric chars
        NSString* candidateString = nil;
        if (range.location!=NSNotFound){
            candidateString = [textField.text stringByReplacingCharactersInRange:range withString:@""];
        }
        else {
            candidateString = textField.text;
        }

        result = [self parseString:candidateString result:&decimal];
    }
    internalTextAssign = TRUE;
    if (!result && enableNil){
        //do not set nil to empty string. this is caused textFieldDidChange event
        if (textField.text.length)
            textField.text = nil;
    }
    else{
        textField.text = [self stringValue:[NSDecimalNumber decimalNumberWithDecimal:decimal]];
    }
    internalTextAssign = FALSE;
    internalDidBeginEditing = TRUE;
    if (delegateFlags.textFieldDidBeginEditing) {
        [delegate textFieldDidBeginEditing:textField];
    }
    internalDidBeginEditing = FALSE;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{

	if (delegate != nil && [delegate isKindOfClass:[UITextFieldReadOnlyDelegate class]]) return FALSE;

    BOOL result = [self processTextField:textField shouldChangeCharactersInRange:range replacementString:string];
    if (result && delegateFlags.textFieldShouldChangeCharactersInRange) {
        if (![delegate textField:textField shouldChangeCharactersInRange:range replacementString:string]){
            return NO;
        }
    }
	return result;
}

- (BOOL)processTextField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    if([string length] == 0){ //backspace
        if (self.useSmartAmount/* && self.style == NSTextFormatterCurrencyStyle*/) {
            if ([textField.text isEqualToString:@"-"] && self.maxValue && self.maxValue.floatValue == 0) return NO; // Avoid removing of '-' sign from field
        }
        if (self.textFieldHandlerDelegate && handlerDelegateFlags.textFieldHandlerShouldChangeValue){
            return [self.textFieldHandlerDelegate textFieldHandler:self shouldChangeValue:nil];
        }
		return YES;
	}
    
	if ([string rangeOfCharacterFromSet:nonNumberSet].location != NSNotFound)
        return NO;
    
    if (maxTextLength > 0) {
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
        if (newLength > maxTextLength) {
            return NO;
        }
    }
    
    NSString *currentText = textField.text;
    NSString* candidateString = [currentText stringByReplacingCharactersInRange:range withString:string];
    
//    if (self.style == NSTextFormatterCurrencyStyle && self.useSmartAmount) {
//        NSDecimal decimalValue = D0;
//        NSScanner* scanner = [NSScanner localizedScannerWithString:currentText];
//        if ([scanner scanDecimal:&decimalValue])
//        {
//            if (!CPDecimalEquals0(decimalValue) || currentText.length == 0) {
//                if ([string isEqualToString:@"-"] && self.maxValue.floatValue > 0) {
//                    NSString *currentText = textField.text;
//                    UITextRange *oldRange = [textField selectedTextRange];
//                    if ([currentText hasPrefix:@"-"]) {
//                        textField.text = [currentText stringByReplacingOccurrencesOfString:@"-" withString:@""];
//                        UITextRange *newRange = [textField textRangeFromPosition:[textField positionFromPosition:oldRange.start offset:-1] toPosition:[textField positionFromPosition:oldRange.end offset:-1]];
//                        [textField setSelectedTextRange:newRange];
//                        return NO;
//                    }
//                    else if (self.minValue.floatValue < 0) {
//                        textField.text = [string stringByAppendingString:currentText];
//                        UITextRange *newRange = [textField textRangeFromPosition:[textField positionFromPosition:oldRange.start offset:1] toPosition:[textField positionFromPosition:oldRange.end offset:1]];
//                        [textField setSelectedTextRange:newRange];
//                        return NO;
//                    }
//                }
//            }
//        }
//        
//        if (self.maxValue.integerValue == 0) {
//            if (![candidateString hasPrefix:@"-"] && candidateString.length > 0) {
//                UITextRange *oldRange = [textField selectedTextRange];
//                NSDecimal decimalValue;
//                
//                NSScanner* scanner = [NSScanner localizedScannerWithString:candidateString];
//                if ([scanner scanDecimal:&decimalValue] && !CPDecimalEquals0(decimalValue)) {
//                    textField.text = [@"-" stringByAppendingString:candidateString];
//                    UITextRange *newRange = [textField textRangeFromPosition:[textField positionFromPosition:oldRange.start offset:1] toPosition:[textField positionFromPosition:oldRange.end offset:1]];
//                    [textField setSelectedTextRange:newRange];
//                    return NO;
//                }
//            }
//        }
//    }
    
    if (/*self.style == NSTextFormatterCurrencyStyle && */self.useSmartAmount && ![candidateString hasPrefix:@"-"] && candidateString.length > 0) {
        if (self.maxValue && self.maxValue.floatValue == 0 && ![candidateString hasPrefix:@"-"]) {
            candidateString = [@"-" stringByAppendingString:candidateString];
        }
        
    }
	NSDecimal decimal;
    BOOL result = [self parseString:candidateString result:&decimal];
    if (result){
//        if (self.useSmartAmount && self.maxValue.floatValue == 0 && CPDecimalGreaterThan0(decimal)) {
//            decimal = CPDecimalNegate(decimal);
//        }
        
        if (self.textFieldHandlerDelegate && handlerDelegateFlags.textFieldHandlerShouldChangeValue){
            result = [self.textFieldHandlerDelegate textFieldHandler:self shouldChangeValue:&decimal];
        }
    }
	return result;
}

-(BOOL)parseString:(NSString*)candidateString result:(NSDecimal*)decimalValue{

    NSDecimal d = [@0 decimalValue];
    if (_formatter.minimum){
        if (CPDecimalGreaterThan([_formatter.minimum decimalValue],d)){
            d = [_formatter.minimum decimalValue];
        }
    }

    if (_formatter.maximum){
        if (CPDecimalLessThan([_formatter.maximum decimalValue],d)){
            d = [_formatter.maximum decimalValue];
        }
    }
	if (!decimalValue)
		decimalValue = &d;
	else
		*decimalValue = d;
    if (!candidateString) return NO;
	NSScanner* scanner = [NSScanner localizedScannerWithString:candidateString];
	if (![scanner scanDecimal:decimalValue])
		return NO;
    if (style==NSTextFormatterCurrencyStyle || style==NSTextFormatterQtyStyle || style==NSTextFormatterPercentStyle || style == NSTextFormatterDecimalStyle || style==NSTextFormatterWeightStyle){
        //check for one sign symbol
        if (candidateString.length>1 && [candidateString rangeOfString:@"-" options:NSCaseInsensitiveSearch range:NSMakeRange(1, candidateString.length-1)].location != NSNotFound)
            return FALSE;
    }
    
    NSUInteger maximumFractionDigits = customFracationDigits ? [customFracationDigits intValue] : _formatter.maximumFractionDigits;
	if (_formatter.allowsFloats){
		NSUInteger count = 0, len = [candidateString length];
        //check for one decimal separtator and maximumFractionDigits digits after
		NSRange range = NSMakeRange(0, len); 
		while(range.location != NSNotFound){
			range = [candidateString rangeOfString: _formatter.decimalSeparator options:0 range:range];
			if(range.location != NSNotFound) {
				range = NSMakeRange(range.location + range.length, len - (range.location + range.length));
				count++; 
				if (count>1)
					return NO;
				NSString* digits = [candidateString substringWithRange:range];
				if (digits.length>maximumFractionDigits)
					return NO;
			}
		}
	}
	
	NSDecimalNumber* decimalNumberValue = [NSDecimalNumber decimalNumberWithDecimal:*decimalValue];
	if ((*decimalValue)._exponent<-((NSInteger)maximumFractionDigits)){
        return NO;
    }
    if (_formatter.minimum && [decimalNumberValue compare: _formatter.minimum]==NSOrderedAscending){
        *decimalValue = [_formatter.minimum decimalValue];
        return NO;
    }
    if (_formatter.maximum &&[decimalNumberValue compare:_formatter.maximum]==NSOrderedDescending){
        *decimalValue = [_formatter.maximum decimalValue];
        return NO;
    }
    return YES;
}
	
- (void)textFieldDidEndEditing:(UITextField *)textField{
    
    if (virtualNumberpadViewController.textField==inputTextField && virtualNumberpadViewController.popoverController.popoverVisible){
        [virtualNumberpadViewController dismissPopover];
    }

    if (!shouldProcessDidEndEditing) return;
	
    NSDecimal decimal = D0;
    BOOL result = FALSE;

    if ([textField isKindOfClass:[UITextFieldNumeric class]] && ((UITextFieldNumeric*)textField).isDecimalValueActual){
        NSDecimalNumber* decimalNumber = ((UITextFieldNumeric*)textField).decimalNumberValue;
        if (decimalNumber){
            decimal = [decimalNumber decimalValue];
            result = TRUE;
        }
    }
    else{
        
        NSString* text = textField.text;
        //replace non numeric
        NSRange range = [text rangeOfCharacterFromSet:nonNumberSet];
        if (range.location!=NSNotFound)
            text = [textField.text stringByReplacingCharactersInRange:range withString:@""];
        
        result = [self parseString:text result:&decimal];
    }
    
    NSDecimalNumber* decimalNumberValue = !result && enableNil ? nil : [NSDecimalNumber decimalNumberWithDecimal:decimal];
    
    if ([inputTextField isKindOfClass:[UITextFieldNumeric class]]){
        //setDecimalNumberValue always formats text
        ((UITextFieldNumeric*)inputTextField).decimalNumberValue = decimalNumberValue;
    }
    else{
        internalTextAssign = TRUE;
        textField.text = [self stringValue:decimalNumberValue];
        internalTextAssign = FALSE;
    }

    

    if (delegateFlags.textFieldDidEndEditing)
        [delegate textFieldDidEndEditing:textField];
}


-(void)dealloc{
    [prevResponder release];
	[_formatter release];
	[nonNumberSet release];
	inputTextField = nil;
    [maxValue release];
    [minValue release];
    [percentSymbol release];
    [customFracationDigits release];
	[super dealloc];
}

- (void)showVirtualNumpadKeyboard {
    UIView* topMostView = nil;
    BOOL useDelayedAnimation = FALSE;
    UIResponder* responder = inputTextField;
    while((responder = [responder nextResponder])){
        if ([responder isKindOfClass:[UIViewController class]]){
            UIViewController* viewController = (UIViewController*)responder;
            if (viewController.presentingViewController){
                topMostView = viewController.view;
                if (viewController.isBeingPresented){
                    useDelayedAnimation  = TRUE;
                }
                break;
            }
        }
    }
    
    if (!topMostView){
        if ([responder isKindOfClass:[UIView class]]){
            topMostView = (UIView*)responder;
        }
        else if ([responder isKindOfClass:[UIViewController class]]){
            topMostView = ((UIViewController*)responder).view;
            if (((UIViewController*)responder).isBeingPresented){
                useDelayedAnimation  = TRUE;
            }
        }
        else{
            topMostView = inputTextField.superview;
        }
    }
    if (!virtualNumberpadViewController){
        virtualNumberpadViewController = [[VirtualNumberpadViewController alloc] initWithTextField:inputTextField];
    }
    else{
        virtualNumberpadViewController.textField = inputTextField;
    }
    virtualNumberpadViewController.popoverController.passthroughViews = @[topMostView];
    //UIView* firstResponder = [topMostView firstResponder];
    if (!useDelayedAnimation  && (!prevResponder || [prevResponder isKindOfClass:[UITextFieldNumeric class]])){
        [self presentVirtualNumberpadViewController];
    }
    else{
        [self performSelector:@selector(presentVirtualNumberpadViewController) withObject:nil afterDelay:0.3f];
    }
}

-(void)presentVirtualNumberpadViewController{
    virtualNumberpadViewController.contents = inputTextField.numpadKeyboardContent;
    [virtualNumberpadViewController presentPopoverFromRect:inputTextField.frame inView:inputTextField.superview permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField  {
    BOOL result = YES;
    
    if (self.useSmartAmount && self.maxValue && self.maxValue.floatValue == 0) {
        if (![textField.text hasPrefix:@"-"]) {
            internalTextAssign = YES;
            textField.text = [@"-" stringByAppendingString:textField.text];
            internalTextAssign = NO;
        }
    }
    
    if (delegateFlags.textFieldShouldEndEditing) {
        result = [delegate textFieldShouldEndEditing:textField];
    }
    return result;
}

- (BOOL)textFieldShouldClear:(UITextField *)textField {
    BOOL result = YES;
    if (self.textFieldHandlerDelegate && handlerDelegateFlags.textFieldHandlerShouldChangeValue){
        result = [self.textFieldHandlerDelegate textFieldHandler:self shouldChangeValue:nil];
    }
    if (result){
        if (self.useSmartAmount && self.maxValue && self.maxValue.floatValue == 0) {
            textField.text = @"0.00";
            [textField selectAllNoMenu];
            result = FALSE;
        }
        
        if (delegateFlags.textFieldShouldClear) {
            result = [delegate textFieldShouldClear:textField];
        }
        if (![inputTextField isFirstResponder]){
            if ([inputTextField isKindOfClass:[UITextFieldNumeric class]])
                ((UITextFieldNumeric*)inputTextField).decimalValue = D0;
        }
    }
    return result;
}

-(void)forwardInvocation:(NSInvocation *)anInvocation{
    if ([delegate respondsToSelector:[anInvocation selector]])
        [anInvocation invokeWithTarget:delegate];
    else
        [super forwardInvocation:anInvocation];
}

-(NSMethodSignature*)methodSignatureForSelector:(SEL)selector {
    NSMethodSignature *signature = [super methodSignatureForSelector:selector];
    if (!signature) {
        signature = [(id)delegate methodSignatureForSelector:selector];
    }
    return signature;
}

@end
