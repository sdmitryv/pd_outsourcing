//
//  UITextField4Padding.m
//  ProductBuilder
//
//  Created by Vitaliy Gervazuk on 6/7/13.
//  Copyright (c) 2013 Cloudworks. All rights reserved.
//

#import "UITextFieldPadding.h"

@implementation UITextField4Padding

- (CGRect)textRectForBounds:(CGRect)bounds;
{
    return CGRectInset([super textRectForBounds:bounds], 4.f, 0.f);
}

- (CGRect)editingRectForBounds:(CGRect)bounds;
{
    return CGRectInset([super editingRectForBounds:bounds], 4.f, 0.f);
}

@end
