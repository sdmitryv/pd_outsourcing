//
//  UITextFieldAccount.m
//  ProductBuilder
//
//  Created by Viacheslav Lulakov on 3/27/15.
//  Copyright (c) 2015 Cloudworks. All rights reserved.
//

#import "UITextFieldAccount.h"

@implementation UITextFieldAccount

-(void)insertText:(NSString *)text {
    text = [text stringByReplacingOccurrencesOfString:@"À" withString:@"|"];
    text = [text stringByReplacingOccurrencesOfString:@"Ç" withString:@"|"];
    [super insertText:text];
}

@end

@implementation UISearchBarTransparentAccount

-(void)insertText:(NSString *)text {
    text = [text stringByReplacingOccurrencesOfString:@"À" withString:@"|"];
    text = [text stringByReplacingOccurrencesOfString:@"Ç" withString:@"|"];
    [super insertText:text];
}

@end
