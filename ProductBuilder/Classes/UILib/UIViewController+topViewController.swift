//
//  UIViewController+topmost.swift
//  ProductBuilder
//
//  Created by valery on 1/5/17.
//  Copyright © 2017 Cloudworks. All rights reserved.
//

import UIKit

extension UIViewController {
    
    static func topViewController(_ viewController: UIViewController? = nil) -> UIViewController? {
        let viewController = viewController ?? UIApplication.shared.keyWindow?.rootViewController
        
        if let navigationController = viewController as? UINavigationController,
            !navigationController.viewControllers.isEmpty
        {
            return self.topViewController(navigationController.viewControllers.last)
            
        } else if let tabBarController = viewController as? UITabBarController,
            let selectedController = tabBarController.selectedViewController
        {
            return self.topViewController(selectedController)
            
        } else if let presentedController = viewController?.presentedViewController {
            return self.topViewController(presentedController)
            
        }
        else if let childControllers = viewController?.childViewControllers {
            for childViewController in childControllers {
                if childViewController.presentingViewController == nil && childViewController.presentedViewController == nil { // if we do not have a controller which presented this child controller, just pass this child
                    continue
                }
                return self.topViewController(childViewController)
            }
        }
        
        return viewController
    }
}
