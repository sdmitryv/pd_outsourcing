//
//  RTAlertView.m
//  Key Chain
//

#import "RTAlertView.h"
#import "UIViewControllerExtended.h"
#import "UIView+FirstResponder.h"
#import "UITextField+Input.h"
#import <objc/runtime.h>
#import <Product_Builder-Swift.h>

NSString* const RTAlertViewDidPresentNotification = @"RTAlertViewDidPresentNotification";
NSString* const RTAlertViewDidDismissNotification = @"RTAlertViewDidDismissNotification";
const char *isKeyboardVisibleTagKey = "isKeyboardVisibleTag";

static NSMutableArray *alerts;

//typedef struct{
//    BOOL clickedButtonAtIndex;
//    BOOL cancel;
//    BOOL willPresent;
//    BOOL didPresent;
//    BOOL willDismissWithButtonIndex;
//    BOOL didDismissWithButtonIndex;
//    BOOL shouldEnableFirstOtherButton;
//} DelegateImplementedMethods;

//@interface SDCAlertViewDelegateProxy : NSObject<RTAlertViewDelegate>{
//    DelegateImplementedMethods delegateMethods;
//    BOOL _isPresent;
//}
//@property (nonatomic, assign) id<RTAlertViewDelegate> delegate;
//@end
//
//@implementation SDCAlertViewDelegateProxy
//
//-(void)setDelegate:(id<RTAlertViewDelegate>)aDelegate{
//    _delegate = aDelegate;
//    delegateMethods.clickedButtonAtIndex = [self.delegate respondsToSelector:@selector(alertView:clickedButtonAtIndex:)];
//    delegateMethods.cancel = [self.delegate respondsToSelector:@selector(alertViewCancel:)];
//    delegateMethods.willPresent = [self.delegate respondsToSelector:@selector(willPresentAlertView:)];
//    delegateMethods.didPresent = [self.delegate respondsToSelector:@selector(didPresentAlertView:)];
//    delegateMethods.willDismissWithButtonIndex = [self.delegate respondsToSelector:@selector(alertView:willDismissWithButtonIndex:)];
//    delegateMethods.didDismissWithButtonIndex = [self.delegate respondsToSelector:@selector(alertView:didDismissWithButtonIndex:)];
//    delegateMethods.shouldEnableFirstOtherButton = [self.delegate respondsToSelector:@selector(alertViewShouldEnableFirstOtherButton:)];
//}
//
//- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
//    if (_delegate && delegateMethods.clickedButtonAtIndex){
//        [self.delegate alertView:alertView clickedButtonAtIndex:buttonIndex];
//    }
//}
//
//- (void)alertViewCancel:(UIAlertView *)alertView{
//    if (_delegate && delegateMethods.cancel){
//        [self.delegate alertViewCancel:alertView];
//    }
//}
//
//
//- (void)willPresentAlertView:(UIAlertView *)alertView{
//    
//    if (!_isPresent){
//        
//        [alerts addObject:alertView];
//        _isPresent = TRUE;
//    }
//    if ([alertView isKindOfClass:RTAlertView.class]){
//        UIResponder* responder = [UIApplication sharedApplication].windows[0].getFirstResponder;
//        if ([responder respondsToSelector:@selector(isKeyboardVisible)]){
//            if ([(id)responder isKeyboardVisible]){
//                ((RTAlertView*)alertView).formerResponder = responder;
//                SEL aSelector = @selector(setKeyboardVisible:);
//                if ([responder respondsToSelector:aSelector]){
//                    objc_setAssociatedObject(responder, isKeyboardVisibleTagKey, @(YES), OBJC_ASSOCIATION_RETAIN_NONATOMIC);
//                    [(id)responder setKeyboardVisible:NO];
//                }
//            }
//        }
//    }
//    if (_delegate && delegateMethods.willPresent)
//        [self.delegate willPresentAlertView:alertView];
//}
//
//
//- (void)didPresentAlertView:(UIAlertView *)alertView{
//    if (_delegate && delegateMethods.didPresent){
//        [self.delegate didPresentAlertView:alertView];
//    }
//    [[NSNotificationCenter defaultCenter] postNotificationName:RTAlertViewDidPresentNotification object:alertView];
//}
//
//
//- (void)alertView:(UIAlertView *)alertView willDismissWithButtonIndex:(NSInteger)buttonIndex{
//
//    if (_isPresent){
//    
//        [alerts removeObject:alertView];
//        _isPresent = FALSE;
//    }
//    
//    if (_delegate && delegateMethods.willDismissWithButtonIndex)
//        [self.delegate alertView:alertView willDismissWithButtonIndex:buttonIndex];
//}
//
//
//- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex{
//    if (_delegate && delegateMethods.didDismissWithButtonIndex){
//        [self.delegate alertView:alertView didDismissWithButtonIndex:buttonIndex];
//    }
//    if ([alertView isKindOfClass:RTAlertView.class]){
//        UIResponder* responder = ((RTAlertView*)alertView).formerResponder;
//        if ([responder respondsToSelector:@selector(setKeyboardVisible:)]){
//            NSNumber* oldKeyboardVisible = objc_getAssociatedObject(responder, isKeyboardVisibleTagKey);
//            if ([oldKeyboardVisible isKindOfClass:NSNumber.class] && oldKeyboardVisible.boolValue){
//                objc_setAssociatedObject(responder, isKeyboardVisibleTagKey, NULL, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
//                [(id)responder setKeyboardVisible:YES];
//            }
//        }
//        ((RTAlertView*)alertView).formerResponder = nil;
//    }
//    [[NSNotificationCenter defaultCenter] postNotificationName:RTAlertViewDidDismissNotification object:alertView];
//}
//
//- (BOOL)alertViewShouldEnableFirstOtherButton:(UIAlertView *)alertView{
//    if (_delegate && delegateMethods.shouldEnableFirstOtherButton){
//        return [self.delegate alertViewShouldEnableFirstOtherButton:alertView];
//    }
//    return TRUE;
//}
//
//@end


@interface RTAlertView (){
    //SDCAlertViewDelegateProxy* proxyDelegate;
    RTAlertView50* _alertView50;
}

@end

//static UIColor *fillColor = nil;
//static UIColor *borderColor = nil;

@implementation RTAlertView
//@synthesize completedBlock;

+ (void) initialize {

    //alerts = [[NSMutableArray alloc] init];
}

+(BOOL)alertViewShown{
    //return self.topMostAlertView!=nil;
    return  [RTAlertView50 alertViewShown];
}

-(instancetype)init{
    if ((self = [super init])){
        _alertView50 = [[RTAlertView50 alloc]init];
    }
    return self;
}

- (instancetype)initWithTitle:(NSString *)title message:(NSString *)message delegate:(id)delegate cancelButtonTitle:(NSString *)cancelButtonTitle otherButtonTitle:(NSString *)otherButtonTitle{
     if ((self = [super init])){
         _alertView50 = [[RTAlertView50 alloc]initWithTitle:title message:message delegate:delegate cancelButtonTitle:cancelButtonTitle otherButtonTitle:otherButtonTitle];
         _alertView50.alertView = self;
     }
    return self;
}

- (instancetype)initWithTitle:(NSString *)title message:(NSString *)message delegate:(id)delegate cancelButtonTitle:(NSString *)cancelButtonTitle otherButtonTitles:(NSString *)otherButtonTitles, ... NS_REQUIRES_NIL_TERMINATION{
    if ((self = [super init])){
        _alertView50 = [[RTAlertView50 alloc]initWithTitle:title message:message delegate:delegate cancelButtonTitle:cancelButtonTitle otherButtonTitle:nil];
        _alertView50.alertView = self;
        va_list args;
        va_start(args, otherButtonTitles);
        for (NSString *arg = otherButtonTitles; arg != nil; arg = va_arg(args, NSString*)){
            [_alertView50 addButtonWithTitle:arg];
        }
        va_end(args);
    }
    return self;
}


-(void)dealloc{
//    [super setDelegate:nil];
//    [proxyDelegate release];
//    proxyDelegate = nil;
//    [completedBlock release];
//    [_formerResponder release];
    [_alertView50 release];
    [super dealloc];
}

-(id)delegate{
    return _alertView50.delegate;
}

-(void)setDelegate:(id)delegate{
    _alertView50.delegate = delegate;
}

-(NSString*)title{
    return _alertView50.title;
}

-(void)setTitle:(NSString *)title{
    _alertView50.title = title;
}

-(NSString*)message{
    return _alertView50.message;
}

-(void)setMessage:(NSString *)message{
    _alertView50.message = message;
}

-(NSInteger)addButtonWithTitle:(nullable NSString *)title{
    return [_alertView50 addButtonWithTitle:title];
}

-(NSString*)buttonTitleAtIndex:(NSInteger)buttonIndex{
    return [_alertView50 buttonTitleAtIndex:buttonIndex];
}

-(NSInteger)numberOfButtons{
    return _alertView50.numberOfButtons;
}

-(void)setCancelButtonIndex:(NSInteger)cancelButtonIndex{
    
}

-(NSInteger)cancelButtonIndex{
    return -1;
}

-(NSInteger)firstOtherButtonIndex{
    return -1;
}

-(void)setFirstOtherButtonIndex:(NSInteger)firstOtherButtonIndex{
    
}

-(BOOL)isVisible{
    return _alertView50.visible;
}

- (void)show{
    [_alertView50 show];
}

//-(UIAlertViewStyle)alertViewStyle{
//    return UIAlertViewStyleDefault;
//}
//
//-(void)setAlertViewStyle:(UIAlertViewStyle)alertViewStyle{
//    
//}

- (nullable UITextField *)textFieldAtIndex:(NSInteger)textFieldIndex{
    return [_alertView50 textFieldAtIndex:0];
}

- (void(^)(NSInteger)) completedBlock{
    return _alertView50.completedBlock;
}

-(void)setCompletedBlock:(void (^)(NSInteger buttonIndex))aCompletedBlock{
//    [completedBlock release];
//    completedBlock = [aCompletedBlock copy];
//    self.delegate = self;
    _alertView50.completedBlock = aCompletedBlock;
}

-(void)showOnMainThread{
    [_alertView50 showOnMainThread];
//    if (![NSThread isMainThread]){
//        dispatch_async(dispatch_get_main_queue(), ^{
//            [self show];
//        });
//    }
//    else{
//        [self show];
//    }
}

//overrides current delegate to self
-(NSInteger)showOnMainThreadAndWait{
    return [_alertView50 showOnMainThreadAndWait];
    
//    clickedButtonIndex = NSNotFound;
//    if (waitSemaphore){
//        dispatch_release(waitSemaphore);
//        waitSemaphore = nil;
//    }
//    if (![NSThread isMainThread]){
//        waitSemaphore = dispatch_semaphore_create(0);
//        self.delegate = self;
//        dispatch_async(dispatch_get_main_queue(), ^{
//            [self show];
//        });
//        dispatch_semaphore_wait(waitSemaphore, DISPATCH_TIME_FOREVER);
//        dispatch_release(waitSemaphore);
//        waitSemaphore = nil;
//    }
//    else{
//        [self show];
//    }
//    return clickedButtonIndex;
}

//- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
//    clickedButtonIndex = buttonIndex;
//    [self perfromCompletedBlockWithButonIndex:buttonIndex];
//}

-(void)perfromCompletedBlockWithButonIndex:(NSInteger)buttonIndex{
//    if (completedBlock){
//        if (![NSThread isMainThread]){
//            dispatch_async(dispatch_get_main_queue(), ^{ [self perfromCompletedBlockWithButonIndex:buttonIndex];});
//        }
//        completedBlock(buttonIndex);
//        [completedBlock release];
//        completedBlock = nil;
//    }
//    if (waitSemaphore){
//        dispatch_semaphore_signal(waitSemaphore);
//    }
}

//- (void)alertView:(SDCAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex{
//    clickedButtonIndex = buttonIndex;
//    [self perfromCompletedBlockWithButonIndex:buttonIndex];
//}


+(NSInteger)alertViewCount{

    return alerts.count;
}

+(id)topMostAlertView{
    return [RTAlertView50 topMostAlertView];
    
//    if (alerts.count){
//        return [alerts lastObject];
//    }
//    for (UIWindow* window in [UIApplication sharedApplication].windows) {
//        NSArray* subviews = window.subviews;
//        if ([subviews count] > 0)
//            for (UIView* view in subviews){
//                if ([view isKindOfClass:[UIAlertView class]])
//                    return view;
//            }
//    }
    return nil;
}


+(void)closeAlerts{
    
//    for (NSInteger i = alerts.count - 1; i >= 0; i--) {
//        
//        RTAlertView *al = alerts[i];
//        al.completedBlock = nil;
//        al.delegate = nil;
//        [al dismissWithClickedButtonIndex:0 animated:NO];
//    }
    
    [alerts removeAllObjects];
}


//- (id)initWithFrame:(CGRect)frame
//{
//    if((self = [super initWithFrame:frame]))
//	{
//        proxyDelegate = [[SDCAlertViewDelegateProxy alloc]init];
//        [super setDelegate:proxyDelegate];
//    }
//	
//    return self;
//}

- (void)disableDismissForIndex:(int)index{
	
//	canIndex = index_;
//	disableDismiss = TRUE;
    [_alertView50 disableDismissForIndex:index];
}

- (void)dismissAlert{
	
//	[self dismissWithClickedButtonIndex:[self cancelButtonIndex] animated:YES];
    [_alertView50 dismissAlert];
}


-(void)dismissWithClickedButtonIndex:(NSInteger)buttonIndex animated:(BOOL)animated {
    
//	if (disableDismiss == TRUE && canIndex == buttonIndex){
//		
//	}else {
//
//	
//	[super dismissWithClickedButtonIndex:buttonIndex animated:animated];
//
//	}
    [_alertView50 dismissWithClickedButtonIndex:buttonIndex animated:animated];
}


- (void)hideAfter:(float)seconds{
	
	[self performSelector:@selector (dismissAlert) withObject:_alertView50 afterDelay:seconds];
	
}

-(UILabel*)messageLabel{
//    for(UIView* view in self.subviews){
//        if([view isKindOfClass:[UILabel class]]){
//            return (UILabel*)view;
//        }
//    }
    return nil;
}

-(void)setTransform:(CGAffineTransform)transform{
    if (self.angle){
        transform = CGAffineTransformRotate(transform,self.angle);
    }
   [_alertView50 setTransform:transform];
}

-(void)setCenter:(CGPoint)center{
//    [super setCenter:center];
//    self.frame = CGRectRound(self.frame);
}

-(void)setNumberOfRows:(NSInteger)value{
    _alertView50.numberOfRows = value;
}

-(NSInteger)numberOfRows{
    return _alertView50.numberOfRows;
}

-(void)setTag:(NSInteger)tag{
    _alertView50.tag = tag;
}
-(NSInteger)tag{
    return _alertView50.tag;
}

-(UIView*)viewWithTag:(NSInteger)tag{
    return nil;
}

-(CGRect)bounds{
    return CGRectZero;
}

@end
