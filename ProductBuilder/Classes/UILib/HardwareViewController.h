//
//  HardwareViewController.h
//  ProductBuilder
//
//  Created by Alexander Martyshko on 7/16/13.
//  Copyright (c) 2013 Cloudworks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HardwareViewController : UIViewController 

@property (nonatomic, assign) BOOL useBarcode;
@property (nonatomic, assign) BOOL useCardSwipe;
@property (nonatomic, assign) BOOL useEncryption;

- (void)launchHardware;
- (void)stopHardware;

@end
