//
//  UIScrollView+UIScrollView_ScrollIndicators.h
//  ProductBuilder
//
//  Created by valery on 2/4/14.
//  Copyright (c) 2014 Cloudworks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIScrollView(ScrollIndicators)

@property(nonatomic, assign)BOOL horizontalScrollIndicatorAlwaysVisible;
@property(nonatomic, assign)BOOL verticalScrollIndicatorAlwaysVisible;
@end
