//
//  UIViewController+Extended.m
//  StockCount
//
//  Created by Valera on 12/3/10.
//  Copyright 2010 DBBest Technologies. All rights reserved.
//

#import "UIViewControllerExtended.h"
#import "ControlProcessor.h"
#import <QuartzCore/QuartzCore.h>
#import "UIView+FirstResponder.h"
#import "SystemVer.h"
#import "NSObject+MethodExchange.h"
#import <objc/runtime.h>

#ifndef __IPHONE_8_0
    #define UIModalPresentationOverCurrentContext 6
#endif

#ifdef __IPHONE_7_0

#import "RTDropAnimationController.h"


@interface UITransitionDelegate : NSObject<UIViewControllerTransitioningDelegate>{
    RTDropAnimationController *transitioning;
}

@end

@implementation UITransitionDelegate

-(void)dealloc{
    [transitioning release];
    [super dealloc];
}

-(RTDropAnimationController*)transitioning{
    if (!transitioning){
        transitioning = [[RTDropAnimationController alloc]init];
    }
    return transitioning;
}

- (id <UIViewControllerAnimatedTransitioning>)animationControllerForPresentedController:(UIViewController *)presented presentingController:(UIViewController *)presenting sourceController:(UIViewController *)source
{
    self.transitioning.isPresenting = TRUE;
    return self.transitioning;
}

- (id <UIViewControllerAnimatedTransitioning>)animationControllerForDismissedController:(UIViewController *)dismissed
{
    self.transitioning.isPresenting = FALSE;

    return self.transitioning;
}

@end

#endif

#define CUSTOM_SIZE_MODAL_PRESENTATION_STYLE UIModalPresentationPageSheet

@interface RTDimmingView : UIView
+(id)dimmingViewForViewController:(id)view;
-(void)display:(BOOL)display withAnimationDuration:(float)animationDuration;
@end

@implementation RTDimmingView

+(id)dimmingViewForViewController:(UIViewControllerExtended*)viewController{
    RTDimmingView* dv = [[[[self class]alloc]initWithFrame:CGRectZero]autorelease];
    dv.autoresizingMask  = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    dv.backgroundColor = [UIColor blackColor];
    dv.alpha = 0;
    return dv;
}

-(void)display:(BOOL)display withAnimationDuration:(float)animationDuration{
    UIView* window = [UIApplication sharedApplication].windows[0];
    if (display){
        self.frame = window.bounds;
        [window addSubview:self];
    }
    [UIView animateWithDuration:animationDuration delay:.0f options:UIViewAnimationOptionAllowUserInteraction | UIViewAnimationOptionBeginFromCurrentState animations:^{
        if (display){
            self.alpha = 0.5;
        }
        else{
            self.alpha = 0;
        }
    }completion:^(BOOL finished){
        if (!display){
            [self removeFromSuperview];
        }
    }];
}

@end


@implementation UIViewController(FindTopmostViewController)

+ (UIViewController*) topmostModalViewController{

    UIViewController* rootController = [UIViewControllerExtended rootViewController];
    UIViewController* systemRootController = [[UIApplication sharedApplication].windows[0] rootViewController];
    
    if (!rootController || (systemRootController.presentedViewController && !systemRootController.isPopoverPresentationController)){
        rootController = systemRootController;
    }
    return [rootController traverseModalChainForUIViewController];
}
@end

@implementation UIViewController(FindModalViewController)

-(BOOL)isPopoverPresentationController{
    return [self.presentedViewController.view.superview.superview.class.description isEqual:@"_UIPopoverView"];
}

- (UIViewController*) traverseModalChainForUIViewController {
    UIViewController* modalViewController = self;
    while (modalViewController.presentedViewController) {
        UIViewController* nextCandidate = modalViewController.presentedViewController;
        if (modalViewController == nextCandidate /*|| nextCandidate.isPopoverPresentationController*/ || nextCandidate.isBeingDismissed || nextCandidate.isMovingFromParentViewController || (([nextCandidate isKindOfClass:UIViewControllerExtended.class]) && ((UIViewControllerExtended*)nextCandidate).dismissing)){
            break;
        }
        modalViewController = nextCandidate;
    }
    return modalViewController;
}
@end

CGRect CGRectRound(CGRect rect){
    rect.origin.x = roundf(rect.origin.x);
    rect.origin.y = roundf(rect.origin.y);
    return rect;
}

@interface UIViewControllerExtended(){
    //BOOL _revertingFrameBack;
   // BOOL _observingFrame;
    CGRect originalFrame;
#ifdef __IPHONE_7_0
    UITransitionDelegate* transitionDelegate;
#endif
    BOOL _simalateParentControllerViewDidAppear;
}

@end

static void * childModalControllerPropertyKey = &childModalControllerPropertyKey;
static void * parentModalControllerPropertyKey = &parentModalControllerPropertyKey;
static void * formerFirstResponderPropertyKey = &formerFirstResponderPropertyKey;

@interface UIViewController (ChildModalRelationsPrivate)

@property (nonatomic, retain)UIViewController* childModalController;
@property (nonatomic, assign)UIViewController* parentModalController;

@end

@implementation UIViewController(ChildModalRelations)

-(UIResponder*)formerFirstResponder{
    return objc_getAssociatedObject(self, formerFirstResponderPropertyKey);
}

-(void)setFormerFirstResponder:(UIResponder *)formerViewResponder{
    objc_setAssociatedObject(self, formerFirstResponderPropertyKey, formerViewResponder, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

-(UIViewController*)childModalController{
    return objc_getAssociatedObject(self, childModalControllerPropertyKey);
}

-(void)setChildModalController:(UIViewController *)childModalController{
    objc_setAssociatedObject(self, childModalControllerPropertyKey, childModalController, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

-(UIViewController*)parentModalController{
    return objc_getAssociatedObject(self, parentModalControllerPropertyKey);
}

-(void)setParentModalController:(UIViewController *)parentModalController{
    objc_setAssociatedObject(self, parentModalControllerPropertyKey, parentModalController, OBJC_ASSOCIATION_ASSIGN);
}

@end

@interface UIViewController(ModalHack)
+(void)fireViewWillAppearForSubviewsOfView:(UIView*)view animated:(BOOL)animated exceptViewController:(UIViewController*)exceptViewController;
+(void)fireViewDidAppearForSubviewsOfView:(UIView*)view animated:(BOOL)animated;
@end

@implementation UIViewController(ModalHack)

+(void)load{
//#ifdef __IPHONE_7_0
    if (/*SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7") && */[[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone){
        EXCHANGE_METHOD(dismissViewControllerAnimated:completion:, dismissViewControllerAnimatedEx:completion:);
        EXCHANGE_METHOD(presentViewController:animated:completion:, presentViewControllerEx:animated:completion:);
        EXCHANGE_METHOD(traverseModalChainForUIViewController, traverseModalChainForUIViewControllerEx);
    }
    
//#endif
}

- (UIViewController*) traverseModalChainForUIViewControllerEx {
    UIViewController* modalViewController = self;
    while (modalViewController.childModalController) {
        if (modalViewController == modalViewController.childModalController){
            break;
        }
        modalViewController = modalViewController.childModalController;
    }
    return modalViewController;
}

-(void)dismissViewControllerAnimatedEx:(BOOL)animated completion:(void (^)(void))completion{
    if ([self isKindOfClass:UIAlertController.class] || [self.presentedViewController isKindOfClass:UIAlertController.class]){
        [self dismissViewControllerAnimatedEx:animated completion:completion];
        return;
    }
    UIViewController* parentViewController = self.parentViewController;
    if ([parentViewController isKindOfClass:[UINavigationController class]]){
        [parentViewController dismissViewControllerAnimated:animated completion:completion];
        return;
    }
    if (!parentViewController || parentViewController!=self.parentModalController) return;
    
    void(^completionBlock)(void) = ^{
        [self willMoveToParentViewController:nil];
        [self removeFromParentViewController];
        [self.view removeFromSuperview];
        [self didMoveToParentViewController:nil];
        if (self.parentModalController.childModalController==self){
            self.parentModalController.childModalController = nil;
        }
        [parentViewController viewDidAppear:TRUE];
        [self.class fireViewDidAppearForSubviewsOfView:parentViewController.view animated:TRUE];
        if (completion)
            completion();
        //check if parent shown new child controller. In this case transfer former responder to it
        UIViewController* newChildController = self.parentModalController.childModalController;
        if (newChildController){
            if (!newChildController.formerFirstResponder){
                newChildController.formerFirstResponder = self.formerFirstResponder;
                self.formerFirstResponder = nil;
            }
        }
        else if (self.formerFirstResponder){
            [self.formerFirstResponder becomeFirstResponder];
            self.formerFirstResponder = nil;
        }
    };
    if (animated){
        parentViewController.view.window.userInteractionEnabled = FALSE;
        [UIView animateWithDuration:0.3f delay:0.0f options:UIViewAnimationOptionBeginFromCurrentState animations:^{
            [parentViewController viewWillAppear:TRUE];
            [self.class fireViewWillAppearForSubviewsOfView:parentViewController.view animated:TRUE exceptViewController:self];
            self.view.frame = CGRectMake(0, parentViewController.view.bounds.size.height, parentViewController.view.bounds.size.width, parentViewController.view.bounds.size.height);
        } completion:^(BOOL finished) {
            completionBlock();
            parentViewController.view.window.userInteractionEnabled = TRUE;
        }];
    }
    else{
        [parentViewController viewWillAppear:FALSE];
        [self.class fireViewWillAppearForSubviewsOfView:parentViewController.view animated:FALSE exceptViewController:self];
        completionBlock();
    }
}

+(void)fireViewWillAppearForSubviewsOfView:(UIView*)view animated:(BOOL)animated exceptViewController:(UIViewController*)exceptViewController{
    if (!view) return;
    Class vcc = [UIViewController class];
    for (UIView* subview in  view.subviews){
        UIResponder* responder = [subview nextResponder];
        if (responder==exceptViewController) continue;
        if ([responder isKindOfClass:vcc]){
            [((UIViewController*)responder) viewWillAppear:animated];
        }
        [self fireViewWillAppearForSubviewsOfView:subview animated:animated exceptViewController:exceptViewController];
    }
}

+(void)fireViewDidAppearForSubviewsOfView:(UIView*)view animated:(BOOL)animated{
    if (!view) return;
    Class vcc = [UIViewController class];
    for (UIView* subview in  view.subviews){
        UIResponder* responder = [subview nextResponder];
        if ([responder isKindOfClass:vcc]){
            [((UIViewController*)responder) viewDidAppear:animated];
        }
        [self fireViewDidAppearForSubviewsOfView:subview animated:animated];
    }
}

-(void)presentViewControllerEx:(UIViewController *)viewControllerToPresent animated:(BOOL)flag completion:(void (^)(void))completion{
    if ([viewControllerToPresent isKindOfClass:UIAlertController.class]){
        [self presentViewControllerEx:viewControllerToPresent animated:flag completion:completion];
        return;
    }
    UIViewController* parentController = self;
    if (!parentController.parentViewController){
        parentController = self.view.window.rootViewController;
        for (UIViewController* controller in parentController.childViewControllers){
            if (controller.childModalController){
                parentController = controller;
                while (parentController.childModalController) {
                    parentController = parentController.childModalController;
                }
                break;
            }
        }
    }
    parentController.childModalController = viewControllerToPresent;
    viewControllerToPresent.parentModalController = parentController;
    
    UIResponder* firstResponder = [[UIApplication sharedApplication].windows[0] getFirstResponder];
    if (firstResponder) {
        viewControllerToPresent.formerFirstResponder = firstResponder;
        [firstResponder resignFirstResponder];
    }
    
    void(^completionBlock)(void) = ^{
        [viewControllerToPresent didMoveToParentViewController:parentController];
        if (completion)
            completion();
    };
    void(^showBlock)(void) = ^{
        viewControllerToPresent.view.frame = parentController.view.bounds;
    };
    [parentController addChildViewController:viewControllerToPresent];
    [parentController.view addSubview:viewControllerToPresent.view];
    
    if (flag){
        parentController.view.window.userInteractionEnabled = FALSE;
        viewControllerToPresent.view.frame = CGRectMake(0, parentController.view.bounds.size.height, parentController.view.bounds.size.width, parentController.view.bounds.size.height);
        [UIView animateWithDuration:0.3f delay:0.0f options:UIViewAnimationOptionBeginFromCurrentState animations:showBlock completion:^(BOOL finished) {
            parentController.view.window.userInteractionEnabled = TRUE;
            completionBlock();
        }];
    }
    else{
        showBlock();
        completionBlock();
    }
}

@end

@interface UIViewControllerExtended()
@end

@implementation UIViewControllerExtended

@synthesize isAppeared;
@synthesize completionBlock = completion;
@synthesize keyboardIsShown;

UIViewController* rootViewController;

+(void)setRootViewController:(UIViewController*)controller{
    rootViewController = controller;
}

+(UIViewController*)rootViewController{
    return rootViewController;
}

-(id)init {
    
    if (self = [super init]) {
        
        _animated = YES;
    }
    
    return self;
}


-(id)initWithNibName:(NSString *)nibName bundle:(NSBundle *)bundle {
    
    if (self = [super initWithNibName:nibName bundle:bundle]) {
        
        _animated = YES;
    }
    
    return self;
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:TRUE];
    _dismissing = FALSE;
    if (!self.childModalController && ([self isBeingPresented] || [self isMovingToParentViewController])){
        
        isBeingShowed = TRUE;
    }
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    if (self.childModalController) {
        self.childModalController = nil;
    }
    isBeingShowed = FALSE;
    isAppeared = TRUE;
    _dismissing = FALSE;
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    if ([self isBeingDismissed] || [self isMovingFromParentViewController]){
        _dismissing = TRUE;
    }
    if(self.parentModalController && !self.childModalController){
        //we are the child
        //simulate viewWillApear on parent
        if (!self.suppressFiringParentAppearance && self.presentingViewController && self.presentingViewController!=self.parentModalController){
            _simalateParentControllerViewDidAppear = TRUE;
            [self.class fireViewWillAppearForSubviewsOfView:self.parentModalController.view animated:animated exceptViewController:self];
        }
    }
}

- (void)viewDidDisappear:(BOOL)animated{
	[super viewDidDisappear:animated];
//    if (_observingFrame){
//        [self.view removeObserver:self forKeyPath:@"frame"];
//        _observingFrame = FALSE;
//    }
	if(self.parentModalController && !self.childModalController){
        //we are the child
        [delegate modalViewControllerWasDismissed:self];
        _dismissing = FALSE;
        //simulate viewDidApear on parent
        if (_simalateParentControllerViewDidAppear){
            _simalateParentControllerViewDidAppear = FALSE;
            [self.class fireViewDidAppearForSubviewsOfView:self.parentModalController.view animated:animated];
        }
        
        self.parentModalController.childModalController = nil;
		self.parentModalController = nil;
        
        if (completion)
            completion();
        delegate = nil;
        [completion release];
        completion = nil;
	}
}

-(void)childViewDidDisappear {
    self.childModalController = nil;
}

-(void)handleDismissViewController{
    if (self.parentModalController && !self.childModalController){
        //we are the child
        _dismissing = TRUE;
        [self.view.superview.layer removeAllAnimations];
        [dimmingView display:FALSE withAnimationDuration:0.3f];
        [dimmingView release];
        dimmingView = nil;
        [[self.view getFirstResponder] resignFirstResponder];
    }
    else{
        if (self.childModalController && [self.childModalController isKindOfClass:[UIViewControllerExtended class]]){
            //we are the parent
            ((UIViewControllerExtended *)self.childModalController)->_dismissing = TRUE;
            [self.childModalController.view.superview.layer removeAllAnimations];
        }
    }
#ifdef __IPHONE_7_0
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7") && self.presentingViewController && [[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)
        [self removeFromParentViewControllerEx];
#endif
}

#ifdef __IPHONE_7_0
-(void)removeFromParentViewControllerEx{
    if (self.parentViewController){
        [self willMoveToParentViewController:nil];
        [self removeFromParentViewController];
        [self didMoveToParentViewController:nil];
    }
    if ([self.presentedViewController isKindOfClass:[UIViewControllerExtended class]]){
        [(UIViewControllerExtended*)self.presentedViewController removeFromParentViewControllerEx];
    }
}
#endif

- (void)dismissViewControllerAnimated: (BOOL)animated completion: (void (^)(void))aCompletion{
    //handle presentedViewController is popover
    if (self.isPopoverPresentationController){
        [super dismissViewControllerAnimated:animated completion:aCompletion];
        return;
    }
    //it seems popover controllers is shown by rootViewControllers
    if (self.view.window.rootViewController.presentedViewController!=self && self.view.window.rootViewController.presentedViewController.popoverPresentationController){
        [self.view.window.rootViewController.presentedViewController dismissViewControllerAnimated:animated completion:nil];
    }
    if (self.presentingViewController.isBeingDismissed || self.presentingViewController.isBeingPresented || self.presentedViewController.isBeingDismissed || self.presentedViewController.isBeingPresented)
    {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.1 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            [self dismissViewControllerAnimated:animated completion:aCompletion];
        });
        return;
    }
    UIViewController* parentController = self.presentingViewController?:self.parentModalController;
    [self handleDismissViewController];
    [super dismissViewControllerAnimated:animated completion:^{
#ifdef __IPHONE_7_0
        if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7") &&  self.presentingViewController)
            [self removeFromParentViewControllerEx];
#endif
        //check if parent shown new child controller. In this case transfer former responder to it
        UIViewController* newChildController = parentController.presentedViewController?:parentController.childModalController;
        if (newChildController){
            if (!newChildController.formerFirstResponder){
                newChildController.formerFirstResponder = self.formerFirstResponder;
                self.formerFirstResponder = nil;
            }
        }
        else if (self.formerFirstResponder){
            [self.formerFirstResponder becomeFirstResponder];
            self.formerFirstResponder = nil;
        }
        if (aCompletion){
            aCompletion();
        }
    }];
}

-(void)dismissModalViewControllerAnimated:(BOOL)animated{
    [self handleDismissViewController];
    [super dismissModalViewControllerAnimated:animated];
}

+ (void)showModal:(UIViewController*)parentController childController:(UIViewControllerExtended*)childController style:(UIModalPresentationStyle)style useViewSize:(BOOL)useViewSize delegate:(id<UIModalViewControllerDelegate>) aDelegate{
    
    [[self class] showModal:parentController childController:childController style:style useViewSize:useViewSize delegate:aDelegate completion:nil];
}

+ (void)showModal:(UIViewController*)parentController childController:(UIViewControllerExtended*)childController style:(UIModalPresentationStyle)style useViewSize:(BOOL)useViewSize completion:(void (^)(void))completion{
    
    [[self class] showModal:parentController childController:childController style:style useViewSize:useViewSize delegate:nil completion:completion];
}

+ (void)showModal:(UIViewController*)parentController childController:(UIViewControllerExtended*)childController style:(UIModalPresentationStyle)style useViewSize:(BOOL)useViewSize delegate:(id<UIModalViewControllerDelegate>) aDelegate completion:(void (^)(void))completion{
    
#ifdef __IPHONE_8_0
    if (style == UIModalPresentationCurrentContext && SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8") && [[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)
        style = UIModalPresentationOverCurrentContext;
#endif
    
    if ([parentController isKindOfClass:[UIViewControllerExtended class]]){
        UIViewControllerExtended* parentModalControllerEx = (UIViewControllerExtended*)parentController;
        if (parentModalControllerEx->isBeingShowed)
        {
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.1 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                [[self class]showModal:parentController childController:childController style:style useViewSize:useViewSize delegate:aDelegate completion:completion];
            });
            return;
        }
        if (parentModalControllerEx->_dismissing){
            //do nothing
            return;
        }
    }
    parentController.childModalController = childController;
    
    UIResponder* firstResponder = [[UIApplication sharedApplication].windows[0] getFirstResponder];
    childController.formerFirstResponder = firstResponder;
    [childController.formerFirstResponder resignFirstResponder];
    
	childController.parentModalController = parentController;
    childController->delegate = aDelegate;
    [childController->completion release];
    childController->completion = [completion copy];
    
    UIModalPresentationStyle newStyle = style;

    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad){
        if (style == UIModalPresentationFormSheet || style == UIModalPresentationPageSheet) {
            if (parentController.modalPresentationStyle == UIModalPresentationPageSheet) {
                newStyle = UIModalPresentationFormSheet;
            }
            else if (parentController.modalPresentationStyle == UIModalPresentationFormSheet) {
                newStyle = UIModalPresentationPageSheet;
            }
        }
    }
    else{
        newStyle = UIModalPresentationCurrentContext;
        useViewSize = FALSE;
    }
    
    childController.modalPresentationStyle = newStyle;
    childController.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
    childController->useCustomSize = useViewSize;
	
    childController->originalSize = childController.view.bounds.size;
	childController->customSizeHandled = FALSE;
	[childController->tx release];
    childController->tx = nil;
    [childController->ty release];
    childController->ty = nil;
    [childController->th release];
    childController->th = nil;
    childController->keyboardIsShown = FALSE;
    childController->_dismissing = FALSE;

    BOOL doHanldeCustomSize = useViewSize;
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)
    {
        RTDimmingView* dv = nil;
        //detect should we show custom dimming view
        if (parentController.presentingViewController && (parentController.modalPresentationStyle==UIModalPresentationFormSheet ||
                                                          parentController.modalPresentationStyle==UIModalPresentationPageSheet) && SYSTEM_VERSION_LESS_THAN(@"7")){
            dv = [RTDimmingView dimmingViewForViewController:childController];
            [childController->dimmingView release];
            childController->dimmingView = [dv retain];
        }
        
        if (dv){
            [dv display:TRUE withAnimationDuration:0.3];
        }
    }
#ifdef __IPHONE_7_0
        if (/*useViewSize && */(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7")) && !([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad && (childController.modalPresentationStyle==UIModalPresentationCurrentContext || childController.modalPresentationStyle==UIModalPresentationOverCurrentContext)))
        {
            if (!childController->transitionDelegate){
                childController->transitionDelegate = [[UITransitionDelegate alloc]init];
            }
            childController->transitionDelegate.transitioning.parentViewController = parentController;
            childController->transitionDelegate.transitioning.presentationDuration = childController->transitionDelegate.transitioning.dismissalDuration = 0.3 * childController.view.bounds.size.height/407.0f;
            childController.transitioningDelegate = childController->transitionDelegate;
            if (useViewSize){
                childController.modalPresentationStyle = UIModalPresentationCustom;
            }
            doHanldeCustomSize = FALSE;
            childController->customSizeHandled = TRUE;
        }
        //observe changing frame value of parent view to stop it moving after closing the child
//        if ((parentController.modalPresentationStyle == UIModalPresentationCustom || parentController.modalPresentationStyle == UIModalPresentationPageSheet || parentController.modalPresentationStyle == UIModalPresentationFormSheet)
//            && [parentController isKindOfClass:[UIViewControllerExtended class]] && !((UIViewControllerExtended*)parentController)->_observingFrame){
//            [parentController.view addObserver:parentController forKeyPath:@"frame" options:NSKeyValueObservingOptionNew | NSKeyValueObservingOptionOld context:parentController];
//            ((UIViewControllerExtended*)parentController)->_observingFrame = TRUE;
//            ((UIViewControllerExtended*)parentController)->originalFrame = parentController.view.frame;
//        }
#endif
    [parentController presentViewController:childController animated:childController.animated completion:^{
        childController->isBeingShowed = FALSE;
#ifdef __IPHONE_7_0
        if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7"))
            if (childController.modalPresentationStyle == UIModalPresentationCurrentContext && [[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad){
                UIViewController* parentViewController = childController.presentingViewController.parentViewController ?:childController.presentingViewController;
                if (parentViewController){
                    [parentViewController addChildViewController:childController];
                    [childController didMoveToParentViewController:parentViewController];
                }
            }
#endif
    }];
    /*
    if (!childController.presentingViewController){
        //clear parent-child hirerarchy if presentViewController failed
        childController.parentModalController.childModalController = nil;
        childController.parentModalController = nil;
    }
    */
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        if (useViewSize){
            if (doHanldeCustomSize){
                [childController handleCustomSize];
            }
            if (childController->keyboardWillShowDelayedNotification){
                [childController keyboardWillShow:childController->keyboardWillShowDelayedNotification];
                [childController->keyboardWillShowDelayedNotification release];
                childController->keyboardWillShowDelayedNotification = nil;
            }
        }
    }
#ifndef __IPHONE_7_0
    else{
        if (!CGPointEqualToPoint(childController.view.superview.center,childController.view.center)){
            childController.view.frame = childController.view.superview.frame;
        }
    }
#endif
    
}
//observe frame and ignore its changing
- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
//    if (_revertingFrameBack){
//        return;
//    }
//	if ([change[NSKeyValueChangeKindKey] intValue] == NSKeyValueChangeSetting) {
//        if (context && [(id)context isKindOfClass:[UIViewControllerExtended class]]){
//            UIViewControllerExtended* controller = (UIViewControllerExtended*)context;
//            //we are thre parent
//            if (self.childModalController && !CGRectEqualToRect(CGRectZero, controller->originalFrame)){
//                //revert back frame's value
//                _revertingFrameBack = TRUE;
//                [object setValue: [NSValue valueWithCGRect:controller->originalFrame] forKey:keyPath];
//                _revertingFrameBack = FALSE;
//                return;
//            }
//        }
//		id oldValue = change[NSKeyValueChangeOldKey];
//		id newValue = change[NSKeyValueChangeNewKey];
//		if ((!oldValue && newValue) || (oldValue && ![oldValue isEqual:newValue])){
//            //we are the parent
//            if (self.childModalController){
//                //revert back frame's value
//                _revertingFrameBack = TRUE;
//                [object setValue:oldValue forKey:keyPath];
//                _revertingFrameBack = FALSE;
//            }
//        }
//
//	}
}

//show self modally

- (void)showModalFormSheetWithCompletion:(void (^)(void))aCompletion{
    UIViewController* parentController = [UIViewController topmostModalViewController];
    [[self class] showModal:parentController childController:self style:UIModalPresentationFormSheet
                useViewSize:FALSE completion:aCompletion];
}

- (void)showModalPageSheetWithCompletion:(void (^)(void))aCompletion{
    UIViewController* parentController = [UIViewController topmostModalViewController];
    [[self class] showModal:parentController childController:self style:UIModalPresentationPageSheet
                useViewSize:FALSE completion:aCompletion];
}


-(void)showModal:(BOOL)useViewSize delegate:(id<UIModalViewControllerDelegate>) aDelegate{
    //find topmost controller
    UIViewController* parentController = [UIViewController topmostModalViewController];
    [[self class] showModal:parentController childController:self style:useViewSize ? CUSTOM_SIZE_MODAL_PRESENTATION_STYLE : UIModalPresentationCurrentContext  useViewSize:useViewSize delegate:aDelegate];
}

- (void)showModal:(BOOL)useViewSize completion:(void (^)(void))aCompletion{
    UIViewController* parentController = [UIViewController topmostModalViewController];
    [[self class] showModal:parentController childController:self style:useViewSize ? CUSTOM_SIZE_MODAL_PRESENTATION_STYLE :UIModalPresentationCurrentContext 
                useViewSize:useViewSize completion:aCompletion];
}

- (void)showModal:(UIViewControllerExtended*)controller style:(UIModalPresentationStyle)style useViewSize:(BOOL)useViewSize delegate:(id<UIModalViewControllerDelegate>) aDelegate{
    [[self class] showModal:self childController:controller style:style useViewSize:useViewSize delegate:aDelegate];
}

- (void)showModal:(UIViewControllerExtended*)controller style:(UIModalPresentationStyle)style useViewSize:(BOOL)useViewSize completion:(void (^)(void))aCompletion{
    [[self class] showModal:self childController:controller style:style useViewSize:useViewSize completion:aCompletion];
}

-(void)handleCustomSize{
    if (!useCustomSize || customSizeHandled) return;
    //originalCenter = self.view.superview.center;
    CGSize newSize = self.view.bounds.size;
    CGFloat widthD = originalSize.width - newSize.width;
    CGFloat heightD  = originalSize.height - newSize.height;
    CGRect svBounds = self.view.superview.bounds;
    svBounds.size.width+=widthD;
    svBounds.size.height+=heightD;
    self.view.autoresizingMask = self.view.superview.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleTopMargin
    | UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    self.view.superview.bounds = svBounds;
    
    //UIInterfaceOrientation  orientation = [UIDevice currentDevice].orientation;
//    if (self.modalPresentationStyle = UIModalPresentationFormSheet){
//        self.view.superview.center = CGPointApplyAffineTransform(CGPointMake(self.view.superview.center.x, self.view.superview.center.y-1), self.view.superview.transform);
//    }
    
    self.view.superview.frame = CGRectRound(self.view.superview.frame);
    customSizeHandled = TRUE;
}

- (void)showModalViewController:(UIViewControllerExtended*)controller{
    [self showModalViewController:controller delegate:self];
}
- (void)showFormViewController:(UIViewControllerExtended*)controller{
    [self showFormViewController:controller delegate:self];
}
- (void)showPageViewController:(UIViewControllerExtended*)controller{
    [self showPageViewController:controller delegate:self];
    
}

- (void)showFormViewController:(UIViewControllerExtended*)controller delegate:(id<UIModalViewControllerDelegate>) aDelegate{
	[self showModal:controller style:UIModalPresentationFormSheet useViewSize:FALSE delegate:aDelegate];
}

- (void)showPageViewController:(UIViewControllerExtended*)controller delegate:(id<UIModalViewControllerDelegate>) aDelegate{
    [self showModal:controller style:UIModalPresentationPageSheet useViewSize:FALSE delegate:aDelegate];
}

- (void)showModalViewController:(UIViewControllerExtended*)controller delegate:(id<UIModalViewControllerDelegate>) aDelegate{
	[self showModal:controller style:UIModalPresentationCurrentContext useViewSize:FALSE delegate:aDelegate];
}

- (void)showModalViewController:(UIViewControllerExtended*)controller useViewSize:(BOOL)useViewSize delegate:(id<UIModalViewControllerDelegate>) aDelegate{
    [self showModal:controller style:CUSTOM_SIZE_MODAL_PRESENTATION_STYLE useViewSize:useViewSize delegate:aDelegate];
}


- (void)showFormViewController:(UIViewControllerExtended*)controller completion:(void (^)(void))aCompletion{
	[self showModal:controller style:UIModalPresentationFormSheet useViewSize:FALSE completion:aCompletion];
}

- (void)showPageViewController:(UIViewControllerExtended*)controller completion:(void (^)(void))aCompletion{
    [self showModal:controller style:UIModalPresentationPageSheet useViewSize:FALSE completion:aCompletion];
}

- (void)showModalViewController:(UIViewControllerExtended*)controller completion:(void (^)(void))aCompletion{
	[self showModal:controller style:UIModalPresentationCurrentContext useViewSize:FALSE completion:aCompletion];
}

- (void)showModalViewController:(UIViewControllerExtended*)controller useViewSize:(BOOL)useViewSize completion:(void (^)(void))aCompletion{
    [self showModal:controller style:useViewSize ? CUSTOM_SIZE_MODAL_PRESENTATION_STYLE : UIModalPresentationCurrentContext useViewSize:useViewSize completion:aCompletion];
}

- (void)presentViewController:(UIViewController *)viewControllerToPresent animated:(BOOL)animated completion:(void (^)(void))comp {
#ifdef __IPHONE_7_0
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7") && [[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone)
    {
        //        if (!transitionDelegate){
        //            transitionDelegate = [[UITransitionDelegate alloc]init];
        //        }
        //        transitionDelegate.transitioning.parentViewController = self;
        //        viewControllerToPresent.transitioningDelegate = transitionDelegate;
    }
#endif
    [super presentViewController:viewControllerToPresent animated:animated completion:comp];
}

- (void)modalViewControllerWasDismissed:(UIViewControllerExtended*)controller{

}

-(void)clearRes{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void)setupRes{
    [[NSNotificationCenter defaultCenter] addObserver:self 
											 selector:@selector(keyboardWillShow:) 
												 name:UIKeyboardWillShowNotification 
											   object:self.view.window];
    [[NSNotificationCenter defaultCenter] addObserver:self 
											 selector:@selector(keyboardDidShow:) 
												 name:UIKeyboardDidShowNotification 
											   object:self.view.window];
    
	[[NSNotificationCenter defaultCenter] addObserver:self 
											 selector:@selector(keyboardWillHide:) 
												 name:UIKeyboardWillHideNotification 
											   object:self.view.window];
//    [[NSNotificationCenter defaultCenter] addObserver:self
//											 selector:@selector(keyboardWillChangeFrame:)
//												 name:UIKeyboardWillChangeFrameNotification
//											   object:self.view.window];
//    [[NSNotificationCenter defaultCenter] addObserver:self
//											 selector:@selector(keyboardDidChangeFrame:)
//												 name:UIKeyboardDidChangeFrameNotification
//											   object:self.view.window];
    [[NSNotificationCenter defaultCenter] addObserver:self
											 selector:@selector(keyboardDidHide:) 
												 name:UIKeyboardDidHideNotification 
											   object:self.view.window];
}

-(void)dealloc{
#ifdef __IPHONE_7_0
    [transitionDelegate release];
#endif
//    if (_observingFrame){
//        [self.view removeObserver:self forKeyPath:@"frame"];
//    }
    [keyboardWillShowDelayedNotification release];
    [completion release]; 
    [self clearRes];
    [tx release];
    [ty release];
    [th release];
    [dimmingView release];
	[super dealloc];
}

-(void)viewDidLoad{
    [self setupRes];
	[super viewDidLoad];
	[ControlProcessor processView:self.view];
}

- (void)keyboardDidHide:(NSNotification *)n{
    
    if (self.skeepKeyboardAnimation) return;
    if (_dismissing || !keyboardIsShown) return;
    //we are the child
    //remove any blur due to floating origin
    if (!self.childModalController && self.parentModalController && useCustomSize && !_dismissing){
        if (self.modalPresentationStyle == UIModalPresentationPageSheet
            || self.modalPresentationStyle == UIModalPresentationFormSheet
#ifdef __IPHONE_7_0
            || (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7") && self.modalPresentationStyle == UIModalPresentationCustom)
#endif
            ){
            
            UIView* superview = self.view.superview;
#ifdef __IPHONE_7_0
            if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7"))
                superview = self.modalPresentationStyle == UIModalPresentationCustom ? self.view : self.view.superview;
#endif
            superview.frame = CGRectRound(superview.frame);
        }
    }
    keyboardIsShown = NO;
}
- (void)keyboardWillHide:(NSNotification *)n{
    
    if (self.skeepKeyboardAnimation) return;
    
    if (self.presentedViewController.modalPresentationStyle == UIModalPresentationPageSheet && self.modalPresentationStyle == UIModalPresentationFormSheet){
        //cancel all animations
        CAAnimation* animationPosition = [self.view.superview.layer animationForKey:@"position"];
        if ([animationPosition isKindOfClass:[CABasicAnimation class]]){
            CABasicAnimation* animationPositionValue = (CABasicAnimation*)animationPosition;
            CGPoint fromValue = [(NSValue*)[animationPositionValue fromValue] CGPointValue];
            self.view.superview.layer.position = fromValue;
        }
        [self.view.superview.layer removeAllAnimations];
    }
    if (![self.view getFirstResponder]) return;
    if (!keyboardIsShown || _dismissing) {
        return;
    }
    //if we are the child
    //restore original position
    if ((!self.childModalController || !self.childModalController.parentViewController) && self.parentModalController && useCustomSize
        && !_dismissing){
        if (self.modalPresentationStyle == UIModalPresentationPageSheet
#ifdef __IPHONE_7_0
            || (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7") && self.modalPresentationStyle == UIModalPresentationCustom)
#endif
            ){
            
            UIView* superview = self.view.superview;
#ifdef __IPHONE_7_0
            if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7"))
                superview = self.modalPresentationStyle == UIModalPresentationCustom ? self.view : self.view.superview;
#endif
            NSNumber* txCopy = tx;
            NSNumber* tyCopy = ty;
            NSNumber* thCopy = th;
            CGFloat duration = [n.userInfo[UIKeyboardAnimationDurationUserInfoKey] floatValue];
            [UIView animateWithDuration:duration  delay:0 options: UIViewAnimationOptionBeginFromCurrentState animations:^{
                if ([thCopy floatValue]){
                    CGRect bounds = superview.bounds;
                    bounds.size.height+=[thCopy floatValue];
                    superview.bounds = bounds;
                }
                if ([txCopy floatValue] || [tyCopy floatValue]){
                    CGPoint newCenter = CGPointApplyAffineTransform(CGPointMake([txCopy floatValue], [tyCopy floatValue]), superview.transform);
                    superview.center = CGPointMake(superview.center.x - newCenter.x,superview.center.y - newCenter.y);
                }
                superview.frame = CGRectRound(superview.frame);
            } completion:nil];
        }
        else if (self.modalPresentationStyle == UIModalPresentationFormSheet){
            //self.view.superview.frame = CGRectRound(self.view.superview.frame);
            CGRect keyboardEndFrame;
            NSDictionary* userInfo = [n userInfo];
            [userInfo[UIKeyboardFrameEndUserInfoKey] getValue:&keyboardEndFrame];
            
            if (keyboardEndFrame.size.width<3 && !tx & !ty){
                UIView* superview = self.view.superview;
                CAAnimation* animationPosition = [superview.layer animationForKey:@"position"];
                if ([animationPosition isKindOfClass:[CABasicAnimation class]]){
                    CABasicAnimation* animationPositionValue = (CABasicAnimation*)animationPosition;
                    CGPoint fromValue = [(NSValue*)[animationPositionValue fromValue] CGPointValue];
                    superview.layer.position = fromValue;
                }
                [superview.layer removeAllAnimations];
            }
        }
        [tx release];
        tx = nil;
        [ty release];
        ty = nil;
        [th release];
        th = nil;
    }
    
    //keyboardIsShown = NO;
    lastKeyboardFrame.size = CGSizeZero;
}

//- (void)keyboardWillChangeFrame:(NSNotification *)n{
//    [self keyboardWillShow:n];
//}
//
//- (void)keyboardDidChangeFrame:(NSNotification *)n{
//    [self keyboardDidShow:n];
//}


- (void)keyboardWillShow:(NSNotification *)n{
    
    if (self.skeepKeyboardAnimation) return;
    
    if (self.presentedViewController.modalPresentationStyle == UIModalPresentationPageSheet && self.modalPresentationStyle == UIModalPresentationFormSheet){
        //cancel all animations
        CAAnimation* animationPosition = [self.view.superview.layer animationForKey:@"position"];
        if ([animationPosition isKindOfClass:[CABasicAnimation class]]){
            CABasicAnimation* animationPositionValue = (CABasicAnimation*)animationPosition;
            CGPoint fromValue = [(NSValue*)[animationPositionValue fromValue] CGPointValue];
            self.view.superview.layer.position = fromValue;
        }
        [self.view.superview.layer removeAllAnimations];
    }
    if (![self.view getFirstResponder]) return;
    NSDictionary* userInfo = [n userInfo];
    // get the size of the keyboard
    CGRect keyboardEndFrame;
    [userInfo[UIKeyboardFrameEndUserInfoKey] getValue:&keyboardEndFrame];
    //do nothing; probably its our fake empty kb
    if ((!keyboardIsShown || lastKeyboardFrame.size.width<3) && !tx && !ty && !th && (self.modalPresentationStyle == UIModalPresentationPageSheet || self.modalPresentationStyle == UIModalPresentationCustom) && keyboardEndFrame.size.width<3) return;
    if (_dismissing || (/*keyboardIsShown &&*/
                       (self.modalPresentationStyle == UIModalPresentationPageSheet || self.modalPresentationStyle == UIModalPresentationCustom)
                       && CGSizeEqualToSize(lastKeyboardFrame.size,keyboardEndFrame.size)
                       && CGPointEqualToPoint(lastKeyboardFrame.origin,keyboardEndFrame.origin)
                       )) return;
    
    CGFloat duration = [userInfo[UIKeyboardAnimationDurationUserInfoKey] floatValue];
    
    if (!self.childModalController && self.parentModalController && useCustomSize){
        if (!customSizeHandled){
            [keyboardWillShowDelayedNotification release];
            keyboardWillShowDelayedNotification = [n retain];
            return;
        }
        UIView* superview = self.view.superview;
#ifdef __IPHONE_7_0
        if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7"))
            superview = self.modalPresentationStyle == UIModalPresentationCustom ? self.view : self.view.superview;
#endif
        
        
        CGRect keyboardRect2 = [superview convertRect:keyboardEndFrame fromView:nil];
//        if (keyboardRect2.origin.y > keyboardRect2.size.height && keyboardRect2.size.height>3) {
//            return;
//        }
        keyboardRect2 = CGRectMake(keyboardRect2.origin.x + [tx floatValue], keyboardRect2.origin.y + [ty floatValue], keyboardRect2.size.width, keyboardRect2.size.height);
        
        CGRect windowBounds = superview.window.bounds;
        CGRect statusBarFrame = [[UIApplication sharedApplication] statusBarFrame];
        if (windowBounds.size.height==statusBarFrame.size.height){
            windowBounds.origin.x+=statusBarFrame.size.width;
            windowBounds.size.width-=statusBarFrame.size.width;
        }
        else if (windowBounds.size.width==statusBarFrame.size.width) {
            windowBounds.origin.y+=statusBarFrame.size.height;
            windowBounds.size.height-=statusBarFrame.size.height;
        }
        CGRect appFrame = [superview convertRect:windowBounds fromView:nil];
        //CGRect appFrame = [superview convertRect:[[UIScreen mainScreen] applicationFrame] fromView:nil];

        appFrame = CGRectMake(appFrame.origin.x + [tx floatValue], appFrame.origin.y + [ty floatValue], appFrame.size.width, appFrame.size.height);
        
        if (self.modalPresentationStyle == UIModalPresentationPageSheet ||
                   self.modalPresentationStyle == UIModalPresentationFormSheet
#ifdef __IPHONE_7_0
            || (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7") && self.modalPresentationStyle == UIModalPresentationCustom)
#endif
            ){
            CGFloat oldTx = [tx floatValue];
            CGFloat oldTy = [ty floatValue];
            CGFloat oldTh = [th floatValue];
            
            CGFloat yDelta = 0.0f;
            CGFloat xDelta = 0.0f;
            CGFloat hDelta = 0.0f;
            
            if (self.modalPresentationStyle == UIModalPresentationFormSheet) {
                BOOL dontUseAnimation = TRUE;
                if (keyboardEndFrame.size.width<3){
                    if (lastKeyboardFrame.size.width>3 || self->isBeingShowed){
                        if (!self->isBeingShowed){
                            yDelta = (appFrame.size.height - superview.frame.size.width)/2.0;
                        }
                        else{
                            superview.frame = CGRectRound(superview.frame);
                        }
                    }
                    else{
                        CAAnimation* animationPosition = [superview.layer animationForKey:@"position"];
                        if ([animationPosition isKindOfClass:[CABasicAnimation class]]){
                            CABasicAnimation* animationPositionValue = (CABasicAnimation*)animationPosition;
                            CGPoint fromValue = [(NSValue*)[animationPositionValue fromValue] CGPointValue];
                            superview.layer.position = fromValue;
                        }
                        [superview.layer removeAllAnimations];
                    }
                }
                else{
                    CGFloat yMod = (appFrame.size.height - superview.bounds.size.height)/2.0;
                    
                    if ((keyboardRect2.origin.y - appFrame.origin.y) - yMod < superview.bounds.size.height && (keyboardRect2.origin.y - appFrame.origin.y) > superview.bounds.size.height){
                        yDelta = keyboardRect2.origin.y - superview.bounds.size.height;
                    }
                }
                
                CGFloat newX = xDelta - oldTx;
                CGFloat newY = yDelta - oldTy;
                
                if (fabs(newX) > 1 || fabs(newY) > 1){
                    CGPoint newCenter = CGPointApplyAffineTransform(CGPointMake(newX,newY), superview.transform);
                    [tx release];
                    [ty release];
                    tx = ty = nil;
                    if (newX || oldTx)
                        tx = [@(oldTx + newX)retain];
                    if (newY || oldTy)
                        ty = [@(oldTy + newY)retain];
                    void (^animations)(void) = ^{
                        superview.center = CGPointMake(superview.center.x + newCenter.x,superview.center.y + newCenter.y);
                        superview.frame = CGRectRound(superview.frame);
                    };
                    if (!dontUseAnimation && self->isAppeared){
                        [UIView animateWithDuration:duration delay:0
                                            options: UIViewAnimationOptionAllowUserInteraction | UIViewAnimationOptionBeginFromCurrentState animations:animations completion:nil];
                    }
                    else{
                        animations();
                    }
                }
            }
            else {
                if (keyboardRect2.origin.y < superview.bounds.size.height && appFrame.origin.y<0){
                    yDelta = keyboardRect2.origin.y - superview.bounds.size.height;
                    if (self.resizeToFitKeyboard){
                        hDelta = oldTh;
                    }
                    if (yDelta<appFrame.origin.y){
                        if (self.resizeToFitKeyboard){
                            hDelta = appFrame.origin.y - yDelta;
                            yDelta = appFrame.origin.y - hDelta/2.0;
                        }
                        else{
                            yDelta = appFrame.origin.y;
                        }
                    }
                }
                /*
                if (keyboardRect2.origin.x>0 && appFrame.origin.x<0){
                    xDelta = -keyboardRect2.origin.x;
                    if (xDelta<appFrame.origin.x)
                        xDelta = appFrame.origin.x;
                }*/
                CGFloat newX = xDelta - oldTx;
                CGFloat newY = yDelta - oldTy;
                CGFloat newH = hDelta - oldTh;
                
                if (fabs(newX) > 1 || fabs(newY) > 1 || fabs(newH) > 1){
                    CGPoint newCenter = CGPointApplyAffineTransform(CGPointMake(newX,newY), superview.transform);
                    [tx release];
                    [ty release];
                    [th release];
                    tx = ty = nil;
                    if (newX || oldTx)
                        tx = [@(oldTx + newX)retain];
                    if (newY || oldTy)
                        ty = [@(oldTy + newY)retain];
                    if (newH || oldTh)
                        th = [@(oldTh + newH)retain];
                    
                    void (^animations)(void) = ^{
                        superview.center = CGPointMake(superview.center.x + newCenter.x,superview.center.y + newCenter.y);
                        if (newH){
                            CGRect bounds = superview.bounds;
                            bounds.size.height-=newH;
                            superview.bounds = bounds;
                        }
                        superview.frame = CGRectRound(superview.frame);
                    };  
                    if (self->isAppeared){
                        UIViewAnimationOptions animOptions = UIViewAnimationOptionAllowUserInteraction;
                        //fixes ugly behaviour with hw keyboard and becomeFirstResponder om viewDidAppear
                        if (superview.layer.animationKeys.count){
                            animOptions|=UIViewAnimationOptionBeginFromCurrentState;
                        }
                        [UIView animateWithDuration:duration delay:0
                                            options: animOptions animations:animations completion:nil];
                        }
                    else{
                        animations();
                    }
                }
            }
            lastKeyboardFrame = keyboardEndFrame;
        }
    }
}

- (void)keyboardDidShow:(NSNotification *)n{
    
    if (self.skeepKeyboardAnimation) return;

    if (![self.view getFirstResponder]) return;
    keyboardIsShown = YES;
    NSDictionary* userInfo = [n userInfo];
    // get the size of the keyboard
    [userInfo[UIKeyboardFrameEndUserInfoKey] getValue:&lastKeyboardFrame];
}

- (BOOL)disablesAutomaticKeyboardDismissal {
    return NO;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Overriden to allow any orientation.
    return (interfaceOrientation == UIInterfaceOrientationLandscapeLeft || interfaceOrientation == UIInterfaceOrientationLandscapeRight);
}

- (BOOL)shouldAutorotate
{
    return TRUE;
}

-(UIInterfaceOrientationMask)supportedInterfaceOrientations {
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone) {
        return UIInterfaceOrientationMaskPortrait | UIInterfaceOrientationMaskPortraitUpsideDown;
    }
    return UIInterfaceOrientationMaskAll;//UIInterfaceOrientationMaskLandscapeLeft | UIInterfaceOrientationMaskLandscapeRight;
}

-(BOOL)shouldAutomaticallyForwardAppearanceMethods{
    return TRUE;
}

- (void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id <UIViewControllerTransitionCoordinator>)coordinator{
    [super viewWillTransitionToSize:size withTransitionCoordinator:coordinator];
}

-(UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;
}

@end


