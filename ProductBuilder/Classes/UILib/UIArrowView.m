//
//  UIArrowView.m
//  ProductBuilder
//
//  Created by Valery Fomenko on 4/4/12.
//  Copyright (c) 2012 Cloudworks. All rights reserved.
//

#import "UIArrowView.h"

@implementation UIArrowView
@synthesize type, arrowColor;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
    }
    return self;
}

- (id)init
{
    self = [super init];
    if (self) {
        arrowColor = [[UIColor grayColor]retain];
    }
    return self;
}

-(void)dealloc{
    [arrowColor release];
    [super dealloc];
}


- (void)drawRect:(CGRect)rect
{
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGFloat minx = CGRectGetMinX(self.bounds);
    CGFloat midx = CGRectGetMidX(self.bounds);
    CGFloat maxx = CGRectGetMaxX(self.bounds);
    CGFloat miny = CGRectGetMinY(self.bounds);
    CGFloat midy = CGRectGetMidY(self.bounds);
    CGFloat maxy = CGRectGetMaxY(self.bounds);
    
    switch(type){
        case UIArrowViewTypeLeft:
            CGContextMoveToPoint(context, maxx, miny);//right top
            CGContextAddLineToPoint(context, minx, midy); //left middle
            CGContextAddLineToPoint(context, maxx,  maxy);//right bottom
            break;
        case UIArrowViewTypeRight:
            CGContextMoveToPoint(context, minx, miny);//left top
            CGContextAddLineToPoint(context, maxx, midy);//left middle
            CGContextAddLineToPoint(context, minx,  maxy);//left bottom
            break;
        case UIArrowViewTypeTop:
            CGContextMoveToPoint(context, minx, maxy);//left bottom
            CGContextAddLineToPoint(context, midx, miny);//middle top
            CGContextAddLineToPoint(context, maxx,  maxy);//right bottom
            break;
        case UIArrowViewTypeBottom:
            CGContextMoveToPoint(context, minx, miny);//left top
            CGContextAddLineToPoint(context, midx, maxy);//middle top
            CGContextAddLineToPoint(context, maxx,  miny);//right top
            break;
        default:
            break;
    }
    CGContextSetFillColorWithColor(context, arrowColor.CGColor);
    CGContextFillPath(context);
    //CGContextClosePath(context);
}


@end
