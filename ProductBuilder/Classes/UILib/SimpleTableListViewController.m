//
//  SimpleTableListViewController.m
//  iPadPOS
//
//  Created by valera on 3/7/11.
//  Copyright 2011 DBBestTechnologies. All rights reserved.
//

#import "SimpleTableListViewController.h"
#import "MOGlassButton.h"
#import "UITitleBarButtonItem.h"
#import "RTPopoverController.h"
#import "Global.h"
#import "BaseSearchTableViewCell.h"
#import "UIView+FirstResponder.h"
#import "UIView+FirstResponder.h"

typedef struct{
    BOOL didSelectIndex;
    BOOL didSelectValue;
    BOOL willSelectIndex;
    BOOL didDismissPopover;
    BOOL willDisplayValue;
    BOOL viewWillAppear;
} SimpleTableListViewControllerDelegateImplementedMethods;

@interface SimpleTableListViewController() <UITableViewDataSource, UITableViewDelegate> {
    SimpleTableListViewControllerDelegateImplementedMethods delegateMethods;
    UIResponder *_formerFirstResponder;
    BOOL _isPresented;
    UIToolbar * toolbar;
}
@end

@implementation SimpleTableListViewController

@synthesize items, titleText, selectedIndex, scroledIndex, delegate, displayMember, columns, gridTableView = _tableView, additionalObject,cancelButton = _bCancel;
#pragma mark -
#pragma mark Initialization

-(id)init {
    self = [super init];
    if (self != nil) {
        
        selectedIndex = NSNotFound;
        scroledIndex = NSNotFound;
        _titleVisible = true;
    }
    return self;
}

-(void)setDelegate:(NSObject<SimpleTableListViewControllerDelegate> *)value{
    delegate = value;
    delegateMethods.didSelectIndex = [delegate respondsToSelector:@selector(simpleTableListViewController:didSelectIndex:)];
    delegateMethods.didSelectValue = [delegate respondsToSelector:@selector(simpleTableListViewController:didSelectValue:)];
    delegateMethods.willSelectIndex = [delegate respondsToSelector:@selector(simpleTableListViewController:willSelectIndex:)];
    delegateMethods.didDismissPopover = [delegate respondsToSelector:@selector(simpleTableListViewController:didDismissPopover:)];
    delegateMethods.willDisplayValue = [delegate respondsToSelector:@selector(simpleTableListViewController:willDisplayValue:inContent:)];
    delegateMethods.viewWillAppear = [delegate respondsToSelector:@selector(simpleTableListViewController:viewWillAppear:)];
}

#pragma mark -
#pragma mark View lifecycle

-(void)cancel:(id)sender{
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        [self dismissViewControllerAnimated:YES completion:^{
            if (_formerFirstResponder) {
                [_formerFirstResponder becomeFirstResponder];
                [_formerFirstResponder release];
                _formerFirstResponder = nil;
            }
        }];
    }
    else {
        [self dismissPopover];
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];

    self.view.backgroundColor = [UIColor clearColor];//MO_RGBCOLOR(249, 249, 249);
    
    toolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, 44)];
    toolbar.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    toolbar.barStyle = UIBarStyleBlack;
    toolbar.translucent = YES;
    toolbar.backgroundColor = [UIColor clearColor];
    toolbar.barTintColor = MO_RGBCOLOR(249, 249, 249);
    
    _bCancel = [[MOGlassButtonTransparent alloc] init];
    [_bCancel setTitle:NSLocalizedString(@"CANCELBTN_TITLE", nil) forState:UIControlStateNormal];
    [_bCancel addTarget:self action:@selector(cancel:) forControlEvents:UIControlEventTouchUpInside];
    _bCancel.frame = CGRectMake(0,0, 64, 33);
    _bCancel.titleLabel.font = [UIFont systemFontOfSize:18];
    UIBarButtonItem * cancelBarButtonItem = [[[UIBarButtonItem alloc] initWithCustomView:_bCancel] autorelease];
    
    UIBarButtonItem * leftSpaceBarButtonItem = [[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil] autorelease];
    UITitleBarButtonItem * titleBarButtonItem = [[[UITitleBarButtonItem alloc] initWithTitle:titleText style:UIBarButtonItemStylePlain target:nil  action:nil] autorelease];
    titleBarButtonItem.tintColor=[UIColor blackColor];
    UIBarButtonItem * rightSpaceBarButtonItem = [[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil] autorelease];
    
    toolbar.items = @[cancelBarButtonItem, leftSpaceBarButtonItem, titleBarButtonItem, rightSpaceBarButtonItem];
    
    [self.view addSubview:toolbar];
    toolbar.hidden = !_titleVisible;
    CGFloat titleBarHeight = _titleVisible ? toolbar.bounds.size.height : -2;
    CGRect tableRect = CGRectMake(0, titleBarHeight - 1, self.view.bounds.size.width, self.view.bounds.size.height - titleBarHeight);
    
    [titleBarButtonItem disable];
    

    if ([displayMember length] <= 0)
        displayMember = @"description";
    
    _plainTableView = [[RTUITableView alloc] initWithFrame:tableRect style:UITableViewStyleGrouped];
    _plainTableView.backgroundColor = [UIColor whiteColor];
    _plainTableView.tintColor = global_blue_color;
    _plainTableView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    _plainTableView.rowHeight = 44;
    _plainTableView.delegate = self;
    _plainTableView.dataSource = self;
    [_plainTableView registerClass:[BaseSearchTableViewCell class] forCellReuseIdentifier:@"SimpleCell"];
    _plainTableView.tableFooterView = [[UIView new] autorelease];
    [self.view addSubview:_plainTableView];
}

-(void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    //in iOs7 by default it set to 11
    //self.view.superview.layer.cornerRadius = 5.0f;
    
    if (self.delegate && delegateMethods.viewWillAppear)
        [self.delegate simpleTableListViewController:self viewWillAppear:animated];
    
    if (self.preferredContentSize.height >= (44 + _plainTableView.rowHeight*[self tableView:_plainTableView numberOfRowsInSection:0])) {
        
        _plainTableView.scrollEnabled = false;
    }
}



-(void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    
    self.scroledIndex = scroledIndex;
    if (IS_IPHONE_6_OR_MORE && selectedIndex >=0 && selectedIndex < [items count]) {
        [_plainTableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:selectedIndex inSection:0] atScrollPosition:UITableViewScrollPositionMiddle animated:YES];
    }
    
    if (_plainTableView) {
        
        if ([_plainTableView numberOfRowsInSection:0]*_plainTableView.rowHeight + 2 <= _plainTableView.frame.size.height) {
            
            _plainTableView.scrollEnabled = false;
        }
    }
}
    
-(BOOL)titleVisible {
    return _titleVisible;
}
    
-(void)setTitleVisible:(BOOL)titleVisible {
    _titleVisible = titleVisible;
    if (self.isViewLoaded) {
        toolbar.hidden = !_titleVisible;
        CGFloat titleBarHeight = _titleVisible ? toolbar.bounds.size.height : -2;
        _plainTableView.frame = CGRectMake(0, titleBarHeight, self.view.bounds.size.width, self.view.bounds.size.height - titleBarHeight);
    }
}


- (BOOL)gridTable:(GridTable*)gridTable getBoolForRow:(NSInteger)row column:(GridTableColumnDescription *)column
{
    NSString* value = [items[row] valueForKey:column.name];
    return [value intValue];
}

- (void)gridTable:(GridTable*)gridTable changeBoolForRow:(NSInteger)row column:(GridTableColumnDescription *)column cell:(GridTableCell *)cell
{
    NSString* currentValue = [items[row] valueForKey:column.name];
    BOOL boolValue = [currentValue intValue];
    NSNumber *intValue = @(!boolValue);
    [items[row] setValue:intValue forKey:column.name];
}

#pragma mark -
#pragma mark GridTableDelegate

- (void)gridTable:(GridTable*)gridTable setContentForRow:(NSInteger)row column:(GridTableColumnDescription *)column content:(id)content {
    id value = items[row];
    [_tableView setContentForRowDefault:row columnView:content column:column value:value];
    if (delegateMethods.willDisplayValue){
        [self.delegate simpleTableListViewController:self willDisplayValue:value inContent:content];
    }
}

- (NSInteger)gridTableNumberOfRows:(GridTable*)gridTable {
	return [items count];
}

- (void)gridTable:(GridTable*)gridTable sortByColumn:(GridTableColumnDescription *)column order:(SortingMode)order {
}

-(void)fireDidSelectedRow:(NSInteger)row{
    if (self.delegate){
		if(delegateMethods.didSelectIndex)
			[self.delegate performSelector:@selector(simpleTableListViewController:didSelectIndex:) withObject:self withObject:@(selectedIndex)];
		if(delegateMethods.didSelectValue){
			id value = row != NSNotFound ? items[row] : nil;
			[self.delegate performSelector:@selector(simpleTableListViewController:didSelectValue:) withObject:self withObject:value];
		}
	}
}

- (void)gridTable:(GridTable*)gridTable didSelectRow:(NSInteger)row {
    selectedIndex = row;
    if (ignoreRowSelection > 0 || gridTable.endingLayoutSubviews) return;
    void(^completion)(void) = ^{
        [self fireDidSelectedRow:selectedIndex];
        [delegate release];
    };
    [delegate retain];
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        [self close:completion];
    }
    else {
        [self dismissPopover:completion];
    }
}

- (void)gridTable:(GridTable*)gridTable didDeselectRow:(NSInteger)row {
    
}

- (BOOL)gridTable:(GridTable*)gridTable willSelectRow:(NSInteger)row {
    if(delegateMethods.willSelectIndex) {
        return [self.delegate simpleTableListViewController:self willSelectIndex:row];
    }
    return YES;
}

- (BOOL)gridTable:(GridTable*)gridTable formatCellForRow:(NSInteger)row column:(GridTableColumnDescription *)column content:(id)content selected:(BOOL)selected initial:(BOOL)initital {
    
    return YES;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    
    return 1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    
    return 1;
}


#pragma mark - Memory management
- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Relinquish ownership any cached data, images, etc that aren't in use.
}

- (void)dealloc {
	self.view = nil;
	self.items = nil;
	[_bCancel release];
	[_titleLabel release];
	[_tableView release];
	[_titleView release];
	[popoverPresentationController release];
    [displayMember release];
    [columns release];
    [titleText release];
    [additionalObject release];
    [_plainTableView release];
    [_formerFirstResponder release];
    _formerFirstResponder = nil;
    [toolbar release];
	[super dealloc];
}

-(void)setSelectedIndex:(NSInteger)value{
    //force load view
    //[self view];
	selectedIndex = value;
    if (![items count] || selectedIndex < 0 || selectedIndex == NSNotFound){
        [_tableView removeSelection:FALSE];
        [_plainTableView deselectRowAtIndexPath:_plainTableView.indexPathForSelectedRow animated:NO];
        [_plainTableView reloadData];
        return;
    }
    [_tableView selectRow:selectedIndex animated:FALSE];
    [_plainTableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:selectedIndex inSection:0] animated:NO scrollPosition:UITableViewScrollPositionMiddle];
    [_plainTableView reloadData];
    if (!_tableView && ignoreRowSelection <= 0){
        [self fireDidSelectedRow:selectedIndex];
    }
}

-(void)setSelectedIndex:(NSInteger)value ignoreSelection:(BOOL)ignore {
    if (ignore) {
        ignoreRowSelection++;
        [self setSelectedIndex:value];
        ignoreRowSelection--;
    }
    else
        [self setSelectedIndex:value];
}


-(void)setScroledIndex:(NSInteger)value{

	scroledIndex = value;
    if ([items count] == 0 || scroledIndex ==NSNotFound || scroledIndex < 0 || !_tableView) return;
    
    [_tableView.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForItem:value inSection:0]
                                atScrollPosition:UITableViewScrollPositionTop
                                        animated:NO];
}


-(id)selectedObject{
	return ![items count] || selectedIndex == NSNotFound || selectedIndex < 0 || selectedIndex >= [items count] ? nil : items[selectedIndex];
}

-(void)setSelectedObject:(id)anObject{
	self.selectedIndex = [items indexOfObject:anObject];
}

-(void)setSelectedObject:(id)anObject ignoreSelection:(BOOL)ignore {
	[self setSelectedIndex:[items indexOfObject:anObject] ignoreSelection:ignore];
}

-(void)setTitleText:(NSString *)text{
	_titleLabel.text = titleText = [text retain];
}

#pragma mark -
#pragma mark showpopover

- (void)presentPopoverFromRect:(CGRect)rect inView:(UIView *)view permittedArrowDirections:(UIPopoverArrowDirection)arrowDirections animated:(BOOL)animated{
    if (_isPresented) {
        return;
    }
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
//        [self presentInController:self];
    }
    else {
        //force viewDidLoad
        if (self.view){
            ignoreRowSelection++;
            
            self.modalPresentationStyle = UIModalPresentationPopover;
            
            [popoverPresentationController release];
            popoverPresentationController = [[self popoverPresentationController] retain];
            popoverPresentationController.permittedArrowDirections = arrowDirections;
            popoverPresentationController.popoverLayoutMargins = UIEdgeInsetsMake(0, 0, 0, 0);
            popoverPresentationController.backgroundColor = MO_RGBCOLOR(249, 249, 249);
            popoverPresentationController.delegate = self;
            popoverPresentationController.sourceView = view;
            popoverPresentationController.sourceRect = rect;
            UIViewController* parentViewController = [view firstAvailableUIViewController];
            _isPresented = YES;
            [parentViewController presentViewController:self animated:animated completion:^{
                [popoverPresentationController release];
                popoverPresentationController = nil;
            }];
            
            ignoreRowSelection--;
        }
    }
}

- (void)dismissPopover{
    [self dismissPopover:nil];
}

- (void)dismissPopover:(void (^)(void))completion {
    [self dismissViewControllerAnimated:YES completion:nil];
    [self popoverPresentationControllerDidDismissPopover:popoverPresentationController];
    if (completion){
//        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.31 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            completion();
//        });
    }
}

- (void)presentInController:(UIViewController *)controller {
    ignoreRowSelection++;
    //force viewDidLoad
    if (self.view) {
        UIResponder* firstResponder = [[UIApplication sharedApplication].windows[0] getFirstResponder];
        [_formerFirstResponder release];
        _formerFirstResponder = [firstResponder retain];
        [firstResponder resignFirstResponder];
        
        [controller presentViewController:self animated:YES completion:^{
            ignoreRowSelection--;
        }];
    }
}

#ifdef __IPHONE_7_0
-(void)setContentSizeForViewInPopover:(CGSize)contentSizeForViewInPopover{
    self.preferredContentSize = contentSizeForViewInPopover;
}

-(CGSize)contentSizeForViewInPopover{
    return self.preferredContentSize;
}
#endif

#pragma mark -
#pragma mark RTPopoverControllerDelegate

- (BOOL)popoverPresentationControllerShouldDismissPopover:(UIPopoverPresentationController *)popoverPresentationController{
	return TRUE;
}

- (void)popoverPresentationControllerDidDismissPopover:(UIPopoverPresentationController *)aPopoverPresentationController{
    if (delegateMethods.didDismissPopover && aPopoverPresentationController)
        [self.delegate performSelector:@selector(simpleTableListViewController:didDismissPopover:) withObject:self];
    if (aPopoverPresentationController == popoverPresentationController){
        [popoverPresentationController release];
        popoverPresentationController = nil;
    }
    _isPresented = NO;
	//NSInteger idx = departmentViewController.selectedIndex;
}

#pragma mark - Properties

-(void)setItems:(id)value {
    if (items != value) {
        [items release];
        items = [value retain];
        [_tableView reloadData];
        [_plainTableView reloadData];
    }
}

-(BOOL)isPopoverVisible {
    return popoverPresentationController.presentingViewController!=nil;
}

#pragma mark - Private Methods

- (void)close:(void (^)(void))completion {
    if (self.presentingViewController != nil && [self.presentingViewController respondsToSelector:@selector(childViewDidDisappear)]) {
        [self.presentingViewController performSelector:@selector(childViewDidDisappear)];
    }
    [self dismissViewControllerAnimated:YES completion:completion];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [items count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SimpleCell" forIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    id value = items[indexPath.row];
    id columnValue = [value valueForKeyPath:displayMember];
    cell.textLabel.text = [columnValue description];
    
    if (selectedIndex == indexPath.row) {
        [cell setAccessoryType:UITableViewCellAccessoryCheckmark];
        cell.textLabel.textColor = global_blue_color;
    }
    else {
        [cell setAccessoryType:UITableViewCellAccessoryNone];
        cell.textLabel.textColor = [UIColor blackColor];
    }
    
    if (delegateMethods.willDisplayValue) {
        [delegate simpleTableListViewController:self willDisplayValue:value inContent:cell.textLabel];
        
        cell.userInteractionEnabled = cell.textLabel.enabled;
    }
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    selectedIndex = indexPath.row;
    [tableView reloadData];
    
    if (ignoreRowSelection > 0) return;
    void(^completion)(void) = ^{
        [self fireDidSelectedRow:selectedIndex];
        [delegate release];
    };
    [delegate retain];
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        [self close:completion];
    }
    else {
        [self dismissPopover:completion];
    }
}

@end
