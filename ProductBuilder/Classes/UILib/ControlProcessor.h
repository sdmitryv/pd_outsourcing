//
//  ControlProcessor.h
//  iPadPOS
//
//  Created by valera on 3/1/11.
//  Copyright 2011 DBBestTechnologies. All rights reserved.
//

#import <Foundation/Foundation.h>

#define GREENBUTTON 1
#define GRAYBUTTON 2

@interface ControlProcessor : NSObject {

}

+(void)processView:(UIView*)view;
@end
