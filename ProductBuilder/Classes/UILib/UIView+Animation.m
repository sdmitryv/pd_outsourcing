//
//  UIView+UIView_Animation.m
//  ProductBuilder
//
//  Created by valery on 5/7/14.
//  Copyright (c) 2014 Cloudworks. All rights reserved.
//

#import "UIView+Animation.h"
#import <QuartzCore/QuartzCore.h>

@implementation UIView(Animation)

-(BOOL)animateFading:(BOOL)fadeOut{
    CGFloat startOpacityValue = fadeOut ? 1.0 : 0.0;
    CGFloat endOpacityValue = fadeOut ? 0.0 : 1.0;
    if (self.layer.opacity == endOpacityValue) return FALSE;
    self.layer.opacity = endOpacityValue;
    CABasicAnimation *fadeAnimation = [CABasicAnimation animationWithKeyPath:@"opacity"];
    [fadeAnimation setFromValue:@(startOpacityValue)];
    [fadeAnimation setToValue:@(endOpacityValue)];
    fadeAnimation.duration = 0.7f;
    fadeAnimation.timingFunction = UIViewAnimationCurveEaseInOut;
    [self.layer addAnimation:fadeAnimation forKey:@"opacity"];
    
    CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"transform"];
    CATransform3D startOpacityTransform = CATransform3DMakeScale(1.0, 1.0, 1.0);
    CATransform3D endOpacityTransform = CATransform3DMakeScale(1.26, 1.26, 1.0);
    [animation setFromValue:[NSValue valueWithCATransform3D:fadeOut ? startOpacityTransform : endOpacityTransform]];
    [animation setToValue:[NSValue valueWithCATransform3D:fadeOut ? endOpacityTransform : startOpacityTransform]];
    [animation setDuration:0.7];
    [self.layer addAnimation:animation forKey:@"transform"];
    return TRUE;
}

@end
