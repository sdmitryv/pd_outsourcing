//
//  RTTextView.h
//  ProductBuilder
//
//  Created by Alexander Martyshko on 8/5/13.
//  Copyright (c) 2013 Cloudworks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RTTextView : UITextView <UITextViewDelegate> {
    id<UITextViewDelegate> _internalDelegate;
}

@end
