//
//  UISearchField+Extention.swift
//  ProductBuilder
//
//  Created by valery on 1/26/17.
//  Copyright © 2017 Cloudworks. All rights reserved.
//

import Foundation
import UIKit

extension UISearchBar{
    
    func setShowsCancelButtonAnimated(_ showsCancelButton:Bool, completion: (() -> Swift.Void)? = nil){
        if self.showsCancelButton != showsCancelButton{
            CATransaction.begin()
            if completion != nil{
                CATransaction.setCompletionBlock(completion)
            }
            self.setShowsCancelButton(showsCancelButton, animated: showsCancelButton)
            if let textField = self.value(forKey: "textField") as? UIView{
                for view in textField.subviews{
                    if type(of: view).description() == "UIFieldEditor"{
                        for subview in view.subviews{
                            subview.layer.removeAllAnimations()
                        }
                    }
                }
            }
            CATransaction.commit()
        }
    }
}
