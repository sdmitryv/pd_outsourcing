//
//  RTAlertViewProxy.swift
//  ProductBuilder
//
//  Created by valery on 1/5/17.
//  Copyright © 2017 Cloudworks. All rights reserved.
//

import Foundation

//@objc protocol RTAlertViewProxyDelegate: NSObjectProtocol {

//@objc protocol RTAlertViewDelegate: NSObjectProtocol {
//    // Called when a button is clicked. The view will be automatically dismissed after this call returns
//    @objc optional func alertView(_ alertView: RTAlertView, clickedButtonat buttonIndex: Int)
//    // Called when we cancel a view (eg. the user clicks the Home button). This is not called when the user clicks the cancel button.
//    // If not defined in the delegate, we simulate a click in the cancel button
//    
//    @objc optional func alertViewCancel(_ alertView: RTAlertView)
//    
//    @objc optional func willPresentAlertView(_ alertView: RTAlertView)
//    // before animation and showing view
//    
//    @objc optional func didPresentAlertView(_ alertView: RTAlertView)
//    // after animation
//    
//    @objc optional func alertView(_ alertView: RTAlertView, willDismissWithButtonIndex buttonIndex: Int)
//    // before animation and hiding view
//    
//    @objc optional func alertView(_ alertView: RTAlertView, didDismissWithButtonIndex buttonIndex: Int)
//    // after animation
//    // Called after edits in any of the default fields added by the style
//    
//    @objc optional func alertViewShouldEnableFirstOtherButton(_ alertView: RTAlertView) -> Bool
//}

public enum RTAlertActionStyle : Int {
    case `default`
    case cancel
    case destructive
    init(ui_style:UIAlertActionStyle){
        switch(ui_style){
            case .default: self = .default
            case .cancel: self = .cancel
            case .destructive : self = .destructive
        }
    }
}

extension UIAlertActionStyle{
    init(rt_style:RTAlertActionStyle){
        switch(rt_style){
            case .default: self = .default
            case .cancel: self = .cancel
            case .destructive : self = .destructive
        }
    }
}

class RTAlertAction{
    
    public init(title: String?, style: RTAlertActionStyle, handler aHandler: ((RTAlertAction) -> Void)? = nil){
        handler = aHandler
        alertAction = RTAlertActionWrapper(title:title, style:UIAlertActionStyle(rt_style:style)){[weak self] action in
            if self?.handler != nil{
                self!.handler!(self!)
            }
        }
        alertAction.rt_alertAction = self
    }
    
    fileprivate weak var alertAction : RTAlertActionWrapper!
    var handler: ((_ action: RTAlertAction) -> Void)?

    open var title: String? {
        get{
            return alertAction.title;
        }
    }
    open var style: RTAlertActionStyle {
        get{
            return RTAlertActionStyle(ui_style:alertAction.style);
        }
    }
    open var isEnabled: Bool{
        get{
            return alertAction.isEnabled;
        }
    }

}

enum RTAlertControllerStyle:Int{
    case actionSheet
    case alert
    init(ui_style:UIAlertControllerStyle){
        switch(ui_style){
        case .actionSheet: self = .actionSheet
        case .alert: self = .alert
        //default: break
        }
    }
}

extension UIAlertControllerStyle{
    init(rt_style:RTAlertControllerStyle){
        switch(rt_style){
        case .actionSheet: self = .actionSheet
        case .alert: self = .alert
        //default: break
        }
    }
}

fileprivate class RTAlertActionWrapper : UIAlertAction{
    var rt_alertAction : RTAlertAction!

}

class RTAlertController {
    
    
    init(title: String?, message: String?, preferredStyle: RTAlertControllerStyle = .alert){
        //super.init(nibName:nil, bundle:nil)
        alertController = UIAlertController(title:title, message:message, preferredStyle:UIAlertControllerStyle(rt_style:preferredStyle))
    }
    
    required init?(coder aDecoder: NSCoder) {
        //super.init(coder:aDecoder)
        alertController = UIAlertController(coder:aDecoder)
    }
    
    private(set) var alertController:UIAlertController?
    
    open func addAction(_ action: RTAlertAction){
        alertController?.addAction(action.alertAction)
    }
    
    open func addAction(title: String?, style: RTAlertActionStyle, handler aHandler: ((RTAlertAction) -> Void)? = nil){
        alertController?.addAction(RTAlertAction(title:title, style:style, handler:aHandler).alertAction)
    }
    
    open var actions: [RTAlertAction] {
        get{
            return alertController == nil ? [RTAlertAction]() : alertController!.actions.map { action -> RTAlertAction in
                guard let alertActionWrapper = action as? RTAlertActionWrapper else{
                    return RTAlertAction(title:nil, style:.default)
                }
                return alertActionWrapper.rt_alertAction
            }
        }
    }
    
    open var preferredAction: RTAlertAction?{
        get{
            if let action = alertController?.preferredAction as? RTAlertActionWrapper{
                return action.rt_alertAction
            }
            return nil
        }
        set{
            alertController?.preferredAction = newValue?.alertAction
        }
    }
    
    
    open func addTextField(configurationHandler: ((UITextField) -> Swift.Void)? = nil){
        alertController?.addTextField(configurationHandler:configurationHandler)
    }
    
    open var textFields: [UITextField]? { get {
            return alertController?.textFields
        }
    }
    
    
    var title: String?{
        get{
            return alertController?.title
        }
        set{
            alertController?.title = newValue
        }
    }
    
    open var message: String?{
        get{
            return alertController?.message
        }
        set{
            alertController?.message = newValue
        }
    }
    
    open var preferredStyle: RTAlertControllerStyle {
        get{
            return alertController == nil ? .alert : RTAlertControllerStyle(ui_style:alertController!.preferredStyle)
        }
    }
    
    func show() {
        if alertController == nil {return}
        UIViewController.topViewController()?.present(alertController!, animated: true)
    }
    
    private var waitSemaphore:DispatchSemaphore? = nil
    
    func showOnMainThreadAndWait() {
        if waitSemaphore != nil {
            waitSemaphore = nil
        }
        if !Thread.isMainThread {
            waitSemaphore = DispatchSemaphore(value: 0)
            var waitSemaphoreHandled:Bool = false
            if let alertActions = alertController?.actions{
                for action in alertActions{
                    if let actionWrapper = action as? RTAlertActionWrapper{
                        waitSemaphoreHandled = true
                        let oldHandler = actionWrapper.rt_alertAction.handler
                        actionWrapper.rt_alertAction.handler = {[weak self] theAction in
                            if oldHandler != nil{
                                oldHandler!(theAction)
                            }
                            self?.waitSemaphore?.signal()
                        }
                    }
                }
            }
            DispatchQueue.main.async{
                self.show()
            }
            if waitSemaphoreHandled{
                waitSemaphore?.wait()
                waitSemaphore = nil
            }
        }
        else {
            self.show()
        }
    }
}

@objc(RTAlertView50) class RTAlertView50 : NSObject{
    var title:String?{
        get{
            return alertController.title
        }
        set{
            alertController.title = newValue
        }
    }
    var message:String? {
        get{
            return alertController.message
        }
        set{
            alertController.message = newValue
        }
    }
    weak var alertView:RTAlertView?
    
    var tag:Int = 0
    var cancelButtonIndex = 0
    private(set) var firstOtherButtonIndex = 0
    private(set) var numberOfButtons = 0
    var numberOfRows = 0
    private(set) var isVisible = false
    var alertViewStyle = UIAlertViewStyle(rawValue: 0)
    private(set) var contentView: UIView!
    private(set) var buttonTitles = [Any]()
    //weak var delegate: RTAlertViewProxyDelegate?
    weak var delegate: NSObject? //RTAlertViewDelegate?
    private var alertController = UIAlertController(title:nil, message:nil, preferredStyle:.alert)

    var completedBlock: ((_ buttonIndex: Int) -> Void)? = nil
    
   
    override init(){
        super.init()
    }
    
    init(title: String?, message: String?){
        super.init()
        alertController.title = title
        alertController.message = message
    }
    
    @objc(initWithTitle:message:delegate:cancelButtonTitle:otherButtonTitle:)
    convenience init(title: String?, message: String?, delegate: NSObject?, cancelButtonTitle: String?, otherButtonTitle: String?) {
        self.init(title:title, message:message, delegate:delegate, cancelButtonTitle:cancelButtonTitle, otherButtonTitles:otherButtonTitle)
    }
    @objc(initWithTitle:message:delegate:cancelButtonTitle:otherButtonTitles:)
    init(title: String?, message: String?, delegate: NSObject?, cancelButtonTitle: String?, otherButtonTitles: String?) {
        super.init()
        alertController.title = title
        alertController.message = message
        self.delegate = delegate
        if cancelButtonTitle != nil{
            addActionWithTitle(cancelButtonTitle!, actionStyle: RTAlertActionStyle.cancel)
        }
        if otherButtonTitles != nil{
           addActionWithTitle(otherButtonTitles!, actionStyle: RTAlertActionStyle.default)
        }
    }
    
    func addAction(action:RTAlertAction){
        alertController.addAction(action.alertAction)
    }
    
    private func addActionWithTitle(_ title:String, actionStyle:RTAlertActionStyle){
        let alertAction = RTAlertAction(title:title, style:actionStyle){
            action in
            //if let anAction = action as? RTAlertAction{
                if action.handler != nil{
                    action.handler!(action)
                }
            //}
        }
        alertAction.handler = { action in
            self.handleAlertAction(action)
        }
        alertController.addAction(alertAction.alertAction)
    }
    
    private func handleAlertAction(_ action:RTAlertAction!){
        guard let index = alertController.actions.index(of: action.alertAction) else{
            return
        }
        clickedButtonIndex = index
        if completedBlock != nil{
            completedBlock!(index)
            completedBlock = nil
        }
        else if delegate != nil && alertView != nil{
            (delegate as? RTAlertViewDelegate)?.alertView?(alertView, willDismissWithButtonIndex:index)
            (delegate as? RTAlertViewDelegate)?.alertView?(alertView, didDismissWithButtonIndex:index)
        }
        if waitSemaphore != nil {
            waitSemaphore?.signal()
        }
        self.clearButtonActions()
    }
    
    func clearButtonActions(){
        for alertAction in alertController.actions{
            if let anAction = alertAction as? RTAlertActionWrapper{
                anAction.rt_alertAction.handler = nil
                anAction.rt_alertAction = nil
            }
        }
    }

    func show() {
        if !Thread.isMainThread{
            DispatchQueue.main.async {
                self.show()
            }
            return
        }
        NotificationCenter.default.post(name: NSNotification.Name.RTAlertViewDidPresent, object: self)
        if delegate != nil && alertView != nil{
            (delegate as? RTAlertViewDelegate)?.willPresent?(alertView)
        }
        guard let topViewController = UIViewController.topViewController()else {
            return
        }
        topViewController.present(alertController, animated: true) {
            NotificationCenter.default.post(name: NSNotification.Name.RTAlertViewDidPresent, object: self)
            if self.delegate != nil && self.alertView != nil{
                (self.delegate as? RTAlertViewDelegate)?.didPresent?(self.alertView)
            }
        }

    }


/**
 *  Convenience method that sets the dismiss block while simultaneously showing the alert.
 */
func show(withDismissHandler dismissHandler: @escaping (_ buttonIndex: Int) -> Void) {
    self.completedBlock = dismissHandler
    show()
}

@discardableResult func addButton(withTitle title: String) -> Int {
    let action = RTAlertAction(title:title, style:.default){ action in
        self.handleAlertAction(action)
    }
    alertController.addAction(action.alertAction)
    return alertController.actions.index(of:action.alertAction) ?? 0
}

    @objc(buttonTitleAtIndex:)
    func buttonTitle(at index: Int) -> String? {
        if alertController.actions.count <= index{
           return nil
        }
        return alertController.actions[index].title
    }

    func dismiss(withClickedButtonIndex buttonIndex: Int, animated: Bool) {
        if alertController.actions.count < buttonIndex{
            if let action = alertController.actions[buttonIndex] as? RTAlertActionWrapper{
                handleAlertAction(action.rt_alertAction)
            }
        }
        dismissAlert()
    }
    
    @objc(textFieldAtIndex:)
    func textField(at textFieldIndex: Int) -> UITextField? {
        if (alertController.textFields==nil || textFieldIndex >= alertController.textFields!.count){
            return nil;
        }
        return alertController.textFields![textFieldIndex]
    }

    class func sdcAlertViewCornerRadius() -> CGFloat {
        return 0
    }
    @objc(disableDismissForIndex:)
    func disableDismiss(for index_: Int) {
        if alertController.actions.count < index_{
            let action = alertController.actions[index_]
            action.isEnabled = false
        }
    }
    
    func dismissAlert() {
        clearButtonActions()
        alertController.dismiss(animated: true, completion: nil)
    }
    
    func hide(after seconds: Float) {
    }
    
//    func messageLabel() -> UILabel {
//    }
    
    func showOnMainThread() {
        if !Thread.isMainThread {
            DispatchQueue.main.async{
                self.show()
            }
        }
        else {
            self.show()
        }
    }
    
    private var waitSemaphore:DispatchSemaphore? = nil
    private var clickedButtonIndex:Int = NSNotFound
    
    func showOnMainThreadAndWait() -> Int {
        clickedButtonIndex = NSNotFound
        if waitSemaphore != nil {
            waitSemaphore = nil
        }
        if !Thread.isMainThread {
            waitSemaphore = DispatchSemaphore(value: 0)
            DispatchQueue.main.async{
                self.show()
            }
            waitSemaphore?.wait()
            waitSemaphore = nil
        }
        else {
            self.show()
        }
        return clickedButtonIndex
    }
    
    var angle: CGFloat = 0.0
    var formerResponder: UIResponder?
    
    class func topMostAlertView() -> NSObject? {
        return UIViewController.topViewController() as? UIAlertController
    }
    
    class func alertViewCount() -> Int {
        return self.topMostAlertView() != nil ? 1 : 0
    }
    
    class func alertViewShown() -> Bool {
        return self.topMostAlertView() != nil
    }
    
    class func closeAlerts() {
        (self.topMostAlertView() as? UIAlertController)?.dismiss(animated: true)
    }
    
    var visible:Bool{
        get{
            return alertController.presentedViewController != nil
        }
    }
    
    func setTransform(_ transform:CGAffineTransform){
        alertController.view.transform = transform;
    }
    
}
