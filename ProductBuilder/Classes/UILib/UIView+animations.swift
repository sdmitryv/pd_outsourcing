//
//  UIView+animations.swift
//  AnimatedPaths
//
//  Created by valery on 2/6/17.
//
//

import Foundation
import UIKit


public extension UIView {
    public func pushTransition(duration:CFTimeInterval) {
        let animation:CATransition = CATransition()
        animation.timingFunction = CAMediaTimingFunction(name:kCAMediaTimingFunctionEaseInEaseOut)
        animation.type = kCATransitionPush
        animation.subtype = kCATransitionFromBottom
        animation.duration = duration
        self.layer.add(animation, forKey: kCATransitionPush)
    }
}
