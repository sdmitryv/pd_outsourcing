//
//  UISearchBarTransparent.h
//  StockCount
//
//  Created by Valera on 12/1/10.
//  Copyright 2010 DBBest Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface UISearchBarTransparent : UISearchBar{
	UITextField* textField;
}

- (UITextField*)textField;
-(void)removeSearchIcon;
- (void)insertText:(NSString*)text;
@end


@interface UISearchBarTransparentBig : UISearchBarTransparent

@end