//
//  UISearchBarTransparent.m
//  StockCount
//
//  Created by Valera on 12/1/10.
//  Copyright 2010 DBBest Technologies. All rights reserved.
//

#import "UISearchBarBig.h"


@implementation UISearchBarBig

-(UITextField*)textField{
	
    if (!textField)
		for(UIView* view in self.subviews)
			if([view isKindOfClass:[UITextField class]]) {

				textField = (UITextField*)view;
                textField.textColor = [UIColor blackColor];
				break;
			}

	return textField;
}


-(void)layoutSubviews {
    
    [super layoutSubviews];
    
    CGRect r = self.textField.frame;
    r.size.height = 44;
    r.origin.y = (self.frame.size.height - r.size.height)/2;
    self.textField.frame = r;    
}

@end
