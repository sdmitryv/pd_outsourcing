//
//  RTDateEditView.m
//  CloudworksPOS
//
//  Created by Vitaliy Gervazuk on 8/1/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "RTDateEditView.h"
#import "DataUtils.h"
#import "UITextField+Input.h"
#import "UIView+FirstResponder.h"
#import "RTPopoverController.h"

@implementation RTDateEditView

@synthesize date = _date, pickerControl = _pickercontrol, delegate;

-(void)initControls {
    _button = [[UIButton alloc] init];
    
    UIImage* img  = [UIImage imageNamed:@"calendar.png"];
    [_button setImage:img forState:UIControlStateNormal];
    _button.frame = CGRectMake(self.frame.size.width - 30, 3, 30, 25);
    _button.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin;
    [_button addTarget:self action:@selector(clickButton:) forControlEvents:UIControlEventTouchUpInside];
    
    _textField = [[UITextField alloc] init];
    [_textField setUserInteractionEnabled:NO];
    _textField.frame = CGRectMake(0, 0, self.frame.size.width - (30 + 10), 31);
    _textField.borderStyle = UITextBorderStyleRoundedRect;
    _textField.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    _textField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    
    self.backgroundColor = [UIColor clearColor];
    [self addSubview:_button];
    [self addSubview:_textField];
}
- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self initControls];
    }
    return self;
}

-(id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self initControls];
    }
    return self;
}

-(void)setDate:(NSDate *)datevalue {
    
    [_date release];
    _date = [datevalue retain];
    
    NSString* text = nil;
    
    if (_date) {
        text = [DataUtils readableDateStringFromDate:_date];
    }
    
    [_textField setText:text];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/


-  (void)okButtonPressed:(id)sender {
}

-  (void)clearButton:(id)sender {
}

-  (void)clickButton:(id)sender {
    
    UIView *responder = [self.window getFirstResponder];
    if (responder && ![responder canResignFirstResponder])
        return;
    
    if (!_pickercontrol){
        _pickercontrol = [[RTDatePickerViewController alloc] init];
        _pickercontrol.preferredContentSize = CGSizeMake(320, 273);
    }
    if (!_popoverCtrl){
        _popoverCtrl = [[RTPopoverController alloc]initWithContentViewController:_pickercontrol];
        _popoverCtrl.delegate = self;
    }
    _pickercontrol.popover = _popoverCtrl;
    
    _pickercontrol.selectedDate = _date;
    // Show calendar
    [_popoverCtrl presentPopoverFromRect:self->_button.frame inView:self
                     permittedArrowDirections:UIPopoverArrowDirectionAny animated:TRUE];
    
}

- (void)dealloc {
    [_textField release];
    [_button release];
    [_pickercontrol release];    
    [_popoverCtrl release];
    [_date release];
    
    [super dealloc];
}

-(BOOL)readonly{
    return readonly;
}

-(void)setReadonly:(BOOL)value{
    if (readonly==value)return;
    readonly = value;
    _button.enabled = !readonly;
    [_textField setReadOnly:readonly];
}

- (void)showDatePicker {
    [self clickButton:nil];
}

#pragma mark -
#pragma mark RTPopoverControllerDelegate

- (BOOL)popoverControllerShouldDismissPopover:(RTPopoverController *)popoverController {
	return TRUE;
}

- (void)popoverControllerDidDismissPopover:(RTPopoverController *)popoverController {
    if (popoverController != _popoverCtrl || (!_pickercontrol.selectedDate && !self.date) || [_pickercontrol.selectedDate isEqualToDate:self.date])
        return;

    [self setDate:_pickercontrol.selectedDate];
    
    if (delegate)
        [delegate valueChanged:self];
    [_popoverCtrl release];
    _popoverCtrl = nil;
}

@end
