//
//  EditableListView.m
//  iPadPOS
//
//  Created by Lulakov Viacheslav on 3/4/11.
//  Copyright 2011 DBBestTechnologies. All rights reserved.
//

#import "EditableListView.h"
#import "QuartzCore/QuartzCore.h"
#import "ModalAlert.h"

@interface EditableListView(Private)
-(void)initDefaults;
-(void)addItem;
@end

@implementation EditableListView

@synthesize itemTextField,
addButton,
listTableView,
itemList,
delegate,
enabled,
maxItemLength;

-(void)addItem{
	if ((itemTextField.text.length > 0) && (delegate != nil)) {
        for (NSObject *item in itemList) {
            if ([[item description] caseInsensitiveCompare:itemTextField.text] == NSOrderedSame) {
                [ModalAlert say: [NSString stringWithFormat:NSLocalizedString(@"LIST_ITEMS_EXISTS_FORMAT", nil), itemTextField.text]];
                return;
            }
        }
		id item = [delegate editableListView:self addItem:itemTextField.text];
		if (item != nil) {
			[itemList addObject:item];
			[itemList sortUsingComparator:^(id obj1, id obj2) {
				return [[obj1 description] localizedCaseInsensitiveCompare:[obj2 description]];
			}];
			itemTextField.text = @"";
			//[itemTextField resignFirstResponder];
			[listTableView reloadData];
			NSUInteger idx = [itemList indexOfObject:item];
			if (idx !=NSNotFound && idx < [listTableView numberOfRowsInSection:0]){
				NSIndexPath* indexPath = [NSIndexPath indexPathForRow:idx inSection:0];
				[listTableView selectRowAtIndexPath:indexPath animated:TRUE scrollPosition:UITableViewScrollPositionBottom];
				[listTableView.delegate tableView:listTableView didSelectRowAtIndexPath:indexPath];
			}
		}
	}
}
- (void) addButtonClick:(id)sender {
	[self addItem];
}

-(void)initDefaults{
	self.backgroundColor = [UIColor clearColor];
	addButton = [[UIButton buttonWithType:UIButtonTypeCustom]retain];
	addButton.frame = CGRectMake(self.frame.size.width - 86, 2, 84, 30);
	[addButton setBackgroundImage:[UIImage imageNamed:@"green_button.png"] forState:UIControlStateNormal];
	[addButton setTitle:NSLocalizedString(@"ADDBTN_TITLE", nil) forState:UIControlStateNormal];
	addButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
	addButton.contentEdgeInsets = UIEdgeInsetsMake(0, 38, 0, 0);
	[addButton addTarget:self action:@selector(addButtonClick:) forControlEvents: UIControlEventTouchUpInside];
	[self addSubview:addButton];
	UIImageView *leftBorder = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"left_border_rounded_rect.png"]];
	leftBorder.frame = CGRectMake(2, 2, 14, 30);
	[self addSubview:leftBorder];
	[leftBorder release];
	UIImageView *centerBorder = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"center_border_rounded_rect.png"]];
	centerBorder.frame = CGRectMake(16, 2, self.frame.size.width - 84, 30);
	[self addSubview:centerBorder];
	[centerBorder release];
	UIImageView *rightBorder = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"right_border_rounded_rect.png"]];
	rightBorder.frame = CGRectMake(self.frame.size.width - 68, 2, 14, 30);
	[self addSubview:rightBorder];
	[rightBorder release];
	itemTextField = [[UITextField alloc] initWithFrame:CGRectMake(16, 7, self.frame.size.width - 84, 30)];
    itemTextField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
	itemTextField.delegate = self;
	[self addSubview:itemTextField];
	UIButton *backButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
	backButton.frame = CGRectMake(2, 36, self.frame.size.width - 4, self.frame.size.height - 38);
	//		backButton.enabled = NO;
	backButton.clipsToBounds = YES;
	[self addSubview:backButton];
	listTableView = [[UITableView alloc] initWithFrame:backButton.bounds style:UITableViewStylePlain];
//	listTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 36, self.frame.size.width, self.frame.size.height - 38) style:UITableViewStyleGrouped];
	listTableView.backgroundColor = [UIColor clearColor];
	//		listTableView = [[UITableView alloc] initWithFrame:CGRectMake(2, 36, frame.size.width - 4, frame.size.height - 38) style:UITableViewStylePlain];
	listTableView.delegate = self;
	listTableView.dataSource = self;
	listTableView.backgroundView = nil;
	listTableView.sectionHeaderHeight = 0.0;
	listTableView.rowHeight = 30;
	listTableView.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | 
	UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
	listTableView.layer.masksToBounds = YES;
	listTableView.layer.cornerRadius = 12.0;
	[backButton addSubview:listTableView];
//	[self addSubview:listTableView];
	
	UIImage *image = [UIImage imageNamed:@"delete.png"];
	deleteButton = [[UIButton buttonWithType:UIButtonTypeCustom] retain];
	CGRect frame = CGRectMake(0.0, 0.0, image.size.width, image.size.height);
	deleteButton.frame = frame;	// match the button's size with the image size
	//[deleteButton setBackgroundImage:image forState:UIControlStateNormal];
	[deleteButton setImage:image forState:UIControlStateNormal];
    [deleteButton setImage:image forState:UIControlStateHighlighted];
	[deleteButton addTarget:self action:@selector(deleteButtonTapped:event:) forControlEvents:UIControlEventTouchUpInside];
	enabled = TRUE;
    maxItemLength = 0;
}

- (id)initWithCoder:(NSCoder *)aDecoder{
	if ((self=[super initWithCoder:aDecoder])){
		[self initDefaults];
	}
	return self;
}
- (id)initWithFrame:(CGRect)frame {
	if ((self = [super initWithFrame:frame])) {
		[self initDefaults];
	}
	return self;
}

- (void)dealloc {
	[itemTextField release];
	[addButton release];
	[listTableView release];
	[itemList release];
	[deleteButton release];
    [super dealloc];
}

- (void)setItemList: (NSMutableArray*)items {
	if (itemList != items) {
		[itemList release];
		itemList = [items retain];
		[itemList sortUsingComparator:^(id obj1, id obj2) {
			return [[obj1 description] localizedCaseInsensitiveCompare:[obj2 description]];
		}];
		[listTableView reloadData];
	}
}

#pragma mark -
#pragma mark Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
	cell.accessoryView = deleteButton;
	if (delegate && [delegate respondsToSelector:@selector(editableListView:didSelectItem:)]){
		id item = itemList[indexPath.row];
		[delegate editableListView:self didSelectItem:item];
	}
}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath{
	
}

- (void)tableView:(UITableView *)tableView accessoryButtonTappedForRowWithIndexPath:(NSIndexPath *)indexPath {
	id item = itemList[indexPath.row];
	if (delegate != nil) {
		if ([delegate editableListView:self deleteItem:item]) {
			[itemList removeObjectAtIndex:indexPath.row];
			[tableView reloadData];	
			if ([listTableView numberOfRowsInSection:0]){
				NSInteger row = indexPath.row;
				if (row >= [listTableView numberOfRowsInSection:indexPath.section]){
					row = [listTableView numberOfRowsInSection:indexPath.section] - 1;
				}
				NSIndexPath* index = [NSIndexPath indexPathForRow:row inSection:indexPath.section];
				[listTableView selectRowAtIndexPath:index animated:TRUE scrollPosition:UITableViewScrollPositionBottom];
				[listTableView.delegate tableView:listTableView didSelectRowAtIndexPath:index];
			}
		}
	}
}

- (NSIndexPath *)tableView:(UITableView *)tableView willDeselectRowAtIndexPath:(NSIndexPath *)indexPath {
	UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
	cell.accessoryView = nil;
	return indexPath;
}

#pragma mark -
#pragma mark Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [itemList count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"RowCell";
	
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
	
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
    }
    
    cell.textLabel.text = [itemList[indexPath.row] description];
	cell.textLabel.font = [UIFont systemFontOfSize:16];
	NSIndexPath* selectedIndexPath = [tableView indexPathForSelectedRow];
	if ((selectedIndexPath != nil) && (indexPath.row == selectedIndexPath.row)) {
		cell.accessoryView = deleteButton;
	}
	else {
		cell.accessoryView = nil;
	}
	
	return cell;
}

-(void)setEnabled:(BOOL)value{
	if (enabled!=value){
		enabled = value;
		itemTextField.enabled = addButton.enabled = enabled;
	}
}

#pragma mark -
#pragma mark UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
	[self addItem];
	return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    if (maxItemLength > 0) {
        if (textField.text.length - range.length + string.length > maxItemLength) {
            return NO;
        }
    }
	return YES;
}

#pragma mark -
#pragma mark Actions

- (void)deleteButtonTapped:(id)sender event:(id)event
{
	NSSet *touches = [event allTouches];
	UITouch *touch = [touches anyObject];
	CGPoint currentTouchPosition = [touch locationInView:listTableView];
	NSIndexPath *indexPath = [listTableView indexPathForRowAtPoint: currentTouchPosition];
	if (indexPath != nil)
	{
		[self tableView: listTableView accessoryButtonTappedForRowWithIndexPath: indexPath];
	}
}

#pragma mark -
#pragma mark Methods

-(NSInteger)selectedRowIndex{
	NSIndexPath* path = [listTableView indexPathForSelectedRow]; 
	if (!path) return -1;
	return path.row;
}

-(id)selectedObject{
	NSInteger idx = self.selectedRowIndex;
	if (idx>=0 && idx < itemList.count){
		return itemList[idx];
	}
	return nil;
}

@end
