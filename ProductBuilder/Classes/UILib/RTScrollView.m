//
//  RTWScrollView.m
//  iPadPOS
//
//  Created by valera on 3/11/11.
//  Copyright 2011 DBBestTechnologies. All rights reserved.
//

#import "RTScrollView.h"
#import "UIView+FirstResponder.h"
#import "UIRoundedCornersView.h"
#import "UIViewControllerExtended.h"

@interface RTScrollView(){
    BOOL insideFrameSet;
    BOOL contentSizeSetExplicit;
}

@end

@implementation RTScrollView

-(BOOL)isScrollEnabled{
     //prevents right selection dot to be partially hidden in right aligned text fields
     NSArray* callStackSymbols = [NSThread callStackSymbols];
     for (NSInteger i = 0; i< callStackSymbols.count; i++){
         NSString* line = callStackSymbols[i];
         if ((i + 2) < callStackSymbols.count && [line containsString:@"[UISelectionGrabber updateDot]"] && ![callStackSymbols[i + 2] containsString:@"[UITextRangeView willScroll]"]){
             return NO;
         }
     }
     return [super isScrollEnabled];
 }

-(void)initDefaults{
    _cornerRadius = 5.0f;
    _scrollViewIsAdjustedToKeyboard = NO;
    _ownerView = self;
}

-(id)init{
	if ((self = [super init])){
		[self initDefaults];
	}
	return self;
}

-(id)initWithCoder:(NSCoder *)aDecoder{
	if ((self = [super initWithCoder:aDecoder])){
		[self initDefaults];
	}
	return self;
}

-(id)initWithFrame:(CGRect)frame{
	if ((self=[super initWithFrame:frame])){
		[self initDefaults];
	}
	return self;
}

- (void)didMoveToSuperview{
    [super didMoveToSuperview];
    [self removeObservers];
    if (!self.superview) return;
    // register for keyboard notifications
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
}

- (void)willMoveToSuperview:(UIView *)newSuperview{
    [super willMoveToSuperview:nil];
    if (!newSuperview){
        [self removeObservers];
    }
}

-(void)removeObservers{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

- (void)dealloc {
    [self removeObservers];
    [super dealloc];
}

#pragma roundedCorners
-(void)layoutSubviews{
    [super layoutSubviews];
    [UIRoundedCornersView drawRoundedCorners:self radius:_cornerRadius];
}

-(void)setCornerRadius:(CGFloat)value{
    _cornerRadius = value;
    [UIRoundedCornersView drawRoundedCorners:self radius:_cornerRadius];
}


-(void)setFrame:(CGRect)value{
    insideFrameSet = TRUE;
	[super setFrame:value];
    if (!contentSizeSetExplicit){
        self.contentSize = self.frame.size;
    }
    
    if (_scrollViewIsAdjustedToKeyboard) {
        UIView * firstResponder = [self getFirstResponder];
        UIEdgeInsets contentInsets = UIEdgeInsetsZero;
        self.contentInset = contentInsets;
        self.scrollIndicatorInsets = contentInsets;
        _scrollViewIsAdjustedToKeyboard = NO;
        if (firstResponder){
            [self adjustFirstResponder:firstResponder toKeyboardRect:lastKeyboardRect];
        }
    }
    
    insideFrameSet = FALSE;
}

-(void)setContentSize:(CGSize)contentSize{
    [super setContentSize:contentSize];
    if (!insideFrameSet)
        contentSizeSetExplicit = TRUE;
}

#pragma mark -
#pragma mark keyboard handlers

- (void)keyboardWillHide:(NSNotification *)n{
    if (!_scrollViewIsAdjustedToKeyboard) {
        return;
    }
    //restore originals
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    self.contentInset = contentInsets;
    self.scrollIndicatorInsets = contentInsets;
    if (!IS_IPHONE_6_OR_MORE) // in case of iPhone - avoid scroll to top when loosing focus
        [self setContentOffset:CGPointZero animated:YES];
    
    lastKeyboardRect = CGRectZero;
    _scrollViewIsAdjustedToKeyboard = NO;
}

- (void)keyboardWillShow:(NSNotification *)n
{
    if (_scrollViewIsAdjustedToKeyboard) {
        return;
    }
    NSDictionary* userInfo = [n userInfo];
    
    // get the size of the keyboard
    CGRect keyboardEndFrame;
	[userInfo[UIKeyboardFrameEndUserInfoKey] getValue:&keyboardEndFrame];
	
	CGRect keyboardRect = [self.superview convertRect:keyboardEndFrame fromView:nil];
    
    UIViewController *appRootVC = [[UIApplication sharedApplication].windows[0] rootViewController];
    CGRect keyboardRectInRootView = [appRootVC.view convertRect:keyboardEndFrame fromView:nil];
    if (keyboardRectInRootView.origin.y >= appRootVC.view.bounds.size.height-1) {
        return;
    }
    
    // This is an ivar I'm using to ensure that we do not do the frame size adjustment on the UIScrollView if the keyboard is already shown.
	//This can happen if the user, after fixing editing a UITextField, scrolls the resized UIScrollView to another UITextField and attempts 
	//to edit the next UITextField.  If we were to resize the UIScrollView again, it would be disastrous.  
	//NOTE: The keyboard notification will fire even when the keyboard is already shown.
    if (_scrollViewIsAdjustedToKeyboard) {
        if (CGRectEqualToRect(keyboardRect, lastKeyboardRect)) {
            return;
        }
        UIEdgeInsets contentInsets = UIEdgeInsetsZero;
        self.contentInset = contentInsets;
        self.scrollIndicatorInsets = contentInsets;
    }

    UIView * firstResponder = !self.superview ? nil : [_ownerView getFirstResponder];
    if (firstResponder == nil) {
        //check if parent view controller currently is showing other view contrtoller modally
        UIViewController* viewController = self.firstAvailableUIViewController;
        if (viewController && (viewController.presentedViewController || ([viewController isKindOfClass:UIViewControllerExtended.class] && ((UIViewControllerExtended*)viewController).childModalController))){
            lastKeyboardRect = CGRectZero;
            return;
        }
    }

    lastKeyboardRect = keyboardRect;
    
    if (CGRectIntersectsRect(self.frame, keyboardRect)) {
        [UIView animateWithDuration:0.3f animations:^{
            [self adjustFirstResponder:firstResponder toKeyboardRect:keyboardRect];
        }];
        _scrollViewIsAdjustedToKeyboard = YES;
    }
}

-(void)adjustFirstResponder:(UIView*)firstResponder toKeyboardRect:(CGRect)keyboardRect{
    if (!CGRectIntersectsRect(self.frame, keyboardRect)) return;
    CGFloat deltaY = (self.frame.origin.y + self.frame.size.height) - keyboardRect.origin.y;
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, deltaY, 0.0);
    self.contentInset = contentInsets;
    self.scrollIndicatorInsets = contentInsets;
    if (!firstResponder) return;
    CGRect aRect = self.frame;
    aRect.size.height -= deltaY;
    CGRect rect = [self convertRect:firstResponder.frame fromView:firstResponder.superview];
    if (!CGRectContainsPoint(aRect, CGPointMake(rect.origin.x, rect.origin.y + rect.size.height))) {
        [self scrollRectToVisible:rect animated:NO];
    }
    else{
        [self scrollRectToVisible:CGRectZero animated:NO];
    }
}

-(void)scrollToFirstResponder{
	UIView* firstResponder = [self getFirstResponder];
	if (!firstResponder) return;
	CGRect scrollRect = [self convertRect:firstResponder.frame fromView:firstResponder.superview];
    [self scrollRectToVisible:scrollRect animated:YES];
}

@end
