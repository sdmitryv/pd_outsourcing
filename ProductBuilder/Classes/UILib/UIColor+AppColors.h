//
//  UIAppColor.h
//  iPadPOS
//
//  Created by valera on 3/25/11.
//  Copyright 2011 DBBestTechnologies. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface UIColor (AppColors)

+(UIColor*)blueGrayColor;
@end
