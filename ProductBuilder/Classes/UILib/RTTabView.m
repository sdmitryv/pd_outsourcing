//
//  RTTabView.m
//  iPadPOS
//
//  Created by valera on 3/21/11.
//  Copyright 2011 DBBestTechnologies. All rights reserved.
//

#import "RTTabView.h"
#import <QuartzCore/QuartzCore.h>


@implementation RTTabButton

@synthesize object;

+(id)buttonWithObject:(NSObject*)value{
	RTTabButton* button = [[self class] buttonWithType:UIButtonTypeCustom];
	button.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
	button.titleLabel.numberOfLines = 0;
	button.contentEdgeInsets = UIEdgeInsetsMake(5,5,16,5);
	button.layer.borderColor = [UIColor grayColor].CGColor;
	button.layer.borderWidth = 1.0f;
	button.layer.masksToBounds = YES;
	button.layer.cornerRadius = 10.0;
	button->object = [value retain];
	return button;
}

-(void) dealloc{
	
	[object release];
	[super dealloc];

}

@end

@interface RTTabView (Private)

- (void)initDefaults;
- (void)selectTab: (id)sender;
- (void)buildUI;

@end

@implementation RTTabView

@synthesize dataSource, tabSpace, tabWidth, currentTabIndex, currentTab, tabTextColor,
tabSelectedTextColor, tabBackground, tabSelectedBackground, delegate;

-(id)initWithCoder:(NSCoder *)aDecoder{
	if ((self=[super initWithCoder:aDecoder])){
		[self initDefaults];
	}
	return self;
}

-(id)initWithFrame:(CGRect)frame{
	if ((self=[super initWithFrame:frame])){
		[self initDefaults];
	}
	return self;
}

-(void)dealloc{
	[dataSource release];
	[tabs release];
	[tabTextColor release];
	[tabBackground release];
	[tabSelectedTextColor release];
	[tabSelectedBackground release];
	[super dealloc];
}

- (void)initDefaults{
	self.clipsToBounds = TRUE;
	tabs = [[NSMutableArray alloc]init];
	tabSpace = 2;
	tabWidth = 93;
	currentTabIndex = -1;
	tabTextColor = [[UIColor grayColor]retain];
	tabSelectedTextColor = [[UIColor whiteColor]retain];
	tabBackground = [[UIColor whiteColor]retain];
	tabSelectedBackground = [[UIColor grayColor]retain];
}

- (void)buildUI{
	
	for(RTTabButton* tab in tabs){
		[tab removeFromSuperview];
	}
	[tabs removeAllObjects];
	
	CGFloat x = 0;
	for(NSObject* item in dataSource)
	{
		RTTabButton *button = [RTTabButton buttonWithObject:item];
		button.frame = CGRectMake(x, 0, tabWidth, self.frame.size.height + 11);
		x+=tabWidth + tabSpace;
		button.autoresizingMask = UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleBottomMargin;
		button.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
		button.contentMode = UIViewContentModeScaleAspectFit;
		button.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
		button.titleLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:15.000];
		
		[button setTitleColor:tabTextColor forState:UIControlStateNormal];
		[button setBackgroundColor:tabBackground forState:UIControlStateNormal];

		[button setTitle: [item description] forState:UIControlStateNormal];
		
		[button addTarget:self action: @selector(selectTab:) forControlEvents: UIControlEventTouchUpInside];
		[tabs addObject:button];
		[self addSubview:button];
	}
}

-(void)setDataSource:(NSArray*)value{
	if (dataSource!=value){
		[dataSource release];
		dataSource = [value retain];
		[self buildUI];		
	}
}

-(void)setTabSpace:(CGFloat)value{
	if (tabSpace!=value){
		tabSpace = value;
		[self buildUI];		
	}
}


-(void)setTabWidth:(CGFloat)value{
	if (tabWidth!=value){
		tabWidth = value;
		[self buildUI];		
	}
}

-(void)setTabTextColor:(UIColor*)value{
	if (![tabTextColor isEqual:value]){
		[tabTextColor release];
		tabTextColor = [value retain];
		for(RTTabButton* tab in tabs){
			[tab setTitleColor:tabTextColor forState:UIControlStateNormal];
		}
	}
}

-(void)setTabBackground:(UIColor*)value{
	if (![tabBackground isEqual:value]){
		[tabBackground release];
		tabBackground = [value retain];
		for(RTTabButton* tab in tabs){
			[tab setBackgroundColor:tabBackground forState:UIControlStateNormal];
		}
	}
	
}

-(void)setTabSelectedTextColor:(UIColor*)value{
	if (![tabSelectedTextColor isEqual:value]){
		[tabSelectedTextColor release];
		tabSelectedTextColor = [value retain];
		[self.currentTab setTitleColor:tabSelectedTextColor forState:UIControlStateNormal];
	}
}

-(void)setSelectedBackground:(UIColor*)value{
	if (![tabSelectedBackground isEqual:value]){
		[tabSelectedBackground release];
		tabSelectedBackground = [value retain];
		[self.currentTab setBackgroundColor:tabSelectedBackground forState:UIControlStateNormal];
	} 
}


-(RTTabButton*) currentTab{
	if (currentTabIndex<0 || tabs.count==0
		|| currentTabIndex >= tabs.count)
		return nil;
	return tabs[currentTabIndex];
		
}
			
-(void)setCurrentTab:(RTTabButton*)value{
	self.currentTabIndex = [tabs indexOfObject:value];
}

-(void)setCurrentTabIndex:(NSInteger)value{
	if (currentTabIndex!=value){
		NSInteger oldCurrentTabIndex = currentTabIndex;
		currentTabIndex  =value;
		if (currentTabIndex < 0 || tabs.count==0
			|| currentTabIndex >= tabs.count){
			currentTabIndex = -1;
		}
		if (oldCurrentTabIndex>=0 && tabs.count > 0
			&& oldCurrentTabIndex < tabs.count){
			RTTabButton *oldSelectedTab = tabs[oldCurrentTabIndex];
			[oldSelectedTab setTitleColor:tabTextColor forState:UIControlStateNormal];
			[oldSelectedTab setBackgroundColor:tabBackground forState:UIControlStateNormal];
		}
		if (currentTabIndex>=0){
			RTTabButton *selectedTab = tabs[currentTabIndex];
			[selectedTab setTitleColor:tabSelectedTextColor forState:UIControlStateNormal];
			[selectedTab setBackgroundColor:tabSelectedBackground forState:UIControlStateNormal];
			if (delegate){
				[delegate rtTabView:self didSelectTab:selectedTab];
			}
			[self setNeedsDisplayInRect:CGRectMake(0, self.frame.size.height - 1, self.frame.size.width, 1)];
		}
	}
}

- (void)selectTab: (id)sender {
	self.currentTabIndex = [tabs indexOfObject:sender];
}

- (void)drawRect:(CGRect)rect {
		if ((rect.origin.y + rect.size.height) >= (self.frame.size.height - 1)){
		CGContextRef context = UIGraphicsGetCurrentContext();
		CGContextSetLineWidth(context, 1.0);
		CGContextSetStrokeColorWithColor( context, tabBackground.CGColor );
		CGContextBeginPath(context);
		CGContextMoveToPoint(context, 0, self.frame.size.height);
		RTTabButton* tab = self.currentTab;
		if (tab){
			CGFloat x1 = tab.frame.origin.x;
			CGFloat x2 = x1 + tab.frame.size.width;
			CGContextAddLineToPoint(context, x1, self.frame.size.height);
			CGContextMoveToPoint(context, x2, self.frame.size.height);
		}
		CGContextAddLineToPoint(context, self.frame.size.width,  self.frame.size.height);
		CGContextClosePath( context );
		CGContextStrokePath( context );
	}
}



@end
