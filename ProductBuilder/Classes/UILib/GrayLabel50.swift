//
//  GrayLabel.swift
//  RPlus
//
//  Created by Alexander Martyshko on 7/22/16.
//  Copyright © 2016 Cloudworks. All rights reserved.
//

import UIKit

class GrayLabel: RPDynamicFontLabel {

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        applyDefaults()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        applyDefaults()
    }
    
    func applyDefaults() {
        self.textColor = ColorUtils.infoLabelTextColor()
    }

}

class RPTableCellGrayLabel: RPDynamicFontLabel {
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        applyDefaults()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        applyDefaults()
    }
    
    func applyDefaults() {
        self.textColor = ColorUtils.tableCellGrayTextColor()
    }
}
