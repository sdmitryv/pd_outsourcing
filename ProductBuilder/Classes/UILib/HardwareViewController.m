//
//  HardwareViewController.m
//  ProductBuilder
//
//  Created by Alexander Martyshko on 7/16/13.
//  Copyright (c) 2013 Cloudworks. All rights reserved.
//

#import "HardwareViewController.h"
#import "iMag.h"

@interface HardwareViewController ()

@end

@implementation HardwareViewController

@synthesize useEncryption;

static NSInteger barcodeSem     = 0;
static NSInteger msrSem         = 0;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        _useBarcode = _useCardSwipe = NO;
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appDidBecomeActive:) name:UIApplicationDidBecomeActiveNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(scanAccessoryConnected:) name:iMagDidConnectNotification object:nil];
    }
    return self;
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidBecomeActiveNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:iMagDidConnectNotification object:nil];
    [super dealloc];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

-(BOOL)isVisible {
    return [self isViewLoaded] && self.view.window;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self updateEncryption];
    //    [self launchHardware];
}

-(void)updateEncryption {
    
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    
//    [self stopHardware];
}

- (void)launchHardware {
    if (self.useCardSwipe || self.useBarcode) {
        
    }
    
    if (self.useBarcode) {
        ++barcodeSem;
    }
    
    if (self.useCardSwipe) {
        ++msrSem;
    }
}

- (void)stopHardware {
    if (self.useCardSwipe || self.useBarcode) {
    }
    
    if (self.useBarcode) {
        --barcodeSem;
        if (barcodeSem == 0) {
        }
    }
    
    if (self.useCardSwipe) {
        --msrSem;
        if (msrSem == 0) {
        }
    }
}

#pragma mark - Notifications handlers
- (void)appDidBecomeActive:(NSNotification *)notif {
    if ([self isVisible]) {
        if (self.useBarcode && barcodeSem > 0) {
            --barcodeSem;
        }
        if (self.useCardSwipe && msrSem > 0) {
            --msrSem;
        }
        [self launchHardware];
    }
}

- (void)scanAccessoryConnected:(NSNotification *)notif {
    if ([notif.object isKindOfClass:[RTSwipeReaderDescription class]]) {
        RTSwipeReaderDescription *readerDescription = (RTSwipeReaderDescription *)notif.object;
        if (readerDescription.type == RTHoneywellReader && [self isVisible]) {
            if (self.useBarcode && barcodeSem > 0) {
                --barcodeSem;
            }
            if (self.useCardSwipe && msrSem > 0) {
                --msrSem;
            }
            [self launchHardware];
        }
    }
}

@end
