//
//  CustomCheckBox.m
//  ProductBuilder
//
//  Created by User on 4/23/12.
//  Copyright (c) 2012 Cloudworks. All rights reserved.
//

#import "CustomCheckBox.h"

@interface CustomCheckBox(Private)
- (void)changeState;
@end

@implementation CustomCheckBox
@synthesize buttonStateIsChecked,delegate;
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        yesImage=[[UIImage imageNamed:@"yesImage.png"] retain];
        noImage=[[UIImage imageNamed:@"noImage.png"] retain];
        buttonStateIsChecked=YES;
        [self addTarget:self action:@selector(click:) forControlEvents:UIControlEventTouchUpInside];
        self.showsTouchWhenHighlighted = YES;
    }
    return self;
}

- (void)click:(id)sender {
    [self changeState];
    if (delegate) {
        [delegate stateChangedInCheckbox:self];
    }
    
}
- (void)addTarget:(id)target action:(SEL)action forControlEvents:(UIControlEvents)controlEvents
{
    [self changeState];
    [super addTarget:target action:action forControlEvents:controlEvents];
}

- (void)changeState {
    
    buttonStateIsChecked=!buttonStateIsChecked;
    if (buttonStateIsChecked==NO) {
        [self setImage:noImage forState:UIControlStateNormal];
    }
    else if (buttonStateIsChecked==YES){
        [self setImage:yesImage forState:UIControlStateNormal];  
    }
    
}

- (void)dealloc {
    [yesImage release];
    [noImage release];

    [super dealloc];
}

@end
