//
//  UIView+GetFirstResponder.h
//  StockCount
//
//  Created by Valera on 12/2/10.
//  Copyright 2010 DBBest Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UIView (FirstResponder)

- (UIView*)getFirstResponder;
- (UIView*)topSuperView;

@end

@interface UIView (FindUIViewController)
- (UIViewController *) firstAvailableUIViewController;
@end
