//
//  ButtonArrayView.m
//  CloudworksPOS
//
//  Created by Lulakov Viacheslav on 8/9/11.
//  Copyright 2011 Cloudworks. All rights reserved.
//

#import "ButtonArrayView.h"
#import "MOGlassButton.h"
#import "UIViewControllerExtended.h"
#import <objc/runtime.h>

#define MIN_BUTTON_WIDTH    70

typedef struct{
    BOOL didSelectItem;
    BOOL enableItemButton;
    BOOL shouldShowMore;
} ButtonArrayViewDelegateImplementedMethods;


@interface UIButton(UIButtonInsideUITableViewCell)

@property (nonatomic, retain) UITableViewCell*tableViewCell;

@end

const char* tableViewCellTagKey = "tableViewCellTag";

@implementation UIButton(UIButtonInsideUITableViewCell)

-(UITableViewCell*)tableViewCell{
    return objc_getAssociatedObject(self, tableViewCellTagKey);
}

-(void)setTableViewCell:(UITableViewCell *)value{
    objc_setAssociatedObject(self, tableViewCellTagKey, value, OBJC_ASSOCIATION_ASSIGN);
}

@end

@interface ButtonArrayView(){
    ButtonArrayViewDelegateImplementedMethods delegateMethods;
}
@end

@implementation ButtonArrayView

@synthesize columnCount;
@synthesize buttonHeight;
@synthesize buttonWidth;
@synthesize rowHorizontalMargin;
@synthesize rowVerticalMargin;
@synthesize buttonArrayDelegate, list, showMore, moreTitle, moreButtonTitle;

#pragma mark - Private methods

-(void)initDefaults{
	buttonWidth = 0;
	buttonHeight = 50;
	rowHorizontalMargin = 8;
	rowVerticalMargin = 8;
	columnCount = 1;
	self.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.alwaysBounceVertical = NO;
	self.clipsToBounds = TRUE;
	self.delegate  = self;
	self.dataSource = self;
    enabled = TRUE;
}

-(void)setButtonArrayDelegate:(id<ButtonArrayViewDelegate>)value{
    buttonArrayDelegate = value;
    delegateMethods.didSelectItem = [buttonArrayDelegate respondsToSelector:@selector(buttonArrayView:didSelectItem:)];
    delegateMethods.enableItemButton = [buttonArrayDelegate respondsToSelector:@selector(buttonArrayView:enableItemButton:)];
    delegateMethods.shouldShowMore = [buttonArrayDelegate respondsToSelector:@selector(buttonArrayView:shouldShowMoreWithButton:)];
}

- (UIButton *)createButtonWithFrame:(CGRect)frame {
	UIButton *button = [[[MOGlassButtonMarine alloc] initWithFrame:frame] autorelease];
    button.titleLabel.font = [UIFont systemFontOfSize:15.0];
    button.enabled = YES;
    button.userInteractionEnabled = YES;
    button.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
    button.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    return button;
}

-(void)selectItem:(id)sender {
	if (buttonArrayDelegate) {
		UIButton* button = (UIButton*)sender;
        UITableViewCell* cell = button.tableViewCell;
        if (!cell) return;
        NSIndexPath *path = [self indexPathForCell:cell];
        NSInteger index = button.tag - 100 + path.row * columnCount;
        [buttonArrayDelegate buttonArrayView:self didSelectItemAtIndex:index button:button];
        if (delegateMethods.didSelectItem){
            [buttonArrayDelegate buttonArrayView:self didSelectItem:list[index]];
        }
	}
}

-(UIButton*) buttonAtIndex:(NSUInteger)index{
    NSInteger row = index/columnCount;
    NSUInteger indexAtRow = index - row*columnCount;
    UITableViewCell* cell = [self cellForRowAtIndexPath:[NSIndexPath indexPathForRow:row inSection:0]];
    return [cell.contentView viewWithTag:100 + indexAtRow];
}

-(UIButton*) buttonForValue:(NSObject*)value{
    NSInteger idx = [(NSArray*)list indexOfObject:value];
    if (idx == NSNotFound) return nil;
    return [self buttonAtIndex:idx];
}

-(void)selectMore:(id)sender{
    if (buttonArrayDelegate && delegateMethods.shouldShowMore) {
        if (![buttonArrayDelegate buttonArrayView:self shouldShowMoreWithButton:(UIButton *)sender])
            return;
    }
    
    SimpleTableListViewController* buttons = [[SimpleTableListViewController alloc] init];
	buttons.items = list;
	buttons.selectedIndex = -1;
	buttons.titleText = self.moreTitle;
	buttons.preferredContentSize = CGSizeMake(400.0, 500.0);
	buttons.delegate = self;
    UIButton* buttonMore = (UIButton*)sender;
    
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad) {
        [buttons presentPopoverFromRect:buttonMore.frame inView:buttonMore.superview
                             permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    }
    else {
         [buttons presentInController:[UIViewController topmostModalViewController]];
    }
    [buttons release];
}

-(void)simpleTableListViewController:(SimpleTableListViewController*)controller didSelectIndex:(NSNumber*)anIndex{
    [buttonArrayDelegate buttonArrayView:self didSelectItemAtIndex:[anIndex intValue] button:nil];
	if (delegateMethods.didSelectItem){
        [buttonArrayDelegate buttonArrayView:self didSelectItem:list[[anIndex intValue]]];
    }
}

//-(BOOL)simpleTableListViewController:(SimpleTableListViewController*)controller willSelectIndex:(NSInteger)anIndex {
//    if (delegateMethods.enableItemButton) {
//        return [self.buttonArrayDelegate buttonArrayView:self enableItemButton:list[anIndex]];
//    }
//    return YES;
//}

-(void)simpleTableListViewController:(SimpleTableListViewController*)controller willDisplayValue:(id)value inContent:(id)content {
    if (delegateMethods.enableItemButton){
        if ([content isKindOfClass:[UILabel class]]){
            BOOL isEnabled = [self.buttonArrayDelegate buttonArrayView:self enableItemButton:value];
            ((UILabel*)content).enabled = isEnabled;
            if ([[content superview] isKindOfClass:[GridTableCell class]]){
                ((GridTableCell*)[content superview]).gridTableRow.userInteractionEnabled = isEnabled;
            }
        }
    }
}

#pragma mark - View lifecycle

-(id)initWithFrame:(CGRect)frame style:(UITableViewStyle)style{
	if ((self =[super initWithFrame:frame style:style])){
		[self initDefaults];
	}
	return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder{
	if ((self = [super initWithCoder:aDecoder])){
		[self initDefaults];
	}
	return self;
}

-(id)initWithFrame:(CGRect)frame{
	if ((self = [super initWithFrame:frame])){
		[self initDefaults];
	}
	return self;
}

-(id)init{
	if ((self = [super init])){
		[self initDefaults];
	}
	return self;
}

- (void)dealloc
{
    [list release];
    self.moreTitle = nil;
    self.moreButtonTitle = nil;
    [super dealloc];
}

#pragma mark properties

-(void)setColumnCount:(NSUInteger)value{
    if (columnCount!=value){
        columnCount = value;
        [self reloadData];
    }
}

-(void)setButtonHeight:(NSUInteger)value{
    if (buttonHeight!=value){
        buttonHeight = value;
        [self reloadData];
    }
}

-(void)setButtonWidth:(NSUInteger)value{
    if (buttonWidth!=value){
        buttonWidth = value;
        [self reloadData];
    }
}

-(void)setShowMore:(BOOL)value{
    if (showMore!=value){
        showMore = value;
        [self reloadData];
    }
}

-(void)calcMaxRowCount{
    rowCount = 0;
    moreVisible = FALSE;
    if (!buttonArrayDelegate) return;
    {
        NSInteger number = [list count];
        rowCount = (number + columnCount - 1) / columnCount;
    }
    if (showMore){
        CGFloat height = self.bounds.size.height;
        NSUInteger maxRowCount = ((height + (CGFloat)rowVerticalMargin) / ((CGFloat)buttonHeight + (CGFloat)rowVerticalMargin));
        if (maxRowCount < rowCount)
        {
            rowCount = maxRowCount;
            moreVisible = true;
        }
    }
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(void)layoutSubviews{
    [super layoutSubviews];
    if (internalLayout) {
        internalLayout = FALSE;
        return;
    }
    if (showMore){
        internalLayout = TRUE;
        [self reloadData];
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    [self calcMaxRowCount];
    return rowCount;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	
    if (indexPath.row==rowCount-1 && showMore && moreVisible){
        UITableViewCell *cell = nil;
        
        if (columnCount > 1) {
            NSUInteger currentColumnCount = columnCount;
            NSInteger indexOfFirstItem = indexPath.row * columnCount;
            NSInteger count = [list count];
            if (indexOfFirstItem + columnCount > count) {
                currentColumnCount = count - indexOfFirstItem;
            }
            
            NSString *CellIdentifier = [NSString stringWithFormat:@"ButtonArrayCell%lu", (unsigned long)currentColumnCount];
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            if (!cell) {
                cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                cell.backgroundColor = [UIColor clearColor];
                for(NSInteger i = 0; i < currentColumnCount - 1; i++) {
                    NSUInteger width = buttonWidth;
                    if (width == 0) {
                        //width = (self.bounds.size.width - rowHorizontalMargin) / columnCount - rowHorizontalMargin;
                        width = (self.bounds.size.width - rowHorizontalMargin*(columnCount-1))/columnCount;
                    }
                    if (width < MIN_BUTTON_WIDTH) {
                        width = MIN_BUTTON_WIDTH;
                    }
                    //CGRect frame = CGRectRound(CGRectMake((i + 1) * rowHorizontalMargin + i * width, rowHorizontalMargin, width, buttonHeight));
                    CGRect frame = CGRectRound(CGRectMake(i * rowHorizontalMargin + i * width, 0, width, buttonHeight));
                    UIButton* button = [self createButtonWithFrame:frame];
                    [button addTarget:self action: @selector(selectItem:) forControlEvents: UIControlEventTouchUpInside];
                    button.tag = i+100;
                    button.tableViewCell = cell;
                    [cell.contentView addSubview:button];
                }
            }
            
            for (NSInteger i = 0; i < currentColumnCount - 1; i++) {
                id item = nil;
                if (buttonArrayDelegate != nil) {
                    item = list[indexOfFirstItem + i];
                }
                UIButton* button = [cell.contentView viewWithTag:i+100];
                [button setTitle:[item description] forState:UIControlStateNormal];
                
                if (delegateMethods.enableItemButton) {
                    
                    button.enabled = [self.buttonArrayDelegate buttonArrayView:self enableItemButton:item];
                }
                else {
                    button.enabled = self.enabled;
                }
            }
            UIButton* button = [cell.contentView viewWithTag:99];
            if (!button) {
                NSUInteger width = buttonWidth;
                if (width == 0) {
                    //width = (self.bounds.size.width - rowHorizontalMargin) / columnCount - rowHorizontalMargin;
                    width = (self.bounds.size.width - rowHorizontalMargin*(columnCount-1))/columnCount;
                }
                if (width < MIN_BUTTON_WIDTH) {
                    width = MIN_BUTTON_WIDTH;
                }
                
                NSInteger i = columnCount - 1;
                
                CGRect frame = CGRectRound(CGRectMake(i * rowHorizontalMargin + i * width, 0, width, buttonHeight));
                button = [self createButtonWithFrame:frame];
                [button setTitle:NSLocalizedString(@"MOREBTN_TITLE", nil) forState:UIControlStateNormal];
                [button addTarget:self action: @selector(selectMore:) forControlEvents: UIControlEventTouchUpInside];
                button.enabled = self.enabled;
                button.tag = 99;
                button.tableViewCell = cell;
                [cell.contentView addSubview:button];
            }
            
            button = [cell.contentView viewWithTag:99];
            if (self.moreButtonTitle.length == 0) {
                [button setTitle:NSLocalizedString(@"MOREBTN_TITLE", nil) forState:UIControlStateNormal];
            }
            else {
                [button setTitle:moreButtonTitle forState:UIControlStateNormal];
            }
            button.enabled = self.enabled;
            
            return cell;
        }
        else {
            NSString *CellIdentifier = @"ButtonMore";
            cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            if (!cell) {
                cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                cell.backgroundColor = [UIColor clearColor];

                NSUInteger width = buttonWidth;
                if (width == 0) {
                    //width = (self.bounds.size.width - rowHorizontalMargin) / columnCount - rowHorizontalMargin;
                    width = (self.bounds.size.width - rowHorizontalMargin*(columnCount-1))/columnCount;
                }
                if (width < MIN_BUTTON_WIDTH) {
                    width = MIN_BUTTON_WIDTH;
                }
                CGRect frame = CGRectRound(CGRectMake((self.bounds.size.width - width)/2, 0, width, buttonHeight));
                UIButton* button = [self createButtonWithFrame:frame];
                [button addTarget:self action: @selector(selectMore:) forControlEvents: UIControlEventTouchUpInside];
                button.tag = 99;
                button.tableViewCell = cell;
                [cell.contentView addSubview:button];
            }
            
           UIButton* button = [cell.contentView viewWithTag:99];
           [button setTitle:NSLocalizedString(@"MOREBTN_TITLE", nil) forState:UIControlStateNormal];
            button.enabled = self.enabled;
        }
        return cell;
    }
    else{

        NSUInteger currentColumnCount = columnCount;
        NSInteger indexOfFirstItem = indexPath.row * columnCount;
        NSInteger count = [list count];
        if (indexOfFirstItem + columnCount > count) {
            currentColumnCount = count - indexOfFirstItem;
        }
        
        NSString *CellIdentifier = [NSString stringWithFormat:@"ButtonArrayCell%lu", (unsigned long)currentColumnCount];
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (!cell) {
            cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.backgroundColor = [UIColor clearColor];
            for(NSInteger i = 0; i < currentColumnCount; i++) {
                NSUInteger width = buttonWidth;
                if (width == 0) {
                    //width = (self.bounds.size.width - rowHorizontalMargin) / columnCount - rowHorizontalMargin;
                    width = (self.bounds.size.width - rowHorizontalMargin*(columnCount-1))/columnCount;
                }
                if (width < MIN_BUTTON_WIDTH) {
                    width = MIN_BUTTON_WIDTH;
                }
                //CGRect frame = CGRectRound(CGRectMake((i + 1) * rowHorizontalMargin + i * width, rowHorizontalMargin, width, buttonHeight));
                CGRect frame = CGRectRound(CGRectMake(i * rowHorizontalMargin + i * width, 0, width, buttonHeight));
                UIButton* button = [self createButtonWithFrame:frame];
                [button addTarget:self action: @selector(selectItem:) forControlEvents: UIControlEventTouchUpInside];
                button.tag = i+100;
                button.tableViewCell = cell;
                [cell.contentView addSubview:button];
            }
        }
        
        for (NSInteger i = 0; i < currentColumnCount; i++) {
            id item = nil;
            if (buttonArrayDelegate != nil) {
                item = list[indexOfFirstItem + i]; 
            }
            UIButton* button = [cell.contentView viewWithTag:i+100];
            [button setTitle:[item description] forState:UIControlStateNormal];
            
            if (delegateMethods.enableItemButton) {
                
                button.enabled = [self.buttonArrayDelegate buttonArrayView:self enableItemButton:item];
            }
            else {
                button.enabled = self.enabled;
            }
        }
        return cell;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return buttonHeight  + (indexPath.row==(rowCount-1) ? 0.0 : rowVerticalMargin);
}

-(BOOL)enabled{
    return enabled;
}

-(void)setEnabled:(BOOL)value{
    enabled = value;
    [self reloadData];
}

-(void)updateButtonsState {
    for (NSObject* item in list) {
        UIButton* button = [self buttonForValue:item];
        if (button.tag == 99) {
            continue;
        }
        if (delegateMethods.enableItemButton) {
            button.enabled = [self.buttonArrayDelegate buttonArrayView:self enableItemButton:item];
        }
        else {
            button.enabled = self.enabled;
        }
    }
}

- (void)setList:(id)value {
    if (list != value) {
        [list release];
        list = [value retain];
        [self reloadData];
    }
}

@end
