//
//  EmployeePhotoView.swift
//  RPlus
//
//  Created by Alexander Martyshko on 11/17/16.
//  Copyright © 2016 Cloudworks. All rights reserved.
//

import UIKit

protocol EmployeePhotoViewDelegate: AnyObject {
    func employeePhotoViewTapped(view: EmployeePhotoView)
}

class EmployeePhotoViewButton: UIButton {
    var employeePhotoView:EmployeePhotoView!
    
    override init(frame: CGRect) {
        super.init(frame:frame)
        initDefaults()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initDefaults()
    }
    
    private func initDefaults(){
        self.isExclusiveTouch = true
        setValue(UIButtonType.custom.rawValue, forKey: "buttonType")
        self.titleLabel?.removeFromSuperview()
        employeePhotoView = EmployeePhotoView(frame:.zero)
        employeePhotoView.isUserInteractionEnabled = false
        addSubview(employeePhotoView)
        employeePhotoView.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        employeePhotoView.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        let proportionalConstraint = employeePhotoView.widthAnchor.constraint(equalTo: employeePhotoView.heightAnchor)
        proportionalConstraint.priority = UILayoutPriorityRequired
        proportionalConstraint.isActive = true
        let fillWidthConstraint = employeePhotoView.widthAnchor.constraint(equalTo: widthAnchor)
        fillWidthConstraint.priority = UILayoutPriorityDefaultHigh
        fillWidthConstraint.isActive = true
        let fillHeightConstraint =  employeePhotoView.heightAnchor.constraint(equalTo: heightAnchor)
        fillHeightConstraint.priority = UILayoutPriorityDefaultHigh
        fillHeightConstraint.isActive = true
    }
    
    private func handleState(){
        if !self.isEnabled{
            employeePhotoView.label.backgroundColor = .lightGray
        }
        else if self.isSelected || self.isHighlighted{
            employeePhotoView.label.backgroundColor =  EmployeePhotoView.labelDefaultColor.darken(0.1)
        }
        else{
            employeePhotoView.label.backgroundColor = EmployeePhotoView.labelDefaultColor
        }
    }
    
    override var isEnabled: Bool{
        get{
            return super.isEnabled
        }
        set{
            if super.isEnabled == newValue{
                return
            }
            super.isEnabled = newValue
            handleState()
        }
    }
    
    override var isHighlighted: Bool{
        get{
            return super.isHighlighted
        }
        set{
            if super.isHighlighted == newValue{
                return
            }
            super.isHighlighted = newValue
            handleState()
        }
    }
    
    override var isSelected: Bool{
        get{
            return super.isSelected
        }
        set{
            if super.isSelected == newValue{
                return
            }
            super.isSelected = newValue
            handleState()
        }
    }
}

class EmployeePhotoView: UIView {
    
    weak var delegate: EmployeePhotoViewDelegate?
    
    var imageView: UIImageView!
    fileprivate var dotImageView: UIImageView!
    fileprivate var label: UILabel!
    var isEditable = false

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initDefaults()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initDefaults()
    }
    
    static let labelDefaultColor : UIColor = {
        return UIColor(r: 170, g: 170, b: 170)
    }()
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.layer.cornerRadius = min(self.frame.size.height, self.frame.size.width) / 2.0
    }
    

    fileprivate func initDefaults() {
        
        self.backgroundColor = UIColor.clear
        
        self.clipsToBounds = true
        
        label = UILabel()
        label.backgroundColor = EmployeePhotoView.labelDefaultColor
        label.textColor = UIColor.white
        label.font = UIFont.systemFont(ofSize: 24, weight: UIFontWeightSemibold)
        label.textAlignment = NSTextAlignment.center
        self.addSubview(label)
        
        imageView = UIImageView()
        imageView.contentMode = UIViewContentMode.scaleAspectFit
        imageView.backgroundColor = UIColor.clear
        self.addSubview(imageView)
        
        dotImageView = UIImageView()
        dotImageView.contentMode = UIViewContentMode.center
        dotImageView.backgroundColor = UIColor.clear
        dotImageView.image = ImageHelper.circleImage(CGSize(width: 12, height: 12), color: ColorUtils.ColorWithRGB(red: 76, green: 217, blue: 100))
        self.addSubview(dotImageView)
    
        self.translatesAutoresizingMaskIntoConstraints = false
        imageView.translatesAutoresizingMaskIntoConstraints = false
        dotImageView.translatesAutoresizingMaskIntoConstraints = false
        label.translatesAutoresizingMaskIntoConstraints = false

        
        self.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-0-[label]-0-|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: ["label" : label]))
        self.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-0-[label]-0-|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: ["label" : label]))
        
        self.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-0-[imageView]-0-|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: ["imageView" : imageView]))
        self.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-0-[imageView]-0-|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: ["imageView" : imageView]))
        
        self.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:[dotImageView]-0-|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: ["dotImageView" : dotImageView]))
        self.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-0-[dotImageView]", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: ["dotImageView" : dotImageView]))
       
        /*let tapRecognizer = UITapGestureRecognizer()
        tapRecognizer.addTarget(self, action: #selector(EmployeePhotoView.tapRecognized(sender:)))
        self.addGestureRecognizer(tapRecognizer)*/
    }

    @objc fileprivate func tapRecognized(sender: UITapGestureRecognizer) {
        delegate?.employeePhotoViewTapped(view: self)
    }
    
    fileprivate var _employee: Employee?
    var employee: Employee? {
        get {
            return _employee
        }
        set {
            _employee = newValue
            
            if let emp = _employee {
                /*if let picture = emp.picture {
                    
                    imageView.isHidden = false
                    label.isHidden = true
                    
                    if ContentHelper.contentExists(picture, contentType: TaskDetail.ContentType.picture, isThumb: true, isTemp: true) {
                        imageView.image = ContentHelper.tempImage(picture, isThumb: true)
                    }
                    else if ContentHelper.contentExists(picture, contentType: TaskDetail.ContentType.picture, isThumb: true, isTemp: false) {
                        imageView.image = ContentHelper.localImage(picture, isThumb: true)
                    }
                    else {
                        imageView.setIndicatorStyle(.gray)
                        imageView.setShowActivityIndicator(true)
                        imageView.sd_setImage(with: ContentLoadHelper.imageUrl(picture, isThumb: true))
                    }
                }
                else {*/
                    
                    if isEditable {
                        imageView.isHidden = false
                        imageView.image = #imageLiteral(resourceName: "icon_addPhoto")
                        label.isHidden = true
                    }
                    else {
                        imageView.isHidden = true
                        label.isHidden = false
                        label.text = emp.initials
                    }
               // }
                
                dotImageView.isHidden = true
            }
            else {
                
                imageView.isHidden = true
                dotImageView.isHidden = true
                label.isHidden = true
            }
        }
    }
}

