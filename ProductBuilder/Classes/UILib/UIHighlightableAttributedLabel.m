//
//  UIHighlightableAttributedLabel.m
//  ProductBuilder
//
//  Created by valera on 5/3/13.
//  Copyright (c) 2013 Cloudworks. All rights reserved.
//

#import "UIHighlightableAttributedLabel.h"
@interface UIHighlightableAttributedLabel()

@property(nonatomic,retain) NSAttributedString *attributedTextCopy;
@property(nonatomic,retain) NSAttributedString *highlightedAttributedText;
@property(nonatomic,assign) BOOL ignoreSetAttributedText;
@end

@implementation UIHighlightableAttributedLabel

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

-(void)dealloc{
    self.attributedTextCopy = nil;
    self.highlightedAttributedText = nil;
    [super dealloc];
}

- (void)setAttributedText:(NSAttributedString *)attributedText {
    [super setAttributedText:attributedText];
    if (self.ignoreSetAttributedText) return;
    self.attributedTextCopy = attributedText;
    self.highlightedAttributedText = nil;
    if(attributedText) {
        NSMutableAttributedString *attributedTextMutCopy = [attributedText mutableCopy];
        [attributedTextMutCopy addAttributes:@{ NSForegroundColorAttributeName : self.highlightedTextColor } range:NSMakeRange(0, attributedText.length)];
        self.highlightedAttributedText = attributedTextMutCopy;
        [attributedTextMutCopy release];
    }
}

-(void)setText:(NSString *)text{
    [super setText:text];
    self.attributedTextCopy = nil;
    self.highlightedAttributedText = nil;
}

-(void)setHighlightedTextColor:(UIColor *)highlightedTextColor{
    [super setHighlightedTextColor:highlightedTextColor];
    if (self.highlightedAttributedText){
        self.attributedText = self.attributedText;
    }
}

- (void)setHighlighted:(BOOL)highlighted {
    [super setHighlighted:highlighted];
    if(self.highlightedAttributedText) {
        self.ignoreSetAttributedText = YES;
        self.attributedText = (highlighted) ? self.highlightedAttributedText : self.attributedTextCopy;
        self.ignoreSetAttributedText = NO;
    }
}

@end
