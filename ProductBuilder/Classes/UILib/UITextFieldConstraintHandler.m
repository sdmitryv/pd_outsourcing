//
//  UITextFieldConstraintHandler.m
//  ProductBuilder
//
//  Created by valera on 10/31/12.
//  Copyright (c) 2012 Cloudworks. All rights reserved.
//

#import "UITextFieldConstraintHandler.h"
#import "UITextField+Input.h"

@interface UITextFieldConstraintHandler(){
    BOOL _insideShouldEndEditing;
}
@end
@implementation UITextFieldConstraintHandler

@synthesize inputTextField,
delegate, maxTextLength;

-(id)init{
    
	if ((self=[super init])){

    }
    
    return self;
}



- (BOOL)textFieldShouldReturn:(UITextField *)textField {

    BOOL result = YES;
    if (delegate != nil && [delegate respondsToSelector:@selector(textFieldShouldReturn:)])
        result = [delegate textFieldShouldReturn:textField];

    return result;
}



- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    BOOL hasSelector = (delegate != nil) & [delegate respondsToSelector:@selector(textField:shouldChangeCharactersInRange:replacementString:)];
    
    
	if (delegate != nil && [delegate isKindOfClass:[UITextFieldReadOnlyDelegate class]]) return FALSE;

	if([string length] == 0)//backspace
    {
        if (hasSelector)
            [delegate textField:textField shouldChangeCharactersInRange:range replacementString:string];

        return YES;
    }
    
	if (nonAplhaNumericSet && [string rangeOfCharacterFromSet:nonAplhaNumericSet].location != NSNotFound)
        return NO;
    
    if (maxTextLength > 0) {

        NSUInteger newLength = [textField.text length] + [string length] - range.length;
        if (newLength > maxTextLength)
            return NO;
    }
    
    if (hasSelector)
        return [delegate textField:textField shouldChangeCharactersInRange:range replacementString:string];
    
    return YES;
}



- (void)textFieldDidEndEditing:(UITextField *)textField{

    if (delegate != nil && [delegate respondsToSelector:@selector(textFieldDidEndEditing:)])
        [delegate textFieldDidEndEditing:textField];
}



- (void)textFieldDidBeginEditing:(UITextField *)textField{
    
    if (delegate != nil && [delegate respondsToSelector:@selector(textFieldDidBeginEditing:)])
        [delegate textFieldDidBeginEditing:textField];
}



-(void)dealloc{
    
	[nonAplhaNumericSet release];
	inputTextField = nil;
	[super dealloc];
}



- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    
    BOOL result = YES;
    
    if (delegate != nil && [delegate respondsToSelector:@selector(textFieldShouldBeginEditing:)])
        result = [delegate textFieldShouldBeginEditing:textField];

    return result;
}



- (BOOL)textFieldShouldEndEditing:(UITextField *)textField  {
    if (_insideShouldEndEditing) return FALSE;
    _insideShouldEndEditing = TRUE;
    BOOL result = YES;
    
    if (delegate != nil && [delegate respondsToSelector:@selector(textFieldShouldEndEditing:)])
        result = [delegate textFieldShouldEndEditing:textField];
    _insideShouldEndEditing = FALSE;
    return result;
}



- (BOOL)textFieldShouldClear:(UITextField *)textField {
    
    BOOL result = YES;
    
    if (delegate != nil && [delegate respondsToSelector:@selector(textFieldShouldClear:)])
        result = [delegate textFieldShouldClear:textField];

    return result;
}

-(void)forwardInvocation:(NSInvocation *)anInvocation{
    if ([delegate respondsToSelector:[anInvocation selector]])
        [anInvocation invokeWithTarget:delegate];
    else
        [super forwardInvocation:anInvocation];
}

-(NSMethodSignature*)methodSignatureForSelector:(SEL)selector {
    NSMethodSignature *signature = [super methodSignatureForSelector:selector];
    if (!signature) {
        signature = [(id)delegate methodSignatureForSelector:selector];
    }
    return signature;
}


@end
