//
//  RTPopoverController.h
//  ProductBuilder
//
//  Created by valery on 12/3/13.
//  Copyright (c) 2013 Cloudworks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (PopoverPresentationController)

@end

@class RTPopoverController;

@protocol RTPopoverControllerDelegate <NSObject>
@optional
- (BOOL)popoverControllerShouldDismissPopover:(nullable RTPopoverController *)popoverController;
- (void)popoverControllerDidDismissPopover:(nullable RTPopoverController *)popoverController;
- (void)popoverController:(nullable RTPopoverController *)popoverController willRepositionPopoverToRect:(nullable inout CGRect *)rect inView:(inout UIView * __nonnull * __nonnull)view;
@end

@interface RTPopoverController : NSObject<UIPopoverPresentationControllerDelegate>{
    UIViewController* _contentViewController;
}
@property (nonatomic, assign) id<RTPopoverControllerDelegate> __nonnull delegate;
@property (nonatomic, retain) UIViewController * __nonnull contentViewController;
@property (nonatomic) CGSize popoverContentSize;
@property (nonatomic, readonly, getter=isPopoverVisible) BOOL popoverVisible;
@property (nonatomic, copy) NSArray<__kindof UIView *> * _Nullable passthroughViews;
-(instancetype _Nullable)initWithContentViewController:(UIViewController* _Nullable)controller;

- (void)presentPopoverFromBarButtonItem:(UIBarButtonItem * _Nullable)item permittedArrowDirections:(UIPopoverArrowDirection)arrowDirections animated:(BOOL)animated;
-(void)presentPopoverFromRect:(CGRect)rect inView:(UIView * _Nullable)view permittedArrowDirections:(UIPopoverArrowDirection)arrowDirections animated:(BOOL)animated;
- (void)dismissPopoverAnimated:(BOOL)animated;
@end



