//
//  UICustomSwitchYesNo.m
//  iPadPOS
//
//  Created by Lulakov Viacheslav on 6/16/11.
//  Copyright 2011 DBBestTechnologies. All rights reserved.
//

#import "UICustomSwitchYesNo.h"

@implementation UICustomSwitchYesNo

-(void)awakeFromNib
{
    [super awakeFromNib];
    self.leftLabel.text = NSLocalizedString(@"YESBTN_TITLE", nil);
    self.rightLabel.text = NSLocalizedString(@"NOBTN_TITLE", nil);
}

@end
