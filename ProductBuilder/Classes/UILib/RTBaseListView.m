//
//  RTBaseListViewController.m
//  ProductBuilder
//
//  Created by Alexander Martyshko on 7/17/15.
//  Copyright (c) 2015 Cloudworks. All rights reserved.
//

#import "RTBaseListView.h"
#import "RTSelectorButton.h"
#import "Global.h"
#import "RTUITableView.h"
#import "UIImage+Arrow.h"
#import "SortOptionsViewController.h"

@interface RTBaseListView () <SortOptionsViewControllerDelegate> {
    UIImageView *_sortStatusImageView;
    RTUnderlinedSelectorButton *_sortButton;
    SortingMode *_currentSort;
    BOOL _defaultsInitialized;
    UIImage *_sortUpArrowImage, *_sortDownArrowImage;
    
    GridTableColumnDescription *_currentSortDescription;
    SortingMode _order;
}

@end

@implementation RTBaseListView

- (void)dealloc {
    [_tableView release];
    [_headerView release];
    [_countLabel release];
    [_sortButton release];
    [_sortStatusImageView release];
    [_sortUpArrowImage release];
    [_sortDownArrowImage release];
    [_sortableColumns release];
    [_currentSortDescription release];
    [super dealloc];
}

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self initDefaults];
    }
    return self;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    [self initDefaults];
}

- (void)initDefaults {
    if (_defaultsInitialized)
        return;
    _defaultsInitialized = YES;
    
    self.backgroundColor = [UIColor clearColor];
    
    _headerHeight = -1;
    _headerView = [[UIRoundedCornersView alloc] init];
    _headerView.backgroundColor = [UIColor whiteColor];
    [self addSubview:_headerView];
    
    _countLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    _countLabel.font = [UIFont systemFontOfSize:17];
    _countLabel.backgroundColor = [UIColor clearColor];
    _countLabel.textColor = [UIColor blackColor];
    [_headerView addSubview:_countLabel];
    
    _sortButton = [[RTUnderlinedSelectorButton alloc] init];
    [_sortButton setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
    [_sortButton addTarget:self action:@selector(sortButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    [_headerView addSubview:_sortButton];
    
    _sortStatusImageView = [[UIImageView alloc] initWithFrame:CGRectZero];
    _sortStatusImageView.backgroundColor = [UIColor clearColor];
    _sortStatusImageView.contentMode = UIViewContentModeScaleAspectFit;
    [_headerView addSubview:_sortStatusImageView];
    
    _tableView = [[RTUITableView alloc] initWithFrame:CGRectZero];
    _tableView.backgroundColor = [UIColor clearColor];
    [_tableView setSeparatorInset:UIEdgeInsetsZero];
    [_tableView setLayoutMargins:UIEdgeInsetsZero];
    _tableView.tintColor=global_blue_color;
    [self addSubview:_tableView];
    
    UIColor *arrowColor = global_blue_color;
    _sortUpArrowImage = [[UIImage arrowImageWithSize:CGSizeMake(16, 10) color:arrowColor direction:ArrowDirectionUp] retain];
    _sortDownArrowImage = [[UIImage arrowImageWithSize:CGSizeMake(16, 10) color:arrowColor direction:ArrowDirectionDown] retain];
    
    _customTableFrame = CGRectNull;
}

-(void)layoutSubviews {
    [super layoutSubviews];
    
    NSInteger headerHeight = _headerHeight;
    if (headerHeight < 0)
        headerHeight = 44;
    
    NSInteger offsetBetweenHeaderAndTable = headerHeight > 0 ? 4 : 0;
    _headerView.frame = CGRectMake(0, 0, self.bounds.size.width, headerHeight);
    CGSize sortButtonTextSize = _sortButton.titleLabel.attributedText ? _sortButton.titleLabel.attributedText.size : CGSizeZero;
    _sortButton.frame = CGRectMake(4, _headerView.bounds.size.height - 30, sortButtonTextSize.width, 30);
    _sortStatusImageView.frame = CGRectMake(_sortButton.frame.origin.x + sortButtonTextSize.width + 2, _sortButton.frame.origin.y + 2,
                                          12, _sortButton.frame.size.height);
    _countLabel.frame = CGRectMake(_sortButton.frame.origin.x, _sortButton.frame.origin.y - 21, self.bounds.size.width/2, 21);
    
    NSInteger tableY = _headerView.frame.origin.y + _headerView.frame.size.height + offsetBetweenHeaderAndTable;
    _tableView.frame = CGRectIsNull(_customTableFrame) ? CGRectMake(0, tableY, self.bounds.size.width, self.bounds.size.height - tableY) : _customTableFrame;
}

- (void)setTableViewDelegateAndDataSource:(id<UITableViewDataSource,UITableViewDelegate>)tableViewDelegateAndDataSource {
    _tableView.delegate = tableViewDelegateAndDataSource;
    _tableView.dataSource = tableViewDelegateAndDataSource;
}

- (void)sortButtonClick:(id)sender {
    if (_sortableColumns.count) {
        SortOptionsViewController *sortOptionsController = [[SortOptionsViewController alloc] initWithAvailableSortDescriptions:_sortableColumns
                                                                                                         currentSortDescription:_currentSortDescription
                                                                                                                          order:_order];
        sortOptionsController.sortDelegate = self;
        RTDialogViewController* dialogViewController = [[RTDialogViewController alloc]initWithContentViewController:
                                                        sortOptionsController];
        [dialogViewController showModal:YES delegate:nil];
        [dialogViewController release];
        [sortOptionsController release];
    }
}

- (void)updateSortControls {
    [_sortButton setTitle:_currentSortDescription.title ? _currentSortDescription.title : @"" forState:UIControlStateNormal];
    
    CGSize sortButtonTextSize = _sortButton.titleLabel.attributedText ? _sortButton.titleLabel.attributedText.size : CGSizeZero;
    CGRect rect = _sortButton.frame;
    rect.size.width = sortButtonTextSize.width;
    _sortButton.frame = rect;
    
    _sortStatusImageView.frame = CGRectMake(_sortButton.frame.origin.x + _sortButton.frame.size.width + 2, _sortButton.frame.origin.y + 2,
                                            12, _sortButton.frame.size.height);
    
    CGFloat minX = 10000;
    for (UIView *view in _sortStatusImageView.superview.subviews) {
        if (view != _sortStatusImageView && view != _sortButton && (CGRectIntersectsRect(view.frame, _sortStatusImageView.frame) || CGRectIntersectsRect(view.frame, _sortButton.frame))) {
            if (view.frame.origin.x < minX) {
                minX = view.frame.origin.x;
            }
        }
    }
    if (minX < 10000) {
        _sortStatusImageView.frame = CGRectMake(minX-4-_sortStatusImageView.frame.size.width, _sortStatusImageView.frame.origin.y,
                                                _sortStatusImageView.frame.size.width, _sortStatusImageView.frame.size.height);
        rect = _sortButton.frame;
        rect.size.width = _sortStatusImageView.frame.origin.x - 2 - _sortButton.frame.origin.x;
        _sortButton.frame = rect;
    }
    
    switch (_order) {
        case NotSorted:
            _sortStatusImageView.image = nil;
            break;
        case AscendingSorted:
            _sortStatusImageView.image = _sortUpArrowImage;
            break;
        case DescendingSorted:
            _sortStatusImageView.image = _sortDownArrowImage;
            break;
    }
}

- (void)setSortableColumns:(NSArray *)sortableColumns {
    [sortableColumns retain];
    [_sortableColumns release];
    _sortableColumns = sortableColumns;
}

- (void)setCurrentSortDescription:(GridTableColumnDescription *)columnDescription order:(SortingMode)order {
    [columnDescription retain];
    [_currentSortDescription release];
    _currentSortDescription = columnDescription;
    
    _order = order;
    
    [self updateSortControls];
    
    if (_delegate && [_delegate respondsToSelector:@selector(baseList:sortWithColumn:order:)])
        [_delegate baseList:self sortWithColumn:_currentSortDescription order:_order];
}

- (GridTableColumnDescription *)currentSortDescription {
    return _currentSortDescription;
}

- (SortingMode)currentSortOrder {
    return _order;
}

- (void)setCountAndSortLabelsHidden:(BOOL)hidden {
    _countLabel.hidden = _sortButton.hidden = _sortStatusImageView.hidden = hidden;
}

#pragma mark - SortOptionsViewControllerDelegate
-(void)sortOptionsController:(SortOptionsViewController *)controller applySortWithDescription:(GridTableColumnDescription *)sortDescription order:(SortingMode)order {
    [self setCurrentSortDescription:sortDescription order:order];
//    [_tableView reloadData];
}

@end
