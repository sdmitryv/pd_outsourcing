/*
 Erica Sadun, http://ericasadun.com
 iPhone Developer's Cookbook, 3.0 Edition
 BSD License, Use at your own risk
 */

/*
 Thanks to Kevin Ballard for suggesting the UITextField as subview approach
 All credit to Kenny TM. Mistakes are mine. 
 To Do: Ensure that only one runs at a time -- is that possible?
 */

#import "ModalAlert.h"
#import "RTAlertView.h"
#import "NSError+Format.h"
#import "Product_Builder-Swift.h"

@implementation ModalAlert

+ (void) askWithTitle:(NSString*)title question:(NSString *)question withCancel: (NSString *) cancelButtonTitle withButtons: (NSArray *) buttons completed:(void(^)(NSInteger))completed{
    RTAlertView50 *alertView = [[RTAlertView50 alloc] initWithTitle:title message:question delegate:nil cancelButtonTitle:cancelButtonTitle otherButtonTitles:nil];
    alertView.completedBlock = completed;
    for (NSString *buttonTitle in buttons) [alertView addButtonWithTitle:buttonTitle];
    [alertView show];
    [alertView release];
}

+ (void) ask: (NSString *) question withCancel: (NSString *) cancelButtonTitle withButtons: (NSArray *) buttons
completed:(void(^)(NSInteger))completed
{
	RTAlertView50 *alertView = [[RTAlertView50 alloc] initWithTitle:question message:nil delegate:nil cancelButtonTitle:cancelButtonTitle otherButtonTitles:nil];
    alertView.completedBlock = completed;
	for (NSString *buttonTitle in buttons) [alertView addButtonWithTitle:buttonTitle];
	[alertView show];
	[alertView release];
}

+ (void) say: (id)formatstring,...
{
	va_list arglist;
	va_start(arglist, formatstring);
	id statement = [[NSString alloc] initWithFormat:formatstring arguments:arglist];
	va_end(arglist);
	[ModalAlert ask:statement withCancel:NSLocalizedString(@"OKAYBTN_TITLE", nil) withButtons:nil completed:nil];
	[statement release];
}

+(void) sayWithTitle:(NSString*)title message:(NSString*)message {
    
    [ModalAlert sayWithTitle:title message:message cancelButtonTitle:NSLocalizedString(@"OKAYBTN_TITLE", nil)];
}

+(void) sayWithTitle:(NSString*)title message:(NSString*)message cancelButtonTitle:(NSString*)cancelButtonTitle {
    
    if ([title length] > 0) {
        
        RTAlertView *alertView = [[RTAlertView alloc] initWithTitle:title message: message delegate:nil cancelButtonTitle:cancelButtonTitle otherButtonTitles:nil];
        [alertView show];
        [alertView release];
    }
    else
        [ModalAlert say:message];
}

+ (void) ask: (NSString*)question completed:(void(^)(NSInteger))completed
{
	[ModalAlert ask:question withCancel:nil withButtons:@[NSLocalizedString(@"YESBTN_TITLE", nil), NSLocalizedString(@"NOBTN_TITLE", nil)] completed:completed];
}

+ (void) show:(NSString*)msg{
    [self show:msg completed:nil];
}

+ (void) show:(NSString*)msg completed:(dispatch_block_t)completed{
    RTAlertView50 *alertView = [[RTAlertView50 alloc] initWithTitle:msg message:nil delegate:nil cancelButtonTitle:NSLocalizedString(@"OKAYBTN_TITLE", nil) otherButtonTitles:nil];
    if (completed){
        alertView.completedBlock = ^(NSInteger index){
            completed();
        };
    }
	[alertView show];
    [alertView release];
}

+ (void) show:(NSString*)title message:(NSString*)message{
    [self show:title message:message completed:nil];
}

+ (void) show:(NSString*)title message:(NSString*)message completed:(dispatch_block_t)completed{
    RTAlertView50 *alertView = [[RTAlertView50 alloc] initWithTitle:title message:message delegate:nil cancelButtonTitle:NSLocalizedString(@"OKAYBTN_TITLE", nil) otherButtonTitles:nil];
    if (completed){
        alertView.completedBlock = ^(NSInteger index){
            completed();
        };
    }
	[alertView show];
    [alertView release];
}

+(void)parseError:(NSError*)error title:(NSString**)aTitle description:(NSString**)aDescription{
    if (!error || (!aTitle && !aDescription)) return;
    NSString* title = error.localizedDescription;
    NSString* description = error.localizedFailureReason;
    
    if (title && description){
        NSString* helpAnchor = error.helpAnchor;
        if (helpAnchor){
            description = [NSString stringWithFormat:@"%@\n[%@]", description, helpAnchor];
        }
    }
    else{
        title = NSLocalizedString(@"ERROR_TEXT", nil);
        description = error ? [error descriptionExtended] : NSLocalizedString(@"UNKNOWNERROR_TEXT", nil);
    }
    if (aTitle){
        *aTitle = [[title retain]autorelease];
    }
    if (aDescription){
        *aDescription = [[description retain]autorelease];
    }
}

+ (void) showError:(NSError*)error completedWithOptions:(void(^)(NSInteger))completed{
    NSString* title = nil;
    NSString* description = nil;
    [self parseError:error title:&title description:&description];

    RTAlertView50 *alertView = [[RTAlertView50 alloc] initWithTitle:title message:description delegate:nil cancelButtonTitle:NSLocalizedString(@"OKAYBTN_TITLE", nil) otherButtonTitles:NSLocalizedString(@"CANCELBTN_TITLE", nil)];
    [alertView show];
    alertView.completedBlock = completed;
    [alertView release];
}

+ (void) showError:(NSError*)error completed:(dispatch_block_t)completed{
    NSString* title = nil;
    NSString* description = nil;
    [self parseError:error title:&title description:&description];
    [self show:title message:description completed:completed];
}

+ (void) showError:(NSError*)error{
    [self showError:error completed:nil];
}

+ (void) showErrorMessage:(NSString*)message{
	[self showErrorMessage:message completed:nil];
}

+ (void) showErrorMessage:(NSString*)message completed:(dispatch_block_t)completed{
	RTAlertView50 *alertView = [[RTAlertView50 alloc] initWithTitle:NSLocalizedString(@"ERROR_TEXT", nil) message: message ?: NSLocalizedString(@"UNKNOWNERROR_TEXT", nil) delegate:nil cancelButtonTitle:NSLocalizedString(@"OKAYBTN_TITLE", nil) otherButtonTitles:nil];
    if (completed){
        alertView.completedBlock = ^(NSInteger index){
            completed();
        };
    }
    [alertView show];
    [alertView release];
}

@end

