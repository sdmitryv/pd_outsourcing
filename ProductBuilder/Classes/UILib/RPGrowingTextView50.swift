//
//  RPGrowingTextView.swift
//  RPlus
//
//  Created by Alexander Martyshko on 11/30/16.
//  Copyright © 2016 Cloudworks. All rights reserved.
//

import UIKit

protocol RPGrowingTextViewHeightDelegate: AnyObject {
    func heightDidChange(_ textView: RPGrowingTextView)
    func didLayoutSubviews(_ textView: RPGrowingTextView)
}

class RPGrowingTextView: RPDynamicFontTextView {
    
    weak var heightDelegate: RPGrowingTextViewHeightDelegate?
    
    fileprivate var textLength = 0
    
    fileprivate var _placeholderString: String?
    var placeholderString: String? {
        get {
            return _placeholderString
        }
        set {
            _placeholderString = newValue
            self.setNeedsDisplay()
        }
    }
    
    override var text: String! {
        get {
            return super.text
        }
        set {
            super.text = newValue
            updateLayout()
        }
    }
    
    override var attributedText: NSAttributedString! {
        get {
            return super.attributedText
        }
        set {
            super.attributedText = newValue
            updateLayout()
        }
    }
    
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        
        guard let placeholder = _placeholderString else {
            return
        }
        
        if self.text.characters.count == 0 && self.attributedText.string.characters.count == 0 {
            let textPoint = CGPoint(x: 5, y: self.textContainerInset.top)
            
            let font = self.font ?? UIFont.systemFont(ofSize: 17)
            (placeholder as NSString).draw(at: textPoint,
                                           withAttributes: [NSFontAttributeName : font, NSForegroundColorAttributeName : ColorUtils.ColorWithRGB(red: 199, green: 199, blue: 205)])
        }
    }
 
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }

    override init(frame: CGRect, textContainer: NSTextContainer?) {
        super.init(frame: frame, textContainer: textContainer)
        
        initDefaults()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        initDefaults()
    }
    
    fileprivate func initDefaults() {
        NotificationCenter.default.addObserver(self, selector: #selector(RPGrowingTextView.textDidChange), name: NSNotification.Name.UITextViewTextDidChange, object: self)
    }
    
    @objc fileprivate func textDidChange() {
        updateLayout()
    }
    
    fileprivate func updateLayout() {
        invalidateIntrinsicContentSize()
        if self.text.characters.count != textLength {
            self.setNeedsDisplay()
        }
        textLength = self.text.characters.count
    }
    
    fileprivate var _calculatedHeight: CGFloat = 0
    override var intrinsicContentSize: CGSize {
        let textRect = self.layoutManager.usedRect(for: self.textContainer)
        let height = textRect.size.height + self.textContainerInset.top + self.textContainerInset.bottom
        
        if _calculatedHeight != height {
            heightDelegate?.heightDidChange(self)
        }
        _calculatedHeight = height
        
        return CGSize(width: UIViewNoIntrinsicMetric, height: height)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        if floor(_calculatedHeight) <= floor(self.frame.size.height) {
            self.scrollRangeToVisible(self.selectedRange)
        }
        
        heightDelegate?.didLayoutSubviews(self)
    }
}
