//
//  BaseSearchTableViewCell.m
//  ProductBuilder
//
//  Created by Alexander Martyshko on 7/9/15.
//  Copyright (c) 2015 Cloudworks. All rights reserved.
//

#import "BaseSearchTableViewCell.h"
#import "RTUITableView.h"
#import "Global.h"

@interface BaseSearchTableViewCell()
@end


@implementation BaseSearchTableViewCell

- (void)dealloc {
    [super dealloc];
}

- (void)awakeFromNib {
    [super awakeFromNib];
    [self initDefaults];
}

- (id)init {
    self = [super init];
    if (self) {
        [self initDefaults];
    }
    return self;
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self initDefaults];
    }
    return self;
}


- (void)initDefaults {
//    self.backgroundView = [[[RTUITableCellBackView alloc] initWithFrame:self.contentView.bounds] autorelease];
    UIView *selectedBackView = [[[UIView alloc] initWithFrame:self.contentView.bounds] autorelease];
    selectedBackView.backgroundColor = global_blue_color;
    self.selectedBackgroundView = selectedBackView;
    self.textLabel.backgroundColor = [UIColor clearColor];
    self.contentView.backgroundColor = [UIColor whiteColor];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    // Configure the view for the selected state
}

/*- (UIEdgeInsets)layoutMargins
{
    return UIEdgeInsetsZero;
}*/

-(void)displayObject:(NSObject *)object currentSearchText:(NSString *)currentSearchText highlightColor:(UIColor *)highlightColor {
    if (object) 
        [self assignText:[object description] forLabel:self.textLabel searchText:currentSearchText color:highlightColor];
    else
        self.textLabel.text = nil;
}


- (void)assignText:(NSString *)str forLabel:(UILabel *)label searchText:(NSString *)searchText color:(UIColor *)color {
    
    NSMutableAttributedString* coloredText = [str stringByHighlightingString:searchText
                                                                  normalFont:[UIFont systemFontOfSize:label.font.pointSize]
                                                               highlightFont:[UIFont boldSystemFontOfSize:label.font.pointSize]
                                                                       color:global_blue_color];
    
    if (!coloredText) {
        if (str)
            coloredText = [[[NSMutableAttributedString alloc] initWithString:str] autorelease];
        else
            coloredText = [[[NSMutableAttributedString alloc] initWithString:@""] autorelease];
    }
    label.attributedText = coloredText;
}

@end
