//
//  UITextField+Input.m
//  StockCount
//
//  Created by Valera on 12/2/10.
//  Copyright 2010 DBBest Technologies. All rights reserved.
//

#import "UITextField+Input.h"
#import "UITextFieldAlphanumericHandler.h"
#import "UITextFieldConstraintHandler.h"
#import <objc/runtime.h>
#import <QuartzCore/QuartzCore.h>
#import "UITextFieldNumeric.h"


@implementation UITextViewReadOnlyDelegate

-(id)initWithUITextViewDelegate:(id<UITextViewDelegate>)textViewDelegate{
    if ((self=[super init])){
        originDelegate = textViewDelegate;
    }
    return self;
}

-(id<UITextViewDelegate>)originDelegate{
    return originDelegate;
}
-(void)setOriginDelegate:(id<UITextViewDelegate>)delegate{
    originDelegate = delegate;
}

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView{
    return TRUE;
}

- (BOOL)textViewShouldEndEditing:(UITextView *)textView{
    return TRUE;
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    return NO;
}

-(void)forwardInvocation:(NSInvocation *)anInvocation{
    if ([originDelegate respondsToSelector:[anInvocation selector]])
        [anInvocation invokeWithTarget:originDelegate];
    else
        [super forwardInvocation:anInvocation];
}

-(NSMethodSignature*)methodSignatureForSelector:(SEL)selector {
    NSMethodSignature *signature = [super methodSignatureForSelector:selector];
    if (!signature) {
        signature = [(id)originDelegate methodSignatureForSelector:selector];
    }
    return signature;
}

@end

@implementation UITextFieldReadOnlyDelegate

-(id)initWithUITextFieldDelegate:(id<UITextFieldDelegate>)textFieldDelegate{
    if ((self=[super init])){
        originDelegate = textFieldDelegate;
    }
    return self;
}

-(id<UITextFieldDelegate>)originDelegate{
    return originDelegate;
}

-(void)setOriginDelegate:(id<UITextFieldDelegate>)delegate{
    originDelegate = delegate;
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    return TRUE;
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField{
    return TRUE;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    return NO;
}

- (BOOL)textFieldShouldClear:(UITextField *)textField{
    return NO;
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    if (originDelegate && [originDelegate respondsToSelector:@selector(textFieldShouldReturn:)]){
        return [originDelegate textFieldShouldReturn:textField];
    }
    return YES;
}

-(void)forwardInvocation:(NSInvocation *)anInvocation{
    if ([originDelegate respondsToSelector:[anInvocation selector]])
        [anInvocation invokeWithTarget:originDelegate];
    else
        [super forwardInvocation:anInvocation];
}

-(NSMethodSignature*)methodSignatureForSelector:(SEL)selector {
    NSMethodSignature *signature = [super methodSignatureForSelector:selector];
    if (!signature) {
        signature = [(id)originDelegate methodSignatureForSelector:selector];
    }
    return signature;
}

@end

const char *readOnlyDelegateTagKey = "readOnlyDelegateTag";
const char *readOnlyChangeColorDelegateTagKey = "readOnlyChangeColorDelegateTag";
const char *numericHandlerDelegateTagKey = "numericHandlerDelegateTag";
const char *aplhaNumericHandlerDelegateTagKey = "aplhaNumericHandlerDelegateTag";
const char *setReadOnlyInProcessTagKey = "setReadOnlyInProcessTag";
const char *oldKeyboardVisibleTagKey = "oldKeyboardVisibleTag";
const char *highlightedPropTagKey = "highlightedPropTag";
const char *highlightedTextColorPropTagKey = "highlightedTextColorPropTag";
const char *textColorPropTagKey = "textColorPropTagKeyPropTag";
const char *inputViewTagKey = "inputViewTag";
const char *numpadKeyboardContentTagKey = "numpadKeyboardContentTag";
const char *oldBackgroundColorTagKey = "oldBackgroundColorTag";
const char *errorTagKey = "errorTag";
const char *internalChangeTagKey = "internalChangeTag";
const char *fetchDataKey = "fetchDataTag";

@implementation UITextField (Additions)

-(NSError*)error{
    return objc_getAssociatedObject(self, errorTagKey);
}

-(void)setError:(NSError *)error{
    objc_setAssociatedObject(self, errorTagKey, error, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

-(NSInteger)internalChange{
    return [objc_getAssociatedObject(self, internalChangeTagKey) integerValue];
}

-(void)setInternalChange:(NSInteger)value{
    objc_setAssociatedObject(self, internalChangeTagKey, value ? @(value) : nil, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

-(void)setFetchDataBlock:(void (^)(id sender))fetchDataBlock{
    objc_setAssociatedObject(self, fetchDataKey, fetchDataBlock, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

-(void (^)(id))fetchDataBlock{
    return objc_getAssociatedObject(self, fetchDataKey);
}

-(void)fetchData{
    if (self.fetchDataBlock){
        self.fetchDataBlock(self);
    }
}
@end

@interface UIResponderHelper : NSObject
+(void)exchangeMethod:(SEL)origSel withNewMethod:(SEL)newSel class:(Class)class;
+(void)handleKeyboard:(BOOL)hide responder:(id)responder;
+(void)setKeyboardVisible:(BOOL)value responder:(id)responder;
+(BOOL)isKeyboardVisible:(id)responder;
+(BOOL)readOnly:(id)responder;
+(void)setReadonly:(BOOL)value responder:(id)responder;
@end

@implementation UIResponderHelper

+(void)exchangeMethod:(SEL)origSel withNewMethod:(SEL)newSel class:(Class)class{
    Method origMethod = class_getInstanceMethod(class, origSel);
    Method newMethod = class_getInstanceMethod(class, newSel);
    method_exchangeImplementations(origMethod, newMethod);
}

+(void)handleKeyboard:(BOOL)hide responder:(id)responder{
    BOOL kbWasVisible = [self isKeyboardVisible:responder];
    if ((!kbWasVisible && hide) || (kbWasVisible && !hide)) return;
    if (hide){
        UIView* inputView = [responder inputView];
        if (inputView && inputView.tag!=9){
            objc_setAssociatedObject(responder, inputViewTagKey, inputView, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
        }
        UIView* dummy = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 0, 0.1)];
        dummy.tag = 9;
        [responder setInputView:dummy];
        [dummy release];
    }
    else {
        UIView* inputView = [responder inputView];
        if (inputView && inputView.tag==9){
            UIView* originalInputView = objc_getAssociatedObject(responder, inputViewTagKey);
            [responder setInputView:originalInputView];
        }
        objc_setAssociatedObject(responder, inputViewTagKey, nil, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    }
    if ([responder isFirstResponder]){
        [self responder:responder reloadInputViewsFromKeyboardState:kbWasVisible];
    }
}

+(void)responder:(id)responder reloadInputViewsFromKeyboardState:(BOOL)kbWasVisible{
    if (![responder isFirstResponder]) return;
    if ([responder window] && ![[responder window] isKeyWindow]){
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.1 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            [self responder:responder reloadInputViewsFromKeyboardState:kbWasVisible];
        });
        return;
    }
    //hide keyboard with animation
    if (kbWasVisible){
        [[NSNotificationCenter defaultCenter]postNotificationName:UIKeyboardWillHideNotification object:nil];
        [UIView animateWithDuration:0.3f delay:0.0f options:UIViewAnimationOptionBeginFromCurrentState | UIViewAnimationOptionAllowUserInteraction animations:^{
            [responder reloadInputViews];
            
        } completion:^(BOOL finished) {
            [[NSNotificationCenter defaultCenter]postNotificationName:UIKeyboardDidHideNotification object:nil];
        }];
    }
    else{
        //try to show keyboard with animation
        [UIView animateWithDuration:0.3f delay:0.0f options:UIViewAnimationOptionBeginFromCurrentState animations:^{
            [responder reloadInputViews];
        } completion:nil];
        [[NSNotificationCenter defaultCenter]postNotificationName:UITextFieldTextDidBeginEditingNotification object:responder];
    }
}

+(void)setKeyboardVisible:(BOOL)value responder:(id)responder{
    if (value && [responder isKindOfClass:[UITextFieldNumeric class]] && ((UITextFieldNumeric*)responder).useVirtualNumericKeyboard){
        return;
    }
    BOOL visible = [self isKeyboardVisible:responder];
    if (visible==value)return;
    [self handleKeyboard:!value responder:responder];
}

+(BOOL)isKeyboardVisible:(id)responder{
	return !([responder inputView] && [[responder inputView] tag]==9);
}

+(BOOL)readOnly:(id)responder{
    return objc_getAssociatedObject(responder, readOnlyDelegateTagKey)!=nil;
}

+(id)getReadonlyDelegate:(id)object{
    if ([object isKindOfClass:[UITextField class]]){
        return [[[UITextFieldReadOnlyDelegate alloc]initWithUITextFieldDelegate:[(UITextField*)object delegate]] autorelease];
    }
    if ([object isKindOfClass:[UITextView class]]){
        return [[[UITextViewReadOnlyDelegate alloc]initWithUITextViewDelegate:[(UITextView*)object delegate]] autorelease];
    }
    return nil;
}

+(void)setReadonly:(BOOL)value responder:(id)responder{
    if (objc_getAssociatedObject(responder, setReadOnlyInProcessTagKey)){
        return;
    }
    NSObject* setReadOnlyInProcessObject = [[NSObject alloc]init];
    objc_setAssociatedObject(responder, setReadOnlyInProcessTagKey, setReadOnlyInProcessObject, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    [setReadOnlyInProcessObject release];
    
    BOOL notNeedChangeColor = objc_getAssociatedObject(responder, readOnlyChangeColorDelegateTagKey) != nil;
    
    UITextFieldReadOnlyDelegate* readOnlyDelegate = objc_getAssociatedObject(responder, readOnlyDelegateTagKey);
    BOOL readOnly = readOnlyDelegate!=nil;
    if (readOnly!=value){
        readOnly = value;
        if (readOnly){
            if ([responder isFirstResponder]){
                if ([responder isKindOfClass:[UITextField class]] && [[responder delegate] respondsToSelector:@selector(textFieldDidEndEditing:)]){
                    [[(UITextField*)responder delegate] textFieldDidEndEditing:responder];
                }
                else if ([responder isKindOfClass:[UITextView class]] && [[responder delegate] respondsToSelector:@selector(textViewDidEndEditing:)]){
                    [[(UITextView*)responder delegate] textViewDidEndEditing:responder];
                }
            }
            id aReadOnlyDelegate = [self getReadonlyDelegate:responder];
            [responder setDelegate:aReadOnlyDelegate];
            objc_setAssociatedObject(responder, readOnlyDelegateTagKey, aReadOnlyDelegate, OBJC_ASSOCIATION_RETAIN_NONATOMIC);

            if (!notNeedChangeColor) {
                
                if (![[responder superview] isKindOfClass:[UISearchBar class]] && ![[[responder superview]superview] isKindOfClass:[UISearchBar class]]){
                    
                    objc_setAssociatedObject(responder, oldBackgroundColorTagKey, (id)[responder backgroundColor], OBJC_ASSOCIATION_RETAIN_NONATOMIC);
                    [responder setBackgroundColor:[UIColor colorWithWhite:0.85f alpha:1.0]];
                }
                else
                    [responder setAlpha:0.85f];
            }
            
            objc_setAssociatedObject(responder, oldKeyboardVisibleTagKey, @([self isKeyboardVisible:responder]), OBJC_ASSOCIATION_RETAIN_NONATOMIC);
            [self setKeyboardVisible:FALSE responder:responder];
        }
        else{
            
            id oldDelegate = readOnlyDelegate.originDelegate;
            objc_setAssociatedObject(responder, readOnlyDelegateTagKey, nil, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
            [responder setDelegate:oldDelegate];
            //remove shadow
            
            if (!notNeedChangeColor) {
                
                if (![[responder superview] isKindOfClass:[UISearchBar class]] && ![[[responder superview]superview] isKindOfClass:[UISearchBar class]]){
                    
                    UIColor* oldBackgroundColor = objc_getAssociatedObject(responder, oldBackgroundColorTagKey);
                    [responder setBackgroundColor:oldBackgroundColor];
                    objc_setAssociatedObject(responder, oldBackgroundColorTagKey, nil, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
                }
                else
                    [responder setAlpha:1.0f];
            }
            
            NSNumber* oldKeyboardVisible = objc_getAssociatedObject(responder, oldKeyboardVisibleTagKey);
            BOOL oldKBVisible = TRUE;
            if (oldKeyboardVisible){
                oldKBVisible = [oldKeyboardVisible boolValue];
            }
            [self setKeyboardVisible:oldKBVisible responder:responder];
            if ([responder isFirstResponder]){
                if ([responder isKindOfClass:[UITextFieldNumeric class]] && ((UITextFieldNumeric*)responder).useVirtualNumericKeyboard){
                    if ([((UITextFieldNumeric*)responder).internalDelegate respondsToSelector:@selector(textFieldShouldBeginEditing:)]){
                        [((UITextFieldNumeric*)responder).internalDelegate textFieldShouldBeginEditing:responder];
                    }
                }
            }
        }
    }
    objc_setAssociatedObject(responder, setReadOnlyInProcessTagKey, nil, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

@end


@implementation UITextView (Input)

+(void)load{
    Class class = [self class];
    [UIResponderHelper exchangeMethod: @selector(canPerformAction:withSender:) withNewMethod:@selector(canPerformActionHackTextView:withSender:) class:class];
    //[UIResponderHelper exchangeMethod: @selector(setDelegate:) withNewMethod:@selector(setDelegateHack:) class:[self class]];
}

-(void)setKeyboardVisible:(BOOL)value{
    return [UIResponderHelper setKeyboardVisible:value responder:self];
}

-(BOOL)isKeyboardVisible{
    return [UIResponderHelper isKeyboardVisible:self];
}

-(BOOL)readOnly{
    return [UIResponderHelper readOnly:self];
}

-(void)setReadOnly:(BOOL)value{
    return [UIResponderHelper setReadonly:value responder:self];
}

-(void)setDelegateHack:(id<UITextViewDelegate>)delegate{
    if (self.readOnly){
        if (![delegate isKindOfClass:[UITextViewReadOnlyDelegate class]]){
            UITextViewReadOnlyDelegate* readOnlyDelegate = objc_getAssociatedObject(self, readOnlyDelegateTagKey);
            readOnlyDelegate.originDelegate = delegate;
            return;
        }
    }
    [self setDelegateHack:delegate];
}

const char* pasteNotAllowedKeyTextView = "TetxViewPasteNotAllowed";

-(BOOL)pasteNotAllowed {
    NSNumber* val = objc_getAssociatedObject(self, pasteNotAllowedKeyTextView);
    
    if (val) {
        return val.boolValue;
    }
    else {
        return FALSE;
    }
}

-(void)setPasteNotAllowed:(BOOL)pasteNotAllowed {
    objc_setAssociatedObject(self, pasteNotAllowedKeyTextView, @(pasteNotAllowed), OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}


- (BOOL)canPerformActionHackTextView:(SEL)action withSender:(id)sender{

    if (!(action==@selector(cut:) || action==@selector(paste:) || action==@selector(copy:) || action==@selector(select:) || action==@selector(selectAll:) || action==@selector(delete:))){
        return NO;
    }

    if ([self readOnly]){
        if (action==@selector(copy:) || action==@selector(select:) || action==@selector(selectAll:))
            return [self canPerformActionHackTextView:action withSender:sender];
        return NO;
    }
    else if ([self pasteNotAllowed]) {
        if (action==@selector(paste:)) {
            return NO;
        }
    }
    // This line at runtime does not go into an infinite loop
    // because it will call the real method instead of ours.
    
    BOOL res = [self canPerformActionHackTextView:action withSender:sender];
    return res;
}


@end

@implementation UITextField (Input)

@dynamic readOnly;

-(void)handleKeyboard:(BOOL)hide{
    [UIResponderHelper handleKeyboard:hide responder:self];
}

-(void)setKeyboardVisible:(BOOL)value{
    [UIResponderHelper setKeyboardVisible:value responder:self];
}

-(BOOL)isKeyboardVisible{
	return [UIResponderHelper isKeyboardVisible:self];
}

+(void)load{
    Class class = [self class];
    [UIResponderHelper exchangeMethod: @selector(canPerformAction:withSender:) withNewMethod:@selector(canPerformActionHack:withSender:) class:class];
    [UIResponderHelper exchangeMethod: @selector(selectAll:) withNewMethod:@selector(selectAllHack:) class:class];
    [UIResponderHelper exchangeMethod: @selector(clearButtonMode) withNewMethod:@selector(clearButtonModeHack) class:class];
    [UIResponderHelper exchangeMethod: @selector(setDelegate:) withNewMethod:@selector(setDelegateHack:) class:class];
    [UIResponderHelper exchangeMethod: @selector(initWithFrame:) withNewMethod:@selector(initWithFrameHack:) class:class];
    [UIResponderHelper exchangeMethod: @selector(initWithCoder:) withNewMethod:@selector(initWithCoderHack:) class:class];
    [UIResponderHelper exchangeMethod: @selector(becomeFirstResponder) withNewMethod:@selector(becomeFirstResponderHack) class:class];
}

-(id)initWithCoderHack:(NSCoder *)aDecoder{
    id result = [self initWithCoderHack:aDecoder];
    [self initDefaults];
    return result;
}

-(id)initWithFrameHack:(CGRect)frame{
    id result = [self initWithFrameHack:frame];
    [self initDefaults];
    return result;
}

-(void)initDefaults{
    self.autocorrectionType = UITextAutocorrectionTypeNo;
    [self addTarget:self action:@selector(editingDidEnd:) forControlEvents:UIControlEventEditingDidEnd];

if (SYSTEM_VERSION_GREATER_THAN(@"9"))
    if ([self respondsToSelector:@selector(inputAssistantItem)]) {
#ifdef __IPHONE_9_0
        UITextInputAssistantItem *inputAssistantItem = [self inputAssistantItem];
        inputAssistantItem.leadingBarButtonGroups = @[];
        inputAssistantItem.trailingBarButtonGroups = @[];
#else
        NSObject* inputAssistantItem = [self performSelector:@selector(inputAssistantItem) withObject:nil];
        if ([inputAssistantItem respondsToSelector:@selector(setLeadingBarButtonGroups:)]){
            [inputAssistantItem performSelector:@selector(setLeadingBarButtonGroups:) withObject:@[]];
        }
        if ([inputAssistantItem respondsToSelector:@selector(setTrailingBarButtonGroups:)]){
            [inputAssistantItem performSelector:@selector(setTrailingBarButtonGroups:) withObject:@[]];
        }
#endif
    }
}

-(BOOL)becomeFirstResponderHack{
    if (self.window && !self.window.isKeyWindow){
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.1 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            [self becomeFirstResponder];
        });
        return YES;
    }
    else{
        return [self becomeFirstResponderHack];
    }
}

-(void)editingDidEnd:(id)sender{
    [NSObject cancelPreviousPerformRequestsWithTarget:self];
}

-(UITextFieldViewMode)clearButtonModeHack{
    if (self.readOnly){
        return UITextFieldViewModeNever;
    }
    return [self clearButtonModeHack];
}

const char* pasteNotAllowedKey = "TetxFieldPasteNotAllowed";

-(BOOL)pasteNotAllowed {
    NSNumber* val = objc_getAssociatedObject(self, pasteNotAllowedKey);
    
    if (val) {
        return val.boolValue;
    }
    else {
        return FALSE;
    }
}

-(void)setPasteNotAllowed:(BOOL)pasteNotAllowed {
    objc_setAssociatedObject(self, pasteNotAllowedKey, @(pasteNotAllowed), OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (BOOL)canPerformActionHack:(SEL)action withSender:(id)sender{
    if ([self readOnly]){
        if (action==@selector(copy:) || action==@selector(select:) || action==@selector(selectAll:))
            return [self canPerformActionHack:action withSender:sender];
        return NO;
    }
    else if ([self pasteNotAllowed]) {
        if (action==@selector(paste:)) {
            return NO;
        }
    }
    // This line at runtime does not go into an infinite loop
    // because it will call the real method instead of ours.
    
    
    if (action==@selector(_share:))
        return NO;
    
    BOOL res = [self canPerformActionHack:action withSender:sender];
    return res;
}

-(void)selectAllHack:(id)sender{
    if (!self.text.length || !self.superview){
        //do nothing
        return;
    }
    [self selectAllHack:sender];
    [self setNeedsDisplay];
}

-(void)selectAllViaRange{
    UITextPosition* startPosition = self.beginningOfDocument;
    if ([self isKindOfClass:[UITextFieldNumeric class]]) {
        UITextFieldNumeric* numbericField = (UITextFieldNumeric*)self;
        if (numbericField.style == NSTextFormatterCurrencyStyle && numbericField.useSmartAmount) {
            if ([self.text hasPrefix:@"-"]) {
                startPosition = [self positionFromPosition:startPosition offset:1];
            }
        }
    }
    
    UITextRange *newRange = [self textRangeFromPosition:startPosition toPosition:self.endOfDocument];
    [self setSelectedTextRange:newRange];
}

-(void)selectAllNoMenu{
    if (![self isFirstResponder] || !self.text.length) return;
    [self performSelector:@selector(selectAllViaRange) withObject:nil afterDelay:0];
}

-(BOOL)readOnly{
    return [UIResponderHelper readOnly:self];
}

-(BOOL)notChangeColorForReadOnly {
    
    return objc_getAssociatedObject(self,
                                    readOnlyChangeColorDelegateTagKey) != nil;
}

-(void)setNotChangeColorForReadOnly:(BOOL)value {
    
    objc_setAssociatedObject(self,
                             readOnlyChangeColorDelegateTagKey,
                             value ? [[[NSObject alloc] init] autorelease] : nil,
                             OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

-(void)setDelegateHack:(id<UITextFieldDelegate>)delegate{
    if (self.readOnly){
        if (![delegate isKindOfClass:[UITextFieldReadOnlyDelegate class]]){
            UITextFieldReadOnlyDelegate* readOnlyDelegate = objc_getAssociatedObject(self, readOnlyDelegateTagKey);
            readOnlyDelegate.originDelegate = delegate;
            return;
        }
    }
    [self setDelegateHack:delegate];
}

-(void)setReadOnly:(BOOL)value{
    [UIResponderHelper setReadonly:value responder:self];
}

-(void)markAsValid:(BOOL)valid {
    self.layer.borderColor = valid ? [[UIColor blackColor] CGColor] : [[UIColor redColor] CGColor];
    self.layer.borderWidth= valid ? 0.0f : 1.0f;
}

-(NSTextFormatterStyle)numberStyle{
    UITextFieldHandler* handlerDelegate = objc_getAssociatedObject(self, numericHandlerDelegateTagKey);
    if (handlerDelegate){
        return handlerDelegate.style;
    }
    return NSTextFormatterNoneStyle;
}

-(void)setNumberStyle:(NSTextFormatterStyle)style{
    if (style == NSTextFormatterNoneStyle){
        UITextFieldHandler* handlerDelegate = objc_getAssociatedObject(self, numericHandlerDelegateTagKey);
        if (handlerDelegate){
            self.delegate = handlerDelegate.delegate;
            objc_setAssociatedObject(self, numericHandlerDelegateTagKey, nil, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
            self.inputView = nil;
        }
        return;
    }
    UITextFieldHandler* handlerDelegate = [[UITextFieldHandler alloc]initWithTextField:self style:style];
    handlerDelegate.enableNil = TRUE;
    handlerDelegate.inputTextField = self;
    //copy previous original delegate
    id internalDelegate = [self.delegate isKindOfClass:[UITextFieldHandler class]] ? ((UITextFieldHandler*)self.delegate).delegate : self.delegate;
    handlerDelegate.delegate = internalDelegate;
    self.delegate = handlerDelegate;
    objc_setAssociatedObject(self, numericHandlerDelegateTagKey, handlerDelegate, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    [handlerDelegate release];
}



-(void)initConstraintHandler:(UITextFieldConstraintHandler *)constraitHandler {
    
    constraitHandler.inputTextField = self;
    id internalDelegate = [self.delegate isKindOfClass:[UITextFieldAlphanumericHandler class]]
                                ? ((UITextFieldAlphanumericHandler*)self.delegate).delegate
                                : self.delegate;
    constraitHandler.delegate = internalDelegate;
    self.delegate = constraitHandler;
}



-(void)setMaxTextLength:(NSUInteger)maxTextLength{
    
    UITextFieldConstraintHandler* constraitHandler = objc_getAssociatedObject(self, aplhaNumericHandlerDelegateTagKey);
    if (!constraitHandler) {
        
        constraitHandler = [[UITextFieldConstraintHandler alloc]init];
        objc_setAssociatedObject(self, aplhaNumericHandlerDelegateTagKey, constraitHandler, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
        [self initConstraintHandler:constraitHandler];
        [constraitHandler release];
    }
    
    constraitHandler.maxTextLength = maxTextLength;
}



-(void)setAplhaNumericMaxTextLength:(NSUInteger)maxTextLength{

    UITextFieldConstraintHandler* constraitHandler = objc_getAssociatedObject(self, aplhaNumericHandlerDelegateTagKey);
    if (!constraitHandler) {
        
        constraitHandler = [[UITextFieldAlphanumericHandler alloc]init];
        objc_setAssociatedObject(self, aplhaNumericHandlerDelegateTagKey, constraitHandler, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
        [self initConstraintHandler:constraitHandler];
        [constraitHandler release];
    }
    
    constraitHandler.maxTextLength = maxTextLength;
}



-(NumpadKeyboardContent)numpadKeyboardContent{
    NSNumber* value = objc_getAssociatedObject(self, numpadKeyboardContentTagKey);
    return [value intValue];
}

-(void)setNumpadKeyboardContent:(NumpadKeyboardContent)value{
    objc_setAssociatedObject(self, numpadKeyboardContentTagKey, @(value), OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}


@end

@implementation UISearchBar (Input)

-(nullable UITextField*)textField{
    return [super valueForKey:@"searchField"];
//    for(UIView* view in self.subviews){
//        if ([view isKindOfClass:[UITextField class]])
//            return (UITextField*)view;
//    }
//    return nil;
}

-(nullable UILabel*)placeholderLabel{
    return [self.textField valueForKey:@"placeholderLabel"];
//    for(UIView* view in self.textField.subviews){
//        if ([view isKindOfClass:[UILabel class]])
//            return (UILabel*)view;
//    }
//    return nil;
}

-(nullable UIButton*)cancelBtn{
    return [super valueForKey:@"cancelButton"];
}

-(void)setKeyboardVisible:(BOOL)value{
    [self.textField setKeyboardVisible:value];
}

-(BOOL)isKeyboardVisible{
	return self.textField.isKeyboardVisible;
}


-(BOOL)readOnly{
    return self.textField.readOnly;
}

-(void)setReadOnly:(BOOL)value{
    self.textField.readOnly = value;
}
@end

@implementation UITextField(Highligted)

-(void)setHighlighted:(BOOL)highlighted{
    if (self.highlighted==highlighted) return;
    objc_setAssociatedObject(self, highlightedPropTagKey, @(highlighted), OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    if (highlighted){
        UIColor* highlightedTextColor = [self highlightedTextColor];
        if (highlightedTextColor){
            //save original textColor
            objc_setAssociatedObject(self, textColorPropTagKey, self.textColor, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
            self.textColor = highlightedTextColor;
        }
    }
    else{
        //restore original
        UIColor* textColor = objc_getAssociatedObject(self, textColorPropTagKey);
        if (textColor){
            self.textColor = textColor;
        }
    }
}

-(BOOL)highlighted{
    NSNumber* highlighted = objc_getAssociatedObject(self, highlightedPropTagKey);
    return [highlighted boolValue];
}

-(void)setHighlightedTextColor:(UIColor *)highlightedTextColor{
    objc_setAssociatedObject(self, highlightedTextColorPropTagKey, highlightedTextColor, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}
-(UIColor*)highlightedTextColor{
    return objc_getAssociatedObject(self, highlightedTextColorPropTagKey);
}
@end
