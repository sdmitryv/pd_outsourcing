//
//  MyClass.m
//  CloudworksPOS
//
//  Created by valera on 8/8/11.
//  Copyright 2011 Retail Teamwork Ukraine. All rights reserved.
//

#import "UIRoundedCornersView.h"
#import <objc/runtime.h>

const char* maskLayerKeyTag = "maskLayerKey";

@interface UIRoundedCornersView(){
    CAShapeLayer* borderLayer;
}
@end

@implementation UIRoundedCornersView

-(void)initDefaults{
    if (defaultsInitited)
        return;
    [self setRoundedCorners:UIViewRoundedCornerAll radius:5.0f];
    defaultsInitited = TRUE;
}

- (id)init{
    if ((self = [super init])) {
        [self initDefaults];
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame{
    if ((self = [super initWithFrame:frame])) {
        [self initDefaults];
    }
    return self;
}

-(id)initWithCoder:(NSCoder *)aDecoder{
    if ((self = [super initWithCoder:aDecoder])) {
        [self initDefaults];
    }
    return self;
}

-(void)awakeFromNib{
    [super awakeFromNib];
    [self initDefaults];
}

-(void)dealloc{
    [borderLayer release];
    [super dealloc];
}

-(UIViewRoundedCornerMask)corners{
    return corners;
}

-(CGFloat)radius{
    return radius;
}

-(void)drawRoundedCorners{
    [[self class]drawRoundedCorners:self corners:corners radius:radius];
}

+(void)drawRoundedCorners:(UIView*)view radius:(CGFloat)radius{
    [[self class]drawRoundedCorners:view corners:UIViewRoundedCornerAll radius:radius];
}

+(void)drawRoundedCorners:(UIView*)view corners:(UIViewRoundedCornerMask)corners radius:(CGFloat)radius{
    [self drawLayerRoundedCorners:view.layer corners:corners radius:radius];
}

+(void)drawLayerRoundedCorners:(CALayer*)layer corners:(UIViewRoundedCornerMask)corners radius:(CGFloat)radius{
    if (corners == UIViewRoundedCornerNone){
        layer.mask = nil;
        objc_setAssociatedObject(layer, maskLayerKeyTag, nil, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
        layer.cornerRadius = 0;
        return;
    }
    
    layer.allowsEdgeAntialiasing = TRUE;
    
    if ((corners & UIViewRoundedCornerAll)==UIViewRoundedCornerAll){
        layer.mask = nil;
        objc_setAssociatedObject(layer, maskLayerKeyTag, nil, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
        layer.cornerRadius = radius;
        layer.masksToBounds = TRUE;
        return;
    }
    layer.cornerRadius = 0.0f;
    CGRect rect = layer.bounds;
    
    UIRectCorner rectCorners = 0;
    if ((corners & UIViewRoundedCornerUpperLeft) == UIViewRoundedCornerUpperLeft)
        rectCorners|=UIRectCornerTopLeft;
    if ((corners & UIViewRoundedCornerUpperRight) == UIViewRoundedCornerUpperRight)
        rectCorners|=UIRectCornerTopRight;
    if ((corners & UIViewRoundedCornerLowerRight) == UIViewRoundedCornerLowerRight)
        rectCorners|=UIRectCornerBottomRight;
    if ((corners & UIViewRoundedCornerLowerLeft) == UIViewRoundedCornerLowerLeft)
        rectCorners|=UIRectCornerBottomLeft;
    
    UIBezierPath* beizerPath = [UIBezierPath bezierPathWithRoundedRect:rect
                                                     byRoundingCorners:rectCorners
                                                           cornerRadii:CGSizeMake(radius, radius)];
    
	CAShapeLayer *maskLayer = nil;
    
    maskLayer = objc_getAssociatedObject(layer, maskLayerKeyTag);
    if (!maskLayer){
        maskLayer = [[CAShapeLayer alloc] init];
        maskLayer.allowsEdgeAntialiasing = TRUE;
        objc_setAssociatedObject(layer, maskLayerKeyTag, maskLayer, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
        [maskLayer release];
    }
  	maskLayer.path=beizerPath.CGPath;
    layer.mask = maskLayer;
}

-(CAShapeLayer*)borderLayer{
    CAShapeLayer* maskLayer = objc_getAssociatedObject(self.layer, maskLayerKeyTag);
    if (!maskLayer) return nil;
    if (!borderLayer){
        borderLayer= [[CAShapeLayer alloc]init];
        [self.layer addSublayer:borderLayer];
        borderLayer.fillColor = [[UIColor clearColor] CGColor];
    }
    borderLayer.path = maskLayer.path;
    borderLayer.frame = self.layer.bounds;
    return borderLayer;
}


-(void)setRoundedCorners:(UIViewRoundedCornerMask)aCorners radius:(CGFloat)aRadius {
    //self.layer.masksToBounds = NO;
    //self.layer.cornerRadius = 0;
    corners = aCorners;
    radius = aRadius;
    customRoundedCorners = TRUE;
    [self drawRoundedCorners];
}

-(void)layoutSubviews{
    [super layoutSubviews];
    if (customRoundedCorners && !CGSizeEqualToSize(lastLayoutedSize, self.bounds.size)){
        [self drawRoundedCorners];
        lastLayoutedSize = self.bounds.size;
        if (borderLayer){
            [self borderLayer];
        }
    }
}

@end

@implementation UIView(RoundedCorners)

-(void)drawRoundedCorners:(UIViewRoundedCornerMask)corners radius:(CGFloat)radius{
    [UIRoundedCornersView drawRoundedCorners:self corners:corners radius:radius];
}
-(void)drawRoundedCornersWithRadius:(CGFloat)radius{
    [UIRoundedCornersView drawRoundedCorners:self radius:radius];
}

@end
