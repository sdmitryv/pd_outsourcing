//
//  UILoopProgressView.m
//  ProductBuilder
//
//  Created by valera on 10/17/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import "UILoopProgressView.h"

@implementation UILoopProgressView


-(void)setup{
    super.progress = 0;
    self.progressTintColor = [UIColor blackColor];
    self.trackTintColor = [UIColor lightGrayColor];
    canAnimate = FALSE;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setup];
    }
    return self;
}

-(id)initWithCoder:(NSCoder *)aDecoder{
    if ((self =[super initWithCoder:aDecoder])){
        [self setup];
    }
    return self;
}


- (void)awakeFromNib {
    [super awakeFromNib];
    [self setup];
}

-(void)dealloc{
    [self stopAnimation];
    [super dealloc];
}

-(void)layoutSubviews{
    [super layoutSubviews];
    canAnimate = TRUE;
    if (shouldAnimate && !animating){
        [self startAnimation];
    }
}

-(void)animateToOne{
    animating = TRUE;
    [timer invalidate];
    [timer release];
    timer = [[NSTimer scheduledTimerWithTimeInterval:3.01 target:self selector:@selector(updateProgress) userInfo:nil repeats:FALSE]retain];
    [UIView animateWithDuration:3.0 animations:^{
        [super setProgress:1.0 animated:TRUE];
    } completion:^(BOOL finished){
    }];
}


-(void)animateToZero{
    animating = TRUE;
    [timer invalidate];
    [timer release];
    timer = [[NSTimer scheduledTimerWithTimeInterval:3.01 target:self selector:@selector(updateProgress) userInfo:nil repeats:FALSE]retain];
    [UIView animateWithDuration:3.0 animations:^{
        [super setProgress:0.0 animated:TRUE];
    } completion:^(BOOL finished){
    }];
}


-(void)updateProgress{
    //self.progress = fmodf(self.progress + 0.01, 1.0f);
    //[self setProgress:fmodf(self.progress + 0.01, 1.0f) animated:TRUE];
    
    if (self.progress< 0.5){
        [self animateToOne];
    }
    else{
        [self animateToZero];
    }
}


-(void)startAnimation{
    shouldAnimate = TRUE;
    if (!canAnimate) return;
    [self stopAnimation];
    //timer = [[NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(updateProgress) userInfo:nil repeats:TRUE]retain];
    super.progress = 0;
    [self animateToOne];
}

-(void)stopAnimation{
    [timer invalidate];
    [timer release];
    timer = nil;
    
    [self.layer removeAllAnimations];
    shouldAnimate = FALSE;
    animating = FALSE;
}

-(void)setProgress:(float)progress{
    [self setProgress:progress animated:TRUE];
}
@end
