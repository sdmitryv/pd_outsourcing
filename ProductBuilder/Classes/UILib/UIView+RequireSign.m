//
//  UIView+RequireSign.m
//  ProductBuilder
//
//  Created by Artem Nikulchenko on 2/3/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "UIView+RequireSign.h"
#import <objc/runtime.h>

@implementation UIView (RequireSign)

const char* signLabelKey = "SignLabelKey";
const char* signLabelPositionKey = "SignLabelPositionKey";

-(void)setSign:(NSString*)value {
    if (!value){
        UILabel* label = self.signLabel;
        if (label){
            objc_setAssociatedObject(self, signLabelKey, nil, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
            [label removeFromSuperview];
        }
        return;
    }
    UILabel* label = self.signLabel;
    if (!label) {
        label = [[UILabel alloc] initWithFrame:self.signRect];
//        label.autoresizingMask = self.autoresizingMask;
        label.text = value;
        label.textColor = [UIColor colorWithRed:255.0/255.0 green:52.0/255.0 blue:52.0/255.0 alpha:1.0f];
        label.backgroundColor = [UIColor clearColor];
        label.baselineAdjustment = UIBaselineAdjustmentAlignCenters;
        [[self superview] addSubview:label];
        objc_setAssociatedObject(self, signLabelKey, label, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
        [label release];
    }
    else {
        label.text = value;
    }
    if ([value isEqualToString:@"*"]) {
        label.font = [UIFont fontWithName:@"Helvetica" size:18.0];
    } else {
        label.font = [UIFont fontWithName:@"Helvetica" size:9.0];
    }
    [label sizeToFit];
    
}

-(UILabel*)signLabel {
    return objc_getAssociatedObject(self, signLabelKey);
}

-(NSString*)sign {
    return self.signLabel.text;
}

-(SignPostion)signPosition{
    SignPostion position = SignPostionRight;
    NSNumber* positionNumber = (NSNumber*)objc_getAssociatedObject(self, signLabelPositionKey);
    if (positionNumber) {
        return [positionNumber intValue];
    }
    return position;
}

-(void)setSignPosition:(SignPostion)value{
    objc_setAssociatedObject(self, signLabelPositionKey, @(value), OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    UILabel* label = self.signLabel;
    if (label){
        label.frame = self.signRect;
    }
}

-(CGRect)rectForTextInLabel{
    CGRect frame = self.frame;
    if (![self isKindOfClass:[UILabel class]]){
        return frame;
    }
    UILabel* label = (UILabel*)self;
    CGSize size = [[[[NSAttributedString alloc]initWithString:label.text attributes:@{NSFontAttributeName:label.font}] autorelease] boundingRectWithSize:(CGSize){label.bounds.size.width, CGFLOAT_MAX} options:NSStringDrawingUsesLineFragmentOrigin | label.lineBreakMode context:nil].size;
    if (label.textAlignment == NSTextAlignmentRight){
        if (frame.size.width > size.width){
            frame.origin.x += frame.size.width - size.width;
        }
    }
    else if (label.textAlignment == NSTextAlignmentCenter){
        if (frame.size.width > size.width){
            frame.origin.x += (int)((frame.size.width - size.width)/2);
        }
    }
    else if (label.textAlignment == NSTextAlignmentLeft){
        //x is the same as label's x

    }
    if (frame.size.height > size.height){
        frame.origin.y += (int)((frame.size.height - size.height)/2);
    }
    frame.size = CGSizeMake(size.width, size.height > self.frame.size.height ? self.frame.size.height : size.height);

    return frame;
}

-(CGRect)signRect{
    CGRect frame = CGRectZero;
    CGRect ownerFrame = self.frame;
    CGFloat yOffset = 5;
    if ([self isKindOfClass:[UILabel class]]){
        ownerFrame = self.rectForTextInLabel;
        yOffset = 1;
    }
    switch(self.signPosition){
        case SignPostionLeft:
            frame = CGRectMake(ownerFrame.origin.x - 12, ownerFrame.origin.y - yOffset, 10, ownerFrame.size.height);
            break;
        case SignPostionRight:
            frame = CGRectMake(ownerFrame.origin.x + ownerFrame.size.width + 2, ownerFrame.origin.y - yOffset, 15, ownerFrame.size.height);            
            break;
    }
    return frame;
}

-(void)hideSign:(BOOL)hidden {
    UILabel* label = (UILabel*)objc_getAssociatedObject(self, signLabelKey);
    if (label) {
        [label setHidden:hidden];
    }
}



@end
