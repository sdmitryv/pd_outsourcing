//
//  AGWindowView.h
//  VG
//
//  Created by Håvard Fossli on 23.01.13.
//  Copyright 2013 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface LowBatteryView : UIView{
    UIImageView* imageView;
    UIImageView* ligtningImageView;
    BOOL _visible;
}

+(LowBatteryView *)instance;

@property (nonatomic, assign) BOOL visible;

@end
