//
//  ButtonCollectionViewCell.swift
//  ProductBuilder
//
//  Created by PavelGurkovskii on 1/12/17.
//  Copyright © 2017 Cloudworks. All rights reserved.
//

import Foundation

class ButtonCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var button: MOButtonBlueTransparent!
    var selectBlock:(()->Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        button.addTarget(self, action: #selector(ButtonCollectionViewCell.didSelectButton), for: .touchUpInside)
    }
    
    func didSelectButton() {
        if let block = selectBlock {
            block()
        }
    }
}
