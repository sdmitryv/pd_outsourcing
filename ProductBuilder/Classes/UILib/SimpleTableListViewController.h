//
//  SimpleTableListViewController.h
//  iPadPOS
//
//  Created by valera on 3/7/11.
//  Copyright 2011 DBBestTechnologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GridTable.h"
#import "RTUITableView.h"

@class SimpleTableListViewController;

@protocol SimpleTableListViewControllerDelegate
@optional
-(void)simpleTableListViewController:(SimpleTableListViewController*)controller didSelectIndex:(NSNumber*)anIndex;
-(void)simpleTableListViewController:(SimpleTableListViewController*)controller didSelectValue:(id)value;
-(BOOL)simpleTableListViewController:(SimpleTableListViewController*)controller willSelectIndex:(NSInteger)anIndex;
-(void)simpleTableListViewController:(SimpleTableListViewController*)controller didDismissPopover:(UIPopoverPresentationController*)popoverPresentationController;
-(void)simpleTableListViewController:(SimpleTableListViewController*)controller willDisplayValue:(id)value inContent:(id)content;
-(void)simpleTableListViewController:(SimpleTableListViewController *)controller viewWillAppear:(BOOL)animated;
@end



@interface SimpleTableListViewController : UIViewController <
        //UITableViewDelegate, UITableViewDataSource,
        GridTableDelegate,
        UIPopoverPresentationControllerDelegate> {
	NSInteger		selectedIndex;
    NSInteger		scroledIndex;
	UIView*			_titleView;
	UILabel*		_titleLabel;
	UIButton*		_bCancel;
	//UITableView*	_tableView;
    GridTable*      _tableView;
    RTUITableView*  _plainTableView;
	UIPopoverPresentationController* popoverPresentationController;
    NSInteger        ignoreRowSelection;
    BOOL            _titleVisible;
}

@property (nonatomic, assign) NSInteger selectedIndex;
@property (nonatomic, assign) NSInteger scroledIndex;
@property (nonatomic, assign) id selectedObject;
@property (nonatomic, assign) NSObject<SimpleTableListViewControllerDelegate>* delegate;
@property (nonatomic, retain) id items;
@property (nonatomic, retain) NSString* titleText;
@property (nonatomic, retain) NSString* displayMember;
@property (nonatomic, retain) NSArray* columns;
@property (nonatomic, readonly) GridTable* gridTableView;
@property (nonatomic, readonly) RTUITableView* plainTableView;
@property (nonatomic, readonly) BOOL isPopoverVisible;
@property (nonatomic, readonly) UIButton* cancelButton;
@property (nonatomic, assign) BOOL titleVisible;

@property (nonatomic, retain) NSObject* additionalObject;

- (void)presentPopoverFromRect:(CGRect)rect inView:(UIView *)view permittedArrowDirections:(UIPopoverArrowDirection)arrowDirections animated:(BOOL)animated;
- (void)presentInController:(UIViewController *)controller;
- (void)dismissPopover;

//- (BOOL)getBoolForRow:(NSInteger)row column:(ColumnDescription *)column;
//- (void)changeBoolForRow:(NSInteger)row column:(ColumnDescription *)column;

-(void)setSelectedIndex:(NSInteger)value ignoreSelection:(BOOL)ignore;
-(void)setSelectedObject:(id)anObject ignoreSelection:(BOOL)ignore;

@end
