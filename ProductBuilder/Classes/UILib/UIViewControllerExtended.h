//
//  UIViewController+Extended.h
//  StockCount
//
//  Created by Valera on 12/3/10.
//  Copyright 2010 DBBest Technologies. All rights reserved.
//
#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import "HardwareViewController.h"

@class UIViewControllerExtended;

@protocol UIModalViewControllerDelegate
- (void)modalViewControllerWasDismissed:(UIViewControllerExtended*)controller;
@end

@class RTDimmingView;

@interface UIViewController (FindTopmostViewController)
+ (UIViewController*) topmostModalViewController;
@end

@interface UIViewController (ChildModalRelations)

@property (nonatomic, readonly)UIViewController* childModalController;
@property (nonatomic, readonly)UIViewController* parentModalController;
@property (nonatomic, retain)UIResponder* formerFirstResponder;
@end

@interface UIViewController (FindModalViewController)
-(BOOL)isPopoverPresentationController;
-(UIViewController*) traverseModalChainForUIViewController;
@end

@interface UIViewControllerExtended : HardwareViewController<UIModalViewControllerDelegate>
{
    //UIViewController *childModalController;
	//UIViewController *parentModalController;
    id<UIModalViewControllerDelegate> delegate;
    BOOL keyboardIsShown;
    CGRect lastKeyboardFrame;
    BOOL useCustomSize;
    //CGPoint originalCenter;
    BOOL _dismissing;
    BOOL isBeingShowed;
    NSNumber* tx;
    NSNumber* ty;
    NSNumber* th;
    BOOL customSizeHandled;
    CGSize originalSize;
    BOOL isAppeared;
    void(^completion)(void);
    RTDimmingView* dimmingView; 
    NSNotification* keyboardWillShowDelayedNotification;
}
@property (nonatomic, readonly) BOOL dismissing;
@property (nonatomic, assign) BOOL keyboardIsShown;
@property (nonatomic, assign) BOOL animated;
@property (nonatomic, assign) BOOL suppressFiringParentAppearance;
@property (nonatomic, assign) BOOL resizeToFitKeyboard;
@property (nonatomic, readonly) BOOL isAppeared;
@property (nonatomic, assign) BOOL skeepKeyboardAnimation;

@property (nonatomic, copy) void(^completionBlock)(void);
//
+(void)setRootViewController:(UIViewController*)controller;
+(UIViewController*)rootViewController;

//show self modally
- (void)showModal:(BOOL)useViewSize delegate:(id<UIModalViewControllerDelegate>) aDelegate;
- (void)showModal:(BOOL)useViewSize completion:(void (^)(void))completion;
- (void)showModalFormSheetWithCompletion:(void (^)(void))aCompletion;
- (void)showModalPageSheetWithCompletion:(void (^)(void))aCompletion;
//show child controller modally
- (void)showModalViewController:(UIViewControllerExtended*)controller;
- (void)showFormViewController:(UIViewControllerExtended*)controller;
- (void)showPageViewController:(UIViewControllerExtended*)controller;
- (void)showModal:(UIViewControllerExtended*)controller style:(UIModalPresentationStyle)style useViewSize:(BOOL)useViewSize completion:(void (^)(void))aCompletion;

- (void)showModalViewController:(UIViewControllerExtended*)controller delegate:(id<UIModalViewControllerDelegate>) delegate;
- (void)showFormViewController:(UIViewControllerExtended*)controller delegate:(id<UIModalViewControllerDelegate>) delegate;
- (void)showPageViewController:(UIViewControllerExtended*)controller delegate:(id<UIModalViewControllerDelegate>) delegate;
- (void)showModalViewController:(UIViewControllerExtended*)controller useViewSize:(BOOL)useViewSize delegate:(id<UIModalViewControllerDelegate>) delegate;

- (void)showModalViewController:(UIViewControllerExtended*)controller completion:(void (^)(void))completion;
- (void)showFormViewController:(UIViewControllerExtended*)controller completion:(void (^)(void))completion;
- (void)showPageViewController:(UIViewControllerExtended*)controller completion:(void (^)(void))completion;
- (void)showModalViewController:(UIViewControllerExtended*)controller useViewSize:(BOOL)useViewSize completion:(void (^)(void))completion;

+ (void)showModal:(UIViewController*)parentController childController:(UIViewControllerExtended*)childController style:(UIModalPresentationStyle)style useViewSize:(BOOL)useViewSize delegate:(id<UIModalViewControllerDelegate>) aDelegate completion:(void (^)(void))completion;

//methods for inheritors
- (void)keyboardDidHide:(NSNotification *)n;
- (void)keyboardWillHide:(NSNotification *)n;
- (void)keyboardWillShow:(NSNotification *)n;
- (void)keyboardDidShow:(NSNotification *)n;

@end

CGRect CGRectRound(CGRect rect);
