//
//  UITextField+Input.h
//  StockCount
//
//  Created by Valera on 12/2/10.
//  Copyright 2010 DBBest Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>

#pragma mark -
#pragma mark Hardware keyboard events

#include <mach/message.h>
#include <CoreFoundation/CoreFoundation.h>
#include <CoreGraphics/CoreGraphics.h>
#include <Availability.h>
#import "UITextFieldHandler.h"
#import "NumpadKeyboardController.h"

extern  const char * _Nullable readOnlyDelegateTagKey;

@interface UITextViewReadOnlyDelegate:NSObject<UITextViewDelegate>{
    id<UITextViewDelegate> originDelegate;
}
-(nullable id)initWithUITextViewDelegate:(_Nullable id<UITextViewDelegate>)textViewDelegate;
-(nullable id<UITextViewDelegate>)originDelegate;
-(void)setOriginDelegate:(_Nullable id<UITextViewDelegate>)delegate;
@end

@interface UITextFieldReadOnlyDelegate:NSObject<UITextFieldDelegate>{
    id<UITextFieldDelegate> originDelegate;
}
-(_Nullable id)initWithUITextFieldDelegate:(_Nullable id<UITextFieldDelegate>)textFieldDelegate;
-(_Nullable id<UITextFieldDelegate>)originDelegate;
-(void)setOriginDelegate:(_Nullable id<UITextFieldDelegate>)delegate;
@end

@interface UITextField (Input)
-(void)setKeyboardVisible:(BOOL)value;
-(BOOL)isKeyboardVisible;
@property (assign) BOOL readOnly;
@property (assign) BOOL notChangeColorForReadOnly;
-(void)selectAllNoMenu;
-(void)markAsValid:(BOOL)valid;
//posibility to set numeric style
-(NSTextFormatterStyle)numberStyle;
-(void)setNumberStyle:(NSTextFormatterStyle)style;
-(void)setMaxTextLength:(NSUInteger)maxTextLength;
-(void)setAplhaNumericMaxTextLength:(NSUInteger)maxTextLength;
-(NumpadKeyboardContent)numpadKeyboardContent;
-(void)setNumpadKeyboardContent:(NumpadKeyboardContent)value;

@property (nonatomic, assign) BOOL pasteNotAllowed;
@end

@interface UITextField (Additions)
@property (nullable, retain) NSError* error;
@property (assign) NSInteger internalChange;
@property (nullable, nonatomic, copy) void(^fetchDataBlock)(_Nullable id sender);
-(void)fetchData;
@end


@interface UITextView (Input)
-(void)setKeyboardVisible:(BOOL)value;
-(BOOL)isKeyboardVisible;
@property (assign) BOOL readOnly;
@end

@interface UISearchBar (Input)
-(void)setKeyboardVisible:(BOOL)value;
-(BOOL)isKeyboardVisible;
@property (assign) BOOL readOnly;
-(nullable UITextField*)textField;
-(nullable UILabel*)placeholderLabel;
-(nullable UIButton*)cancelBtn;
@end


@interface UITextField (Highligted)
@property (nullable, nonatomic, retain) UIColor* highlightedTextColor;
@property(nonatomic, assign) BOOL highlighted;
@end
