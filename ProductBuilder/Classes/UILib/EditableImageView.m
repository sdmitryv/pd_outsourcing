//
//  EditableImageView.m
//  iPadPOS
//
//  Created by Lulakov Viacheslav on 3/11/11.
//  Copyright 2011 DBBestTechnologies. All rights reserved.
//

#import <AssetsLibrary/AssetsLibrary.h>
#import <MobileCoreServices/MobileCoreServices.h>
#import "EditableImageView.h"
#import "QuartzCore/QuartzCore.h"

@interface EditableImageView(Private)
-(void)initDefaults;
@end

@implementation EditableImageView

@synthesize addImageButton;
@synthesize changeImageButton;
@synthesize deleteImageButton;
//@synthesize imageView;
@synthesize popover;

- (id)initWithCoder:(NSCoder *)aDecoder{
	if ((self = [super initWithCoder:aDecoder])){
		[self initDefaults];
	}
	return self;
}

- (id)initWithFrame:(CGRect)frame {
	if ((self = [super initWithFrame:frame])) {
		[self initDefaults];
	}
	return self;
}

- (void)dealloc {
	[addImageButton release];
	[changeImageButton release];
	[deleteImageButton release];
	[popover release];
	//[imageView release];
    [super dealloc];
}

-(void)initDefaults{
	
	self.backgroundColor = [UIColor clearColor];
	addImageButton = [[UIButton buttonWithType:UIButtonTypeRoundedRect]retain];
	[addImageButton addTarget:self action:@selector(addImageButtonClick) forControlEvents:UIControlEventTouchUpInside];
	addImageButton.frame = self.bounds;
	addImageButton.titleLabel.font = [UIFont systemFontOfSize:18];
	[addImageButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
	[addImageButton setTitle:NSLocalizedString(@"ADDIMAGEBTN_TITLE", nil) forState:UIControlStateNormal];
	addImageButton.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
	addImageButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
	[self addSubview:addImageButton];
	[self bringSubviewToFront:addImageButton];
	//imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, frame.size.width - 8, frame.size.height - 8)];
	//imageView.center = addImageButton.center;
	//imageView.clipsToBounds = YES;
	//imageView.hidden = YES;
	//[self addSubview:imageView];
	changeImageButton = [[UIButton buttonWithType:UIButtonTypeCustom]retain];
	[changeImageButton addTarget:self action:@selector(addImageButtonClick) forControlEvents:UIControlEventTouchUpInside];
	[changeImageButton setImage:[UIImage imageNamed:@"plus.png"] forState:UIControlStateNormal];
	changeImageButton.imageView.image = [UIImage imageNamed:@"plus.png"];
	changeImageButton.alpha = 0.4;
	changeImageButton.frame = CGRectMake(2, self.frame.size.height - 34, 32, 32);
	changeImageButton.hidden = YES;
	[self addSubview:changeImageButton];
	deleteImageButton = [[UIButton buttonWithType:UIButtonTypeCustom]retain];
	[deleteImageButton addTarget:self action:@selector(deleteImageButtonClick) forControlEvents:UIControlEventTouchUpInside];
	[deleteImageButton setImage:[UIImage imageNamed:@"delete.png"] forState:UIControlStateNormal];
	deleteImageButton.alpha = 0.4;
	deleteImageButton.frame = CGRectMake(self.frame.size.width - 34, self.frame.size.height - 34, 32, 32);
	deleteImageButton.hidden = YES;
	[self addSubview:deleteImageButton];
	self.layer.masksToBounds = YES;
	self.layer.cornerRadius = 5.0;
	self.userInteractionEnabled = TRUE;
	
}


#pragma mark -
#pragma mark Actions

- (IBAction)addImageButtonClick {
	// If a popover is already showing, dismiss it.
	if(self.popover){
		[self.popover dismissPopoverAnimated:YES];
		self.popover = nil;
		return;
	}
	UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
	imagePicker.delegate = self;
    imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
	RTPopoverController *popoverController = [[RTPopoverController alloc] initWithContentViewController:imagePicker];
	popoverController.delegate = self;
	self.popover = popoverController;
	[popoverController presentPopoverFromRect:self.frame inView:self.superview permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
	[popoverController release];
	[imagePicker release];
}

- (IBAction)deleteImageButtonClick {
	self.image = nil;
}

#pragma mark -
#pragma mark Properties
/*
- (UIImage *)image {
	return imageView.image;
}*/

- (void)setImage:(UIImage *)value {
	//imageView.image = value;
	//imageView.hidden = value == nil;
	[super setImage:value];
	changeImageButton.hidden = value == nil;
	deleteImageButton.hidden = value == nil;
	addImageButton.enabled = value == nil;
	addImageButton.hidden = value != nil;
}

#pragma mark delegate methods

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
	if(info){
		// Don't pay any attention if somehow someone picked something besides an image.
		if([info[UIImagePickerControllerMediaType] isEqualToString:(NSString *)kUTTypeImage]){
			// Hand on to the asset URL for the picked photo..
			NSURL *imageURL = info[UIImagePickerControllerReferenceURL];
			// To get an asset library reference we need an instance of the asset library.
			ALAssetsLibrary* assetsLibrary = [[ALAssetsLibrary alloc] init];
			// The assetForURL: method of the assets library needs a block for success and
			// one for failure. The resultsBlock is used for the success case.
			ALAssetsLibraryAssetForURLResultBlock resultsBlock = ^(ALAsset *asset) {
				ALAssetRepresentation *representation = [asset defaultRepresentation];
				CGImageRef image = [representation fullScreenImage];
				// Make sure that the UIImage we create from the CG image has the appropriate
				// orientation, based on the EXIF data from the image.
				ALAssetOrientation orientation = [representation orientation];
				self.image = [UIImage imageWithCGImage:image scale:1.0 orientation:(UIImageOrientation)orientation];
			};
			ALAssetsLibraryAccessFailureBlock failureBlock = ^(NSError *error){
				/*  A failure here typically indicates that the user has not allowed this app access
				 to location data. In that case the error code is ALAssetsLibraryAccessUserDeniedError.
				 In principle you could alert the user to that effect, i.e. they have to allow this app
				 access to location services in Settings > General > Location Services and turn on access
				 for this application.
				 */
				NSLog(@"FAILED! due to error in domain %@ with error code %ld", error.domain, (long)error.code);
			};
			
			// Get the asset for the asset URL.
			[assetsLibrary assetForURL:imageURL resultBlock:resultsBlock failureBlock:failureBlock];
			// Release the assets library now that we are done with it.
			[assetsLibrary release];
			
			// If we were presented with a popover, dismiss it.
			if(self.popover){
				[self.popover dismissPopoverAnimated:YES];
				self.popover = nil;
			}
		}
	}
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
	//	[self dismissModalViewControllerAnimated:YES];
}

- (void)popoverControllerDidDismissPopover:(RTPopoverController *)popoverController {
	self.popover = nil;
}

- (void)navigationController:(UINavigationController *)navigationController willShowViewController:(UIViewController *)viewController animated:(BOOL)animated {

	UINavigationBar *bar = navigationController.navigationBar;
	bar.hidden = NO;
    
    UIBarButtonItem *cancelButton = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(dismissImagePicker:)];
    UINavigationItem* ipcNavBarTopItem = bar.topItem;
    ipcNavBarTopItem.title = @"Images";
	ipcNavBarTopItem.rightBarButtonItem = cancelButton;
    [cancelButton release];

}

-(void)dismissImagePicker:(id)object{
    [self.popover dismissPopoverAnimated:YES];
    self.popover = nil;
}

@end
