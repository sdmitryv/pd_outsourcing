//
//  RTDateEditView.h
//  CloudworksPOS
//
//  Created by Vitaliy Gervazuk on 8/1/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RTDatePickerViewController.h"

@protocol RTDateEditViewDelegate

    -(void)valueChanged:(id)sender;

@end


@interface RTDateEditView : UIView<RTPopoverControllerDelegate, IRTDatePickerViewController> {
    UIButton* _button;
    UITextField* _textField;
    RTPopoverController* _popoverCtrl;
    RTDatePickerViewController* _pickercontrol;
    NSDate* _date;
    BOOL readonly;
}

@property (nonatomic, retain) NSDate* date;
@property (nonatomic, assign) BOOL readonly;
@property (nonatomic, assign) id<RTDateEditViewDelegate> delegate;
@property (nonatomic, readonly) RTDatePickerViewController* pickerControl;

- (void)showDatePicker;

@end
