/*
 Erica Sadun, http://ericasadun.com
 iPhone Developer's Cookbook, 3.0 Edition
 BSD License, Use at your own risk
 */

#import <UIKit/UIKit.h>

@interface ModalAlert : NSObject
//+ (NSUInteger) askWithTitle:(NSString*)title question:(NSString *)question withCancel: (NSString *) cancelButtonTitle withButtons: (NSArray *) buttons;
+ (void) askWithTitle:(NSString*)title question:(NSString *)question withCancel: (NSString *) cancelButtonTitle withButtons: (NSArray *) buttons completed:(void(^)(NSInteger))completed;
+ (void) ask: (NSString *) question withCancel: (NSString *) cancelButtonTitle withButtons: (NSArray *) buttons completed:(void(^)(NSInteger))completed;
+ (void) ask: (NSString*)question completed:(void(^)(NSInteger))completed;
+ (void) say: (id)formatstring,...;
+ (void) sayWithTitle:(NSString*)title message:(NSString*)message;
+ (void) sayWithTitle:(NSString*)title message:(NSString*)message cancelButtonTitle:(NSString*)cancelButtonTitle;
+ (void) show:(NSString*)msg;
+ (void) show:(NSString*)msg completed:(dispatch_block_t)completed;
+ (void) show:(NSString*)title message:(NSString*)message;
+ (void) show:(NSString*)title message:(NSString*)message completed:(dispatch_block_t)completed;
+ (void) showError:(NSError*)error;
+ (void) showError:(NSError*)error completed:(dispatch_block_t)completed;
+ (void) showError:(NSError*)error completedWithOptions:(void(^)(NSInteger))completed;
+ (void) showErrorMessage:(NSString*)message;
+ (void) showErrorMessage:(NSString*)message completed:(dispatch_block_t)completed;
+(void)parseError:(NSError*)error title:(NSString**)aTitle description:(NSString**)aDescription;
@end
