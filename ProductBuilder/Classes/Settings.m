//
//  Settings.m
//  ProductBuilder
//
//  Created by Lulakov Viacheslav on 9/30/10.
//  Copyright 2010 DBBestTechnologies. All rights reserved.
//

#import "Settings.h"
#import "Workstation.h"
#import "Device.h"
#import "StatusViewController.h"
#import "SettingManager.h"
#import "AppSettingManager.h"



@implementation Settings

static Settings *instance = nil;


- (id)init
{
	if((self = [super init])) {
        
		NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults synchronize];
	}
	return self;
}


- (NSString *)hqAddress {
    
    if (!hqAddress) {
        
        NSString *urlStr = [SettingManager instance].cloudHQUrl;
        
        if (!urlStr.length && [AppSettingManager instance].serverUrl) {
            
            NSString *prefix = @"";
            if ([[AppSettingManager instance].serverUrl hasPrefix:@"http://"])
                prefix = @"http://";
            else if ([[AppSettingManager instance].serverUrl hasPrefix:@"https://"])
                prefix = @"https://";
            
            NSString *urlStringToAnalyse = [[AppSettingManager instance].serverUrl substringFromIndex:prefix.length];
            
            if ([urlStringToAnalyse hasPrefix:@"cts."]) {
                
                NSString *addr = [urlStringToAnalyse substringFromIndex:4];
                urlStr = [NSString stringWithFormat:@"chq.%@", addr];
            }
            else
                urlStr = urlStringToAnalyse;
            
            urlStr = [NSString stringWithFormat:@"%@%@", prefix, urlStr];
        }
        else {
            
            if (![urlStr hasPrefix:@"http://"] && ![urlStr hasPrefix:@"https://"])
                urlStr = [NSString stringWithFormat:@"https://%@", urlStr];
        }
        
        urlStr = [urlStr stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"/"]];
        hqAddress = [urlStr retain];
    }
    
    return hqAddress;
}


-(NSString *)appURLScheme {
    
    NSArray *urlTypes = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleURLTypes"];
    if (urlTypes.count) {
       
        NSDictionary *firstType = urlTypes[0];
        NSArray *urlSchemes = firstType[@"CFBundleURLSchemes"];
        NSString *mainScheme = urlSchemes[0];
        return mainScheme;
    }
    
    return nil;
}


- (void)dealloc {
    
    [hqAddress release];

	[super dealloc];
}

+(Settings *)sharedInstance {
    Settings* anInstance = nil;
    @synchronized(self){
        if (!instance) {
            instance = [[Settings alloc] init];
        }
        anInstance = [[instance retain]autorelease];
    }
	return anInstance;
}

+(void)reset {
    @synchronized(self){
        if (instance) {
            [instance release];
            instance = nil;
        }
    }
}

@end
