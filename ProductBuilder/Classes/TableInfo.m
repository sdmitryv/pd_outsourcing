//
//  Settings.m
//  ProductBuilder
//
//  Created by Lulakov Viacheslav on 9/30/10.
//  Copyright 2010 DBBestTechnologies. All rights reserved.
//


#import "TableInfo.h"

@implementation TableInfo

const char* disableChangingContentOffsetKey = "disableChangingContentOffset";

@synthesize count,
completed,
canceled,
loadTime,
name,
errorMessage;

-(void)dealloc{
    [name release];
    [errorMessage release];
    [super dealloc];
}

@end
