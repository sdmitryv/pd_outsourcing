//
//  MessageMismatchController.h
//  ProductBuilder
//
//  Created by Roman on 4/9/12.
//  Copyright (c) 2012 Cloudworks. All rights reserved.
//

#import "RTMessageController.h"

@interface MessageMismatchController : RTMessageController

@property (retain, nonatomic) IBOutlet UIView    *locationInfoView;
@property (retain, nonatomic) IBOutlet UILabel *locationInitLabel;
@property (retain, nonatomic) IBOutlet UILabel *locationSyncLabel;
@property (retain, nonatomic) IBOutlet UILabel   *locationInitTitle;
@property (retain, nonatomic) IBOutlet UILabel   *locationSyncTitle;

@end
