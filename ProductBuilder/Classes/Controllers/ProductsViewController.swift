//
//  ProductsViewController.swift
//  ProductBuilder
//
//  Created by Viacheslav Lulakov on 8/31/17.
//  Copyright © 2017 Cloudworks. All rights reserved.
//

import UIKit

class ProductsViewController: UITableViewController {

    var products: ProductList? {
        didSet {
            self.tableView.reloadData()
        }
    }
    var selectedProduct : Product?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.tableFooterView = UIView(frame: CGRect.zero)
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        if (segue.destination is UINavigationController) {
            let navigationViewController = segue.destination as? UINavigationController
            if let productDetailsViewController = navigationViewController?.visibleViewController as? ProductDetailsViewController {
                productDetailsViewController.product = selectedProduct
            }
        }
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let width = tableView.frame.size.width
        let headerView = UIView(frame: CGRect(x: 0, y: 0, width: width, height: 44))
        headerView.backgroundColor = UIColor(r: 237, g: 237, b: 237)
        let titleLabel = UILabel(frame: CGRect(x:16, y:0, width:width - 16, height: 43))
        titleLabel.font = UIFont.boldSystemFont(ofSize: 17)
        titleLabel.text = "Recent Products Created"
        headerView.addSubview(titleLabel)
        let separatorView = UILabel(frame: CGRect(x:0, y:43, width:width, height: 0.5))
        separatorView.backgroundColor = UIColor(r: 199, g: 200, b: 204)
        headerView.addSubview(separatorView)
        
        return headerView
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return products != nil ? products!.count() : 0
    }

    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let product : Product = products![indexPath.row] as! Product
        let description : String = product.description
        let constraintRect = CGSize(width: 350, height: 75)
        let boundingBox = description.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSFontAttributeName: UIFont.systemFont(ofSize: 15)], context: nil)
        return ceil(boundingBox.height) + 90
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell : ProductTableViewCell!
        if let c = tableView.dequeueReusableCell(withIdentifier: "ProductCellIdentifier", for: indexPath) as? ProductTableViewCell {
            cell = c
        }
        else {
            cell = ProductTableViewCell()
        }
        if indexPath.row < products!.count() {
            cell.updateWithProduct(products![indexPath.row] as! Product)
        }
        return cell
    }

    override func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        selectedProduct = products![indexPath.row] as? Product
        return indexPath
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
}
