//
//  DecimalTextFieldTableViewCell.swift
//  RPlus
//
//  Created by PavelGurkovskii on 9/12/16.
//  Copyright © 2016 Cloudworks. All rights reserved.
//

import Foundation

class DecimalTextFieldTableViewCell : NumberTextFieldTableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        let textField = self.valueTextField as? UITextFieldNumeric
        textField?.maxValue = (99999999.999991)
        textField?.minValue = (-99999999.999991)
        textField!.style = .decimalStyle
        textField!.customFracationDigits = 5
    }
    
    override func textFieldDidEndEditing(_ textField: UITextField) {
        if let del = self.customFieldDelegate {
            let tField = textField as? UITextFieldNumeric
            del.customFieldDidChangeValue(self, value: tField?.decimalNumberValue)
        }
    }
    
    override func setValue(_ value:AnyObject?){
        if let v = value as? NSDecimalNumber {
            let textField = self.valueTextField as? UITextFieldNumeric
            textField?.decimalNumberValue = v
        } else {
            self.valueTextField.text = nil
        }
    }
    
    func setDecimalValue(_ value:Decimal) {
        let textField = self.valueTextField as? UITextFieldNumeric
        textField?.decimalValue = value
    }

}
