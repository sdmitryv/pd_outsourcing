//
//  NumberTextFieldTableViewCell.swift
//  RPlus
//
//  Created by PavelGurkovskii on 9/12/16.
//  Copyright © 2016 Cloudworks. All rights reserved.
//

import Foundation

class NumberTextFieldTableViewCell: LabelTextFieldTableViewCell {
    
    var textField: UITextFieldNumeric? {
        return self.valueTextField as? UITextFieldNumeric
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        let textField = self.valueTextField as? UITextFieldNumeric
        valueTextField.autocorrectionType = .no
        textField!.keyboardType = .numbersAndPunctuation
        textField!.style = .numbersStyle
        textField?.maxValue = (99999999)
        textField?.minValue = (-99999999)
//        textField?.clearZero = true
//        textField?.enableNil = true
        //valueTextField.placeholder = NSLocalizedString("ENTER_NUMBER_TEXT", comment: "")
        
        self.selectionStyle = UITableViewCellSelectionStyle.none
    }
    
    override func textFieldDidEndEditing(_ textField: UITextField) {
        if let del = self.customFieldDelegate {
            del.customFieldDidChangeValue(self, value: Int(textField.text!) as AnyObject?)
        }
    }
    
    override  func setValue(_ value:AnyObject?) {
        if let v = value as? NSNumber {
            valueTextField.text = String(format:"%d",v.int32Value)
        } else {
            valueTextField.text = ""
        }
    }
    
}
