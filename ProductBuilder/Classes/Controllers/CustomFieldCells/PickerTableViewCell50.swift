//
//  PickerTableViewCell.swift
//  RPlus
//
//  Created by Alexander Martyshko on 8/25/16.
//  Copyright © 2016 Cloudworks. All rights reserved.
//

import UIKit
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}


protocol PickerTableViewCellDelegate: AnyObject {
    func pickerCellSelected(_ cell: PickerTableViewCell)
    func valueSelected(_ inCell: PickerTableViewCell, value: String?)
    func valueSelected(atIndex: Int, inCell: PickerTableViewCell)
}

class PickerTableViewCell: UITableViewCell, SimpleTableListViewControllerDelegate {
    
    static let nibName = "PickerTableViewCell50"
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var button: UIButton!
    
    weak var delegate: PickerTableViewCellDelegate?
    
    var list : SimpleTableListViewController!
    
    
    var pickerValues: [String]? {
        didSet {
            
            list.items = pickerValues
            list.preferredContentSize = CGSize(width: 320.0, height: Double((pickerValues?.count)!*44 + 44))
            list.selectedIndex = 0
            
            if let values = pickerValues , pickerValues?.count > 0 {

                if let selectedVal = selectedValue {
                    var index = 0
                    for val in values {
                        if val == selectedVal {
                            break
                        }
                        index += 1
                    }
                    
                    list.selectedIndex = index
                }
                else {
                    
                    selectedValue = values[0]
                }
            }
        }
    }
    
    var selectedValue: String? {
        didSet {
            
            self.setValueText(selectedValue)
            
            if let values = pickerValues , pickerValues?.count > 0  {
                
                if let selectedVal = selectedValue {
                    var index = 0
                    for val in values {
                        if val == selectedVal {
                            break
                        }
                        index += 1
                    }
                    
                    if list.selectedIndex != index {
                        
                        list.selectedIndex = index
                    }
                }
            }
        }
    }
    
    
    func setValueText(_ text: String?) {
        
        button.setTitle(selectedValue, for: UIControlState.normal)
    }
    
    
    override func awakeFromNib() {
        
        if UIDevice.current.userInterfaceIdiom != UIUserInterfaceIdiom.pad {
            
        }
        
        super.awakeFromNib()
        let backView = UIView()
        backView.backgroundColor = ColorUtils.ColorWithRGB(red: 217, green: 217, blue: 217)
        self.selectedBackgroundView = backView
        
        
        list = SimpleTableListViewController()
        list.preferredContentSize = CGSize(width: 320.0, height: 300.0)
        list.delegate = self
        
        self.selectionStyle = UITableViewCellSelectionStyle.none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func buttonTouched(_ sender: AnyObject) {
       
        let firstResponder = self.getFirstResponder();
        firstResponder?.resignFirstResponder();
        
        let frame = button.superview!.convert(button.frame, to: button.superview!)
        
        list.titleText = titleLabel.text
        list.presentPopover(from: frame, in: button.superview, permittedArrowDirections: UIPopoverArrowDirection.up, animated: true)
        list.cancelButton.isHidden = true
    }

    
    // MARK: - SimpleTableListViewControllerDelegate
    func simpleTableListViewController(_ controller : SimpleTableListViewController,  didSelectIndex idx :NSNumber){
    
        selectedValue = pickerValues?[idx.intValue]
        
        delegate?.valueSelected(self, value: selectedValue)
        delegate?.valueSelected(atIndex: idx.intValue, inCell: self)
    }
}
