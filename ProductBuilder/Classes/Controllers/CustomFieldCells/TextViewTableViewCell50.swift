//
//  TextViewTableViewCell.swift
//  RPlus
//
//  Created by Alexander Martyshko on 10/18/16.
//  Copyright © 2016 Cloudworks. All rights reserved.
//

import UIKit

class TextViewTableViewCell: UITableViewCell {
    
    static let nibName = "TextViewTableViewCell50"

    @IBOutlet weak var textView: RPGrowingTextView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.selectionStyle = UITableViewCellSelectionStyle.none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
