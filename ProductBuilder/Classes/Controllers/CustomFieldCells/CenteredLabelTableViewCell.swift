//
//  CenteredLabelTableViewCell.swift
//  ProductBuilder
//
//  Created by PavelGurkovskii on 1/18/17.
//  Copyright © 2017 Cloudworks. All rights reserved.
//

import Foundation

class CenteredLabelTableViewCell: UITableViewCell {
    
    @IBOutlet weak var titleLabel:UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
}
