//
//  CustomFieldTableViewCell.swift
//  RPlus
//
//  Created by PavelGurkovskii on 9/9/16.
//  Copyright © 2016 Cloudworks. All rights reserved.
//

import Foundation

enum CustomFieldsControlType: Int {
    case text = 0
    case date = 1
    case number = 2
    case decimal = 3
    case flag = 4
    case lookup = 5
}


protocol CustomFieldTableViewCellDelegate:AnyObject {
    func customFieldDidChangeValue(_ cell:CustomFieldTableViewCell, value:AnyObject?)
}

class CustomFieldTableViewCell: UITableViewCell {
    
    public var currentIndexPath : IndexPath?
    
    static var nibName = "CustomFieldTableViewCell50"
    
    static fileprivate let textCellId = "TextCell"
    static fileprivate let dateCellId = "DateCell"
    static fileprivate let numberCellId = "NumberCell"
    static fileprivate let decimalCellId = "DecimalCell"
    static fileprivate let flagCellId = "FlagCell"
    static fileprivate let lookUpId = "LookUpCell"
    
    weak var customFieldDelegate:CustomFieldTableViewCellDelegate?
    var readOnly:Bool = false {
        didSet {
            selectionStyle = readOnly ? .none : .default
        }
    }
    
    static func cellId(_ type:CustomFieldsControlType) -> String {
        switch type {
            case .text:
                return textCellId
            case .date:
                return dateCellId
            case .number:
                return numberCellId
            case .decimal:
                return decimalCellId
            case .flag:
                return flagCellId
            case .lookup:
                return lookUpId
        }
    }
    
    static func registerNibs(_ tableView:UITableView) {
        tableView.register(UINib(nibName:"LabelTextFieldTableViewCell50", bundle: nil), forCellReuseIdentifier: textCellId)
        tableView.register(UINib(nibName:"DateTableViewCell50", bundle: nil), forCellReuseIdentifier: dateCellId)
        tableView.register(UINib(nibName:"NumberTextFieldTableViewCell50", bundle: nil), forCellReuseIdentifier: numberCellId)
        tableView.register(UINib(nibName:"DecimalTextFieldTableViewCell50", bundle: nil), forCellReuseIdentifier: decimalCellId)
        tableView.register(UINib(nibName:"BoolTableViewCell50", bundle: nil), forCellReuseIdentifier: flagCellId)
        tableView.register(UINib(nibName:"TextValueTableViewCell50", bundle: nil), forCellReuseIdentifier: lookUpId)
    }
    
    func setValue(_ value:AnyObject?){
       
    }
    
    func setLabelText(_ text:String?) {
        
    }
}
