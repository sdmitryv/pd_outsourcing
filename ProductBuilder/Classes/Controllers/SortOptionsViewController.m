//
//  SortOptionsViewController.m
//  ProductBuilder
//
//  Created by Alexander Martyshko on 7/9/15.
//  Copyright (c) 2015 Cloudworks. All rights reserved.
//

#import "SortOptionsViewController.h"
#import "UIImage+Arrow.h"
#import "Global.h"

@interface SortOptionsTableViewCell : UITableViewCell {
//    UILabel *_titleLabel;
//    UIImageView *_sortArrowImage;
}
@property (nonatomic, readonly) UILabel *titleLabel;
@property (nonatomic, readonly) UIImageView *sortArrowImage;
@end


@interface SortOptionsViewController () {
    NSMutableArray *_sortOptions;
    GridTableColumnDescription *_currentSortOption;
    UIImage *_upArrowImage, *_downArrowImage;
    SortingMode _order;
}
@end

static NSString *sortCellReuseIdentifier = @"SortCell";

@implementation SortOptionsViewController

- (void)dealloc {
    [_currentSortDescription release];
    [_sortOptions release];
    [_upArrowImage release];
    [_downArrowImage release];
    [super dealloc];
}

-(id)initWithAvailableSortDescriptions:(NSArray *)sortDescriptions currentSortDescription:(GridTableColumnDescription *)currentSortDescription order:(SortingMode)order {
    self = [super init];
    if (self) {
        _sortOptions = [[NSMutableArray alloc] initWithArray:sortDescriptions];
        _order = order;
        for (GridTableColumnDescription *descr in sortDescriptions) {
            if ([descr.name isEqualToString:currentSortDescription.name])
                _currentSortDescription = [descr retain];
        }
    }
    return self;
}

- (NSString *)title {
    return NSLocalizedString(@"SORT_TITLE", nil);
}

-(void)loadView{
    
    UIView* view = [[UIView alloc]init];
    view.backgroundColor = [UIColor whiteColor];

    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone) {
        UITableView *tableView = [[UITableView alloc] initWithFrame:view.bounds style:UITableViewStylePlain];
        tableView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
        [tableView registerClass:[SortOptionsTableViewCell class] forCellReuseIdentifier:sortCellReuseIdentifier];
        tableView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        [tableView setSeparatorInset:UIEdgeInsetsZero];
        [tableView setLayoutMargins:UIEdgeInsetsZero];
        tableView.rowHeight = 50;
        tableView.dataSource = self;
        tableView.delegate = self;
        UIView *backView = [[[UIView alloc] initWithFrame:tableView.bounds] autorelease];
        backView.backgroundColor = [UIColor clearColor];
        [tableView setBackgroundView:backView];
        [tableView setBackgroundColor:[UIColor clearColor]];
        tableView.tableFooterView = [[UIView new] autorelease];
        [view addSubview:tableView];
        [tableView release];
    }

    self.view = view;
    [view release];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self.dialogViewController.bSave setTitle:NSLocalizedString(@"APPLYBTN_TITLE", nil) forState:UIControlStateNormal];
    _upArrowImage = [[UIImage arrowImageWithSize:CGSizeMake(16, 10) color:global_blue_color direction:ArrowDirectionUp] retain];
    _downArrowImage = [[UIImage arrowImageWithSize:CGSizeMake(16, 10) color:global_blue_color direction:ArrowDirectionDown] retain];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)willSave {
    if (_sortDelegate && [_sortDelegate respondsToSelector:@selector(sortOptionsController:applySortWithDescription:order:)])
        [_sortDelegate sortOptionsController:self applySortWithDescription:_currentSortDescription order:_order];
    
    return YES;
}

#pragma mark - UITableViewDataSource
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _sortOptions.count;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    SortOptionsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:sortCellReuseIdentifier forIndexPath:indexPath];
    if (!cell)
        cell = [[[SortOptionsTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:sortCellReuseIdentifier] autorelease];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    GridTableColumnDescription *description = _sortOptions[indexPath.row];
    cell.titleLabel.text = description.title;
    if (description == self.currentSortDescription) {
        switch (_order) {
            case NotSorted:
                cell.sortArrowImage.image = nil;
                cell.titleLabel.textColor = [UIColor blackColor];
                break;
            case AscendingSorted:
                cell.sortArrowImage.image = _upArrowImage;
                cell.titleLabel.textColor = global_blue_color;
                break;
            case DescendingSorted:
                cell.sortArrowImage.image = _downArrowImage;
                cell.titleLabel.textColor = global_blue_color;
                break;
            default:
                break;
        }
    }
    else {
        cell.sortArrowImage.image = nil;
        cell.titleLabel.textColor = [UIColor blackColor];
    }
    
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    GridTableColumnDescription *description = _sortOptions[indexPath.row];
    if (description != self.currentSortDescription)
        _order = AscendingSorted;
    else {
        switch (_order) {
            case NotSorted:
                _order = AscendingSorted;
                break;
            case AscendingSorted:
                _order = DescendingSorted;
                break;
            case DescendingSorted:
                _order = NotSorted;
                break;
            default:
                break;
        }
    }
    
    self.currentSortDescription = description;
    [tableView reloadData];
}

@end


@implementation SortOptionsTableViewCell

- (void)dealloc {
    [_titleLabel release];
    [_sortArrowImage release];
    [super dealloc];
}

-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        _titleLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        _titleLabel.textColor = [UIColor blackColor];
        _titleLabel.backgroundColor = [UIColor clearColor];
        _titleLabel.font = [UIFont systemFontOfSize:17];
        [self.contentView addSubview:_titleLabel];
        
        _sortArrowImage = [[UIImageView alloc] initWithFrame:CGRectZero];
        _sortArrowImage.contentMode = UIViewContentModeCenter;
        _sortArrowImage.backgroundColor = [UIColor clearColor];
        [self.contentView addSubview:_sortArrowImage];
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    const NSInteger imageWidht = 30;
    const NSInteger offset = 8;
    _titleLabel.frame = CGRectMake(offset, 0, self.contentView.bounds.size.width - 3*offset - imageWidht, self.contentView.bounds.size.height);
    _sortArrowImage.frame = CGRectMake(_titleLabel.frame.origin.x + _titleLabel.frame.size.width + offset, 0, imageWidht, self.contentView.bounds.size.height);
}

- (UIEdgeInsets)layoutMargins
{
    return UIEdgeInsetsZero;
}

@end
