//
//  SessionsViewController.m
//  ProductBuilder
//
//  Created by Julia Korevo on 9/25/14.
//  Copyright (c) 2014 Cloudworks. All rights reserved.
//

#import "SessionsViewController.h"
#import "SessionsTableViewCell.h"
#import "RTAlertView.h"
#import "FrequentBuyerProgram.h"
#import "DataUtils.h"
#import "ShopperDisplayManager.h"
#import "ModalAlert.h"
#import "FeeEditViewController.h"
#import "SyncTableOperation.h"
#import "SyncRecordStatusDeletedOperation.h"
#import "SyncCustomerSessionItemOperation.h"
#import "AutoLogOutManager.h"
#import "GeniusShopperDisplayManager.h"

@interface SessionsViewController (){
    //NSArray* tokenPrograms;
    NSArray* frequentBuyerPrograms;
}

@end

@implementation SessionsViewController

- (id)initWithReceipt:(Receipt *)receipt receiptController:(ReceiptEditViewController*)aReceiptController{
    if ((self = [super initWithNibName:@"SessionsView" bundle:nil])) {
        _receipt = [receipt retain];
        receiptController = aReceiptController;
    }
    return self;
}
- (void)dealloc
{
    [_receipt release];
    
    [_headerView release];
    [_addToReceiptButton release];
    [_selectAllButton release];
    [_unselectAllButton release];
    [_actionView release];
    [_sessionsView release];
    [_itemsView release];
    [sessionsGrid release];
    
    [_itemsTotalQty release];
    [_itemSelectedQty release];
    [sessionsArray release];
    [blocksArray release];
    [currentSession release];
    [selectedCommonArray release];
    [buttonToScroll release];
    [roomsArray release];
    [topBarV release];
    [itemsAreaScrollV release];
    [pageControl release];
    //[tokenPrograms release];
    [frequentBuyerPrograms release];
    
    [_refreshButton release];
    [_backView release];
    [super dealloc];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [_unselectAllButton setTitle:NSLocalizedString(@"SESSIONS_UNSELECT_BUTTON_TITLE", nil) forState:UIControlStateNormal];
    [_selectAllButton setTitle:NSLocalizedString(@"SESSIONS_SELECT_BUTTON_TITLE", nil) forState:UIControlStateNormal];
    [_addToReceiptButton setTitle:NSLocalizedString(@"SESSIONS_ADD_TO_RECEIPT_BUTTON_TITLE", nil) forState:UIControlStateNormal];
    [_refreshButton setTitle:NSLocalizedString(@"REFRESHBTN_TITLE", nil) forState:UIControlStateNormal];
    
    _addToReceiptButton.enabled=_selectAllButton.enabled=_unselectAllButton.enabled=NO;
    
    roomsArray=[[NSMutableArray alloc]init];
    int butonQty=IS_IPAD_PRO?19:11;
    topBarV = [[TopBarView alloc] initWithFrame:_headerView.frame array:roomsArray isDisabled:NO buttonsQty:butonQty];
    
    topBarV.addButton.hidden=YES;
    topBarV.delegate = self;
    [_headerView addSubview:topBarV.view];
    
    _sessionsView.backgroundColor=[UIColor darkGrayColor];
    
    sessionsGrid=[[UITableView alloc]initWithFrame:CGRectMake(0,0,_sessionsView.frame.size.width,_sessionsView.frame.size.height-44)];
    sessionsGrid.dataSource=self;
    sessionsGrid.delegate=self;
    sessionsGrid.backgroundColor=[UIColor darkGrayColor];
    sessionsGrid.contentSize=CGSizeMake(_sessionsView.frame.size.width-100, _sessionsView.frame.size.height-44);
    [sessionsGrid setSeparatorInset:UIEdgeInsetsMake(0, 7, 0, 7)];
    sessionsGrid.autoresizingMask=UIViewAutoresizingFlexibleHeight;
    
    [_sessionsView addSubview:sessionsGrid];

    
    itemsAreaScrollV=[[UIScrollView alloc]initWithFrame:_itemsView.frame];
    itemsAreaScrollV.backgroundColor=[UIColor clearColor];
    itemsAreaScrollV.pagingEnabled = YES;
    itemsAreaScrollV.showsHorizontalScrollIndicator = NO;
    itemsAreaScrollV.showsVerticalScrollIndicator = NO;
    itemsAreaScrollV.scrollsToTop = NO;
    itemsAreaScrollV.delegate = self;
    [_actionView addSubview:itemsAreaScrollV];
    
    pageControl=[[UIPageControl alloc]initWithFrame:CGRectMake(_itemsView.frame.origin.x+_itemsView.frame.size.width-45,186,100,30)];
    pageControl.currentPage = 0;
    pageControl.backgroundColor=[UIColor clearColor];
    pageControl.pageIndicatorTintColor=[UIColor lightGrayColor];
    pageControl.currentPageIndicatorTintColor=[UIColor colorWithRed:21.0/255.0 green:157.0/255.0 blue:211.0/255.0 alpha:1];
    pageControl.transform = CGAffineTransformMakeRotation(M_PI / 2);
    [_actionView addSubview:pageControl];
    
    sessionsArray = [[NSMutableArray alloc] initWithCapacity:0];
    blocksArray=[[NSMutableArray alloc]init];
    selectedCommonArray=[[NSMutableArray alloc]init];
    
    [self hideEmptySeparators];
    
    buttonToScroll=[[UIButton alloc]initWithFrame:CGRectMake(0, 426, 176, 39)];
    buttonToScroll.backgroundColor=[UIColor darkGrayColor];
    [buttonToScroll setImage:[UIImage imageNamed:@"gray_arrow_down"] forState:UIControlStateNormal];
    [buttonToScroll addTarget:self action:@selector(scrollButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [_sessionsView addSubview:buttonToScroll];
    buttonToScroll.autoresizingMask=UIViewAutoresizingFlexibleTopMargin;
    
    
    [self refreshDataFromServer];
    _backView.layer.borderColor=[UIColor lightGrayColor].CGColor;
    _backView.layer.borderWidth=1;
    
}
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    NSError* error = nil;
    canChangeItems = [_receipt canChangeItems:&error];
    if (!canChangeItems) {
        _addToReceiptButton.enabled=canChangeItems;
        [ModalAlert showError:error];
    }
}
- (void)refreshDataFromServer {
    
    BackgroundOperationViewController* backgroundController = [[BackgroundOperationViewController alloc] init];
    backgroundController.parentView = self.view;
    NSArray *operations = @[[SyncTableOperation syncTableOperation:@"CustomerSession" mode:SyncOperationModeSynchronization isMandatory:YES],
                            [SyncCustomerSessionItemOperation syncTableOperation:@"CustomerSessionItem" mode:SyncOperationModeSynchronization isMandatory:YES],
                            [SyncRecordStatusDeletedOperation syncTableOperation:@"SyncRecordStatusDeleted" mode:SyncOperationModeSynchronization isMandatory:YES]];
    [backgroundController setTitle:NSLocalizedString(@"SESSIONS_PROCESSING_TITLE", nil)];
    [backgroundController startTask:^{
        NSAutoreleasePool* pool = [[NSAutoreleasePool alloc] init];
        SyncOperationQueue * queue = [[[SyncOperationQueue alloc] init] autorelease];
        [queue addOperations:operations waitUntilFinished:YES breakOnError:YES];
        [pool release];
    }
                          completed:^{
                              
                              for (SyncTableOperation *oper in operations) {
                                  if ([self isOperationWithError:oper]) {
                                      return;
                                  }
                              }
                              
                              [self updateUI];
                              
                          }];
    [backgroundController release];
}

- (void)updateUI {
    [roomsArray removeAllObjects];
    
    [roomsArray addObjectsFromArray:[CustomerSession getSessionsGroupedByRoom]];
    
    CustomerSession *session=[[CustomerSession alloc]init];
    session.roomName=[NSString stringWithFormat:NSLocalizedString(@"ROOMS_TITLE_ALL", nil)];
    [roomsArray insertObject:session atIndex:0];
    [session release];
    
    NSInteger roomIndexToSelect = roomsArray.count > 0 ? 0 : -1;
    [topBarV initHeaderButtons:roomsArray selectedIndex:roomIndexToSelect];
    topBarV.addButton.hidden=YES;
    [topBarV.centerView scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:NO];
    if (roomIndexToSelect>=0)
        [self barItemSelectedAtIndex:roomIndexToSelect];
    
    
    [sessionsArray release];
    sessionsArray=[[CustomerSession getAllSessions]retain];
    [sessionsGrid reloadData];
    
    if (sessionsArray.count){
        NSInteger sessionIndexToSelect = 0;
        [sessionsGrid selectRowAtIndexPath:[NSIndexPath indexPathForRow:sessionIndexToSelect inSection:0]
                                  animated:YES scrollPosition:UITableViewScrollPositionMiddle];
        [[sessionsGrid delegate] tableView:sessionsGrid didSelectRowAtIndexPath:[NSIndexPath indexPathForRow:sessionIndexToSelect inSection:0]];
    }
    
    buttonToScroll.hidden=sessionsArray.count<=(IS_IPAD_PRO?11:7);
}

- (BOOL)isOperationWithError:(SyncOperation *)operation {
    
    if (!operation.result) {
        NSString* errorMessage = operation.lastError;
        NSError *error = operation.lastErrorObject;
        
        // 404 - not found; 503 - service unavailable; -1009 - NSURLErrorNotConnectedToInternet
        if (error && (error.code == NSURLErrorTimedOut || error.code == 404 || error.code == 503 || error.code == NSURLErrorNotConnectedToInternet || error.code == NSURLErrorCannotFindHost)) {
            [self showServerUnavailableErrorWithCompletedBlock:nil];
            return YES;
        }
        
        if (errorMessage) {
            RTAlertView* alertView = [[RTAlertView alloc]initWithTitle:NSLocalizedString(@"ERROR_TEXT", nil)
                                                               message:errorMessage
                                                              delegate:self
                                                     cancelButtonTitle:NSLocalizedString(@"OKBTN_TITLE", nil) otherButtonTitles:nil];
            [alertView show];
            [alertView release];
            return YES;
        }
    }
    
    return NO;
}

- (void)showServerUnavailableErrorWithCompletedBlock:(void(^)())completedBlock {
    RTAlertView *alert = [[RTAlertView alloc] initWithTitle:NSLocalizedString(@"SERVER_UNAVAILABLE", nil)
                                                    message:NSLocalizedString(@"SERVER_UNAVAILABLE_DESCRIPTION", nil)
                                                   delegate:nil
                                          cancelButtonTitle:NSLocalizedString(@"OKBTN_TITLE", nil) otherButtonTitles:nil, nil];
    if (completedBlock) {
        alert.completedBlock = ^(NSInteger buttonIndex) {
            completedBlock();
        };
    }
    
    [alert show];
    [alert release];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    
    if (scrollView.isDragging) {
        
        CGFloat pageH = itemsAreaScrollV.frame.size.height;
        float fractionalPage = itemsAreaScrollV.contentOffset.y / pageH;
        NSInteger page = lround(fractionalPage);
        pageControl.currentPage = page;
    }
}
-(void)scrollButtonClick{
    if(sessionsArray.count>=8)
        [sessionsGrid scrollToRowAtIndexPath:[sessionsGrid.indexPathsForVisibleRows lastObject] atScrollPosition:UITableViewScrollPositionMiddle animated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}
#pragma mark - Actions

- (IBAction)addToReceiptBtnClick:(id)sender {
    
    [self processMultiplyItems:selectedCommonArray];
    
}

- (IBAction)selectAllBtnClick:(id)sender {
    for (SessionItemBlock *iblock in blocksArray) {
        iblock.isSelected=YES;
        [iblock updateViewForState];
        if (![selectedCommonArray containsObject:iblock.sessionItem])
            [selectedCommonArray addObject:iblock.sessionItem];
        [self updateTotalView];
    }
}

- (IBAction)unselectAllBtnClick:(id)sender {
    for (SessionItemBlock *iblock in blocksArray) {
        iblock.isSelected=NO;
        [iblock updateViewForState];
        if ([selectedCommonArray containsObject:iblock.sessionItem])
            [selectedCommonArray removeObject:iblock.sessionItem];
        
        [self updateTotalView];
    }
}

- (IBAction)refreshButtonClick:(id)sender {
    [self refreshDataFromServer];
}

#pragma mark - TopBar delegate methods

-(void)barItemSelectedAtIndex:(NSInteger)index {
    [selectedCommonArray removeAllObjects];
    if (index > 0) {
        
        [self updateSessionsAreaForRoomid:((CustomerSession*)roomsArray[index]).roomId];
    }
    else {
        if (sessionsArray)
            [sessionsArray release];
        sessionsArray=[[CustomerSession getAllSessions]retain];
        sessionsGrid.hidden=NO;
    }
    [sessionsGrid reloadData];
    
    if (sessionsArray.count){
        [sessionsGrid selectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] animated:YES scrollPosition:UITableViewScrollPositionMiddle];
        [[sessionsGrid delegate] tableView:sessionsGrid didSelectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    }
    else{
        [blocksArray removeAllObjects];
        for(UIView *subview in [itemsAreaScrollV subviews]) {
            [subview removeFromSuperview];
        }
        pageControl.hidden=YES;
        if (currentSession) {
            [currentSession release];
            currentSession=nil;
        }
        [self updateTotalView];
    }
    buttonToScroll.hidden=sessionsArray.count<=7;
    sessionsGrid.scrollEnabled = sessionsArray.count>=8;
    
}
-(void)barItemLongTouchAtIndex:(NSInteger)index {
    
}
-(void)updateItemsArea{
    [blocksArray removeAllObjects];
    for(UIView *subview in [itemsAreaScrollV subviews]) {
        [subview removeFromSuperview];
    }
    
    CGFloat blockViewX = 4;
    CGFloat blockViewY = 0;
    CGFloat blockWidth = 305;
    CGFloat blockHeight = 94;
    CGFloat offset = 7;
    int blockCounter = 0;
    
    for (CustomerSessionItem *sItem in currentSession.itemsArray) {
        
        //if (!sItem.item)
        //    continue;
            
        SessionItemBlock *blockView = [[SessionItemBlock alloc]initWithFrame:CGRectMake(blockViewX,blockViewY, blockWidth,blockHeight) sessionItem:sItem];
        
        if ([selectedCommonArray containsObject:sItem]){
            
            blockView.isSelected = YES;
            [blockView updateViewForState];
        }
        
        blockView.delegate = self;
        [itemsAreaScrollV addSubview:blockView];
        [blocksArray addObject:blockView];
        [blockView updateData];
        
        [blockView release];
        blockCounter++;
        
        if(blockCounter%2 == 0){
            
            blockViewX = 4;
            blockViewY = blockViewY+blockHeight+offset+4;
        }
        else
            blockViewX=blockViewX+blockWidth+offset+4;
        
    }
    
    float variable = ceil((float)blockCounter/(float)8);
    
    CGSize scrollViewContentSize = CGSizeMake(self.itemsView.frame.size.width, variable*self.itemsView.frame.size.height);
    [itemsAreaScrollV setContentSize:scrollViewContentSize];
    pageControl.hidden=variable == 1 ? YES : NO;
    pageControl.numberOfPages = variable;
    [itemsAreaScrollV setContentOffset:CGPointZero animated:NO];
    
    [self updateTotalView];
}

-(void)updateSessionsAreaForRoomid:(BPUUID*)roomId{
    if (sessionsArray)
        [sessionsArray release];
    
    sessionsArray=[[CustomerSession getSessionsByRoomId:roomId]retain];
    
    sessionsGrid.hidden=(!sessionsArray.count)?YES:NO;
}
-(void)updateTotalView{
    int qty=0;
    for (CustomerSessionItem *sesItem in selectedCommonArray) {
        if ([sesItem.customerSessionId isEqual:currentSession.id])
            qty++;
    }
    _itemSelectedQty.text=[NSString stringWithFormat:NSLocalizedString(@"TOTAL_VIEW_QTY_SELECTED", nil),qty];
    _itemsTotalQty.text=[NSString stringWithFormat:NSLocalizedString(@"TOTAL_VIEW_TOTAL_QTY", nil),currentSession?currentSession.itemsArray.count:0];
    
    
    _selectAllButton.enabled=(qty==currentSession.itemsArray.count)?NO:YES;
    _unselectAllButton.enabled=(qty>0)?YES:NO;
    
    if (canChangeItems)
        _addToReceiptButton.enabled=selectedCommonArray.count;
    else
        _addToReceiptButton.enabled=NO;
        
}
#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return sessionsArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    SessionsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (!cell) {
        
        cell = [[[SessionsTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier ] autorelease];
    }
    cell.session=sessionsArray[indexPath.row];
    
    return cell;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
     [selectedCommonArray removeAllObjects];
    if (currentSession) {
        [currentSession release];
        currentSession=nil;
    }
    currentSession=[(CustomerSession*)sessionsArray[indexPath.row] retain];
    
    [self updateItemsArea];
    pageControl.currentPage = 0;
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60;
}

- (void)hideEmptySeparators
{
    UIView *v = [[UIView alloc] initWithFrame:CGRectZero];
    v.backgroundColor = [UIColor clearColor];
    [sessionsGrid setTableFooterView:v];
    [v release];
}
#pragma mark - SessionItemBlock delegate method

-(void)itemButtonClick:(id)sender{
    if ([sender isKindOfClass:[SessionItemBlock class]]) {
        SessionItemBlock* block = (SessionItemBlock*)sender;
        block.isSelected=!block.isSelected;
        [block updateViewForState];
        
        if (![selectedCommonArray containsObject:block.sessionItem])
            [selectedCommonArray addObject:block.sessionItem];
        else if ([selectedCommonArray containsObject:block.sessionItem])
            [selectedCommonArray removeObject:block.sessionItem];
        
        [self updateTotalView];
    }
}

#pragma mark -add to receipt methods

-(void)processMultiplyItems:(NSArray*)items{
    
    NSMutableArray* selectedItems = [NSMutableArray arrayWithArray:items];
    
    BackgroundOperationViewController* backgroundController = [[BackgroundOperationViewController alloc] init];
    [backgroundController setTitle:NSLocalizedString(@"ITEMS_VALIDATING", nil)];
    backgroundController.loopProgress = FALSE;
    backgroundController.progress = 0;
    [backgroundController startTask:^{
        
        dispatch_semaphore_t sem = dispatch_semaphore_create(0);
        for (__block int i = 0; i < selectedItems.count; i++) {
            
            NSError* validateError = nil;
            CustomerSessionItem *sessionItem=selectedItems[i];
            Item* item =sessionItem.item;
            dispatch_sync(dispatch_get_main_queue(), ^{
                backgroundController.progress = (float)(i+1)/(float)selectedItems.count;
            });
            if (![self validateItem:item error:&validateError]) {
                if (validateError){
                    //show error and wait
                    dispatch_sync(dispatch_get_main_queue(), ^{
                        RTAlertView *alertView = [[RTAlertView alloc] initWithTitle:validateError.localizedDescription message:validateError.localizedFailureReason delegate:nil cancelButtonTitle:NSLocalizedString(@"OKBTN_TITLE", nil) otherButtonTitles:nil];
                        alertView.completedBlock = ^(NSInteger buttonIndex){
                            dispatch_semaphore_signal(sem);
                        };
                        [alertView show];
                        [alertView release];
                    });
                    dispatch_semaphore_wait(sem, DISPATCH_TIME_FOREVER);
                }
                [selectedItems removeObjectAtIndex:i--];
            }
            else if (item.isDiscontinued) { // check if item isDiscontinued
                //show error and wait
                dispatch_sync(dispatch_get_main_queue(), ^{
                    RTAlertView *alert = [[RTAlertView alloc] initWithTitle:NSLocalizedString(@"SALE_ITEM_DISCONTINUED_WARNING", nil)
                                                                    message:nil
                                                                   delegate:nil
                                                          cancelButtonTitle:NSLocalizedString(@"YESBTN_TITLE", nil) otherButtonTitles:NSLocalizedString(@"NOBTN_TITLE", nil), nil];
                    alert.completedBlock = ^(NSInteger buttonIndex) {
                        if (buttonIndex == 1) { // NO
                            [selectedItems removeObjectAtIndex:i--];
                        }
                        dispatch_semaphore_signal(sem);
                    };
                    [alert show];
                    [alert release];
                });
                dispatch_semaphore_wait(sem, DISPATCH_TIME_FOREVER);
            }
        }
        
        dispatch_sync(dispatch_get_main_queue(), ^{
            [backgroundController setTitle:NSLocalizedString(@"ITEMS_ADDING", nil)];
            backgroundController.progress = 0.0f;
        });
        int i = 1;
        NSMutableArray* addedReceiptItems = [[NSMutableArray alloc]init];
        [_receipt beginUpdatingItems];
        
        for (CustomerSessionItem* sessionItem in selectedItems){
            dispatch_sync(dispatch_get_main_queue(), ^{
                backgroundController.progress = (float)(i)/(float)selectedItems.count;
            });
            NSError *error = nil;
            
            ReceiptItem* receiptItem = [self addItemToReceipt:sessionItem.item error:&error];
            if (receiptItem){
                receiptItem.customerSessionItemID=sessionItem.id;
                [addedReceiptItems addObject:receiptItem];
                [[AutoLogOutManager instance] Stop];
            }
            else if (error){
                
                dispatch_sync(dispatch_get_main_queue(), ^{
                    
                    [ModalAlert showError:error completed:^{
                        dispatch_semaphore_signal(sem);
                    }];
                });
                dispatch_semaphore_wait(sem, DISPATCH_TIME_FOREVER);
            }
            i++;
        }
        
        [_receipt endUpdatingItems];
        
        dispatch_sync(dispatch_get_main_queue(), ^{
            [backgroundController setTitle:NSLocalizedString(@"ITEMS_HANDLING", nil)];
        });
        i = 1;
        for (ReceiptItem* receiptItem in addedReceiptItems){
            dispatch_sync(dispatch_get_main_queue(), ^{
                backgroundController.progress = (float)i/(float)addedReceiptItems.count;
                [self handleItemFees:receiptItem completed:^(ReceiptItem * ri) {
                    dispatch_semaphore_signal(sem);
                }];
            });
            dispatch_semaphore_wait(sem, DISPATCH_TIME_FOREVER);
            i++;
        }
        dispatch_release(sem);
        [[GeniusShopperDisplayManager sharedInstance] addItems:addedReceiptItems toReceipt:_receipt];
        [[ShopperDisplayManager sharedInstance].currentPosAssistant addItems:addedReceiptItems toReceipt:_receipt];
        [addedReceiptItems release];
        
    } completed:^{
        [selectedCommonArray removeAllObjects];
        [self updateItemsArea];
    }];
    [backgroundController release];
}
-(void)handleItemFees:(ReceiptItem*)receiptItem completed:(void(^)(ReceiptItem*))completed{
    
    if (!CPDecimalLessThan0(receiptItem.qty) && receiptItem.item.defaultServiceFeeId){
        Fee* serviceFee = [Fee getInstanceById:receiptItem.item.defaultServiceFeeId];
        if (serviceFee){
            if (serviceFee.promptType==FeePromptTypeAutoAdd){
                [receiptItem addSalesFee:serviceFee];
                [[GeniusShopperDisplayManager sharedInstance] showReceipt:_receipt];
                [[ShopperDisplayManager sharedInstance].currentPosAssistant showReceipt:_receipt];
            }
            else if (serviceFee.promptType==FeePromptTypeAutoSuggest){
                [receiptItem.feeList beginEdit];
                id<ISalesFee> salesFee = [receiptItem addSalesFee:serviceFee];
                FeeEditViewController* feeEditViewController = [[FeeEditViewController alloc]initWithSalesFee:salesFee];
                [feeEditViewController showModal:^{
                    if (feeEditViewController.dialogViewController.dialogResult==RTDialogResultOK){
                        [receiptItem.feeList endEdit];
                        [[GeniusShopperDisplayManager sharedInstance] showReceipt:_receipt];
                        [[ShopperDisplayManager sharedInstance].currentPosAssistant showReceipt:_receipt];
                    }
                    else{
                        [receiptItem.feeList cancelEdit];
                    }
                    if (completed){
                        completed(receiptItem);
                    }
                }];
                [feeEditViewController release];
                return;
            }
        }
    }
    if (completed){
        completed(receiptItem);
    }
    
}
-(ReceiptItem*)addItemToReceipt:(Item*)item  error:(NSError**)error{
   
        
   if (item)
        return (ReceiptItem*)[_receipt addItem:item qty:!item.qty ? CPDecimalFromInt(1) : item.qty.decimalValue error:error];
    
    return  nil;
}
//-(NSArray*)tokenPrograms{
//    if (!tokenPrograms)
//        tokenPrograms = [[TokenProgram getProgramsList]retain];
//    return tokenPrograms;
//}

-(NSArray*)frequentBuyerPrograms{
    if (!frequentBuyerPrograms)
        frequentBuyerPrograms = [[FrequentBuyerProgram getProgramsList]retain];
    return frequentBuyerPrograms;
}
-(BOOL)validateItem:(Item *)item error:(NSError**)error{
    
    if (!item)
        return NO;
    
    if (!item.isReleased) {
        [CEntity writeError:error title:NSLocalizedString(@"ITEM_NOT_RELEASED_TITLE", nil) description:[NSString stringWithFormat:NSLocalizedString(@"ITEM_NOT_RELEASED_MSG", nil), [DataUtils readableDateStringFromDate:item.releaseDateTime]] domain:nil];
        return NO;
    }
    
    if ([ReceiptItem isNonInventoryPLU:item.plu]) {
        
        [CEntity writeError:error title:NSLocalizedString(@"ERROR_TEXT", nil) description:NSLocalizedString(@"ITEM_NOT_INVENTORY_MSG", nil) domain:nil];
        return NO;
    }
    
//    for (TokenProgram* program in self.tokenPrograms) {
//        
//        if (item.plu == program.adjustmentPLU || item.plu == program.redemptionPLU) {
//            [CEntity writeError:error title:NSLocalizedString(@"SALE_ITEM_TOKEN_SPECIAL_MANUALADD_TITLE", nil) description:NSLocalizedString(@"SALE_ITEM_TOKEN_SPECIAL_MANUALADD_MSG", nil) domain:nil];
//            return NO;
//        }
//    }
    
    for (FrequentBuyerProgram* prog in self.frequentBuyerPrograms) {
        if (item.plu == prog.adjustItemPLU) {
            [CEntity writeError:error title:NSLocalizedString(@"SALE_ITEM_FB_SPECIAL_MANUALADD_TITLE", nil) description:NSLocalizedString(@"SALE_ITEM_FB_SPECIAL_MANUALADD_MSG", nil) domain:nil];
            return NO;
        }
    }
    
    if (!item.isNotDeliverable && ![MembershipLevel isMembershipItemId:item.id]) { // if item is eligible for delivery
        [CEntity writeError:error title:nil description:NSLocalizedString(@"SO_ITEM_ELIGIBLE_ONLY_FOR_DELIVERY_ORDERS_MESSAGE", nil) domain:@"com.soItem.validate"];
        return NO;
    }
    
    return YES;
}


@end
