//
//  SessionsViewController.h
//  ProductBuilder
//
//  Created by Julia Korevo on 9/25/14.
//  Copyright (c) 2014 Cloudworks. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Receipt.h"
#import "ReceiptEditViewController.h"
#import "PreSetsChooseItemView.h"
#import "GridTable.h"
#import "SessionItemBlock.h"
#import "CustomerSession.h"

@interface SessionsViewController : UIViewController<TopBarViewDelegate,UIScrollViewDelegate,UITableViewDataSource,UITableViewDelegate, SessionItemBlockDelegate>{
    Receipt* _receipt;
    ReceiptEditViewController* receiptController;
    
    TopBarView *topBarV;
    UITableView *sessionsGrid;
    UIScrollView *itemsAreaScrollV;
    UIPageControl *pageControl;
    
    NSMutableArray *sessionsArray;
    NSMutableArray *roomsArray;
    NSMutableArray *blocksArray;
    
    CustomerSession *currentSession;
    NSMutableArray *selectedCommonArray;
    UIButton *buttonToScroll;
    BOOL canChangeItems;
}
@property (retain, nonatomic) IBOutlet UIView *headerView;
@property (retain, nonatomic) IBOutlet MOGlassButtonMarine *addToReceiptButton;
@property (retain, nonatomic) IBOutlet MOGlassButtonBlack *selectAllButton;
@property (retain, nonatomic) IBOutlet MOGlassButtonBlack *unselectAllButton;
@property (retain, nonatomic) IBOutlet UIView *actionView;
@property (retain, nonatomic) IBOutlet UIRoundedCornersView *sessionsView;
@property (retain, nonatomic) IBOutlet UIRoundedCornersView *itemsView;
@property (retain, nonatomic) IBOutlet UILabel *itemsTotalQty;
@property (retain, nonatomic) IBOutlet UILabel *itemSelectedQty;
@property (retain, nonatomic) IBOutlet MOGlassButtonBlack *refreshButton;
@property (retain, nonatomic) IBOutlet UIRoundedCornersView *backView;

- (IBAction)addToReceiptBtnClick:(id)sender;
- (IBAction)selectAllBtnClick:(id)sender;
- (IBAction)unselectAllBtnClick:(id)sender;
- (IBAction)refreshButtonClick:(id)sender;

- (id)initWithReceipt:(Receipt *)receipt receiptController:(ReceiptEditViewController*)aReceiptController;

@end
