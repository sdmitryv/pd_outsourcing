//
//  BaseDetailsViewController.h
//  CloudworksPOS
//
//  Created by Lulakov Viacheslav on 7/11/11.
//  Copyright 2011 Cloudworks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BaseDetailsViewController : UIViewController {
    
}

- (void) displayItem:(id)item;

@end
