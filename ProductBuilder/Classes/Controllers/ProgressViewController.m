//
//  Class.m
//  iPadPOS
//
//  Created by valera on 5/5/11.
//  Copyright 2011 DBBestTechnologies. All rights reserved.
//

#import "ProgressViewController.h"
#import "QuartzCore/QuartzCore.h"
#import "Global.h"

@interface ProgressViewController(){
    BOOL blinking;
    BOOL isViewAppeared;
    BOOL webViewIsLoaded;
}
@property (retain, nonatomic) NSString* titleText;
@property (retain, nonatomic) NSString* detailsText;
@property (retain, nonatomic) NSString* bottomTitleText;

@end

@implementation ProgressViewController
@synthesize vTitleLabel,
vBottomTitleLabel;
@synthesize vErrorMessageView;
@synthesize imageOk;
@synthesize imageError;
@synthesize colorTitleSuccess,
colorDetailsSuccess,
colorTitleError,
colorDetailsError,
colorTitleBottom,
fontTitle,
fontDetails,
fontTitleBottom,

imageOkDefault,
imageErrorDefault,
colorTitleSuccessDefault,
colorDetailsSuccessDefault,
colorTitleErrorDefault,
colorDetailsErrorDefault,
colorTitleBottomDefault,
fontTitleDefault,
fontDetailsDefault,
fontTitleBottomDefault,
imageSize;


- (id)init{
	self = [super initWithNibName:@"ProgressView" bundle:[NSBundle mainBundle]];
    if (self) {
        queue = dispatch_queue_create("com.cloudworks.bkgrAuth", nil);
        
        self.imageOkDefault = self.imageOk = [UIImage imageNamed:@"ok.png"];
        self.imageErrorDefault = self.imageError = [UIImage imageNamed:@"cancel.png"];
        self.colorTitleErrorDefault = self.colorDetailsErrorDefault = self.colorTitleError = self.colorDetailsError = [UIColor redColor];
        self.colorTitleSuccessDefault = self.colorTitleSuccess = MO_RGBCOLOR(31,130,81);
        self.colorDetailsSuccessDefault = self.colorDetailsSuccess = MO_RGBCOLOR(40,73,172);
        self.colorTitleBottomDefault = [UIColor redColor];
    }
    return self;
}

- (void)dealloc{
    dispatch_sync(queue, ^{});
    dispatch_release(queue);
    self.titleText = nil;
    self.detailsText = nil;
    self.bottomTitleText = nil;
    [colorTitleSuccess release];
    [colorDetailsSuccess release];
    [colorTitleError release];
    [colorDetailsError release];
    [progressView release];
    [authorizationProgressView release];
    [resultImageView release];
    [processingTextLabel release];
    [vTitleLabel release];
    [vErrorMessageView release];
    [fontTitle release];
    [fontDetails release];
    
    [imageOkDefault release];
    [imageErrorDefault release];
    [colorTitleSuccessDefault release];
    [colorDetailsSuccessDefault release];
    [colorTitleErrorDefault release];
    [colorDetailsErrorDefault release];
    [fontTitleDefault release];
    [fontDetailsDefault release];
    
    [vBottomTitle release];
    [vBottomTitleLabel release];
    
    [backgroundController release];

    [_detailsTextView release];
    [imageOk release];
    [imageError release];
    [colorTitleBottom release];
    [fontTitleBottom release];
    [colorTitleBottomDefault release];
    [fontTitleBottomDefault release];
    [super dealloc];
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle


- (void)viewDidLoad {
    [super viewDidLoad];
    self.titleText =
    self.detailsText =
    self.bottomTitleText =
    vTitleLabel.text = self.detailsTextView.text = vBottomTitleLabel.text = nil;
    verticalSpaceBetweenTitleAndDetails = self.detailsTextView.frame.origin.y - (vTitleLabel.frame.origin.y + vTitleLabel.frame.size.height);
    verticalSpaceBetweenDetailsAndBottomTitle = vBottomTitle.frame.origin.y - (self.detailsTextView.frame.origin.y + self.detailsTextView.frame.size.height);
    horizontalSpaceBetweenImageAndTitle = vTitleLabel.frame.origin.x - (resultImageView.frame.origin.x + resultImageView.frame.size.width);
    rigthMarginOfTitleAndDetailsOriginal = vTitleLabel.superview.bounds.size.width - (vTitleLabel.frame.origin.x + vTitleLabel.frame.size.width);
    self.fontTitleDefault = self.fontTitle = vTitleLabel.font;
    self.fontDetailsDefault = self.fontDetails = self.detailsTextView.font;
    self.fontTitleBottom = self.fontTitleBottomDefault = vBottomTitleLabel.font;
    if (CGSizeEqualToSize(CGSizeZero, imageSize)){
        imageSize = resultImageView.bounds.size;
    }
    
    //[authorizationProgressView setProgressTintColor:MO_RGBCOLOR(153, 188, 237)];
    CGRect progressFrame = authorizationProgressView.frame;
    progressFrame.size.height = 9;
    authorizationProgressView.frame = progressFrame;
    
    [authorizationProgressView setProgressTintColors:@[MO_RGBCOLOR(154, 187, 236), MO_RGBCOLOR(154, 187, 236)]];
    authorizationProgressView.stripesColor = MO_RGBCOLOR(110, 134, 170);
    
    authorizationProgressView.progress = 1.0f;
}

#pragma mark -
#pragma mark Progress

-(void)hideAuthozisationResultAnimated:(BOOL)animated completed:(void(^)())completed{
    dispatch_block_t block = ^{
        resultImageView.alpha =
        vTitleLabel.alpha =
        self.detailsTextView.alpha = 0;
        vBottomTitle.alpha = 0;
        isErrorShown = FALSE;
        self.titleText = nil;
        self.detailsText = nil;
        self.bottomTitleText = nil;
    };
    void (^completion)(BOOL)  = ^(BOOL finished){
        vTitleLabel.text = self.titleText;
        self.detailsTextView.text = self.detailsText;
        vBottomTitleLabel.text = self.bottomTitleText;
        if (completed)
            completed();
    };
    if (!vTitleLabel.text.length && !self.detailsTextView.text.length && !vBottomTitleLabel.text.length){
        animated = FALSE;
    }
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad || IS_IPHONE_6_OR_MORE) {
        if (animated){
            [UIView animateWithDuration:0.3f delay:0 options:UIViewAnimationOptionAllowUserInteraction animations:block completion:completion];
        }
        else{
            block();
            completion(TRUE);
        }
    }
    else {
        [backgroundController hide];
        [backgroundController release];
        backgroundController = nil;
    }
}

-(void)hideAuthozisationResultAnimated:(BOOL)animated {
    [self hideAuthozisationResultAnimated:animated completed:nil];
}

-(void)showBottomTitle:(NSString *)bottomTitle animated:(BOOL)animated {
    [self showBottomTitle:bottomTitle animated:animated onFinishBlock:nil];
}

-(void)showBottomTitle:(NSString*)bottomTitle animated:(BOOL)animated onFinishBlock:(void(^)())onFinishBlock {
    UIImage* oldImageOk = nil;
    UIImage* oldImageError = nil;
    if (!resultImageView.image){
        oldImageOk = [imageOk retain];
        oldImageError = [imageError retain];
        self.imageError = nil;
        self.imageOk = nil;
    }
    if (self.detailsTextView.text.length > 0 || vErrorMessageView.superview) {
        [self showAuthorizationResult:vTitleLabel.text detailsText:self.detailsTextView.text bottomTitleText:bottomTitle isError:isErrorShown animated:animated onFinishBlock:onFinishBlock];
    }
    else {
        [self showAuthorizationResult:vTitleLabel.text detailsText:bottomTitle bottomTitleText:nil isError:isErrorShown animated:animated onFinishBlock:onFinishBlock];
    }
    if(oldImageOk)
        self.imageOk = oldImageOk;
    if(oldImageError)
        self.imageError = oldImageError;
    [oldImageOk release];
    [oldImageError release];
}

- (void)showAuthorizationResult:(NSString*)titleText detailsText:(NSString*)detailsText
                        isError:(BOOL)isError animated:(BOOL)animated{
    [self showAuthorizationResult:titleText detailsText:detailsText bottomTitleText:vBottomTitleLabel.text isError:isError animated:animated onFinishBlock:nil];
}


// onFinishBlock only used for iPhone/iPod versions, because auth result is presented like dialog
- (void)showAuthorizationResult:(NSString*)titleText detailsText:(NSString*)detailsText 
                        bottomTitleText:(NSString*)bottomTitleText
                        isError:(BOOL)isError animated:(BOOL)animated
                  onFinishBlock:(void(^)())onFinishBlock{
    //[self.view.layer removeAllAnimations];
    
    isErrorShown = isError;
    resultImageView.alpha =
    vTitleLabel.alpha =
    self.detailsTextView.alpha =
    vBottomTitle.alpha = 0;
    vBottomTitle.hidden = (!bottomTitleText);
    
    if (titleText.length > 0) {
        resultImageView.image = isError ? imageError : imageOk;
    }
    else {
        resultImageView.image = nil;
    }
    self.titleText = vTitleLabel.text = titleText;
    self.detailsText = self.detailsTextView.text = detailsText;
    
    self.bottomTitleText = vBottomTitleLabel.text = bottomTitleText;
    
    if(isError){
        vTitleLabel.textColor = colorTitleError;
        self.detailsTextView.textColor = colorDetailsError;
    }
    else{
        vTitleLabel.textColor = colorTitleSuccess;
        self.detailsTextView.textColor = colorDetailsSuccess;
    }
    vTitleLabel.font = self.fontTitle;
    self.detailsTextView.font = self.fontDetails;
    vBottomTitleLabel.font = self.fontTitleBottom;
    vTitleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    vTitleLabel.numberOfLines = 0;
    
    resultImageView.hidden = (!resultImageView.image);
    
    CGRect imageViewRect;
    if (resultImageView.image){
        imageViewRect = CGRectMake(resultImageView.frame.origin.x, resultImageView.frame.origin.y, imageSize.width, imageSize.height);
    }
    else{
        imageViewRect = CGRectMake(resultImageView.frame.origin.x, resultImageView.frame.origin.y, 0, 0);
    }
    resultImageView.frame = imageViewRect;
    
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad || IS_IPHONE_6_OR_MORE) {
        CGRect frame = vTitleLabel.frame;
        frame.origin.x = imageViewRect.origin.x + imageViewRect.size.width + horizontalSpaceBetweenImageAndTitle;
        frame.size.width = vTitleLabel.superview.bounds.size.width - rigthMarginOfTitleAndDetailsOriginal - frame.origin.x;
        frame.size.height = 0;
        vTitleLabel.frame = frame;
        [vTitleLabel sizeToFit];
        
        frame = self.detailsTextView.frame;
        frame.origin.y = vTitleLabel.frame.origin.y + vTitleLabel.frame.size.height + verticalSpaceBetweenTitleAndDetails;
        self.detailsTextView.frame = frame;
        frame.origin.x = (imageViewRect.origin.y + imageViewRect.size.height) > self.detailsTextView.frame.origin.y ? imageViewRect.origin.x + imageViewRect.size.width + horizontalSpaceBetweenImageAndTitle : imageViewRect.origin.x;
        frame.size.width = self.detailsTextView.superview.bounds.size.width - rigthMarginOfTitleAndDetailsOriginal - frame.origin.x;
        frame.size.height = self.detailsTextView.superview.frame.size.height - self.detailsTextView.frame.origin.y - verticalSpaceBetweenDetailsAndBottomTitle - (!vBottomTitle.hidden ? vBottomTitle.frame.size.height : 0);
        self.detailsTextView.frame = frame;

        [UIView animateWithDuration:animated ? 0.3f:0.0f delay:0.0f options: UIViewAnimationCurveEaseInOut | UIViewAnimationOptionBeginFromCurrentState | UIViewAnimationOptionAllowUserInteraction animations:^{
            
            resultImageView.alpha =
            vTitleLabel.alpha =
            self.detailsTextView.alpha =
            vBottomTitle.alpha = 1;
            
            if (progressView.superview && CGRectContainsRect(self.view.bounds,  progressView.frame)){
                progressView.frame =  CGRectMake(10, self.view.bounds.size.height, self.view.bounds.size.width-20, self.view.bounds.size.height-20);
            }
            
        }
                         completion:^(BOOL finished){
                             //animate icon
                             if (animated){
                                 [self bounceIcon];
                             }
                         }];
    }
    else {
        CGRect frame = vTitleLabel.frame;
        frame.origin.x = imageViewRect.origin.x + imageViewRect.size.width + horizontalSpaceBetweenImageAndTitle;
        frame.size.width = self.view.bounds.size.width - rigthMarginOfTitleAndDetailsOriginal - frame.origin.x;
        frame.size.height = 0;
        vTitleLabel.frame = frame;
        [vTitleLabel sizeToFit];
        
        frame = self.detailsTextView.frame;
        frame.origin.y = vTitleLabel.frame.origin.y + vTitleLabel.frame.size.height + verticalSpaceBetweenTitleAndDetails;
        self.detailsTextView.frame = frame;
        frame.origin.x = (imageViewRect.origin.y + imageViewRect.size.height) > self.detailsTextView.frame.origin.y ? imageViewRect.origin.x + imageViewRect.size.width + horizontalSpaceBetweenImageAndTitle : imageViewRect.origin.x;
        frame.size.width = self.view.bounds.size.width - rigthMarginOfTitleAndDetailsOriginal - frame.origin.x;
        frame.size.height = self.view.frame.size.height - self.self.detailsTextView.frame.origin.y - verticalSpaceBetweenDetailsAndBottomTitle - (!vBottomTitle.hidden ? vBottomTitle.frame.size.height : 0);
        self.detailsTextView.frame = frame;
        
        if ([self.detailsTextView.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].length == 0) {
            CGFloat contentWidth = resultImageView.frame.size.width + horizontalSpaceBetweenImageAndTitle + vTitleLabel.frame.size.width;
            NSInteger newX = (self.view.frame.size.width - contentWidth)/2;
            resultImageView.frame = CGRectMake(newX, resultImageView.frame.origin.y, resultImageView.frame.size.width, self.view.frame.size.height - 2*vTitleLabel.frame.origin.y - 50); // 50 - height of cancel button
            vTitleLabel.frame = CGRectMake(resultImageView.frame.origin.x + resultImageView.frame.size.width + horizontalSpaceBetweenImageAndTitle, vTitleLabel.frame.origin.y,
                                           vTitleLabel.frame.size.width, resultImageView.frame.size.height);
        }
        
        resultImageView.alpha =
        vTitleLabel.alpha =
        self.detailsTextView.alpha =
        vBottomTitle.alpha = 1;
        [backgroundController hide];
        [backgroundController release];
        backgroundController = [[BackgroundOperationViewController alloc] initWithStyle:BackgroundOperationStyleDefault];
        backgroundController.hideCancel = NO;
        NSTimeInterval duration = 0.0;
        if (!isError) {
            [backgroundController.bCancel setTitle:NSLocalizedString(@"CONTINUEBTN_TITLE", nil) forState:UIControlStateNormal];
            duration = 2.3;
        }
        backgroundController.onCancel = ^{
            [backgroundController hide];
            if (onFinishBlock) {
                onFinishBlock();
            }
        };
        [backgroundController showView:self.view duration:duration];
    }
}

-(void)bounceIcon{
    UIView* animateView = resultImageView;
    CALayer *layer = animateView.layer;
    CAKeyframeAnimation *bounceAnimation = [CAKeyframeAnimation animationWithKeyPath:@"position"];
    bounceAnimation.removedOnCompletion = NO;
    // Create the path for the bounces
    CGMutablePathRef thePath = CGPathCreateMutable();
    CGFloat animationDuration = 0.2f;
    CGFloat midX = animateView.center.x;
    CGFloat midY = animateView.center.y;
    CGFloat originalOffsetX = 0;
    CGFloat originalOffsetY = -10;
    CGFloat offsetDivider = 1.4f;
    
    BOOL stopBouncing = NO;
    
    // Start the path at the placard's current location
    CGPathMoveToPoint(thePath, NULL, midX, midY);
    CGPathAddLineToPoint(thePath, NULL, midX + originalOffsetX, midY + originalOffsetY);
    CGPathAddLineToPoint(thePath, NULL, midX, midY);
    
    // Add to the bounce path in decreasing excursions from the center
    while (stopBouncing != YES) {
        CGPathAddLineToPoint(thePath, NULL, midX + originalOffsetX/offsetDivider, midY + originalOffsetY/offsetDivider);
        CGPathAddLineToPoint(thePath, NULL, midX, midY);
        
        offsetDivider += 0.5;
        animationDuration += 0.5/offsetDivider;
        if ((fabs(originalOffsetX/offsetDivider) < 5) && (fabs(originalOffsetY/offsetDivider) < 5)) {
            stopBouncing = YES;
        }
    }
    
    bounceAnimation.path = thePath;
    bounceAnimation.duration = animationDuration;
    bounceAnimation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseIn];
    CGPathRelease(thePath);
    //bounceAnimation.delegate = self;
    // Add the animation to the layer
    [layer addAnimation:bounceAnimation forKey:@"bounce"];
}

- (void)showTextMessage:(NSString *)message title:(NSString *)title {
    vErrorMessageView.frame = CGRectMake(resultImageView.frame.origin.x, resultImageView.frame.origin.y, self.detailsTextView.frame.origin.x + self.detailsTextView.frame.size.width, self.detailsTextView.frame.origin.y + self.detailsTextView.frame.size.height);
    [self.view addSubview:vErrorMessageView];
    UILabel *titleLabel = [vErrorMessageView viewWithTag:1];
    UIWebView *messageWebView = [vErrorMessageView viewWithTag:2];
    titleLabel.alpha = 0;
    titleLabel.text = title;
    messageWebView.alpha = 0;
    messageWebView.delegate = self;
    [messageWebView loadHTMLString:message baseURL:nil];
    blinking = TRUE;
    [self animateBlinkingTitle];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView{
    webViewIsLoaded = TRUE;
    if (isViewAppeared && webView.alpha == 0){
        webView.alpha = 0;
        [UIView animateWithDuration:0.3f delay:0.0f options:UIViewAnimationOptionCurveEaseInOut | UIViewAnimationOptionAllowUserInteraction animations:^{
            webView.alpha = 1;
        } completion:nil];
    }
}

-(void)animateBlinkingTitle{
    if (blinking && isViewAppeared){
        UILabel *titleLabel = [vErrorMessageView viewWithTag:1];
        [titleLabel.layer removeAllAnimations];
        titleLabel.alpha = 0;
        [UIView animateWithDuration:0.5f delay:0.0f options:UIViewAnimationOptionCurveEaseInOut | UIViewAnimationOptionAllowUserInteraction | UIViewAnimationOptionAutoreverse | UIViewAnimationOptionRepeat animations:^{
            titleLabel.alpha = 1;
        } completion:nil];
        if (isViewAppeared && webViewIsLoaded){
            [self webViewDidFinishLoad:[vErrorMessageView viewWithTag:2]];
        }
    }
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    isViewAppeared = TRUE;
    [self animateBlinkingTitle];
}

- (void)hideTextMessage {
    blinking = FALSE;
    UILabel *titleLabel = [vErrorMessageView viewWithTag:1];
    [titleLabel.layer removeAllAnimations];
    webViewIsLoaded = FALSE;
    [vErrorMessageView removeFromSuperview];
}

- (void)start {
    [self hideAuthozisationResultAnimated:TRUE];
    progressView.frame =  CGRectMake(10, self.view.bounds.size.height, self.view.bounds.size.width-20, self.view.bounds.size.height-20);
    authorizationProgressView.progress = 1;
    if (!progressView.superview){
        progressView.layer.cornerRadius = 5.0f;
        progressView.layer.borderColor = UIColor.lightGrayColor.CGColor;
        progressView.layer.borderWidth = 1.0f;
        progressView.layer.shadowColor = UIColor.blackColor.CGColor;
        progressView.layer.shadowOpacity = 0.8;
        progressView.layer.shadowRadius = 5.0f;
        progressView.layer.shadowOffset = CGSizeMake(5, 5);
        progressView.clipsToBounds = NO;
        progressView.layer.masksToBounds = NO;
        [self.view addSubview:progressView];
    }
    [UIView animateWithDuration:0.3f delay:0.0f options:UIViewAnimationOptionLayoutSubviews | UIViewAnimationOptionBeginFromCurrentState | UIViewAnimationOptionAllowUserInteraction animations:^{
        //progressView.frame = self.view.bounds;
        progressView.frame = CGRectMake(10, 10, self.view.bounds.size.width-20, self.view.bounds.size.height-20);
    } completion:^(BOOL finished){
    }];
}

- (void)startAuthorization:(dispatch_block_t)block {
    [self startAuthorization:block title:nil];
}

- (void)startAuthorization:(dispatch_block_t)block title:(NSString*)title {
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad || IS_IPHONE_6_OR_MORE) {
        [self start];
        dispatch_async(queue, block);
        if (title) {
            processingTextLabel.text = title;
        }
    }
    else {
        [backgroundController hide];
        [backgroundController release];
        backgroundController = [[BackgroundOperationViewController alloc] init];
        if (!title) {
            [backgroundController setTitle:NSLocalizedString(@"AUTHORIZING_MSG", nil)];
        }
        else {
            [backgroundController setTitle:title];
        }
        [backgroundController startTask:^{
            block();
        } completed:^{
        }];
    }
}

- (void)setProcessingTextMessage:(NSString*)text{
    //load view;
    if (self.view)
        processingTextLabel.text = text;
}

-(void)setFontTitle:(UIFont *)aFontTitle{
    if (self.view){
        [fontTitle release];
        fontTitle = [aFontTitle retain];
    }
}

-(void)setFontDetails:(UIFont *)aFontDetails{
    if (self.view){
        [fontDetails release];
        fontDetails = [aFontDetails retain];
    }
}

@end
