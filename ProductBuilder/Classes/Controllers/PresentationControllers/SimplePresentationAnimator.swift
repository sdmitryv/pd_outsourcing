//
//  SimplePresentationAnimator.swift
//  TeamworkPOS
//
//  Created by valery on 12/21/16.
//  Copyright © 2016 Cloudworks. All rights reserved.
//

import UIKit

final class SimplePresentationAnimator: NSObject {

    // MARK: - Properties
    let direction: PresentationDirection
    let isPresentation: Bool
    var hidesNavigationBar :Bool = false
    weak var sourceController: UIViewController?

  // MARK: - Initializers
  init(direction: PresentationDirection, isPresentation: Bool) {
    self.direction = direction
    self.isPresentation = isPresentation
    super.init()
  }
}

// MARK: - UIViewControllerAnimatedTransitioning
extension SimplePresentationAnimator: UIViewControllerAnimatedTransitioning {
  
  func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
    return 0.3
  }
  
  func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
    let key = isPresentation ? UITransitionContextViewControllerKey.to : UITransitionContextViewControllerKey.from
    let controller = transitionContext.viewController(forKey: key)!
    let presentingController = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.from)!

    if isPresentation {
      transitionContext.containerView.addSubview(controller.view)
    }
    
    if isPresentation {
        if !hidesNavigationBar, let navigationController = presentingController as? UINavigationController ?? presentingController.navigationController ?? sourceController as? UINavigationController ?? sourceController?.navigationController{
            transitionContext.containerView.frame.size.height -= (navigationController.navigationBar.frame.size.height + navigationController.navigationBar.frame.origin.y)
            transitionContext.containerView.frame.origin.y += navigationController.navigationBar.frame.size.height + navigationController.navigationBar.frame.origin.y
            transitionContext.containerView.clipsToBounds = true
        }
    }

    let presentedFrame = transitionContext.finalFrame(for: controller)
    var dismissedFrame = presentedFrame
    switch direction {
    case .left:
      dismissedFrame.origin.x = -presentedFrame.width
    case .right:
      dismissedFrame.origin.x = transitionContext.containerView.frame.size.width
    case .top:
      dismissedFrame.origin.y = -presentedFrame.height
    case .bottom:
      dismissedFrame.origin.y = transitionContext.containerView.frame.size.height
    }

    let initialFrame = isPresentation ? dismissedFrame : presentedFrame
    let finalFrame = isPresentation ? presentedFrame : dismissedFrame

    let animationDuration = transitionDuration(using: transitionContext)
    controller.view.frame = initialFrame
    UIView.animate(withDuration: animationDuration, animations: {
      controller.view.frame = finalFrame
    }) { finished in
      transitionContext.completeTransition(finished)
    }
  }
}
