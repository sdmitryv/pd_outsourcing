//
//  SimplePresentationAnimator.swift
//  TeamworkPOS
//
//  Created by valery on 12/21/16.
//  Copyright © 2016 Cloudworks. All rights reserved.
//

import UIKit

final class SimplePresentationController: UIPresentationController {
  
  // MARK: - Properties
  fileprivate var dimmingView: UIView!
  private var direction: PresentationDirection
  private var sizeCoefficient: CGFloat
  
    //var hidesNavigationBar : Bool = false
    
  override var frameOfPresentedViewInContainerView: CGRect {
    var frame: CGRect = .zero
    frame.size = size(forChildContentContainer: presentedViewController, withParentContainerSize: containerView!.bounds.size)
    switch direction {
    case .right:
      frame.origin.x = containerView!.frame.width*(1.0 - sizeCoefficient)
    case .bottom:
      frame.origin.y = containerView!.frame.height*(1.0 - sizeCoefficient)
    default:
      frame.origin = .zero
    }
    return frame
  }
  
  // MARK: - Initializers
    init(presentedViewController: UIViewController, presenting presentingViewController: UIViewController?, direction: PresentationDirection, sizeCoefficient:CGFloat) {
        self.direction = direction
        self.sizeCoefficient = fabs(sizeCoefficient)
        if self.sizeCoefficient > 1.0{
           self.sizeCoefficient = 1.0
        }
        self.sizeCoefficient = sizeCoefficient
        super.init(presentedViewController: presentedViewController, presenting: presentingViewController)
        setupDimmingView()
    }
  
  override func presentationTransitionWillBegin() {
    containerView?.insertSubview(dimmingView, at: 0)
    NSLayoutConstraint.activate(NSLayoutConstraint.constraints(withVisualFormat: "V:|[dimmingView]|", options: [], metrics: nil, views: ["dimmingView": dimmingView]))
    NSLayoutConstraint.activate(NSLayoutConstraint.constraints(withVisualFormat: "H:|[dimmingView]|", options: [], metrics: nil, views: ["dimmingView": dimmingView]))
    
    guard let coordinator = presentedViewController.transitionCoordinator else {
      dimmingView.alpha = 1.0
      return
    }
    
    coordinator.animate(alongsideTransition: { _ in
      self.dimmingView.alpha = 1.0
    })
  }
  
  override func dismissalTransitionWillBegin() {
    guard let coordinator = presentedViewController.transitionCoordinator else {
      dimmingView.alpha = 0.0
      return
    }
    
    coordinator.animate(alongsideTransition: { _ in
      self.dimmingView.alpha = 0.0
    })
  }
  
  override func containerViewWillLayoutSubviews() {
    presentedView?.frame = frameOfPresentedViewInContainerView
  }
  
  override func size(forChildContentContainer container: UIContentContainer, withParentContainerSize parentSize: CGSize) -> CGSize {
    switch direction {
    case .left, .right:
      return CGSize(width: parentSize.width*sizeCoefficient, height: parentSize.height)
    case .bottom, .top:
      return CGSize(width: parentSize.width, height: parentSize.height*sizeCoefficient)
    }
  }
    
//    override var containerView: UIView?{
//        get{
//            let aContainerView = super.containerView
//            if !hidesNavigationBar && aContainerView != nil, aContainerView!.frame.origin.y == 0, let navigationController = presentingViewController as? UINavigationController ?? presentingViewController.navigationController{
//                aContainerView!.frame.size.height -= (navigationController.navigationBar.frame.size.height + navigationController.navigationBar.frame.origin.y)
//                aContainerView!.frame.origin.y += navigationController.navigationBar.frame.size.height + navigationController.navigationBar.frame.origin.y
//            }
//            return aContainerView
//        }
//    }
}

// MARK: - Private
private extension SimplePresentationController {
  
  func setupDimmingView() {
    dimmingView = UIView()
    dimmingView.translatesAutoresizingMaskIntoConstraints = false
    dimmingView.backgroundColor = UIColor(white: 0.0, alpha: 0.5)
    dimmingView.alpha = 0.0
    
    //let recognizer = UITapGestureRecognizer(target: self, action: #selector(handleTap(recognizer:)))
    //dimmingView.addGestureRecognizer(recognizer)
  }
  
  dynamic func handleTap(recognizer: UITapGestureRecognizer) {
    presentingViewController.dismiss(animated: true)
  }
}
