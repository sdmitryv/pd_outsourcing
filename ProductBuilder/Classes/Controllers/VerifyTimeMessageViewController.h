//
//  VerifyTimeMessageViewController.h
//  ProductBuilder
//
//  Created by Viacheslav Lulakov on 6/12/14.
//  Copyright (c) 2014 Cloudworks. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RTMessageController.h"

@interface VerifyTimeMessageViewController : RTMessageController {
    NSDate  * _date;
    NSTimeZone * _timeZone;
    NSDateFormatter * _dateFormatter;
}

@property (retain, nonatomic) IBOutlet UILabel * deviceTimeLabel;
@property (retain, nonatomic) IBOutlet UILabel * deviceUtcLabel;
@property (retain, nonatomic) IBOutlet UILabel * headquartersTimeLabel;
@property (retain, nonatomic) IBOutlet UILabel * headquartersUtcLabel;
@property (retain, nonatomic) IBOutlet UIView  * timeDifferenceView;
@property (retain, nonatomic) IBOutlet UILabel * dayMismatchLabel;
@property (retain, nonatomic) IBOutlet UILabel * timeMismatchLabel;
@property (retain, nonatomic) IBOutlet UILabel * zoneMismatchLabel;

- (id)initWithDate:(NSDate *)date timeZone:(NSTimeZone *)timeZone;

+(void)verifyTime:(void(^)())completedBlock;

@end
