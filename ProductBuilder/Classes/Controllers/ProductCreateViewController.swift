//
//  ProductCreateViewController.swift
//  ProductBuilder
//
//  Created by Viacheslav Lulakov on 9/1/17.
//  Copyright © 2017 Cloudworks. All rights reserved.
//

import UIKit

class ProductCreateViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, SimpleTableListViewControllerDelegate, UITextFieldDelegate {

    var product : Product?
    let productLabels : Array<Any>? = MainViewController50.sharedInstance()?.productLabels[0] as? Array<Any>
    let productValues : Array<Any>? = MainViewController50.sharedInstance()?.productValues[0] as? Array<Any>
    var selectedValues  = NSMutableArray()
    var currentInstructions : String?
    let showModifications = (MainViewController50.sharedInstance()?.modifications.count)! > 0
    
    
    var currentValues : Array<String>? {
        
        var values = Array<String>()
        
        let value = getValueOfLastLevel()
        let level = value == nil ? productValues! : (value?.value(forKey: "valuesList") as! Array<Any>)
        
        for value in level {
            if let v = (value as? NSDictionary)?.object(forKey: "value") {
                values.append(v as! String)
            }
        }
        return values
    }
    
    
    func getValueOfLastLevel() -> NSDictionary? {
        
        var currentValue : NSDictionary? = nil
        var idx = 0
        
        for item in selectedValues {
            
            if (selectedRow >= 0 && idx == selectedRow) {
                
                return currentValue
            }
            
            let stringItem = item as! String
            let level = currentValue != nil
                        ? (currentValue?.value(forKey: "valuesList") as! Array<Any>)
                        : (productValues!)
                
            for levelItem in level {
                
                let levelValue = levelItem as? NSDictionary
                if stringItem == (levelValue?.value(forKey: "value") as? String)! {
                    
                    currentValue = levelValue
                    break;
                }
            }
            
            idx = idx + 1
        }
        
        return currentValue
    }
    
    var currentModifications = Set<String>() {
        didSet {
            fieldTableView.reloadData()
        }
    }
    
    @IBOutlet var fieldsScrollView: RTScrollView!
    
    @IBOutlet var fieldTableView: UITableView!
    
    @IBAction func createProductClick(_ sender: Any) {

        let product = Product()
        
        product.clu = String(format: "%@%01d", DataUtils.readableDateString(from: Date(), format: "yyMMddHHmm"), Int(arc4random()%10))

        selectedRow = -1;
        let val = getValueOfLastLevel()
        
        product.price = AppSettingManager.instance().userInputBasePrice == 0
                            ? NSDecimalNumber(string: val?.object(forKey: "basePrice") as? String)
                            : priceTextField.decimalNumberValue
        product.cost = AppSettingManager.instance().userInputOrderCost == 0
                            ? NSDecimalNumber(string: val?.object(forKey: "orderCost") as? String)
                            : costTextField.decimalNumberValue
        if (product.price == nil) {
            product.price = NSDecimalNumber(decimal: zero)
        }
        if (product.cost == nil) {
            product.cost = NSDecimalNumber(decimal: zero)
        }
        
        product.taxCategoryCode = val?.object(forKey: "taxGroupCode") as! String
        product.manufacturerName = val?.object(forKey: "manufacturer") as! String
        product.department = val?.object(forKey: "departmentCode") as! String
        product.procuctClass = val?.object(forKey: "classCode") as! String
        product.primaryVendor = val?.object(forKey: "primaryVendor") as! String
        
        product.elements = selectedValues
        product.modifications = Array(currentModifications) as! NSMutableArray
        if currentInstructions != nil {
            product.instructions = currentInstructions! as String!
        }
        
        let descr = NSMutableString()
        
        if (AppSettingManager.instance().useSourceDescription4) {
            
            descr.append(val?.object(forKey: "description4") as! String)
        }
        else {
            
            var idx = 0
            for dic in productLabels! {
                
                let product = dic as! NSDictionary
                if (product["inDescription"] as! NSNumber).intValue == 1 {
                    
                    if idx > 0 {
                        descr.append(" ");
                    }
                    descr.append(selectedValues.object(at: idx) as! String);
                }
                idx = idx + 1
            }
        }
        
        product.storeDescription = (descr.length > 30 ? descr.substring(to: 30) : descr as String)
        
        var uploadOperation = UploadProductOperation()
        uploadOperation.product = product
        uploadOperation.deptSetting = MainViewController50.sharedInstance()?.settingValues["deptSetting"] as! String
        uploadOperation.vendorSetting = MainViewController50.sharedInstance()?.settingValues["vendorSetting"] as! String
        var canceling = false
        
        let loadingContr = BackgroundOperationController50()
        loadingContr.hideCancel = true
        loadingContr.title = L("SESSIONS_PROCESSING_TITLE")
        loadingContr.startAlertTask(task: {

            let queue = SyncOperationQueue()
            queue.addOperations([uploadOperation], waitUntilFinished: true, breakOnError: true)
            
            while uploadOperation.uploadStatus == .inProgress && !canceling{

                DispatchQueue.main.async() {
                    
                    loadingContr.hideCancel = false
                    loadingContr.onCancel =  {() -> Void in
                        
                        canceling = true;
                    }
                }
                
                sleep(1)

                let checkDocId = uploadOperation.inProcessApiDocumentId
                uploadOperation = UploadProductOperation()
                uploadOperation.checkApiDocumentId = checkDocId
                queue.addOperations([uploadOperation], waitUntilFinished: true, breakOnError: true)
            }
        },
        completed: {

            if uploadOperation.uploadStatus != .ok && canceling {
                
                return
            }
            
            if uploadOperation.uploadStatus == .none {
                
                DispatchQueue.main.async() {
                    
                    let alertView = RTAlertView50(title: NSLocalizedString("ERROR_TEXT", comment: ""),
                                                  message: uploadOperation.lastError, delegate: nil,
                                                  cancelButtonTitle: NSLocalizedString("OKBTN_TITLE", comment: ""),
                                                  otherButtonTitle:nil)
                    
                    alertView.completedBlock = {(_ buttonIndex: Int) -> Void in
                    }
                    alertView.show()
                }
                return
            }
            
            product.save()
            self.product = product
//            DispatchQueue.main.async() {
//                let storyboard = UIStoryboard(name: "Main50", bundle:nil)
//                if let controller: ProductReviewViewController = storyboard.instantiateViewController(withIdentifier: "ProductReviewViewControllerId") as? ProductReviewViewController {
//                    controller.product = product
//                    self.navigationController?.pushViewController(controller, animated: true)
//                }
//            }
            self.navigationController?.popToRootViewController(animated: true)
        })
    }
    
    var selectedRow = -1
    
    @IBAction func valueButtonClick(_ sender: UIButton) {
        
        selectedRow = ((fieldTableView.indexPathForRow(at: fieldTableView.convert(sender.center, from: sender.superview)))?.row)!
        
        enableControls()
        let itemsListViewController = SimpleTableListViewController();
        itemsListViewController.items = currentValues?.sorted(by: { $0.caseInsensitiveCompare($1) == ComparisonResult.orderedAscending })
        itemsListViewController.delegate = self;
        itemsListViewController.titleVisible = false;
        itemsListViewController.presentPopover(from: self.view.convert(sender.frame, from: sender.superview), in: self.view, permittedArrowDirections: UIPopoverArrowDirection.any, animated: true)
        itemsListViewController.preferredContentSize = CGSize(width: 300, height: 300)
    }
    
    @IBAction func editModificationsClick(_ sender: Any) {
    }
    
    @IBOutlet var createProductButton: UIButton!
    @IBOutlet var priceLabel: UILabel!
    @IBOutlet var priceInfoLabel: UILabel!
    @IBOutlet var priceTextField: UITextFieldNumeric!
    @IBOutlet var costTextField: UITextFieldNumeric!
    @IBOutlet var costLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        fieldsScrollView.contentSize = CGSize(width: 1024, height: 550)
        fieldTableView.tableFooterView = UIView(frame: CGRect.zero)
        enableControls()
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        super.prepare(for: segue, sender: sender)
        
        if segue.destination is UINavigationController {
            
            let navigationViewController = segue.destination as? UINavigationController
            if let editModificationsViewController = navigationViewController?.visibleViewController as? EditModificationsViewController {
                
                editModificationsViewController.selectedModifications = currentModifications
                editModificationsViewController.save = { self.currentModifications = editModificationsViewController.selectedModifications }
            }
        }
    }

    private func enableControls() {
        
        createProductButton.isEnabled = (productLabels?.count)! == selectedValues.count
        
        if (AppSettingManager.instance().userInputBasePrice == 2 && priceTextField != nil && CPDecimalEquals0(priceTextField.decimalValue)) {
        
            createProductButton.isEnabled = false
            return
        }
        if (AppSettingManager.instance().userInputOrderCost == 2 && costTextField != nil && CPDecimalEquals0(costTextField.decimalValue)) {
            
            createProductButton.isEnabled = false
            return
        }
    }
    
    // MARK: UITableView DataSource
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return section == 0
                            ? (productLabels?.count)! +
                                (4 - (showModifications ? 0 : 1) - ( AppSettingManager.instance().userInputOrderCost > 0 ? 0 : 1))
                            : 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell:UITableViewCell?
        if indexPath.section == 0    {
            let row = indexPath.row
            let count = (productLabels?.count)!
            if row < count {
                let label: NSDictionary = (productLabels?[indexPath.row] as? NSDictionary)!
                cell = tableView.dequeueReusableCell(withIdentifier: "ItemFieldCellIdentifier")
                let titleLabel: UILabel? = cell?.contentView.viewWithTag(1) as? UILabel
                titleLabel?.text = label.value(forKey: "label") as? String
                let valueButton : UIButton? = cell?.contentView.viewWithTag(2) as? UIButton
                if row < selectedValues.count {
                    
                    valueButton?.setTitle(selectedValues[indexPath.row] as? String, for: UIControlState.normal)
                }
                else if row == selectedValues.count {
                    
                    valueButton?.setTitle("Select", for: UIControlState.normal)
                }
                valueButton?.isHidden = row > selectedValues.count
                let messageLabel : UILabel? = cell?.contentView.viewWithTag(3) as? UILabel
                messageLabel?.isHidden = row <= selectedValues.count
            }
            else if row == count && showModifications {
                cell = tableView.dequeueReusableCell(withIdentifier: "ItemModificationsCellIdentifier")
                let valueButton: UIButton? = cell?.contentView.viewWithTag(2) as? UIButton
                valueButton?.setTitle(currentModifications.count > 0 ? Array(currentModifications).joined(separator: ", ") : "None", for: UIControlState.normal)
            }
            else if row == count + (showModifications ? 1 : 0) {
                
                if ( AppSettingManager.instance().userInputBasePrice == 0) {
                
                    cell = tableView.dequeueReusableCell(withIdentifier: "ItemPriceCellIdentifier")
                    priceInfoLabel = cell?.contentView.viewWithTag(1) as? UILabel
                    priceLabel = cell?.contentView.viewWithTag(2) as? UILabel
                }
                else {
                    
                    cell = tableView.dequeueReusableCell(withIdentifier: "ItemEditPriceCellIdentifier")
                    priceLabel = cell?.contentView.viewWithTag(1) as? UILabel
                    priceLabel.text = L("SETTINGS_GENERAL_INFO_INPUT_BASE_PRICE")
                    priceTextField = cell?.contentView.viewWithTag(2) as? UITextFieldNumeric
                    priceTextField.style = NSTextFormatterStyle.currencyStyle
                    priceTextField.clearButtonMode = .whileEditing;
                    priceTextField.decimalValue = zero
                    
                    priceLabel.signPosition = .right
                    priceLabel.sign = "*"
                    priceLabel.hideSign(AppSettingManager.instance().userInputBasePrice != 2)
                    priceTextField.delegate = self
                }
            }
            else if (row == count + (showModifications ? 1 : 0) + 1 && AppSettingManager.instance().userInputOrderCost > 0) {
                cell = tableView.dequeueReusableCell(withIdentifier: "ItemEditPriceCellIdentifier")
                costLabel = cell?.contentView.viewWithTag(1) as? UILabel
                costLabel.text = L("SETTINGS_GENERAL_INFO_INPUT_ORDER_COST")
                costTextField = cell?.contentView.viewWithTag(2) as? UITextFieldNumeric
                costTextField.style = NSTextFormatterStyle.currencyStyle
                costTextField.clearButtonMode = .whileEditing;
                costTextField.decimalValue = zero
                
                costLabel.signPosition = .right
                costLabel.sign = "*"
                costLabel.hideSign(AppSettingManager.instance().userInputOrderCost != 2)
                costTextField.delegate = self
            }
            else {
                cell = UITableViewCell()
            }
        }
        else if indexPath.section == 1 {
            cell = tableView.dequeueReusableCell(withIdentifier: "ItemInstructionsCellIdentifier")
        }
        else {
            cell = UITableViewCell()
        }
        return cell!
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return section == 0 ? "Product" : "Instructions"
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        var height : CGFloat = 44
        switch (indexPath.section, indexPath.row) {
        case (0, _):
            height = 44
        case (1, 0):
            height = 60
        default:
            height = 1
        }
        return height
    }

    // MARK: SimpleTableListViewControllerDelegate
    
    func simpleTableListViewController(_ controller: SimpleTableListViewController!, didSelectValue value: Any!) {
        if let stringValue : String = value as? String {
            
            if selectedRow < selectedValues.count {
                
                if (selectedValues.object(at: selectedRow) as? String) == stringValue {
                    
                    selectedRow = -1;
                    return
                }
                
                selectedValues.removeObjects(in: NSRange(location: selectedRow, length: selectedValues.count - selectedRow))
                
                if (AppSettingManager.instance().userInputBasePrice == 0) {
                
                    priceLabel.isHidden = true
                    priceInfoLabel.isHidden = !priceLabel.isHidden
                }
            }
            
            selectedRow = -1;
        
            selectedValues.add(stringValue)
            fieldTableView.reloadData()
            
            if ((productLabels?.count)! == selectedValues.count && AppSettingManager.instance().userInputBasePrice == 0) {
                
                let val = getValueOfLastLevel()
                priceLabel.isHidden = false
                priceInfoLabel.isHidden = !priceLabel.isHidden
                let price = NSDecimalNumber(string: val?.object(forKey: "basePrice") as? String).decimalValue
                priceLabel.text = NSCurrencyFormatter.formatCurrency(price)
            }
            
            enableControls()
        }
    }

    // MARK: UITextField Delegate
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if ((priceTextField != nil && textField == priceTextField) || (costTextField != nil && textField == costTextField)) {
            
            textField.selectAllNoMenu()
        }
    }
    
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        if (priceTextField != nil && textField == priceTextField) {
            
        }
        else if (costTextField != nil && textField == costTextField) {
            
        }
        else {
            currentInstructions = textField.text
        }
        enableControls()
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
