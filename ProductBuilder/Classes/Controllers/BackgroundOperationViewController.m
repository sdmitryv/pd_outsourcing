//
//  BackgroundOperationViewController.m
//  CloudworksPOS
//
//  Created by valera on 7/27/11.
//  Copyright 2011 Retail Teamwork Ukraine. All rights reserved.
//

#import "BackgroundOperationViewController.h"

#import "UIView+FirstResponder.h"
#import "Global.h"
#import "AutoLogOutManager.h"
#import "YLProgressBar.h"
#import "KeyboardHelper.h"
#import "UIViewControllerExtended.h"

static NSMutableArray *backgroundOperations;

@interface BackgroundOperationViewController(){
    CGFloat _deltaY;
}
    -(void)showWithTask:(dispatch_block_t)block;
@end

@implementation BackgroundOperationViewController

@synthesize bCancel;
@synthesize progressView;
@synthesize progressLabel, queue, hideCancel, isCancelling, onCancel, delayedInvokeTimer;

+ (void) initialize {
    
    backgroundOperations = [[NSMutableArray alloc] init];
}


- (void)initDefaults {
    
    queue = dispatch_queue_create("com.cloudworks.backgroundTask",NULL);
    dispatch_queue_t high = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0);
    dispatch_set_target_queue(queue,high);
    hideCancel = TRUE;
    isCancelling = FALSE;
}


- (id)init{
    
    self = [self initWithStyle:BackgroundOperationStyleSmallGrey];
    return self;
}


- (id)initWithStyle:(BackgroundOperationStyle)style {
    
    if (style == BackgroundOperationStyleDefault) {
        loopProgress = YES;
    }
    
    NSString *nibName = [self nibNameForStyle:style];
    if (nibName)
        self = [super initWithNibName:nibName bundle:[NSBundle mainBundle]];
    else
        self = [super init];
    
    if (self) {
        _style = style;
        [self initDefaults];
    }
    
    return self;
}

-(NSString *)nibNameForStyle:(BackgroundOperationStyle)style {
    
    switch (style) {
            
        case BackgroundOperationStyleSmallGrey:
            return @"BackgroundOperationSmallGrayView";
            
        default:
            if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad)
                return @"BackgroundOperationView";
            else
                return @"BackgroundOperationView_iPhone";
    }
    return nil;
}






-(BOOL)loopProgress{
    
    return loopProgress;
}


-(void)setHideCancel:(BOOL)value{
    
    if (value == hideCancel) return;
    
    if (![NSThread isMainThread]){
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self setHideCancel:value];
        });
        return;
    }
    
    hideCancel = value;
    if (self.view) { // force load view
        
        if (self.isViewLoaded){
            
            self.bCancel.hidden = hideCancel;
            
            CGRect rect = self.view.bounds;
            CGRect brect = bCancel.frame;
            if (hideCancel)
                rect.size.height = originalHeight;
            else
                rect.size.height = brect.origin.y + brect.size.height + (_style == BackgroundOperationStyleSmallGrey ? 23 : 8);

            self.view.bounds = rect;
        }
    }
    
    
}


-(void)setLoopProgress:(BOOL)value{
    
    if (value!=loopProgress){
        
        if (![NSThread isMainThread]){
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [self setLoopProgress:value];
            });
            return;
        }
        
        loopProgress = value;
        if (loopProgress){
            
            progressView.progress = 0.0f;
            [self progressStartAnimation];
        }
        else{
            
            [self progressStopAnimation];
            progressView.progress = 0.0f;
        }
    }
}


-(float)progress{
    
    return progressView.progress;
}


-(void)setProgress:(float)value{
    
    if (![NSThread isMainThread]){
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self setProgress:value];
        });
        return;
    }
    
    if (!loopProgress)
        progressView.progress = value;
}

- (void)keyboardWillShow:(NSNotification *)n{
    if (_state == BackgroundOperationViewStateHide) return;
    NSDictionary* userInfo = [n userInfo];
    CGRect keyboardEndFrame;
    [userInfo[UIKeyboardFrameEndUserInfoKey] getValue:&keyboardEndFrame];
    UIView* rootView = self.view.window.rootViewController.view;
    if (rootView){
        keyboardEndFrame = CGRectApplyAffineTransform(keyboardEndFrame,rootView.transform);
    }
    [self applyKeyboardTransform:keyboardEndFrame animated:TRUE];
    
}

- (void)keyboardWillHide:(NSNotification *)n{
    if (_state == BackgroundOperationViewStateHide) return;
    [self removeKeyboardTransform:TRUE];
}

- (void)keyboardDidHide:(NSNotification *)n{
    if (_state == BackgroundOperationViewStateHide) return;
    [self removeKeyboardTransform:TRUE];
}

-(void)applyKeyboardTransform:(CGRect)keyboardEndFrame animated:(BOOL)animated{
    if (_state == BackgroundOperationViewStateHide) return;
    if (animated){
        [UIView animateWithDuration:0.3f delay:.0f options:UIViewAnimationOptionAllowUserInteraction | UIViewAnimationOptionBeginFromCurrentState animations:^{
            [self applyKeyboardTransform:keyboardEndFrame animated:FALSE];
        } completion:nil];
        return;
    }
    [self removeKeyboardTransform:FALSE];
    //the UIWindow's coordinate system is always in portrait orientation
    // Lulakov - before iOS 8
    CGFloat windowHeight = self.view.window.frame.size.height;
    if (!SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8")) {
        UIInterfaceOrientation orientation = [[UIApplication sharedApplication] statusBarOrientation];
        if ( orientation==UIInterfaceOrientationLandscapeLeft || orientation==UIInterfaceOrientationLandscapeRight){
            windowHeight = self.view.window.frame.size.width;
        }
    }
    
    CGFloat deltaY = (windowHeight - (windowHeight + self.view.bounds.size.height)/2.0f) - keyboardEndFrame.size.height;
    deltaY = roundf(deltaY);
    if (deltaY < 0){
        _deltaY = deltaY;
        self.view.transform = CGAffineTransformTranslate(self.view.transform, 0, _deltaY);
    }
}

-(void)removeKeyboardTransform:(BOOL)animated{
    if (_state == BackgroundOperationViewStateHide) return;
    if (animated){
        [UIView animateWithDuration:0.3f delay:.0f options:UIViewAnimationOptionAllowUserInteraction | UIViewAnimationOptionBeginFromCurrentState animations:^{
            [self removeKeyboardTransform:FALSE];
        } completion:nil];
        return;
    }
    if (_deltaY){
        self.view.transform = CGAffineTransformTranslate(self.view.transform, 0, -_deltaY);
        _deltaY = 0.0f;
    }
}


- (void)flipViewAccordingToStatusBarOrientation:(NSNotification *)notification {
#ifdef __IPHONE_8_0
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8")) return;
#endif
    if (self.parentView && ![self.parentView isKindOfClass:[UIWindow class]]) return;
    UIInterfaceOrientation orientation = [[UIApplication sharedApplication] statusBarOrientation];
    CGFloat angle = 0.0;
    switch (orientation) { 
        case UIInterfaceOrientationPortraitUpsideDown:
            angle = M_PI; 
            break;
        case UIInterfaceOrientationLandscapeLeft:
            angle = - M_PI_2;
            break;
        case UIInterfaceOrientationLandscapeRight:
            angle = M_PI_2;
            break;
        default: // as UIInterfaceOrientationPortrait
            angle = 0.0;
            break;
    } 
    self.view.transform = CGAffineTransformMakeRotation(angle);
}

- (void)dealloc{
    [_parentView release];
    [bCancel removeTarget:nil action:NULL forControlEvents:UIControlEventAllEvents];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [delayedInvokeTimer invalidate];
    [delayedInvokeTimer release];
    delayedInvokeTimer = nil;
    [durationTimer invalidate];
    [durationTimer release];
    durationTimer = nil;
    
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    //wait until queue finished
    dispatch_sync(queue, ^{});
    dispatch_release(queue);
    [onCancel release];
    [progressView release];
    [progressLabel release];
    [backgroundView release];
    [bCancel release];
    [firstResponderView release];
    [contentView release];
    [super dealloc];
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView
{
}
*/


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{
    [super viewDidLoad];
    originalHeight = self.view.bounds.size.height;
    self.hideCancel = hideCancel;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(flipViewAccordingToStatusBarOrientation:) name:UIApplicationDidChangeStatusBarOrientationNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidHide:) name:UIKeyboardDidHideNotification object:nil];
    
    [bCancel setTitle:NSLocalizedString(@"CANCELBTN_TITLE", nil) forState:UIControlStateNormal];
    CGFloat radius;
    if (_style == BackgroundOperationStyleSmallGrey) {
//        [self.progressView setProgressTintColor:MO_RGBCOLOR(153, 188, 237)];
        radius = 5.0f;
        self.progressView.progress = 1.0f;
        YLProgressBar *progress = (YLProgressBar *)self.progressView;
        CGRect progressFrame = progress.frame;
        progressFrame.size.height = 9;
        progress.frame = progressFrame;
        [progress setProgressTintColors:@[MO_RGBCOLOR(154, 187, 236), MO_RGBCOLOR(154, 187, 236)]];
        progress.stripesColor = MO_RGBCOLOR(110, 134, 170);
    }
    else{
        radius = 10.0f;
        contentView.layer.borderWidth = 2.0f;
        contentView.layer.borderColor = [UIColor lightGrayColor].CGColor;
    }
    [contentView setRoundedCorners:UIViewRoundedCornerAll radius:radius];
    
    //shadow
    self.view.layer.masksToBounds = NO;
    self.view.layer.shadowColor = [[UIColor blackColor] CGColor];
    self.view.layer.shadowOffset = CGSizeMake(10, 10);
    self.view.layer.shadowOpacity = 0.2f;
    UIRectCorner rectCorners = UIRectCornerAllCorners;
    UIBezierPath* beizerPath = [UIBezierPath bezierPathWithRoundedRect:self.view.bounds
                                                     byRoundingCorners:rectCorners
                                                           cornerRadii:CGSizeMake(radius, radius)];
    self.view.layer.shadowPath = beizerPath.CGPath;
}


/*
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
	return YES;
}
*/

#pragma mark -
#pragma mark Progress

-(BOOL)isShown{
    return backgroundView.superview != nil;
}

- (void)show{
    if (_state == BackgroundOperationViewStateShow) return;
//    if (_hiding){
//        [self.view.layer removeAllAnimations];
//    }
    
    if (![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self showWithTask:nil];
        });
        return;
    }
    
    [self showWithTask:nil];
}


-(void)startTask:(dispatch_block_t)task delay:(CGFloat)delay completed:(dispatch_block_t)completed{

    if (delay <= 0){
        
        [self startTask:task completed:completed];
        return;
    }
    
    if (task){
        
        completed = completed ? completed : ^{};
        dispatch_block_t compl = completed;
        
        if ([AutoLogOutManager instance].isStarted)
            compl = ^{
                
                [[AutoLogOutManager instance] Start];
                completed();
            };
        
        [[AutoLogOutManager instance] Stop];
        
        
        [delayedInvokeTimer invalidate];
        [delayedInvokeTimer release];
        delayedInvokeTimer = [[NSTimer scheduledTimerWithTimeInterval:delay target:self selector:@selector(show) userInfo:nil repeats:NO]retain];
        
        dispatch_async(queue, ^{
            
            task();
            [delayedInvokeTimer invalidate];
            [delayedInvokeTimer release];
            delayedInvokeTimer = nil;
            [self hide];

            dispatch_async(dispatch_get_main_queue(), compl);
        });
    }
}

-(void)showWithTask:(dispatch_block_t)task{
    
    idleTimerDisabledBeforeStart = [UIApplication sharedApplication].idleTimerDisabled;
    [[UIApplication sharedApplication] setIdleTimerDisabled:YES];
    
    isCancelling = FALSE;
    _state = BackgroundOperationViewStateShow;
    UIView* window = self.parentView?:[UIApplication sharedApplication].windows[0];
    [firstResponderView release];
    firstResponderView = [[window getFirstResponder] retain];
    [firstResponderView resignFirstResponder];
    
    self.view.frame = CGRectRound(self.view.frame);
    if (!backgroundView){
        backgroundView = [[UIView alloc] initWithFrame:window.bounds];
    }
    else{
        backgroundView.frame = window.bounds;
    }
    backgroundView.autoresizingMask  = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    backgroundView.backgroundColor = [UIColor blackColor];
    backgroundView.alpha = 0;
    [window addSubview:backgroundView];
    
    CGRect frame = self.view.frame;
    if (frame.size.width > window.frame.size.width) {
        frame.size.width = window.frame.size.width;
        self.view.frame = frame;
    }
    [window addSubview:self.view];
    [window bringSubviewToFront:backgroundView];
    [window bringSubviewToFront:self.view];
    [self flipViewAccordingToStatusBarOrientation:nil];
    self.view.center = window.center;
    self.view.frame = CGRectRound(self.view.frame);
    
    UIView* keyboardWindow = [KeyboardHelper keyboardWindow];
    keyboardWindow.userInteractionEnabled = NO;
    if (keyboardWindow && !keyboardWindow.layer.animationKeys.count){
        [self applyKeyboardTransform:keyboardWindow.frame animated:FALSE];
    }
    self.view.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1.26, 1.26);
    self.view.alpha = 0.0f;
    
    [backgroundOperations addObject:self];
    
    [UIView animateWithDuration:0.3f delay:.0f options:UIViewAnimationOptionAllowUserInteraction | UIViewAnimationOptionBeginFromCurrentState animations:^{
        backgroundView.alpha = 0.5;
        self.view.alpha = 1.0f;
        self.view.transform = CGAffineTransformIdentity;//CGAffineTransformScale(self.view.transform, 1/1.26, 1/1.26);
    }
        completion:^(BOOL finished){
            if (loopProgress){
                 progressView.progress = 0;
                [self progressStartAnimation];
            }
             if (task){
                 dispatch_async(queue, ^{
                     task();
                 });
             }
        }];
}


-(void)hideInternal {
    
    [self hideInternal:YES];
}


-(void)hideInternal:(BOOL)animated{
    
    if (_state == BackgroundOperationViewStateHide || !self.view.superview) return;
//    if (_showing){
//        [self.view.layer removeAllAnimations];
//    }
    _state = BackgroundOperationViewStateHide;
    if (!idleTimerDisabledBeforeStart)
        [[UIApplication sharedApplication] setIdleTimerDisabled:NO];
    
    if (loopProgress){
        [self progressStopAnimation];
        progressView.progress = 1.0f;
    }
    
    if (animated){
        [UIView animateWithDuration:0.3f delay:.0f options:UIViewAnimationOptionBeginFromCurrentState | UIViewAnimationOptionAllowUserInteraction animations:^{
            backgroundView.alpha = 0;
            self.view.alpha = 0.0f;
            self.view.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.84, 0.84);
        }
                         completion:^(BOOL finished){
                             if (_state != BackgroundOperationViewStateHide)
                                 return;
                             [backgroundView removeFromSuperview];
                             [self.view removeFromSuperview];
                             self.view.transform = CGAffineTransformIdentity;
                         }];
    }
    else {
        if (_state == BackgroundOperationViewStateHide){
            [backgroundView removeFromSuperview];
            [self.view removeFromSuperview];
        }
    }
    
    UIView* keyboardWindow = [KeyboardHelper keyboardWindow];
    keyboardWindow.userInteractionEnabled = YES;

    [backgroundOperations removeObject:self];
}

-(void)recoverFirstResponder {
    [firstResponderView becomeFirstResponder];
    [firstResponderView release];
    firstResponderView = nil;
}

- (void)hide{
    if (_state == BackgroundOperationViewStateHide) return;
    if (![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self hide];
        });
        return;
    }
    [self hideInternal];
    [self recoverFirstResponder];
}


- (void)startTask:(dispatch_block_t)task completed:(dispatch_block_t)completed {
    
    completed = completed ? completed : ^{};
    dispatch_block_t compl = completed;
    
    if ([AutoLogOutManager instance].isStarted)
        compl = ^{
            
            [[AutoLogOutManager instance] Start];
            completed();
        };

    [[AutoLogOutManager instance] Stop];
    
    [self showWithTask: ^{
        
        if (task)
            task();
      
        [self hide];
        self.onCancel = nil;
        dispatch_async(dispatch_get_main_queue(), compl);
    }];
}


- (void)setTitle:(NSString*)text{
    //load view before
    if (![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self setTitle:text];
        });
        return;
    }
    if (self.view)
        progressLabel.text = text;
}

- (IBAction)bCancelClick:(id)sender {
    isCancelling = TRUE;
    [durationTimer invalidate];
    [durationTimer release];
    durationTimer = nil;
    if (onCancel){
        onCancel();
    }
}

- (void)showView:(UIView *)aView {
    [self showView:aView duration:0.0];
}

- (void)showView:(UIView*)aView duration:(NSTimeInterval)interval {
    CGRect frame = self.view.bounds;
    frame.size.height = 216;
    self.view.frame = frame;
    if (!hideCancel) {
        frame.size.height = bCancel.frame.origin.y;
    }
    aView.frame = CGRectMake(8, 8, frame.size.width - 16, frame.size.height - 16);
    [self.view addSubview:aView];
    [self show];
    if (interval != 0.0) {
        [durationTimer invalidate];
        [durationTimer release];
        durationTimer = [[NSTimer scheduledTimerWithTimeInterval:interval target:self selector:@selector(bCancelClick:) userInfo:bCancel repeats:NO] retain];
    }
}

- (void)progressStartAnimation {
    if ([progressView isKindOfClass:[UILoopProgressView class]]) {
        [((UILoopProgressView *)progressView) startAnimation];
    }
}

- (void)progressStopAnimation {
    if ([progressView isKindOfClass:[UILoopProgressView class]]) {
        [((UILoopProgressView *)progressView) stopAnimation];
    }
}


+(NSInteger)backgroundOperationsCount{
    
    return backgroundOperations.count;
}



+(void)closeBackgroundOperations{
    
    while (backgroundOperations.count > 0) {
        
        BackgroundOperationViewController *bo = backgroundOperations[0];
        bo.onCancel = nil;
        [bo hideInternal:NO];
        [backgroundOperations removeObject:bo];
    }
}

@end
