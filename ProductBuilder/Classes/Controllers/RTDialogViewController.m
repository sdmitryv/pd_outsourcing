//
//  RTDialogView.m
//  CloudworksPOS
//
//  Created by valera on 8/9/11.
//  Copyright 2011 Retail Teamwork Ukraine. All rights reserved.
//

#import "RTDialogViewController.h"
#import "UIView+FirstResponder.h"
#import "SystemVer.h"
#import "RTAlertView.h"

@implementation RTDialogContentViewController
@synthesize dialogViewController;

-(NSString*)title{
    return nil;
}

-(id)selectedValue{
    return nil;
}

-(void)setSelectedValue:(id)value{
    
}

-(BOOL)willSave{
    if (self.validate){
        if (!self.validate()){
            self.selectedValue = nil;
            return FALSE;
        }
    }
    return TRUE;
}

-(BOOL)willCancel{
    return TRUE;
}

-(void)willClose{

}

-(void)didClose{
    self.validate = nil;
}

-(NSString*)selectButtonTitle{
    return NSLocalizedString(@"SAVEBTN_TITLE", nil);
}

-(NSString*)cancelButtonTitle{
    return NSLocalizedString(@"CANCELBTN_TITLE", nil);
}

-(void)showModal:(void (^)(void))aCompletion{
    [RTDialogViewController showModal:self completion:aCompletion];
}

-(void)close{
    [self.dialogViewController cancel];
}

-(void)dealloc{
    self.validate = nil;
    [super dealloc];
}

@end

@interface RTDialogViewController ()

@property (nonatomic, retain)RTDialogContentViewController* contentController;
-(void)close;

@end

@implementation RTDialogViewController
@synthesize vTitle;
@synthesize vContentView;
@synthesize backView;
@synthesize bCancel;
@synthesize bSave;
@synthesize contentController, dialogResult;
@synthesize cancelling;

-(id)initWithContentViewController:(RTDialogContentViewController *)aContentController{
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        if (IS_IPHONE_6P || IS_IPHONE_6)
            self = [super initWithNibName:@"RTDialogView_iPhone@3x" bundle:[NSBundle mainBundle]];
        else
            self = [super initWithNibName:@"RTDialogView_iPhone" bundle:[NSBundle mainBundle]];
    } else {
        self = [super initWithNibName:@"RTDialogView" bundle:[NSBundle mainBundle]];
    }

    if (self != nil) {
        self.contentController = aContentController;
        self.contentController.dialogViewController = self;
        self.dialogResult = RTDialogResultCancel;
    }
    return self;
}

- (void)dealloc{
    self.contentController.dialogViewController = nil;
    [self setVTitle:nil];
    [self setBackView:nil];
    [self setBSave:nil];
    [self setBCancel:nil];
    [self setVContentView:nil];
    self.contentController = nil;
    [super dealloc];
}

+(void)showModal:(RTDialogContentViewController*)contentController completion:(void (^)(void))completion{
    RTDialogViewController* dialogView = [[RTDialogViewController alloc]initWithContentViewController:
                                          contentController];
    [dialogView showModal:TRUE completion:completion];
    [dialogView release];
}

+(void)showModal:(RTDialogContentViewController*)contentController delegate:(id<UIModalViewControllerDelegate>) aDelegate{
    RTDialogViewController* dialogView = [[RTDialogViewController alloc]initWithContentViewController:
                                          contentController];
    [dialogView showModal:TRUE delegate:aDelegate];
    [dialogView release];
}

-(void)setContentController:(RTDialogContentViewController *)aContentController{
    [contentController release];
    contentController = [aContentController retain];
}

-(void)setDialogResult:(RTDialogResult)value{
    dialogResult = value;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView
{
}
*/

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    if (SYSTEM_VERSION_LESS_THAN(@"5.0"))
        [contentController viewWillAppear:animated];
}
- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    if (SYSTEM_VERSION_LESS_THAN(@"5.0"))
        [contentController viewDidAppear:animated];
}
- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    if (SYSTEM_VERSION_LESS_THAN(@"5.0"))
        [contentController viewWillDisappear:animated];    
}
- (void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    if (SYSTEM_VERSION_LESS_THAN(@"5.0"))
        [contentController viewDidDisappear:animated];
}


- (void)viewDidLoad{
    [super viewDidLoad];
    if (!contentController) return;
    vTitle.text = contentController.title;
    [bSave setTitle:contentController.selectButtonTitle forState:UIControlStateNormal];
    [bCancel setTitle:contentController.cancelButtonTitle forState:UIControlStateNormal];
    [contentController view];
    CGSize originalFrame =  vContentView.bounds.size;
    CGSize newFrame = contentController.view.frame.size;
    CGFloat widthD = originalFrame.width - newFrame.width;
    CGFloat heightD  = originalFrame.height - newFrame.height;
    CGRect originalBounds = self.view.bounds;
    originalBounds.size.width-=widthD;
    originalBounds.size.height-=heightD;
    self.view.bounds = originalBounds;
    contentController.view.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    contentController.view.frame = contentController.view.bounds;
    [vContentView addSubview:contentController.view];
    [self addChildViewController:contentController];
}

- (BOOL)saving {
    return _saving;
}

- (BOOL)shouldAutorotate
{
    return TRUE;
}


- (IBAction)save:(id)sender {
    [self save];
}

-(void)save{
    _saving = TRUE;
    UIView* responder = [self.view getFirstResponder];
    if (responder){
        if (responder.canResignFirstResponder){
            //[self setValue:@(TRUE) forKey:@"dismissing"];
            [responder resignFirstResponder];
        }
        else{
            _saving = FALSE;
            return;
        }
    }
    id  alertView = [RTAlertView topMostAlertView];
    if (alertView && alertView!=self.ignoredAlertViewOnSave) {
        _saving = FALSE;
        return;
    }
    if (contentController && ![contentController willSave]) {
        _saving = FALSE;
        return;
    }
    self.dialogResult = RTDialogResultOK;
    [self close];
    _saving = FALSE;
}

- (IBAction)cancel:(id)sender {
    [self cancel];
}

-(void)cancel{
    //[self setValue:@(TRUE) forKey:@"dismissing"];
    cancelling = TRUE;
    UIView* responder = [self.view getFirstResponder];
    if (responder){
        [responder resignFirstResponder];
    }
    if (contentController && ![contentController willCancel]) return;
    self.dialogResult = RTDialogResultCancel;
    [self close];
    cancelling = FALSE;
}

-(void)close{
    [contentController willClose];
    [self dismissViewControllerAnimated:TRUE completion:^{ [contentController didClose];}];
}

-(void)setSaveHidden:(BOOL)value {
    [self view];
    if (bSave.hidden == value) return;
    bSave.hidden = value;
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        
        vTitle.frame = CGRectMake(vTitle.frame.origin.x,
                                  vTitle.frame.origin.y,
                                  value
                                  ? bCancel.frame.origin.x - vTitle.frame.origin.x - 10
                                  : bSave.frame.origin.x - vTitle.frame.origin.x - 10,
                                  vTitle.frame.size.height);

    }
}

@end
