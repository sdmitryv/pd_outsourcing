//
//  SupportSessionKeyViewController.h
//  ProductBuilder
//
//  Created by Vitaliy Gervazuk on 2/16/16.
//  Copyright © 2016 Cloudworks. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RTDialogViewController.h"
#import "UITextFieldNumeric.h"

@interface SupportSessionKeyViewController : RTDialogContentViewController<UITextFieldDelegate> {
    NSString* sessionKey;
    UITextFieldNumeric* sessionKeyTextField;
}

@property (nonatomic, readonly) NSString* sessionKey;

@end
