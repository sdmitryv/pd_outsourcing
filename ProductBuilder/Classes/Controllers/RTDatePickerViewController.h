//
//  RTDatePickerViewController.h
//  CloudworksPOS
//
//  Created by Vitaliy Gervazuk on 8/2/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MOGlassButton.h"
#import "RTDialogViewController.h"
#import "RTPopoverController.h"

@interface RTDatePickerViewController : UIViewController {
    
    MOGlassButton *clearButton;
    MOGlassButton *okButton;
    UIDatePicker *datePicker;
    NSDate* selectedDate;
    UIDatePickerMode picMode;
    BOOL modeChanged;
}
@property (nonatomic, retain) IBOutlet MOGlassButton *clearButton;
@property (nonatomic, retain) IBOutlet MOGlassButton *okButton;
@property (nonatomic, retain) IBOutlet UIDatePicker *datePicker;

@property (nonatomic, assign) RTPopoverController* popover;
@property (nonatomic, assign) RTDialogContentViewController* contentViewController;
@property (nonatomic, retain) NSDate* selectedDate;
@property (nonatomic, assign) BOOL useDateOnly; // indicates if we want to work only with date (without time); by default it is TRUE

-(id)initWithMode:(UIDatePickerMode)mode;
-(void)close;

@end


@protocol IRTDatePickerViewController <NSObject>
@property (nonatomic, retain) NSDate* date;
@property (nonatomic, assign) BOOL readonly;
@property (nonatomic, readonly) RTDatePickerViewController* pickerControl;
@end