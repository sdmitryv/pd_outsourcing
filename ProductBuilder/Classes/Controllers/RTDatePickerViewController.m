//
//  RTDatePickerViewController.m
//  CloudworksPOS
//
//  Created by Vitaliy Gervazuk on 8/2/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "RTDatePickerViewController.h"
#import "DataUtils.h"
#import "NSDate+Compare.h"
#import "UIRoundedCornersView.h"

@implementation RTDatePickerViewController
@synthesize clearButton;
@synthesize okButton;
@synthesize datePicker;
@synthesize popover;
@synthesize selectedDate;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        picMode = UIDatePickerModeDate;
        modeChanged=YES;
        self.useDateOnly = YES;
    }
    return self;
}

-(id)initWithMode:(UIDatePickerMode)mode{
    self = [self initWithNibName:nil bundle:[NSBundle mainBundle]];
    if (self) {
       picMode=mode;
    }
    return self;
}

- (void)dealloc {
    [clearButton release];
    [okButton release];
    [datePicker release];
    [selectedDate release];
    [super dealloc];
}

-(void)setSelectedDate:(NSDate *)aSelectedDate {
    if (![selectedDate isEqual:aSelectedDate]) {
        [selectedDate release];
        selectedDate = [aSelectedDate retain];
    }
    [datePicker setDate:selectedDate ? selectedDate : (self.useDateOnly ? [[NSDate date] dateOnly] : [NSDate date]) ];

}

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle4

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [okButton addTarget:self action:@selector(okButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    [clearButton addTarget:self action:@selector(clearButtonClick:) forControlEvents:UIControlEventTouchUpInside];
#ifdef __IPHONE_7_0
    datePicker.backgroundColor = [UIColor whiteColor];
    [UIRoundedCornersView drawRoundedCorners:datePicker corners:UIViewRoundedCornerLowerLeft | UIViewRoundedCornerLowerRight |UIViewRoundedCornerUpperLeft | UIViewRoundedCornerUpperRight radius:8.0f];
#endif
    if (self.useDateOnly)
        datePicker.timeZone = [NSTimeZone timeZoneForSecondsFromGMT:0];
    datePicker.minimumDate = [DataUtils dateWithYear:1753 month:1 day:1];
    datePicker.maximumDate = [DataUtils dateWithYear:2100 month:12 day:31];
    if (modeChanged)
        datePicker.datePickerMode=picMode;
    if (selectedDate) {
        [datePicker setDate:selectedDate];
    }
    else {
        [datePicker setDate: (self.useDateOnly ? [[NSDate date]dateOnly] : [NSDate date]) ];
    }
    datePicker.locale=[NSLocale currentLocale];
}

- (void) okButtonClick:(id)sender {
    self.selectedDate = (self.useDateOnly ? [datePicker.date dateOnly] : datePicker.date) ;
    [self close];
}

- (void) clearButtonClick:(id)sender {
    self.selectedDate = nil;
    [self close];
}

-(void)close{
    if (popover){
        [popover dismissPopoverAnimated:YES];
        [popover.delegate popoverControllerDidDismissPopover:popover];
    }
    else if (self.contentViewController){
        [self.contentViewController.dialogViewController save];
    }
}

-(void)setUseDateOnly:(BOOL)value{
    _useDateOnly = value;
    if (_useDateOnly){
        datePicker.timeZone = [NSTimeZone timeZoneForSecondsFromGMT:0];
    }
}

#ifdef __IPHONE_7_0
-(void)setContentSizeForViewInPopover:(CGSize)contentSizeForViewInPopover{
    self.preferredContentSize = contentSizeForViewInPopover;
}

-(CGSize)contentSizeForViewInPopover{
    return self.preferredContentSize;
}
#endif

@end
