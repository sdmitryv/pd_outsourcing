//
//  VerifyTimeMessageViewController.m
//  ProductBuilder
//
//  Created by Viacheslav Lulakov on 6/12/14.
//  Copyright (c) 2014 Cloudworks. All rights reserved.
//

#import "VerifyTimeMessageViewController.h"
#import "BackgroundOperationViewController.h"
#import "ModalAlert.h"
#import "DataUtils.h"
#import "GetDateTimeOperation.h"
#import "NSDate+System.h"
#import "Location.h"
#import "TimeZone.h"
#import "AppSettingManager.h"
#import "SystemVer.h"
#import "DecimalHelper.h"

@interface VerifyTimeMessageViewController ()

-(void)showDifference;

@end

@implementation VerifyTimeMessageViewController

- (id)initWithDate:(NSDate *)date timeZone:(NSTimeZone *)timeZone
{
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        if (IS_IPHONE_6P || IS_IPHONE_6) {
            self = [super initWithNibName:@"VerifyTimeMessageView_iPhone@3x" bundle:nil];
        }
        else
            self = [super initWithNibName:@"VerifyTimeMessageView_iPhone" bundle:nil];
    }
    else {
        self = [super initWithNibName:@"VerifyTimeMessageView" bundle:nil];
    }
    if (self) {
        _date = [date retain];
        _timeZone = [timeZone retain];
        _dateFormatter = [[NSDateFormatter alloc] init];
        [_dateFormatter setDateStyle:NSDateFormatterShortStyle];
        [_dateFormatter setTimeStyle:NSDateFormatterShortStyle];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    [self showDifference];
    
}

- (void)dealloc
{
    [_date release];
    [_timeZone release];
    [_dateFormatter release];
    
    [_deviceTimeLabel release];
    [_deviceUtcLabel release];
    [_headquartersTimeLabel release];
    [_headquartersUtcLabel release];
    [_timeDifferenceView release];
    [_dayMismatchLabel release];
    [_timeMismatchLabel release];
    [_zoneMismatchLabel release];

    [super dealloc];
}

#pragma mark - Private Methods

-(void)showDifference
{
    Location * localLocation = [Location getInstanceById:[AppSettingManager instance].locationId];
    TimeZone * localTimeZone = nil;
    NSInteger localUtcOffset = 0;
    NSTimeZone * locationTimeZone = nil;
    if (localLocation.timeZoneID != nil) {
        localTimeZone = [TimeZone getInstanceById:localLocation.timeZoneID];
        localUtcOffset = [[NSDecimalNumber decimalNumberWithDecimal:CPDecimalMultiply(localTimeZone.utcOffset, CPDecimalFromInteger(60))] integerValue];
        locationTimeZone = [NSTimeZone timeZoneForSecondsFromGMT:localUtcOffset * 60];
    }

    NSTimeZone * deviceTimeZone = [NSTimeZone localTimeZone];
    NSDate * deviceDate = [NSDate date];
    NSInteger deviceSeconds = [deviceTimeZone secondsFromGMT] - [deviceTimeZone daylightSavingTimeOffset];
    
    NSString * deviceTimeString = [_dateFormatter stringFromDate:deviceDate];
    NSString * deviceUtcString =@"";
    
    deviceUtcString =  [NSString stringWithFormat:@"UTC %c%02li:%02li - %@",
                        deviceSeconds > 0 ? '+' : '-', labs(deviceSeconds) / 3600, (labs(deviceSeconds) % 3600) / 60,
                        [deviceTimeZone localizedName:NSTimeZoneNameStyleShortDaylightSaving locale:[NSLocale currentLocale]]];
    NSString * hqTimeString = nil;
    NSString * hqUtcString = nil;
    if (locationTimeZone != nil) {
        [_dateFormatter setTimeZone:locationTimeZone];
        hqTimeString = [_dateFormatter stringFromDate:_date];
        hqUtcString = [NSString stringWithFormat:@"UTC %c%02li:%02li - %@",
                       localUtcOffset > 0 ? '+' : '-', labs(localUtcOffset) / 60, labs(localUtcOffset) % 60,
                       localTimeZone.name];
    }
    else {
        hqTimeString = localTimeZone == nil ? NSLocalizedString(@"TIME_ZONE_VERIFICATION_NO_TIME_ZONE_TEXT", nil) :
            NSLocalizedString(@"TIME_ZONE_VERIFICATION_BAD_TIME_ZONE_TEXT", nil);
        hqUtcString = nil;
    }

   
        NSMutableAttributedString * devString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@ - %@", deviceTimeString, deviceUtcString]];
        [devString addAttributes:@{NSFontAttributeName: [UIFont systemFontOfSize:15]}
                           range:NSMakeRange(0, devString.length)];
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone  && (IS_IPHONE_6P || IS_IPHONE_6)) {

        [devString addAttributes:@{NSForegroundColorAttributeName: [UIColor lightGrayColor]}
                           range:NSMakeRange(deviceTimeString.length, devString.length - deviceTimeString.length)];}
    else{
        [devString addAttributes:@{NSForegroundColorAttributeName: [UIColor darkGrayColor]}
                           range:NSMakeRange(deviceTimeString.length, devString.length - deviceTimeString.length)];}
        
        NSMutableAttributedString * hqString = nil;
        if (hqUtcString != nil) {
            hqString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@ - %@", hqTimeString, hqUtcString]];
             if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone  && (IS_IPHONE_6P || IS_IPHONE_6)) {
            [hqString addAttributes:@{NSForegroundColorAttributeName: [UIColor lightGrayColor]}
                              range:NSMakeRange(hqTimeString.length, hqString.length - hqTimeString.length)];
             }
             else{
                 [hqString addAttributes:@{NSForegroundColorAttributeName: [UIColor darkGrayColor]}
                                   range:NSMakeRange(hqTimeString.length, hqString.length - hqTimeString.length)];
             }
        }
        else {
            hqString = [[NSMutableAttributedString alloc] initWithString: hqTimeString];
        }
        [hqString addAttributes:@{NSFontAttributeName: [UIFont systemFontOfSize:15]}
                           range:NSMakeRange(0, hqString.length)];
        _deviceTimeLabel.attributedText = devString;
        _headquartersTimeLabel.attributedText = hqString;
        [devString release];
        [hqString release];
    
    
    if (localTimeZone != nil) {
        NSCalendar * gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
        NSInteger deviceDay = [gregorian ordinalityOfUnit:NSCalendarUnitDay inUnit: NSCalendarUnitEra forDate:deviceDate];
        NSInteger hqDay = [gregorian ordinalityOfUnit:NSCalendarUnitDay inUnit: NSCalendarUnitEra forDate:_date];
        
        if (deviceDay < hqDay) {
            _dayMismatchLabel.text = [NSString stringWithFormat:NSLocalizedString(@"TIME_ZONE_VERIFICATION_DAYS_AHEAD_FORMAT", nil), hqDay - deviceDay];
        }
        else if (deviceDay > hqDay) {
            _dayMismatchLabel.text = [NSString stringWithFormat:NSLocalizedString(@"TIME_ZONE_VERIFICATION_DAYS_BEHIND_FORMAT", nil), deviceDay - hqDay];
        }
        else {
            _dayMismatchLabel.text = nil;
        }
        
        NSInteger deviceMinutes = [gregorian ordinalityOfUnit:NSCalendarUnitMinute inUnit:NSCalendarUnitDay forDate:deviceDate];
        NSInteger hqMinutes = [gregorian ordinalityOfUnit:NSCalendarUnitMinute inUnit:NSCalendarUnitDay forDate:_date];
        [gregorian release];
        NSInteger hours = labs(deviceMinutes - hqMinutes) / 60;
        NSInteger minutes = labs(deviceMinutes - hqMinutes) % 60;
        NSString * hoursText = hours == 0 ? @"" : [NSString stringWithFormat:NSLocalizedString(@"TIME_ZONE_VERIFICATION_HOURS_FORMAT", nil), hours];
        if (deviceMinutes < hqMinutes) {
            _timeMismatchLabel.text = [NSString stringWithFormat:NSLocalizedString(@"TIME_ZONE_VERIFICATION_MINUTES_AHEAD_FORMAT", nil), hoursText, minutes];
        }
        else if (deviceMinutes > hqMinutes) {
            _timeMismatchLabel.text = [NSString stringWithFormat:NSLocalizedString(@"TIME_ZONE_VERIFICATION_MINUTES_BEHIND_FORMAT", nil), hoursText, minutes];
        }
        else {
            _timeMismatchLabel.text = nil;
        }
    }
    else {
        _dayMismatchLabel.text = nil;
        _timeMismatchLabel.text = nil;
    }
    
    NSInteger deviceUtcOffset = [deviceTimeZone secondsFromGMT] / 60;
    if (localTimeZone == nil || localUtcOffset != deviceUtcOffset) {
    if (localTimeZone != nil) {
        _zoneMismatchLabel.text = [NSString stringWithFormat:@"UTC %c%02li:%02li - %@",
                                   localUtcOffset > 0 ? '+' : '-', labs(localUtcOffset) / 60, labs(localUtcOffset) % 60, localTimeZone.name];
    }
    else {
        _zoneMismatchLabel.text = NSLocalizedString(@"TIME_ZONE_VERIFICATION_NO_TIME_ZONE_TEXT", nil);
    }
    }
    else {
        _zoneMismatchLabel.text = nil;
    }
    
    CGRect frame = _timeDifferenceView.frame;
    CGPoint center = _timeDifferenceView.center;
    CGFloat y = 0.0;
    CGFloat h = _dayMismatchLabel.frame.size.height;
    if (_dayMismatchLabel.text.length > 0) {
        _dayMismatchLabel.frame = CGRectMake(0, y, frame.size.width, h);
        y += h;
    }
    else {
        [_dayMismatchLabel removeFromSuperview];
    }
    if (_timeMismatchLabel.text.length > 0) {
        _timeMismatchLabel.frame = CGRectMake(0, y, frame.size.width, h);
        y += h;
    }
    else {
        [_timeMismatchLabel removeFromSuperview];
    }
    if (_zoneMismatchLabel.text.length > 0) {
        _zoneMismatchLabel.frame = CGRectMake(0, y, frame.size.width, h);
        y += h;
    }
    else {
        [_zoneMismatchLabel removeFromSuperview];
    }
    frame.size.height = y;
    _timeDifferenceView.frame = frame;
    _timeDifferenceView.center = center;
}

+(void)verifyTime:(void(^)())completedBlock  {
    GetDateTimeOperation * operation = [[GetDateTimeOperation alloc] init];
    __block BOOL isCanceled = NO;
    
    BackgroundOperationViewController* backgroundTimeVerificationViewController = [[BackgroundOperationViewController alloc] init];
    backgroundTimeVerificationViewController.title = NSLocalizedString(@"VERIFY_TIME_SETTINGS_TITLE", nil);
    backgroundTimeVerificationViewController.hideCancel = NO;
    backgroundTimeVerificationViewController.onCancel = ^{
        [operation cancel];
        isCanceled = YES;
    };
    [backgroundTimeVerificationViewController startTask:^{
        NSAutoreleasePool* pool = [[NSAutoreleasePool alloc] init];
        SyncOperationQueue * queue = [[[SyncOperationQueue alloc] init] autorelease];
        [queue addOperations:@[operation] waitUntilFinished:YES breakOnError:YES];
        [pool release];
    } completed:^{
        
        if (operation.result) {
            
            NSDate * deviceDate = [NSDate date];
            NSDate * hqDate = operation.date;
            Location * localLocation = [Location getInstanceById:[AppSettingManager instance].locationId];
            TimeZone * localTimeZone = nil;
            NSInteger localUtcOffset = 0;
            if (localLocation.timeZoneID != nil) {
                
                localTimeZone = [TimeZone getInstanceById:localLocation.timeZoneID];
                localUtcOffset = [[NSDecimalNumber decimalNumberWithDecimal:CPDecimalMultiply(localTimeZone.utcOffset, CPDecimalFromInteger(3600))] integerValue];
            }
            
            NSTimeZone * deviceTimeZone = [NSTimeZone localTimeZone];
            NSInteger deviceUtcOffset = [deviceTimeZone secondsFromGMT] - [deviceTimeZone daylightSavingTimeOffset];
            if (fabs([deviceDate timeIntervalSinceDate:hqDate]) > 60 * 5 || localTimeZone == nil || localUtcOffset != deviceUtcOffset) {
                
                VerifyTimeMessageViewController * verifyTimeMessageViewController = [[VerifyTimeMessageViewController alloc] initWithDate:operation.date timeZone:operation.timeZone];
                [verifyTimeMessageViewController showModal:YES completion:completedBlock];
                [verifyTimeMessageViewController release];
            }
            else if (completedBlock)
                completedBlock();
        }
        else if (!isCanceled) {
            NSError * error = operation.lastErrorObject;
            if ([error.domain isEqualToString:@"WebServiceResponseHTTP"]) {
                [ModalAlert show:NSLocalizedString(@"SERVER_UNAVAILABLE", nil) message:error.localizedDescription completed:completedBlock];
            }
            else {
                [ModalAlert show:NSLocalizedString(@"SERVER_UNAVAILABLE", nil) message:NSLocalizedString(@"SERVER_UNAVAILABLE_DESCRIPTION", nil) completed:completedBlock];
            }
        }
        else if (completedBlock)
            completedBlock();
    }];
    
    [operation release];
    [backgroundTimeVerificationViewController release];
}

@end
