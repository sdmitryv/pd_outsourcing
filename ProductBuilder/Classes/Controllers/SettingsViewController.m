//
//  SettingsViewController.m
//  ProductBuilder
//
//  Created by Lulakov Viacheslav on 5/14/13.
//  Copyright (c) 2013 Cloudworks. All rights reserved.
//

#import "SettingsViewController.h"

@interface SettingsViewController ()

@end

@implementation SettingsViewController

@synthesize itemImageLabel;
@synthesize itemImageView;
@synthesize itemImageSwitch;
@synthesize line1View;
@synthesize line1Label;
@synthesize line1ValueLabel;
@synthesize line1Button;
@synthesize line2View;
@synthesize line2Label;
@synthesize line2ValueLabel;
@synthesize line2Button;

- (id)init
{
    self = [super initWithNibName:@"SettingsView_iPhone" bundle:nil];
    if (self) {
        
        AppSettingManager * settings = [AppSettingManager instance];
        _showItemImage = settings.isDisplayItemImage;
        _itemDescriptionMode = settings.itemDescriptionModeView;
        _itemAttributeMode = settings.itemAttributeModeView;
        
        _descriptionListViewController = [[SimpleTableListViewController alloc] init];

        _descriptionListViewController.titleText = @"Line 1";
        _descriptionListViewController.selectedIndex = _itemDescriptionMode - 1;
        _descriptionListViewController.delegate = self;
        
        _attributeListViewController = [[SimpleTableListViewController alloc] init];
        _attributeListViewController.items = @[@"Quantity", @"Attribute 1", @"Attribute 2", @"Attribute 3", @"Discount %"];
        _attributeListViewController.titleText = @"Line 2";
        _attributeListViewController.selectedIndex = _itemAttributeMode - 1;
        _attributeListViewController.delegate = self;

    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [itemImageView setRoundedCorners:UIViewRoundedCornerUpperLeft | UIViewRoundedCornerUpperRight radius:5.0f];
    [line1View setRoundedCorners:0 radius:5.0f];
    [line2View setRoundedCorners:UIViewRoundedCornerLowerLeft | UIViewRoundedCornerLowerRight radius:5.0f];
    
    itemImageSwitch.on = _showItemImage;
    line1ValueLabel.text = (_descriptionListViewController.items)[_itemDescriptionMode - 1];
    line2ValueLabel.text = (_attributeListViewController.items)[_itemAttributeMode - 1];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc {
    [_descriptionListViewController release];
    [_attributeListViewController release];
    
    [itemImageLabel release];
    [itemImageView release];
    [itemImageSwitch release];
    [line1View release];
    [line1Label release];
    [line1ValueLabel release];
    [line1Button release];
    [line2View release];
    [line2Label release];
    [line2ValueLabel release];
    [line2Button release];

    [super dealloc];
}

#pragma mark - Actions

-(IBAction)cancelButtonPress:(id)sender
{
	[self dismissViewControllerAnimated:YES completion:nil];
}

-(IBAction)saveButtonPress:(id)sender
{
    [[AppSettingManager instance] setItemSettingDisplayImage:itemImageSwitch.on descriptionMode:_itemDescriptionMode attributeMode:_itemAttributeMode];
	[self dismissViewControllerAnimated:YES completion:nil];
}

-(IBAction)line1ButtonPress:(id)sender
{
    [_descriptionListViewController presentInController:self];
}

-(IBAction)line2ButtonPress:(id)sender
{
    [_attributeListViewController presentInController:self];
}

#pragma mark -
#pragma mark SimpleTableListViewControllerDelegate

-(void)simpleTableListViewController:(SimpleTableListViewController*)controller didSelectIndex:(NSNumber*)anIndex {
	if (controller == _descriptionListViewController) {
        _itemDescriptionMode = [anIndex intValue] + 1;
        line1ValueLabel.text = (_descriptionListViewController.items)[_itemDescriptionMode - 1];
    }
	else if (controller == _attributeListViewController) {
        _itemAttributeMode = [anIndex intValue] + 1;
        line2ValueLabel.text = (_attributeListViewController.items)[_itemAttributeMode - 1];
    }
}

@end
