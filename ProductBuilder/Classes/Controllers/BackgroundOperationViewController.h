//
//  BackgroundOperationViewController.h
//  CloudworksPOS
//
//  Created by valera on 7/27/11.
//  Copyright 2011 Retail Teamwork Ukraine. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UILoopProgressView.h"
#import "UIRoundedCornersView.h"

typedef NS_ENUM(NSUInteger, BackgroundOperationStyle) {BackgroundOperationStyleDefault,
    BackgroundOperationStyleSmallGrey, BackgroundOperationStyleCustom, BackgroundOperationStyleCustom2Lines, BackgroundOperationStyleAlert};

typedef NS_ENUM(NSUInteger, BackgroundOperationViewState) {BackgroundOperationViewStateNone, BackgroundOperationViewStateShow,
    BackgroundOperationViewStateHide};

@interface BackgroundOperationViewController : UIViewController {
    
    UIProgressView *progressView;
    UILabel *progressLabel;
    dispatch_queue_t queue;
    UIView *    backgroundView;
    BOOL loopProgress;
    BOOL hideCancel;
    BOOL isCancelling;
    dispatch_block_t onCancel;
    UIView * firstResponderView;
    NSTimer* delayedInvokeTimer;
    NSTimer* durationTimer;
    CGFloat originalHeight;
    IBOutlet UIRoundedCornersView *contentView;
    
    BOOL idleTimerDisabledBeforeStart;
    
    BackgroundOperationStyle _style;
    BackgroundOperationViewState _state;
}
@property (nonatomic, retain) IBOutlet UIProgressView *progressView;
@property (nonatomic, retain) IBOutlet UILabel *progressLabel;
@property (nonatomic, readonly)BOOL isShown;
@property (nonatomic, readonly) dispatch_queue_t queue;
- (id)initWithStyle:(BackgroundOperationStyle)style;
- (void)show;
- (void)hide;
- (void)startTask:(dispatch_block_t)block completed:(dispatch_block_t)completed;
- (void)startTask:(dispatch_block_t)task delay:(CGFloat)delay completed:(dispatch_block_t)completed;
- (void)setTitle:(NSString*)text;
- (void)showView:(UIView*)aView;
- (void)showView:(UIView*)aView duration:(NSTimeInterval)interval;

@property (nonatomic, assign) BOOL loopProgress;
@property (nonatomic, assign) float progress;
@property (retain, nonatomic) IBOutlet UIButton *bCancel;
@property (nonatomic, assign)BOOL hideCancel;
@property (nonatomic, readonly)BOOL isCancelling;
@property (nonatomic, copy) dispatch_block_t onCancel;
@property(nonatomic,readonly)NSTimer* delayedInvokeTimer;
@property(nonatomic,retain)UIView* parentView;
@property(nonatomic,readonly)BackgroundOperationStyle style;
- (IBAction)bCancelClick:(id)sender;

+(NSInteger)backgroundOperationsCount;
+(void)closeBackgroundOperations;
-(NSString *)nibNameForStyle:(BackgroundOperationStyle)style;

@end
