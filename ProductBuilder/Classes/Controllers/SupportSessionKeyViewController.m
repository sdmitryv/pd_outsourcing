//
//  SupportSessionKeyViewController.m
//  ProductBuilder
//
//  Created by Vitaliy Gervazuk on 2/16/16.
//  Copyright © 2016 Cloudworks. All rights reserved.
//

#import "SupportSessionKeyViewController.h"
#import "UITextField+Input.h"
#import "UIRoundedCornersView.h"

@implementation SupportSessionKeyViewController

-(NSString *)sessionKey {
    return sessionKey;
}

-(NSString *)title {
    return NSLocalizedString(@"SUPPORT_ENTER_SESSION_KEY_TITLE", nil);
}

-(void)loadView {
    self.view = [[[UIRoundedCornersView alloc]initWithFrame:CGRectMake(0, 0, 507, 100)] autorelease];
    self.view.backgroundColor = [UIColor whiteColor];
    UILabel* currentTitleLabel = [[UILabel alloc]initWithFrame:CGRectMake(5,40, IS_IPHONE ? 100 : 170, 20)];
    currentTitleLabel.font = [UIFont systemFontOfSize:17];
    currentTitleLabel.adjustsFontSizeToFitWidth = YES;
    currentTitleLabel.minimumScaleFactor = 0.5;
    currentTitleLabel.textAlignment = NSTextAlignmentRight;
    currentTitleLabel.text = [NSString stringWithFormat:@"%@:", NSLocalizedString(@"SUPPORT_SESSION_KEY_TITLE", nil)];
    currentTitleLabel.backgroundColor = [UIColor clearColor];
    currentTitleLabel.textColor = [UIColor darkGrayColor];
//    currentTitleLabel.hidden = IS_IPHONE_6_OR_MORE;
    
    sessionKeyTextField = [[UITextFieldNumeric alloc]initWithFrame:CGRectMake(currentTitleLabel.frame.origin.x + currentTitleLabel.frame.size.width + 10, currentTitleLabel.frame.origin.y + currentTitleLabel.frame.size.height/2 - 31/2, 226, 31)];
    sessionKeyTextField.text = nil;
    sessionKeyTextField.font = [UIFont systemFontOfSize:17];
    sessionKeyTextField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    sessionKeyTextField.borderStyle = UITextBorderStyleRoundedRect;
    sessionKeyTextField.clearButtonMode = UITextFieldViewModeWhileEditing;
    sessionKeyTextField.returnKeyType = UIReturnKeyDone;
    sessionKeyTextField.keyboardType = UIKeyboardTypeNumberPad;
    sessionKeyTextField.style = NSTextFormatterNumbersStyle;
    sessionKeyTextField.enableNil = TRUE;
    sessionKeyTextField.minValue = @0;
    sessionKeyTextField.text = nil;
    sessionKeyTextField.maxTextLength = 12;
    sessionKeyTextField.delegate = self;
    [sessionKeyTextField addTarget:self action:@selector(textFieldEditingChanged:) forControlEvents:UIControlEventEditingChanged];
    
    [self.view addSubview:currentTitleLabel];
    [self.view addSubview:sessionKeyTextField];
    
    [currentTitleLabel release];
    [self checkSaveEnabled];
}

-(void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    [self.dialogViewController.bSave setTitle:NSLocalizedString(@"OKBTN_TITLE", nil) forState:UIControlStateNormal];
    
    [sessionKeyTextField becomeFirstResponder];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    if (![self checkSaveEnabled]) {
        return NO;
    }
    else {
        [[self dialogViewController] save];
    }
    
    return YES;
}

-(void)textFieldDidBeginEditing:(UITextField *)textField{
    if (textField.text.length){
        [textField selectAllNoMenu];
    }
}

-(void)textFieldEditingChanged:(id)sender{
    if (sender == sessionKeyTextField){
        [self checkSaveEnabled];
    }
}

-(BOOL)checkSaveEnabled{
    self.dialogViewController.bSave.enabled = sessionKeyTextField.text.length > 0;
    return self.dialogViewController.bSave.enabled;
}

-(BOOL)willSave {
    if (![self checkSaveEnabled]) {
        return FALSE;
    }
    
    sessionKey = [sessionKeyTextField.text retain];
    
    return TRUE;
}

-(void)dealloc {
    [sessionKeyTextField release];
    [sessionKey release];
    
    [super dealloc];
}

@end
