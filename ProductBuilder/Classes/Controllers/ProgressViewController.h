//
//  Class.h
//  iPadPOS
//
//  Created by valera on 5/5/11.
//  Copyright 2011 DBBestTechnologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UILoopProgressView.h"
#import "BackgroundOperationViewController.h"
#import "YLProgressBar.h"

@interface ProgressViewController : UIViewController<UIWebViewDelegate> {
    IBOutlet	UIView		* progressView;
    IBOutlet	YLProgressBar	* authorizationProgressView;
    IBOutlet	UIImageView	* resultImageView;
    IBOutlet    UILabel*      processingTextLabel;
    
    IBOutlet UIView *vBottomTitle;
    UIImage*    imageOk;
    UIImage*    imageError;
    UIColor*    colorTitleSuccess;
    UIColor*    colorDetailsSuccess;
    UIColor*    colorTitleError;
    UIColor*    colorDetailsError;
    UIColor*    colorTitleBottom;
    UIFont*     fontTitle;
    UIFont*     fontDetails;
    
    UIImage*    imageOkDefault;
    UIImage*    imageErrorDefault;
    UIColor*    colorTitleSuccessDefault;
    UIColor*    colorDetailsSuccessDefault;
    UIColor*    colorTitleErrorDefault;
    UIColor*    colorDetailsErrorDefault;
    UIFont*     fontTitleDefault;
    UIFont*     fontDetailsDefault;
    UIFont*     fontTitleBottomDefault;
    
    CGFloat     verticalSpaceBetweenTitleAndDetails;
    CGFloat     verticalSpaceBetweenDetailsAndBottomTitle;
    CGFloat     horizontalSpaceBetweenImageAndTitle;
    CGFloat     rigthMarginOfTitleAndDetailsOriginal;
    CGSize      imageSize;
    BOOL        isErrorShown;
    
    BackgroundOperationViewController* backgroundController;
    
@private
    dispatch_queue_t queue;
}
@property (retain, nonatomic) IBOutlet UILabel *vBottomTitleLabel;
@property (retain, nonatomic) IBOutlet UILabel *vTitleLabel;
@property (retain, nonatomic) IBOutlet UIView *vErrorMessageView;
@property (retain, nonatomic) IBOutlet UITextView *detailsTextView;
- (void)startAuthorization:(dispatch_block_t)block;
- (void)startAuthorization:(dispatch_block_t)block title:(NSString*)title;
- (void)showAuthorizationResult:(NSString*)titleText detailsText:(NSString*)detailsText
                        isError:(BOOL)isError animated:(BOOL)animated;
- (void)showAuthorizationResult:(NSString*)titleText detailsText:(NSString*)detailsText
                bottomTitleText:(NSString*)bottomTitle
                isError:(BOOL)isError animated:(BOOL)animated
                  onFinishBlock:(void(^)())onFinishBlock;
-(void)showBottomTitle:(NSString*)bottomTitle animated:(BOOL)animated;
-(void)showBottomTitle:(NSString*)bottomTitle animated:(BOOL)animated onFinishBlock:(void(^)())onFinishBlock;
- (void)hideAuthozisationResultAnimated:(BOOL)animated;
- (void)hideAuthozisationResultAnimated:(BOOL)animated completed:(void(^)())completed;
- (void)setProcessingTextMessage:(NSString*)text;
- (void) start;
- (void)showTextMessage:(NSString *)message title:(NSString *)title;
@property (retain, nonatomic) UIImage* imageOk;
@property (retain, nonatomic) UIImage* imageError;
@property (retain, nonatomic) UIColor* colorTitleSuccess;
@property (retain, nonatomic) UIColor* colorDetailsSuccess;
@property (retain, nonatomic) UIColor* colorTitleError;
@property (retain, nonatomic) UIColor* colorDetailsError;
@property (retain, nonatomic) UIColor* colorTitleBottom;
@property (retain, nonatomic) UIFont* fontTitle;
@property (retain, nonatomic) UIFont* fontDetails;
@property (retain, nonatomic) UIFont* fontTitleBottom;

@property (retain, nonatomic) UIImage* imageOkDefault;
@property (retain, nonatomic) UIImage* imageErrorDefault;
@property (retain, nonatomic) UIColor* colorTitleSuccessDefault;
@property (retain, nonatomic) UIColor* colorDetailsSuccessDefault;
@property (retain, nonatomic) UIColor* colorTitleErrorDefault;
@property (retain, nonatomic) UIColor* colorDetailsErrorDefault;
@property (retain, nonatomic) UIColor* colorTitleBottomDefault;
@property (retain, nonatomic) UIFont* fontTitleDefault;
@property (retain, nonatomic) UIFont* fontDetailsDefault;
@property (retain, nonatomic) UIFont* fontTitleBottomDefault;
@property (assign, nonatomic)CGSize imageSize;
@end
