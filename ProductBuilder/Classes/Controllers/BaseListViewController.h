//
//  BaseListViewController.h
//  CloudworksPOS
//
//  Created by Lulakov Viacheslav on 7/8/11.
//  Copyright 2011 Cloudworks. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GridTable.h"
#import "UIViewControllerExtended.h"
#import "NSSortedArray.h"
#import "IObjectList.h"
#import "UIRoundedCornersView.h"
#import "RTBarcodeButton.h"
#import "RTBaseListView.h"

@class BaseDetailsViewController;

@interface BaseListViewController : UIViewControllerExtended <GridTableDelegate, UISearchBarDelegate, UIScrollViewDelegate, UITableViewDataSource, UITableViewDelegate, RTBaseListViewDelegate> {
    id<IObjectList>           itemList;
    UIImage                 * upArrow;
    UIImage                 * downArrow;
    NSString                * title;
    
    // Controls
    
	UILabel                * titleButton;
    UIToolbar               * toolbar;
    UISegmentedControl      * filterSegmentedControl;
    GridTable               * itemGridTable;
    UITableView             * itemTableView;
	UISearchBar             * searchBar;
    UIView                  * itemsView;
    UILabel                 * itemsPlaceHolderLabel;
    UIRoundedCornersView    * detailsTitleView;
    UIButton                * detailsButton;
    UIView                  * detailsView;
    UIRoundedCornersView    * totalView;
    UILabel                 * totalLabel;
    UIView *contentView;
    UIPageControl           * pageControl;
    
    BaseDetailsViewController * detailsController;
    NSMutableArray* leftBarButtons;
    NSMutableArray* rightBarButtons;
    NSMutableArray* leftBarDividers;
    NSMutableArray* rightBarDividers;
    
    IBOutlet UIBarButtonItem *searchBarButtonItem;
}

@property (nonatomic, retain) IBOutlet	UILabel        * titleButton;
@property (retain, nonatomic) IBOutlet  UILabel         *titleLabel;
@property (nonatomic, retain) IBOutlet	UIToolbar       * toolbar;
@property (retain, nonatomic) IBOutlet UIView * buttonsView;
@property (nonatomic, retain) IBOutlet	UISegmentedControl  * filterSegmentedControl;
@property (nonatomic, retain) IBOutlet	UISearchBar		* searchBar;
@property (retain, nonatomic) IBOutlet RTBarcodeButton *barcodeButton;
@property (nonatomic, retain) IBOutlet  UIView          * itemsView;
@property (nonatomic, retain) IBOutlet  UILabel         * itemsPlaceHolderLabel;
@property (nonatomic, retain) IBOutlet  UIView          * detailsTitleView;
@property (nonatomic, retain) IBOutlet  UIButton        * detailsButton;
@property (nonatomic, retain) IBOutlet  UIView          * detailsView;
@property (nonatomic, retain) IBOutlet  UIView          * totalView;
@property (nonatomic, retain) IBOutlet  UILabel         * totalLabel;
@property (nonatomic, retain) IBOutlet UIView *contentView;
@property (nonatomic, retain) IBOutlet UIPageControl    * pageControl;
@property (nonatomic, retain) IBOutlet  RTBaseListView  * itemsBaseListView;
@property (nonatomic, retain) NSString *countFormatString;
@property (nonatomic, readonly)BOOL itemDetailsIsHidden;
@property (retain, nonatomic) IBOutlet UIRoundedCornersView *backView;

- (IBAction)returnButtonClick;
- (IBAction)detailsButtonClick;
- (IBAction)filterValueChanged:(id)sender;
- (IBAction)controlPageValueChanged:(id)sender;
- (IBAction)keyboardButtonClick;

- (id)initWithTitle:(NSString *)text detailsController:(BaseDetailsViewController *)controller;
- (void)buildToolBar;
- (void)loadItems;
- (void)adjustTableView;
-(void)setDetailsIsHidden:(BOOL)value animated:(BOOL)animated;
-(BOOL)shouldShowToolBarButton3;
-(UIButton*)leftBarButtonByIndex:(NSUInteger)index;
-(UIButton*)rightBarButtonByIndex:(NSUInteger)index;
-(UIView*)leftBarDividerByIndex:(NSUInteger)index;
-(UIView*)rightBarDividerByIndex:(NSUInteger)index;
@end
