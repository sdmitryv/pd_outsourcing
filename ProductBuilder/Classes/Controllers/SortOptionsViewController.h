//
//  SortOptionsViewController.h
//  ProductBuilder
//
//  Created by Alexander Martyshko on 7/9/15.
//  Copyright (c) 2015 Cloudworks. All rights reserved.
//

#import "RTDialogViewController.h"
#import "GridTable.h"

@class SortOptionsViewController;
@protocol SortOptionsViewControllerDelegate <NSObject>
-(void)sortOptionsController:(SortOptionsViewController *)controller applySortWithDescription:(GridTableColumnDescription *)sortDescription order:(SortingMode)order;
@end


@interface SortOptionsViewController : RTDialogContentViewController <UITableViewDelegate, UITableViewDataSource>
@property (nonatomic, retain) GridTableColumnDescription *currentSortDescription;
@property (nonatomic, assign) id<SortOptionsViewControllerDelegate> sortDelegate;
-(id)initWithAvailableSortDescriptions:(NSArray *)sortDescriptions currentSortDescription:(GridTableColumnDescription *)currentSortDescription order:(SortingMode)order;
@end



