//
//  RTDialogView.h
//  CloudworksPOS
//
//  Created by valera on 8/9/11.
//  Copyright 2011 Retail Teamwork Ukraine. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIViewControllerExtended.h"
#import "HardwareViewController.h"
#import "RTAlertView.h"

@class RTDialogViewController;
@class UIRoundedCornersView;
@interface RTDialogContentViewController : UIViewControllerExtended {
    RTDialogViewController* dialogViewController;
}
@property (nonatomic, assign) RTDialogViewController* dialogViewController;
@property (nonatomic, copy) BOOL(^validate)(void);
-(NSString*)title;
-(NSString*)selectButtonTitle;
-(id)selectedValue;
-(void)setSelectedValue:(id)value;
-(BOOL)willSave;
-(BOOL)willCancel;
-(void)showModal:(void (^)(void))completion;
-(void)close;
-(void)willClose;
-(void)didClose;
@end

typedef NS_ENUM(NSUInteger, RTDialogResult) {RTDialogResultOK = 1, RTDialogResultCancel = 0};

@interface RTDialogViewController : UIViewControllerExtended {
    
    UILabel *vTitle;
    UIView *vContentView;
    UIButton *bCancel;
    UIButton *bSave;
    RTDialogContentViewController* contentController;
    RTDialogResult dialogResult;
    BOOL cancelling, _saving;
}
@property (nonatomic, retain) IBOutlet UILabel *vTitle;
@property (nonatomic, retain) IBOutlet UIView *vContentView;
@property (nonatomic, retain) IBOutlet UIRoundedCornersView *backView;

@property (nonatomic, retain) IBOutlet UIButton *bCancel;
@property (nonatomic, retain) IBOutlet UIButton *bSave;
@property (nonatomic, retain, readonly)RTDialogContentViewController* contentController;
@property (nonatomic, assign)RTDialogResult dialogResult;
@property (nonatomic, assign)NSObject* ignoredAlertViewOnSave;
@property(nonatomic, assign)BOOL cancelling;
@property(nonatomic,readonly) BOOL saving;
- (IBAction)save:(id)sender;
- (void)save;
- (IBAction)cancel:(id)sender;
- (void)cancel;
- (void)close;

-(id)initWithContentViewController:(RTDialogContentViewController*)contentController;
+(void)showModal:(RTDialogContentViewController*)contentController completion:(void (^)(void))completion;
+(void)showModal:(RTDialogContentViewController*)contentController delegate:(id<UIModalViewControllerDelegate>) aDelegate;

-(void)setSaveHidden:(BOOL)value;

@end
