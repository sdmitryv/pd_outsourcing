//
//  SettingsAreaDetailViewController.swift
//  ProductBuilder
//
//  Created by Julia Korevo on 12/27/16.
//  Copyright © 2016 Cloudworks. All rights reserved.
//

class SettingsAreaDetailViewController: UIViewController, SettingsAreaRootViewControllerDelegate{
    @IBOutlet weak var detailsViewTitleLabel: UILabel!
    @IBOutlet weak var navItem: UINavigationItem!
    
     var copyBackUpContr: CopyBackupViewController?
     var backgroundController:BackgroundOperationController50?
     var generalInfoController : GeneralInfoViewController?
     var helpViewController : HelpSupportViewController?
     var currentSettingsArea: SettingsArea?
        
    func settingsAreaRootViewController(_ rootController:SettingsAreaRootViewController, areaSelected: SettingsArea) {
        var hasDetailController = false
        detailsViewTitleLabel.text = areaSelected.title
        copyBackUpContr?.view.isHidden = true
        generalInfoController?.view.isHidden = true
        helpViewController?.view.isHidden = true
        navItem.rightBarButtonItem = nil

        
        switch(areaSelected) {
        case .generalInfo:
            if generalInfoController == nil {
                generalInfoController = GeneralInfoViewController()
                self.view.addSubview(generalInfoController!.view)
                self.addChildViewController(generalInfoController!)
                
                generalInfoController!.view.translatesAutoresizingMaskIntoConstraints = false
                let hconstraints = NSLayoutConstraint.constraints(withVisualFormat: "H:|[detailContr]|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: ["detailContr":generalInfoController!.view])
                NSLayoutConstraint.activate(hconstraints)
                let vconstraints = NSLayoutConstraint.constraints(withVisualFormat: "V:|[detailContr]|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: ["detailContr":generalInfoController!.view])
                NSLayoutConstraint.activate(vconstraints)
            }
            generalInfoController?.view.isHidden = false
            generalInfoController?.generalTableView.reloadData()
            hasDetailController = true
        case .printers:
           
            hasDetailController = true
            
        case .helpAndSupport:
            if helpViewController == nil {
                helpViewController = HelpSupportViewController()
                self.view.addSubview(helpViewController!.view)
                self.addChildViewController(helpViewController!)
                helpViewController!.view.translatesAutoresizingMaskIntoConstraints = false
                let hconstraints = NSLayoutConstraint.constraints(withVisualFormat: "H:|[detailContr]|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: ["detailContr":helpViewController!.view])
                NSLayoutConstraint.activate(hconstraints)
                let vconstraints = NSLayoutConstraint.constraints(withVisualFormat: "V:|[detailContr]|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: ["detailContr":helpViewController!.view])
                NSLayoutConstraint.activate(vconstraints)
                

            }
            helpViewController?.view.isHidden = false
            helpViewController?.generalTableView.reloadData()
            hasDetailController = true
            
        case .language:
            let selectLanguage = SelectLanguageViewController50()
            let navControl = RTNavigationController50(rootViewController: selectLanguage)
            navControl.modalPresentationStyle = UIModalPresentationStyle.formSheet;
            self.present(navControl, animated: true)
            
        case .reinit:
            
            let alert = RTAlertController(title: NSLocalizedString("SETTINGS_INITIALIZE_WARNING_TITLE", comment: ""), message: NSLocalizedString("SETTINGS_INITIALIZE_WARNING_MESSAGE", comment: ""), preferredStyle: RTAlertControllerStyle.alert)
            alert.addAction(RTAlertAction(title: NSLocalizedString("CONTINUEBTN_TITLE", comment: ""), style: RTAlertActionStyle.default, handler: { action in
                
                
            }))
            alert.addAction(RTAlertAction(title: NSLocalizedString("CANCELBTN_TITLE", comment: ""), style: RTAlertActionStyle.cancel, handler: nil))
            //self.present(alert, animated: true, completion: nil)
            alert.show()
         
        case .copyBackup:
            if copyBackUpContr == nil {
                copyBackUpContr = CopyBackupViewController()
                self.view.addSubview(copyBackUpContr!.view)
                self.addChildViewController(copyBackUpContr!)
                copyBackUpContr!.view.translatesAutoresizingMaskIntoConstraints = false
                let hconstraints = NSLayoutConstraint.constraints(withVisualFormat: "H:|[detailContr]|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: ["detailContr":copyBackUpContr!.view])
                NSLayoutConstraint.activate(hconstraints)
                let vconstraints = NSLayoutConstraint.constraints(withVisualFormat: "V:|[detailContr]|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: ["detailContr":copyBackUpContr!.view])
                NSLayoutConstraint.activate(vconstraints)
            }
            copyBackUpContr?.view.isHidden = false
            hasDetailController = true
            
        case .checkIndex:
            DataManager.instance().checkValidIndexesImmediately(true)

        case .verifyTimeSettings:
            VerifyTimeMessageViewController50.verifyTime(completionBlock: { (showSuccessMessage) in
                if showSuccessMessage{
                    ModalAlert.show(NSLocalizedString("SETTINGS_VERIFYING_TIME_SUCCESS_LABEL",comment: ""), message: NSLocalizedString("SETTINGS_VERIFYING_TIME_SUCCESS_MESSAGE",comment: ""), completed: { })
                }
            })
        default: VerifyTimeMessageViewController50.verifyTime(completionBlock: { (showSuccessMessage) in
            if showSuccessMessage{
                ModalAlert.show(NSLocalizedString("SETTINGS_VERIFYING_TIME_SUCCESS_LABEL",comment: ""), message: NSLocalizedString("SETTINGS_VERIFYING_TIME_SUCCESS_MESSAGE",comment: ""), completed: { })
            }
        })
        }
        if hasDetailController {
            currentSettingsArea = areaSelected
        }
    }
    
    override func viewDidLoad() {
         super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(SettingsAreaDetailViewController.dataManagerIndexesCheckingWillStart(_:)), name: NSNotification.Name.DataManagerIndexesCheckingWillStart, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(SettingsAreaDetailViewController.dataManagerIndexesCheckingDidEnd(_:)), name: NSNotification.Name.DataManagerIndexesCheckingDidEnd, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(SettingsAreaDetailViewController.dataManagerIndexesWillStartUpdate(_:)), name: NSNotification.Name.DataManagerIndexesWillStartUpdate, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(SettingsAreaDetailViewController.dataManagerIndexesWillEndUpdate(_:)), name: NSNotification.Name.DataManagerIndexesWillEndUpdate, object: nil)
    }
    
    //MARK: - CheckIndexes
    func dataManagerIndexesCheckingWillStart(_ notification:Notification) {
        if backgroundController == nil {

            backgroundController = BackgroundOperationController50(title: NSLocalizedString("STATUS_CHECKING_INDEXES",comment: ""), message: nil, hideCancel: false)
        }
        backgroundController?.onCancel = {
            DataManager.instance().cancelUpdatingIndexes()
        }
        backgroundController?.show()
    }
    
    func dataManagerIndexesCheckingDidEnd(_ notification:Notification) {
      
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) { [weak self] in
        
        self?.backgroundController?.hide()
        self?.backgroundController = nil
           DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            ModalAlert.show(NSLocalizedString("SETTINGS_AREAS_TITLE_CHECK_INDEX_COMPLETED_TITLE",comment: ""), message: NSLocalizedString("SETTINGS_AREAS_TITLE_CHECK_INDEX_COMPLETED_MESSAGE",comment: ""), completed: { })}
            
        }
    }
    
    func dataManagerIndexesWillStartUpdate(_ notification:Notification) {
    }
    
    func dataManagerIndexesWillEndUpdate(_ notification:Notification) {
    }
}
