//
//  GeneralInfoViewController50.swift
//  ProductBuilder
//
//  Created by Julia Korevo on 2/1/17.
//  Copyright © 2017 Cloudworks. All rights reserved.
//

import Foundation
class GeneralInfoViewController : UIViewController, UITableViewDelegate, UITableViewDataSource{
    let settingValues : NSMutableDictionary? = MainViewController50.sharedInstance()?.settingValues
    
    @IBOutlet weak var generalTableView : UITableView!
    
    required init(){
        super.init(nibName: "GeneralInfoView50", bundle: Bundle.main)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        generalTableView.register(UINib(nibName: GeneralInfoCommonCell.nibName, bundle: nil), forCellReuseIdentifier: "GeneralCell")
        generalTableView.separatorStyle = UITableViewCellSeparatorStyle.none
        generalTableView.tableFooterView = UIView()
        generalTableView.rowHeight = UITableViewAutomaticDimension
        generalTableView.estimatedRowHeight = 20
        generalTableView.scrollIndicatorInsets = UIEdgeInsetsMake(0, 0, 0, -25)
        generalTableView.clipsToBounds = false
    }
    
    //MARK: - UITableViewDelegate
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return 1
        case 1:
            return 1
        case 2:
            return 2
        default:
           return 1
        }
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch ((indexPath as NSIndexPath).section, (indexPath as NSIndexPath).row) {
        case (0,0):
            let cell = tableView.dequeueReusableCell(withIdentifier: "GeneralCell", for: indexPath) as!  GeneralInfoCommonCell
            cell.leftLabel.text = NSLocalizedString("SETTINGS_GENERAL_INFO_COMPANY_NAME_TITLE", comment: "")
            if let companyName = settingValues?["company"] {
                cell.rightLabel.text = companyName as? String
            }
            else{
                cell.rightLabel.text = ""
            }
            cell.separator.isHidden = true
            return cell
        case (1,0):
            let cell = tableView.dequeueReusableCell(withIdentifier: "GeneralCell", for: indexPath) as!  GeneralInfoCommonCell
            cell.layoutMargins = UIEdgeInsets.zero
            cell.leftLabel.text = NSLocalizedString("SYNC_SERVER_LABEL_TITLE", comment: "")
            cell.rightLabel.text = AppSettingManager.instance().serverUrl
            cell.separator.isHidden = true
            return cell
        case (2,0):
            let cell = tableView.dequeueReusableCell(withIdentifier: "GeneralCell", for: indexPath) as!  GeneralInfoCommonCell
            cell.layoutMargins = UIEdgeInsets.zero
            cell.leftLabel.text = NSLocalizedString("SETTINGS_GENERAL_INFO_APP_VERSION_TITLE", comment: "")
            cell.rightLabel.text = DeviceAgentManager.sharedInstance().appVersion
            return cell
         case(2,1):
            let cell = tableView.dequeueReusableCell(withIdentifier: "GeneralCell", for: indexPath) as!  GeneralInfoCommonCell
            cell.layoutMargins = UIEdgeInsets.zero
            cell.leftLabel.text = NSLocalizedString("SETTINGS_GENERAL_INFO_DEVICE_ID_TITLE", comment: "")
            cell.rightLabel.text = settingValues?["deviceId"] as? String
            cell.separator.isHidden = true
            return cell
        default: break
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if (tableView == self.generalTableView)
        {
            //Top Left Right Corners
            let maskPathTop = UIBezierPath(roundedRect: cell.bounds, byRoundingCorners: [.topLeft, .topRight], cornerRadii: CGSize(width: 5.0, height: 5.0))
            let shapeLayerTop = CAShapeLayer()
            shapeLayerTop.frame = cell.bounds
            shapeLayerTop.path = maskPathTop.cgPath
            
            //Bottom Left Right Corners
            let maskPathBottom = UIBezierPath(roundedRect: cell.bounds, byRoundingCorners: [.bottomLeft, .bottomRight], cornerRadii: CGSize(width: 5.0, height: 5.0))
            let shapeLayerBottom = CAShapeLayer()
            shapeLayerBottom.frame = cell.bounds
            shapeLayerBottom.path = maskPathBottom.cgPath
            
            //All Corners
            let maskPathAll = UIBezierPath(roundedRect: cell.bounds, byRoundingCorners: [.topLeft, .topRight, .bottomRight, .bottomLeft], cornerRadii: CGSize(width: 5.0, height: 5.0))
            let shapeLayerAll = CAShapeLayer()
            shapeLayerAll.frame = cell.bounds
            shapeLayerAll.path = maskPathAll.cgPath
            
            //No rounding Corners
            let maskPathNone = UIBezierPath(roundedRect: cell.bounds, byRoundingCorners: [.topLeft, .topRight, .bottomRight, .bottomLeft], cornerRadii: CGSize(width: 0, height: 0))
            let shapeLayerNone = CAShapeLayer()
            shapeLayerNone.frame = cell.bounds
            shapeLayerNone.path = maskPathNone.cgPath
            
            switch ((indexPath as NSIndexPath).section, (indexPath as NSIndexPath).row) {
            case (2,0):
                cell.layer.mask = shapeLayerTop
            case (2,1):
                cell.layer.mask = shapeLayerBottom
            case (0,0),(1,0):
               cell.layer.mask = shapeLayerAll
            default:
               cell.layer.mask = shapeLayerNone
            }
        }
    }
  
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0 {
            return 0.0001
        } else {
            return 20
        }
    }
}

class GeneralInfoCommonCell : UITableViewCell{
    
    static let nibName = "GeneralInfoCommonCell50"
    @IBOutlet var leftLabel: UILabel!
    @IBOutlet var rightLabel : UILabel!
    @IBOutlet var separator: UIView!

    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = UITableViewCellSelectionStyle.none

    }
}


