//
//  SettingsAreaViewController50.swift
//  ProductBuilder
//
//  Created by Julia Korevo on 12/27/16.
//  Copyright © 2016 Cloudworks. All rights reserved.
//

class SettingsAreaWrapperViewController: UIViewController{
    var selectedArea:SettingsArea = .defaultArea
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        if (segue.destination is SettingsAreaViewController){
            (segue.destination as! SettingsAreaViewController).selectedArea = selectedArea
        }
    }
    
    static func instantiate() -> SettingsAreaWrapperViewController
    {
        return UIStoryboard(name: "Main50", bundle: nil).instantiateViewController(withIdentifier: "SettingsAreaWrapperViewController") as! SettingsAreaWrapperViewController
    }
}

class SettingsAreaViewController: UISplitViewController{
    
    var selectedArea:SettingsArea = .defaultArea{
        didSet{
            if isViewLoaded{
                handleSelectedArea()
            }
        }
    }
    private func handleSelectedArea(){
        settingsAreaRootViewController?.selectedArea = selectedArea
    }
    
    var settingsAreaRootViewController:SettingsAreaRootViewController?{
        get{
            return (self.viewControllers[0] as? UINavigationController)?.topViewController as? SettingsAreaRootViewController
        }
    }
    
    var settingsAreaDetailViewController:SettingsAreaDetailViewController?{
        get{
            return (self.viewControllers[1] as? UINavigationController)?.topViewController as? SettingsAreaDetailViewController
        }
    }
    
    override func viewDidLoad() {
        
        settingsAreaRootViewController?.delegate = settingsAreaDetailViewController
        handleSelectedArea()
    }
}

//class SettingsAreaStoryboardSegue : UIStoryboardSegue{
//
//    var selectedArea:SettingsArea = .defaultArea{
//        didSet{
//            if let destinationController = destination as? SettingsAreaWrapperViewController{
//                destinationController.selectedArea = selectedArea
//            }
//        }
//    }
//    override init(identifier: String?, source: UIViewController, destination: UIViewController) {
//        super.init(identifier:identifier, source:source, destination:destination)
//    }
//}

