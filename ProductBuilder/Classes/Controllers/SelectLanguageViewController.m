//
//  SelectLanguageViewController.m
//  ProductBuilder
//
//  Created by Sergey Lugovoy on 4/22/15.
//  Copyright (c) 2015 Cloudworks. All rights reserved.
//

#import "SelectLanguageViewController.h"
#import "RTAlertView.h"
#import "AppSettingManager.h"

@interface SelectLanguageViewController () {

}

@property (nonatomic, retain) NSString *currentLanguage;

@end

@implementation SelectLanguageViewController



@synthesize currentLanguage;

-(void)dealloc {
    
    [currentLanguage release];
    [_languageSelector release];
    
    [super dealloc];
}

- (void)viewDidLoad {
    
    [super viewDidLoad];

    [self.dialogViewController.bSave setTitle:NSLocalizedString(@"SAVEBTN_TITLE", nil)
                                     forState:UIControlStateNormal];
    self.dialogViewController.bCancel.enabled = YES;
}


- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    [self loadData];
}


- (void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
}


-(void)loadData {
    
    self.currentLanguage = [AppSettingManager instance].deviceLanguage;
    if (!self.currentLanguage)
        self.currentLanguage = @"";
    
    NSBundle* bundle = [NSBundle mainBundle];
    NSString* plistPath = [bundle pathForResource:@"LanguageList" ofType:@"plist"];
    
    NSMutableArray* languageArray = [NSMutableArray arrayWithArray:[NSArray arrayWithContentsOfFile:plistPath]];
    [languageArray insertObject:@{@"Key": @"", @"Title": NSLocalizedString(@"SELECT_SYSTEM_LANGUAGE", nil)}
                        atIndex:0];
    
    _languageSelector.titleForNullValue = NSLocalizedString(@"SELECT_SYSTEM_LANGUAGE", nil);
    [_languageSelector initializeControl:@"Title"
                       listDisplayMember:@"Title"
                              dataMember:@"Key"
                              dataSource:languageArray
                            currentValue:currentLanguage
                                   title:NSLocalizedString(@"LANGUAGE_TITLE", nil)];
}

-(NSString*)title {
    if (IS_IPHONE_6_OR_MORE)
        return NSLocalizedString(@"SELECT_LANGUAGE_TITLE", nil);
    else
        return [super title];
}

-(BOOL)willSave {
    
    NSString *language = (NSString *)_languageSelector.value;
    
    if ([language isEqualToString:currentLanguage]) {
        
        RTAlertView *alert = [[RTAlertView alloc] initWithTitle:NSLocalizedString(@"SELECT_LANGUAGE_ERROR", nil)
                                                        message:nil
                                                       delegate:nil
                                              cancelButtonTitle:NSLocalizedString(@"OKBTN_TITLE", @"Ok")
                                              otherButtonTitles:nil];
        [alert setCompletedBlock:^(NSInteger buttonIndex) {
            [self close];
        }];
        
        [alert show];
        [alert release];
        return NO;
    }
    
    
    self.dialogViewController.bCancel.enabled = NO;
    
    if (language.length == 0)
        language = nil;
    
    [AppSettingManager instance].deviceLanguage = language;
    if (language) 
        [[NSUserDefaults standardUserDefaults] setObject:@[language] forKey:@"AppleLanguages"];
    else
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"AppleLanguages"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    
    RTAlertView *alert = [[RTAlertView alloc] initWithTitle:NSLocalizedString(@"SELECT_LANGUAGE_MSG", @"Ok")
                                                    message:nil
                                                   delegate:nil
                                          cancelButtonTitle:NSLocalizedString(@"OKBTN_TITLE", @"Ok")
                                          otherButtonTitles:nil];
    [alert setCompletedBlock:^(NSInteger buttonIndex) {
       
        [self close];
    }];
    
    [alert show];
    [alert release];
    
    return NO;
}

@end
