//
//  MessageMismatchController.m
//  ProductBuilder
//
//  Created by Roman on 4/9/12.
//  Copyright (c) 2012 Cloudworks. All rights reserved.
//

#import "MessageMismatchController.h"

@implementation MessageMismatchController

@synthesize locationInfoView, locationInitLabel, locationSyncLabel, locationInitTitle, locationSyncTitle;

- (id)init
{
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad) {
        self = [super initWithNibName:@"MessageMismatchView" bundle:nil];
    }
    else {
        self = [super initWithNibName:@"MessageMismatchView_iPhone" bundle:nil];
    }
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)dealloc {
    [locationInitLabel release];
    [locationSyncLabel release];
    [locationInitTitle release];
    [locationSyncTitle release];
    [locationInfoView release];
    [super dealloc];
}

#pragma mark - View lifecycle

-(void)adjustSize{
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad) {
        self.titleLabel.frame = self.titleView.bounds;
        CGFloat oldTitleLabelHeight = self.titleLabel.bounds.size.height;
        [self.titleLabel sizeToFit];
        CGFloat deltaTitle = self.titleLabel.bounds.size.height - oldTitleLabelHeight;
        self.titleView.frame = CGRectMake(self.titleView.frame.origin.x, self.titleView.frame.origin.y,
                                          self.titleView.frame.size.width, self.titleView.bounds.size.height + deltaTitle);
        self.titleLabel.frame = self.titleView.bounds;
        
        locationInfoView.frame = CGRectMake(locationInfoView.frame.origin.x, locationInfoView.frame.origin.y + deltaTitle,
                                            locationInfoView.bounds.size.width, locationInfoView.bounds.size.height);
        
        //self.detailsLabel.frame = self.detailsView.bounds;
        CGFloat oldDetailsLabelHeight = self.detailsLabel.bounds.size.height;
        [self.detailsLabel sizeToFit];
        CGFloat deltaDetails = self.detailsLabel.bounds.size.height - oldDetailsLabelHeight;
        
        self.detailsView.frame = CGRectMake(self.detailsView.frame.origin.x, self.detailsView.frame.origin.y + deltaTitle,
                                            self.detailsView.bounds.size.width, self.detailsView.bounds.size.height + deltaDetails);
        //self.detailsLabel.frame = self.detailsView.bounds;
        //bClose.frame = CGRectMake(bClose.frame.origin.x, bClose.frame.origin.y + deltaTitle + deltaDetails,
        //                          bClose.bounds.size.width, bClose.bounds.size.height);
        self.view.bounds = CGRectMake(0, 0, self.view.frame.size.width, self.view.bounds.size.height + deltaTitle + deltaDetails);
    }
}


@end
