//
//  StatusViewController.m
//  ProductBuilder
//
//  Created by Lulakov Viacheslav on 9/28/10.
//  Copyright 2010 DBBestTechnologies. All rights reserved.
//

#import "StatusViewController.h"
#import "AppSettingManager.h"
#import "Settings.h"
#import "SettingManager.h"
#import "SyncOperationQueue.h"

static StatusViewController *statusViewController;

@interface StatusViewController(){
}

@end

@implementation StatusViewController

@synthesize associateButton;
@synthesize storeNameButton;
@synthesize buildButton;
@synthesize synchronizedButton;
@synthesize logo;
@synthesize deviceIdButton;
@synthesize syncDate;

- (void)viewDidLoad {
    [super viewDidLoad];
    
//    self.view.layer.masksToBounds = NO;
//    if (!IS_IPHONE_6_OR_MORE) {
//        self.view.layer.shadowOffset = CGSizeMake(0, -3);
//        self.view.layer.shadowRadius = 2.0;
//        self.view.layer.shadowOpacity = 0.5;
//    }
	
	self.associate = nil;
	self.storeName = nil;
    
    [self updateDeviceIdTitle];

    NSString *appVersionNumber = [[NSBundle mainBundle] infoDictionary][@"CFBundleVersion"];
    
    [buildButton setTitle:[NSString stringWithFormat:NSLocalizedString(@"STATUS_BUILD", nil), appVersionNumber]
                 forState:UIControlStateNormal];
    //do not acces the database at this moment
    //[self setStoreName:[Workstation currentWorkstation].name];
    
    //[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(statusNotification:) name:StatusViewConnectionStateChanged object:nil];

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(syncQueueDidComplete:)
                                                 name:SyncQueueDidCompleteNotification
                                               object:[SyncOperationQueue sharedInstance]];
    updateLastSyncTimer = [[NSTimer scheduledTimerWithTimeInterval:60 target:self selector:@selector(updateLastSyncTimeDescription) userInfo:nil repeats:TRUE] retain];

    deviceIdButton.titleLabel.adjustsFontSizeToFitWidth = YES;
    deviceIdButton.titleLabel.minimumScaleFactor = 0.3;
}

-(void)updateDeviceIdTitle {
    
    if ([AppSettingManager instance].iPadId)
        [deviceIdButton setTitle:[NSString stringWithFormat:NSLocalizedString(@"STATUS_ID", nil), [AppSettingManager instance].iPadId]
                    forState:UIControlStateNormal];
    else
        [deviceIdButton setTitle:@""
                        forState:UIControlStateNormal];
        
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    if ([SettingManager instance].isDemoMode){
        
        [logo setImage:nil forState:UIControlStateNormal];
        [logo setTitle:nil forState:UIControlStateNormal];
    }

    [self updateDeviceIdTitle];
}


#pragma mark -

-(void)statusNotification:(NSNotification *)notification {
    //NSNumber* status = [notification object];
    //[self setStatus:[status boolValue]];
}


//- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
//    // Overriden to allow any orientation.
//    return YES;
//}


- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

- (void)dealloc {
    [updateLastSyncTimer invalidate];
    [updateLastSyncTimer release];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [associateButton release];
	[storeNameButton release];
	[buildButton release];
    [synchronizedButton release];
    [logo release];
    [deviceIdButton release];
    [timer release];
    [syncDate release];
    [_lastSyncTime release];
    [super dealloc];
}

+(StatusViewController *)sharedInstance {
	if (statusViewController == nil) {
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
            if (IS_IPHONE_6P || IS_IPHONE_6)
                statusViewController = [[StatusViewController alloc] initWithNibName:@"StatusView_iPhone@3x" bundle:[NSBundle mainBundle]];
            else
                statusViewController = [[StatusViewController alloc] initWithNibName:@"StatusView_iPhone" bundle:[NSBundle mainBundle]];
        } else {
            statusViewController = [[StatusViewController alloc] initWithNibName:@"StatusView" bundle:[NSBundle mainBundle]];
        }
        
    }
	return statusViewController;
}

#pragma mark -
#pragma mark Properties

-(void)setAssociate:(NSString *)value {
	[associateButton setTitle:value forState:UIControlStateNormal];
}

-(NSString *)associate {
	return associateButton.titleLabel.text;
}

-(void)setStoreName:(NSString *)value {
	[storeNameButton setTitle:value forState:UIControlStateNormal];
}

-(NSString *)storeName {
	return storeNameButton.titleLabel.text;
}

-(void)setSyncTime:(NSDate*)value{
    
    if ([NSThread isMainThread]){
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT,0), ^{
            
            [self setSyncTime:value];
        });
        return;
    }
    
    
    @synchronized (self) {
    
        if (self.lastSyncTime && [self.lastSyncTime isEqualToDate:value]) {
            
            return;
        }
        
        self.lastSyncTime = value;
    }
    
    
    dispatch_async(dispatch_get_main_queue(),^{
        
        [self updateLastSyncTimeDescription];
    });
}


-(void)updateLastSyncTimeDescription{
    
    CGFloat timeInterval = 0;
    
    @synchronized(self) {
        
        timeInterval = [[NSDate date]timeIntervalSinceDate:self.lastSyncTime];
    }
    
    NSString* description = nil;

    if (isnan(timeInterval))
        description = NSLocalizedString(@"SYNC_STATUS_UNKNOWN", @"Unknown");
    else if (timeInterval < 60)
        description = NSLocalizedString(@"SYNC_STATUS_LESS_THEN1", @"Less than 1 minute ago");
    else{
    
        if (timeInterval >=60 && timeInterval<60*120)
            description = [NSString stringWithFormat:NSLocalizedString(@"SYNC_STATUS_MIN_AGO", @"%i minutes ago"), (NSInteger)timeInterval/60];
        else if (timeInterval>60*120 && timeInterval<60*60*24)
            description = [NSString stringWithFormat:NSLocalizedString(@"SYNC_STATUS_HOURS_AGO", @"%i hours ago"), (NSInteger)timeInterval/60/60];
        else
            description = [NSString stringWithFormat:NSLocalizedString(@"SYNC_STATUS_DAYS_AGO", @"%i days ago"), (NSInteger)timeInterval/60/60/24];
    }
    
    [synchronizedButton setTitle:[NSString stringWithFormat:NSLocalizedString(@"SYNC_STATUS_LAST_SYNC", @"Last Sync: %@"), description] forState:UIControlStateNormal];
}


- (void) syncQueueDidComplete:(NSNotification *) notification{
    SyncOperationQueue* queue = [notification object];
    if (!queue) return;
    [self setSyncTime:[NSDate date]];
}

@end
