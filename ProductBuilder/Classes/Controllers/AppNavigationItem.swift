//
//  AppNavigationItem.swift
//  ProductBuilder
//
//  Created by valery on 12/23/16.
//  Copyright © 2016 Cloudworks. All rights reserved.
//

import Foundation

class AppNavigationBar : UINavigationBar{
    
}

class AppNavigationItem : UINavigationItem{
    
    var _leftBarButtonItems: [UIBarButtonItem]?
    var _rightBarButtonItems: [UIBarButtonItem]?
    
    public override init(title: String){
        super.init(title: title)
        self.setup()
    }
    
    public required init?(coder: NSCoder){
        super.init(coder: coder)
        self.setup()
    }

    func setup(){
        let firstView = Bundle.main.loadNibNamed("LaunchScreen", owner: nil, options: nil)?[0] as? UIView
        if (firstView != nil && firstView!.subviews.count > 0 && firstView!.subviews[0].subviews.count > 0){
            let logoView = firstView!.subviews[0].subviews[0]
            self.titleView = logoView;
            logoView.translatesAutoresizingMaskIntoConstraints = true
        }
    }
    
    override func setLeftBarButton(_ item: UIBarButtonItem?, animated: Bool) {
        if (self.isLeftBarButtonItemsHidden){
            _leftBarButtonItems = item != nil ? [item!] : [UIBarButtonItem]()
        }
        else{
            super.setLeftBarButton(item, animated: animated)
        }
    }
    
    override func setLeftBarButtonItems(_ items: [UIBarButtonItem]?, animated: Bool) {
        if (self.isLeftBarButtonItemsHidden){
            _leftBarButtonItems = items ?? [UIBarButtonItem]()
        }
        else{
            super.setLeftBarButtonItems(items, animated: animated)
        }
    }
    
    override func setRightBarButton(_ item: UIBarButtonItem?, animated: Bool) {
        if (self.isRightBarButtonItemsHidden){
            _rightBarButtonItems = item != nil ? [item!] : [UIBarButtonItem]()
        }
        else{
            super.setRightBarButton(item, animated: animated)
        }
    }
    
    override func setRightBarButtonItems(_ items: [UIBarButtonItem]?, animated: Bool) {
        if (self.isRightBarButtonItemsHidden){
            _rightBarButtonItems = items ?? [UIBarButtonItem]()
        }
        else {
            super.setRightBarButtonItems(items, animated: animated)
        }
    }
    
    var isLeftBarButtonItemsHidden:Bool{
        get{
            return self.leftBarButtonItems == nil && _leftBarButtonItems != nil
        }
        set{
            if ((newValue && self.isLeftBarButtonItemsHidden) || (!newValue && !self.isLeftBarButtonItemsHidden)){
                return;
            }
            if (newValue){
                let leftBarButtonItemsTmp =  self.leftBarButtonItems ?? [UIBarButtonItem]()
                self.leftBarButtonItems = nil
                self.leftBarButtonItem = nil
                _leftBarButtonItems = leftBarButtonItemsTmp
            }
            else{
                if (_leftBarButtonItems != nil){
                    let leftBarButtonItemsTmp = _leftBarButtonItems
                    _leftBarButtonItems = nil
                    if (leftBarButtonItemsTmp!.count == 1){
                        self.setLeftBarButton(leftBarButtonItemsTmp!.first, animated: true)
                    }
                    else{
                        self.setLeftBarButtonItems(leftBarButtonItemsTmp, animated: true)
                    }
                }
            }
        }
    }
    
    var isRightBarButtonItemsHidden:Bool{
        get{
            return self.rightBarButtonItems == nil && _rightBarButtonItems != nil
        }
        set{
            if ((newValue && self.isRightBarButtonItemsHidden) || (!newValue && !self.isRightBarButtonItemsHidden)){
                return;
            }
            if (newValue){
                let rightBarButtonItemsTmp = self.rightBarButtonItems ?? [UIBarButtonItem]()
                self.rightBarButtonItems = nil
                self.rightBarButtonItem = nil
                _rightBarButtonItems = rightBarButtonItemsTmp
            }
            else{
                if (_rightBarButtonItems != nil){
                    let rightBarButtonItemsTmp = _rightBarButtonItems
                    _rightBarButtonItems = nil
                    if (rightBarButtonItemsTmp!.count == 1){
                        self.setRightBarButton(rightBarButtonItemsTmp!.first, animated: true)
                    }
                    else{
                        self.setRightBarButtonItems(rightBarButtonItemsTmp, animated: true)
                    }
                }
            }
        }
    }

}
