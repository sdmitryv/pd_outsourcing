//
//  RTDialogView.swift
//  CloudworksPOS
//
//  Created by dsm on 12/16/16.
//  Copyright © 2016 Cloudworks. All rights reserved.
//


enum RTDialogResult50: Int {
    case cancel = 0
    case ok = 1
}


class RTDialogContentViewController50: UIViewControllerExtended{
    
    var dialogViewController:RTDialogViewController50?
    var validate : ((Void)->Bool)?
    
    
    func dilogTitle() -> String? {
        return nil;
    }

    func selectedValue() -> AnyObject?{
        return nil;
    }

    func setSelectedValue(_ value:AnyObject?){
    
    }

    func selectButtonTitle() -> String{
        return NSLocalizedString("SAVEBTN_TITLE", comment: "")
    }

    func cancelButtonTitle() -> String{
        return NSLocalizedString("CANCELBTN_TITLE", comment: "")
    }

   func willSave() -> Bool{
    
        if self.validate != nil {
            
            if !self.validate!() {
                
                setSelectedValue(nil)
                return false;
            }
        }
        return true;
    }

    func willCancel() -> Bool{
        return true
    }

    func willClose(){
    
    }

    func didClose(){
        
        self.validate = nil;
    }

    func close(){
        self.dialogViewController?.cancel()
    }

    func showModal(_ aCompletion:((Void)->Void)?){
        RTDialogViewController50.showModal(self, completion: aCompletion)
    }
}



class RTDialogViewController50 : UIViewControllerExtended {
    
     private(set) var contentController: RTDialogContentViewController50;
     private(set) var saving: Bool = false
    
     @IBOutlet var vTitle: UILabel?
     @IBOutlet var vContentView: UIView?
     @IBOutlet var backView: UIRoundedCornersView?
     @IBOutlet var bCancel: UIButton?
     @IBOutlet var bSave: UIButton?
     var dialogResult: RTDialogResult50 = RTDialogResult50.cancel
     var ignoredAlertViewOnSave: NSObject? = nil
     var cancelling: Bool = false
    
    required init(withContentViewController aContentController:RTDialogContentViewController50){
        
        self.contentController = aContentController
        
        super.init(nibName: "RTDialogView50", bundle: Bundle.main)
        

        self.contentController.dialogViewController = self
       
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    static func showModal(_ contentController:RTDialogContentViewController50, completion aCompletion:((Void)->Void)?){
        
        let dialogView = RTDialogViewController50(withContentViewController: contentController)
        dialogView.showModal(true, completion: aCompletion)
    }
    
    static func showModal(_ contentController:RTDialogContentViewController50, delegate aDelegate:UIModalViewControllerDelegate) {
        
        let dialogView = RTDialogViewController50(withContentViewController: contentController)
        dialogView.showModal(true, delegate: aDelegate)
    }

    
    override func viewDidLoad() {
        
        super.viewDidLoad()

        vTitle?.text = contentController.dilogTitle()
        bSave?.setTitle(contentController.selectButtonTitle(), for: UIControlState.normal)
        bCancel?.setTitle(contentController.cancelButtonTitle(), for: UIControlState.normal)
        
        if contentController.view == nil {
            
            return
        }
        let originalFrame =  self.vContentView?.bounds.size
        let newFrame = contentController.view.frame.size
        let widthD = (originalFrame?.width)! - newFrame.width
        let heightD  = (originalFrame?.height)! - newFrame.height
        var originalBounds = self.view.bounds;
        originalBounds.size.width-=widthD;
        originalBounds.size.height-=heightD;
        self.view.bounds = originalBounds
        contentController.view.autoresizingMask = [UIViewAutoresizing.flexibleWidth, UIViewAutoresizing.flexibleHeight]
        contentController.view.frame = contentController.view.bounds
        vContentView?.addSubview(contentController.view)
        self.addChildViewController(contentController)
        
        backView?.setRoundedCorners([UIViewRoundedCornerMask.lowerLeft, UIViewRoundedCornerMask.lowerRight], radius: (backView?.radius)!)
    }
    
    @IBAction func saveClick() {
        self.save()
    }
    
    func save(){
    
        saving = true
    
        if let responder = self.view.getFirstResponder() {
            
            if responder.canResignFirstResponder{
    
                responder.resignFirstResponder()
            }
            else {
                
                saving = false;
                return;
            }
        }
        
        let topAlertView = RTAlertView.topMostAlertView()
        if (topAlertView != nil && topAlertView != self.ignoredAlertViewOnSave) {
           
            self.saving = false
            return
        }
    
        if !contentController.willSave() {
            
            self.saving = false
            return
        }
        
        self.dialogResult = RTDialogResult50.ok
        self.close()
        saving = false
    }
    
    @IBAction func cancelClick() {
        self.cancel()
    }

    
    func cancel(){
    
        self.cancelling = true
        if let responder = self.view.getFirstResponder() {
            
            responder.resignFirstResponder()
        }
        
        if !contentController.willCancel() {
            return
        }
        
        self.dialogResult = RTDialogResult50.cancel
        self.close()
        cancelling = false
    }
    
    func close(){
        
        contentController.willClose()
        self.dismiss(animated: true, completion: { self.contentController.didClose() })
    }
    
    func setSaveHidden(_ value:Bool) {
        
        if self.view == nil {
            
            return
        }
        
        if bSave?.isHidden == value {
            return
        }
        bSave?.isHidden = value
    }
    
    func setCancelHidden(_ value:Bool) {
        
        if self.view == nil {
            
            return
        }
        
        if bCancel?.isHidden == value {
            return
        }
        bCancel?.isHidden = value
    }
}
