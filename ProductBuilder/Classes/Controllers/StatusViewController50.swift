//
//  StatusViewController50.swift
//  ProductBuilder
//
//  Created by valery on 12/16/16.
//  Copyright © 2016 Cloudworks. All rights reserved.
//

import Foundation

class StatusViewController50 : UIViewController{
    
    @IBOutlet var associateLabel: UILabel?
    @IBOutlet var storeNameLabel: UILabel?
    @IBOutlet var buildLabel: UILabel?
    @IBOutlet var deviceIdLabel: UILabel?
    @IBOutlet var serverCodeLabel: UILabel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        updateDeviceIdTitle()
        
    }
    
    func updateDeviceIdTitle(){
        deviceIdLabel?.text = AppSettingManager.instance().iPadId != nil ? String(format: NSLocalizedString("%@", comment: ""), AppSettingManager.instance().iPadId) : ""
        buildLabel?.text = String(format: NSLocalizedString("v%@", comment: ""), Bundle.main.infoDictionary!["CFBundleVersion"] as! CVarArg)
        updateServerCode()
    }
    
    func updateServerCode(){
        serverCodeLabel?.text = AppSettingManager.instance().defaultServerCode ?? AppSettingManager.instance().serverUrl
    }
    
    var storeName : String?{
        get{
            return storeNameLabel?.text
        }
        set{
            storeNameLabel?.text = newValue
        }
    }
    
    var associateName : String?{
        get{
            return associateLabel?.text
        }
        set{
            associateLabel?.text = newValue
        }
    }
    
    var lastSyncTime: Date?
    
    static func sharedInstance()->StatusViewController50?{
        return SuperViewController50.instance()?.statusViewController
    }
}
