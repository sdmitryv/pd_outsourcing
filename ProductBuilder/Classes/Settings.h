//
//  Settings.h
//  ProductBuilder
//
//  Created by Lulakov Viacheslav on 9/30/10.
//  Copyright 2010 DBBestTechnologies. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BPUUID.h"

@class Device;

@interface Settings : NSObject {
@private
	
	// Synchnization preferences
    NSString	*hqAddress;
//    BOOL        useDemoMode;
}

@property (nonatomic, readonly)     NSString    *hqAddress;
@property (atomic, readonly)        NSString    *appURLScheme;

+(Settings *)sharedInstance;
+(void)reset;

@end
