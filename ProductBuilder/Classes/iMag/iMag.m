//
//  iMag.m
//  IDTech
//
//  Created by Randy Palermo on 5/4/10.
//  Copyright 2010 ID_Tech. All rights reserved.
//

#import "iMag.h"
#import "UIView+FirstResponder.h"
#import "UITextField+Input.h"
#import "UITextFieldNumeric.h"


NSString* const iMagDidReceiveDataNotification = @"iMagDidReceiveDataNotification";
NSString* const iMagDidConnectNotification = @"iMagDidConnectNotification";
NSString* const iMagDidDisconnectNotification = @"iMagDidDisconnectNotification";
NSString* const kReaderIdTech = @"com.idtechproducts.reader";
NSString* const kReaderMagteckDynamo = @"com.magtek.idynamo";
NSString* const kReaderMW = @"com.merchantwarehouse.merchantware";
NSString* const kBTMagDevice = @"com.idtechproducts.BTMag";
NSString* const GerTechBTMOBIPIN = @"br.com.gertec.protocoloGertec";

@implementation RTSwipeReaderDescription
@synthesize protocolString, type;
-(void)dealloc{
    [protocolString release];
    [super dealloc];
}

-(id)initWithProtocolString:(NSString*)aProtocolString type:(RTSwipeReaderType)aType{
    if ((self=[super init])){
        protocolString = [aProtocolString retain];
        type = aType;
    }
    return self;
}
               
+(RTSwipeReaderDescription*)swipeReaderDescription:(NSString*)aProtocolString type:(RTSwipeReaderType)aType{
    return [[[[self class]alloc]initWithProtocolString:aProtocolString type:aType]autorelease];
}

@end

@implementation RTSwipeReaderScan

@synthesize data, readerType, ksn, encryptedData;

-(id)initWithData:(NSData*)aData type:(RTSwipeReaderType)aType ksn:(NSString*)aKsn encTracks:(NSString*)encTracks{
    if ((self=[super init])){
        data = [aData retain];
        readerType = aType;
        ksn = [aKsn retain];
        encryptedData = [encTracks retain];
    }
    return self;
}


-(id)initWithData:(NSData*)aData type:(RTSwipeReaderType)aType{
    if ((self=[super init])){
        data = [aData retain];
        readerType = aType;
    }
    return self;
}

+(RTSwipeReaderScan*)swipeReaderScanWithData:(NSData*)aData type:(RTSwipeReaderType)aType{
    return [[[[self class]alloc]initWithData:aData type:aType]autorelease];
}

+(RTSwipeReaderScan*)encryptedSwipeWithTracks:(NSString*)encTracks ksn:(NSString*)ksn maskedTrack1:(NSData*)maskedtrack1 type:(RTSwipeReaderType)aType {
    return [[[[self class]alloc]initWithData:maskedtrack1 type:aType ksn:ksn encTracks:encTracks]autorelease];
}

-(void)dealloc{
    [ksn release];
    [encryptedData release];
    [data release];
    [super dealloc];
}

@end

@implementation iMag

@synthesize currentReaderDescription;

+(iMag *)sharedInstance {
    static dispatch_once_t pred;
    static iMag *instance = nil;
    
    dispatch_once(&pred, ^{
        instance = [[iMag alloc] init];
    });
    return instance;
}

NSArray* validReaders;

-(NSArray*)validReaders{
    if (!validReaders){
        validReaders = [@[[ RTSwipeReaderDescription swipeReaderDescription:kReaderIdTech type:RTSwipeReaderTypeIdTech], 
                                                  [RTSwipeReaderDescription swipeReaderDescription:kReaderMagteckDynamo type:RTSwipeReaderiDynamo],
                                                    [RTSwipeReaderDescription swipeReaderDescription:kReaderMW type:RTSwipeReaderiDynamo],
                                                    [RTSwipeReaderDescription swipeReaderDescription:kBTMagDevice type:RTSwipeReaderBTMagReader]]retain];
    }
    return validReaders;
}


// private routines

-(void)shutdownSession{
    if (session){
        session.accessory.delegate = nil;
        [session.outputStream close];
        [session.outputStream removeFromRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
        session.outputStream.delegate = nil;
        
        [session.inputStream close];
        [session.inputStream removeFromRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
        session.inputStream.delegate = nil;
        [session release];
        session = nil;
    }
    waitForMoreData = FALSE;
    [data release];
    data = nil;
}

-(BOOL)checkAccessoryConnected:(EAAccessory*) acc{
    if (!acc || !acc.connected) return FALSE;
    for(RTSwipeReaderDescription* readerDescription in [self validReaders]){
        if ([acc.protocolStrings indexOfObject:readerDescription.protocolString] != NSNotFound){
            [currentReaderDescription release];
            currentReaderDescription = [readerDescription retain];
            break;
        }
    }
    if (currentReaderDescription) {
        acc.delegate = self;
        _connected = TRUE;
        [self shutdownSession];
        type = currentReaderDescription.type;
        session = [[EASession alloc] initWithAccessory:acc forProtocol:currentReaderDescription.protocolString];
        [session.inputStream setDelegate:self];
        [session.inputStream scheduleInRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
        [session.inputStream open];
        [session.outputStream setDelegate:self];
        [session.outputStream scheduleInRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
        [session.outputStream open];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:iMagDidConnectNotification object:currentReaderDescription];
        return TRUE;
    }
    return FALSE;
}

- (void)accessoryConnected:(NSNotification *)notification {
    @synchronized(self) {
        EAAccessory* acc2 = [notification userInfo][EAAccessoryKey];
        [self checkAccessoryConnected:acc2];
    }
}

- (void)accessoryDisconnected:(NSNotification *)notification {
    @synchronized(self) {
        if (_connected){
            EAAccessory* accessory = [notification userInfo][EAAccessoryKey];
            if (accessory && accessory == session.accessory){
                _connected = FALSE;
                [self shutdownSession];
                [[NSNotificationCenter defaultCenter] postNotificationName:iMagDidDisconnectNotification object:currentReaderDescription];
                [currentReaderDescription release];
                currentReaderDescription = nil;
            }
        }
    }
}


- (void)accessoryDidDisconnect:(EAAccessory *)accessory{
    @synchronized(self) {
        if (_connected){
            if (accessory && accessory == session.accessory){
                _connected = FALSE;
                [self shutdownSession];
                [[NSNotificationCenter defaultCenter] postNotificationName:iMagDidDisconnectNotification object:currentReaderDescription];
                [currentReaderDescription release];
                currentReaderDescription = nil;
            }
        }
    }
}

///////////

-(id)init
{
    if ((self = [super init]))
    {
        _connected = FALSE;

		[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(accessoryConnected:) name:EAAccessoryDidConnectNotification object:nil];
		[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(accessoryDisconnected:) name:EAAccessoryDidDisconnectNotification object:nil];
        
        EAAccessoryManager *p = [EAAccessoryManager sharedAccessoryManager];
		[p registerForLocalNotifications];
		
		for (EAAccessory *acc in [p connectedAccessories]){
			if ([self checkAccessoryConnected:acc]) break;
		}
    }
    return self;
}

-(void)dealloc{
    [self shutdownSession];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    EAAccessoryManager *p = [EAAccessoryManager sharedAccessoryManager];
    [p unregisterForLocalNotifications];
    for (EAAccessory *acc in [p connectedAccessories]){
        if (acc.delegate==self) acc.delegate = nil;
    }
    [currentReaderDescription release];
    [super dealloc];
}

//RTSwipeReaderDescription* fakeCurrentReaderDescription;
//-(RTSwipeReaderDescription*) currentReaderDescription{
//    if (!fakeCurrentReaderDescription){
//        fakeCurrentReaderDescription = [[RTSwipeReaderDescription alloc]initWithProtocolString:@"" type:RTSwipeReaderiDynamo];
//    }
//    return fakeCurrentReaderDescription;
//}


- (void) sendCommand: (NSString*)cmd{
	NSLog(@"Sending Command: %@",cmd );
	const uint8_t *rawString=(const uint8_t *)[cmd UTF8String];
	[session.outputStream  write:rawString maxLength:[cmd length]];	
}



- (void)stream:(NSStream*)theStream handleEvent:(NSStreamEvent)streamEvent  
{  
	switch (streamEvent)  
	{  
		case NSStreamEventNone:  //Sent when open complete
			//NSLog(@"NSStreamEventNone");
			break;			
		case NSStreamEventOpenCompleted:  //Sent when open complete
			//NSLog(@"NStreamEventOpenCompleted: %@", theStream);
			break ;
			
		case NSStreamEventHasBytesAvailable:
		{
			uint8_t buffer[1024];
            memset(buffer, 0, sizeof(buffer));
			NSInteger len=0;
			len=[(NSInputStream *)theStream  read:buffer maxLength:1024];
            if (type == RTSwipeReaderTypeIdTech){
                if(len>0){      
                    NSData* d=[NSData dataWithBytes:buffer length:len];
                    [[NSNotificationCenter defaultCenter] postNotificationName:iMagDidReceiveDataNotification object:[RTSwipeReaderScan swipeReaderScanWithData:d type:type]];
                }
            }
            else if (type == RTSwipeReaderiDynamo || type == RTSwipeReaderBTMagReader){

                BOOL sendNotification = FALSE;
                if (waitForMoreData)
                {
                    // Wait for more data already set, second batch of data arrived
                    if (len){
                        [data appendBytes:buffer length:len];
                    }
                    waitForMoreData = NO;
                    sendNotification = TRUE;
                }
                else if (len)
                {
                    [data release];
                    data = [[NSMutableData alloc]init];
                    [data appendBytes:buffer length:len];
                    if (buffer[len-1] != 'x')
                    {
                        //More data on the way, set flag to wait for it
                        waitForMoreData = YES;
                    }
                    else
                    {
                        sendNotification = TRUE;
                    }
                }
                if (sendNotification){
                    if (type == RTSwipeReaderBTMagReader) {
                        NSString* tracks = [self getTrackDataFromCardData:data];
//                        NSLog(tracks, nil);
                        if (tracks.length <= 3) return;
                        
                        //validate end sentinel as '?'
                        NSRange quastionSentinel = [tracks rangeOfString:@"?"];
                        
                        if (quastionSentinel.length) {
                            tracks = [tracks substringToIndex:quastionSentinel.location];
                        }
                        
                        
                        NSRange startSentinel = [tracks rangeOfString:@";"];
                        NSRange endSentinel = [tracks rangeOfString:@"="];
                        
                        NSString* number = nil;
                        
                        if (startSentinel.length == 0 && endSentinel.length == 0) {
                            number = tracks;
                        }
                        else if (startSentinel.length != 0 && endSentinel.length != 0) {
                            number = [tracks substringWithRange:NSMakeRange(startSentinel.location + 1, endSentinel.location - startSentinel.location - 1)];
                        }
                        else if (startSentinel.length != 0 && endSentinel.length == 0) {
                            number = [tracks substringFromIndex:startSentinel.location + 1];
                        }
                        else {
                            number = [tracks substringToIndex:endSentinel.location];
                        }
                        
                        NSMutableString *asciiCharacters = [NSMutableString string];
                        for (int i = 32; i < 127; i++)  {
                            [asciiCharacters appendFormat:@"%c", i];
                        }
                        
                        NSCharacterSet *nonAsciiCharacterSet = [[NSCharacterSet characterSetWithCharactersInString:asciiCharacters] invertedSet];
                        
                        number = [[number componentsSeparatedByCharactersInSet:nonAsciiCharacterSet] componentsJoinedByString:@""];
                        
                        if ([number hasPrefix:@"EH"] || [number hasPrefix:@"EI"] || [number hasPrefix:@"EJ"]) {
                            number = [number substringFromIndex:2];
                        }
                        
                        if ([number hasSuffix:@"EH"] || [number hasSuffix:@"EI"] || [number hasSuffix:@"EJ"]) {
                            number = [number substringToIndex:number.length - 2];
                        }
                        
                        if (number.length > 0) {
                            [self postResult:number];
                        }
                        
                        [data release];
                        data = nil;
                    }
                    else {
                        [[NSNotificationCenter defaultCenter] postNotificationName:iMagDidReceiveDataNotification object:[RTSwipeReaderScan swipeReaderScanWithData:data type:type]];
                        [data release];
                        data = nil;
                    }
                }
            }
        }
		break;

        case NSStreamEventHasSpaceAvailable:
			break;
			
		case NSStreamEventErrorOccurred:
		case NSStreamEventEndEncountered:
            [data release];
            data = nil;
            waitForMoreData = FALSE;
			break;			
			
        default:  
			break;  
	}  
} 



-(BOOL) iMagConnected{
	return _connected;
}

-(NSString*)getTrackDataFromCardData:(NSData*)adata {
    // Handle card data received to post it to first resonder.
    Byte * temp=(Byte*)[adata bytes];
    
    if(temp[0]==0x01&&temp[1]==0x01&&temp[2]==0x1a&&temp[3]==0x02&&temp[4]==0x00&&data.length==285)
    {
        return adata.description;
    }
    else
    {
        return [[[NSString alloc] initWithBytes:temp length:adata.length  encoding:NSASCIIStringEncoding] autorelease];
    }
}

-(void)postResult:(NSString*)adata {
    UIWindow *mWindow = [UIApplication sharedApplication].windows[0];
    
    id currentResponder = [mWindow getFirstResponder];
    if (!currentResponder)
        return;
    
    if ([currentResponder isKindOfClass:[UISearchBar class]]) {
        UISearchBar *searchBar = (UISearchBar *)currentResponder;
        searchBar.text = adata;
        if (searchBar.delegate && [searchBar.delegate respondsToSelector:@selector(searchBarSearchButtonClicked:)]) {
            [searchBar.delegate searchBarSearchButtonClicked:searchBar];
        }
        return;
    }
    
    UITextField* textField = (UITextField*)currentResponder;
    
    if (adata.length > 0){
        
        NSString* text = adata;
        
        if ([currentResponder isKindOfClass:[UITextField class]]){
            
            id responderDelegate = nil;
            if ([currentResponder isKindOfClass:[UITextFieldNumeric class]])
                responderDelegate = ((UITextFieldNumeric*)currentResponder).internalDelegate;
            else
                responderDelegate = [textField delegate];
            
            if (responderDelegate
                && [responderDelegate respondsToSelector:@selector(textField:shouldChangeCharactersInRange:replacementString:)]){
                
                NSRange range = [self selectedRangeOfTextField:textField];
                
                if (![responderDelegate textField:textField shouldChangeCharactersInRange:range replacementString:text]) {
                    if (textField.delegate != nil
                        && [textField.delegate respondsToSelector:@selector(textFieldShouldReturn:)]
                        && [textField.delegate textFieldShouldReturn:textField] && textField.canResignFirstResponder ){
                        [textField resignFirstResponder];
                    }
                    return;
                }
            }
        }
        
        if ([currentResponder respondsToSelector:@selector(insertText:)])
            [currentResponder performSelector:@selector(insertText:) withObject:text];
        
        if (textField.delegate != nil
            && [textField.delegate respondsToSelector:@selector(textFieldShouldReturn:)]
            && [textField.delegate textFieldShouldReturn:textField] && textField.canResignFirstResponder ){
            [textField resignFirstResponder];
        }
    }
    
}

-(NSRange)selectedRangeOfTextField:(UITextField *)textField{
    UITextRange *selectedTextRange = textField.selectedTextRange;
    NSUInteger location = [textField offsetFromPosition:textField.beginningOfDocument
                                             toPosition:selectedTextRange.start];
    NSUInteger length = [textField offsetFromPosition:selectedTextRange.start
                                           toPosition:selectedTextRange.end];
    return NSMakeRange(location, length);
}



@end
