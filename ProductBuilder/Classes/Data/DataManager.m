//
//  DataManager.m
//  StockCount
//
//  Created by Office user on 7/23/10.
//  Copyright 2010 1. All rights reserved.
//

#import "DataManager.h"
#import "DataUtils.h"
#import "USAdditions.h"
#import "NSDate+ISO8601Parsing.h"
#import "SqliteFunctions.h"
#import <libkern/OSAtomic.h>
#include <sys/sysctl.h>
#include <sys/utsname.h>
#import "AppSettingManager.h"
#import "NSData+Ext.h"
#import <pthread.h>
#import "SyncOperationQueue.h"
#import "AppSetting.h"
#import "CEntity+Sqlite.h"

NSString* const DataManagerWillStartUpdateNotification = @"DataManagerWillStartUpdateNotification";
NSString* const DataManagerDidEndUpdateNotification = @"DataManagerDidEndUpdateNotification";
NSString* const DataManagerDatabaseIsReady = @"DataManagerDatabaseIsReadyNotification";

NSString* const DataManagerIndexesCheckingWillStartNotification = @"DataManagerIndexesCheckingWillStartNotification";
NSString* const DataManagerIndexesWillStartUpdateNotification   = @"DataManagerIndexesWillStartUpdateNotification";
NSString* const DataManagerIndexesWillEndUpdateNotification     = @"DataManagerIndexesWillEndUpdateNotification";
NSString* const DataManagerIndexesCheckingDidEndNotification    = @"DataManagerIndexesCheckingDidEndNotification";


typedef NS_ENUM(NSUInteger, SqlColumnType) {SqlColumnTypeDatetime = 0,SqlColumnTypeInt = 1, SqlColumnTypeBlob = 2, SqlColumnTypeReal = 3, SqlColumnTypeText = 4};

static CFMutableDictionaryRef table_info_dict = nil;

// 256-bits encryption key for database
const unsigned char encryptKey[32] = {
    0xB5, 0xE3, 0xD5, 0x41, 0x9C, 0xAC, 0xA3, 0x27,
    0xA4, 0xF8, 0x02, 0x2A, 0x66, 0xDE, 0xD3, 0xB5,
    0xB6, 0x55, 0xC8, 0xB5, 0x64, 0x83, 0x64, 0x81,
    0x17, 0x48, 0x9C, 0x6E, 0xB4, 0x3B, 0x6C, 0xC1
};

@implementation TableRowInfo
@synthesize columnName, columnType, notNull, pk, dfltValue;
-(void)dealloc{
    [columnName release];
    [columnType release];
    [dfltValue release];
    [super dealloc];
}

@end


@interface TableDetails(){
    BOOL statementsBuilt;
}

-(void)addTableRowsInfo:(TableRowInfo*)tableRowInfo;
@end

@implementation TableDetails

@synthesize name, tableRowsInfo = tableRowsInfoDict, tableRowsInfoArray, insertStmt, replaceStmt, tablePrimaryKeys;

-(id)initWithName:(NSString*)aName{
    if ((self=[super init])){
        self.name = aName;
        tableRowsInfoDict = [[NSMutableDictionary alloc]init];
        tableRowsInfoArray = [[NSMutableArray alloc]init];
        tablePrimaryKeys = [[NSMutableArray alloc]init];
    }
    return self;
}
-(void)dealloc{
    [name release];
    [replaceStmt release];
    [insertStmt release];
    [tableRowsInfoDict release];
    [tableRowsInfoArray release];
    [tablePrimaryKeys release];
    [super dealloc];
}

-(void)addTableRowsInfo:(TableRowInfo*)tableRowInfo{
    if (tableRowInfo.columnName){
        tableRowsInfoDict[tableRowInfo.columnName] = tableRowInfo;
    }
    [tableRowsInfoArray addObject:tableRowInfo];
    if( tableRowInfo.pk){
        [tablePrimaryKeys addObject:tableRowInfo];
    }
}

-(NSString*)insertStmt{
    [self buildStatements];
    return insertStmt;
}

-(NSString*)replaceStmt{
    [self buildStatements];
    return replaceStmt;
}

-(void)buildStatements{
    if (statementsBuilt || !self.tableRowsInfoArray.count) return;
    statementsBuilt = TRUE;
    NSMutableArray* paramsStrArr = [[NSMutableArray alloc]initWithCapacity:self.tableRowsInfoArray.count];
    NSMutableArray* columnsStrArr = [[NSMutableArray alloc]initWithCapacity:self.tableRowsInfoArray.count*4-1];
    
    for (NSUInteger columnNum = 0; columnNum<self.tableRowsInfoArray.count; columnNum++){
        TableRowInfo* tbInfoRow = (self.tableRowsInfoArray)[columnNum];
        [paramsStrArr addObject:@"?"];
        if (columnsStrArr.count){
            [columnsStrArr addObject:@","];
        }
        [columnsStrArr addObject:@"\""];
        [columnsStrArr addObject:tbInfoRow.columnName];
        [columnsStrArr addObject:@"\""];
    }
    if (columnsStrArr.count && paramsStrArr.count){
        NSString* stmt = [NSString stringWithFormat:@" into %@(%@) values(%@)", [DataManager quoteName:self.name], [columnsStrArr componentsJoinedByString:@""], [paramsStrArr componentsJoinedByString:@","]];
        replaceStmt = [[NSString stringWithFormat:@"replace %@", stmt] retain];
        insertStmt = [[NSString stringWithFormat:@"insert %@", stmt] retain];
    }
    [columnsStrArr release];
    [paramsStrArr release];
}

@end

@interface DataManagerTLSWrapper : NSObject
    @property (nonatomic, retain) DataManager* dataManager;
    @property (nonatomic, retain) EGODatabase* database;
@end

@implementation DataManagerTLSWrapper

-(void)dealloc{
    [_dataManager release];
    [_database release];
    [super dealloc];
}

@end

@interface DataManager(){
    //OSSpinLock spinlock;
    os_unfair_lock _lock;
    //NSString* _databaseId;
    NSMutableArray* _dbThreadReferences;
    pthread_key_t thread_db_key;
}
-(void)removeDbmTLSReference:(DataManagerTLSWrapper*)database;

@end


static void threadDidExit(void *value){
    if ([(id)value isKindOfClass:[DataManagerTLSWrapper class]]) {
        DataManagerTLSWrapper* dbmTLS = (DataManagerTLSWrapper*)value;
        [dbmTLS.dataManager removeDbmTLSReference:dbmTLS];
        [dbmTLS release];
    }
}

@implementation DataManager

- (id)init {
    
    if ((self = [super init])){
        
//        spinlock = OS_SPINLOCK_INIT;
        _lock = OS_UNFAIR_LOCK_INIT;
#if !(TARGET_IPHONE_SIMULATOR)
//        encrypted = YES;
#endif
        _dbThreadReferences = [[NSMutableArray alloc]init];
        [self checkAndCreateDatabase];
        useTempDB = NO;
        NSLog(@"Database path: %@", self.currentDatabasePath);
    }
    
	return self;
}

-(void)removeDbmTLSReference:(DataManagerTLSWrapper*)dbmTLS{
    @synchronized(_dbThreadReferences) {
        [_dbThreadReferences removeObject:dbmTLS];
    }
}


-(NSString *)currentDatabasePath {
    
    return useTempDB ? [DataManager pathForDatabaseTemp] : [DataManager pathForDatabase];
}


+(void)initDatabase:(EGODatabase*)db{
    //do not use self.currentDatabase
    [db execute:
     @"PRAGMA foreign_keys = 1;\n\
     PRAGMA secure_delete = 0;\n\
PRAGMA recursive_triggers = 1;\n\
PRAGMA case_sensitive_like = 0;\n\
PRAGMA ignore_check_constraints = 0;\n\
PRAGMA cache_size= 10240;\n\
PRAGMA cache_size= 4096;\n\
PRAGMA journal_mode=WAL;\n\
"
];
    [db setShouldCacheStatements:YES];
}

+(NSArray *)updated:(EGODatabase*)db {
    NSMutableArray *array = [NSMutableArray array];
    EGODatabaseResult* result = [db executeQuery:@"SELECT * from DataBaseUpdate"];
    for (EGODatabaseRow *row in result.rows) {
        [array addObject:[row stringForColumn:@"Name"]];
    }
    return array;
}

+(void)checkVersion:(EGODatabase*)db{
    
    [[NSNotificationCenter defaultCenter] postNotificationName:DataManagerWillStartUpdateNotification object:self userInfo:nil];
    NSArray *updated = [self updated:db];
    NSMutableArray *updatesList = [NSMutableArray arrayWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"DataBaseUpdates" ofType:@"plist"]];
    [updatesList removeObjectsInArray:updated];
    for (NSString* ver in updatesList){
        NSString* stmt = [NSBundle.mainBundle pathForResource:ver ofType:@"sql"];
        if (!stmt){
            [NSException raise:NSInternalInconsistencyException format:@"Unable to update database. File %@.sql not found", ver];
        }
        if (![self database:db executeUpdate:stmt version:ver]){
            [NSException raise:NSInternalInconsistencyException format:@"Unable to update database. %@", db.lastErrorMessage ?: @"Unknown error"];
        }
    }
    
    [[NSNotificationCenter defaultCenter] postNotificationName:DataManagerDidEndUpdateNotification object:self userInfo:nil];
}


-(void)checkValidIndexesImmediately:(BOOL)immediately{
    
    if (!immediately) {
        
        struct utsname systemInfo;
        uname(&systemInfo);
        NSString *platform = @(systemInfo.machine);
        
        if ([platform rangeOfString:@"iPhone1"].location!=NSNotFound || [platform rangeOfString:@"iPhone2"].location!=NSNotFound || [platform rangeOfString:@"iPod1"].location!=NSNotFound || [platform rangeOfString:@"iPod2"].location!=NSNotFound || [platform rangeOfString:@"iPad1"].location!=NSNotFound || [platform rangeOfString:@"iPad2"].location!=NSNotFound)
            return;
    }
    
    isNeedCancelChecking = NO;
    
    @try {
        
        EGODatabase *currentDatabase = [self currentDatabase];
        [[NSNotificationCenter defaultCenter] postNotificationName:DataManagerIndexesCheckingWillStartNotification
                                                            object:self
                                                          userInfo:nil];
        [self checkValidIndexes:currentDatabase immediately:immediately];
    }
    @finally {
        
        [[NSNotificationCenter defaultCenter] postNotificationName:DataManagerIndexesCheckingDidEndNotification
                                                            object:self
                                                          userInfo:nil];
        isNeedCancelChecking = NO;
    }
}


-(void)cancelUpdatingIndexes {
    
    isNeedCancelChecking = YES;
}


-(void)checkValidIndexes:(EGODatabase*)db immediately:(BOOL)immediately {
    
    EGODatabaseResult* deletedObjects = [self deletedObjects];
    if (!immediately) {
        
        NSDate *lastCheckIndexDate = [[AppSettingManager instance] dateForKey:@"LastCheckIndexDate"];
        if (!deletedObjects.count && fabs([[NSDate date] timeIntervalSinceDate:lastCheckIndexDate]) < 24*60*60)
            return;
    }
    
    [[AppSettingManager instance] setDate:[NSDate date] forKey:@"LastCheckIndexDate"];

    
    [[NSNotificationCenter defaultCenter] postNotificationName:DataManagerIndexesWillStartUpdateNotification object:self userInfo:nil];
    @try{
        for (NSString *table in [self ftsTables:db]) {
            
            if ([self isTableIndexesValid:db tableName:table immediately:immediately])
                continue;
            
            if (isNeedCancelChecking) return;
            
            NSString* pathForDropIndexes = [[NSBundle mainBundle] pathForResource:[NSString stringWithFormat:@"Drop%@Indexes", table]
                                                                           ofType:@"sql"];
            NSString* pathForCreateIndexes = [[NSBundle mainBundle] pathForResource:[NSString stringWithFormat:@"Create%@Indexes", table]
                                                                             ofType:@"sql"];
            if (pathForDropIndexes && pathForCreateIndexes){
                NSString* stmt = [NSString stringWithContentsOfFile:pathForDropIndexes encoding:NSUTF8StringEncoding error:nil];
                [db execute:stmt];
                stmt = [NSString stringWithContentsOfFile:pathForCreateIndexes encoding:NSUTF8StringEncoding error:nil];
                [db execute:stmt];
            }
            
        }
        if (isNeedCancelChecking) return;
        [self handleDeletedObjects:deletedObjects];
        [db execute:@"analyze main"];
    }
    @finally{
        [[NSNotificationCenter defaultCenter] postNotificationName:DataManagerIndexesWillEndUpdateNotification object:self userInfo:nil];
    }
}


-(BOOL)isTableIndexesValid:(EGODatabase*)db tableName:(NSString *)tableName immediately:(BOOL)immediately{
    
    NSInteger tableCnt = -1, ftsCnt = -1, ftsInvertCnt = -1;
    
    if (isNeedCancelChecking) return YES;
    
    
    EGODatabaseResult* result = [db executeQuery:[NSString stringWithFormat:@"SELECT count(1) FROM %@", tableName]];
    if (result.rows.count > 0)
        tableCnt = [((EGODatabaseRow *)result.rows[0]) intForColumnIndex:0];
    else
        return YES;
    
    if (isNeedCancelChecking) return YES;
    
    
    result = [db executeQuery:[NSString stringWithFormat:@"SELECT count(1) FROM %@FTS", tableName]];
    if (result.rows.count > 0)
        ftsCnt = [((EGODatabaseRow *)result.rows[0]) intForColumnIndex:0];
    else
        return YES;
    
    if (tableCnt == -1 || tableCnt != ftsCnt)
        return NO;
    
    if (isNeedCancelChecking) return YES;
    
    if ([self object_exists:[NSString stringWithFormat:@"%@FTSInvert", tableName] type:@"table"]){
        result = [db executeQuery:[NSString stringWithFormat:@"SELECT count(1) FROM %@FTSInvert", tableName]];
        if (result.rows.count > 0)
            ftsInvertCnt = [((EGODatabaseRow *)result.rows[0]) intForColumnIndex:0];
        
        if (ftsInvertCnt >= 0 && tableCnt != ftsInvertCnt)
            return NO;
    }
    
    if (isNeedCancelChecking) return YES;
    
    
    if (ftsCnt > 0)
        if (![self checkMach:db tableName:[NSString stringWithFormat:@"%@FTS", tableName] ftsCnt:ftsCnt immediately:immediately])
            return NO;
    
    if (isNeedCancelChecking) return YES;
    
    if (ftsInvertCnt > 0)
        if (![self checkMach:db tableName:[NSString stringWithFormat:@"%@FTSInvert", tableName] ftsCnt:ftsInvertCnt immediately:immediately])
            return NO;
    
    return YES;
}


-(BOOL)checkMach:(EGODatabase*)db tableName:(NSString *)tableName ftsCnt:(NSInteger)ftsCnt immediately:(BOOL)immediately{
    
    NSString *val = nil;
    
    for (NSInteger cnt = 1; cnt <= (immediately ? ftsCnt : 1000); cnt++) {
        
        if (isNeedCancelChecking) return YES;
        
        EGODatabaseResult* result = [db executeQuery:[NSString stringWithFormat:@"SELECT * FROM %@ LIMIT %ld", tableName, (long)cnt]];
        
        if (result.count == 0)
            continue;
        
        if (result.count < cnt) {
            if (immediately)
                break;
            else
                return NO;
        }

        
        EGODatabaseRow *row = [result rowAtIndex:cnt - 1];
        
        if (isNeedCancelChecking) return YES;
        
        for (NSInteger i = 0; i < result.columnNames.count; i++) {
            
            if (isNeedCancelChecking) return YES;
            
            val = [[[row stringForColumnIndex:i] retain] autorelease];
            if (val && val.length > 0)
                break;
        }
        
        if (val && val.length > 0)
            break;
    }
    
    if (!val)
        return YES;
    
    if (isNeedCancelChecking) return YES;
    
    EGODatabaseResult* result = [db executeQuery:[NSString stringWithFormat:@"SELECT count(1) FROM %@ WHERE %@ MATCH '%@*'", tableName, tableName, val]];
    
    if (result.rows.count > 0)
        return [((EGODatabaseRow *)result.rows[0]) intForColumnIndex:0] > 0;
    
    return NO;
}


-(NSArray *)ftsTables:(EGODatabase*)db{
    
    NSMutableArray *tables = [[[NSMutableArray alloc] init] autorelease];
    
    EGODatabaseResult* result = [db executeQuery:@"SELECT name FROM sqlite_master where name like '%FTS' and type = 'table'"];
    for (EGODatabaseRow *row in result) {
        
        NSString *tblName = [row stringForColumn:@"name"];
        if (tblName.length > 3)
            [tables addObject:[tblName substringToIndex:tblName.length - 3]];
    }
    
    return tables;
}

+(BOOL)updateDatabase:(EGODatabase*)db setCurrentVersion:(NSString *)version{
    return [db executeUpdateWithParameters:@"INSERT OR REPLACE INTO DataBaseUpdate (Name) VALUES (?)", version, nil];
}

+(BOOL)database:(EGODatabase*)db executeUpdate:(NSString*)pathToUpdate version:(NSString *)version{
    NSAutoreleasePool* pool = [[NSAutoreleasePool alloc]init];
    NSError* error = nil;
    NSString *stmt = [NSString stringWithContentsOfFile:pathToUpdate encoding:NSUTF8StringEncoding error:&error];
    BOOL result = FALSE;
    if (!error){
        [db execute:@"PRAGMA foreign_keys = 0;\n\
         PRAGMA ignore_check_constraints = 1"];
        if ([db execute:stmt]) {
        
            result = [self updateDatabase:db setCurrentVersion:version];
        }
    }
    [pool release];
    return result;
}


-(void) checkAndCreateDatabase {
	
	NSFileManager *fileManager = [NSFileManager defaultManager];
	if ([fileManager fileExistsAtPath:self.currentDatabasePath]) return;
	
	NSString *databasePathFromApp = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:[DataManager databaseName]];

    if (encrypted) {
        [self copyDatabaseFrom:databasePathFromApp to:self.currentDatabasePath encryptKey:[NSData dataWithBytes:encryptKey length:sizeof(encryptKey)]];
    }
    else {
        [[NSFileManager defaultManager] copyItemAtPath:databasePathFromApp toPath:self.currentDatabasePath error:nil];
    }
    
    NSURL *currentDatabasePathURL = [NSURL fileURLWithPath:self.currentDatabasePath];
    
    NSError *error = nil;
    BOOL success = [currentDatabasePathURL setResourceValue:@YES
                                                     forKey:NSURLIsExcludedFromBackupKey
                                                      error:&error];
    if(!success)
        NSLog(@"Error excluding %@ from backup %@", [currentDatabasePathURL lastPathComponent], error);
}

-(void)enableDatabaseAccess{
    if (enableDatabaseAccess) return;
//    OSSpinLockLock(&spinlock);
    os_unfair_lock_lock(&_lock);
    if (!thread_db_key){
        [self generateNewDatabaseThreadReference];
    }
    [self currentDatabaseInternal];
    enableDatabaseAccess = TRUE;
//    OSSpinLockUnlock(&spinlock);
    os_unfair_lock_unlock(&_lock);
    [[NSNotificationCenter defaultCenter] postNotificationName:DataManagerDatabaseIsReady object:self userInfo:nil];
}

-(EGODatabase*)createDatabase{
    EGODatabase* db = [[[[EGODatabase class] alloc] initWithPath:self.currentDatabasePath]autorelease];
    if (![db open]){
        return nil;
    }
    if (encrypted) {
        NSData * key = [NSData dataWithBytes:encryptKey length:sizeof(encryptKey)];
        [db execute:[NSString stringWithFormat: @"PRAGMA key = \"x'%@'\";", [key hexString]]];
    }
    sqlite3_create_function_v2(db.handle, "only_digits", 1, SQLITE_ANY, 0, sqlite3_onlydigits, NULL, NULL, NULL);
    sqlite3_create_function_v2(db.handle, "strinvert", 1, SQLITE_ANY, 0, sqlite3_strinvert, NULL, NULL, NULL);
    if (!enableDatabaseAccess){
        [db execute:@"CREATE TABLE if not exists DataBaseUpdate(Name text NULL, CONSTRAINT DataBaseUpdate$PK PRIMARY KEY(Name ASC));"];
        [[self class]checkVersion:db];
    }
    [[self class ]initDatabase:db];
    return db;
}

-(void)generateNewDatabaseThreadReference{
    if (thread_db_key){
        pthread_key_delete(thread_db_key);
    }
    @synchronized(_dbThreadReferences) {
        for (DataManagerTLSWrapper* wrapper in _dbThreadReferences){
            [wrapper.database close];
            wrapper.database = nil;
        }
        [_dbThreadReferences removeAllObjects];
    }
    pthread_key_create(&thread_db_key, threadDidExit);
}

-(void)setThreadDatabase:(EGODatabase*)database{
    if (!thread_db_key) return;
    DataManagerTLSWrapper* dbmTLS = pthread_getspecific(thread_db_key);
    if (dbmTLS && !database){
        @synchronized(_dbThreadReferences) {
            [_dbThreadReferences removeObject:dbmTLS];
        }
        [dbmTLS release];
        pthread_setspecific(thread_db_key, NULL);
        return;
    }
    if (database){
        if (!dbmTLS){
            dbmTLS = [[DataManagerTLSWrapper alloc]init];
            dbmTLS.database = database;
            dbmTLS.dataManager = self;
            @synchronized(_dbThreadReferences) {
                [_dbThreadReferences addObject:dbmTLS];
            }
            pthread_setspecific(thread_db_key,dbmTLS);
        }
        else{
            dbmTLS.database = database;
        }
    }
}

-(EGODatabase*)threadDatabase{
    return thread_db_key ? [[((DataManagerTLSWrapper*)pthread_getspecific(thread_db_key)).database retain] autorelease] : nil;
}

-(EGODatabase*) currentDatabaseInternal{
    EGODatabase* currentDatabase = self.threadDatabase;
    if (!currentDatabase){
        self.threadDatabase = currentDatabase = [self createDatabase];
    }
    return currentDatabase;
}

-(EGODatabase*) currentDatabase{
    EGODatabase* currentDatabase = self.threadDatabase;
    if (!currentDatabase && enableDatabaseAccess){
//        OSSpinLockLock(&spinlock);
        os_unfair_lock_lock(&_lock);
        if (!currentDatabase && enableDatabaseAccess){
            currentDatabase = [self currentDatabaseInternal];
        }
//        OSSpinLockUnlock(&spinlock);
        os_unfair_lock_unlock(&_lock);
    }
    return [[currentDatabase retain]autorelease];
}

-(BOOL)clearTable:(NSString *)name {
    BOOL handleTransaction = FALSE;
    if (!self.currentDatabase.inTransaction){
        [[self currentDatabase] beginTransaction];
        handleTransaction = TRUE;
    }
	NSString *sqlDelete = [NSString stringWithFormat:@"delete from %@", [[self class]quoteName:name]];
	BOOL result = [[self currentDatabase] executeUpdate:sqlDelete];
    if (handleTransaction){
        if (result)
            [[self currentDatabase] commit];
        else
            [[self currentDatabase] rollback];
    }
    return result;
}

-(BOOL)addSavePoint:(NSString *)name {
    NSString *sqlSavepoint = [NSString stringWithFormat:@"savepoint %@", name];
	EGODatabaseResult *result = [self.currentDatabase executeQuery:sqlSavepoint];
    return result.errorCode == 0;
}

-(BOOL)deleteSavePoint:(NSString *)name commited:(BOOL)commit {
    NSString *sqlCommit;
    if (commit) {
        sqlCommit = [NSString stringWithFormat:@"release %@", name];
    }
    else {
        sqlCommit = [NSString stringWithFormat:@"rollback to %@", name];
    }
	EGODatabaseResult *result = [self.currentDatabase executeQuery:sqlCommit];
    return result.errorCode == 0;
}

-(void)copyDatabaseFrom:(NSString *)sourcePath to:(NSString *)destPath encryptKey:(NSData *)key {
    NSString * tempPath = [NSTemporaryDirectory() stringByAppendingPathComponent:[DataManager databaseName]];
    [[NSFileManager defaultManager] copyItemAtPath:sourcePath toPath:tempPath error:nil];
    EGODatabase * database = [EGODatabase databaseWithPath:tempPath];
    if(![database open]) {
        NSLog(@"[EGODatabase] Error opening DB: %@", database.lastErrorMessage);
        return;
    }
    NSInteger databaseVersion = 0;
    EGODatabaseResult* queryResult = [database executeQuery:@"PRAGMA user_version"];
    if (queryResult.count){
        databaseVersion = [[queryResult rowAtIndex:0] intForColumnIndex:0];
    }
    BOOL result = [database execute:[NSString stringWithFormat:
                                     @"ATTACH DATABASE '%@' AS encrypted KEY \"x'%@'\";\n\
                                     SELECT sqlcipher_export('encrypted');\n\
                                     PRAGMA encrypted.user_version = %li;\n\
                                     DETACH DATABASE encrypted;\n\
                                     ", destPath, [key hexString], (long)databaseVersion]];
    if (!result) {
        NSLog(@"[EGODatabase] Exec Failed, Error: %@\n", database.lastErrorMessage);
    }
    [[NSFileManager defaultManager] copyItemAtPath:tempPath toPath:self.currentDatabasePath error:nil];
    [[NSFileManager defaultManager] removeItemAtPath:tempPath error:nil];
}

#pragma mark Singleton Methods

static dispatch_once_t pred;
static DataManager *instance = nil;

+(DataManager *)instance {
    
    dispatch_once(&pred, ^{
        instance = [[DataManager alloc] init];
    });
    return instance;
}



+(NSString *)databaseName {
    
    return [NSString stringWithFormat:@"%@%@", APPLICATION_DATABASE_NAME, @".sqlite"];
}


+(NSString *)pathForDatabase {
    
    NSArray *documentPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDir = documentPaths[0];
    return [documentsDir stringByAppendingPathComponent:[DataManager databaseName]];
}



+(NSString *)pathForDatabaseTemp {
    
    return [DataManager pathForDatabase:[NSString stringWithFormat:@"%@-temp%@", APPLICATION_DATABASE_NAME, @".sqlite"]];
}


+(NSString *)pathForDatabase:(NSString *)dbName {
    
    NSArray *documentPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDir = documentPaths[0];
    return [documentsDir stringByAppendingPathComponent:dbName];
}


+(NSString *)sqliteversion{
    
    return @SQLITE_VERSION;
}


-(void)startUseTempDB {
    
    [SyncOperationQueue.sharedInstance cancelAllOperationsAsync];
    [DataManager removeDataBase:[DataManager pathForDatabaseTemp]];
    [self switchDbToTemp:YES];
    [self enableDatabaseAccess];
}


-(void)finishUseTempDB {

    [self switchDbToTemp:NO];
    
    [DataManager moveDataBase:[DataManager pathForDatabaseTemp] to:[DataManager pathForDatabase]];
    [self enableDatabaseAccess];
 }


-(void)cancelUseTempDB {
    
    [self switchDbToTemp:NO];
    [DataManager removeDataBase:[DataManager pathForDatabaseTemp]];
    [self enableDatabaseAccess];
}


-(void)switchDbToTemp:(BOOL)isTemp {
    
//    OSSpinLockLock(&spinlock);
    os_unfair_lock_lock(&_lock);
    
    enableDatabaseAccess = NO;

//    EGODatabase* db = [[[NSThread currentThread] threadDictionary][self.databaseId] retain];
//    if (db){
//        [db beginTransaction];
//        [db close];
//        [db release];
//    }
    [self generateNewDatabaseThreadReference];
    useTempDB = isTemp;
    
    [self checkAndCreateDatabase];
    
//    OSSpinLockUnlock(&spinlock);
    os_unfair_lock_unlock(&_lock);
}

-(BOOL)isTempDb{
    return useTempDB;
}


+(void)moveDataBase:(NSString *)fromPath to:(NSString *)toPath {
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
	if (![fileManager fileExistsAtPath:fromPath]) return;

    [DataManager removeDataBase:toPath];
	[fileManager moveItemAtPath:fromPath toPath:toPath error:nil];
    [fileManager moveItemAtPath:[NSString stringWithFormat:@"%@-shm", fromPath] toPath:[NSString stringWithFormat:@"%@-shm", toPath] error:nil];
    [fileManager moveItemAtPath:[NSString stringWithFormat:@"%@-wal", fromPath] toPath:[NSString stringWithFormat:@"%@-wal", toPath] error:nil];
    [DataManager removeDataBase:fromPath];
}


+(void)removeDataBase:(NSString *)dbPath {

    NSFileManager *fileManager = [NSFileManager defaultManager];
	[fileManager removeItemAtPath:[NSString stringWithFormat:@"%@", dbPath] error:nil];
    [fileManager removeItemAtPath:[NSString stringWithFormat:@"%@-shm", dbPath] error:nil];
    [fileManager removeItemAtPath:[NSString stringWithFormat:@"%@-wal", dbPath] error:nil];
}


+(void)initialize{
    nullValue = [NSNull null];
}

- (id)copyWithZone:(NSZone *)zone {
	return self;
}

- (id)retain {
	return self;
}

- (NSUInteger)retainCount {
	return UINT_MAX; //denotes an object that cannot be released
}

- (oneway void)release {
	// never release
}

- (id)autorelease {
	return self;
}
- (void)dealloc {
	// Should never be called, but just here for clarity really.
    if (thread_db_key){
        pthread_key_delete(thread_db_key);
    }
    [_dbThreadReferences release];
    [super dealloc];
}

-(NSString*)lastError{
    return self.currentDatabase.lastErrorMessage;
}

#pragma mark -
#pragma mark record actions

-(TableDetails*)getTableInfo:(NSString*)tableName{
//    OSSpinLockLock(&spinlock);
    os_unfair_lock_lock(&_lock);
		if(!table_info_dict)
            table_info_dict = CFDictionaryCreateMutable(NULL, 0, &kCFTypeDictionaryKeyCallBacks, &kCFTypeDictionaryValueCallBacks);
//    OSSpinLockUnlock(&spinlock);
    os_unfair_lock_unlock(&_lock);
    TableDetails* info = CFDictionaryGetValue(table_info_dict, tableName);
    if (info) return info;
    EGODatabaseResult* result = [self.currentDatabase executeQuery:[NSString stringWithFormat:@"pragma table_info(%@)", [[self class] quoteName:tableName]]];
    NSInteger rows = [result count];
    info = [[TableDetails alloc]initWithName:tableName];
	for (NSInteger i = 0; i < rows; i++) {
        EGODatabaseRow *row = [result rowAtIndex:i];
        NSString *columnName = [row stringForColumn:@"name"];
        NSString *columnType = [row stringForColumn:@"type"];
        NSNumber* sqlColumnType = nil;
        if ([columnType isEqualToString:@"datetime"])
            sqlColumnType = [[NSNumber alloc]initWithInt:SqlColumnTypeDatetime];
        else if ([columnType isEqualToString:@"real"])
            sqlColumnType = [[NSNumber alloc]initWithInt:SqlColumnTypeReal];
        else if ([columnType isEqualToString:@"integer"])
            sqlColumnType = [[NSNumber alloc]initWithInt:SqlColumnTypeInt];
        else if ([columnType isEqualToString:@"blob"])
            sqlColumnType = [[NSNumber alloc]initWithInt:SqlColumnTypeBlob];
        else
            sqlColumnType = [[NSNumber alloc]initWithInt:SqlColumnTypeText];
        TableRowInfo* tbInfoRow = [[TableRowInfo alloc]init];
        tbInfoRow.columnName = columnName;
        tbInfoRow.columnType = sqlColumnType;
        [sqlColumnType release];
        tbInfoRow.notNull = [row boolForColumn:@"notnull"];
        tbInfoRow.pk = [row boolForColumn:@"pk"];
        tbInfoRow.dfltValue = [row objectForColumn:@"dflt_value"];
        [info addTableRowsInfo:tbInfoRow];
        [tbInfoRow release];
    }
    //[table_info_dict setObject:info forKey:tableName];
    CFDictionarySetValue(table_info_dict, tableName, info);
    [info release];
    return info;
        
}

-(BOOL)insertRecord:(NSDictionary *)record toTable:(NSString *)tableName replace:(BOOL)replace{
    @autoreleasepool {
        TableDetails* tableInfo = [self getTableInfo:tableName];
        if (!tableInfo) return FALSE;
        //CFMutableArrayRef parameters = CFArrayCreateMutable(NULL, tableInfo.tableRowsInfoArray.count, &cb);
        //its non retained array
        CFMutableArrayRef parameters = CFArrayCreateMutable(NULL, tableInfo.tableRowsInfoArray.count, NULL);
        for (TableRowInfo* tbInfoRow in tableInfo.tableRowsInfoArray){
        //for (NSUInteger columnNum = 0; columnNum<tableInfo.tableRowsInfoArray.count; columnNum++){
            //TableRowInfo* tbInfoRow = [tableInfo.tableRowsInfoArray objectAtIndex:columnNum];
            NSObject *value = nil;
            NSString *stringValue = CFDictionaryGetValue((CFDictionaryRef)record, tbInfoRow.columnName);
            if ((!stringValue || (id)stringValue == nullValue) && tbInfoRow.notNull){
                //stringValue = (![tbInfoRow.dfltValue isEqual:nullValue] && tbInfoRow.dfltValue.description) ? tbInfoRow.dfltValue.description :(id)EMPTY_STRING;
                stringValue = (id)EMPTY_STRING;
            }
            if (stringValue){
                switch ([tbInfoRow.columnType intValue]) {
                    case SqlColumnTypeDatetime:
                    {
                        NSTimeInterval timeInterval;
                        if ([NSDate parseISO8601String:stringValue inTimeIntervalSince1970:&timeInterval]){
                            value = @(timeInterval);
                        }
                    }
                        break;
                    case SqlColumnTypeReal:
                        value = @([stringValue doubleValue]);
                        break;
                    case SqlColumnTypeInt:
                        value = @([stringValue intValue]);
                        break;
                    case SqlColumnTypeBlob:
                        value = [NSData dataWithBase64EncodedString:stringValue];
                        break;
                    default:
                        value = stringValue;
                        break;
                }
            }
            if (!value){
                value = nullValue;
            }
            CFArrayAppendValue(parameters, value);
        };
        BOOL result = FALSE;
        if (CFArrayGetCount(parameters)){
            NSString* stmt = replace ? tableInfo.replaceStmt : tableInfo.insertStmt;
            result = [self.currentDatabase executeUpdate:stmt parameters:(NSArray*)parameters];
        }
        CFRelease(parameters);
        return result;
    }//autorelease
}

NSArray* lastUsedColumnArray;
NSIntegerArray* columnMappings;
NSNull *nullValue ;
NSString* const EMPTY_STRING = @"";
CFArrayCallBacks cb = {0};

-(BOOL)insertRecordFromColumns:(NSArray*)columns values:(NSArray*)values toTable:(NSString *)tableName replace:(BOOL)replace{
    @autoreleasepool {
        TableDetails* tableInfo = [self getTableInfo:tableName];
        if (!tableInfo) return FALSE;
        if (lastUsedColumnArray!=columns || !columnMappings || columnMappings.count != tableInfo.tableRowsInfoArray.count){
            [columnMappings release];
            columnMappings = [[NSIntegerArray alloc]init];
            for (NSUInteger columnNum = 0; columnNum<tableInfo.tableRowsInfoArray.count; columnNum++){
                TableRowInfo* tbInfoRow = (tableInfo.tableRowsInfoArray)[columnNum];
                //NSInteger valueIndex = [columns indexOfObject:tbInfoRow.columnName];
                NSString* columnName = tbInfoRow.columnName;
                NSInteger valueIndex = [columns indexOfObjectPassingTest:^BOOL(id obj, NSUInteger idx, BOOL *stop) {
                    return (BOOL)([obj caseInsensitiveCompare:columnName] == NSOrderedSame);
                }];
                [columnMappings addInteger:valueIndex];
            }
        }
        [lastUsedColumnArray release];
        lastUsedColumnArray = [columns retain];

        CFMutableArrayRef parameters = CFArrayCreateMutable(NULL, tableInfo.tableRowsInfoArray.count, &cb);
        //for (TableRowInfo* tbInfoRow in tableInfo.tableRowsInfoArray){
        for (NSUInteger columnNum = 0; columnNum<tableInfo.tableRowsInfoArray.count; columnNum++){
            TableRowInfo* tbInfoRow = (tableInfo.tableRowsInfoArray)[columnNum];
            NSObject *value = nil;
                @try{
                    NSInteger valueIndex = [columnMappings integerAtIndex:columnNum];
                    if (valueIndex!=NSNotFound){
                        value = values[valueIndex];
                    }
                    if ((!value || (id)value == nullValue) && tbInfoRow.notNull){
//                        if (tbInfoRow.dfltValue && ![tbInfoRow.dfltValue isEqual:nullValue]){
//                            value = tbInfoRow.dfltValue;
//                        }
//                        else{
                            SqlColumnType columnType = tbInfoRow.columnType.integerValue;
                            value = columnType == SqlColumnTypeInt || columnType == SqlColumnTypeReal || columnType == SqlColumnTypeDatetime ? @(0): (id)EMPTY_STRING;
//                        }
                        
                    }
                }@catch (NSException* e) {
                    @throw;
                }
            if (!value){
                value = nullValue;
            }
            CFArrayAppendValue(parameters, value);
        };
        BOOL result = [self.currentDatabase executeUpdate:replace ? tableInfo.replaceStmt : tableInfo.insertStmt parameters:(NSArray*)parameters];
        CFRelease(parameters);
        return result;
    }//autorelease
}

-(NSNamedMutableArray *)getRecords:(EGODatabaseResult *)result {
    NSNamedMutableArray *records = [[[NSNamedMutableArray alloc] init] autorelease];
    for (EGODatabaseRow * row in result.rows) {
        NSMutableDictionary * record = [[NSMutableDictionary alloc] init];
        for (NSInteger i = 0; i < result.columnNames.count; i++) {
            NSString *columnName = (result.columnNames)[i];
            NSString *columnType = (result.columnTypes)[i];
            NSObject *value = nil;
            if ([columnType isEqual:@"datetime"]) {
                value = [row dateForColumnIndex:i];
                NSDate* minDate = [DataUtils dateWithYear:1753 month:1 day:1];
                if (value && [minDate compare:(NSDate*)value]==NSOrderedDescending){
                    value = minDate;
                }
            }
            else {
                value = [row objectForColumnIndex:i];
            }
            if (value != nil) {
                record[columnName] = value;
            }
        }
        [records addObject:record];
        [record release];
    }
    return records;
}

-(NSNumber*)getSyncStateForRowInTable:(NSString *)tableName rowId:(id)rowid syncColumnName:(NSString*)syncColumnName{
    NSString *sqlSelect = [NSString stringWithFormat:@"select %@ from %@ where rowid = ?",[[self class ]quoteName:syncColumnName], [[self class ]quoteName:tableName]];
    EGODatabaseResult *result = [self.currentDatabase executeQueryWithParameters:sqlSelect, rowid, nil];
    if (result.errorCode == 0 && result.rows.count) {
        return [[result rows][0] numberForColumnIndex:0];
    }
    return nil;
}

-(NSNumber*)getSyncStateForRowInTable:(NSString *)tableName rowId:(id)rowid{
    return [self getSyncStateForRowInTable:tableName rowId:rowid syncColumnName:@"SyncState"];
}

-(NSNamedMutableArray *)getSyncRecordsFromTable:(NSString *)tableName{
    return [self getSyncRecordsFromTable:tableName whereClause:nil];
}

-(NSNamedMutableArray *)getSyncRecordsFromTable:(NSString *)tableName syncColumnName:(NSString*)syncColumnName{
    return [self getSyncRecordsFromTable:tableName whereClause:nil syncColumnName:syncColumnName];
}

-(NSNamedMutableArray *)getSyncRecordsFromTable:(NSString *)tableName whereClause:(NSString*)whereClause{
    return [self getSyncRecordsFromTable:tableName whereClause:whereClause syncColumnName:@"SyncState"];
}

-(NSNamedMutableArray *)getSyncRecordsFromTable:(NSString *)tableName whereClause:(NSString*)whereClause syncColumnName:(NSString*)syncColumnName{
    //index on SyncState won't be used in case of 'is not null'
    NSString *sqlSelect = [NSString stringWithFormat:@"select rowid,* from %@ where %@ > -1%@",  [[self class ]quoteName:tableName], [[self class ]quoteName:syncColumnName], whereClause ? [NSString stringWithFormat:@" and %@", whereClause] : @""];
    EGODatabaseResult *result = [self.currentDatabase executeQuery:sqlSelect];
    if (result.errorCode == 0) {
        return [self getRecords:result];
    }
    return nil;
}

-(NSInteger)getSyncRecordsCountFromTable:(NSString *)tableName{
    return [self getSyncRecordsCountFromTable:tableName whereClause:nil syncColumnName:@"SyncState"];
}

-(NSInteger)getSyncRecordsCountFromTable:(NSString *)tableName syncColumnName:(NSString*)syncColumnName{
    return [self getSyncRecordsCountFromTable:tableName whereClause:nil syncColumnName:syncColumnName];
}

-(NSInteger)getSyncRecordsCountFromTable:(NSString *)tableName whereClause:(NSString*)whereClause{
    return [self getSyncRecordsCountFromTable:tableName whereClause:whereClause syncColumnName:@"SyncState"];
}

-(NSInteger)getSyncRecordsCountFromTable:(NSString *)tableName whereClause:(NSString*)whereClause syncColumnName:(NSString*)syncColumnName{
    //index on SyncState won't be used in case of 'is not null'
    NSString *sqlSelect = [NSString stringWithFormat:@"select count(*) from %@ where %@ > -1%@", [[self class ]quoteName:tableName], [[self class ]quoteName:syncColumnName], whereClause ? [NSString stringWithFormat:@" and %@", whereClause] : @""];
    EGODatabaseResult *result = [[self currentDatabase] executeQuery:sqlSelect];
    if (result.errorCode == 0 && result.count) {
        return [(result.rows)[0] intForColumnIndex:0];
    }
    return 0;
}

-(NSInteger)getSyncRecordsCountFromTable:(NSString *)tableName withMask:(NSInteger)mask {
    return [self getSyncRecordsCountFromTable:tableName withMask:mask exceptRowid:nil];
}

-(NSInteger)getSyncRecordsCountFromTable:(NSString *)tableName withMask:(NSInteger)mask exceptRowid:(NSNumber*)rowid{
    NSString *sqlSelect = [NSString stringWithFormat:@"select count(*) from %@ where (SyncState & %li) != 0%@", [[self class ]quoteName:tableName], (long)mask, rowid ? [NSString stringWithFormat:@" and rowid!=%i", [rowid intValue]] : @""];
    EGODatabaseResult *result = [[self currentDatabase] executeQuery:sqlSelect];
    if (result.errorCode == 0 && result.count) {
        return [(result.rows)[0] intForColumnIndex:0];
    }
    return 0;
}

-(void)commitSyncRecordFromTable:(NSString *)tableName rownum:(id)rowid syncColumnName:(NSString*)syncColumnName{
    if (!syncColumnName.length) return;
    NSString *sqlSelect = [NSString stringWithFormat:@"update %@ set %@ = null where rowid=?", [[self class]quoteName:tableName], [[self class]quoteName:syncColumnName]];
    [[self currentDatabase] executeQueryWithParameters:sqlSelect, rowid, nil];
}

-(void)commitSyncRecordFromTable:(NSString *)tableName rownum:(id)rowid{
    [self commitSyncRecordFromTable:tableName rownum:rowid syncColumnName:@"SyncState"];
}

-(void)commitSyncRecordsFromTable:(NSString *)tableName rownum:(id)rowid syncState:(NSInteger)syncState {
    NSString *sqlSelect = [NSString stringWithFormat:@"update %@ set SyncState = %li where rowid=?", [[self class]quoteName:tableName], (long)syncState];
    [[self currentDatabase] executeQueryWithParameters:sqlSelect, rowid, nil];
}

-(NSNamedMutableArray *)getAllRecordsFromTable:(NSString *)tableName {
    NSString *sqlSelect = [NSString stringWithFormat:@"select rowid, * from %@", [[self class ]quoteName:tableName]];
    EGODatabaseResult *result = [[self currentDatabase] executeQuery:sqlSelect];
    if (result.errorCode == 0) {
        return [self getRecords:result];
    }
    return nil;
}

-(NSNamedMutableArray *)getAllRecordsFromTable:(NSString *)tableName modified:(NSDate *)date {
    if (date == nil) {
        date = [NSDate dateWithTimeIntervalSince1970:0.0];
    }
    NSNamedMutableArray * records = nil;
    NSString *sqlSelect = [NSString stringWithFormat:@"select rowid,* from %@ where RecModified > ?1", [[self class ]quoteName:tableName]];
	NSMutableArray* parameters = [[NSMutableArray alloc] init];
	[parameters addObject:date];
    EGODatabaseResult *result = [[self currentDatabase] executeQuery:sqlSelect parameters:parameters];
    if (result.errorCode == 0) {
        records = [self getRecords:result];
    }
    [parameters release];
    return records;
}

-(NSNamedMutableArray *)getAllRecordsFromTable:(NSString *)tableName timestamp:(NSData *)timestamp {
    if (timestamp == nil) {
        timestamp = [NSData data];
    }
    NSNamedMutableArray * records = nil;
    NSString *sqlSelect = [NSString stringWithFormat:@"select rowid,* from %@ where ts > ?1", [[self class ]quoteName:tableName]];
	NSMutableArray* parameters = [[NSMutableArray alloc] init];
	[parameters addObject:timestamp];
    EGODatabaseResult *result = [[self currentDatabase] executeQuery:sqlSelect parameters:parameters];
    if (result.errorCode == 0) {
        records = [self getRecords:result];
    }
    [parameters release];
    return records;
}

-(NSNamedMutableArray *)getAllRecordsFromTable:(NSString *)tableName whereField:(NSString *)fieldName equal:(id)value {
    NSNamedMutableArray * records = nil;
    NSString *sqlSelect = [NSString stringWithFormat:@"select rowid, * from %@ where %@ = ?1", [[self class ]quoteName:tableName], [[self class ]quoteName:fieldName]];
	NSMutableArray* parameters = [[NSMutableArray alloc] init];
	[parameters addObject:value];
    EGODatabaseResult *result = [[self currentDatabase] executeQuery:sqlSelect parameters:parameters];
    if (result.errorCode == 0) {
        records = [self getRecords:result];
    }
    [parameters release];
    return records;
}

-(NSDate*)getMaxRecModifiedDate:(NSString*)tableName{
    NSMutableString * sqlQuery = [NSMutableString stringWithFormat:@"select max(RecModified) from %@", [[self class ]quoteName:tableName]];
    TableDetails * info = [self getTableInfo:tableName];
    if ((info.tableRowsInfo)[@"SyncState"] != nil) {
        [sqlQuery appendString:@" where SyncState is null"];
    }
    EGODatabaseResult* result = [[self currentDatabase]executeQuery:sqlQuery];
    if (result && result.rows.count){
        return [(result.rows)[0]dateForColumnIndex:0];
    }
    return nil;
}

-(NSData*)getMaxTs:(NSString*)tableName{
    NSMutableString * sqlQuery = [NSMutableString stringWithFormat:@"select max(ts) from %@", [[self class ]quoteName:tableName]];
    TableDetails * info = [self getTableInfo:tableName];
    if ((info.tableRowsInfo)[@"SyncState"] != nil) {
        [sqlQuery appendString:@" where SyncState is null"];
    }
    EGODatabaseResult* result = [[self currentDatabase]executeQuery:sqlQuery];
    if (result && result.rows.count){
        return [(result.rows)[0]dataForColumnIndex:0];
    }
    return nil;
}

+(NSString*)quoteName:(NSString*)name{
    //return [NSString stringWithFormat:@"\"%@\"",[name stringByReplacingOccurrencesOfString:@"\"" withString:@"\"\""]];   
    NSMutableString* s = [[NSMutableString alloc]init];
    [s appendString:@"\""];
    NSMutableString* n = [name mutableCopy];
    [n replaceOccurrencesOfString:@"\"" withString:@"\"\"" options:0 range:NSMakeRange(0, n.length)];
    //[s appendString:[name stringByReplacingOccurrencesOfString:@"\"" withString:@"\"\""]];
    [s appendString:n];
    [n release];
    [s appendString:@"\""];
    return [s autorelease];
}

- (NSInteger)getEmailTasksCount {
    NSString *sqlSelect = @"select count(*) from EmailTask";
    EGODatabaseResult *result = [[self currentDatabase] executeQuery:sqlSelect];
    if (result.errorCode == 0 && result.count) {
        return [(result.rows)[0] intForColumnIndex:0];
    }
    return 0;
}

-(BOOL)isDBLocked {
    return NO;
}

-(BOOL)dropTableIndexes:(NSString*)tableName{
    if (!tableName) return TRUE;
    if (![self.currentDatabase execute:@"create table if not exists sql_objects_bak as select * from sqlite_master where 1=2;"]) return FALSE;
    if (![self.currentDatabase executeQueryWithParameters:@"insert into sql_objects_bak select * from sqlite_master where type in ('index','trigger') and sql is not null and tbl_name=?;", tableName, nil]) return FALSE;
    NSMutableString* stmt = nil;
    EGODatabaseResult *result = [self.currentDatabase executeQueryWithParameters:@"select * from sql_objects_bak where tbl_name=? and type='trigger'", tableName, nil];
    if (result.count){
        if (!stmt){
            stmt = [[NSMutableString alloc]init];
        }
        for(EGODatabaseRow* row in result.rows){
            NSString* triggerName = [row stringForColumn:@"name"];
            [stmt appendFormat:@"drop trigger if exists [%@];\n", triggerName];
        }
    }
    result = [self.currentDatabase executeQueryWithParameters:@"select * from sql_objects_bak where tbl_name=? and type='index'", tableName, nil];
    if (result.count){
        if (!stmt){
            stmt = [[NSMutableString alloc]init];
        }
        for(EGODatabaseRow* row in result.rows){
            NSString* indexName = [row stringForColumn:@"name"];
            [stmt appendFormat:@"drop index if exists [%@];\n", indexName];
        }
    }
    BOOL res = TRUE;
    if (stmt){
        res = [self.currentDatabase execute:stmt];
    }
   [stmt release];
    return res;
}


-(BOOL)createTableIndexes:(NSString*)tableName{
    if (!tableName) return TRUE;
    if (![self object_exists:@"sql_objects_bak" type:@"table"]) return TRUE;
    NSMutableString* stmt = nil;
    EGODatabaseResult *result = [self.currentDatabase executeQueryWithParameters:@"select distinct name, type, sql from sql_objects_bak where tbl_name=? order by type", tableName, nil];
    BOOL res = TRUE;
    if (result.count){
        if (!stmt){
            stmt = [[NSMutableString alloc]init];
        }
        for(EGODatabaseRow* row in result.rows){
            NSString* name = [row stringForColumn:@"name"];
            NSString* sql = [row stringForColumn:@"sql"];
            NSString* type = [row stringForColumn:@"type"];
            if ([type isEqualToString:@"index"]){
                if (![self object_exists:name type:@"index"]){
                    [stmt appendFormat:@"%@;\n", sql];
                }
            }
            else if ([type isEqualToString:@"trigger"]){
                if (![self object_exists:name type:@"trigger"]){
                    [stmt appendFormat:@"%@;\n", sql];
                }
            }
        }
        [stmt appendFormat:@"delete from sql_objects_bak where tbl_name='%@';", tableName];
        res = [self.currentDatabase execute:stmt];
    }
    [stmt release];
    return res;
}

-(EGODatabaseResult*)deletedObjects{
    if (![self object_exists:@"sql_objects_bak" type:@"table"]) return nil;
    return [self.currentDatabase executeQuery:@"select tbl_name from sql_objects_bak group by tbl_name"];
}

-(void)handleDeletedObjects:(EGODatabaseResult*)result{
    for(EGODatabaseRow* row in result.rows){
        if (isNeedCancelChecking) return;
        NSString* table = [row stringForColumn:@"tbl_name"];
        [self createTableIndexes:table];
    }
}

-(BOOL)object_exists:(NSString*)name type:(NSString*)type{
    EGODatabaseResult *result = [self.currentDatabase executeQueryWithParameters:@"SELECT 1 FROM sqlite_master WHERE type= ? AND name= ?", type, name, nil];
    return result.count > 0;
}

@end
