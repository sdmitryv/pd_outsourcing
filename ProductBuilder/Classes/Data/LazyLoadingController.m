//
//  LazyLoadingController.m
//  ProductBuilder
//
//  Created by Valera on 9/17/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "LazyLoadingController.h"

@interface LazyLoadingController (Private)
-(void) prepare;
-(BOOL) read:(NSUInteger)limit offset:(NSUInteger)offset;
-(SqlCommand*) command;
@end

@implementation LazyLoadingController

@synthesize prepared = _prepared;

-(SqlCommand*) command
{
	return [_obj command];
}

-(void) prepare
{
	if (!_prepared)
	{
		[_listIds release];
		_listIds = nil;
		SqlCommand* command = [self command];
		if (command){
            _listIds = [[command executeIds] retain];
		}
		if (!_listIds){
			//_listIds = [[NSMutableArray alloc]init];
            _listIds = [[NSIntegerArray alloc]init];
		}
        
		_prepared = TRUE;
	}
}

-(NSString*)readOffsetStatement{
    return self.command.stmt;
}

-(BOOL) read:(NSUInteger)limit offset:(NSUInteger)offset
{
	SqlCommand* command = [self command];
	if (!limit || !command || !command.stmt || offset >= [_listIds count]) return FALSE;
	if( (limit + offset) > [_listIds count]){
		limit = [_listIds count] - offset;
	}
	NSRange range = NSMakeRange(offset, limit);
	NSMutableArray *ids = [NSMutableArray arrayWithArray:[_listIds subarrayWithRange:range]];
	/*
     NSLog(@"request for keys:");
     for(NSNumber* rowid in ids){
     NSLog(@"%@", rowid);
     }
     
     NSLog(@"keys in buffer:");
     for(NSNumber* rowid in [_listValues allKeys]){
     NSLog(@"%@", rowid);
     }
     */
	
	//remove rows already in buffer
	[ids removeObjectsInArray:[_listValues allKeys]];
	
    NSString* stmt = [NSString stringWithFormat:@"select * from (%@) where rowid in (%@)",self.readOffsetStatement, [@"" stringByPaddingToLength:ids.count*2-1 withString: @"?," startingAtIndex:0]];
    DataManager *dbManager =[DataManager instance];
	EGODatabase *db = dbManager.currentDatabase;
	EGODatabaseResult* result = [db executeQuery:stmt parameters:ids];
	//NSString *filterClause = [ids componentsJoinedByString:@","];
	//SqlCommand* readCommand = [[SqlCommand alloc]initWithStmt:[NSString stringWithFormat:@"select * from (%@) where rowid in(%@)",command.stmt, filterClause]];
	//NSLog(@"read buffer: %@", readCommand.stmt);
	//EGODatabaseResult* result = [readCommand execute];
	//[readCommand release];
	if (result.errorCode!=SQLITE_OK){
		return FALSE;
	}
	if ([result count]){
		NSInteger bufferSizeDiff = [_listValues count] + [result count] - BUFFERSIZE;
		//free buffer
		if (bufferSizeDiff > 0){
			for (NSInteger i= 0; i < [_listValuesIds count]; i++){
				NSNumber* rowid = _listValuesIds[i];
                NSUInteger indexOfRow = [_listIds indexOfObject:rowid];
				if (indexOfRow!=NSNotFound && !NSLocationInRange(indexOfRow, range)){
					[_listValues removeObjectForKey:rowid];
					[_listValuesIds removeObject:rowid];
					i--;
					bufferSizeDiff--;
					if (bufferSizeDiff == 0) break;
				}
			}
		}
		@autoreleasepool {
            for(EGODatabaseRow* row in result) {
                NSObject<ILazyItem>* obj = [[_obj class] getItem:row];
                _listValues[[obj rowid]] = obj;
                if ([_listValuesIds indexOfObject:[obj rowid]]==NSNotFound){
                    [_listValuesIds addObject:[obj rowid]];
                }
            }
        }
	}
	else {
		[_listValues removeObjectsForKeys:ids];
	}
    
	return TRUE;
}

NSUInteger const WINDOWSIZE = 25;
NSUInteger const BUFFERSIZE = 75;

-(id)initWithLazyLoadable:(NSObject<ILazyLoadableList>*) obj;
{
	if ((self = [super init])) {
		_listValues = [[NSMutableDictionary alloc]init];
		_listValuesIds = [[NSMutableArray alloc]init];
		_obj = obj;
	}
	return self;
}


-(NSUInteger) count
{
	[self prepare];
	return _listIds ? [_listIds count] : 0;
}

-(void) resetData
{
	[_listValues removeAllObjects];
	[_listValuesIds removeAllObjects];
}

-(void) reset
{
	[self resetData];
	_prepared = FALSE;
	if (_listIds){
		[_listIds release];
		_listIds = nil;
	}
}

-(NSUInteger)indexOfObject:(NSObject<ILazyItem>*) anObject{
	[self prepare];
    if (!_listIds) return NSNotFound;
	return [_listIds indexOfObject:[anObject rowid]];
}


-(id)objectAtIndex:(NSUInteger)index{
	[self prepare];
	if (!_listIds || index >=[_listIds count]){
		return nil;
	}
	NSNumber *rowid =_listIds[index];
	id obj = _listValues[rowid];
	if (!obj){
		NSUInteger newOffset = index > WINDOWSIZE/2 ? index - WINDOWSIZE/2 : 0;
		[self read:WINDOWSIZE offset:newOffset];
		obj = _listValues[rowid];
	}
	return obj;
}


-(void)replaceObject:(id)obj atIndex:(NSUInteger)index{
	
	if (!_listIds || index >=[_listIds count])
		return ;
    
    NSNumber *rowid = _listIds[index];
    _listValues[rowid] = obj;
    _listValuesIds[index] = [obj rowid];
}


-(id)objectByRowid:(NSNumber *)rowid{
    [self prepare];
    if (!_listIds) return nil;
    NSUInteger idx = [_listIds indexOfObject:rowid];
    if (idx==NSNotFound) return nil;
    return [self objectAtIndex:idx]; 
}


-(id)refreshObject:(NSObject<ILazyItem>*) anObject{
    [self prepare];
    if (!_listIds) return nil;
    NSNumber* rowid = [anObject rowid];
    if (!rowid) return nil;
    NSUInteger idx = [_listIds indexOfObject:rowid];
	if (idx == NSNotFound) return nil;
    [_listValues removeObjectForKey:rowid];
	NSUInteger newOffset = idx;
	[self read:1 offset:newOffset];
	return _listValues[rowid];
}

#pragma mark -
#pragma mark for fast enumeration

- (NSUInteger)countByEnumeratingWithState:(NSFastEnumerationState *)state objects:(id __unsafe_unretained [])stackbuf count:(NSUInteger)len
{
	unsigned long count = state->state;
	if(state->state == 0)
	{
		// state->mutationsPtr MUST NOT be NULL.
		//state->mutationsPtr = &state->extra[0];
		state->mutationsPtr = (unsigned long *)self;
		state->extra[0] = 0;
	}
	NSUInteger i = 0;
	if(count < [self count])
	{
		while((count < [self count]) && (i < len))
		{
			id obj = [self objectAtIndex:count];
            [[obj retain] autorelease];
			stackbuf[i++] = obj;
			count++;
		}
	}
	state->state = count;
	state->itemsPtr = stackbuf;
	//state->mutationsPtr = (unsigned long *)self;
	return i;
}

-(void) dealloc
{
    //[_obj release];
	[_listIds release];
	[_listValues release];
	[_listValuesIds release];
    [super dealloc];
}
@end
