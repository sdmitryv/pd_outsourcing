//
//  DataManager.h
//  StockCount
//
//  Created by Office user on 7/23/10.
//  Copyright 2010 1. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <sqlite3.h>
#import "EGODatabase.h"
#import "NSNamedMutableArray.h"


extern NSString* const DataManagerWillStartUpdateNotification;
extern NSString* const DataManagerDidEndUpdateNotification;
extern NSString* const DataManagerDatabaseIsReady;

extern NSString* const DataManagerIndexesCheckingWillStartNotification;
extern NSString* const DataManagerIndexesWillStartUpdateNotification;
extern NSString* const DataManagerIndexesWillEndUpdateNotification;
extern NSString* const DataManagerIndexesCheckingDidEndNotification;


@class Item;
@class EGODatabaseRow;

@interface TableRowInfo : NSObject {
@private
    NSString* columnName;
    NSNumber* columnType;
    BOOL      notNull;
    BOOL      pk;
    NSObject* dfltValue;
}
@property (nonatomic, retain)NSString* columnName;
@property (nonatomic, retain)NSNumber* columnType;
@property (nonatomic, retain)NSObject* dfltValue;
@property (nonatomic, assign)BOOL      notNull;
@property (nonatomic, assign)BOOL      pk;
@end


@interface TableDetails : NSObject{
    NSMutableDictionary* tableRowsInfoDict;
    NSMutableArray* tableRowsInfoArray;
    NSString* insertStmt;
    NSString* replaceStmt;
    NSString* name;
    NSMutableArray* tablePrimaryKeys;
}
@property(nonatomic, retain)NSString* name;
@property(nonatomic, readonly)NSDictionary* tableRowsInfo;
@property(nonatomic, readonly)NSArray* tableRowsInfoArray;
@property(nonatomic, readonly)NSArray* tablePrimaryKeys;
@property(nonatomic, readonly)NSString* insertStmt;
@property(nonatomic, readonly)NSString* replaceStmt;

-(void)addTableRowsInfo:(TableRowInfo*)tableRowInfo;
@end

@interface DataManager : NSObject {

    BOOL enableDatabaseAccess;
    BOOL useTempDB;
    BOOL isNeedCancelChecking;
    BOOL encrypted;
}

@property (nonatomic, readonly) EGODatabase *currentDatabase;
@property (nonatomic, readonly) NSString    *currentDatabasePath;
@property (nonatomic, readonly) BOOL        isDBLocked;

+(DataManager *)instance;
-(void)enableDatabaseAccess;
-(void)checkAndCreateDatabase;
+(NSString*)sqliteversion;
-(BOOL)clearTable:(NSString *)name;
-(BOOL)insertRecordFromColumns:(NSArray*)columns values:(NSArray*)values toTable:(NSString *)tableName replace:(BOOL)replace;
//-(BOOL)insertRecord:(id)record values:(NSArray*)values toTable:(NSString *)tableName replace:(BOOL)replace valuesAreTyped:(BOOL)valuesAreTyped;
-(BOOL)insertRecord:(NSDictionary *)record toTable:(NSString *)tableName replace:(BOOL)replace;
-(NSDate*)getMaxRecModifiedDate:(NSString*)tableName;
-(NSData*)getMaxTs:(NSString*)tableName;
-(NSNamedMutableArray *)getSyncRecordsFromTable:(NSString *)tableName;
-(NSNamedMutableArray *)getSyncRecordsFromTable:(NSString *)tableName syncColumnName:(NSString*)syncColumnName;
-(NSNamedMutableArray *)getSyncRecordsFromTable:(NSString *)tableName whereClause:(NSString*)whereClause;
-(NSNamedMutableArray *)getSyncRecordsFromTable:(NSString *)tableName whereClause:(NSString*)whereClause syncColumnName:(NSString*)syncColumnName;
-(void)commitSyncRecordFromTable:(NSString *)tableName rownum:(id)rowid syncColumnName:(NSString*)syncColumnName;
-(void)commitSyncRecordFromTable:(NSString *)tableName rownum:(id)rowid;
-(void)commitSyncRecordsFromTable:(NSString *)tableName rownum:(id)rowid syncState:(NSInteger)syncState;
-(NSNamedMutableArray *)getAllRecordsFromTable:(NSString *)tableName;
-(NSNamedMutableArray *)getAllRecordsFromTable:(NSString *)tableName modified:(NSDate *)date;
-(NSNamedMutableArray *)getAllRecordsFromTable:(NSString *)tableName timestamp:(NSData *)timestamp;
-(NSNamedMutableArray *)getAllRecordsFromTable:(NSString *)tableName whereField:(NSString *)fieldName equal:(id)value;
-(NSInteger)getSyncRecordsCountFromTable:(NSString *)tableName;
-(NSInteger)getSyncRecordsCountFromTable:(NSString *)tableName syncColumnName:(NSString*)syncColumnName;
-(NSInteger)getSyncRecordsCountFromTable:(NSString *)tableName whereClause:(NSString*)whereClause;
-(NSInteger)getSyncRecordsCountFromTable:(NSString *)tableName whereClause:(NSString*)whereClause syncColumnName:(NSString*)syncColumnName;
-(NSInteger)getSyncRecordsCountFromTable:(NSString *)tableName withMask:(NSInteger)mask;
-(NSInteger)getSyncRecordsCountFromTable:(NSString *)tableName withMask:(NSInteger)mask exceptRowid:(NSNumber*)rowid;
-(NSNumber*)getSyncStateForRowInTable:(NSString *)tableName rowId:(id)rowid syncColumnName:(NSString*)syncColumnName;
-(NSNumber*)getSyncStateForRowInTable:(NSString *)tableName rowId:(id)rowid;
-(BOOL)addSavePoint:(NSString *)name;
-(BOOL)deleteSavePoint:(NSString *)name commited:(BOOL)commit;
-(NSString*)lastError;
-(NSNamedMutableArray *)getRecords:(EGODatabaseResult *)result;
+(NSString*)quoteName:(NSString*)name;
-(NSInteger)getEmailTasksCount;
-(TableDetails*)getTableInfo:(NSString*)tableName;

+(NSString *)pathForDatabase;
+(NSString *)pathForDatabaseTemp;

-(void)startUseTempDB;
-(void)finishUseTempDB;
-(void)cancelUseTempDB;
@property (nonatomic, readonly)BOOL isTempDb;

-(void)checkValidIndexesImmediately:(BOOL)immediately;
-(void)cancelUpdatingIndexes;

-(BOOL)dropTableIndexes:(NSString*)tableName;
-(BOOL)createTableIndexes:(NSString*)tableName;
-(EGODatabaseResult*)deletedObjects;
-(void)handleDeletedObjects:(EGODatabaseResult*)result;

-(BOOL)object_exists:(NSString*)name type:(NSString*)type;

@end
