//
//  MobileExceptionLog.m
//  ProductBuilder
//
//  Created by Alexander Martyshko on 10/3/13.
//  Copyright (c) 2013 Cloudworks. All rights reserved.
//

#import "MobileExceptionLog.h"
#import "CROEntity+Sqlite.h"
#import "Settings.h"
#import "Location.h"
#import "Workstation.h"
#import "WebServiceResponse.h"
#import "UploadExceptionLogOperation.h"
#import "AppSettingManager.h"

@implementation MobileExceptionLog

@synthesize description = _description;

- (void)dealloc {
    [_message release];
    [_locationID release];
    [_worstationID release];
    [_deviceID release];
    [_description release];
    [super dealloc];
}

+(void)postSoapFault:(SOAPFault *)soapFault originalRequest:(NSString *)originalRequest {
    MobileExceptionLog *log = [[MobileExceptionLog alloc] init];
    
    log.id = [BPUUID UUID];
    NSDate *date = [NSDate date];
    log.recCreated = date;
    log.recModified = date;
    log.description = originalRequest;
    log.locationID = [AppSettingManager instance].locationId;
    log.worstationID = [Workstation currentWorkstation].id;
    log.deviceID = [AppSettingManager instance].iPadId ? [AppSettingManager instance].iPadId : @"";
    
    XmlElement *messageXml = [soapFault.detailElement childWithNameByPath:@"ExceptionDetail\\message"];
    XmlElement *stackTraceXml = [soapFault.detailElement childWithNameByPath:@"ExceptionDetail\\stackTrace"];
    log.message = [NSString stringWithFormat:@"FaultCode:%@\nFaultString:%@\nmessage:%@\nStackTrace:%@",
                         soapFault.faultcode,
                        soapFault.faultstring,
                         messageXml.value,
                         stackTraceXml.value];
    
    [log save];
    [log release];
    
    // try to immediately upload this log entry
//    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
//        UploadExceptionLogOperation *operation = [UploadExceptionLogOperation syncOperation];
//        [operation sync];
//    });
}

@end
