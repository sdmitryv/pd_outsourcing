//
//  DeviceAgentManager.m
//  DeviceAgent
//
//  Created by Alexander Martyshko on 2/28/14.
//  Copyright (c) 2014 Cloudworks. All rights reserved.
//

#import "DeviceAgentManager.h"
#import "KeychainItemWrapper.h"
#import "RTAlertView.h"
#import "Device.h"
#import "Settings.h"
#import "AppSettingManager.h"
#import "AppParameterManager.h"

@implementation DeviceAgentManager

@synthesize currentDevice = _currentDevice;

+(DeviceAgentManager *)sharedInstance {
    static dispatch_once_t pred;
    static DeviceAgentManager *instance = nil;
    
    dispatch_once(&pred, ^{
        instance = [[DeviceAgentManager alloc] init];
    });
    return instance;
}

- (id)init {
    self = [super init];
    if (self) {
    }
    return self;
}

- (void)dealloc {

    [_appBundleId release];
    [_appVersion release];
    [_currentDevice release];
    [_currentLocation release];
    
    [super dealloc];
}

-(DeviceAgentState)deviceAgentState{
    
    if (AppParameterManager.instance.disableDeviceAgent) {
     
        return DeviceAgentStateOk;
    }
    
    @synchronized(self){
        return [AppSettingManager instance].deviceState;
    }
}

- (void)setDeviceAgentState:(DeviceAgentState)deviceAgentState {
    
    @synchronized(self){
        
        if ([AppSettingManager instance].deviceState != deviceAgentState) {
            
            [AppSettingManager instance].deviceState = deviceAgentState;
        }
    }
}

- (void)writeServerAddress:(NSString *)address {
#if !(TARGET_IPHONE_SIMULATOR)
    @synchronized(self) {
        KeychainItemWrapper *wrapper = [[KeychainItemWrapper alloc] initWithIdentifier:DEVICE_AGENT_SERVER_ADDRESS accessGroup:[KeychainItemWrapper keychainAccessGroup]];
        [wrapper setObject:address forKey:(NSString*)kSecAttrAccount];
        [wrapper release];
    }
#endif
}

- (void)writeAppVersion {

    // Writing the version of the DeviceAgent app to Keychain
    KeychainItemWrapper *wrapper = [[KeychainItemWrapper alloc] initWithIdentifier:AppParameterManager.instance.bundleId accessGroup:[KeychainItemWrapper keychainAccessGroup]];
    [wrapper setObject:self.appVersion forKey:(NSString*)kSecAttrAccount];
    [wrapper release];
}

- (NSString *)appVersion {
    if (!_appVersion){
#ifdef  DEBUG
        char* settingsAppVersion = getenv("RTAppVersion");
    if (settingsAppVersion)
        _appVersion = [@(settingsAppVersion) retain];
    else
#endif
        _appVersion = [[[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleVersion"] retain];
    }
    return _appVersion;
}


- (BOOL)checkIsDeviceActive {

    return [self checkIsDeviceActive:nil];
}

- (BOOL)checkIsDeviceActive:(void (^)(NSInteger buttonIndex))aCompletedBlock {

    if (self.isDeviceActive){
        return YES;
    }
    
    NSString *title = nil;
    NSString *message = nil;
    
    switch ([AppSettingManager instance].deviceState) {
        case DeviceAgentStateAgentIdMismatch:
            title = NSLocalizedString(@"DEVICE_AGENT_ID_MISMATCH_DETAILED_TITLE", nil);
            message = NSLocalizedString(@"DEVICE_AGENT_ID_MISNATCH_DETAILED_MESSAGE", nil);
            break;
        default:
            title = NSLocalizedString(@"ERROR_TEXT", nil);
            message = self.deviceStateStringRepresentation;
            break;
    }
    
    RTAlertView *alert = [[RTAlertView alloc] initWithTitle:title
                                                    message:message
                                                   delegate:nil
                                          cancelButtonTitle:NSLocalizedString(@"OKBTN_TITLE", nil)
                                          otherButtonTitles:nil, nil];
    alert.completedBlock = aCompletedBlock;
    [alert show];
    [alert release];
    
    return NO;
}

-(BOOL)isDeviceActive{
    return (AppParameterManager.instance.disableDeviceAgent || [AppSettingManager instance].deviceState == DeviceAgentStateOk || [AppSettingManager instance].deviceState == DeviceAgentStateUnknown);
}

- (NSString *)deviceStateStringRepresentation {
    
    switch ([AppSettingManager instance].deviceState) {
            
        case DeviceAgentStateOk:
            return nil;
        case DeviceAgentStateNoSuchDevice:
            return NSLocalizedString(@"DEVICE_AGENT_NO_SUCH_DEVICE_MESSAGE", nil);
        case DeviceAgentStateDeactivated:
            return NSLocalizedString(@"DEVICE_AGENT_DEVICE_WAS_DEACTIVATED_MESSAGE", nil);
        case DeviceAgentStateNotAuthorizedApp:
            return NSLocalizedString(@"DEVICE_AGENT_NOT_AUTHORIZED_APP", nil);
        case DeviceAgentStateAgentIdMismatch:
            return NSLocalizedString(@"DEVICE_AGENT_ID_MISMATCH", nil);
        case DeviceAgentStateDeviceIdMismatch:
            return NSLocalizedString(@"DEVICE_AGENT_SERVER_ID_MISNATCH_DETAILED_MESSAGE", nil);
        case DeviceAgentStateLocationIdMismatch:
            return NSLocalizedString(@"DEVICE_AGENT_LOCATION_ID_MISNATCH_DETAILED_MESSAGE", nil);
        case DeviceAgentStateUnknown:
            return NSLocalizedString(@"UNKNOWNERROR_TEXT", nil);
        default:
            break;
    }
    
    return nil;
}

-(Device*)currentDevice{
    @synchronized(self){
        return [[_currentDevice retain]autorelease];
    }
}

- (void)setCurrentDevice:(Device *)currentDevice {
    
    @synchronized(self){
        
        [_currentDevice release];
        _currentDevice = [currentDevice retain];
        
        if (!currentDevice)
            return;
        
        self.deviceAgentState = DeviceAgentStateOk;
        
        [[AppSettingManager instance] updateSettingsWithDevice:_currentDevice];
    }
}

@end
