//
//  Product.m
//  ProductBuilder
//
//  Created by Viacheslav Lulakov on 9/5/17.
//  Copyright © 2017 Cloudworks. All rights reserved.
//

#import "Product.h"

@implementation Product

- (void)dealloc {
    
    [_price release];
    [_cost release];
    [_taxCategoryCode release];
    [_manufacturerName release];
    [_department release];
    [_procuctClass release];
    [_primaryVendor release];

    [_clu release];
    [_elements release];
    [_modifications release];
    [_instructions release];
    [_storeDescription release];

    [super dealloc];
}

- (NSUInteger)numberOfModifications {
    return _modifications.count;
}

-(NSString *)description {
    NSMutableString * result = [NSMutableString stringWithFormat:@"Description: %@", self.elementsDescription];
    if (_modifications.count > 0) {
        [result appendString: [NSString stringWithFormat:@"\nModifications: %@", self.modificationsDescriptions]];
    }
    return result;
}

- (NSString *)elementsDescription {
    return [_elements componentsJoinedByString: @", "];
}

- (NSString *)modificationsDescriptions {
    return [_modifications componentsJoinedByString: @", "];
}

- (NSString *)longDescription {
    
    NSMutableString *str = [NSMutableString string];
    [str appendString:[NSString stringWithFormat:@"Description: %@", [_elements componentsJoinedByString: @" "]]];
    
    if (_modifications.count > 0) {
        
        [str appendString:[NSString stringWithFormat:@"\nModifications: %@", [_modifications componentsJoinedByString: @" "]]];
    }
    
    if (_instructions) {
        
        [str appendString:[NSString stringWithFormat:@"\nInstructions: %@", _instructions]];
    }
    
    return str;

}


+(NSMutableArray*)getProducts {
    return nil;
}

@end
