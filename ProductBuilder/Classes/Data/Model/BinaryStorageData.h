//
//  BinaryStorageData.h
//  ProductBuilder
//
//  Created by Sergey Lugovoy on 2/16/16.
//  Copyright © 2016 Cloudworks. All rights reserved.
//

#import "CROEntity.h"

@interface BinaryStorageData : CROEntity

@property (nonatomic, retain) BPUUID *ownerID;
@property (nonatomic, assign) NSInteger type;
@property (nonatomic, retain) NSData *data;
@property (nonatomic, retain) NSString *name;
@property (nonatomic, retain) NSString *version;

+(NSArray *)getBinaryStorageDataWithOwnerID:(BPUUID *)ownerID;

@end
