//
//  CustomLookup.h
//  CloudworksPOS
//
//  Created by Vitaliy Gervazuk on 7/22/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CROEntity.h"

@interface CustomLookup : CROEntity {
    NSInteger _areaNum;
    NSInteger _fieldNum;
    NSString *_name;
    NSString *_alias;
    NSString *_fullName;
    BOOL _isDeleted;
}

@property (nonatomic, assign) NSInteger areaNum;
@property (nonatomic, assign) NSInteger fieldNum;
@property (nonatomic, retain) NSString* name;
@property (nonatomic, retain) NSString* alias;
@property (nonatomic, retain) NSString* fullName;
@property (nonatomic, assign) BOOL isDeleted;

+(NSMutableArray*)getLookups:(NSInteger)areaNum lookupNum:(NSInteger)lookupNum;

@end
