//
//  AppSetting.m
//  StockCount
//
//  Created by Lulakov Viacheslav on 10/19/10.
//  Copyright 2010 DBBestTechnologies. All rights reserved.
//

#import "Setting.h"
#import "Workstation.h"
#import "DecimalHelper.h"

NSString* const settingWorkstation = @"Workstation";
NSString* const settingCompany = @"Company";
NSString* const settingLocation = @"Location";

@implementation Setting

@synthesize type = _type;
@synthesize key = _key;
@synthesize value = _value, designate = _designate;

-(void) dealloc
{
	[_type release];
	[_key release];
	[_value release];
	[_designate release];
	[super dealloc];
}

-(SettingType)settingType{
    return [_type isEqualToString:settingWorkstation]?SettingTypeWorkstation :
            [_type isEqualToString:settingCompany] ? SettingTypeCompany :
    SettingTypeLocation;
}

+(NSString* const)settingTypeStringByType:(SettingType)type{
    switch (type) {
        case SettingTypeCompany:
            return settingCompany;
        case SettingTypeWorkstation:
            return settingWorkstation;
        default:
            return settingLocation;
    }
}

+ (NSMutableArray *)getSettings{
	return nil;
}

+(BPUUID*)getContextIdByType:(SettingType)type{
    switch(type){
        case SettingTypeWorkstation:
            return [[[Workstation currentWorkstation].id retain]autorelease];
        case SettingTypeLocation:
            return [[[Workstation currentWorkstation].locationID retain]autorelease];
        default:
            return nil;
    }
}

+ (Setting *)getSettingForKey:(NSString *)key type:(NSString*)type designate:(BPUUID*)designate{
	return nil;
}

+ (Setting *)getSettingForKey:(NSString *)key type:(SettingType)type{
    return [[self class] getSettingForKey:key type:[[self class] settingTypeStringByType:type] designate:[[self class] getContextIdByType:type]];
}

+ (NSString*)stringForKey:(NSString *)key type:(SettingType)type{
	return [[[self class]getSettingForKey:key type:type].value stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
}

+ (BOOL)boolForKey:(NSString *)key type:(SettingType)type{
	return [[[self class]stringForKey:key type:type] boolValue];
}

+ (NSInteger)intForKey:(NSString *)key type:(SettingType)type{
	return [[[self class]stringForKey:key type:type] intValue];
}

+ (NSString*)stringForKey:(NSString *)key type:(SettingType)type defaultValue:(NSString*)defaultValue{
	Setting* setting = [[self class]getSettingForKey:key type:type];
	if (!setting) return defaultValue;
	return [setting.value stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
}

+ (BOOL)boolForKey:(NSString *)key type:(SettingType)type defaultValue:(BOOL)defaultValue{
	Setting* setting = [[self class]getSettingForKey:key type:type];
	if (!setting) return defaultValue;
	return [setting.value boolValue];
}

+ (NSInteger)intForKey:(NSString *)key type:(SettingType)type defaultValue:(NSInteger)defaultValue{
	Setting* setting = [[self class]getSettingForKey:key type:type];
	if (!setting) return defaultValue;
	return [setting.value intValue];
}

+ (NSDecimal)decimalForKey:(NSString *)key type:(SettingType)type defaultValue:(NSDecimal)defaultValue{
    Setting* setting = [[self class]getSettingForKey:key type:type];
    if (!setting) return defaultValue;
    return CPDecimalFromString(setting.value);
}


@end

