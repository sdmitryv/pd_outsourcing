//
//  State.h
//  ProductBuilder
//
//  Created by Alexander Martyshko on 1/16/15.
//  Copyright (c) 2015 Cloudworks. All rights reserved.
//

#import "CROEntity.h"

@interface State : CROEntity
@property (nonatomic, retain) NSString *code;
@property (nonatomic, retain) NSString *name;
@property (nonatomic, retain) BPUUID *countryId;
+(NSArray*)getList;
@end
