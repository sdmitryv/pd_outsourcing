//
//  CustomField.m
//  CloudworksPOS
//
//  Created by Vitaliy Gervazuk on 7/18/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "CustomField.h"


@implementation CustomField

@synthesize label = _label;
@synthesize defaultLabel = _defaultLabel;
@synthesize type = _type;
@synthesize customFieldType = _customFieldType;
@synthesize customFieldNumber = _customFieldNumber;
@synthesize language = _language;
@synthesize used = _used;
@synthesize Secure = _secure;

-(void)dealloc{
    [_label release];
    [_defaultLabel release];
    [super dealloc];
}

-(NSString *)caption {
    if (_used) {
        return _label.length > 0 ? _label : _defaultLabel;
    }
    return nil;
}

+(NSString*)getCaption:(CustomFieldArea)areatype fieldtype:(CustomFieldType)fieldtype fieldNumber:(NSInteger)fieldnumber{

    NSArray *result =  [self getCustomFieldControls:areatype fieldType:fieldtype fieldNum:fieldnumber];
    
    for (CustomField *field in result) {
        
        if (field.used){
                if (!field.label.length){
                return field.defaultLabel;
            }
            else {
                return field.label;
            }
        }
        else {
            return nil;
        }
        
    }
    
    return nil;
}

+(NSArray*)getCustomFieldControls:(CustomFieldArea)areaType fieldType:(CustomFieldType)fieldType fieldNum:(NSInteger)fieldNum{
    return nil;
}

+(NSArray *)getCustomFieldsByArea:(NSInteger)area {
    return nil;
}

+(BOOL)getSecure:(CustomFieldArea)areaType fieldType:(CustomFieldType)fieldType fieldNum:(NSInteger)fieldNum {
    NSArray *customFields =  [self getCustomFieldControls:areaType fieldType:fieldType fieldNum:fieldNum];
    if (customFields.count > 0) {
        CustomField *field = customFields[0];
        if (field.used) {
            return field.Secure;
        }
    }
    return NO;
}

@end
