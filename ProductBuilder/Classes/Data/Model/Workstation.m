//
//  Workstation.m
//  ProductBuilder
//
//  Created by Valera on 9/13/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "DataUtils.h"
#import "Workstation.h"
#import "AppSettingManager.h"

@implementation Workstation

@synthesize wsNum = _wsNum;
@synthesize name = _name;
@synthesize locationID = _locationID;
@synthesize taxZoneID = _taxZoneID;
@synthesize nodeID = _nodeID;
@synthesize machineName = _machineName;
@synthesize machineID = _machineID;

-(void) dealloc
{
	[_name release];
	[_locationID release];
	[_taxZoneID release];
	[_nodeID release];
	[_machineName release];
	[_machineID release];

	[super dealloc];
}

+(NSArray*)getWorkstations{
    return nil;
}

Workstation* _currentWorkstation;

+(void)resetCache{
    @synchronized(self){
        [_currentWorkstation release];
        _currentWorkstation = nil;
    }
}


+(Workstation *)currentWorkstation {
    Workstation* aCurrentWorktation = nil;
    @synchronized(self){
        if (!_currentWorkstation) {
            BPUUID *workstationId = [AppSettingManager instance].workstationId;
            if (!workstationId)
                return nil;
            _currentWorkstation = [[Workstation getInstanceById:workstationId] retain];
        }
        aCurrentWorktation = [[_currentWorkstation retain]autorelease];
    }
    return aCurrentWorktation;
}


+(void)setCurrentWorkstationId:(BPUUID*)id{
    @synchronized(self){
        [_currentWorkstation release];
        _currentWorkstation = nil;
        [AppSettingManager instance].workstationId = id;
    }
}

@end
