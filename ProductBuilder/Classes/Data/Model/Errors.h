//
//  Errors.h
//  iPadPOS
//
//  Created by valera on 5/16/11.
//  Copyright 2011 DBBestTechnologies. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface Errors : NSObject<NSFastEnumeration>{
@private
	NSMutableDictionary *_errors;
	id base;
}

-(id)init:(id)base;
-(void)add:(NSString *)key error:(NSString*)error;
-(void)removeErrorsForKey:(NSString*)key;
-(NSArray*)errorsForKey:(NSString*)key;
-(NSUInteger)count;
-(void)clear;
-(void)setObject:(NSString*)error forKeyedSubscript:(NSString*)key;
-(NSArray*)objectForKeyedSubscript:(NSString*)key;
-(NSArray*)allKeys;
@end

