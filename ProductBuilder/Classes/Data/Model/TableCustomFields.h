//
//  TableCustomFields.h
//  CloudworksPOS
//
//  Created by
//  Copyright 2015 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CEntityList.h"
#import "TableCustomField.h"

@interface TableCustomFields : CEntity {
    
    NSString *_tableName;
    CEntityList *_fieldsList;
}

@property (nonatomic, readonly) CEntityList *fieldsList;
@property (nonatomic, retain) NSString *tableName;

-(id)initForTable:(NSString *)table;
-(void)loadFields;

-(TableCustomField*)getFieldByCode:(NSString *)fieldCode;
+(void)resetCache;
@end
