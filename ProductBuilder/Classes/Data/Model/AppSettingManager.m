//
//  AppSetting.m
//  ProductBuilder
//
//  Created by Lulakov Viacheslav on 10/19/10.
//  Copyright 2010 DBBestTechnologies. All rights reserved.
//

#import "AppSettingManager.h"
#import "AppSetting.h"
#import "KeychainItemWrapper.h"
#import "Device.h"
#import "StatusViewController.h"
#import "DeviceAgentManager.h"
#import "SecurityManager.h"
#import "AppParameterManager.h"
#import "Product_Builder-Swift.h"

NSString* const LastSyncDateKey = @"LastSyncDate";

@interface AppSettingManager(Private)

@end

@implementation AppSettingManager

#pragma mark -
#pragma mark helper methods


-(NSMutableDictionary *)settingsList {
    
    if (_settingsList)
        return _settingsList;
    
    NSArray* settings = [AppSetting getSettings];
    if (!settings)
        return nil;
    
    _settingsList = [[NSMutableDictionary alloc]init];
    for(AppSetting* setting in settings)
        if (setting.key && !_settingsList[setting.key])
            _settingsList[setting.key] = setting;
    
    return _settingsList;
}

-(void)setString:(NSString*)value forKey:(NSString*)key{
    
	AppSetting* setting = self.settingsList[key];
	if (!setting){
		
        setting = [[[AppSetting alloc]init]autorelease];
		setting.key = key;
		self.settingsList[key] = setting;
	}
	setting.value = value;
	[setting save];
}

-(void)setBool:(BOOL)object forKey:(NSString*)key{
	[self setString:[@(object) stringValue] forKey:key];
}

-(void)setInt:(NSInteger)object forKey:(NSString*)key{
	[self setString:[@(object) stringValue] forKey:key];
}

-(void)setDate:(NSDate*)object forKey:(NSString*)key{
	[self setString:object ? [[NSDecimalNumber numberWithFloat:[object timeIntervalSince1970]] stringValue]: nil forKey:key];
}

-(void)setDecimalNumber:(NSDecimalNumber*)object forKey:(NSString*)key{
    [self setString:object ? [object stringValue]: nil forKey:key];
}

-(void)setUuid:(BPUUID*)object forKey:(NSString*)key{
	[self setString:object ? object.stringRepresentation : nil forKey:key];
}

-(NSString*)stringForKey:(NSString*)key{
	AppSetting* setting = self.settingsList[key];
	return setting.value;
}

-(BOOL)boolForKey:(NSString*)key{
	AppSetting* setting = self.settingsList[key];
	return [setting.value boolValue];
}

-(NSInteger)intForKey:(NSString*)key{
	AppSetting* setting = self.settingsList[key];
	return [setting.value intValue];
}

-(NSString*)stringForKey:(NSString*)key defaultValue:(NSString*)defaultValue{
	AppSetting* setting = self.settingsList[key];
	return setting? setting.value : defaultValue;
}

-(BOOL)boolForKey:(NSString*)key defaultValue:(BOOL)defaultValue{
	AppSetting* setting = self.settingsList[key];
	return setting? [setting.value boolValue] : defaultValue;
}

-(NSInteger)intForKey:(NSString*)key defaultValue:(NSInteger)defaultValue{
	AppSetting* setting = self.settingsList[key];
	return setting? [setting.value intValue] : defaultValue;
}

-(NSDate*)dateForKey:(NSString*)key{
	AppSetting* setting = self.settingsList[key];
	return setting? [NSDate dateWithTimeIntervalSince1970:[setting.value floatValue]] : nil;
}

-(NSDecimalNumber*)decimalNumberForKey:(NSString*)key{
    AppSetting* setting = self.settingsList[key];
    return setting ? [NSDecimalNumber decimalNumberWithString:setting.value] : nil;
}

-(BPUUID*)uuidForKey:(NSString*)key{
	AppSetting* setting = self.settingsList[key];
	return setting ? [BPUUID UUIDWithString:setting.value] : nil;
}

#pragma mark -
#pragma mark properties

-(BOOL)paymentSwipeCreditCard{
	return [self boolForKey:SWIPE_CREDIT_CARD_KEY defaultValue:YES];
}

-(void)setPaymentSwipeCreditCard:(BOOL)value{
	return [self setBool:value forKey:SWIPE_CREDIT_CARD_KEY];
}

-(NSInteger)receiptAccountsTabSelectedIndex{
    return [self intForKey:RECEIPT_ACCOUNTS_TAB_SELECTED_INDEX defaultValue:0];
}

-(void)setReceiptAccountsTabSelectedIndex:(NSInteger)value{
    return [self setInt:value forKey:RECEIPT_ACCOUNTS_TAB_SELECTED_INDEX];
}

-(NSInteger)customerAccountsTabSelectedIndex{
    return [self intForKey:CUSTOMER_ACCOUNTS_TAB_SELECTED_INDEX defaultValue:0];
}

-(void)setCustomerAccountsTabSelectedIndex:(NSInteger)value{
    return [self setInt:value forKey:CUSTOMER_ACCOUNTS_TAB_SELECTED_INDEX];
}


-(NSString*)lastCustomerCode{
    
    NSString *code = [self stringForKey:LAST_CUSTOMER_CODE];
	return code.length == 0 ? nil : code;
}

-(void)setLastCustomerCode:(NSString *)value{
	[self setString:value == nil ? @"" : value forKey:LAST_CUSTOMER_CODE];
}


-(NSInteger)receiptLastNumber{

    AppSetting* setting = self.settingsList[LAST_RECEIPT_NUMBER_KEY];
    
    NSInteger currentNum = 0;
	if (!setting) {
        
        setting = self.settingsList[NEXT_RECEIPT_NUMBER_KEY];
        currentNum = [setting.value intValue] - 1;
    }
    else
        currentNum = [setting.value intValue];
    
	return currentNum;
}

-(void)setReceiptLastNumber:(NSInteger)value{
	return [self setInt:value forKey:LAST_RECEIPT_NUMBER_KEY];
}


-(BOOL)receiptSearchKBVisible{
	return [self boolForKey:RECEIPT_SEARCH_KB_VISIBLE_KEY defaultValue:TRUE];
}

-(void)setReceiptSearchKBVisible:(BOOL)value{
	return [self setBool:value forKey:RECEIPT_SEARCH_KB_VISIBLE_KEY];
}

-(BOOL)customerSearchKeyboardEnable {
	return [self boolForKey:CUSTOMER_SEARCH_KEYBOARD_ENABLE_KEY defaultValue:TRUE];
}

-(void)setCustomerSearchKeyboardEnable:(BOOL)value {
	return [self setBool:value forKey:CUSTOMER_SEARCH_KEYBOARD_ENABLE_KEY];
}


-(BOOL)itemSearchKeyboardEnable {
	return [self boolForKey:ITEM_SEARCH_KEYBOARD_ENABLE_KEY defaultValue:TRUE];
}

-(void)setItemSearchKeyboardEnable:(BOOL)value {
	return [self setBool:value forKey:ITEM_SEARCH_KEYBOARD_ENABLE_KEY];
}

-(BOOL)giftBalanceKeyboardEnable {
	return [self boolForKey:GIFT_BALANCE_KEYBOARD_ENABLE_KEY defaultValue:TRUE];
}

-(void)setGiftBalanceKeyboardEnable:(BOOL)value {
	return [self setBool:value forKey:GIFT_BALANCE_KEYBOARD_ENABLE_KEY];
}

-(BOOL)sellGiftCardKeyboardEnable {
	return [self boolForKey:SELL_GIFTCARD_KEYBOARD_ENABLE_KEY defaultValue:TRUE];
}
-(void)setSellGiftCardKeyboardEnable:(BOOL)value { [self setBool:value forKey: SELL_GIFTCARD_KEYBOARD_ENABLE_KEY]; }


-(BPUUID*)locationId { return [self uuidForKey:LOCATION_ID_KEY]; }
-(void)setLocationId:(BPUUID*)value { [self setUuid:value forKey:LOCATION_ID_KEY]; }

-(BPUUID*)workstationId { return [self uuidForKey:WORKSTATIION_ID_KEY]; }
-(void)setWorkstationId:(BPUUID*)value { [self setUuid:value forKey:WORKSTATIION_ID_KEY]; }

-(BOOL)initStatus { return [self boolForKey:INIT_STATUS_KEY];}
-(void)setInitStatus:(BOOL)value { [self setBool:value forKey:INIT_STATUS_KEY]; }


-(NSString*)iPadId { return [self stringForKey:IPAD_ID_KEY]; }
-(void)setIPadId:(NSString *)value { [self setString:value forKey:IPAD_ID_KEY]; }

-(NSString*)printingServer { return [self stringForKey:PRINTING_IP_KEY]; }
-(void)setPrintingServer:(NSString *)value { [self setString:value forKey:PRINTING_IP_KEY]; }

-(NSInteger)printingServerPort { return [self intForKey:PRINTING_PORT_KEY]; }
-(void)setPrintingServerPort:(NSInteger)value { [self setInt:value forKey:PRINTING_PORT_KEY]; }

-(NSString*)scaleServerAddress { return [self stringForKey:SCALE_IP_KEY]; }
-(void)setScaleServerAddress:(NSString *)value { [self setString:value forKey:SCALE_IP_KEY]; }

-(NSInteger)scaleServerPort { return [self intForKey:SCALE_PORT_KEY]; }
-(void)setScaleServerPort:(NSInteger)value { [self setInt:value forKey:SCALE_PORT_KEY]; }

-(NSString*)SVSAccessToken { return [self stringForKey:SVS_ACCESS_TOKEN]; }
-(void)setSVSAccessToken:(NSString *)value { [self setString:value forKey:SVS_ACCESS_TOKEN]; }



-(NSInteger)richCatalogSegment1Index {
    return [self intForKey:RICH_CATALOG_SEGMENT1_INDEX defaultValue:1];
}

-(void)setRichCatalogSegment1Index:(NSInteger)richCatalogSegment1Index {
    [self setInt:richCatalogSegment1Index forKey:RICH_CATALOG_SEGMENT1_INDEX];
}

-(NSString *)richCatalogCategoriesState {
    return [self stringForKey:RICH_CATALOG_CATEGORIES_STATE defaultValue:nil];
}

-(void)setRichCatalogCategoriesState:(NSString *)richCatalogCategoriesState {
    [self setString:richCatalogCategoriesState forKey:RICH_CATALOG_CATEGORIES_STATE];
}
-(NSString*)deviceIP{
    return [self stringForKey:DEVICE_IP defaultValue:nil];
}
-(void)setDeviceIP:(NSString *)deviceIP{
    [self setString:deviceIP forKey:DEVICE_IP];
}
-(NSString*)deviceNotes{
    return [self stringForKey:DEVICE_NOTES defaultValue:nil];
}
-(void)setDeviceNotes:(NSString *)deviceNotes{
    [self setString:deviceNotes forKey:DEVICE_NOTES];
}


-(void)resetCache {
    
    [_settingsList release];
    _settingsList = nil;
}

-(void)saveGlobalValues {
    
    [_globalList removeAllObjects];
    
    NSString *serverIdKey = [NSString stringWithFormat:@"%@_", DEVICE_SERVER_ID_KEY];
    
    for (NSString *key in self.settingsList.keyEnumerator) {
        
        if (!((AppSetting *)self.settingsList[key]).value)
            continue;
        
        if ([key isEqualToString:LAST_RECEIPT_NUMBER_KEY]
            || [key isEqualToString:INIT_STATUS_KEY]
            || [key isEqualToString:APP_LANGUAGE])
            _globalList[key] = ((AppSetting *)self.settingsList[key]).value;
        else if (!self.initStatus
                 && ([key isEqualToString:DEVICE_UNIQUE_ID_KEY]
                     || [key isEqualToString:SERVER_URL]
                     || [key isEqualToString:DEFAULT_SERVER_CODE]))
            _globalList[key] = ((AppSetting *)self.settingsList[key]).value;
        else {

            NSRange range = [key rangeOfString:serverIdKey], range2 = [key rangeOfString:APP_VERSION];
            if (range.length > 0 || range2.length > 0)
                _globalList[key] = ((AppSetting *)self.settingsList[key]).value;
        }
    }
}

-(void)restoreGlobalValues {
    
    for (NSString *key in _globalList.keyEnumerator)
        [self setString:_globalList[key] forKey:key];
}


-(void)updateSettingsWithDevice:(Device *)pdevice {
    
    @synchronized(self) {
        
        NSString *prevIPadId = [self.iPadId retain];
        self.iPadId = [NSString stringWithFormat:@"%ld", (long)pdevice.deviceNo];
        self.deviceServerId = pdevice.id;
        
        if (![prevIPadId isEqualToString:self.iPadId]) {
            dispatch_sync(dispatch_get_main_queue(), ^{
                //[[StatusViewController sharedInstance] updateDeviceIdTitle];
                [SuperViewController50.instance.statusViewController updateDeviceIdTitle];
            });
        }
        [prevIPadId release];
        
        self.printingServer = pdevice.printServerIP;
        self.printingServerPort = pdevice.printerPort;
        self.scaleServerAddress = pdevice.printServerIP;
        self.scaleServerPort = pdevice.scalesPort;
        
        self.deviceServerId = pdevice.id;
        self.locationId = pdevice.locationId;
        self.locationDeviceCode = pdevice.locationDeviceCode;
        self.deviceIP = pdevice.IPAddress;
        self.deviceNotes = pdevice.notes;
        
        if (pdevice.SVSAuthToken)
            self.SVSAccessToken = pdevice.SVSAuthToken;
        
        [self updateDeviceSettings:pdevice];
    }
}


-(void)updateDeviceSettings:(Device *)pdevice {
    
    if (!pdevice)
        return;
    
    if (!_isDeviceSettingsLoaded)
        [self loadDeviceSettings];
    
    if (_pushLogsToGae != pdevice.pushLogsToGae) {
        _pushLogsToGae = pdevice.pushLogsToGae;
        [self setBool:self.pushLogsToGae forKey:PUSH_LOGS_TO_GAE];
    }
    
    if (_gaeLogSeverity != pdevice.gaeLogSeverity) {
        
        _gaeLogSeverity = pdevice.gaeLogSeverity;
        [self setInt:self.gaeLogSeverity forKey:GAE_LOG_SEVERITY];
    }
    
    if (_collectNetworkStats != pdevice.collectNetworkStats) {
        _collectNetworkStats = pdevice.collectNetworkStats;
        [self setBool:self.collectNetworkStats forKey:COLLECT_NETWORK_STATS];
    }
    
    if (_isMultiWorkstation != pdevice.isMultiWorkstation) {
        
        _isMultiWorkstation = pdevice.isMultiWorkstation;
        [self setBool:self.isMultiWorkstation forKey:IS_MULTI_WORKSTATION];
    }
}


-(void)loadDeviceSettings {
    
    _isDeviceSettingsLoaded = YES;
    
    _pushLogsToGae = [self boolForKey:PUSH_LOGS_TO_GAE defaultValue:NO];
    _gaeLogSeverity = [self intForKey:GAE_LOG_SEVERITY defaultValue:0];
    _collectNetworkStats = [self boolForKey:COLLECT_NETWORK_STATS defaultValue:NO];
    _isMultiWorkstation = [self boolForKey:IS_MULTI_WORKSTATION defaultValue:NO];
}


-(NSString*)scaleName { return [self stringForKey:SCALE_NAME_KEY]; }
-(void)setScaleName:(NSString *)value { [self setString:value forKey:SCALE_NAME_KEY]; }


-(BOOL)isDisplayItemImage { return [self boolForKey:IMAGE_PREFERENCE_KEY defaultValue:YES]; }
-(void)setIsDisplayItemImage:(BOOL)value { [self setBool:value forKey:IMAGE_PREFERENCE_KEY]; }

-(ItemDescriptionModeView)itemDescriptionModeView { return [self intForKey:DESCRIPTION_PREFERENCE_KEY defaultValue:1]; }
-(void)setItemDescriptionModeView:(ItemDescriptionModeView)value { [self setInt:value forKey:DESCRIPTION_PREFERENCE_KEY]; }

-(ItemAttributeModeView)itemAttributeModeView { return [self intForKey:ATTRIBUTE_PREFERENCE_KEY defaultValue:1]; }
-(void)setItemAttributeModeView:(ItemAttributeModeView)value { [self setInt:value forKey:ATTRIBUTE_PREFERENCE_KEY]; }

-(void)setItemSettingDisplayImage:(BOOL)displayImage descriptionMode:(ItemDescriptionModeView)descriptionMode attributeMode:(ItemAttributeModeView)attributeMode {
    
    self.isDisplayItemImage = displayImage;
    self.itemDescriptionModeView = descriptionMode;
    self.itemAttributeModeView = attributeMode;
}

-(NSString *)itemSearchKey { return [NSString stringWithFormat:@"%@", ITEM_SEARCH_TYPE]; }

-(BPUUID *)deviceServerId {
    
    NSString *serverIdKey = [NSString stringWithFormat:@"%@_%@", DEVICE_SERVER_ID_KEY, [self.serverUrl uppercaseString]];
    BPUUID *result = [self uuidForKey:serverIdKey];
    if (!result){
        serverIdKey = [NSString stringWithFormat:@"%@_%@", DEVICE_SERVER_ID_KEY, [self.defaultServerCode uppercaseString]];
        result = [self uuidForKey:serverIdKey];
    }
    return result;
}
-(void)setDeviceServerId:(BPUUID *)value {
    
    NSString *serverIdKey = [NSString stringWithFormat:@"%@_%@", DEVICE_SERVER_ID_KEY, [self.serverUrl uppercaseString]];
    [self setUuid:value forKey:serverIdKey];
}


-(NSInteger)locationDeviceCode {
    
    NSString *serverIdKey = [NSString stringWithFormat:@"%@_%@", DEVICE_LOCATION_CODE_KEY, [self.serverUrl uppercaseString]];
    return [self intForKey:serverIdKey];
}
-(void)setLocationDeviceCode:(NSInteger)value {
    
    NSString *serverIdKey = [NSString stringWithFormat:@"%@_%@", DEVICE_LOCATION_CODE_KEY, [self.serverUrl uppercaseString]];
    [self setInt:value forKey:serverIdKey];
}


- (BPUUID *)deviceUniqueId {
    
    BPUUID *duid = [self uuidForKey:DEVICE_UNIQUE_ID_KEY];
    if (!duid) {
        
        duid = [BPUUID UUID];
        [self setUuid:duid forKey:DEVICE_UNIQUE_ID_KEY];
    }
    return duid;
}
-(void)setDeviceUniqueId:(BPUUID *)value {
    [self setUuid:value forKey:DEVICE_UNIQUE_ID_KEY];
}


-(NSString*)serverUrl { return [self stringForKey:SERVER_URL]; }
-(NSString*)serverApiKey { return [self stringForKey:SERVER_API_KEY]; }
-(NSString *)geniusIp { return [self stringForKey:GENIUS_IP]; }
-(NSString *)cayanTerminalId { return [self stringForKey:CAYAN_TERMINAL_ID]; }

-(void)setServerUrl:(NSString *)value { [self setString:value forKey:SERVER_URL]; }
-(void)setServerApiKey:(NSString *)value { [self setString:value forKey:SERVER_API_KEY]; }
-(void)setGeniusIp:(NSString *)geniusIp { [self setString:geniusIp forKey:GENIUS_IP]; }
-(void)setCayanTerminalId:(NSString *)cayanTerminalId { [self setString:cayanTerminalId forKey:CAYAN_TERMINAL_ID]; }

-(NSString*)defaultServerCode { return [self stringForKey:DEFAULT_SERVER_CODE defaultValue:@"Demo"]; }
-(void)setDefaultServerCode:(NSString *)value { [self setString:value forKey:DEFAULT_SERVER_CODE]; }

-(NSString*)pluginXML { return [self stringForKey:PLUGIN_XML]; }
-(void)setPluginXML:(NSString *)value { [self setString:value forKey:PLUGIN_XML]; }

-(BOOL)payworksSignOnShopper { return [self boolForKey:PAYWORKS_SIGN_ON_SHOPPER]; }
-(void)setPayworksSignOnShopper:(BOOL)value { [self setBool:value forKey:PAYWORKS_SIGN_ON_SHOPPER]; }

-(NSDate*)lastSyncDate {
	return [self dateForKey:LastSyncDateKey];
}

-(void)setLastSyncDate:(NSDate*)value {
    @synchronized (self) {
        return [self setDate:value forKey:LastSyncDateKey];
    }
}

-(NSInteger)salesOrderNextNumber{
    KeychainItemWrapper *wrapper = [[KeychainItemWrapper alloc] initWithIdentifier:NEXT_SALES_ORDER_NUMBER_KEY accessGroup:nil];
    NSString *str=[wrapper objectForKey:(id)kSecAttrAccount];
    [wrapper release];
    if (str.length)
        return [str integerValue];
    return 1;
}

-(void)setSalesOrderNextNumber:(NSInteger)value {
    KeychainItemWrapper *wrapper = [[KeychainItemWrapper alloc] initWithIdentifier:NEXT_SALES_ORDER_NUMBER_KEY accessGroup:nil];
    [wrapper setObject:[NSString stringWithFormat:@"%ld",(long)value] forKey:(NSString*)kSecAttrAccount];
    [wrapper release];
}

//svs settings
-(NSDecimalNumber*)svs_LRPThreshold{
    return [self decimalNumberForKey:@"svs_LRPThreshold"];
}

-(void)setSvs_LRPThreshold:(NSDecimalNumber *)value{
    [self setDecimalNumber:value forKey:@"svs_LRPThreshold"];
}

-(BOOL)svs_FreshAddressEnabled{
    return [self boolForKey:@"svs_FreshAddressEnabled"];
}

-(void)setSvs_FreshAddressEnabled:(BOOL)value{
    [self setBool:value forKey:@"svs_FreshAddressEnabled"];
}

-(BOOL)svs_TokenEnabled{
    return [self boolForKey:@"svs_TokenEnabled"];
}

-(void)setSvs_TokenEnabled:(BOOL)value{
    [self setBool:value forKey:@"svs_TokenEnabled"];
}

-(BOOL)svs_CrmLRPCalculationEnabled{
    return [self boolForKey:@"svs_CrmLRPCalculationEnabled"];
}

-(void)setSvs_CrmLRPCalculationEnabled:(BOOL)value{
    [self setBool:value forKey:@"svs_CrmLRPCalculationEnabled"];
}


-(NSDate*)richMediaStorageLastClearDate{
    return [self dateForKey:CLEAN_RICH_MEDIA_STORAGE_LAST_DATE_KEY];
}
-(void)setRichMediaStorageLastClearDate:(NSDate *)richMediaStorageLastClearDate{
    [self setDate:richMediaStorageLastClearDate forKey:CLEAN_RICH_MEDIA_STORAGE_LAST_DATE_KEY];
}


-(NSInteger)deviceState {
    
    @synchronized(self) {
        
        if (_deviceState >= 0)
            return _deviceState;
        
        _deviceState = [self intForKey:DEVICE_STATE defaultValue:-1];
        return _deviceState >= 0 ? _deviceState : DeviceAgentStateUnknown;
    }
}

-(void)setDeviceState:(NSInteger)value {
    
    @synchronized(self) {
        
        if (_deviceState == value)
            return;
        
        _deviceState = value;
        [self setInt:value forKey:DEVICE_STATE];
    }
}


-(NSString*)deviceLanguage { return [self stringForKey:APP_LANGUAGE]; }
-(void)setDeviceLanguage:(NSString*)value { return [self setString:value forKey:APP_LANGUAGE]; }


-(NSString*)productBuilderData { return [self stringForKey:@"ProductBuilderData"]; }
-(void)setProductBuilderData:(NSString*)value { return [self setString:value forKey:@"ProductBuilderData"]; }

-(BOOL)closeBarcodeReaderOnGoodScan { return [self boolForKey:CLOSE_BARCODE_READE_ON_GOOD_SCAN defaultValue:YES]; }
-(void)setCloseBarcodeReaderOnGoodScan:(BOOL)value { return [self setBool:value forKey:CLOSE_BARCODE_READE_ON_GOOD_SCAN]; }


-(BOOL)pushLogsToGae {
    
    if (!_isDeviceSettingsLoaded)
        [self loadDeviceSettings];
    
    return _pushLogsToGae;
}

-(NSInteger)gaeLogSeverity {
    
    if (!_isDeviceSettingsLoaded)
        [self loadDeviceSettings];
    
    return _gaeLogSeverity;
}

-(BOOL)collectNetworkStats {
    if (!_isDeviceSettingsLoaded)
        [self loadDeviceSettings];
    return _collectNetworkStats;
}

-(BOOL)isMultiWorkstation; {
    
    if (!_isDeviceSettingsLoaded)
        [self loadDeviceSettings];
    
    return _isMultiWorkstation;
}

-(BPUUID*)fillFromLocationId {
    //return [self uuidForKey:@"fillFromLocationId"];
    return _fillFromLocationId;
}

-(void)setFillFromLocationId:(BPUUID*)value {
    //[self setUuid:value forKey:@"fillFromLocationId"];
    [value retain];
    [_fillFromLocationId release];
    _fillFromLocationId = value;
    [[NSNotificationCenter defaultCenter] removeObserver:self name:SecurityManagerCurrentEmployeeDidChange object:nil];
    if (_fillFromLocationId){
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleEmployeeDidChange) name:SecurityManagerCurrentEmployeeDidChange object:nil];
    }
}

-(void)handleEmployeeDidChange{
    if (![SecurityManager isUserInRole:SecurityManager.currentEmployee.id role:UserRoleSalesReceiptsAddAnotherLocation]){
        self.fillFromLocationId = nil;
    }
}


#pragma mark Singleton Methods

-(id)init{
	if ((self=[super init])){
        
        _globalList = [[NSMutableDictionary alloc] init];
        _deviceState = -1;
	}
	return self;
}

+(AppSettingManager *)instance {
    static dispatch_once_t pred;
    static AppSettingManager *instance = nil;
    
    dispatch_once(&pred, ^{
        instance = [[AppSettingManager alloc] init];
    });
    return instance;
}


-(void)updateVersionData {
    
    NSString *currentAppVersion = [self stringForKey:CURRENT_APP_VERSION];
    NSString *versionNumber = [[NSBundle mainBundle] infoDictionary][@"CFBundleVersion"];
    
    if ([versionNumber isEqualToString:currentAppVersion])
        return;
    
    [self setString:versionNumber forKey:CURRENT_APP_VERSION];
    NSString *appInfo = [NSString stringWithFormat:@"DB version - %@, deveice name - %@,  device model - %@, system version - %@, hardware - %@",
                         CURRENT_DB_VERSION,
                         AppParameterManager.instance.deviceName,
                         AppParameterManager.instance.deviceModel,
                         AppParameterManager.instance.deviceSystemVersion,
                         AppParameterManager.instance.hardwareDescription];
    [self setString:appInfo forKey:[APP_VERSION stringByAppendingString:versionNumber]];
}

-(NSString *)appVersionDetails {
    NSString *versionNumber = [self stringForKey:CURRENT_APP_VERSION defaultValue:[[NSBundle mainBundle] infoDictionary][@"CFBundleVersion"]];
    return [self stringForKey:[APP_VERSION stringByAppendingString:versionNumber] defaultValue:@"N/A"];
}
-(NSString *)lastSyncDateString {
    CGFloat timeInterval = 0;
    
    @synchronized(self) {
        
        timeInterval = [[NSDate date]timeIntervalSinceDate:self.lastSyncDate];
    }
    
    NSString* description = nil;
    
    if (isnan(timeInterval))
        description = NSLocalizedString(@"SYNC_STATUS_UNKNOWN", @"Unknown");
    else if (timeInterval < 60)
        description = NSLocalizedString(@"SYNC_STATUS_LESS_THEN1", @"Less than 1 minute ago");
    else{
        
        if (timeInterval >=60 && timeInterval<60*120)
            description = [NSString stringWithFormat:NSLocalizedString(@"SYNC_STATUS_MIN_AGO", @"%i minutes ago"), (NSInteger)timeInterval/60];
        else if (timeInterval>60*120 && timeInterval<60*60*24)
            description = [NSString stringWithFormat:NSLocalizedString(@"SYNC_STATUS_HOURS_AGO", @"%i hours ago"), (NSInteger)timeInterval/60/60];
        else
            description = [NSString stringWithFormat:NSLocalizedString(@"SYNC_STATUS_DAYS_AGO", @"%i days ago"), (NSInteger)timeInterval/60/60/24];
    }
    return description;
}

- (id)retain {
	return self;
}

- (NSUInteger)retainCount {
	return UINT_MAX; //denotes an object that cannot be released
}

- (oneway void)release {
	// never release
}

- (id)autorelease {
	return self;
}

- (void)dealloc {
    
	// Should never be called, but just here for clarity really.
    [_globalList release];
	[_settingsList release];
    [_initializedLocation release];
    [_fillFromLocationId release];
    [super dealloc];
}

@end

