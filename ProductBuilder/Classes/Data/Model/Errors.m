//
//  Errors.m
//  iPadPOS
//
//  Created by valera on 5/16/11.
//  Copyright 2011 DBBestTechnologies. All rights reserved.
//

#import "Errors.h"


@implementation Errors

-(id)init:(id)_base{
    if ((self = [super init])){
        _errors = [[NSMutableDictionary alloc] init];
        base = _base;
    }
	return self;
}

-(void)dealloc{
    [_errors release];
    [super dealloc];
}

-(void)add:(NSString *)key error:(NSString*)error{
	NSMutableArray *keyErrors = _errors[key];
	if (!keyErrors) {
		keyErrors = [[NSMutableArray alloc] init];
        _errors[key] = keyErrors;
        [keyErrors release];
	}
	[keyErrors addObject:error];
}

- (void)setObject:(NSString*)error forKeyedSubscript:(NSString*)key{
    [self add:key error:error];
}

-(NSArray*)objectForKeyedSubscript:(NSString*)key{
    return [self errorsForKey:key];
}

-(NSArray*)errorsForKey:(NSString*)key{
    return  _errors[key];
}

-(void)removeErrorsForKey:(NSString*)key{
    [_errors removeObjectForKey:key];
}

-(NSArray*)allKeys{
    return _errors.allKeys;
}

-(NSString*)description{
    NSMutableString* message = [[[NSMutableString alloc]init] autorelease];
    [_errors enumerateKeysAndObjectsUsingBlock:^(NSString* key, NSMutableArray* keyErrors, BOOL *stop) {

        for(NSString* errMessage in keyErrors){
           if (message.length){
                [message appendString:@"\n"];
           }
           [message appendString:errMessage];
        }
    }];
    return message;
}

-(NSUInteger)count {
	return [_errors count];
}

-(void)clear {
	[_errors removeAllObjects];
}

- (NSUInteger)countByEnumeratingWithState:(NSFastEnumerationState *)state objects:(id __unsafe_unretained [])buffer count:(NSUInteger)len{
    return [_errors countByEnumeratingWithState:state objects:buffer count:len];
}

@end

