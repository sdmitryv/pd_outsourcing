//
//  EmployeeList.h
//  CloudworksPOS
//
//  Created by Vitaliy Gervazuk on 8/22/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LazyLoadingController.h"
#import "IObjectList.h"

@class Employee;

extern NSString * const employeeSelectStm;
extern NSString * const employeeSelectAllStm;

@interface EmployeeList : NSObject<ILazyLoadableList, IObjectList, NSFastEnumeration> {
    LazyLoadingController* _lzController;
	SqlCommand* _command;
}

-(Employee*)objectByRowid:(NSNumber *)rowid;
-(NSUInteger)indexOfObject:(Employee*)item;
-(NSUInteger) count;
@property (nonatomic, retain) SqlFilterCondition* filterCondition;
@property (nonatomic, retain) NSArray* sortOrderList;
@property (nonatomic,readonly) LazyLoadingController* lzController;

-(id)init;
-(id)init:(BOOL)isAll;

@end
