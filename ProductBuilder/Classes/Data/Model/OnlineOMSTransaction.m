//
//  OnlineOMSTransaction.m
//  ProductBuilder
//
//  Created by Alexander Martyshko on 1/9/15.
//  Copyright (c) 2015 Cloudworks. All rights reserved.
//

#import "OnlineOMSTransaction.h"

@implementation OnlineOMSTransaction

- (void)dealloc {
    [_receiptId release];
    [_orderId release];
    [_firstItemDateTime release];
    [_preConfirmDateTime release];
    [_confirmDateTime release];
    [super dealloc];
}

+ (OnlineOMSTransaction *)onlineOMSTransactionForReceiptWithId:(BPUUID *)receiptId {
    return nil;
}

@end
