//
//  CEntity.h
//  ProductBuilder
//
//  Created by Valera on 9/2/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CROEntity.h"
#import "Validatable.h"
#import "NSObject+MethodExchange.h"
#import "DBAdditions.h"

#define DECLARE_PROPERTIES(...) + (NSArray *)getPropertiesList \
{ \
return [NSArray arrayWithObjects: \
__VA_ARGS__ \
, nil]; \
}
#define DECLARE_PROPERTY(n,t) [NSArray arrayWithObjects:n, t, nil]

@protocol IPropertyChangedReceiver
-(void)propertyChanged:(id)sender keyPath:(NSString*)keyPath value:(id)value;
@end

@protocol EditableObject
-(BOOL)canEdit;
-(void)beginEdit;
-(void)cancelEdit;
-(void)endEdit;
-(NSUInteger)editLevel;
-(BOOL)isCancellingEdit;
-(BOOL)readOnly;
-(BOOL)canEdit:(NSError**)error;
-(void)beginIgnoringReadonly;
-(void)endIgnoringReadonly;
-(BOOL)copyEditLevelsToEntity:(NSObject<EditableObject>*)toEntity;
@end

@protocol CEntity<CROEntity, Validatable, IPropertyChangedReceiver, ISaveable, EditableObject>
- (void)markClean;
- (void)markDirty;
- (void)markOld;
- (void)markNew;
- (void)markDeleted;
- (BOOL)isDirty;
- (BOOL)isNew;
- (BOOL)isDeleted;
-(void)setIsDeleted:(BOOL)value;
-(BOOL)save;
-(BOOL)save:(NSError**)error;
-(void)subscribeReceiver:( NSObject<IPropertyChangedReceiver>*)receiver;
-(void)unSubscribeReceiver:( NSObject<IPropertyChangedReceiver>*)receiver;
@property(nonatomic,readonly)BOOL readOnly;
-(NSError*)readonlyError;
@end

@interface CEntity : CROEntity<CEntity/*, NSCoding*/> {
@protected
	Errors *_errors;
	NSMutableArray* propertyChangedReceivers;
    NSMutableArray* _savedStates;
@private
    NSInteger        _isCancellingEdit;
	BOOL		_isDirty;
	BOOL		_isNew;
	BOOL		_isDeleted;
	BOOL volatile _observingProps;
    NSUInteger        _editLevel;
    NSInteger _ignoringReadonly;
}
+ (NSSet *)properties;

-(void)initDefaults;
-(void)validate;

-(void)propertyChanged:(NSString*)keyPath;
-(void)propertyChanged:(NSString*)keyPath value:(id)value;
//IPropertyChangedReceiver implementation
-(void)propertyChanged:(id)sender keyPath:(NSString*)keyPath value:(id)value;

-(NSNumber*)syncState;

+(BOOL)writeError:(NSError**)error description:(NSString*)description domain:(NSString*)domain;
+(BOOL)writeError:(NSError**)error description:(NSString*)description domain:(NSString*)domain code:(NSInteger)code;
+(BOOL)writeError:(NSError**)error title:(NSString*)title description:(NSString*)description domain:(NSString*)domain;
+(BOOL)writeError:(NSError**)error title:(NSString*)title description:(NSString*)description domain:(NSString*)domain code:(NSInteger)code;
+(BOOL)writeError:(NSError**)error title:(NSString*)title description:(NSString*)description domain:(NSString*)domain code:(NSInteger)code additionalInfo:(NSDictionary*)additionalInfo;
- (BOOL)shouldEncodeProperty:(NSString *)name;
-(void)installPropertyObservers;
-(void)removePropertyObservers;
@end
