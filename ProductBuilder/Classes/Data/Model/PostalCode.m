//
//  PostalCode.m
//  CloudworksPOS
//
//  Created by Lulakov Viacheslav on 7/14/11.
//  Copyright 2011 Cloudworks. All rights reserved.
//

#import "PostalCode.h"

@implementation PostalCode

@synthesize postalCode = _postalCode;
@synthesize city = _city;
@synthesize state = _state;
@synthesize country = _country;
@synthesize stateCode = _stateCode;
@synthesize countryCode = _countryCode;

-(void) dealloc
{
    [_postalCode release];
    [_city release];
    [_state release];
    [_country release];
    [_stateCode release];
    [_countryCode release];
	
	[super dealloc];
}

- (NSString *)description {
    return _postalCode;
}

+(PostalCode *)getPostalCode:(NSString *)code{
    return nil;
}

@end
