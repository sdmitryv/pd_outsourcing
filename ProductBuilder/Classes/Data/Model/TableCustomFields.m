//
//  TableCustomFields.m
//  CloudworksPOS
//
//  Created by
//  Copyright 2015 __MyCompanyName__. All rights reserved.
//

#import "TableCustomFields.h"

@implementation TableCustomFields

@synthesize fieldsList = _fieldsList;
@synthesize tableName = _tableName;

-(id)initForTable:(NSString *)table {
    
    self = [super init];
    
    if (self) {
    
        _tableName = [table retain];
        _fieldsList = [[CEntityList alloc] initWithOwner:self];
    }
    
    return self;
}


-(void)dealloc {
    
    [_fieldsList release];
    [_tableName release];
    
    [super dealloc];
}


-(void)loadFields {
}

+(void)resetCache{
}

-(TableCustomField*)getFieldByCode:(NSString *)fieldCode {
    
    for (TableCustomField *field in _fieldsList.list)
        if ([field.code isEqualToString:fieldCode])
            return field;
    
    return nil;
}

@end
