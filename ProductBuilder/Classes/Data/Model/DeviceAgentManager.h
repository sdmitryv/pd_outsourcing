//
//  DeviceAgentManager.h
//  DeviceAgent
//
//  Created by Alexander Martyshko on 2/28/14.
//  Copyright (c) 2014 Cloudworks. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BPUUID.h"
#import <CoreLocation/CoreLocation.h>

@class Device;

typedef NS_ENUM(NSUInteger, DeviceAgentState) {
    DeviceAgentStateOk = 0,
    DeviceAgentStateNoSuchDevice = 1,
    DeviceAgentStateDeactivated = 2,
    DeviceAgentStateNotAuthorizedApp = 3,
    DeviceAgentStateAgentIdMismatch = 4,
    DeviceAgentStateDeviceIdMismatch = 5,
    DeviceAgentStateLocationIdMismatch = 6,
    DeviceAgentStateUnknown = 7
};

@interface DeviceAgentManager : NSObject {

    NSString *_appVersion;
    NSString *_appBundleId;
    Device *_currentDevice;
}

@property (atomic, readonly) NSString *appVersion;
@property (atomic, assign) DeviceAgentState deviceAgentState;
@property (atomic, readonly) NSString *deviceStateStringRepresentation;
@property (nonatomic, retain) CLLocation *currentLocation;
@property (atomic, retain) Device *currentDevice;

+(DeviceAgentManager *)sharedInstance;

- (void)writeServerAddress:(NSString *)address;
- (void)writeAppVersion;

- (BOOL)isDeviceActive;
- (BOOL)checkIsDeviceActive;
- (BOOL)checkIsDeviceActive:(void (^)(NSInteger buttonIndex))aCompletedBlock;

@end
