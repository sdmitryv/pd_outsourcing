//
//  Department.h
//  ProductBuilder
//
//  Created by Valera on 10/12/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CEntity.h"

@interface Department : CROEntity {
	NSString* name;
	NSUInteger level;
	Department* parent;
	NSMutableArray *subLevels;
	NSNumber *classificationType;
}
@property(nonatomic,retain)NSString* name;
@property(nonatomic,assign)NSUInteger level;
@property(nonatomic,retain)Department* parent;
@property(nonatomic,readonly)NSArray* subLevels;
@property(nonatomic,retain)NSNumber* classificationType;
+(NSArray*)getDepartments:(BOOL)alt;
@end
