//
//  CurrencyDenomination.h
//  DrawerMemo
//
//  Created by Alexander Martyshko on 11/22/12.
//  Copyright (c) 2012 Cloudworks. All rights reserved.
//

#import "CEntity.h"

@interface CurrencyDenomination : CEntity {
    NSString *description;
}

@property (nonatomic, retain) BPUUID *currencyID;
@property (nonatomic, retain) NSString *description;
@property (nonatomic, assign) NSDecimal value;
@property (nonatomic, assign) NSInteger listOrder;
@property (nonatomic, assign) NSInteger status;
@property (nonatomic, retain) NSString *code;
@property (nonatomic, assign) BOOL isQuickTender;

+(NSArray *)getDenominationByCurrency:(BPUUID *)currencyId quickTenderOnly:(BOOL)quickTenderOnly;

@end
