//
//  Certificate.m
//  ProductBuilder
//
//  Created by Виталий Гервазюк on 3/16/12.
//  Copyright (c) 2012 Cloudworks. All rights reserved.
//

#import "Certificate.h"
#import "SettingManager.h"

@implementation Certificate

-(NSData *)encryptValue:(NSString *)value {
    if (!value) return nil;
    
    SecCertificateRef cert = SecCertificateCreateWithData(NULL, (CFDataRef)_fileContent);
    SecPolicyRef policy = SecPolicyCreateBasicX509();
    
    SecCertificateRef certArray[1] = { cert };
    CFArrayRef certs = CFArrayCreate(
                                       NULL, (void *)certArray,
                                       1, NULL);
    SecTrustRef myTrust;
    OSStatus status = SecTrustCreateWithCertificates(
                                                     certs,
                                                     policy,
                                                     &myTrust);
    SecTrustResultType trustResult;
    if (status == noErr) {
        SecTrustEvaluate(myTrust, &trustResult);
    }
    else {
        NSLog(@"Status: %d", (int)status);
    }
    
    SecKeyRef publicKey = SecTrustCopyPublicKey(myTrust);
    
    NSData *someData = [value dataUsingEncoding:NSUTF8StringEncoding];
    uint8_t* nonce = (uint8_t*)[someData bytes];
    
    //  Allocate a buffer
    size_t cipherBufferSize = SecKeyGetBlockSize(publicKey);
    @try{
        
        //  Error handling
        
        if (cipherBufferSize < sizeof(nonce)) {
            // Ordinarily, you would split the data up into blocks
            // equal to cipherBufferSize, with the last block being
            // shorter. For simplicity, this example assumes that
            // the data is short enough to fit.
            NSLog(@"%s", "Could not encrypt.  Packet too large.");
            return nil;
        }
        uint8_t *cipherBuffer = malloc(cipherBufferSize);
        
        // Encrypt using the public.
        //status = 
        SecKeyEncrypt(publicKey,
                               kSecPaddingPKCS1,
                               nonce,
                               [someData length],
                               cipherBuffer,
                               &cipherBufferSize
                               );

        Byte * revertedData = malloc(cipherBufferSize);
        
        for (NSInteger j = 0, i = cipherBufferSize - 1; i >= 0; i--)
        {
            revertedData[j++]=cipherBuffer[i];
        }
        if(publicKey) CFRelease(publicKey);
        if (myTrust) CFRelease(myTrust);
        if (cert) CFRelease(cert);
        if (cipherBuffer) free(cipherBuffer);
    
        NSData* rData = [NSData dataWithBytes:revertedData length:cipherBufferSize];
        free(revertedData);
        return rData;
    }
    @finally{
        if (certs) CFRelease(certs);
        if (policy) CFRelease(policy);
    }
}

+(Certificate *)getLatesCertificateByName:(NSString *)name {
    return nil;
}

+(NSData*)ecnryptString:(NSString*)string {
    NSString* certName = [[SettingManager instance] rsaCertificateName];
    
    Certificate* cert = [Certificate getLatesCertificateByName:certName];
    
    NSData* result = [cert encryptValue:string];
    
    return result;
}

-(void)dealloc {
    [_fileName release];
    [_fileContent release];
    [super dealloc];
}

@end
