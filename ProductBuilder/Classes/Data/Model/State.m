//
//  State.m
//  ProductBuilder
//
//  Created by Alexander Martyshko on 1/16/15.
//  Copyright (c) 2015 Cloudworks. All rights reserved.
//

#import "State.h"

@implementation State

- (void)dealloc {
    [_code release];
    [_name release];
    [_countryId release];
    [super dealloc];
}

+(NSArray *)getList {
    return nil;
}

-(NSString *)description {
    return _name;
}

@end
