//
//  CROEntity.h
//  ProductBuilder
//
//  Created by Valera on 9/13/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#if (! TARGET_OS_IPHONE)
#import <objc/objc-runtime.h>
#else
#import <objc/runtime.h>
#import <objc/message.h>
#endif
#import "BPUUID.h"
#import "ILazyLoadableList.h"

@protocol ISaveable

- (BOOL)save;
- (BOOL)save:(NSError**)error;
- (BOOL)validateOnSave:(NSError**)error;
@end

@protocol CROEntity <NSObject>
-(BPUUID*)id;
-(NSNumber*)rowid;
-(NSDate*) recCreated;
-(NSDate*) recModified;
-(NSString*) customRecId;
+(instancetype)getInstanceById:(BPUUID*)id;
-(void)initInternal;
-(id)initWithRow:(id)row;
-(void)refresh;
@end

@interface CROEntity : NSObject <CROEntity, ILazyItem, NSCopying> {
@protected
	NSDate	* _recCreated;
	NSDate	* _recModified;
	BPUUID*	  _id;
	NSNumber *_rowid;
    NSString *_customRecId;
}

@property (nonatomic,retain) BPUUID *id;
@property (nonatomic,retain) NSNumber *rowid;
@property (nonatomic, retain) NSDate	 * recCreated;
@property (nonatomic, retain) NSDate	 * recModified;
@property (nonatomic,retain) NSString *customRecId;
-(NSDictionary*)getProperties;
-(NSDictionary *)getJSONProperties;
+(NSArray*)list;
+(NSArray*)listWithQuery:(NSString*)query;
-(id)initWithEntity:(CROEntity*)entity;
@end
