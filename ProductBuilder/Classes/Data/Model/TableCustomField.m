//
//  TableCustomField.m
//  CloudworksPOS
//
//  Created by
//  Copyright 2015 __MyCompanyName__. All rights reserved.
//

#import "TableCustomField.h"

@implementation TableCustomField

@synthesize code = _code;
@synthesize fieldTypeStr = _fieldTypeStr;
@synthesize title = _title;
@synthesize description = _description;
@synthesize lookupTable = _lookupTable;
@synthesize lookupTablePK = _lookupTablePK;
@synthesize lookupTableDescription = _lookupTableDescription;
@synthesize lookupText = _lookupText;
@synthesize lookupDefaultText = _lookupDefaultText;
@synthesize require = _require;

@synthesize stringValue = _stringValue;
@synthesize numberValue = _numberValue;
@synthesize lookupId = _lookupId;

-(void)dealloc{

    [_code release];
    [_fieldTypeStr release];
    [_title release];
    [_lookupTable release];
    [_lookupTablePK release];
    [_lookupTableDescription release];
    [_lookupText release];
    [_lookupDefaultText release];
    [_description release];
    [_stringValue release];
    [_numberValue release];
    [_lookupId release];
    
    [super dealloc];
}

-(id)initWithEntity:(TableCustomField*)entity{
    if ((self=[super init])){
        _code = [entity->_code retain];
        _fieldTypeStr = [entity->_fieldTypeStr retain];
        _title = [entity->_title retain];
        _lookupTable = [entity->_lookupTable retain];
        _lookupTablePK = [entity->_lookupTablePK retain];
        _lookupTableDescription = [entity->_lookupTableDescription retain];
        _lookupText = [entity->_lookupText retain];
        _lookupDefaultText = [entity->_lookupDefaultText retain];
        _description = [entity->_description retain];
        _stringValue = [entity->_stringValue retain];
        _numberValue = [entity->_numberValue retain];
        _lookupId = [entity->_lookupId retain];
        _require = entity->_require;
    }
    return self;
}


-(TableFieldType)fieldType {
    
    if ([_fieldTypeStr isEqualToString:@"text"])
        return TableFieldTypeText;
    if ([_fieldTypeStr isEqualToString:@"lookup"])
        return TableFieldTypeLookup;
    if ([_fieldTypeStr isEqualToString:@"number"])
        return TableFieldTypeNumber;

    return TableFieldTypeEmpty;
}

-(void)installPropertyObservers{
    //do nothing
}

-(void)removePropertyObservers{
    
}

@end
