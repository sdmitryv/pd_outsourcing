//
//  CustomFields.m
//  ProductBuilder
//
//  Created by valery on 6/8/16.
//  Copyright © 2016 Cloudworks. All rights reserved.
//

#import "CustomColumns.h"

@implementation CustomColumns

-(void)dealloc{
    [_customText1 release];
    [_customText2 release];
    [_customText3 release];
    [_customText4 release];
    [_customText5 release];
    [_customText6 release];
    [_customDate1 release];
    [_customDate2 release];
    [_customDate3 release];
    [_customDate4 release];
    [_customDate5 release];
    [_customDate6 release];
    [_customLookup1Id release];
    [_customLookup2Id release];
    [_customLookup3Id release];
    [_customLookup4Id release];
    [_customLookup5Id release];
    [_customLookup6Id release];
    [_customLookup7Id release];
    [_customLookup8Id release];
    [_customLookup9Id release];
    [_customLookup10Id release];
    [_customLookup11Id release];
    [_customLookup12Id release];
    [_lookup1 release];
    [_lookup2 release];
    [_lookup3 release];
    [_lookup4 release];
    [_lookup5 release];
    [_lookup6 release];
    [_lookup7 release];
    [_lookup8 release];
    [_lookup9 release];
    [_lookup10 release];
    [_lookup11 release];
    [_lookup12 release];
    [super dealloc];
}


-(NSString *)customLookup1 {
    if (!_lookup1 || ![_lookup1.id isEqual:_customLookup1Id]) {
        _lookup1 = [[CustomLookup getInstanceById:_customLookup1Id] retain];
    }
    
    return _lookup1.name;
}

-(NSString *)customLookup2 {
    if (!_lookup2 || ![_lookup2.id isEqual:_customLookup2Id]) {
        _lookup2 = [[CustomLookup getInstanceById:_customLookup2Id] retain];
    }
    
    return _lookup2.name;
}

-(NSString *)customLookup3 {
    if (!_lookup3 || ![_lookup3.id isEqual:_customLookup3Id]) {
        _lookup3 = [[CustomLookup getInstanceById:_customLookup3Id] retain];
    }
    
    return _lookup3.name;
}

-(NSString *)customLookup4 {
    if (!_lookup4 || ![_lookup4.id isEqual:_customLookup4Id]) {
        _lookup4 = [[CustomLookup getInstanceById:_customLookup4Id] retain];
    }
    
    return _lookup4.name;
}

-(NSString *)customLookup5 {
    if (!_lookup5 || ![_lookup5.id isEqual:_customLookup5Id]) {
        _lookup5 = [[CustomLookup getInstanceById:_customLookup5Id] retain];
    }
    
    return _lookup5.name;
}

-(NSString *)customLookup6 {
    if (!_lookup6 || ![_lookup6.id isEqual:_customLookup6Id]) {
        _lookup6 = [[CustomLookup getInstanceById:_customLookup6Id] retain];
    }
    
    return _lookup6.name;
}

-(NSString *)customLookup7 {
    if (!_lookup7 || ![_lookup7.id isEqual:_customLookup7Id]) {
        _lookup7 = [[CustomLookup getInstanceById:_customLookup7Id] retain];
    }
    
    return _lookup7.name;
}

-(NSString *)customLookup8 {
    if (!_lookup8 || ![_lookup8.id isEqual:_customLookup8Id]) {
        _lookup8 = [[CustomLookup getInstanceById:_customLookup8Id] retain];
    }
    
    return _lookup8.name;
}

-(NSString *)customLookup9 {
    if (!_lookup9 || ![_lookup9.id isEqual:_customLookup9Id]) {
        _lookup9 = [[CustomLookup getInstanceById:_customLookup9Id] retain];
    }
    
    return _lookup9.name;
}

-(NSString *)customLookup10 {
    if (!_lookup10 || ![_lookup10.id isEqual:_customLookup10Id]) {
        _lookup10 = [[CustomLookup getInstanceById:_customLookup10Id] retain];
    }
    
    return _lookup10.name;
}

-(NSString *)customLookup11 {
    if (!_lookup11 || ![_lookup11.id isEqual:_customLookup11Id]) {
        _lookup11 = [[CustomLookup getInstanceById:_customLookup11Id] retain];
    }
    
    return _lookup11.name;
}

-(NSString *)customLookup12 {
    if (!_lookup12 || ![_lookup12.id isEqual:_customLookup12Id]) {
        _lookup12 = [[CustomLookup getInstanceById:_customLookup12Id] retain];
    }
    
    return _lookup12.name;
}
@end
