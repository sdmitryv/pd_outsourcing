//
//  TimeZone.m
//  ProductBuilder
//
//  Created by Viacheslav Lulakov on 6/25/14.
//  Copyright (c) 2014 Cloudworks. All rights reserved.
//

#import "TimeZone.h"

@implementation TimeZone

-(void) dealloc
{
	[_name release];
	[_description release];
    
	[super dealloc];
}

-(void)setDescription:(NSString *)description{
    [_description release];
    _description = [description retain];
}

-(NSString*)description{
    return _description;
}

+(NSMutableArray*)getTimeZones {
    return nil;
}

@end
