//
//  POSQuery.m
//  ProductBuilder
//
//  Created by Виталий Гервазюк on 3/13/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "POSQuery.h"

@implementation POSQuery

@synthesize deviceID, locationID, queryStr;

+(NSMutableArray *)getActiveQueries {
    return nil;
}

-(void)dealloc {
    [deviceID release];
    [locationID release];
    [queryStr release];
    
    [super dealloc];
}
@end
