//
//  AppParameterManager.h
//  ProductBuilder
//
//  Created by Lulakov Viacheslav on 10/19/10.
//  Copyright 2010 DBBestTechnologies. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BPUUID.h"

typedef NS_ENUM(NSUInteger, HardwareType) {
    HardwareTypeiPhone2G,
    HardwareTypeiPhone3G,
    HardwareTypeiPhone3GS,
    HardwareTypeiPhone4,
    HardwareTypeiPhone4CDMA,
    HardwareTypeiPhone4S,
    HardwareTypeiPhone5,
    HardwareTypeiPhone5GSMCDMA,
    HardwareTypeiPhone5CGSMCDMA,
    HardwareTypeiPhone5CCDMA,
    HardwareTypeiPhone5S,
    HardwareTypeiPhone5SGSMCDMA,
    HardwareTypeiPodTouch1Gen,
    HardwareTypeiPodTouch2Gen,
    HardwareTypeiPodTouch3Gen,
    HardwareTypeiPodTouch4Gen,
    HardwareTypeiPodTouch5Gen,
    HardwareTypeiPad,
    HardwareTypeiPad3G,
    HardwareTypeiPad2,
    HardwareTypeiPad2GSM,
    HardwareTypeiPad2CDMA,
    HardwareTypeiPadMini,
    HardwareTypeiPadMiniGSM,
    HardwareTypeiPadMiniGSMCDMA,
    HardwareTypeiPad3,
    HardwareTypeiPad3GSMCDMA,
    HardwareTypeiPad3GSM,
    HardwareTypeiPad4,
    HardwareTypeiPad4GSM,
    HardwareTypeiPad4GSMCDMA,
    HardwareTypeiPadAir,
    HardwareTypeiPadAirGSM,
    HardwareTypeiPadAirGSMCDMA,
    HardwareTypeiPadAir2,
    HardwareTypeiPadMini2,
    HardwareTypeiPadMini2GSM,
    HardwareTypeiPadPro,
    HardwareTypeiPadProBig,
    HardwareTypeSimulator,
    HardwareTypeUnknown
};

@interface AppParameterManager : NSObject {

    NSMutableDictionary* _parameters;
    UIDevice *device;
    
    NSString *_deviceName;
    NSString *_bundleId;
    NSString *_cardSwipe;
    NSString *_cardSwipeGC;
    NSString *_employeeName;
    NSString *_employeePassword;
    
    BOOL _skipLogin;
    BOOL _enableLogs;
    BOOL _disableUploads;
    BOOL _disableSVS;
    BOOL _disableDeviceAgent;
    BOOL _disablePing;
    BOOL _isDemoMode;
    BOOL _useNewUI;
    
    NSString *_hardware;
}

+(AppParameterManager *)instance;
+(void)resetCache;

@property (nonatomic, readonly) NSString *systemName;
@property (nonatomic, readonly) NSString *deviceSystemVersion;
@property (nonatomic, readonly) NSString *deviceModel;
@property (nonatomic, readonly) NSString *hardwareDescription;
@property (nonatomic, readonly) HardwareType hardwareType;
    
@property (nonatomic, retain) NSString *deviceName;
@property (nonatomic, retain) NSString *bundleId;
@property (nonatomic, retain) NSString *cardSwipe; //only in debug
@property (nonatomic, retain) NSString *cardSwipeGC; //only in debug
@property (nonatomic, retain) NSString *employeeName; //only in debug once
@property (nonatomic, retain) NSString *employeePassword; //only in debug once

@property (nonatomic, assign) BOOL skipLogin; //only in debug
@property (nonatomic, assign) BOOL enableLogs; //only in debug
@property (nonatomic, assign) BOOL disableUploads;
@property (nonatomic, assign) BOOL disableSVS;
@property (nonatomic, assign) BOOL disableDeviceAgent;
@property (nonatomic, assign) BOOL disablePing;
@property (nonatomic, assign) BOOL isDemoMode;
@property (nonatomic, assign) BOOL useNewUI;

@property (nonatomic, readonly) BOOL isUploadsDisabled;
@property (nonatomic, readonly) BOOL isSvsDisabled;


@end
