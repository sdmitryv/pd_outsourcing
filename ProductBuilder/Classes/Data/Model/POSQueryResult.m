//
//  POSQueryResult.m
//  ProductBuilder
//
//  Created by Виталий Гервазюк on 3/13/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "POSQueryResult.h"

@implementation POSQueryResult

@synthesize queryID, result;

-(void)dealloc {
    [queryID release];
    [result release];
    
    [super dealloc];
}

@end
