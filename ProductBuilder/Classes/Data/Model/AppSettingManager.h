//
//  AppSetting.h
//  ProductBuilder
//
//  Created by Lulakov Viacheslav on 10/19/10.
//  Copyright 2010 DBBestTechnologies. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BPUUID.h"
#import "Device.h"
#import "Location.h"

#define SWIPE_CREDIT_CARD_KEY				@"SwipeCreditCard"
#define LAST_RECEIPT_NUMBER_KEY				@"LastReceiptNumber"
#define NEXT_RECEIPT_NUMBER_KEY				@"NextReceiptNumber"
#define RECEIPT_SEARCH_KB_VISIBLE_KEY		@"ReceiptSearchKBVisible"
#define CUSTOMER_SEARCH_KEYBOARD_ENABLE_KEY @"CustomerSearchKeyboardEnable"
#define ITEM_SEARCH_KEYBOARD_ENABLE_KEY     @"ItemSearchKeyboardEnable"
#define GIFT_BALANCE_KEYBOARD_ENABLE_KEY    @"GiftBalanceKeyboardEnable"
#define SELL_GIFTCARD_KEYBOARD_ENABLE_KEY   @"SellGiftCardKeyboardEnable"
#define SALES_RCPT_KEYBOARD_ENABLED_KEY     @"SalesReceiptKeyboardEnable"


#define LOCATION_ID_KEY                     @"locationId"
#define WORKSTATIION_ID_KEY                 @"workstationId"
#define INIT_STATUS_KEY                     @"initStatus"

#define IPAD_ID_KEY                         @"ipad_id_preference"
#define PRINTING_IP_KEY                     @"ip_address_printing_preference"
#define PRINTING_PORT_KEY                   @"port_printing_preference"
#define SCALE_IP_KEY                        @"ip_address_scale_preference"
#define SCALE_PORT_KEY                      @"port_scale_preference"
#define SCALE_NAME_KEY                      @"scale_name_preference"
#define SVS_ACCESS_TOKEN                    @"svs_access_token_preference"

#define IMAGE_PREFERENCE_KEY                @"item_display_image_preference"
#define DESCRIPTION_PREFERENCE_KEY          @"item_description_preference"
#define ATTRIBUTE_PREFERENCE_KEY            @"item_attribute_preference"
#define ITEM_SEARCH_TYPE                    @"item_search_type"

#define DEVICE_SERVER_ID_KEY                @"deviceServerID"
#define DEVICE_UNIQUE_ID_KEY                @"deviceUniqueID"
#define DEVICE_LOCATION_CODE_KEY            @"locationDeviceCode"

#define RECEIPT_ACCOUNTS_TAB_SELECTED_INDEX @"ReceiptAccountsTabSelectedIndex"
#define CUSTOMER_ACCOUNTS_TAB_SELECTED_INDEX @"CustomerAccountsTabSelectedIndex"
#define LAST_CUSTOMER_CODE                  @"LastCustomerCode"
#define NEXT_SALES_ORDER_NUMBER_KEY         @"NextSalesOrderNumber"

#define PLUGIN_XML                          @"PluginXML"

#define CLEAN_RICH_MEDIA_STORAGE_LAST_DATE_KEY  @"ClearRichMediaStorageLastDate"
#define SERVER_URL                          @"ServerURL"
#define SERVER_API_KEY                      @"ServerApiKey"
#define DEFAULT_SERVER_CODE                 @"DefaultServerCode"
#define GENIUS_IP                           @"GeniusIP"
#define CAYAN_TERMINAL_ID                   @"POSCayanTerminalId"


#define CURRENT_APP_VERSION                 @"currentAppVersion"
#define APP_VERSION                         @"appVersion_"

#define APP_LANGUAGE                        @"LANGUAGE"

#define DEVICE_STATE                        @"DeviceState"
#define CLOSE_BARCODE_READE_ON_GOOD_SCAN    @"CloseOnGoodScanSettingKey"

#define PUSH_LOGS_TO_GAE                    @"PushLogsToGae"
#define GAE_LOG_SEVERITY                    @"GaeLogSeverity"
#define COLLECT_NETWORK_STATS               @"CollectNetworkStats"
#define IS_MULTI_WORKSTATION                @"isMultiWorkstation"

#define PAYWORKS_SIGN_ON_SHOPPER            @"PayworksSignOnShopper"

#define RICH_CATALOG_SEGMENT1_INDEX         @"RichCatalogSegment1Index"
#define RICH_CATALOG_CATEGORIES_STATE       @"RichCatalogCategoriesState"

#define DEVICE_IP                           @"DeviceIPAddress"
#define DEVICE_NOTES                        @"DeviceNotes"


typedef NS_ENUM(NSUInteger, ItemDescriptionModeView) { ItemDescriptionModeViewStoreDescription = 1, ItemDescriptionModeViewDescription1 = 2, ItemDescriptionModeViewDescription2 = 3, ItemDescriptionModeViewDescription3 = 4 } ;

typedef NS_ENUM(NSUInteger, ItemAttributeModeView) { ItemAttributeModeViewQuantity = 1, ItemAttributeModeViewAttribute1 = 2, ItemAttributeModeViewAttribute2 = 3, ItemAttributeModeViewAttribute3 = 4, ItemAttributeModeViewDiscount = 5 };


@interface AppSettingManager : NSObject {

	NSMutableDictionary* _settingsList;
    NSMutableDictionary* _globalList;
    
    BOOL _isDeviceSettingsLoaded;
    BOOL _pushLogsToGae;
    NSInteger _gaeLogSeverity;
    BOOL _collectNetworkStats;
    BOOL _isMultiWorkstation;
    
    NSInteger _deviceState;
    BPUUID* _fillFromLocationId;
}

+(AppSettingManager *)instance;

-(void)setBool:(BOOL)object forKey:(NSString*)key;
-(void)setString:(NSString*)value forKey:(NSString*)key;
-(void)setInt:(NSInteger)object forKey:(NSString*)key;
-(void)setDate:(NSDate*)object forKey:(NSString*)key;
-(void)setDecimalNumber:(NSDecimalNumber*)object forKey:(NSString*)key;
-(void)setUuid:(BPUUID*)object forKey:(NSString*)key;

-(BOOL)boolForKey:(NSString*)key;
-(BOOL)boolForKey:(NSString*)key defaultValue:(BOOL)defaultValue;
-(NSString*)stringForKey:(NSString*)key;
-(NSString*)stringForKey:(NSString*)key defaultValue:(NSString*)defaultValue;
-(NSInteger)intForKey:(NSString*)key;
-(NSInteger)intForKey:(NSString*)key defaultValue:(NSInteger)defaultValue;
-(NSDate*)dateForKey:(NSString*)key;
-(NSDecimalNumber*)decimalNumberForKey:(NSString*)key;
-(BPUUID*)uuidForKey:(NSString*)key;

@property (nonatomic, assign) BOOL paymentSwipeCreditCard;
@property (nonatomic, assign) NSInteger receiptLastNumber;
@property (nonatomic, assign) BOOL receiptSearchKBVisible;
@property (nonatomic, assign) BOOL customerSearchKeyboardEnable;
@property (nonatomic, assign) BOOL itemSearchKeyboardEnable;
@property (nonatomic, assign) BOOL giftBalanceKeyboardEnable;
@property (nonatomic, assign) BOOL sellGiftCardKeyboardEnable;
@property (nonatomic, assign) NSInteger receiptAccountsTabSelectedIndex;
@property (nonatomic, assign) NSInteger customerAccountsTabSelectedIndex;
@property (nonatomic, assign) NSDate* lastSyncDate;
@property (nonatomic, retain) NSString* lastCustomerCode;
@property (nonatomic, assign) NSInteger salesOrderNextNumber;

//svs settings
@property (nonatomic, assign) NSDecimalNumber* svs_LRPThreshold;
@property (nonatomic, assign) BOOL svs_FreshAddressEnabled;
@property (nonatomic, assign) BOOL svs_TokenEnabled;
@property (nonatomic, assign) BOOL svs_CrmLRPCalculationEnabled;

@property (atomic, retain) BPUUID                   *workstationId;
@property (atomic, retain) BPUUID                   *locationId;
@property (atomic, retain) Location                 *initializedLocation;
@property (atomic, assign) BOOL                      initStatus;

@property (atomic, readonly) NSString               *iPadId;
@property (atomic, readonly) NSString               *printingServer;
@property (atomic, readonly) NSInteger               printingServerPort;
@property (atomic, readonly) NSString               *scaleServerAddress;
@property (atomic, readonly) NSInteger               scaleServerPort;

@property (atomic, retain) NSString                 *scaleName;

@property (atomic, retain) NSString                 *SVSAccessToken;

@property (atomic, readonly) BOOL                    isDisplayItemImage;
@property (atomic, readonly) ItemDescriptionModeView itemDescriptionModeView;
@property (atomic, readonly) ItemAttributeModeView   itemAttributeModeView;
@property (atomic, readonly) NSString                *itemSearchKey;

@property (nonatomic, retain) BPUUID                *deviceServerId;//equal deviceID
@property (nonatomic, retain) BPUUID                *deviceUniqueId;//equal deviceAgentID
@property (nonatomic, assign) NSInteger              locationDeviceCode;


@property (atomic, retain) NSString                 *pluginXML;
@property (nonatomic, assign) NSDate                *richMediaStorageLastClearDate;

@property (atomic, retain) NSString                 *serverUrl;
@property (atomic, retain) NSString                 *serverApiKey;
@property (atomic, retain) NSString                 *defaultServerCode;
@property (atomic, assign) NSInteger                userInputBasePrice;
@property (atomic, assign) NSInteger                userInputOrderCost;
@property (atomic, assign) BOOL                     useSourceDescription4;

@property (nonatomic, assign) NSInteger              deviceState;
@property (nonatomic, retain) NSString              *deviceLanguage;
@property (nonatomic, retain) NSString              *productBuilderData;
@property (nonatomic, assign) BOOL                   closeBarcodeReaderOnGoodScan;

@property (atomic, readonly) BOOL                    pushLogsToGae;
@property (atomic, readonly) NSInteger               gaeLogSeverity;
@property (atomic, readonly) BOOL                    collectNetworkStats;
@property (atomic, readonly) BOOL                    isMultiWorkstation;

@property (nonatomic, retain) NSString              *geniusIp;
@property (nonatomic, retain) NSString              *cayanTerminalId;
@property (nonatomic, assign) BOOL                  payworksSignOnShopper;

@property (nonatomic, assign) NSInteger             richCatalogSegment1Index;
@property (nonatomic, assign) NSString              *richCatalogCategoriesState;
@property (nonatomic, assign) BPUUID*               fillFromLocationId;

@property (nonatomic, readonly) NSString            *appVersionDetails;
@property (nonatomic, readonly) NSString            *lastSyncDateString;
@property (nonatomic, retain) NSString              *deviceIP;
@property (nonatomic, retain) NSString              *deviceNotes;

-(void)saveGlobalValues;
-(void)resetCache;
-(void)restoreGlobalValues;

-(void)updateSettingsWithDevice:(Device *)device;
-(void)setItemSettingDisplayImage:(BOOL)displayImage descriptionMode:(ItemDescriptionModeView)descriptionMode attributeMode:(ItemAttributeModeView)attributeMode;

-(void)updateVersionData;

@end
