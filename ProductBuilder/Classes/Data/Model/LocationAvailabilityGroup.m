//
//  LocationAvailabilityGroup.m
//  TeamworkPOS
//
//  Created by Lulakov Viacheslav on 1/16/13.
//  Copyright (c) 2013 Cloudworks. All rights reserved.
//

#import "LocationAvailabilityGroup.h"

@implementation LocationAvailabilityGroup

@synthesize description = _description;
@synthesize code = _code;
@synthesize displayDistance = _displayDistance;

-(void)dealloc {
    [_description release];
    [_code release];
    
    [super dealloc];
}

@end
