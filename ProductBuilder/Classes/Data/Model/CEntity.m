//
//  CEntity.m
//  ProductBuilder
//
//  Created by Valera on 9/2/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "CEntity.h"
#import "NSObject+NSCoding.h"

@implementation CEntity
@synthesize customRecId;
- (id)initWithEntity:(CEntity *)entity{
    if((self=[super initWithEntity:entity])){
        _errors = [entity->_errors retain];
        _isCancellingEdit = entity->_isCancellingEdit;
        _isDirty = entity->_isDirty;
        _isNew = entity->_isNew;
        _isDeleted = entity->_isDeleted;
        _editLevel = entity->_editLevel;
        _savedStates = [entity->_savedStates retain];
    }
    return self;
}

+(NSSet *)properties
{
	// Recurse up the classes, but stop at NSObject. Each class only reports its own properties, not those inherited from its superclass
	NSMutableSet *theProps;
	
	if ([self superclass] != [CROEntity class])
		theProps = (NSMutableSet *)[[self superclass] properties];
	else
		theProps = [NSMutableSet set];
	
	unsigned int outCount;
	
	objc_property_t *propList = class_copyPropertyList([self class], &outCount);
	NSInteger i;
	
	// Loop through properties and add declarations for the create
	for (i=0; i < outCount; i++)
	{
		objc_property_t oneProp = propList[i];
		NSString *propName = @(property_getName(oneProp));
		const char *attrs = property_getAttributes(oneProp);
		// Read only attributes are assumed to be derived or calculated
		// See http://developer.apple.com/documentation/Cocoa/Conceptual/ObjCRuntimeGuide/Articles/chapter_8_section_3.html
        if(strstr(attrs, ",R,") == NULL){
            [theProps addObject:propName];
		}
	}
	free( propList );
	return theProps;	
}

-(id)init{
	if ((self=[super init]))
	{
        [self initDefaults];
	}
	return self;
}

-(void)removePropertyObservers{
    if (_observingProps){
        @synchronized(self){
            if (_observingProps){
                for (NSString *oneProp in [[self class] properties]){
                    [self removeObserver:self forKeyPath:oneProp];
                }
                _observingProps = FALSE;
            }
        }
    }
}

-(void)installPropertyObservers{
    if (!_observingProps){
        @synchronized(self){
            if (!_observingProps){
                for (NSString *oneProp in [[self class] properties]){
                    [self addObserver:self forKeyPath:oneProp options:NSKeyValueObservingOptionNew | NSKeyValueObservingOptionOld context:nil];
                }
                _observingProps = TRUE;
            }
        }
    }
}

-(void)initInternal{
    [super initInternal];
    _isDirty = YES;
    _isNew = YES;
    _isDeleted = FALSE;
    propertyChangedReceivers = [[NSMutableArray alloc]init];
    _savedStates = [[NSMutableArray alloc]init];
}

-(void)initDefaults{
    self.id = [BPUUID UUID];
    self.recModified = self.recCreated = [NSDate date];
}

#pragma mark -
#pragma mark KV

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
	if ([self isDirty]) return;
	if ([change[NSKeyValueChangeKindKey] intValue] == NSKeyValueChangeSetting) {
		id oldValue = change[NSKeyValueChangeOldKey];
		id newValue = change[NSKeyValueChangeNewKey];
		//NSLog(@"oldValue for %@ = %@, new value = %@", keyPath, oldValue, newValue);
		if ((!oldValue && newValue) || (oldValue && ![oldValue isEqual:newValue]))
			[self markDirty];
	}
}

#pragma mark -
#pragma mark Property changed and receivers

-(void)propertyChanged:(NSString*)keyPath{
	[self propertyChanged:keyPath value:nil];
}

-(void)propertyChanged:(NSString*)keyPath value:(id)value{
    for (NSInteger i = 0; i < propertyChangedReceivers.count; i++){
        NSValue* receiverValue = propertyChangedReceivers[i];
		if (receiverValue){
			NSObject<IPropertyChangedReceiver>* receiver = (NSObject<IPropertyChangedReceiver>*)[receiverValue nonretainedObjectValue];
			if (receiver){
                if (([receiver isKindOfClass:[UIViewController class]] || [receiver isKindOfClass:[UIView class]]) && ![[NSThread currentThread] isEqual:[NSThread mainThread]]){
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [receiver propertyChanged:self keyPath:keyPath value:value];    
                    });
                }
                else{
                    [receiver propertyChanged:self keyPath:keyPath value:value];
                }
            }
		}
	}
    if (![keyPath isEqualToString:@""]){
        [self markDirty];
    }
}

-(void)subscribeReceiver:( NSObject<IPropertyChangedReceiver>*)receiver{
    //avoid double subscription
    for(NSUInteger i = 0; i < propertyChangedReceivers.count; i++){
        NSValue *value = propertyChangedReceivers[i];
        if ([value nonretainedObjectValue]==receiver){
            return;
        }
    }
    NSValue *value = [NSValue valueWithNonretainedObject:receiver];
	[propertyChangedReceivers addObject:value];
}

-(void)unSubscribeReceiver:( NSObject<IPropertyChangedReceiver>*)receiver{
    for(NSUInteger i = 0; i < propertyChangedReceivers.count; i++){
        NSValue *value = propertyChangedReceivers[i];
        if ([value nonretainedObjectValue]==receiver){
            [propertyChangedReceivers removeObjectAtIndex:i];
            break;
        }
    }
}

-(void)propertyChanged:(id)sender keyPath:(NSString*)keyPath value:(id)value{
    
}

#pragma mark -
#pragma mark Methods
- (void)markDirty
{
	_isDirty = YES;
    [self removePropertyObservers];
}

- (void)markClean
{
	_isDirty = NO;
    [self installPropertyObservers];
}

-(void)markOld
{
	_isNew = FALSE;
	[self markClean];
}

- (void)markNew
{
	_isNew = TRUE;
	_isDeleted = FALSE;
	[self markDirty];
}

-(void)markDeleted
{
	_isDeleted = TRUE;
	[self markDirty];
}

- (BOOL)isDirty
{
	return _isDirty;
}

- (BOOL)isNew
{
	return _isNew;
}

- (BOOL)isDeleted
{
	return _isDeleted;
}

-(void)setIsDeleted:(BOOL)value{
    _isDeleted = value;
    if (_isDeleted){
        [self markDirty];
    }
}

-(NSNumber*)syncState{
    return nil;
}

-(void) dealloc
{
	[self removePropertyObservers];
    [propertyChangedReceivers release];
    propertyChangedReceivers = nil;
    [_savedStates release];
    _savedStates = nil;
    [_errors release];
    _errors = nil;
    [customRecId release];
    customRecId = nil;
	[super dealloc];
}

#pragma mark -
#pragma mark Editing

-(void)handleEditableChildObjectsWithSelector:(SEL)selector{
    NSDictionary *iVars = [self iVars];
    for (NSString *key in iVars) {
        if (![self shouldEncodeProperty:key]) continue;
        NSString *type = iVars[key];
        Ivar ivar = object_getInstanceVariable(self, [key UTF8String], NULL);
        if (!ivar) continue;
        switch ([type characterAtIndex:0]) {
            case '@':   // object
            {
                NSInteger countOfQuatation = 0;
                for(NSInteger i=0; i<type.length;i++){
                    if ([type characterAtIndex:i]== '"'){
                        countOfQuatation++;
                        if (countOfQuatation > 1) break;
                    }
                }
                //if ([[type componentsSeparatedByString:@"\""] count] > 1) {
                if (countOfQuatation > 1) {
                    id value = object_getIvar(self, ivar);
                    if ([value conformsToProtocol:@protocol(EditableObject)]){
                        BOOL ignoringReadonly = _ignoringReadonly;
                        if (ignoringReadonly){
                            [value beginIgnoringReadonly];
                        }
                        objc_msgSend(value, selector);
                        if (ignoringReadonly){
                            [value endIgnoringReadonly];
                        }
                        
                    }
                }
                }
                break;
            default:
                break;
        }
    }
}

-(NSUInteger)editLevel{
    return _editLevel;
}

-(BOOL)isCancellingEdit{
    return _isCancellingEdit;
}

-(void)beginIgnoringReadonly{
    _ignoringReadonly++;
}

-(void)endIgnoringReadonly{
    _ignoringReadonly--;
}

-(BOOL)canEdit{
    return [self canEdit:nil];
}

-(void)beginEdit{
    if (!_ignoringReadonly && !self.canEdit) return;
    _editLevel++;
    NSObject* data = [[NSObject alloc]init];
    [self saveToObject:data];
    [self handleEditableChildObjectsWithSelector:_cmd];
    [_savedStates addObject:data];
    [data release];
}

-(void)cancelEdit{
    if (!_editLevel) return;
    _isCancellingEdit++;
    if (_savedStates.count){
        NSObject* data = [_savedStates lastObject];
        [self restoreFromObject:data];
        [self handleEditableChildObjectsWithSelector:_cmd];
        [_savedStates removeLastObject];
    }
    _isCancellingEdit--;
    _editLevel--;
    [self propertyChanged:@""];
}

-(void)endEdit{
    if (!_editLevel) return;
    [self handleEditableChildObjectsWithSelector:_cmd];
    if (_savedStates.count)
        [_savedStates removeLastObject];
    _editLevel--;
}

- (BOOL)shouldEncodeProperty:(NSString *)name{
    if ([name isEqualToString:@"_observingProps"]
        || [name isEqualToString:@"_editLevel"]
        || [name isEqualToString:@"_isCancellingEdit"]
        || [name isEqualToString:@"_savedStates"]
        || [name isEqualToString:@"propertyChangedReceivers"]){
        return FALSE;
    }
    return TRUE;
}

-(BOOL)copyEditLevelsToEntity:(NSObject<EditableObject>*)value{
    if ([self class]!=[value class]) return FALSE;
    CEntity* toEntity = (CEntity*)value;
    if (![self.id isEqual:toEntity.id]) return FALSE;
    toEntity->_editLevel = _editLevel;
    [toEntity->_savedStates release];
    toEntity->_savedStates = [_savedStates retain];
    return TRUE;
}

#pragma mark -
#pragma mark IValidatable

-(void)validate{
    
}

- (BOOL) is_valid
{
	[self.errors clear];
	[self validate];
	return !self.errors.count;
}

- (Errors *) errors
{
	if (_errors==nil) {
		_errors = [[Errors alloc] init:self];
	}
	return _errors;
}

#pragma mark -
#pragma mark save

-(BOOL)save{
	return [self save:nil];
}

-(BOOL)save:(NSError**)error{
    return TRUE;
}

- (BOOL)validateOnSave:(NSError**)error{
    return TRUE;
}

#pragma mark -

-(BOOL)readOnly{
    return ![self canEdit:nil];
}

-(NSError*)readonlyError{
    NSError* error = nil;
    [self canEdit:&error];
    return error;
}

-(BOOL)canEdit:(NSError**)error{
    return YES;
}

#pragma mark -

+(BOOL)writeError:(NSError**)error description:(NSString*)description domain:(NSString*)domain{
    return [[self class] writeError:error title:nil description:description domain:domain code:100 additionalInfo:nil];
}

+(BOOL)writeError:(NSError**)error description:(NSString*)description domain:(NSString*)domain code:(NSInteger)code{
    return [[self class]writeError:error title:description description:nil domain:domain code:code additionalInfo:nil];
}

+(BOOL)writeError:(NSError**)error title:(NSString*)title description:(NSString*)description domain:(NSString*)domain{
    return [[self class]writeError:error title:title description:description domain:domain code:100 additionalInfo:nil];
}

+(BOOL)writeError:(NSError**)error title:(NSString*)title description:(NSString*)description domain:(NSString*)domain code:(NSInteger)code{
    return [[self class]writeError:error title:title description:description domain:domain code:100 additionalInfo:nil];
}

+(BOOL)writeError:(NSError**)error title:(NSString*)title description:(NSString*)description domain:(NSString*)domain code:(NSInteger)code additionalInfo:(NSDictionary*)additionalInfo{
    if (error){
        NSMutableDictionary *errorDetail = additionalInfo ? [NSMutableDictionary dictionaryWithDictionary:additionalInfo] : [NSMutableDictionary dictionary];
        if (title || description)
            [errorDetail setValue:title ?: description forKey:NSLocalizedDescriptionKey];
        if (title && description)
            [errorDetail setValue:description forKey:NSLocalizedFailureReasonErrorKey];
        *error = [NSError errorWithDomain:domain ? domain : @"unspecified domain" code:code userInfo:errorDetail];
    }
    return error!=nil;
}

@end
