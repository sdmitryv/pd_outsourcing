//
//  AttributeSet.h
//  Buyer
//
//  Created by Lulakov Viacheslav on 11/2/10.
//  Copyright 2010 DBBestTechnologies. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CEntity.h"

@interface AttributeSet : CROEntity  {

    NSString    * _code;
	NSString    * _description;
    NSString    * _attributeLabel;
}

@property (nonatomic, retain) NSString	* code;
@property (nonatomic, retain) NSString	* description;
@property (nonatomic, retain) NSString	* attributeLabel;

+(AttributeSet *)loadAttributeSetByCode:(NSString *)code;

@end
