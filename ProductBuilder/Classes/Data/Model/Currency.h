//
//  TestClass.h
//  ProductBuilder
//
//  Created by Roman on 11/1/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CROEntity.h"
#import "Location.h"

typedef NS_ENUM(NSUInteger, RoundingType) { RoundingTypeNearest = 0, RoundingTypeUp = 1, RoundingTypeDown = 2 };
typedef NS_ENUM(NSUInteger, SymbolPosition) { SymbolPositionLeft = 0, SymbolPositionRight = 1, SymbolPositionDontShow = 2 };

@interface Currency : CROEntity{
    NSString* _description;
}

@property (nonatomic, retain) NSString *code;
@property (nonatomic, retain) NSString *currencyCode;
@property (nonatomic, retain) NSString *description;
@property (nonatomic, assign) NSDecimal amountRoundingPrecision;
@property (nonatomic, assign) NSInteger displayDecimalPrecision;
@property (nonatomic, assign) RoundingType roundingType;
@property (nonatomic, retain) NSString *symbol;
@property (nonatomic, assign) SymbolPosition symbolPosition;
@property (nonatomic, assign) BOOL isBase;
@property (nonatomic, readonly) NSLocale* locale;
@property (nonatomic, assign)NSDecimal paymentRoundingPrecision;
@property (nonatomic, readonly)NSDecimalNumber* cashRoundingPrecision;

+(Currency *) localCurrency;
+(Currency *)locationCurrency:(Location*)location;
+(Currency *) companyCurrency;
+(void)setLocalCurrencyUnsafe:(Currency*)currency;
//+(BOOL)isLocalCurrencyId:(BPUUID*)currencyId;
+(void) resetCache;
-(void)initializeFromLocalSystem;
+(NSDecimal)getAmountRoundingPrecisionFromDecimalDigits:(NSInteger)decimalDigits;

-(NSDecimal)roundDisplayAmount:(NSDecimal) value;
-(NSDecimal)roundAmount:(NSDecimal) value;
+(NSDecimal)roundAmount:(NSDecimal) value toPrecision:(NSDecimal)amountRoundingPrecision roundingMode:(NSRoundingMode)roundingMode;
@end
