//
//  MobileExceptionLog.h
//  ProductBuilder
//
//  Created by Alexander Martyshko on 10/3/13.
//  Copyright (c) 2013 Cloudworks. All rights reserved.
//

#import "CEntity.h"

@class SOAPFault;

@interface MobileExceptionLog : CEntity{
    NSString* _description;
}

@property (nonatomic, retain) NSString  *message;
@property (nonatomic, retain) BPUUID    *locationID;
@property (nonatomic, retain) BPUUID    *worstationID;
@property (nonatomic, retain) NSString  *deviceID;
@property (nonatomic, retain) NSString  *description;

+(void)postSoapFault:(SOAPFault *)soapFault originalRequest:(NSString *)originalRequest;

@end
