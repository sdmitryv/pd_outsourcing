//
//  CustomFields.h
//  ProductBuilder
//
//  Created by valery on 6/8/16.
//  Copyright © 2016 Cloudworks. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BPUUID.h"
#import "CustomLookup.h"

@interface CustomColumns : NSObject

@property (nonatomic, retain) NSString* customText1;
@property (nonatomic, retain) NSString* customText2;
@property (nonatomic, retain) NSString* customText3;
@property (nonatomic, retain) NSString* customText4;
@property (nonatomic, retain) NSString* customText5;
@property (nonatomic, retain) NSString* customText6;
@property (nonatomic, retain) NSDate* customDate1;
@property (nonatomic, retain) NSDate* customDate2;
@property (nonatomic, retain) NSDate* customDate3;
@property (nonatomic, retain) NSDate* customDate4;
@property (nonatomic, retain) NSDate* customDate5;
@property (nonatomic, retain) NSDate* customDate6;
@property (nonatomic, assign) BOOL customFlag1;
@property (nonatomic, assign) BOOL customFlag2;
@property (nonatomic, assign) BOOL customFlag3;
@property (nonatomic, assign) BOOL customFlag4;
@property (nonatomic, assign) BOOL customFlag5;
@property (nonatomic, assign) BOOL customFlag6;
@property (nonatomic, assign) NSInteger customNumber1;
@property (nonatomic, assign) NSInteger customNumber2;
@property (nonatomic, assign) NSInteger customNumber3;
@property (nonatomic, assign) NSInteger customNumber4;
@property (nonatomic, assign) NSInteger customNumber5;
@property (nonatomic, assign) NSInteger customNumber6;
@property (nonatomic, assign) NSDecimal customDecimal1;
@property (nonatomic, assign) NSDecimal customDecimal2;
@property (nonatomic, assign) NSDecimal customDecimal3;
@property (nonatomic, assign) NSDecimal customDecimal4;
@property (nonatomic, assign) NSDecimal customDecimal5;
@property (nonatomic, assign) NSDecimal customDecimal6;
@property (nonatomic, retain) BPUUID* customLookup1Id;
@property (nonatomic, retain) BPUUID* customLookup2Id;
@property (nonatomic, retain) BPUUID* customLookup3Id;
@property (nonatomic, retain) BPUUID* customLookup4Id;
@property (nonatomic, retain) BPUUID* customLookup5Id;
@property (nonatomic, retain) BPUUID* customLookup6Id;
@property (nonatomic, retain) BPUUID* customLookup7Id;
@property (nonatomic, retain) BPUUID* customLookup8Id;
@property (nonatomic, retain) BPUUID* customLookup9Id;
@property (nonatomic, retain) BPUUID* customLookup10Id;
@property (nonatomic, retain) BPUUID* customLookup11Id;
@property (nonatomic, retain) BPUUID* customLookup12Id;
@property (nonatomic, retain) CustomLookup* lookup1;
@property (nonatomic, retain) CustomLookup* lookup2;
@property (nonatomic, retain) CustomLookup* lookup3;
@property (nonatomic, retain) CustomLookup* lookup4;
@property (nonatomic, retain) CustomLookup* lookup5;
@property (nonatomic, retain) CustomLookup* lookup6;
@property (nonatomic, retain) CustomLookup* lookup7;
@property (nonatomic, retain) CustomLookup* lookup8;
@property (nonatomic, retain) CustomLookup* lookup9;
@property (nonatomic, retain) CustomLookup* lookup10;
@property (nonatomic, retain) CustomLookup* lookup11;
@property (nonatomic, retain) CustomLookup* lookup12;
@property (nonatomic, readonly) NSString* customLookup1;
@property (nonatomic, readonly) NSString* customLookup2;
@property (nonatomic, readonly) NSString* customLookup3;
@property (nonatomic, readonly) NSString* customLookup4;
@property (nonatomic, readonly) NSString* customLookup5;
@property (nonatomic, readonly) NSString* customLookup6;
@property (nonatomic, readonly) NSString* customLookup7;
@property (nonatomic, readonly) NSString* customLookup8;
@property (nonatomic, readonly) NSString* customLookup9;
@property (nonatomic, readonly) NSString* customLookup10;
@property (nonatomic, readonly) NSString* customLookup11;
@property (nonatomic, readonly) NSString* customLookup12;

@end
