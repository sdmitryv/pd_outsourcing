//
//  SecurityLog.h
//  ProductBuilder
//
//  Created by Vitaliy Gervazuk on 9/10/12.
//  Copyright (c) 2012 Cloudworks. All rights reserved.
//

#import "CEntity.h"

typedef NS_ENUM(NSUInteger, DocumentLineType) {
    DocumentLineTypeNone = 0,
    DocumentLineTypeItem = 1,
    DocumentLineTypePayment = 2,
    DocumentLineTypeCharge = 3,
};

typedef NS_ENUM(NSUInteger, DocumentType) {
    DocumentTypeSalesNone = 0,
    DocumentTypeSalesReceipt = 1,
    DocumentTypeSalesOrder = 2,
};

@interface SecurityLog : CEntity {
    BPUUID* documentId;
    DocumentType docType;
    BPUUID* documentLineId;
    DocumentLineType docLineType;
    BPUUID* srcEmployeeId;
    BPUUID* destEmployeeId;
    NSString* roleCode;
    NSString* description;
    NSDate* utcLogDate;
    NSDate* localLogDate;
}

@property(retain, nonatomic) BPUUID* documentId;
@property(assign, nonatomic) DocumentType docType;
@property(retain, nonatomic) BPUUID* documentLineId;
@property(assign, nonatomic) DocumentLineType docLineType;
@property(retain, nonatomic) BPUUID* srcEmployeeId;
@property(retain, nonatomic) BPUUID* destEmployeeId;
@property(retain, nonatomic) NSString* roleCode;
@property(retain, nonatomic) NSString* description;
@property(retain, nonatomic) NSDate* utcLogDate;
@property(retain, nonatomic) NSDate* localLogDate;

/* ------------------ RECEIPT Security Log Methods ----------------------------------------- */

-(id)initWithReceipt:(BPUUID*)receiptId rcptItemline:(BPUUID*) lineId srcEmployee:(BPUUID*)sourceEmployee destEmployee:(BPUUID*)destEmployee roleCode:(NSString*)roleCode description:(NSString*)description;
-(id)initWithReceipt:(BPUUID*)receiptId rcptChargeline:(BPUUID*) lineId srcEmployee:(BPUUID*)sourceEmployee destEmployee:(BPUUID*)destEmployee roleCode:(NSString*)roleCode description:(NSString*)description;
-(id)initWithReceipt:(BPUUID*)receiptId rcptPaymentline:(BPUUID*) lineId srcEmployee:(BPUUID*)sourceEmployee destEmployee:(BPUUID*)destEmployee roleCode:(NSString*)roleCode description:(NSString*)description;
-(id)initWithReceipt:(BPUUID*)receiptId srcEmployee:(BPUUID*)sourceEmployee destEmployee:(BPUUID*)destEmployee roleCode:(NSString*)aroleCode description:(NSString*)adescription;

/* ------------------ ORDER Security Log Methods ----------------------------------------- */

-(id)initWithOrder:(BPUUID*)orderId orderItemline:(BPUUID*) lineId srcEmployee:(BPUUID*)sourceEmployee destEmployee:(BPUUID*)destEmployee roleCode:(NSString*)roleCode description:(NSString*)description;
-(id)initWithOrder:(BPUUID*)orderId orderChargeline:(BPUUID*) lineId srcEmployee:(BPUUID*)sourceEmployee destEmployee:(BPUUID*)destEmployee roleCode:(NSString*)roleCode description:(NSString*)description;
-(id)initWithOrder:(BPUUID*)orderId orderPaymentline:(BPUUID*) lineId srcEmployee:(BPUUID*)sourceEmployee destEmployee:(BPUUID*)destEmployee roleCode:(NSString*)roleCode description:(NSString*)description;
-(id)initWithOrder:(BPUUID*)orderId srcEmployee:(BPUUID*)sourceEmployee destEmployee:(BPUUID*)destEmployee roleCode:(NSString*)aroleCode description:(NSString*)adescription;

+(NSMutableArray*)getSecurityLogsForReceipt:(BPUUID*)receiptId;

+(NSMutableArray*)getSecurityLogsForOrder:(BPUUID*)orderId;

@end
