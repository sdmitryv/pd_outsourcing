//
//  Workstation.h
//  ProductBuilder
//
//  Created by Valera on 9/13/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CROEntity.h"

@interface Workstation : CROEntity {

	int _wsNum;
	NSString* _name;
	BPUUID* _locationID;
	BPUUID* _taxZoneID;
	BPUUID* _nodeID;
	NSString* _machineName;
	BPUUID* _machineID;
}


@property(nonatomic,assign) int wsNum;
@property(nonatomic,retain) NSString* name;
@property(nonatomic,retain) BPUUID* locationID;
@property(nonatomic,retain) BPUUID* taxZoneID;
@property(nonatomic,retain) BPUUID* nodeID;
@property(nonatomic,retain) NSString* machineName;
@property(nonatomic,retain) BPUUID* machineID;

+(Workstation*)currentWorkstation;
+(NSArray*)getWorkstations;
+(void)resetCache;
//dummy
+(void)setCurrentWorkstationId:(BPUUID*)id;
@end
