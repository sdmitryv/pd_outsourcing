//
//  Device.m
//  DeviceAgent
//
//  Created by Alexander Martyshko on 11/7/13.
//  Copyright (c) 2013 Cloudworks. All rights reserved.
//

#import "Device.h"
#import "Settings.h"
#import "DeviceAgentManager.h"
#import "Location.h"
#include "NetUtilities.h"
#import <sys/utsname.h>

@implementation Device

- (void)dealloc {
    [_deviceAgentId release];
    [_locationId release];
    [_deviceName release];
    [_deviceAlias release];
    [_printServerIP release];
    [_shopperDisplayId release];
    [_notes release];
    [_techInfo1 release];
    [_techInfo2 release];
    [_techInfo3 release];
    [_techInfo4 release];
    [_techInfo5 release];
    [_techInfo6 release];
    [_techInfo7 release];
    [_techInfo8 release];
    [_techInfo9 release];
    [_techInfo10 release];
    [_IPAddress release];
    [_systemName release];
    [_systemVersion release];
    [_model release];
    [_SVSAuthToken release];
    [_SVSDeviceId release];
    [_mobileApplicationId release];
    [_applicationVersion release];
    [_firstLaunchDate release];
    [_lastTransactionNo release];
    [_location release];
    [_settings release];
    [super dealloc];
}


- (NSDictionary *)dictionaryRepresentation {
    NSMutableDictionary *ret = [[NSMutableDictionary alloc] init];
    
    [ret setValue:self.recCreated forKey:@"RecCreated"];
    [ret setValue:self.recModified forKey:@"RecModified"];
    [ret setValue:self.id.stringRepresentation forKey:@"DeviceId"];
    [ret setValue:self.deviceAgentId.stringRepresentation forKey:@"DeviceAgentId"];
    [ret setValue:self.locationId.stringRepresentation forKey:@"LocationId"];
    [ret setValue:self.deviceName forKey:@"DeviceName"];
    [ret setValue:self.deviceAlias forKey:@"DeviceAlias"];
    [ret setValue:@(self.deviceType) forKey:@"DeviceType"];
    [ret setValue:self.printServerIP forKey:@"PrintServerIP"];
    [ret setValue:@(self.printerPort) forKey:@"PrinterPort"];
    [ret setValue:@(self.scalesPort) forKey:@"ScalesPort"];
    [ret setValue:@(self.deviceNo) forKey:@"DeviceNo"];
    [ret setValue:self.shopperDisplayId.stringRepresentation forKey:@"ShopperDisplayId"];
    [ret setValue:@(self.isDeactivated) forKey:@"IsDeactivated"];
    [ret setValue:self.notes forKey:@"Notes"];
    [ret setValue:self.techInfo1 forKey:@"TechInfo1"];
    [ret setValue:self.techInfo2 forKey:@"TechInfo2"];
    [ret setValue:self.techInfo3 forKey:@"TechInfo3"];
    [ret setValue:self.techInfo4 forKey:@"TechInfo4"];
    [ret setValue:self.techInfo5 forKey:@"TechInfo5"];
    [ret setValue:self.techInfo6 forKey:@"TechInfo6"];
    [ret setValue:self.techInfo7 forKey:@"TechInfo7"];
    [ret setValue:self.techInfo8 forKey:@"TechInfo8"];
    [ret setValue:self.techInfo9 forKey:@"TechInfo9"];
    [ret setValue:self.techInfo10 forKey:@"TechInfo10"];
    [ret setValue:self.IPAddress forKey:@"IPAddess"];
    [ret setValue:@(self.latitude) forKey:@"Latitude"];
    [ret setValue:@(self.longitude) forKey:@"Longitude"];
    [ret setValue:self.systemName forKey:@"SystemName"];
    [ret setValue:self.systemVersion forKey:@"SystemVersion"];
    [ret setValue:self.model forKey:@"Model"];
    [ret setValue:self.mobileApplicationId forKey:@"MobileApplicationId"];
    [ret setValue:self.applicationVersion forKey:@"ApplicationVersion"];
    [ret setValue:self.firstLaunchDate forKey:@"FirstLaunchDate"];
    [ret setValue:self.SVSAuthToken forKey:@"SvsAuthToken"];
    [ret setValue:self.SVSDeviceId forKey:@"SvsDeviceId"];
    [ret setValue:@(self.locationDeviceCode) forKey:@"LocationDeviceCode"];
    [ret setValue:@(self.pushLogsToGae) forKey:@"PushLogsToGae"];
    [ret setValue:@(self.gaeLogSeverity) forKey:@"GaeLogSeverity"];
    [ret setValue:@(self.collectNetworkStats) forKey:@"CollectNetworkStats"];
    [ret setValue:@(self.isMultiWorkstation) forKey:@"isMultiWorkstation"];
        
    return [ret autorelease];
}

- (NSString *)deviceName {
    if (!_deviceName.length) {
        return [UIDevice currentDevice].name;
    }
    return _deviceName;
}

- (NSString *)systemName {
    if (!_systemName) {
        return [UIDevice currentDevice].systemName;
    }
    return _systemName;
}

- (NSString *)systemVersion {
    if (!_systemVersion) {
        return [UIDevice currentDevice].systemVersion;
    }
    return _systemVersion;
}

- (NSString *)model {
    if (_model) {
        return [UIDevice currentDevice].model;
    }
    return _model;
}

- (Location *)location {
    if (!_location && _locationId)
        _location = [[Location getInstanceById:_locationId] retain];
    return _location;
}

- (NSString *)deviceTypeStringRepresentation {
    switch (_deviceType) {
        case DeviceTypeIPad:
            return @"iPad";
            
        case DeviceTypeIPodIPhone:
            return @"iPhone/iPod";
            
        default:
            break;
    }
    return @"";
}

- (NSString *)IPAddress {
    if (!_IPAddress.length) {
        [_IPAddress release];
        _IPAddress = [LocalAddress() retain];
    }
    return _IPAddress;
}
-(NSString*)devicePlatformeStr{
    struct utsname systemInfo;
    uname(&systemInfo);
    
    NSString *platform = [NSString stringWithUTF8String:systemInfo.machine];
    return platform;
}

@end
