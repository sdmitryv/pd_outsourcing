//
//  BinaryStorageData.m
//  ProductBuilder
//
//  Created by Sergey Lugovoy on 2/16/16.
//  Copyright © 2016 Cloudworks. All rights reserved.
//

#import "BinaryStorageData.h"

@implementation BinaryStorageData

@synthesize ownerID, type, data, name, version;

-(void)dealloc{
    [ownerID release];
    [data release];
    [name release];
    [version release];
    [super dealloc];
}

+(NSArray *)getBinaryStorageDataWithOwnerID:(BPUUID *)ownerID {
    return @[];
}

@end
