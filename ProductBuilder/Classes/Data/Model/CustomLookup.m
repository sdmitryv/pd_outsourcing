//
//  CustomLookup.m
//  CloudworksPOS
//
//  Created by Vitaliy Gervazuk on 7/22/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "CustomLookup.h"
#import "CROEntity.h"

@implementation CustomLookup

@synthesize areaNum = _areaNum;
@synthesize fieldNum = _fieldNum;
@synthesize name = _name;
@synthesize alias = _alias;
@synthesize fullName = _fullName;

-(void)dealloc{
    
    [_alias release];
    [_name release];
    [_fullName release];
    [super dealloc];
}

+(NSMutableArray*)getLookups:(NSInteger)areaNum  lookupNum:(NSInteger)lookupNum {
    return nil;
}

- (NSString *)description {
    
    return _name;
}

@end
