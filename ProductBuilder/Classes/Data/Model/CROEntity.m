//
//  CROEntity.m
//  ProductBuilder
//
//  Created by Valera on 9/13/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "CROEntity.h"

@implementation CROEntity

@synthesize id = _id;
@synthesize rowid = _rowid;
@synthesize recCreated = _recCreated;
@synthesize recModified = _recModified;

-(id)initWithEntity:(CROEntity*)entity{
    if ((self=[super init])){
        _id = [entity->_id retain];
        _rowid = [entity->_rowid retain];
        _recCreated = [entity->_recCreated retain];
        _recModified = [entity->_recModified retain];
    }
    return self;
}

- (id)copyWithZone:(NSZone *)zone{
    return [[[self class] allocWithZone:zone] initWithEntity:self];
}

-(id)initWithRow:(id)row{
    if ((self=[super init])){
        [self initInternal];
	}
	return self;
}

-(instancetype)init{
	if ((self=[super init])){
        [self initInternal];
	}
	return self;
}

-(void)initInternal{
}

+(instancetype)getInstanceById:(BPUUID*)id{
	return nil;	
}

-(void)refresh {
}

-(void) dealloc
{
	[_id release];
	[_rowid release];
	[_recCreated release];
	[_recModified release];
    [_customRecId release];
	[super dealloc];
}

- (NSMethodSignature *)methodSignatureForSelector:(SEL)inSelector
{
	const char * encoding = method_getTypeEncoding(class_getInstanceMethod([self class], inSelector));
    if (encoding){
        NSString * strEncoding = [[NSString alloc]initWithUTF8String:encoding];
        NSRange range = [strEncoding rangeOfString:@(@encode(NSDecimal))];
        if (range.length > 0){
            [strEncoding autorelease];
            return [NSMethodSignature signatureWithObjCTypes:[[strEncoding stringByReplacingCharactersInRange:range withString:@"{?=iIIII[8S]}"]UTF8String]];
        }
        [strEncoding release];
    }
	return [super methodSignatureForSelector:inSelector];
}

-(void)setValue:(id)value forUndefinedKey:(NSString*)key{
	NSLog(@"invalid request for setting value '%@' for unknown key %@ in class %@", value, key,[self class]);
}

- (id)valueForUndefinedKey:(NSString *)key {
	return @"";
}

-(NSDictionary*)getProperties{
    return @{};
}

-(NSDictionary *)getJSONProperties {
    return [self getJSONPropertiesForDictionary:[self getProperties]];
}

-(NSObject *)jsonObjectFor:(NSObject *)object {
    if (!object) { 
        return nil;
    } else if ([object isKindOfClass:[NSDictionary class]]) {
        return [self getJSONPropertiesForDictionary:(NSDictionary *)object];
    } else if ([object isKindOfClass:[NSNamedMutableArray class]]) {
        return [self getJSONPropertiesForNamedMutableArray:(NSNamedMutableArray *)object];
    } else if ([object isKindOfClass:[NSNull class]]){
        return nil;
    } else if ([object isKindOfClass:[NSNumber class]]) {
        return object;
    } else if ([object isKindOfClass:[NSDate class]]) {
        return @(((NSDate *)object).timeIntervalSince1970);
    } else {
        return object.description;
    }
}

-(NSDictionary *)getJSONPropertiesForDictionary:(NSDictionary *)dictionary {
    NSMutableDictionary *resultDictionary = [NSMutableDictionary new];
    NSArray *keys = dictionary.allKeys;
    for (NSString *key in keys) {
        NSObject *value = [self jsonObjectFor:dictionary[key]];
        if (value) {
            resultDictionary[key] = value;
        }
    }
    return [resultDictionary autorelease];
}

-(NSArray *)getJSONPropertiesForNamedMutableArray:(NSNamedMutableArray *)namedArray {
    NSMutableArray *resultArray = [NSMutableArray new];
    for (NSObject *object in namedArray.data) {
        NSObject *value = [self jsonObjectFor:object];
        if (value) {
            [resultArray addObject:value];
        }
    }
    return [resultArray autorelease];
}

#pragma mark -
#pragma mark ILazyItem implementation

-(NSNumber*)rowid{
	return _rowid;
}

+(NSArray*)list{
    return nil;
}

+(NSArray *)listWithQuery:(NSString *)query {
    return nil;
}

-(BOOL)isEqual:(id)object{
   if (object == self) return YES;
   if (!object || ![object isKindOfClass:[self class]] || ![object respondsToSelector:@selector(id)]) return NO;
   return [self.id isEqual:[object id]];
}

@end
