//
//  CEntityList.m
//  CloudworksPOS
//
//  Created by valera on 8/4/11.
//  Copyright 2011 Retail Teamwork Ukraine. All rights reserved.
//

#import "CEntityList.h"

@interface CEntityList ()
@end

@implementation CEntityList
@synthesize owner;

-(id)init{
    if ((self=[super init])){
        list = [[NSMutableArray alloc]init];
        deletedList = [[NSMutableArray alloc]init];
        _unsafeEdit = 0;
    }
    return self;
}

-(id)initWithOwner:(id<CEntity>)anOwner{
    if ((self=[self init])){
        self.owner = anOwner;
    }
    return self;
}

-(id)initWithList:(NSMutableArray*)aList{
    return [self initWithList:aList deletedList:[NSMutableArray array]];
}

-(id)initWithList:(NSMutableArray*)aList deletedList:(NSMutableArray*)aDeletedList{
    if ((self=[super init])){
        list = [aList retain];
        deletedList = [aDeletedList retain];
        _unsafeEdit = 0;
    }
    return self;
}

-(void)dealloc{
    [list release];
    [deletedList release];
    [savedStates release];
    [super dealloc];
}

-(BOOL)readOnly{
    return ![self canEdit:nil];
}

-(BOOL)canEdit:(NSError **)error{
    return !owner || [owner canEdit:error];
}

-(BOOL)canEdit{
    return [self canEdit:nil];
}

- (NSMutableArray*)list{
    if (!list){
        list = [[NSMutableArray alloc]init];
    }
    return list;
}

- (NSArray*)deletedList{
    if (!deletedList){
         deletedList = [[NSMutableArray alloc]init];
    }
    return deletedList;
}

#pragma mark NSCopying
- (id)copyWithZone:(NSZone *)zone{
    NSMutableArray* aList = [list copyWithZone:zone];
    NSMutableArray* aDeletedlist = [deletedList copyWithZone:zone];
    id obj = [[[self class]allocWithZone:zone]initWithList:aList deletedList:aDeletedlist];
    [aList release];
    [aDeletedlist release];
    return obj;
}

#pragma mark NSMutableCopying

- (id)mutableCopyWithZone:(NSZone *)zone{
    NSMutableArray* aList = [list mutableCopyWithZone:zone];
    NSMutableArray* aDeletedlist = [deletedList mutableCopyWithZone:zone];
    id obj = [[[self class]allocWithZone:zone]initWithList:aList deletedList:aDeletedlist];
    [aList release];
    [aDeletedlist release];
    return obj;
}

#pragma mark NSCoding
- (void)encodeWithCoder:(NSCoder *)aCoder{
    [aCoder encodeObject:list forKey:@"list"];
    [aCoder encodeObject:deletedList forKey:@"deletedList"];
}

- (id)initWithCoder:(NSCoder *)aDecoder{
    if ((self=[super init])){
        list = [[aDecoder decodeObjectForKey:@"list"] retain];
        deletedList = [[aDecoder decodeObjectForKey:@"deletedList"] retain];
    }
    return self;
}

#pragma mark NSFastEnumeration

- (NSUInteger)countByEnumeratingWithState:(NSFastEnumerationState *)state objects:(id __unsafe_unretained [])stackbuf count:(NSUInteger)len{
    return [list countByEnumeratingWithState:state objects:stackbuf count:len];
}

#pragma mark methods

- (NSUInteger)count{
    return [list count];
}

- (id)objectAtIndex:(NSUInteger)index{
    return list[index];
}

-(id)objectAtIndexedSubscript:(NSUInteger)index{
    return list[index];
}

- (id)objectForKeyedSubscript:(id <NSCopying>)key{
    if ([(id)key isKindOfClass:[BPUUID class]]){
        BPUUID* id = (BPUUID*)key;
        for (CEntity* entity in self){
            if ([entity.id isEqual:id]){
                return entity;
            }
        }
    }
    else if ([(id)key isKindOfClass:[NSString class]]){
        NSString* id = (NSString*)key;
        for (CEntity* entity in self){
            if ([entity.id.description isEqual:id]){
                return entity;
            }
        }
    }
    return nil;
}

- (NSArray*)objectsAtIndexes:(NSIndexSet *)indexes{
    return [list objectsAtIndexes:indexes]; 
}

- (NSUInteger)indexOfObject:(id)anObject{
    return [list indexOfObject:anObject];
}

- (BOOL)containsObject:(id)anObject{
    return [list containsObject:anObject];
}

- (void)addObject:(id<CEntity>)anObject{
    if (!_unsafeEdit && ![self canAddObject:nil]) return;
    [list addObject:anObject];
    [self objectAdded:anObject];
}

- (void)addObjectsFromArray:(NSArray *)otherArray{
    if (!_unsafeEdit && ![self canAddObject:nil]) return;
    [list addObjectsFromArray:otherArray];
    for (CEntity *entity in otherArray) {
        [self objectAdded:entity];
    }
}

- (void)insertObject:(id<CEntity>)anObject atIndex:(NSUInteger)index{
    [list insertObject:anObject atIndex:index];
    [self objectAdded:anObject];
}

-(void)objectAdded:(id<CEntity>)anObject;{
}


-(BOOL)canAddObject:(NSError**)error{
    return TRUE;
}

-(BOOL)canRemoveObject:(id<CEntity>)object error:(NSError**)error{
    return TRUE;
}

- (void)removeObjectAtIndex:(NSUInteger)index{
    if (index >= list.count) return;
    id<CEntity> object = list[index];
    if (!_unsafeEdit && ![self canRemoveObject:object error:nil]) return;
    [object retain];
    [list removeObjectAtIndex:index];
    [self objectRemoved:object];
    [object release];
}

- (void)removeObjectAtIndexes:(NSIndexSet*)indexSet{
    __block NSMutableIndexSet* indexSetSkipped = nil;
    [indexSet enumerateIndexesUsingBlock:^(NSUInteger index, BOOL *stop) {
        id<CEntity> object = nil;
        if (index < list.count){
            object = list[index];
        }
        if (!object || (!_unsafeEdit && ![self canRemoveObject:object error:nil])){
            if (!indexSetSkipped){
                indexSetSkipped = [[NSMutableIndexSet alloc]init];
            }
            [indexSetSkipped addIndex:index];
        }
    }];
    NSMutableIndexSet* indexSetModified = nil;
    if (indexSetSkipped.count){
        indexSetModified = [[NSMutableIndexSet alloc]initWithIndexSet:indexSet];
        [indexSetModified removeIndexes:indexSetSkipped];
        indexSet = indexSetModified;
    }
    [indexSetSkipped release];
    NSArray* array = [list objectsAtIndexes:indexSet];
    [list removeObjectsAtIndexes:indexSet];
    for (id<CEntity> object in array){
        [self objectRemoved:object];
    }
    [indexSetModified release];
}

- (void)removeAllObjects{
    NSIndexSet* indexesToRemove = [[NSIndexSet alloc]initWithIndexesInRange:NSMakeRange(0, list.count)];
    [self removeObjectAtIndexes:indexesToRemove];
    [indexesToRemove release];
}

- (void)removeObject:(id<CEntity>)anObject{
    NSUInteger index = [list indexOfObject:anObject];
    if (index!=NSNotFound && index < [self count])
        [self removeObjectAtIndex:index];
}

-(void)objectRemoved:(id<CEntity>)anObject{
    if (![anObject isNew]){
        [anObject markDeleted];
        [deletedList addObject:anObject];
    }
}

- (void)sortUsingDescriptors:(NSArray *)sortDescriptors{
    [list sortUsingDescriptors:sortDescriptors];
}

- (void)enumerateObjectsUsingBlock:(void (^)(id obj, NSUInteger idx, BOOL *stop))block{
    [list enumerateObjectsUsingBlock:block];
}

#pragma mark -

- (BOOL)isDirty{
    if (self.deletedList.count) return TRUE;
    for(id<CEntity> item in self){
        if (item.isDirty) return TRUE;
    }
    return FALSE;
}

- (void)markNew{
    for(id<CEntity> item in self){
        [item markNew];
    }
}

- (void)markClean {
    for(id<CEntity> item in self){
        [item markClean];
    }
}

#pragma mark EditableObject

-(NSUInteger)editLevel{
    return _editLevel;
}

-(void)beginIgnoringReadonly{
    _ignoringReadonly++;
}

-(void)endIgnoringReadonly{
    _ignoringReadonly--;
}

-(void)beginEdit{
    if (!_ignoringReadonly && !self.canEdit) return;
    _isBeginningEdit = YES;
    _editLevel++;
    if (!savedStates){
        savedStates = [[NSMutableArray alloc]init];
    }
    for (id object in list){
        if ([object conformsToProtocol:@protocol(EditableObject)]){
            BOOL ignoringReadonly = _ignoringReadonly;
            if (ignoringReadonly){
                [object beginIgnoringReadonly];
            }
            objc_msgSend(object, @selector(beginEdit));
            if (ignoringReadonly){
                [object endIgnoringReadonly];
            }
        }
    }
    NSArray* listCopy = [[NSArray alloc]initWithArray:list]; //[list mutableCopy];
    NSArray* deletedListCopy =  [[NSArray alloc]initWithArray:deletedList];//[deletedList mutableCopy];
    NSMutableDictionary* currentState = [[NSMutableDictionary alloc] initWithDictionary:@{@"list": listCopy, @"deletedList": deletedListCopy}];
    [listCopy release];
    [deletedListCopy release];
    [savedStates addObject:currentState];
    [currentState release];
    _isBeginningEdit = NO;
}

-(void)cancelEdit{
    if (!_editLevel) return;
    _isCancellingEdit = TRUE;
    NSDictionary* newState = (NSDictionary*)[savedStates lastObject];
    //[list release];
    //[deletedList release];
    //list = [newState[@"list"] retain];
    //deletedList = [newState[@"deletedList"] retain];
    [list removeAllObjects];
    [list addObjectsFromArray:newState[@"list"]];
    [deletedList removeAllObjects];
    [deletedList addObjectsFromArray:newState[@"deletedList"]];
    for (id object in list){
        if ([object conformsToProtocol:@protocol(EditableObject)]){
            objc_msgSend(object, @selector(cancelEdit));
        }
    }
    [savedStates removeLastObject];
    _isCancellingEdit = FALSE;
    _editLevel--;
}

-(void)endEdit{
    if (!_editLevel) return;
    _isEndingEdit = YES;
    for (id object in list){
        if ([object conformsToProtocol:@protocol(EditableObject)]){
            objc_msgSend(object, @selector(endEdit));
        }
    }
    [savedStates removeLastObject];
    _isEndingEdit = NO;
    _editLevel--;
}

-(BOOL)copyEditLevelsToEntity:(NSObject<EditableObject>*)value{
    if ([self class]!=[value class]) return FALSE;
    CEntityList* toEntity = (CEntityList*)value;
    for (CEntity* toObject in toEntity){
        for (CEntity* object in list){
            if ([object.id isEqual:toObject.id]){
                [object copyEditLevelsToEntity:toObject];
            }
        }
    }
    toEntity->_editLevel = _editLevel;
    [toEntity->savedStates release];
    toEntity->savedStates = [savedStates retain];
    return TRUE;
}

-(void)beginUnsafeListEditing {
    _unsafeEdit++;
}
-(void)endUnsafeListEditing {
    if (!_unsafeEdit) return;
    _unsafeEdit--;
}

- (BOOL)shouldEncodeProperty:(NSString *)name{
    if ([name isEqualToString:@"owner"]
        || [name isEqualToString:@"_editLevel"]
        || [name isEqualToString:@"_isCancellingEdit"]
        || [name isEqualToString:@"_isBeginingEdit"]
        || [name isEqualToString:@"_isEndingEdit"]
        || [name isEqualToString:@"_savedStates"]
        ){
        return FALSE;
    }
    return TRUE;
}

@end
