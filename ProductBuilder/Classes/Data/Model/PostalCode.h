//
//  PostalCode.h
//  CloudworksPOS
//
//  Created by Lulakov Viacheslav on 7/14/11.
//  Copyright 2011 CloudWorks. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CROEntity.h"

@interface PostalCode : CROEntity {
	NSString *_postalCode;
	NSString *_city;
	NSString *_state;
	NSString *_country;
	NSString *_stateCode;
	NSString *_countryCode;
}

@property(nonatomic,retain) NSString * postalCode;
@property(nonatomic,retain) NSString * city;
@property(nonatomic,retain) NSString * state;
@property(nonatomic,retain) NSString * country;
@property(nonatomic,retain) NSString * stateCode;
@property(nonatomic,retain) NSString * countryCode;

+(PostalCode *)getPostalCode:(NSString *)code;
@end
