//
//  URLSettingHelper.h
//  ProductBuilder
//
//  Created by Alexander Martyshko on 7/28/15.
//  Copyright (c) 2015 Cloudworks. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface URLSettingHelper : NSObject
+(NSString *)processUrlString:(NSString *)urlString;
@end
