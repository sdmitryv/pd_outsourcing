//
//  AttributeSet.m
//  Buyer
//
//  Created by Lulakov Viacheslav on 11/2/10.
//  Copyright 2010 DBBestTechnologies. All rights reserved.
//

#import "AttributeSet.h"
#import "NSString+Ext.h"

@implementation AttributeSet

@synthesize code = _code;
@synthesize description = _description;
@synthesize attributeLabel = _attributeLabel;

-(id)init {
    
	if ((self = [super init])) {

	}
    
	return self;
}

-(void) dealloc {
    
	[_code release];
	[_description release];
    [_attributeLabel release];
	
	[super dealloc];
}

+(NSMutableArray *)loadAttributeSetByCode:(NSString *)code { return nil; }

@end
