//
//  Employee.m
//  ProductBuilder
//
//  Created by Lulakov Viacheslav on 10/18/10.
//  Copyright 2010 DBBestTechnologies. All rights reserved.
//

#import "Employee.h"
//static Employee *_currentEmployee = nil;
//static BOOL _currentEmployeeReady = FALSE;

@implementation Employee

-(void) dealloc {
    
	[_loginName release];
	[_lastName release];
	[_firstName release];
	[_code release];
	[_nickName release];
	[_password release];
    [_locationID release];
    [_lastLoginDate release];
    [_passwordChangeDate release];
	[super dealloc];
}


+(Employee *)getEmployeesByName:(NSString *)loginName andPassword:(NSString *)password isAll:(BOOL)isAll {
    return nil;
}

-(NSString *)initials {
    
    NSString *first = (_firstName ? [_firstName stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] : @"").uppercaseString;
    NSString *last = (_lastName ? [_lastName stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] : @"").uppercaseString;
    
    if (first.length > 0 || last.length > 0)
        return [NSString stringWithFormat:@"%@%@", first.length > 0 ? [first substringToIndex:1] : @"", last.length > 0 ? [last substringToIndex:1] : @""];
    
    NSString *login = (_loginName ? [_loginName stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] : @"").uppercaseString;
    return (login.length > 1 ? [login substringToIndex:2] : (login.length > 0 ? [login substringToIndex:1] : @""));
}


-(NSString *)shortName {
    
    NSString *nick = [_nickName stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    if (nick.length > 0) {
        return nick;
    }
    
    NSString *first = _firstName ? [_firstName stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] : @"";
    NSString *last = _lastName ? [_lastName stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] : @"";
    
    if (first.length > 0 || last.length > 0)
        return [NSString stringWithFormat:@"%@%@", first.length > 0 ? [first substringToIndex:1] : @"", last.length > 0 ? [last substringToIndex:1] : @""];
    
    NSString *login = (_loginName ? [_loginName stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] : @"").uppercaseString;
    return (login.length > 0 ? login : @"");
}

-(NSString *)shortNameFormatted {
    
    if (_nickName.length > 0)
        return [_nickName stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    NSString *first = _firstName ? [_firstName stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] : @"";
    NSString *last = _lastName ? [_lastName stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] : @"";
    return [NSString stringWithFormat:@"%@ %@", first.length > 0 ? first : @"", last.length > 0 ? (first.length > 0 ? [NSString stringWithFormat:@"%@.", [last substringToIndex:1]] : last) : @""];
}



-(NSString *)description {
    return self.displayName;
}


-(NSString*)displayName{
    return self.nickName.length ? self.nickName : self.loginName.length ? self.loginName : self.code;
}


- (BOOL)updateLastLoginDateWithDate:(NSDate *)date error:(NSError **)error {
    return NO;
}


-(NSDictionary *)getProperties {
    return nil;
};

-(void)updateDiscountLimitsFromRole{
}

-(void)resetDiscountLoadFlag {

    objc_setAssociatedObject(self, _cmd, (@0), OBJC_ASSOCIATION_RETAIN);
}


-(void)setMaxDiscPercent:(NSDecimal)value{
    @synchronized (self) {
        _maxDiscPercent =value;
    }
}

-(NSDecimal)maxDiscPercent{
    @synchronized (self) {
        return self.overrideRoleDiscountLimits ? _maxDiscPercent : self.maxDiscPercentRole;
    }
}

-(void)setMaxGlobalDiscPercent:(NSDecimal)value{
    @synchronized (self) {
        _maxGlobalDiscPercent = value;
    }
}

-(NSDecimal)maxGlobalDiscPercent{
    @synchronized (self) {
        return self.overrideRoleDiscountLimits ? _maxGlobalDiscPercent : self.maxGlobalDiscPercentRole;
    }
}

-(void)setDiscRequireAuthCode:(BOOL)value{
    @synchronized (self) {
        _discRequireAuthCode = value;
    }
}

-(BOOL)discRequireAuthCode{
    @synchronized (self) {
        return self.overrideRoleDiscountLimits ? _discRequireAuthCode : self.discRequireAuthCodeRole;
    }
}

-(void)setMaxDiscPercentRole:(NSDecimal)value{
    @synchronized (self) {
        _maxDiscPercentRole = value;
    }
}

-(NSDecimal)maxDiscPercentRole{
    @synchronized (self) {
        [self updateDiscountLimitsFromRole];
        return _maxDiscPercentRole;
    }
}

-(void)setMaxGlobalDiscPercentRole:(NSDecimal)value{
    @synchronized (self) {
        _maxGlobalDiscPercentRole = value;
    }
}

-(NSDecimal)maxGlobalDiscPercentRole{
    @synchronized (self) {
        [self updateDiscountLimitsFromRole];
        return _maxGlobalDiscPercentRole;
    }
}

-(void)setDiscRequireAuthCodeRole:(BOOL)value{
    @synchronized (self) {
    _discRequireAuthCodeRole = value;
    }
}

-(BOOL)discRequireAuthCodeRole{
    @synchronized (self) {
        [self updateDiscountLimitsFromRole];
        return _discRequireAuthCodeRole;
    }
}

@end
