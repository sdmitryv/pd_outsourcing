//
//  AttributeSetValue+Sqlite.m
//  Buyer
//
//  Created by Lulakov Viacheslav on 11/2/10.
//  Copyright 2010 DBBestTechnologies. All rights reserved.
//
#import "AttributeSetValue.h"
#import "DataManager.h"
#import "DBAdditions.h"
#import "CROEntity+Sqlite.h"
#import "CustomField.h"
#import "NSString+Ext.h"


#define attrSetvalSelectSTMT @"SELECT * FROM AttributeSetValue atsv"

NSString * const attrSetValSelectStm = @""attrSetvalSelectSTMT"";


@implementation AttributeSetValue (Sqlite)

////////////////////////////////////////////////////////////
+(void)load{
    
    [[self class] exchangeMethod:@selector(loadAttributeSetValuesForSetCode:)
                   withNewMethod:@selector(loadAttributeSetValuesForSetCodeImpl:)];

    [[self class] exchangeMethod:@selector(hasAttributeSetValuesWithAlias:)
                   withNewMethod:@selector(hasAttributeSetValuesWithAliasImpl:)];
    
    [[self class] exchangeMethod:@selector(loadAttributeSetValuesForSetCode:attrName:andStyle:)
                   withNewMethod:@selector(loadAttributeSetValuesForSetCodeImpl:attrName:andStyle:)];
}


////////////////////////////////////////////////////////////
+(NSMutableArray *)loadAttributeSetValuesForSetCodeImpl:(NSString *)code {
    
    NSMutableArray *list = [NSMutableArray array];
    
    if (code == nil || [@"" isEqualToString:code])
        return list;
    
    
    NSString *sqlStm = [NSString stringWithFormat:@"%@ WHERE SetCode = @code", attrSetValSelectStm];

    NSString *sql = [NSString stringWithFormat:@"%@ ORDER BY [Order], [Value]", sqlStm];

    
    DataManager *dbManager =[DataManager instance];
	EGODatabase *db = dbManager.currentDatabase;
    NSMutableDictionary* parameters = [NSMutableDictionary dictionary];
    [parameters setObjectNilSafe:code forKey:@"code"];
    
	EGODatabaseResult* queryResult = [db executeQuery:sql namedParameters:parameters];
	for(EGODatabaseRow* row in queryResult) {
        
		AttributeSetValue *attrSet = (AttributeSetValue*)[[self class] instanceFromRow:row];
		[list addObject:attrSet];
	}
    
	return list;
}

////////////////////////////////////////////////////////////
+(NSMutableArray *)loadAttributeSetValuesForSetCodeImpl:(NSString *)code attrName:(NSString *)attrName andStyle:(BPUUID *)styleId {
    
    NSMutableArray *list = [NSMutableArray array];
    
    if (code == nil || [@"" isEqualToString:code])
        return list;
    
    
    NSString *sql = [NSString stringWithFormat:@"%@  WHERE SetCode = @code AND EXISTS (SELECT 1 FROM InvenItem WHERE StyleId = @styleId AND atsv.value = %@ collate nocase) ORDER BY [Order], [Value]", attrSetValSelectStm, attrName];
    
    DataManager *dbManager =[DataManager instance];
    EGODatabase *db = dbManager.currentDatabase;
    NSMutableDictionary* parameters = [NSMutableDictionary dictionary];
    [parameters setObjectNilSafe:code forKey:@"code"];
    [parameters setObjectNilSafe:styleId forKey:@"styleId"];
    
    EGODatabaseResult* queryResult = [db executeQuery:sql namedParameters:parameters];
    for(EGODatabaseRow* row in queryResult) {
        
        AttributeSetValue *attrSet = (AttributeSetValue*)[[self class] instanceFromRow:row];
        [list addObject:attrSet];
    }
    
    return list;
}

////////////////////////////////////////////////////////////
+(BOOL)hasAttributeSetValuesWithAliasImpl:(NSString *)code {
    
    if (code == nil || [@"" isEqualToString:code])
        return NO;
    
    BOOL res = NO;
    
    NSString *sqlStm = [NSString stringWithFormat:@"%@ WHERE SetCode = @code AND (length(ifnull(alias,'')) > 0 OR length(ifnull(alias2,'')) > 0)", attrSetValSelectStm];
    NSString *sql = [NSString stringWithFormat:@"SELECT COUNT(1) cnt FROM (%@)", sqlStm];
    
    
    DataManager *dbManager =[DataManager instance];
    EGODatabase *db = dbManager.currentDatabase;
    NSMutableDictionary* parameters = [NSMutableDictionary dictionary];
    [parameters setObjectNilSafe:code forKey:@"code"];
    
    EGODatabaseResult* queryResult = [db executeQuery:sql namedParameters:parameters];
    for(EGODatabaseRow* row in queryResult)
        res = [row intForColumn:@"cnt"] > 0;
    
    return res;
}


////////////////////////////////////////////////////////////
-(void)fetch:(EGODatabaseRow *)row {
    
	[super fetch:row];
	
    self.attributeSetId = [row UuidForColumn:@"AttributeSetID"];
    self.id = [row UuidForColumn:@"AttributeSetValueID"];
    self.setCode = [row stringForColumn:@"SetCode"];
    self.value = [row stringForColumn:@"Value"];
    self.alias = [row stringForColumn:@"alias"];
    self.alias2 = [row stringForColumn:@"alias2"];
    self.order = [row intForColumn:@"Order"];
}

@end
