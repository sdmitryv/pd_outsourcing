//
//  CustomRequiredField+Sqlite.m
//  ProductBuilder
//
//  Created by Viacheslav Lulakov on 11/15/16.
//  Copyright © 2016 Cloudworks. All rights reserved.
//

#import "CustomRequiredField.h"
#import "CROEntity+Sqlite.h"

@implementation CustomRequiredField(Sqlite)

+ (id)getInstanceById:(BPUUID*)id {
    EGODatabaseResult* result = [[DataManager instance].currentDatabase
                                 executeQueryWithParameters:@"select * from CustomRequiredField where CustomRequiredFieldID=?", [id description], nil];
    if ([result count] > 0) {
        return [CustomRequiredField instanceFromRow:[result rowAtIndex:0]];
    }
    return nil;
}

- (void)fetch:(EGODatabaseRow *)row {
    [super fetch:row];
    self.id = [row UuidForColumn:@"CustomRequiredFieldID"];
    self.fieldLabel = [row stringForColumn:@"FieldCaption"];
    self.fieldName = [[row stringForColumn:@"FieldName"] lowercaseString];
    
    if ([self.fieldName hasPrefix:@"customlookup"]) {
        self.fieldName = [NSString stringWithFormat:@"%@id", self.fieldName];
    }
    
    self.requireSymbol = [[row stringForColumn:@"Require"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    self.convertEntry = [row intForColumn:@"ConvertEntry"];
    self.code = [row intForColumn:@"Code"];
}

+(void)load{
    EXCHANGE_METHOD(getRequiredFieldsListForArea:, getRequiredFieldsListForAreaImpl:);
    EXCHANGE_METHOD(getCaseSensitiveFieldsListForArea:, getCaseSensitiveFieldsListForAreaImpl:);
    EXCHANGE_METHOD(getSearchFieldsListForArea:, getSearchFieldsListForAreaImpl:);
}

+ (NSArray*)getRequiredFieldsListForAreaImpl:(CustomRequiredFieldArea)area {
    DataManager *dbManager =[DataManager instance];
    EGODatabase *db = dbManager.currentDatabase;
    NSMutableArray *list = [[NSMutableArray alloc] init];
    EGODatabaseResult* result = [db executeQuery:[NSString stringWithFormat:@"SELECT rowid, * FROM CustomRequiredField WHERE AreaNum = %i AND IsDeleted != 1 ORDER BY Require", (int)area]];
    for(EGODatabaseRow* row in result) {
        CustomRequiredField* item = [[self class] instanceFromRow:row];
        if (item.requireSymbol.length > 0) {
            [list addObject:item];
        }
    }
    return [list autorelease];
}

+ (NSArray*)getCaseSensitiveFieldsListForAreaImpl:(CustomRequiredFieldArea)area {
    DataManager *dbManager =[DataManager instance];
    EGODatabase *db = dbManager.currentDatabase;
    NSMutableArray *list = [[NSMutableArray alloc] init];
    EGODatabaseResult* result = [db executeQuery:[NSString stringWithFormat:@"SELECT rowid, * FROM CustomRequiredField WHERE AreaNum = %i AND IsDeleted != 1 AND ConvertEntry != 0", (int)area]];
    for(EGODatabaseRow* row in result) {
        id item = [[self class] instanceFromRow:row];
        [list addObject:item];
    }
    return [list autorelease];
}

+ (NSArray *)getSearchFieldsListForAreaImpl:(CustomRequiredFieldArea)area {
    DataManager *dbManager =[DataManager instance];
    EGODatabase *db = dbManager.currentDatabase;
    NSMutableArray *list = [[NSMutableArray alloc] init];
    EGODatabaseResult* result = [db executeQuery:[NSString stringWithFormat:@"SELECT rowid, * FROM CustomRequiredField WHERE AreaNum = %i AND Searchable > 0 AND SearchDisplayOrder != 0 AND IsDeleted != 1 ORDER BY SearchDisplayOrder", (int)area]];
    for(EGODatabaseRow* row in result) {
        id item = [[self class] instanceFromRow:row];
        [list addObject:item];
    }
    return [list autorelease];
}

@end
