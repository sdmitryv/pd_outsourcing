//
//  CustomField+Sqlite.m
//  CloudworksPOS
//
//  Created by Vitaliy Gervazuk on 7/19/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "CustomField.h"
#import "DBAdditions.h"
#import "CROEntity+Sqlite.h"

@implementation CustomField (Sqlite)

-(void)fetch:(EGODatabaseRow *)row{
    [super fetch:row];
    self.recCreated = [row dateForColumn:@"RecCreated"];
	self.recModified = [row dateForColumn:@"RecModified"];
	self.id = [row UuidForColumn:@"CustomFieldsControlID"];
    self.label = [row stringForColumn:@"Label"];
    self.defaultLabel = [row stringForColumn:@"DefaultLabel"];
    self.used = [row boolForColumn:@"Used"];
    self.type = [row intForColumn:@"Type"];
    self.language = [row intForColumn:@"Language"];
    self.customFieldType = [row intForColumn:@"CustomFieldType"];
    self.customFieldNumber = [row intForColumn:@"CustomFieldNumber"];
    self.Secure = [row boolForColumn:@"Secure"];
    self.listOrder = [row intForColumn:@"ListOrder"];
    self.isRequired = [row boolForColumn:@"Required"];
}

+(void)load{
    [[self class] exchangeMethod:@selector(getCustomFieldControls:fieldType:fieldNum:) withNewMethod:@selector(getCustomFieldControlsImpl:fieldType:fieldNum:)];
    [[self class] exchangeMethod:@selector(getCustomFieldsByArea:) withNewMethod:@selector(getCustomFieldsByAreaImpl:)];
}


+(NSArray*)getCustomFieldControlsImpl:(NSInteger)areaType fieldType:(int)fieldType fieldNum:(int)fieldNum {
    DataManager *dbManager =[DataManager instance];
	EGODatabase *db = dbManager.currentDatabase;
	NSMutableArray *list = [[NSMutableArray alloc] init];
    NSString *selectStatement = @"SELECT rowid, * FROM CustomFieldsControl WHERE Type = ? AND CustomFieldType = ? AND CustomFieldNumber = ?";
    NSMutableArray *parameters = [[NSMutableArray alloc]init];
    [parameters addInteger:areaType];
    [parameters addInteger:fieldType];
    [parameters addInteger:fieldNum];
	EGODatabaseResult* result = [db executeQuery:selectStatement parameters:parameters];
	for(EGODatabaseRow* row in result) {
		id item = [[self class] instanceFromRow:row];
		[list addObject:item];
	}
    
    [parameters release];
	return [list autorelease];
}

+(NSArray*)getCustomFieldsByAreaImpl:(NSInteger)area {
    DataManager *dbManager =[DataManager instance];
    EGODatabase *db = dbManager.currentDatabase;
    NSMutableArray *list = [[NSMutableArray alloc] init];
    NSString *selectStatement = @"SELECT rowid, * FROM CustomFieldsControl WHERE Type = ? AND Used = 1 ORDER BY ListOrder, Required, CASE WHEN length(trim(ifnull(Label,''))) > 0 THEN Label ELSE DefaultLabel END";
    NSMutableArray *parameters = [[NSMutableArray alloc]init];
    [parameters addInteger:area];
    
    EGODatabaseResult* result = [db executeQuery:selectStatement parameters:parameters];
    for(EGODatabaseRow* row in result) {
        id item = [[self class] instanceFromRow:row];
        [list addObject:item];
    }
    
    [parameters release];
    return [list autorelease];
}

@end
