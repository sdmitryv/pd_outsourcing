//
//  Setting+Sqlite.m
//  CloudworksPOS
//
//  Created by valera on 9/5/11.
//  Copyright 2011 Retail Teamwork Ukraine. All rights reserved.
//

#import "Setting.h"
#import "CROEntity+Sqlite.h"

@implementation Setting(Sqlite)

-(void)fetch:(EGODatabaseRow *)row {
	[super fetch:row];
	self.id = [row UuidForColumn:@"RecID"];
	self.type = [row stringForColumn:@"Type"];
	self.key = [row stringForColumn:@"Key"];
	self.value = [row stringForColumn:@"Value"];
    self.designate = [row UuidForColumn:@"Designate"];
}

+(void)load{
    [[self class] exchangeMethod:@selector(getSettings) withNewMethod:@selector(getSettingsImpl)];
}

+(NSMutableArray*)getSettingsImpl
{
	DataManager *dbManager =[DataManager instance];
	EGODatabase *db = dbManager.currentDatabase;
	NSMutableArray *settingArray = [[NSMutableArray alloc] init];
	EGODatabaseResult* result = [db executeQuery:@"SELECT * FROM Setting"];
	for(EGODatabaseRow* row in result) {
		Setting *setting = [Setting instanceFromRow:row];
		[settingArray addObject:setting];
	}
	return [settingArray autorelease];
}

+ (Setting *)getSettingForKey:(NSString *)key type:(NSString*)type designate:(BPUUID*)designate{
	Setting *setting = nil;
	DataManager *dbManager =[DataManager instance];
	EGODatabase *db = dbManager.currentDatabase;
    NSMutableDictionary* params = [[NSMutableDictionary alloc]init];
    [params setObjectNilSafe:key forKey:@"Key"];
    [params setObjectNilSafe:designate forKey:@"Designate"];
    [params setObjectNilSafe:type forKey:@"Type"];
	EGODatabaseResult* result = [db executeQuery:@"SELECT * FROM Setting WHERE Key = @Key and Type=@Type and (@Designate is null and designate is null or (designate=@Designate))" namedParameters:params];
    [params release];
	for(EGODatabaseRow* row in result) {
		setting = [Setting instanceFromRow:row];
		break;
	}
	return setting;
}
@end
