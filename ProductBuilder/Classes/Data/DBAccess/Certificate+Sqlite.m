//
//  Certificate+Sqlite.m
//  ProductBuilder
//
//  Created by Виталий Гервазюк on 3/16/12.
//  Copyright (c) 2012 Cloudworks. All rights reserved.
//

#import "Certificate.h"
#import "CROEntity+Sqlite.h"

@implementation Certificate(Sqlite)

+(void)load{
    [[self class] exchangeMethod:@selector(getLatesCertificateByName:) withNewMethod:@selector(getLatesCertificateByNameImpl:)];
}

+(Certificate *)getLatesCertificateByNameImpl:(NSString *)name {
    DataManager *dbManager =[DataManager instance];
	EGODatabase *db = dbManager.currentDatabase;
    
	EGODatabaseResult* result = [db executeQueryWithParameters:
                                 @"SELECT rowid, * FROM Certificate WHERE FileName = ? ORDER BY RecModified DESC", name, nil];
    Certificate* cert = nil;
	for(EGODatabaseRow* row in result) {
		cert = [[self class] instanceFromRow:row];
		break;
	}
	
    return cert;
}

-(void)fetch:(EGODatabaseRow *)row {
    [super fetch:row];
    
    self.fileName = [row stringForColumn:@"FileName"];
    self.fileContent = [row dataForColumn:@"FileContent"];
}

@end
