//
//  TableCustomField+Sqlite.m
//  CloudworksPOS
//
//  Created by
//  Copyright 2015 __MyCompanyName__. All rights reserved.
//

#import "TableCustomField.h"
#import "DBAdditions.h"
#import "CROEntity+Sqlite.h"

@implementation TableCustomField (Sqlite)

-(void)fetch:(EGODatabaseRow *)row{
    
    [super fetch:row];

    self.recCreated = [row dateForColumn:@"RecCreated"];
	self.recModified = [row dateForColumn:@"RecModified"];
	self.id = [row UuidForColumn:@"CustomFieldTypeId"];
    self.code = [row stringForColumn:@"Code"];
    if (!self.code)
        self.code = [row stringForColumn:@"FieldCode"];
    self.fieldTypeStr = [row stringForColumn:@"FieldType"];
    self.title = [row stringForColumn:@"Title"];
    self.description = [row stringForColumn:@"Description"];
    self.lookupTable = [row stringForColumn:@"LookupTable"];
    self.lookupTablePK = [row stringForColumn:@"LookupTablePK"];
    self.lookupTableDescription = [row stringForColumn:@"LookupTableDescription"];
    self.lookupText = [row stringForColumn:@"LookupText"];
    self.lookupDefaultText = [row stringForColumn:@"LookupDefaultText"];
    self.require = [row boolForColumn:@"Require"];
    
    self.stringValue = [row stringForColumn:@"StringValue"];
    self.numberValue = [row numberForColumn:@"NumberValue"];
    self.lookupId = [row UuidForColumn:@"LookupId"];
}

@end
