//
//  BinaryStorageData+Sqlite.m
//  ProductBuilder
//
//  Created by Sergey Lugovoy on 2/16/16.
//  Copyright © 2016 Cloudworks. All rights reserved.
//

#import "BinaryStorageData.h"
#import "CROEntity+Sqlite.h"

@implementation BinaryStorageData (Sqlite)

+(void)load {
    [[self class] exchangeMethod:@selector(getBinaryStorageDataWithOwnerID:) withNewMethod:@selector(getBinaryStorageDataWithOwnerIDImpl:)];
}

-(void)fetch:(EGODatabaseRow *)row {
    [super fetch:row];
    self.id = [row UuidForColumn:@"BinaryStorageID"];
    self.ownerID = [row UuidForColumn:@"OwnerID"];
    self.type = [row intForColumn:@"Type"];
    self.data = [row dataForColumn:@"Data"];
    self.name = [row stringForColumn:@"Name"];
    self.version = [row stringForColumn:@"Version"];
}

+(NSArray *)getBinaryStorageDataWithOwnerIDImpl:(BPUUID *)ownerID {
    static NSString *selectQuery = @"SELECT * FROM BinaryStorage WHERE OwnerID = ?";
    
    NSMutableArray *binaryStorageDataArray = [NSMutableArray new];
    
    EGODatabaseResult *result = [[DataManager instance].currentDatabase executeQuery:selectQuery parameters:@[ownerID.description]];
    
    if (result.count > 0) {
        
        for (EGODatabaseRow *row in result.rows) {
            BinaryStorageData *binaryStorageData = [[BinaryStorageData alloc] initWithRow:row];
            [binaryStorageDataArray addObject:binaryStorageData];
            [binaryStorageData release];
        }
        
    }
    
    return [binaryStorageDataArray autorelease];
}

@end
