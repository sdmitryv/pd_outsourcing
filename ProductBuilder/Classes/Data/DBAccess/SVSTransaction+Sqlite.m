//
//  SVSTransaction+Sqlite.m
//  ProductBuilder
//
//  Created by Viacheslav Lulakov on 1/24/14.
//  Copyright (c) 2014 Cloudworks. All rights reserved.
//

#import "SVSTransaction.h"
#import "CEntity+Sqlite.h"

@implementation SVSTransaction (Sqlite)

-(void)fetch:(EGODatabaseRow *)row {
	[super fetch:row];
    
    self.type = [row intForColumn:@"Type"];
    self.account = [row stringForColumn:@"Account"];
    _status = [row intForColumn:@"Status"];
    self.amount = [row decimalForColumn:@"Amount"];
    self.receiptID = [row UuidForColumn:@"ReceiptID"];
    self.receiptLineID = [row UuidForColumn:@"ReceiptLineID"];
    self.receiptLineType = [row intForColumn:@"ReceipLineType"];
    self.transactionID = [row stringForColumn:@"TransactionID"];
    self.employeeID = [row UuidForColumn:@"EmployeeID"];
    self.managerOverrideID = [row UuidForColumn:@"ManagerOverrideID"];
    self.isOffline = [row boolForColumn:@"IsOffline"];
    self.afterBalance = [row decimalForColumn:@"AfterBalance"];
    self.locationID = [row UuidForColumn:@"LocationID"];
    self.deviceUniqueID = [row UuidForColumn:@"DeviceUniqueID"];
    self.deviceName = [row stringForColumn:@"DeviceName"];
    self.programID = [row stringForColumn:@"ProgramID"];
    self.localTransactionTime = [row dateForColumn:@"LocalTransactionTime"];
    self.isRefund = [row intForColumn:@"isRefund"];
	[self markClean];
}

-(BOOL)save:(EGODatabase*)db error:(NSError**)error{
	
    if (!self.isDirty || (self.isDeleted && self.isNew))
        return TRUE;
    //update properties from parent object
    
	NSString * const stmtInsert = @"REPLACE INTO SVSTransaction (RecCreated, RecModified, SVSTransactionID, Type,\
	Account, Status, Amount, ReceiptID, ReceiptLineID, ReceiptLineType, TransactionID, EmployeeID, ManagerOverrideID, IsOffline,\
    AfterBalance, LocationID, DeviceUniqueID, DeviceName, ProgramID, LocalTransactionTime, isRefund, SyncState) \
	VALUES (@RecCreated, @RecModified, @SVSTransactionID, @Type,\
	@Account, @Status, @Amount, @ReceiptID, @ReceiptLineID, @ReceiptLineType, @TransactionID, @EmployeeID, @ManagerOverrideID, @IsOffline,\
    @AfterBalance, @LocationID, @DeviceUniqueID, @DeviceName, @ProgramID, @LocalTransactionTime, @isRefund, 1)";
    
    NSString* const stmtUpdate = @"UPDATE SVSTransaction set RecModified = @RecModified, Type = @Type,\
	Account = @Account, Status = @Status, Amount = @Amount, ReceiptID = @ReceiptID, ReceiptLineID = @ReceiptLineID,\
    ReceiptLineType = @ReceiptLineType, TransactionID = @TransactionID, EmployeeID = @EmployeeID, ManagerOverrideID = @ManagerOverrideID,\
    IsOffline = @IsOffline, AfterBalance = @AfterBalance, LocationID = @LocationID,\
    DeviceUniqueID = @DeviceUniqueID, DeviceName = @DeviceName, ProgramID = @ProgramID, LocalTransactionTime = @LocalTransactionTime, isRefund=@isRefund,\
    SyncState = IFNULL(SyncState,0) + 1\
    where SVSTransactionID=@SVSTransactionID";
    
    NSString* const stmtDelete = @"delete from SVSTransaction where SVSTransactionID=@SVSTransactionID";
    NSString* stmt = nil;
	NSMutableDictionary* parameters = [[NSMutableDictionary alloc]init];
    
    [parameters setObjectNilSafe:self.id forKey:@"SVSTransactionID"];
    
    if (self.isDeleted){
        stmt = stmtDelete;
    }
    else{
        stmt = self.isNew ? stmtInsert : stmtUpdate;
        if (self.isNew){
            [parameters setObjectNilSafe:_recCreated forKey:@"RecCreated"];
        }
        [parameters setObjectNilSafe:_recModified forKey:@"RecModified"];
        [parameters setInteger:self.type forKey:@"Type"];
        [parameters setObjectNilSafe:self.account forKey:@"Account"];
        [parameters setInt:self.status forKey:@"Status"];
        [parameters setDecimal:self.amount forKey:@"Amount"];
        [parameters setObjectNilSafe:self.receiptID forKey:@"ReceiptID"];
        [parameters setObjectNilSafe:self.receiptLineID forKey:@"ReceiptLineID"];
        [parameters setInt:self.receiptLineType forKey:@"ReceiptLineType"];
        [parameters setObjectNilSafe:self.transactionID forKey:@"TransactionID"];
        [parameters setObjectNilSafe:self.employeeID forKey:@"EmployeeID"];
        [parameters setObjectNilSafe:self.managerOverrideID  forKey:@"ManagerOverrideID"];
        [parameters setBool:self.isOffline forKey:@"IsOffline"];
        [parameters setDecimal:self.afterBalance forKey:@"AfterBalance"];
        [parameters setObjectNilSafe:self.locationID forKey:@"LocationID"];
        [parameters setObjectNilSafe:self.deviceUniqueID forKey:@"DeviceUniqueID"];
        [parameters setObjectNilSafe:self.deviceName forKey:@"DeviceName"];
        [parameters setObjectNilSafe:self.programID forKey:@"ProgramID"];
        [parameters setObjectNilSafe:self.localTransactionTime forKey:@"LocalTransactionTime"];
        [parameters setBool:self.isRefund forKey:@"isRefund"];
	}
	BOOL result = [db executeUpdate:stmt namedParameters:parameters];
	[parameters release];
	if (!result && [db hadError]){
        [[self class] writeDbError:db error:error domain:@"com.SVSTransaction.save"];
	}
    if (result && !self.isDeleted){
        [self markOld];
    }
	return result;
}

@end
