//
//  CEntity+Sqlite.m
//  iPadPOS
//
//  Created by Lulakov Viacheslav on 3/11/11.
//  Copyright 2011 DBBestTechnologies. All rights reserved.
//

#import "CEntity+Sqlite.h"

@implementation CEntity (Sqlite)

-(id)initWithRow:(EGODatabaseRow *)row {
	
	if ((self = [super initWithRow:row])) {
		[self markOld];
	}
	return self;
}

+(void)load{
    [[self class] exchangeMethod:@selector(save:) withNewMethod:@selector(saveImpl:)];
}

-(BOOL)saveImpl:(NSError**)error{
    return [self save:[DataManager instance].currentDatabase error:error];
}

-(BOOL)save:(EGODatabase*)db error:(NSError**)error{
    return TRUE;
}

@end
