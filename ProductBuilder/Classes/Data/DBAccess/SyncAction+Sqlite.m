//
//  SyncAction+Sqlite.m
//  ProductBuilder
//
//  Created by Lulakov Viacheslav on 10/4/10.
//  Copyright 2010 DBBestTechnologies. All rights reserved.
//

#import "SyncAction.h"
#import "CROEntity+Sqlite.h"

@implementation SyncAction (Sqlite)

-(BOOL)save:(EGODatabase*)db error:(NSError**)error {
	const NSString *sqlSyncActionInsert = @"insert into SyncAction (RecCreated, RecModified, Action, Target) values (?1, ?2, ?3, ?4)";
	const NSString *sqlSyncActionUpdate = @"update SyncAction set RecModified = ?2 where Action = ?3 and Target = ?4";
	int result = SQLITE_OK;
	if (self.isDirty) {
		NSArray *params = @[self.recCreated, self.recModified, self.action, self.target];
		if (self.isNew) {
			result = [db executeUpdate: (NSString *)sqlSyncActionInsert parameters:params];
		}
		else {
			result = [db executeUpdate: (NSString *)sqlSyncActionUpdate parameters:params];
		}
		if (result == SQLITE_OK) {
			[self markOld];
		}
	}
	return result;
}

-(void)fetch:(EGODatabaseRow *)row{
    [super fetch:row];
    self.action =[row stringForColumn:@"Action"];
    self.target =[row stringForColumn:@"Target"];
    [self markOld];
}

+(void)load{
    [[self class] exchangeMethod:@selector(getSyncAction:target:) withNewMethod:@selector(getSyncActionImpl:target:)];
}

+(SyncAction *)getSyncActionImpl:(NSString *)act target:(NSString *)targ {
	DataManager *dbManager =[DataManager instance];
	EGODatabase *db = dbManager.currentDatabase;
    NSMutableArray* params = [[NSMutableArray alloc]init];
    [params addObjectNilSafe:act];
    [params addObjectNilSafe:targ];
	EGODatabaseResult* result = [db executeQuery:@"SELECT * FROM SyncAction WHERE Action=? AND Target=?" parameters:params];
    [params release];
	if([result.rows count]) {
		return [[[SyncAction alloc] initWithRow:(result.rows)[0]] autorelease];
	}
	return nil;
}

@end
