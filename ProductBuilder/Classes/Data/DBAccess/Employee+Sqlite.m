//
//  Employee+Sqlite.m
//  ProductBuilder
//
//  Created by Lulakov Viacheslav on 10/18/10.
//  Copyright 2010 DBBestTechnologies. All rights reserved.
//

#import "Employee.h"
#import "CROEntity+Sqlite.h"
#import "NSOrderedDictionary.h"

NSString * const employeeSelectStm = @"SELECT * FROM (SELECT rowid, *, trim(CASE WHEN (NickName IS NOT NULL AND length(trim(NickName)) > 0) THEN NickName WHEN (FirstName IS NOT NULL AND length(trim(FirstName)) > 0) THEN trim(FirstName) || ' ' || ifnull(LastName, '') ELSE ifnull(LastName, '') END) DisplayName  FROM Employee WHERE Active = 1)";
NSString * const employeeSelectAllStm = @"SELECT rowid, *, trim(CASE WHEN (NickName IS NOT NULL AND length(trim(NickName)) > 0) THEN NickName WHEN (FirstName IS NOT NULL AND length(trim(FirstName)) > 0) THEN trim(FirstName) || ' ' || ifnull(LastName, '') ELSE ifnull(LastName, '') END) DisplayName FROM Employee";

@implementation Employee (Sqlite)

-(void)fetch:(EGODatabaseRow *)row {
    
	[super fetch:row];
	self.recCreated = [row dateForColumn:@"RecCreated"];
	self.recModified = [row dateForColumn:@"RecModified"];
	self.id = [row UuidForColumn:@"EmployeeID"];
	self.employeeNum = [row intForColumn:@"EmployeeNum"];
	self.loginName = [[row stringForColumn:@"LoginName"]stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
	self.lastName = [row stringForColumn:@"LastName"];
	self.firstName = [row stringForColumn:@"FirstName"];
	self.maxDiscPercent = [row decimalForColumn:@"MaxDiscPercent"];
	self.universal = [row boolForColumn:@"Universal"];
	self.active = [row boolForColumn:@"Active"];
	self.isManager = [row boolForColumn:@"IsManager"];
	self.maxGlobalDiscPercent = [row decimalForColumn:@"MaxGlobalDiscPercent"];
	self.code = [row stringForColumn:@"Code"];
	self.nickName = [[row stringForColumn:@"NickName"]stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
	self.listOrder = [row intForColumn:@"ListOrder"];
	self.password = [row stringForColumn:@"Password"];
    self.discRequireAuthCode = [row boolForColumn:@"DiscRequireAuthCode"];
    self.locationID = [row UuidForColumn:@"LocationID"];
    self.lastLoginDate = [row dateForColumn:@"LastLoginDate"];
    self.passwordChangeDate = [row dateForColumn:@"PasswordChangeDate"];
    self.overrideRoleDiscountLimits = [row boolForColumn:@"overrideRoleDiscountLimits"];
}

+(void)load {
    
    [[self class]exchangeMethod:@selector(getEmployeesByName:andPassword:isAll:) withNewMethod:@selector(getEmployeesByNameImpl:andPassword:isAll:)];
    [[self class]exchangeMethod:@selector(updateLastLoginDateWithDate:error:) withNewMethod:@selector(updateLastLoginDateWithDateImpl:error:)];
    [[self class]exchangeMethod:@selector(getProperties) withNewMethod:@selector(getPropertiesImpl)];
    [[self class]exchangeMethod:@selector(updateDiscountLimitsFromRole) withNewMethod:@selector(updateDiscountLimitsFromRoleImpl)];
}


+(Employee *)getEmployeesByNameImpl:(NSString *)loginName andPassword:(NSString *)password isAll:(BOOL)isAll{
    
	DataManager *dbManager =[DataManager instance];
	EGODatabase *db = dbManager.currentDatabase;

    NSString *sql = [NSString stringWithFormat:@"%@ WHERE LoginName = @loginName collate nocase AND Active = 1 %@ ORDER BY LoginName", isAll ? employeeSelectAllStm : employeeSelectStm, password ? @"AND Password = @password" : @""];
    
    NSMutableDictionary* parameters = [[[NSMutableDictionary alloc] init] autorelease];
    [parameters setObjectNilSafe:loginName forKey:@"loginName"];
    if (password)
        [parameters setObjectNilSafe:password forKey:@"password"];
    
	EGODatabaseResult* result = [db executeQuery:sql namedParameters:parameters];
	for (EGODatabaseRow* row in result) {
        
		Employee *employee = [Employee instanceFromRow:row];
        return employee;
	}
	return nil;
}


+(id)getInstanceById:(BPUUID*)employeeId{
	if (!employeeId) return nil;
	EGODatabaseResult* result = [[DataManager instance].currentDatabase
								 executeQueryWithParameters:@"SELECT * FROM Employee where EmployeeID=?", [employeeId description],nil];
	if ([result count]>0){
		return [[self class] instanceFromRow:[result rowAtIndex:0]];
	}
	return nil;
}


- (BOOL)updateLastLoginDateWithDateImpl:(NSDate *)date error:(NSError **)error {
    self.lastLoginDate = date;
    
    NSString *sql = @"update Employee set LastLoginDate=@LastLoginDate where EmployeeID=@EmployeeID";
    
    NSMutableDictionary* parameters = [[NSMutableDictionary alloc]init];
    [parameters setObjectNilSafe:date                   forKey:@"LastLoginDate"];
    [parameters setObjectNilSafe:self.id                forKey:@"EmployeeID"];
    
    EGODatabase *db = [DataManager instance].currentDatabase;
    BOOL result = [db executeUpdate:sql namedParameters:parameters];
    [parameters release];
    
    if (!result || [db hadError]){
        [[self class] writeDbError:db error:error domain:@"com.Employee.updateLastLoginDate"];
        return FALSE;
    }
    return result;
}

-(NSDictionary *)getPropertiesImpl {
    
    @synchronized(self){
        
        NSMutableDictionary* parameters = [[[NSOrderedDictionary alloc]init]autorelease];
        [parameters setObjectNilSafe:[self.id description] forKey:@"EmployeeID"];
        [parameters setInteger:self.employeeNum forKey:@"EmployeeNum"];
        [parameters setObjectNilSafe:self.loginName forKey:@"LoginName"];
        [parameters setObjectNilSafe:self.lastName forKey:@"LastName"];
        [parameters setObjectNilSafe:self.firstName forKey:@"FirstName"];
        [parameters setDecimal:self.maxDiscPercent forKey:@"MaxDiscPercent"];
        [parameters setBool:self.universal forKey:@"Universal"];
        [parameters setBool:self.active forKey:@"Active"];
        [parameters setBool:self.isManager forKey:@"IsManager"];
        [parameters setDecimal:self.maxGlobalDiscPercent forKey:@"MaxGlobalDiscPercent"];
        [parameters setObjectNilSafe:self.code forKey:@"Code"];
        [parameters setInteger:self.listOrder forKey:@"ListOrder"];
        [parameters setBool:self.discRequireAuthCode forKey:@"DiscRequireAuthCode"];
        [parameters setObjectNilSafe:[self.locationID description] forKey:@"LocationID"];
        [parameters setObjectNilSafe:self.lastLoginDate forKey:@"LastLoginDate"];
        
         return parameters;
    }
}

-(void)updateDiscountLimitsFromRoleImpl{
    NSNumber* discountLimitsUpdated = objc_getAssociatedObject(self, _cmd);
    if (!discountLimitsUpdated){
        objc_setAssociatedObject(self, _cmd, (@1), OBJC_ASSOCIATION_RETAIN);
        EGODatabaseResult* result = [[DataManager instance].currentDatabase executeQuery:@"select min(r.AuthorizationCodeRequired) AuthorizationCodeRequired, max(r.MaxLineDiscount) MaxLineDiscount, max(r.MaxGlobalDiscount) MaxGlobalDiscount from RoleEmployee re inner join Role r ON re.RoleID = r.RoleID where re.EmployeeId = @EmployeeId" namedParameters:@{@"EmployeeId" : self.id}];
        if (result.rows.count) {
            EGODatabaseRow* row = result.rows[0];
            self.maxDiscPercentRole = [row decimalForColumn:@"MaxLineDiscount"];
            self.maxGlobalDiscPercentRole = [row decimalForColumn:@"MaxGlobalDiscount"];
            self.discRequireAuthCodeRole = [row boolForColumn:@"AuthorizationCodeRequired"];
        }
    }
    
}

@end
