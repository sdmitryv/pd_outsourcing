//
//  Currency+Sqlite.m
//  ProductBuilder
//
//  Created by Roman on 11/7/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import "Currency.h"
#import "CROEntity+Sqlite.h"
#import "DecimalHelper.h"
#import "SettingManager.h"

@implementation Currency (Sqlite)

+(id)getInstanceById:(BPUUID*)currencyId{
	if (!currencyId) return nil;
	EGODatabaseResult* result = [[DataManager instance].currentDatabase
								 executeQueryWithParameters:@"select * from Currency where CurrencyID=?", [currencyId description],nil];
	if ([result count]>0){
		return [Currency instanceFromRow:[result rowAtIndex:0]];
	}
	return nil;
}

-(void)fetch:(EGODatabaseRow *)row {
	[super fetch:row];
	self.id = [row UuidForColumn:@"CurrencyID"];
	self.currencyCode = [row stringForColumn:@"CurrencyCode"];
    self.code = [row stringForColumn:@"Code"];
	self.description = [row stringForColumn:@"Description"];
    self.amountRoundingPrecision = [row decimalForColumn:@"AmountRoundingPrecision"];
	self.displayDecimalPrecision = [row intForColumn:@"DisplayDecimalPrecision"];
    self.roundingType = [row intForColumn:@"RoundingType"];
    self.symbol = [row stringForColumn:@"Symbol"];
    self.symbolPosition = [row intForColumn:@"SymbolPosition"];
    self.isBase = [row boolForColumn:@"Base"];
    if (CPDecimalEquals0(self.amountRoundingPrecision)){
        NSInteger decimalDigits =- self.displayDecimalPrecision;
        if (!decimalDigits){
            decimalDigits = [[SettingManager instance] systemDecimals];
        }
        if (decimalDigits){
            self.amountRoundingPrecision = [[self class] getAmountRoundingPrecisionFromDecimalDigits:self.displayDecimalPrecision];
        }
    }
    self.paymentRoundingPrecision = [row decimalForColumn:@"PaymentRoundingPrecision"];
}

+(NSString*)stmtGetList{
    return @"SELECT * FROM Currency";
}

@end
