//
//  Location+Sqlite.m
//  TeamworkPOS
//
//  Created by Lulakov Viacheslav on 10/12/10.
//  Copyright 2010 DBBestTechnologies. All rights reserved.
//

#import "Location.h"
#import "LocationAvailabilityGroup.h"
#import "CROEntity+Sqlite.h"

@implementation Location (Sqlite)

+(id)getInstanceById:(BPUUID*)locationId{
	if (!locationId) return nil;
	EGODatabaseResult* result = [[DataManager instance].currentDatabase
								 executeQueryWithParameters:@"select * from Location where LocationID=?", [locationId description],nil];
	if ([result count]>0){
		return [Location instanceFromRow:[result rowAtIndex:0]];
	}
	return nil;
}


+(id)getLocationByCodeImpl:(NSString*)code{
    
    if (!code) return nil;
    
    EGODatabaseResult* result = [[DataManager instance].currentDatabase
                                 executeQueryWithParameters:@"select * from Location where LocationCode=?", code, nil];
    if ([result count] > 0)
        return [Location instanceFromRow:[result rowAtIndex:0]];

    return nil;
}


+(id)getLocationByEIDImpl:(NSString*)eid {
    
    if (!eid) return nil;
    
    EGODatabaseResult* result = [[DataManager instance].currentDatabase
                                 executeQueryWithParameters:@"select * from Location where eid=?", eid, nil];
    if ([result count] > 0)
        return [Location instanceFromRow:[result rowAtIndex:0]];
    
    return nil;
}



-(void)fetch:(EGODatabaseRow *)row {
    
	[super fetch:row];
    
	self.id = [row UuidForColumn:@"LocationID"];
	self.name = [row stringForColumn:@"Name"];
	self.name2 = [row stringForColumn:@"Name2"];
	self.locationNum = [row intForColumn:@"LocationNum"];
	self.taxZoneID = [row UuidForColumn:@"TaxZoneID"];
	self.locationGroupID = [row UuidForColumn:@"LocationGroupID"];
    self.locationPriceGroupID = [row UuidForColumn:@"LocationPriceGroupID"];
    self.locationExternalID = [row stringForColumn:@"EID"];
    
	self.locationBaseCurrencyID = [row UuidForColumn:@"LocationBaseCurrencyID"]; // Roma
    
	self.transferGroup = [row stringForColumn:@"TransferGroup"];
	self.locationCode = [row stringForColumn:@"LocationCode"];
    self.defaultPriceLevelCode = [row stringForColumn:@"DefaultPriceLevelCode"];
	self.area = [row stringForColumn:@"Area"];
	self.phone1 = [row stringForColumn:@"Phone1"];
    self.faxNumber = [row stringForColumn:@"Fax"];
    self.email = [row stringForColumn:@"EMail"];
    self.homePage = [row stringForColumn:@"HomePage"];
	self.latitude = [row decimalForColumn:@"Latitude"];
	self.longitude = [row decimalForColumn:@"Longitude"];
    self.locationAvailabilityGroupID = [row UuidForColumn:@"LocationAvailabilityGroupID"];
    self.timeZoneID = [row UuidForColumn:@"TimeZoneID"];
    
    self.address1 = [row stringForColumn:@"Address1"];
    self.address2 = [row stringForColumn:@"Address2"];
    self.address3 = [row stringForColumn:@"Address3"];
    self.address4 = [row stringForColumn:@"Address4"];
    self.city = [row stringForColumn:@"City"];
    self.state = [row stringForColumn:@"State"];
    self.postalCode = [row stringForColumn:@"PostalCode"];
    NSString* country = [row stringForColumn:@"CountryCode"];
    self.country = [[country stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] lowercaseString];
    
    self.availableForStorePickup = [row boolForColumn:@"AvailableForStorePickup"];
    self.isActive = [row boolForColumn:@"IsActive"];
    self.open = [row boolForColumn:@"Open"];
}

+(NSString*)stmtGetList{
    return @"select rowid,* from Location where Type <> 2 ORDER BY LocationNum";
}

+(void)load{
    
    EXCHANGE_METHOD(getLocations, getLocationsImpl);
    EXCHANGE_METHOD(getLocationsFromLocalAvailabilityGroup, getLocationsFromLocalAvailabilityGroupImpl);
    EXCHANGE_METHOD(getLocationByCode:, getLocationByCodeImpl:);
    EXCHANGE_METHOD(getLocationByEID:, getLocationByEIDImpl:);
    EXCHANGE_METHOD(getCompanyName, getCompanyNameImpl);
}

+(NSMutableArray*)getLocationsImpl
{
	DataManager *dbManager =[DataManager instance];
	EGODatabase *db = dbManager.currentDatabase;
	NSMutableArray *list = [[NSMutableArray alloc] init];
	EGODatabaseResult* result = [db executeQuery:[[self class] stmtGetList]];
	for(EGODatabaseRow* row in result) {
		id item = [[self class] instanceFromRow:row];
		[list addObject:item];
	}
	return [list autorelease];
}

+(NSMutableArray*)getLocationsFromLocalAvailabilityGroupImpl {
    Location * local = [Location localLocation];
    if (local.locationAvailabilityGroupID == nil) {
        return [Location getLocations];
    }
    LocationAvailabilityGroup * availabilityGroup = [LocationAvailabilityGroup getInstanceById:local.locationAvailabilityGroupID];
    if (availabilityGroup == nil || [availabilityGroup.code caseInsensitiveCompare:@"ALL LOCAT"] == NSOrderedSame) {
        return [Location getLocations];
    }
	DataManager *dbManager =[DataManager instance];
	EGODatabase *db = dbManager.currentDatabase;
	NSMutableArray *list = [[NSMutableArray alloc] init];
	EGODatabaseResult* result = [db executeQueryWithParameters:@"select * from Location where  LocationID = ? or LocationID in                                  (select LocationID from LocationAvailabilityGroup_Location where LocationAvailabilityGroupID = ?)", local.id, local.locationAvailabilityGroupID, nil];
	for (EGODatabaseRow* row in result) {
		id item = [[self class] instanceFromRow:row];
		[list addObject:item];
	}
	return [list autorelease];
}
+(NSString*)getCompanyNameImpl{
    DataManager *dbManager =[DataManager instance];
    EGODatabase *db = dbManager.currentDatabase;
    EGODatabaseResult* result = [db executeQuery:@"Select CompanyName From Company limit 1"];
    for(EGODatabaseRow* row in result) {
        return [row stringForColumn:@"CompanyName"];
    }
    return @"";
    
}

@end
