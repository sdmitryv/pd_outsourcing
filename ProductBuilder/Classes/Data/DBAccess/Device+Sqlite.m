//
//  Device+Sqlite.m
//  DeviceAgent
//
//  Created by Alexander Martyshko on 11/8/13.
//  Copyright (c) 2013 Cloudworks. All rights reserved.
//

#import "Device.h"
#import "DataManager.h"
#import "DBAdditions.h"
#import "CROEntity+Sqlite.h"
#import "Settings.h"

#define deviceSelectSTMT @"SELECT rowid, * FROM Device"

NSString * const deviceSelectStm = @"SELECT * FROM ("deviceSelectSTMT")";

@implementation Device (Sqlite)

+(id)getInstanceById:(BPUUID*)id{
	DataManager *dbManager =[DataManager instance];
	EGODatabase *db = dbManager.currentDatabase;
    NSString *sql = [NSString stringWithFormat:@"%@ WHERE DeviceId=?", deviceSelectStm];
	EGODatabaseResult* result = [db executeQueryWithParameters:sql, [id description],nil];
    
	if ([result count] > 0)
		return [Device instanceFromRow:[result rowAtIndex:0]];
    
	return nil;
}

- (void)fetch:(EGODatabaseRow *)row{
	[super fetch:row];
    self.id = [row UuidForColumn:@"DeviceId"];
    
    self.deviceAgentId = [row UuidForColumn:@"DeviceAgentId"];
    self.locationId = [row UuidForColumn:@"LocationId"];
    
    self.deviceName = [row stringForColumn:@"DeviceName"];
    self.deviceAlias = [row stringForColumn:@"DeviceAlias"];
    self.deviceType = [row intForColumn:@"DeviceType"];
    
    self.printServerIP = [row stringForColumn:@"PrintServerIP"];
    self.printerPort = [row intForColumn:@"PrinterPort"];
    self.scalesPort = [row intForColumn:@"ScalesPort"];
    self.deviceNo = [row intForColumn:@"DeviceNo"];
    
    self.shopperDisplayId = [row UuidForColumn:@"ShopperDisplayId"];
    self.isDeactivated = [row boolForColumn:@"IsDeactivated"];
    self.notes = [row stringForColumn:@"Notes"];
    
    self.techInfo1 = [row stringForColumn:@"TechInfo1"];
    self.techInfo2 = [row stringForColumn:@"TechInfo2"];
    self.techInfo3 = [row stringForColumn:@"TechInfo3"];
    self.techInfo4 = [row stringForColumn:@"TechInfo4"];
    self.techInfo5 = [row stringForColumn:@"TechInfo5"];
    self.techInfo6 = [row stringForColumn:@"TechInfo6"];
    self.techInfo7 = [row stringForColumn:@"TechInfo7"];
    self.techInfo8 = [row stringForColumn:@"TechInfo8"];
    self.techInfo9 = [row stringForColumn:@"TechInfo9"];
    self.techInfo10 = [row stringForColumn:@"TechInfo10"];
    
    self.IPAddress = [row stringForColumn:@"IPAddress"];
    self.latitude = [row doubleForColumn:@"Latitude"];
    self.longitude = [row doubleForColumn:@"Longitude"];
    
    self.mobileApplicationId = [row UuidForColumn:@"MobileApplicationId"];
    self.applicationVersion = [row stringForColumn:@"ApplicationVersion"];
    self.firstLaunchDate = [row dateForColumn:@"FirstLaunchDate"];
    self.SVSAuthToken = [row stringForColumn:@"SvsAuthToken"];
    self.SVSDeviceId = [row stringForColumn:@"SvsDeviceId"];
    
    self.locationDeviceCode = [row intForColumn:@"LocationDeviceCode"];
    
    self.lastTransactionNo = [row stringForColumn:@"LastTransactionNo"];
    
    self.pushLogsToGae = [row boolForColumn:@"PushLogsToGae"];
    self.gaeLogSeverity = [row intForColumn:@"GaeLogSeverity"];
    
    self.collectNetworkStats = [row boolForColumn:@"CollectNetworkStats"];
    
    self.isMultiWorkstation = [row boolForColumn:@"isMultiWorkstation"];
}

@end
