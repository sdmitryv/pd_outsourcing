//
//  Country+Sqlite.m
//  CloudworksPOS
//
//  Created by Lulakov Viacheslav on 7/13/11.
//  Copyright 2011 Cloudworks. All rights reserved.
//

#import "Country.h"
#import "CROEntity+Sqlite.h"

@implementation Country (Sqlite)

-(void)fetch:(EGODatabaseRow *)row {
	[super fetch:row];
	self.recCreated = [row dateForColumn:@"RecCreated"];
	self.recModified = [row dateForColumn:@"RecModified"];
	self.id = [row UuidForColumn:@"CountryID"];
	self.code = [row stringForColumn:@"Code"];
	self.shortName = [row stringForColumn:@"ShortName"];
}

+(void)load{
    EXCHANGE_METHOD(getCountries, getCountriesImpl);
    EXCHANGE_METHOD(getCountryByCode:, getCountryByCodeImpl:);
}

+(NSMutableArray*)getCountriesImpl
{
	DataManager *dbManager =[DataManager instance];
	EGODatabase *db = dbManager.currentDatabase;
	NSMutableArray *list = [[NSMutableArray alloc] init];
	EGODatabaseResult* result = [db executeQuery:@"SELECT rowid, * FROM Country order by ShortName asc"];
	for(EGODatabaseRow* row in result) {
		id item = [[self class] instanceFromRow:row];
		[list addObject:item];
	}
	return [list autorelease];
}

+(Country*)getCountryByCodeImpl:(NSString*)code{
    DataManager *dbManager =[DataManager instance];
	EGODatabase *db = dbManager.currentDatabase;
	EGODatabaseResult* result = [db executeQueryWithParameters: @"SELECT * FROM Country where Code=?", code ? code : [NSNull null], nil];
    if (!result.rows.count) return nil;
	return [[[Country alloc]initWithRow:(result.rows)[0]] autorelease];
}

+(Country*)getInstanceById:(BPUUID*)countryId{
    DataManager *dbManager =[DataManager instance];
	EGODatabase *db = dbManager.currentDatabase;
	EGODatabaseResult* result = [db executeQueryWithParameters: @"SELECT * FROM Country where CountryID=?", countryId ? countryId : [NSNull null], nil];
    if (!result.rows.count) return nil;
	return [[[Country alloc]initWithRow:(result.rows)[0]] autorelease];
}



@end
