//
//  AttributeSet+Sqlite.m
//  Buyer
//
//  Created by Lulakov Viacheslav on 11/2/10.
//  Copyright 2010 DBBestTechnologies. All rights reserved.
//
#import "AttributeSet.h"
#import "DataManager.h"
#import "DBAdditions.h"
#import "CROEntity+Sqlite.h"
#import "CustomField.h"
#import "NSString+Ext.h"


#define attrSetSelectSTMT @"SELECT ats.rowid, ats.RecCreated, ats.RecModified, ats.AttributeSetID, ats.Code, ats.Description, ifnull(ats.AttributeLabel, ats.Code) AS AttributeLabel, ats.Position FROM AttributeSet ats WHERE ats.HasValues = 1"

NSString * const attrSetSelectStm = @"SELECT * FROM("attrSetSelectSTMT")";


@implementation AttributeSet (Sqlite)

+(void)load{

    [[self class] exchangeMethod:@selector(loadAttributeSetByCode:) withNewMethod:@selector(loadAttributeSetByCodeImpl:)];
}


+(AttributeSet *)loadAttributeSetByCodeImpl:(NSString *)code {
    
    NSString *sql = [NSString stringWithFormat:@"%@ WHERE code = @Code", attrSetSelectStm];
    
    DataManager *dbManager =[DataManager instance];
    EGODatabase *db = dbManager.currentDatabase;
    NSMutableDictionary* parameters = [NSMutableDictionary dictionary];
    [parameters setObjectNilSafe:code forKey:@"Code"];
    
    EGODatabaseResult* queryResult = [db executeQuery:sql namedParameters:parameters];
    for(EGODatabaseRow* row in queryResult)
        return (AttributeSet*)[[self class] instanceFromRow:row];
    
    return nil;
}


-(void)fetch:(EGODatabaseRow *)row {
    
    [super fetch:row];
	
    self.id = [row UuidForColumn:@"AttributeSetID"];
    self.code = [row stringForColumn:@"Code"];
    self.description = [row stringForColumn:@"Description"];
    self.attributeLabel = [row stringForColumn:@"AttributeLabel"];
}


@end
