//
//  Setting+Sqlite.m
//  StockCount
//
//  Created by Lulakov Viacheslav on 10/19/10.
//  Copyright 2010 DBBestTechnologies. All rights reserved.
//

#import "AppSetting.h"
#import "CEntity+Sqlite.h"

@implementation AppSetting (Sqlite)

-(void)fetch:(EGODatabaseRow *)row {
	[super fetch:row];
	self.id = [row UuidForColumn:@"RecID"];
	self.type = [row stringForColumn:@"Type"];
	self.key = [row stringForColumn:@"Key"];
	self.value = [row stringForColumn:@"Value"];
}

+(void)load{
    [[self class] exchangeMethod:@selector(getSettings) withNewMethod:@selector(getSettingsImpl)];
    [[self class] exchangeMethod:@selector(getSettingForKey:) withNewMethod:@selector(getSettingForKeyImpl:)];
    [[self class] exchangeMethod:@selector(getSettingForKey:usingDb:) withNewMethod:@selector(getSettingForKeyImpl:usingDb:)];
    [[self class] exchangeMethod:@selector(setString:forKey:) withNewMethod:@selector(setStringImpl:forKey:)];
}

+(NSMutableArray*)getSettingsImpl
{
	DataManager *dbManager =[DataManager instance];
	EGODatabase *db = dbManager.currentDatabase;
    if (!db)
        return nil;
    
	NSMutableArray *settingArray = [[NSMutableArray alloc] init];
	EGODatabaseResult* result = [db executeQuery:@"SELECT * FROM AppSetting"];
	for(EGODatabaseRow* row in result) {
		AppSetting *setting = [AppSetting instanceFromRow:row];
		[settingArray addObject:setting];
	}
	return [settingArray autorelease];
}

+ (AppSetting *)getSettingForKeyImpl:(NSString *)key usingDb:(EGODatabase*)db{
    if (!db)
        return nil;
    
    AppSetting *setting = nil;
    EGODatabaseResult* result = [db executeQueryWithParameters:@"SELECT * FROM AppSetting WHERE Key = ?", key, nil];
    for(EGODatabaseRow* row in result) {
        setting = [AppSetting instanceFromRow:row];
        break;
    }
    return setting;
}

+(AppSetting *)getSettingForKeyImpl:(NSString *)key {
	return [self getSettingForKey:key usingDb:[DataManager instance].currentDatabase];
}

+ (void)setStringImpl:(NSString *)value forKey:(NSString *)key {
	AppSetting *setting = [AppSetting getSettingForKey:key];
	if (setting == nil) {
		setting = [[[AppSetting alloc] init] autorelease];
		setting.key = key;
	}
	setting.value = value;
	[setting save];
}

-(BOOL)save:(EGODatabase*)db error:(NSError**)error {
    
	NSString* const sqlSettingInsert = @"insert or replace into AppSetting (RecCreated, RecModified, RecID, Type, Key, Value) "
	@"values (@RecCreated, @RecModified, @RecID, @Type, @Key, @Value)";
	NSString* const sqlSettingDelete = @"delete from AppSetting where rowid=@rowid";
	
    NSString*	sqlStmt  = nil;
    NSMutableDictionary* parameters = [[NSMutableDictionary alloc] init];
    
    if (self.isDeleted){
        sqlStmt = sqlSettingDelete;
        [parameters setObjectNilSafe:self.rowid forKey:@"rowid"];
    }
    else{
        
        sqlStmt = sqlSettingInsert;
        [parameters setObjectNilSafe:_recCreated forKey:@"RecCreated"];
        [parameters setObjectNilSafe:[_id stringRepresentation] forKey:@"RecID"];
        [parameters setObjectNilSafe:_recModified  forKey:@"RecModified"];
        [parameters setObjectNilSafe:_type forKey:@"Type"];
        [parameters setObjectNilSafe:_key forKey:@"Key"];
        [parameters setObjectNilSafe:_value forKey:@"Value"];
    }
    BOOL result = [db executeUpdate:sqlStmt namedParameters:parameters];
    [parameters release];
    
    if (!result || [db hadError]) {
        NSLog(@"error: %@", [db lastErrorMessage]);
        return FALSE;
    }

	return TRUE;
}

@end
