//
//  CustomLookup+Sqlite.m
//  CloudworksPOS
//
//  Created by Vitaliy Gervazuk on 7/22/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "CustomLookup.h"
#import "DBAdditions.h"
#import "CROEntity+Sqlite.h"
#import "SettingManager.h"

@implementation CustomLookup (Sqlite)

-(void)fetch:(EGODatabaseRow *)row{
    
    [super fetch:row];
    
    self.recCreated = [row dateForColumn:@"RecCreated"];
	self.recModified = [row dateForColumn:@"RecModified"];
	self.id = [row UuidForColumn:@"LookupID"];
    self.areaNum = [row intForColumn:@"AreaNum"];
    self.fieldNum = [row intForColumn:@"FieldNum"];
    self.name = [row stringForColumn:@"Name"];
    self.alias = [row stringForColumn:@"Alias"];
    self.isDeleted = [row boolForColumn:@"isDeleted"];
}

+(void)load{
    [[self class] exchangeMethod:@selector(getLookups:lookupNum:) withNewMethod:@selector(getLookupsImpl:lookupNum:)];
}

+(id)getInstanceById:(BPUUID *)lookupId {
    if (!lookupId) return nil;
	EGODatabaseResult* result = [[DataManager instance].currentDatabase
								 executeQueryWithParameters:@"select rowid, * from CustomLookup where LookupID=?", [lookupId description],nil];
    if ([result count] > 0) {
        
		return [CustomLookup fetchList:result][0];
	}
	return nil;
}

+(NSMutableArray*)getLookupsImpl:(NSInteger)areaNum lookupNum:(NSInteger)lookupNum {
    DataManager *dbManager =[DataManager instance];
	EGODatabase *db = dbManager.currentDatabase;
	    NSString *selectStatement = @"SELECT rowid, * FROM CustomLookup WHERE AreaNum = ? AND FieldNum = ? AND IsDeleted = 0 order by Name asc";
    NSMutableArray *parameters = [[NSMutableArray alloc]init];
    [parameters addInteger:areaNum];
    [parameters addInteger:lookupNum];
	EGODatabaseResult* result = [db executeQuery:selectStatement parameters:parameters];
    [parameters release];
    
    return [CustomLookup fetchList:result];
}


+(NSMutableArray *)fetchList:(EGODatabaseResult*)dbresult {

    CustomLookupDisplayType displayType = [SettingManager instance].customLookupDisplayType;
    
    NSMutableArray *list = [[[NSMutableArray alloc] init] autorelease];

    for (EGODatabaseRow* row in dbresult) {
        
        CustomLookup *item = [[self class] instanceFromRow:row];
        
        switch (displayType) {
            case CustomLookupDisplayTypeName:
                item.fullName = item.name.length > 0 ? item.name : item.alias;
                break;
            case CustomLookupDisplayTypeAlias:
                item.fullName = item.alias.length > 0 ? item.alias : item.name;
                break;
            default: {
                
                NSString *name = item.name ? [item.name stringByTrimmingCharactersInSet:[NSCharacterSet newlineCharacterSet]] : @"";
                NSString *alias = item.alias ? [item.alias stringByTrimmingCharactersInSet:[NSCharacterSet newlineCharacterSet]] : @"";
                if (name.length == 0 && alias.length == 0)
                    item.fullName = @"";
                else if (name.length == 0)
                    item.fullName = alias;
                else if (alias.length == 0)
                    item.fullName = name;
                else
                    item.fullName = [NSString stringWithFormat:@"%@ - %@", name, alias];
            }
                break;
        }
        [list addObject:item];
    }
    
    return list;
}

@end
