//
//  State+Sqlite.m
//  ProductBuilder
//
//  Created by Alexander Martyshko on 1/16/15.
//  Copyright (c) 2015 Cloudworks. All rights reserved.
//

#import "State.h"
#import "CROEntity+Sqlite.h"

@implementation State (Sqlite)

+(void)load{
    EXCHANGE_METHOD(getList, getListImpl);
}

+(NSArray *)getListImpl {
    NSMutableArray *list = [[NSMutableArray alloc] init];
    
    DataManager *dbManager =[DataManager instance];
    EGODatabase *db = dbManager.currentDatabase;
    EGODatabaseResult* result = [db executeQuery:@"SELECT rowid, * FROM State"];
    for(EGODatabaseRow* row in result) {
        State *state = [State instanceFromRow:row];
        [list addObject:state];
    }
    return [list autorelease];
}

-(void)fetch:(EGODatabaseRow *)row {
    [super fetch:row];
    self.id = [row UuidForColumn:@"StateId"];
    self.name = [row stringForColumn:@"Name"];
    self.code = [row stringForColumn:@"Code"];
    self.countryId = [row UuidForColumn:@"CountryId"];
}

@end
