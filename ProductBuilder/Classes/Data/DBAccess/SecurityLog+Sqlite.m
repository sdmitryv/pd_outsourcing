//
//  SecurityLog.m
//  ProductBuilder
//
//  Created by Vitaliy Gervazuk on 9/11/12.
//  Copyright (c) 2012 Cloudworks. All rights reserved.
//

#import "SecurityLog.h"
#import "CEntity+Sqlite.h"

@implementation SecurityLog(Sqlite)

+(void)load{
    [[self class]exchangeMethod:@selector(getSecurityLogsForReceipt:) withNewMethod:@selector(getSecurityLogsForReceiptImp:)];
    [[self class]exchangeMethod:@selector(getSecurityLogsForOrder:) withNewMethod:@selector(getSecurityLogsForOrderImp:)];
}

+(NSMutableArray*)getSecurityLogsForReceiptImp:(BPUUID*)receiptId {
    EGODatabaseResult* result = [[DataManager instance].currentDatabase executeQueryWithParameters:
								 @"SELECT * FROM SecurityLog WHERE DocumentID = ? AND DocType = 1",
								 [receiptId description],nil];
	NSMutableArray *list = [[[NSMutableArray alloc] init]autorelease];
	for(EGODatabaseRow* row in result) {
		SecurityLog *log = [[SecurityLog alloc] initWithRow:row];
		[list addObject:log];
		[log release];
	}
	return list;
}

+(NSMutableArray*)getSecurityLogsForOrderImp:(BPUUID*)orderId {
    EGODatabaseResult* result = [[DataManager instance].currentDatabase executeQueryWithParameters:
                                 @"SELECT * FROM SecurityLog WHERE DocumentID = ? AND DocType = 2",
                                 [orderId description],nil];
    NSMutableArray *list = [[[NSMutableArray alloc] init]autorelease];
    for(EGODatabaseRow* row in result) {
        SecurityLog *log = [[SecurityLog alloc] initWithRow:row];
        [list addObject:log];
        [log release];
    }
    return list;
}

-(void)fetch:(EGODatabaseRow *)row {
    [super fetch:row];
    
    self.id = [row UuidForColumn:@"SecurityLogID"];
    self.documentId = [row UuidForColumn:@"DocumentID"];
    self.docType = [row intForColumn:@"DocType"];
    self.documentLineId = [row UuidForColumn:@"DocumentLineID"];
    self.docLineType = [row intForColumn:@"LineType"];
    self.srcEmployeeId = [row UuidForColumn:@"SourceEmployeeID"];
    self.destEmployeeId = [row UuidForColumn:@"DestEmployeeID"];
    self.roleCode = [row stringForColumn:@"RoleCode"];
    self.utcLogDate = [row dateForColumn:@"UtcLogDate"];
    self.localLogDate = [row dateForColumn:@"LocalLogDate"];
    
}

-(BOOL)save:(EGODatabase *)db error:(NSError **)error {
    if (!self.isDirty || (self.isDeleted && self.isNew)) return TRUE;
    
    static NSString* insertStatement = @"REPLACE INTO SecurityLog (RecCreated, RecModified, SecurityLogID, DocumentID, DocType, DocumentLineID, LineType, SourceEmployeeID, DestEmployeeID, RoleCode, Description, UtcLogDate, LocalLogDate, SyncState) VALUES (@RecCreated, @RecModified, @SecurityLogID, @DocumentID, @DocType, @DocumentLineID, @LineType, @SourceEmployeeID, @DestEmployeeID, @RoleCode, @Description, @UtcLogDate, @LocalLogDate, @SyncState)";
    static NSString* stmtDelete = @"delete from SecurityLog where SecurityLogID=@SecurityLogID";
    NSString* stmt = nil;
    BOOL result = TRUE;
    if (self.isDirty){
        NSMutableDictionary* parameters = [[NSMutableDictionary alloc]init];
        [parameters setObjectNilSafe:[_id description] forKey:@"SecurityLogID"];
        
        if (self.isDeleted){
            stmt = stmtDelete;
        }
        else{
            stmt = insertStatement;
            [parameters setObjectNilSafe:self.recCreated forKey:@"RecCreated"];
            [parameters setObjectNilSafe:self.recModified forKey:@"RecModified"];
            [parameters setObjectNilSafe:self.documentId forKey:@"DocumentID"];
            [parameters setInt:self.docType forKey:@"DocType"];
            [parameters setObjectNilSafe:self.documentLineId forKey:@"DocumentLineID"];
            [parameters setInt:self.docLineType forKey:@"LineType"];
            [parameters setObjectNilSafe:self.srcEmployeeId forKey:@"SourceEmployeeID"];
            [parameters setObjectNilSafe:self.destEmployeeId forKey:@"DestEmployeeID"];
            [parameters setObjectNilSafe:self.roleCode forKey:@"RoleCode"];
            [parameters setObjectNilSafe:self.description forKey:@"Description"];
            [parameters setObjectNilSafe:self.utcLogDate forKey:@"UtcLogDate"];
            [parameters setObjectNilSafe:self.localLogDate forKey:@"LocalLogDate"];
            [parameters setInt:1 forKey:@"SyncState"];
        }
        
        result = [db executeUpdate:stmt
                   namedParameters:parameters];
        [parameters release];
        
        if (!result || [db hadError]){
            [[self class] writeDbError:db error:error domain:@"com.receipt.save"];
            return FALSE;
        }
    }
    if (result && !self.isDeleted){
        [self markOld];
    }
    
    return result;
}

@end
