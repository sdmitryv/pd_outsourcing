//
//  OnlineOMSTransaction+Sqlite.m
//  ProductBuilder
//
//  Created by Alexander Martyshko on 1/9/15.
//  Copyright (c) 2015 Cloudworks. All rights reserved.
//

#import "CEntity+Sqlite.h"
#import "OnlineOMSTransaction.h"

@implementation OnlineOMSTransaction (Sqlite)

+(void)load{
    [[self class] exchangeMethod:@selector(onlineOMSTransactionForReceiptWithId:) withNewMethod:@selector(onlineOMSTransactionForReceiptWithIdImpl:)];
}

+ (OnlineOMSTransaction *)onlineOMSTransactionForReceiptWithIdImpl:(BPUUID *)receiptId {
    EGODatabaseResult* result = [[DataManager instance].currentDatabase executeQueryWithParameters:@"SELECT * FROM OnlineOMSTransaction WHERE ReceiptId = ?", [receiptId description], nil];
    if ([result count] > 0){
        return [[self class] instanceFromRow:[result rowAtIndex:0]];
    }
    return nil;
}

-(void)fetch:(EGODatabaseRow *)row {
    [super fetch:row];
    
    self.id = [row UuidForColumn:@"OnlineOMSTransactionId"];
    self.receiptId = [row UuidForColumn:@"ReceiptID"];
    self.orderId = [row stringForColumn:@"OrderId"];
    self.status = [row intForColumn:@"Status"];
    self.firstItemDateTime = [row dateForColumn:@"FirstItemDateTime"];
    self.preConfirmDateTime = [row dateForColumn:@"PreConfirmDateTime"];
    self.confirmDateTime = [row dateForColumn:@"ConfirmDateTime"];
    
    [self markClean];
}

-(BOOL)save:(EGODatabase*)db error:(NSError**)error{
    
    if (!self.isDirty || (self.isDeleted && self.isNew))
        return TRUE;

    NSString * const stmtInsert = @"INSERT INTO OnlineOMSTransaction (\
    RecCreated \
    ,RecModified \
    ,OnlineOMSTransactionId \
    ,ReceiptId \
    ,OrderId \
    ,Status \
    ,FirstItemDateTime \
    ,PreConfirmDateTime \
    ,ConfirmDateTime \
    ,SyncState \
    ) \
    VALUES (\
    @RecCreated \
    ,@RecModified \
    ,@OnlineOMSTransactionId \
    ,@ReceiptId \
    ,@OrderId \
    ,@Status \
    ,@FirstItemDateTime \
    ,@PreConfirmDateTime \
    ,@ConfirmDateTime \
    ,1 \
    )";
    
    NSString* const stmtUpdate = @"UPDATE OnlineOMSTransaction set \
    RecModified = @RecModified \
    ,ReceiptId = @ReceiptId \
    ,OrderId = @OrderId \
    ,Status = @Status \
    ,FirstItemDateTime = @FirstItemDateTime \
    ,PreConfirmDateTime = @PreConfirmDateTime \
    ,ConfirmDateTime = @ConfirmDateTime \
    ,SyncState = IFNULL(SyncState,0) + 1 \
    where OnlineOMSTransactionId = @OnlineOMSTransactionId";
    
    NSString* const stmtDelete = @"delete from OnlineOMSTransaction where OnlineOMSTransactionId = @OnlineOMSTransactionId";
    NSString* stmt = nil;
    NSMutableDictionary* parameters = [[NSMutableDictionary alloc]init];
    
    [parameters setObjectNilSafe:self.id forKey:@"OnlineOMSTransactionId"];
    
    if (self.isDeleted){
        stmt = stmtDelete;
    }
    else{
        stmt = self.isNew ? stmtInsert : stmtUpdate;
        if (self.isNew){
            [parameters setObjectNilSafe:_recCreated forKey:@"RecCreated"];
        }
        [parameters setObjectNilSafe:_recModified forKey:@"RecModified"];
        [parameters setObjectNilSafe:self.receiptId forKey:@"ReceiptId"];
        [parameters setObjectNilSafe:self.orderId forKey:@"OrderId"];
        [parameters setObjectNilSafe:@(self.status) forKey:@"Status"];
        [parameters setObjectNilSafe:self.firstItemDateTime forKey:@"FirstItemDateTime"];
        [parameters setObjectNilSafe:self.preConfirmDateTime forKey:@"PreConfirmDateTime"];
        [parameters setObjectNilSafe:self.confirmDateTime forKey:@"ConfirmDateTime"];
    }
    
    BOOL result = [db executeUpdate:stmt namedParameters:parameters];
    [parameters release];
    
    if (!result && [db hadError]){
        [[self class] writeDbError:db error:error domain:@"com.OnlineOMSTransaction.save"];
    }
    if (result && !self.isDeleted){
        [self markOld];
    }
    return result;
}


@end
