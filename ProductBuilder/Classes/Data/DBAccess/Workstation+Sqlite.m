//
//  WorkStation+Sqlite.m
//  ProductBuilder
//
//  Created by Valera on 9/14/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "Workstation.h"
#import "CROEntity+Sqlite.h"

@implementation Workstation (Sqlite)

+(void)load{
    EXCHANGE_METHOD(getWorkstations, getWorkstationsImpl);
}

+(id)getInstanceById:(BPUUID *)id {
    if (!id) return nil;
	EGODatabaseResult* result = [[DataManager instance].currentDatabase
								 executeQueryWithParameters:@"SELECT rowid, * FROM Workstation WHERE WorkstationID=?",[id description],nil];
	if ([result count]>0){
		return [[self class] instanceFromRow:[result rowAtIndex:0]];
	}
	return nil;
}

+(NSArray*)getWorkstationsImpl
{
	DataManager *dbManager =[DataManager instance];
	EGODatabase *db = dbManager.currentDatabase;
	NSString *sqlRequest = @"SELECT * FROM Workstation";
	EGODatabaseResult* result = [db executeQuery:sqlRequest];
	if (result.errorCode!=SQLITE_OK){
		return nil;
	}
		
	NSMutableArray *wsArray = [[NSMutableArray alloc] init];		
	for(EGODatabaseRow* row in result) {
		Workstation *workstation = [[Workstation alloc] initWithRow:row];
		[wsArray addObject:workstation];
		[workstation release];
	}
    
    [wsArray sortUsingComparator:^(id obj1, id obj2) {
        return [[obj1 description] localizedCaseInsensitiveCompare:[obj2 description]];
    }];
    
	return [wsArray autorelease];
}

+(Workstation*)getWorkstation:(BPUUID*) nodeID machineName:(NSString*) machineName{
	DataManager *dbManager =[DataManager instance];
	EGODatabase *db = dbManager.currentDatabase;
	EGODatabaseResult* result = [db executeQueryWithParameters:@"SELECT * FROM Workstation where MachineName=? and NodeId=? ORDER BY Name", machineName,nodeID, nil];
	if (result.errorCode!=SQLITE_OK){
		return nil;
	}
	if( [result count]){
		EGODatabaseRow* row = [result rowAtIndex:0];
		Workstation *workstation = [[Workstation alloc] initWithRow:row];
		return [workstation autorelease];
	}
	return nil;
}

- (void)fetch:(EGODatabaseRow *)row{
	[super fetch:row];
    self.id = [row UuidForColumn:@"WorkstationID"];
    self.wsNum = [row intForColumn:@"WSNum"];
    self.name = [row stringForColumn:@"Name"];
    self.locationID = [row UuidForColumn:@"LocationID"];
    self.taxZoneID = [row UuidForColumn:@"TaxZoneID"];
    self.nodeID =[row UuidForColumn:@"NodeID"];
    self.machineName = [row stringForColumn:@"MachineName"];
    self.machineID = [row UuidForColumn:@"MachineID"];
}

- (NSString *)description {
    
    NSString *wsName = [self.name stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    return wsName.length > 0 ? wsName : [NSString stringWithFormat:@"%d", self.wsNum];
}

@end
