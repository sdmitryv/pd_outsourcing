//
//  DBAdditions.h
//  ProductBuilder
//
//  Created by Lulakov Viacheslav on 10/8/10.
//  Copyright 2010 DBBestTechnologies. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSMutableOrderedSet (NSNilSafeOrderedSet)
- (void)addObjectNilSafe:(id)anObject;
@end

@interface NSMutableArray (NSNilSafeArray)

- (void)addObjectNilSafe:(id)anObject;
- (void)addChar:(char)value;
- (void)addUnsignedChar:(unsigned char)value;
- (void)addShort:(short)value;
- (void)addUnsignedShort:(unsigned short)value;
- (void)addInt:(int)value;
- (void)addUnsignedInt:(unsigned int)value;
- (void)addLong:(long)value;
- (void)addUnsignedLong:(unsigned long)value;
- (void)addLongLong:(long long)value;
- (void)addUnsignedLongLong:(unsigned long long)value;
- (void)addFloat:(float)value;
- (void)addDouble:(double)value;
- (void)addBool:(BOOL)value;
- (void)addInteger:(NSInteger)value;
- (void)addUnsignedInteger:(NSUInteger)value;
- (void)addDecimal:(NSDecimal)value;																				
@end

@interface NSMutableDictionary (NSNilSafeArray)
- (void)setObjectNilSafe:(id)anObject forKey:(NSString*)key;
- (void)setChar:(char)value forKey:(NSString*)key;
- (void)setUnsignedChar:(unsigned char)value forKey:(NSString*)key;
- (void)setShort:(short)value forKey:(NSString*)key;
- (void)setUnsignedShort:(unsigned short)value forKey:(NSString*)key;
- (void)setInt:(NSInteger)value forKey:(NSString*)key;
- (void)setUnsignedInt:(unsigned int)value forKey:(NSString*)key;
- (void)setLong:(long)value forKey:(NSString*)key;
- (void)setUnsignedLong:(unsigned long)value forKey:(NSString*)key;
- (void)setLongLong:(long long)value forKey:(NSString*)key;
- (void)setUnsignedLongLong:(unsigned long long)value forKey:(NSString*)key;
- (void)setFloat:(float)value forKey:(NSString*)key;
- (void)setDouble:(double)value forKey:(NSString*)key;
- (void)setBool:(BOOL)value forKey:(NSString*)key;
- (void)setInteger:(NSInteger)value forKey:(NSString*)key;
- (void)setUnsignedInteger:(NSUInteger)value forKey:(NSString*)key;
- (void)setDecimal:(NSDecimal)value forKey:(NSString*)key;

@end
