//
//  Department+Sqlite.m
//  ProductBuilder
//
//  Created by Valera on 10/12/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "Department.h"
#import "CROEntity+Sqlite.h"

@interface Department (SqlitePrivate)

-(id)initWithRowFromParent:(Department*)parent row:(EGODatabaseRow *)row;
+(NSString*) columnNameInSet:(NSUInteger) level;
@end

@implementation Department (SqlitePrivate)

////////////////////////////////////////////////////////////
+(id)getInstanceById:(BPUUID*)id{
#ifdef ShowLog
    NSLog(@" %s", __FUNCTION__);
#endif
    
	DataManager *dbManager =[DataManager instance];
	EGODatabase *db = dbManager.currentDatabase;
    NSString *sql = @"SELECT * from invenDepartment WHERE DepartmentID=?";
	EGODatabaseResult* result = [db executeQueryWithParameters:sql, [id description],nil];
    
	if ([result count] > 0)
		return [Department instanceFromRow:[result rowAtIndex:0]];
    
	return nil;
}


-(id)initWithRowFromParent:(Department*)parentDepartment row:(EGODatabaseRow *)row{
	if ((self = [super init])) {
		self.parent = parentDepartment;
		level=parent.level + 1;
		self.id = [row UuidForColumn:@"InvenClassID"];
		self.name = [row stringForColumn:@"Name"];
		//self.level = [row intForColumn:@"Level"] + 1;
	}
	return self;
}

+(NSString*) columnNameInSet:(NSUInteger) level{
	switch (level) {
		case 0:
			return @"DeptID";
		case 1:
			return @"ClassID";
		case 2:
			return @"SubClass1ID";
		default:
			return @"SubClass2ID";
	}
}

@end

@implementation Department (Sqlite)

+(void)load{
    EXCHANGE_METHOD(getDepartments:, getDepartmentsImpl:);
}
+(NSArray*)getDepartmentsImpl:(BOOL) alt
{
	DataManager *dbManager =[DataManager instance];
	EGODatabase *db = dbManager.currentDatabase;
	EGODatabaseResult* result = [db executeQueryWithParameters:@"SELECT * FROM InvenDepartment where ClassificationType=?", @(alt), nil];
	NSMutableArray *arr = [[NSMutableArray alloc] init];
	if (result.errorCode==SQLITE_OK){
		for(EGODatabaseRow* row in result) {
			Department *item = [[Department alloc] initWithRow:row];
			[arr addObject:item];
			[item release];
		}
	}
	return [arr autorelease];
}

- (id)initWithRow:(EGODatabaseRow *)row{
	
	if ((self = [super init])) {
		level = 0;
		self.id = [row UuidForColumn:@"DepartmentID"];
		self.name = [row stringForColumn:@"Name"];
	}
	return self;
}


-(NSArray*) subLevels{
	if (!subLevels)	{
	subLevels = [[NSMutableArray alloc] init];
	if (level==3) return subLevels;
	
	NSMutableString *stmt = [NSMutableString stringWithString:@"select distinct c.* from InvenDeptSet ds join InvenClass c on c.InvenClassID=ds."];
	[stmt appendString:[Department columnNameInSet:level + 1]];
	
	NSMutableArray *params = [NSMutableArray array];
	[stmt appendFormat:@" where %@=?", [Department columnNameInSet:level]];
	[params addObject:self.id];
	Department *parentDep = parent;
	while (parentDep){
		[stmt appendFormat:@" and %@=?", [Department columnNameInSet:parentDep.level]];
		[params addObject:parent.id];
		parentDep = parentDep.parent;
	}
	
	DataManager *dbManager =[DataManager instance];
	EGODatabase *db = dbManager.currentDatabase;
	EGODatabaseResult* result = [db executeQuery:stmt parameters:params];
	if (result.errorCode==SQLITE_OK){
		for(EGODatabaseRow* row in result) {
			Department *item = [[Department alloc] initWithRowFromParent:self row:row];
			[subLevels addObject:item];
			[item release];
		}
	}
	}
	return subLevels;
}

@end
