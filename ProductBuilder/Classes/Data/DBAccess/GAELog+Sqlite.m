//
//  GAELog+Sqlite.m
//  ProductBuilder
//
//  Created by Alexander Martyshko on 10/20/14.
//  Copyright (c) 2014 Cloudworks. All rights reserved.
//

#import "GAELog.h"
#import "CEntity+Sqlite.h"

@implementation GAELog (Sqlite)

+(void)load{
    [[self class] exchangeMethod:@selector(listOfLogs) withNewMethod:@selector(listOfLogsImpl)];
    [[self class] exchangeMethod:@selector(deleteFromList:error:) withNewMethod:@selector(deleteFromListImpl:error:)];
}

+ (NSArray *)listOfLogsImpl {
    NSMutableArray *array=[[NSMutableArray alloc]init];
    
    DataManager *dbManager =[DataManager instance];
    EGODatabase *db = dbManager.currentDatabase;
    
    static NSString *stmtMemo = @"SELECT rowid, * FROM GAELog limit 1000";
    
    EGODatabaseResult* result = [db executeQuery:stmtMemo];
    
    if ([result count]>0){
        for(EGODatabaseRow* row in result) {
            [array addObject:((GAELog*)[[self class] instanceFromRow:row])];
        }
    }
    return [array autorelease];
}

-(void)fetch:(EGODatabaseRow *)row{
    [super fetch:row];
    
    self.id = [row UuidForColumn:@"GAELogId"];
    self.serverName = [row stringForColumn:@"ServerName"];
    self.appName = [row stringForColumn:@"AppName"];
    self.dateTimeStr = [row stringForColumn:@"DateTime"];
    self.threadNo = [row stringForColumn:@"ThreadNo"];
    self.type = [row intForColumn:@"LogType"];
    self.area = [row stringForColumn:@"Area"];
    self.message = [row stringForColumn:@"Message"];
    self.stackTrace = [row stringForColumn:@"StackTrace"];
    self.locationId = [row UuidForColumn:@"LocationId"];
    self.workstationId = [row UuidForColumn:@"WorkstationId"];
    self.deviceNo = [row stringForColumn:@"DeviceNo"];
    self.appShortVersion = [row stringForColumn:@"AppShortVersion"];
    self.appVersion = [row stringForColumn:@"AppVersion"];
    self.systemVersion = [row stringForColumn:@"SystemVersion"];
    self.additionalInfo = [row stringForColumn:@"AdditionalInfo"];
}

-(BOOL)save:(NSError **)error{
    @try{
        EGODatabase *db = [DataManager instance].currentDatabase;
        static NSString *stmtMemo =
        @"INSERT OR REPLACE INTO GAELog ( \
        RecCreated \
        ,RecModified \
        ,GAELogId \
        ,ServerName \
        ,AppName \
        ,DateTime \
        ,ThreadNo \
        ,LogType \
        ,Area \
        ,Message \
        ,StackTrace \
        ,LocationId \
        ,WorkstationId \
        ,DeviceNo \
        ,AppShortVersion \
        ,AppVersion \
        ,SystemVersion \
        ,AdditionalInfo \
        ) \
        VALUES ( \
        @RecCreated \
        ,@RecModified \
        ,@GAELogId \
        ,@ServerName \
        ,@AppName \
        ,@DateTime \
        ,@ThreadNo \
        ,@LogType \
        ,@Area \
        ,@Message \
        ,@StackTrace \
        ,@LocationId \
        ,@WorkstationId \
        ,@DeviceNo \
        ,@AppShortVersion \
        ,@AppVersion \
        ,@SystemVersion \
        ,@AdditionalInfo \
        )";
        
        BOOL result;
        
        NSMutableDictionary* parameters = [[NSMutableDictionary alloc]init];
        [parameters setObjectNilSafe:self.recCreated                            forKey:@"RecCreated"];
        [parameters setObjectNilSafe:self.recModified                           forKey:@"RecModified"];
        [parameters setObjectNilSafe:self.id                                    forKey:@"GAELogId"];
        [parameters setObjectNilSafe:self.serverName                            forKey:@"ServerName"];
        [parameters setObjectNilSafe:self.appName                               forKey:@"AppName"];
        [parameters setObjectNilSafe:self.dateTimeStr                           forKey:@"DateTime"];
        [parameters setObjectNilSafe:self.threadNo                              forKey:@"ThreadNo"];
        [parameters setInt:self.type                                            forKey:@"LogType"];
        [parameters setObjectNilSafe:self.area                                  forKey:@"Area"];
        [parameters setObjectNilSafe:self.message                               forKey:@"Message"];
        [parameters setObjectNilSafe:self.stackTrace                            forKey:@"StackTrace"];
        [parameters setObjectNilSafe:self.locationId                            forKey:@"LocationId"];
        [parameters setObjectNilSafe:self.workstationId                         forKey:@"WorkstationId"];
        [parameters setObjectNilSafe:self.deviceNo                              forKey:@"DeviceNo"];
        [parameters setObjectNilSafe:self.appShortVersion                       forKey:@"AppShortVersion"];
        [parameters setObjectNilSafe:self.appVersion                            forKey:@"AppVersion"];
        [parameters setObjectNilSafe:self.systemVersion                         forKey:@"SystemVersion"];
        [parameters setObjectNilSafe:self.additionalInfo                        forKey:@"AdditionalInfo"];
        
        result = [db executeUpdate:stmtMemo namedParameters:parameters];
        [parameters release];
        if (!result) {
            
            if ([db hadError])
                return NO;
        }
        return YES;
    }
    @finally{
    }
}

+(BOOL)deleteFromListImpl:(NSArray*)list error:(NSError**)error {
    EGODatabase *db = [DataManager instance].currentDatabase;
    
    static NSString *stmt = @"DELETE FROM GAELog WHERE GAELogId = @GAELogId";
    [db beginTransaction];
    BOOL result;
    for (GAELog* log in list){
        NSMutableDictionary* parameters = [[[NSMutableDictionary alloc]init] autorelease];
        [parameters setObjectNilSafe:log.id forKey:@"GAELogId"];
        result = [db executeUpdate:stmt namedParameters:parameters];
        if (!result) {
            if ([db hadError])
                [[self class] writeDbError:db error:error domain:@"com.GAELog.delete"];
            [db rollback];
            return FALSE;
        }
    }
    [db commit];
    return YES;
}

@end
