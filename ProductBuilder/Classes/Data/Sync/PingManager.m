//
//  PingManager.m
//  ProductBuilder
//
//  Created by Valery Fomenko on 10/7/11.
//  Copyright 2011 Retail Teamwork Ukraine. All rights reserved.
//

#import "PingManager.h"
#import "SyncOperationQueue.h"
#import "Reachability.h"
#import "AppParameterManager.h"
#import "Product_Builder-Swift.h"

#define OBSERVE_ONLINE_STATUS 1
NSString* const StatusViewConnectionStateChanged = @"StatusViewConnectionStateChanged";

@interface PingManager(Private)

-(void)checkOnlineStatus;

@end

@implementation PingManager

@synthesize syncManager = _syncManager;

- (id)init
{
    self = [super init];
    if (self) {

        networkChangedSemaphore = dispatch_semaphore_create(0);
        waitForRequestSemaphore = dispatch_semaphore_create(0);
    }
    
    return self;
}

+(PingManager *)instance {
    static dispatch_once_t pred;
    static PingManager *instance = nil;
    
    dispatch_once(&pred, ^{
        instance = [[PingManager alloc] init];
    });
    return instance;
}

-(void)startPing{
    
    if (!AppParameterManager.instance.disablePing) {
        if (OBSERVE_ONLINE_STATUS){
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reachabilityChanged:) name:kReachabilityChangedNotification object:nil];
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(willEnterForeground:) name:UIApplicationWillEnterForegroundNotification object:nil];
        }
        if (!queue){
            queue = dispatch_queue_create("com.cloudworks.ping", nil);
            dispatch_async(queue, ^{[self checkOnlineStatus];});
        }
    }
}

-(void)willEnterForeground:(NSNotification*)object{
    [_syncManager readSettings];
    prevResult = FALSE;
    lastCheckTime = 15;
}

-(BOOL)isNetworkReachable{
    return [Reachability isNetworkReachableViaWiFi];
}

#pragma mark NetReachabilityDelegate

-(void)reachabilityChanged:(NSNotification *)note{
    dispatch_semaphore_signal(networkChangedSemaphore);
}

-(void)checkStatus{
    dispatch_semaphore_signal(networkChangedSemaphore);
}

-(void)syncronize{
    //start default sync and wait until it finishes
    if (![[SyncOperationQueue sharedInstance]operationCount])
        [[SyncOperationQueue sharedInstance]startDefaultSync:FALSE];
}

-(void)checkOnlineStatus{
    
    while (true){
        @autoreleasepool {
            //reset semaphore
            while (!dispatch_semaphore_wait(networkChangedSemaphore,DISPATCH_TIME_NOW)){
                ;
            }
            //if ([self isNetworkReachable]){
                if (OBSERVE_ONLINE_STATUS){
                    if (!_syncManager){
                        _syncManager = [[SyncManager alloc]init];
                        _syncManager.defaultTimeout = 13;
                    }
                    [_syncManager test:self];
                    dispatch_semaphore_wait(waitForRequestSemaphore, DISPATCH_TIME_FOREVER);
                }
                //if status ok then run sync and wait
                if ([self isOnline]){
                    [self syncronize];
                }
            //}
            //[NSThread sleepForTimeInterval:30];
            dispatch_semaphore_wait(networkChangedSemaphore,dispatch_time(DISPATCH_TIME_NOW, 15LL * NSEC_PER_SEC));
        }
    }
}

- (void) operation:(WebServiceOperation *)operation completedWithResponse:(WebServiceResponse *)response{
    if (waitForRequestSemaphore)
        dispatch_semaphore_signal(waitForRequestSemaphore);
    BOOL old_isOnline = [self isOnline];
    BOOL result = response && !response.error;
    NSTimeInterval timeSinceLastCheck = [NSDate timeIntervalSinceReferenceDate] - lastCheckTime;
    //status changes to FALSE only if we get FAIL second time during 14 sec
    status = result ? TRUE : !(!result && !prevResult && timeSinceLastCheck > 14);
    prevResult = result;
    lastCheckTime = [NSDate timeIntervalSinceReferenceDate];
    BOOL new_isOnline = [self isOnline];
    if (old_isOnline!=new_isOnline|| !firstRun){
        firstRun = TRUE;
        dispatch_async(dispatch_get_main_queue(),^{
            [[NSNotificationCenter defaultCenter] postNotificationName:StatusViewConnectionStateChanged object:@(new_isOnline)];
        });
    }
}

-(BOOL)isOnline {
    return OBSERVE_ONLINE_STATUS ? status : TRUE;
}

-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    dispatch_release(queue);
    [_syncManager release];
    if (waitForRequestSemaphore)
        dispatch_release(waitForRequestSemaphore);
    if (networkChangedSemaphore)
        dispatch_release(networkChangedSemaphore);
    [super dealloc];
}

@end
