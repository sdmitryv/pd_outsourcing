//
//  SyncTableHandler.m
//  ProductBuilder
//
//  Created by Valery Fomenko on 6/28/12.
//  Copyright (c) 2012 Cloudworks. All rights reserved.
//

#import "SyncTableHandler.h"
#import "DataManager.h"

@implementation SyncTableHandler

+(NSString*)pathForSqlResource:(NSString*)fileName{
    NSString* path = [[NSBundle mainBundle] pathForResource:fileName ofType:@"sql"];
    return path;
}

+(BOOL)executeFile:(NSString*)fileName{
    return [self executeFileByFullPath:[self pathForSqlResource:fileName]];
}

+(BOOL)executeFileByFullPath:(NSString*)path{
    if (!path) return FALSE;
    NSString *stmt = [NSString stringWithContentsOfFile:path encoding:NSUTF8StringEncoding error:nil];
    return [[DataManager instance].currentDatabase execute:stmt];
}

+(BOOL)beforeTruncateTable:(SyncTableOperation*)operation{
    return [self beforeClearTable:operation];
}

+(BOOL)beforeClearTable:(SyncTableOperation*)operation{
    BOOL result = [[DataManager instance] dropTableIndexes:operation.tableName];
    NSString* pathForDropIndexes = [self pathForSqlResource:[self nameForDropIndexes:operation]];
    NSString* pathForCreateIndexes = [self pathForSqlResource:[self nameForCreateIndexes:operation]];
    if (pathForDropIndexes && pathForCreateIndexes){
        result &= [self executeFileByFullPath:pathForDropIndexes];
    }
    operation.tableIsMissingIndexes = TRUE;
    return result;
}

+(NSString*)nameForDropIndexes:(SyncTableOperation*)operation{
    return [NSString stringWithFormat:@"Drop%@Indexes", operation.tableName];
}

+(NSString*)nameForCreateIndexes:(SyncTableOperation*)operation{
    return [NSString stringWithFormat:@"Create%@Indexes", operation.tableName];
}

+(void)beforeInsertIntoTable:(SyncTableOperation*)operation totalRows:(NSInteger)totalRows{
    if (totalRows<1000) return;
    NSUInteger currentRowsCount = 0;
    EGODatabaseResult* result = [[[DataManager instance] currentDatabase] executeQuery:[NSString stringWithFormat:@"select count(*) from %@", operation.tableName]];
    if (result.count){
        currentRowsCount = [((EGODatabaseRow*)(result.rows)[0])intForColumnIndex:0];
    }
    if (totalRows>=1000 && (currentRowsCount ==0 || (double)totalRows/currentRowsCount > 0.25)){
        NSString* oldTitle = [operation.title retain];
        operation.title = [NSString stringWithFormat:@"%@ %@s...", NSLocalizedString(@"OPERATION_PREPARING_TEXT", nil), operation.tableName];
        [self beforeClearTable:operation];
        operation.title = oldTitle;
        [oldTitle release];
    }
}

+(void)afterInsertIntoTable:(SyncTableOperation*)operation totalRows:(NSInteger)totalRows{
    if (operation.tableIsMissingIndexes){
        NSString* oldTitle = [operation.title retain];
        operation.title = [NSString stringWithFormat:@"%@ %@s...", NSLocalizedString(@"OPERATION_INDEXING_TEXT", nil), operation.tableName];
        [[DataManager instance] createTableIndexes:operation.tableName];
        [self executeFile:[self nameForCreateIndexes:operation]];
        operation.title = oldTitle;
        [oldTitle release];
    }
}

@end
