//
//  MyClass.m
//  CloudworksPOS
//
//  Created by valera on 7/21/11.
//  Copyright 2011 Retail Teamwork Ukraine. All rights reserved.
//

#import "SyncOperationQueue.h"
#import "SyncOperation.h"
#import "SyncDefaultOperations.h"
#import "SyncTableOperation.h"
#import "Product_Builder-Swift.h"

NSString* const SyncQueueDidErrorOccurNotification = @"SyncQueueDidErrorOccurNotification";
NSString* const SyncQueueDidStartOperationNotification = @"SyncQueueDidStartOperationNotification";
NSString* const SyncQueueDidEndOperationNotification = @"SyncQueueDidEndOperationNotification";
NSString* const SyncQueueDidChangeOperationTitleNotification = @"SyncQueueDidChangeOperationTitleNotification";
NSString* const SyncQueueDidCompleteNotification = @"SyncQueueDidCompleteNotification";
//NSString* const SyncQueueDidCancelNotification = @"SyncQueueDidCancelNotification";
NSString* const SyncQueueInProgressNotification = @"SyncQueueInProgressNotification";
NSString* const SyncQueueDidUpdateRecordsOperationNotification = @"SyncQueueDidUpdateRecordsOperationNotification";
NSString* const SyncQueueErrorKey =@"SyncQueueErrorKey";
NSString* const SyncQueueIsCanceledKey =@"SyncQueueIsCanceledKey";
NSString* const SyncQueueErrorDescriptionKey =@"SyncQueueErrorDescriptionKey";
NSString* const SyncQueueCurrentOperationKey =@"SyncQueueCurrentOperationKey";
NSString* const SyncQueueOperationObject = @"SyncQueueOperationObject";
NSString* const SyncQueueSyncLocationMistmatch = @"SyncQueueSyncLocationMistmatch";
NSString* const SyncQueueCurrentLocationKey = @"SyncQueueCurrentLocationKey";
NSString* const SyncQueueOpsLocationKey = @"SyncQueueOpsLocationKey";
NSString* const SyncQueueBackupOperationStateChanged = @"SyncQueueBackupOperationStateChangedNotification";

//static void * kOperationCountChanged = &kOperationCountChanged;

@interface SyncOperationQueue(){
    NSTimeInterval lastTotalRuntime;
    NSTimeInterval lastStartInit;
}
@end

@implementation SyncOperationQueue

-(void)postNotification:(NSString*)name userInfo:(id)userInfo sender:(SyncOperation *)syncOperation{
    dispatch_async(dispatch_get_main_queue(),^{
        [[NSNotificationCenter defaultCenter] postNotificationName:name object:self userInfo:userInfo];
    });
    if (breakOnError && [name isEqualToString:SyncQueueDidErrorOccurNotification]){
        NSError * error = [userInfo valueForKey:SyncQueueErrorKey];
        if ((!syncOperation || syncOperation.isCancelled || ([error.domain isEqualToString:NSURLErrorDomain] && error.code == NSURLErrorNotConnectedToInternet)) && !([error.domain isEqualToString:@"WebServiceResponseHTTP"] && error.code != 404) && ![error.domain isEqualToString:@"com.cloudworks.syncOperation"]) {
            if ([NSThread isMainThread])
                [self cancelAllOperationsAsync];
            else
                [self cancelAllOperations];
        }
    }
}

-(void)postNotification:(NSString*)name userInfo:(id)userInfo{
    [self postNotification:name userInfo:userInfo sender:nil];
}

-(void)postNotification:(NSString*)name{
    [self postNotification:name userInfo:nil sender:nil];
}

-(id)init{
    if ((self = [super init])){
//        [self addObserver:self forKeyPath:@"operationCount" options:NSKeyValueObservingOptionNew | NSKeyValueObservingOptionOld context:kOperationCountChanged];
        [self setMaxConcurrentOperationCount:1];
        breakOnError = FALSE;
    }
    return self;
}

-(NSTimeInterval)lastTotalRuntime{
    return lastTotalRuntime;
}

-(void)cancelAllOperations{
    [super cancelAllOperations];
}

-(void)cancelAllOperationsAsync{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{[self cancelAllOperations];});
}

+(SyncOperationQueue *)sharedInstance {
    static dispatch_once_t pred;
    static SyncOperationQueue *instance = nil;
    
    dispatch_once(&pred, ^{
        instance = [[SyncOperationQueue alloc] init];
        [[NSNotificationCenter defaultCenter] addObserver:self.class selector:@selector(syncQueueDidComplete:) name:SyncQueueDidCompleteNotification object:instance];
    });
    return instance;
}

+ (void) syncQueueDidComplete:(NSNotification *) notification{
    [[AppSettingManager instance] setLastSyncDate:[NSDate date]];

}

-(void)addOperation:(NSOperation *)op{
    [self addOperations:@[op] waitUntilFinished:FALSE];
}

-(void)addOperations:(NSArray *)ops waitUntilFinished:(BOOL)wait{
    [self addOperations:ops waitUntilFinished:wait breakOnError:FALSE];
}

-(void)addOperations:(NSArray *)ops waitUntilFinished:(BOOL)wait breakOnError:(BOOL)breakOnErrorValue{
    if(!ops || !ops.count) return;
    NSMutableArray* operations = [[NSMutableArray alloc]initWithArray:ops];
    __block NSOperation* finishedHandlerOperation = [[NSOperation alloc]init];
    finishedHandlerOperation.completionBlock = ^{
        lastTotalRuntime = [NSDate timeIntervalSinceReferenceDate] - lastStartInit;
        [self postNotification:SyncQueueDidCompleteNotification userInfo:@{SyncQueueIsCanceledKey: @(finishedHandlerOperation.isCancelled)}];
        breakOnError = FALSE;
    };
    [operations addObject:finishedHandlerOperation];
    [finishedHandlerOperation release];
    [operations enumerateObjectsUsingBlock: ^(id op, NSUInteger idx, BOOL *stop)
     {
         if ([[op class]isSubclassOfClass:[SyncOperation class]]){
             ((SyncOperation*)op).parentQueue = self;
         }
     }];
    breakOnError = breakOnErrorValue;
    lastTotalRuntime = 0;
    lastStartInit = [NSDate timeIntervalSinceReferenceDate];
    [super addOperations:operations waitUntilFinished:wait];
    [operations release];
    
}

-(void)setMaxConcurrentOperationCount:(NSInteger)cnt{
    if (cnt!=1) return;
    [super setMaxConcurrentOperationCount:cnt];
}

-(void)dealloc{
    [super dealloc];
}

-(void)startDefaultInit{
    NSAutoreleasePool* pool  = [[NSAutoreleasePool alloc]init];
    [self addOperations:[SyncDefaultOperations defaultDownloadOperations:SyncOperationModeInitialization] waitUntilFinished:FALSE
                                         breakOnError:TRUE];
    [pool release];
}

-(void)startDefaultSync{
    [self startDefaultSync:FALSE];
}

-(void)startDefaultSync:(BOOL)waitUntilFinished{
    if (DataManager.instance.isTempDb) return;
    @autoreleasepool {
        NSMutableArray* syncOps = [[NSMutableArray alloc]initWithArray:[SyncDefaultOperations defaultUploadOperations]];
        if (![[self class] partialSynchronization]){
            [syncOps addObjectsFromArray:[SyncDefaultOperations defaultDownloadOperations:SyncOperationModeSynchronization]];
        }
        [self addOperations:syncOps waitUntilFinished:waitUntilFinished breakOnError:TRUE];
        [syncOps release];
    }
}

+(BOOL)partialSynchronization{
    /*UIViewController* parentController = [MainViewController50 sharedInstance];
    UIViewController* controller = parentController.childModalController ?:parentController.presentedViewController;*/
    if (NO)
        return TRUE;
    return FALSE;
}


@end
