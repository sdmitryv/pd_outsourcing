//
//  GetDateTimeOperation.m
//  ProductBuilder
//
//  Created by Viacheslav Lulakov on 6/12/14.
//  Copyright (c) 2014 Cloudworks. All rights reserved.
//

#import "GetDateTimeOperation.h"
#import "NSDate+ISO8601Parsing.h"

@implementation GetDateTimeOperation

- (void)dealloc {
    [_date release];
    [_timeZone release];
    
    [super dealloc];
}

- (void) sync {
    [_timeZone release];
    _timeZone = nil;
    [syncManager getLocalDateTime];
    [self waitForRequest];
    
    if (result) {
        XmlElement * xmlResult = (XmlElement *)operationResult;
        if ([xmlResult.name isEqualToString:@"GetLocalDateTimeResult"]) {
            NSTimeZone* timeZone = nil;
            _date = [[NSDate dateWithString:xmlResult.value timeZone:&timeZone] retain];
            _timeZone = [timeZone retain];
        }
    }
}

@end
