//
//  UpdateDeviceStatsOperation.h
//  DeviceAgent
//
//  Created by Alexander Martyshko on 11/21/13.
//  Copyright (c) 2013 Cloudworks. All rights reserved.
//

#import "SyncOperation.h"

@interface UpdateDeviceStatsOperation : SyncOperation {

}

@property (nonatomic, assign) BOOL isSecond;
+(id)secondSyncOperation;

@end
