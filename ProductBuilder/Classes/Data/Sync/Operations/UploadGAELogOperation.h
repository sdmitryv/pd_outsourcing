//
//  UploadGAELogOperation.h
//  ProductBuilder
//
//  Created by Alexander Martyshko on 10/20/14.
//  Copyright (c) 2014 Cloudworks. All rights reserved.
//

#import "SyncOperation.h"

@interface UploadGAELogOperation : SyncOperation <NSURLSessionDelegate>

@end
