//
//  UploadGAELogOperation.m
//  ProductBuilder
//
//  Created by Alexander Martyshko on 10/20/14.
//  Copyright (c) 2014 Cloudworks. All rights reserved.
//

#import "UploadGAELogOperation.h"
#import "GAELog.h"
#import "NSURLSession+Extentions.h"
#import "SettingManager.h"

@interface UploadGAELogOperation()

@property (atomic, retain) NSURLSessionDataTask* task;

@end

@implementation UploadGAELogOperation

- (void)dealloc {
    if (_task) {
        [_task cancel];
        [_task release];
        _task = nil;
    }
    [super dealloc];
}

-(id)init{
    if ((self =[super init])){
        self.name = NSLocalizedString(@"UPLOADING_GAE_LOGS_TEXT", nil);
        self.title = [NSString stringWithFormat:@"%@...", NSLocalizedString(@"UPLOADING_GAE_LOGS_TEXT", nil)];
    }
    return self;
}

- (void) sync {
    NSArray* logsList = [GAELog listOfLogs];
    if (!logsList.count || self.isCancelled) return;
    
    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
    
    NSMutableArray* logsDictionaryList = [[NSMutableArray alloc]initWithCapacity:logsList.count];
    for (GAELog* log in logsList){
        [logsDictionaryList addObject:log.dictionatyRepresentation];
    }
    

    NSURL *gaeLoggerServiceURL = [NSURL URLWithString:[SettingManager instance].gaeLoggerServiceUrl];
    NSURL *url = [gaeLoggerServiceURL URLByAppendingPathComponent:@"/put_ipad_log"];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url
                                                                cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                            timeoutInterval:30];
    [request setHTTPMethod:@"POST"];
    [request setValue:@"application/x-www-form-urlencoded"  forHTTPHeaderField:@"Content-Type"];
    
    NSError *error = nil;
    NSData *postData = [NSJSONSerialization dataWithJSONObject:logsDictionaryList options:0 error:&error];
    [request setValue:[NSString stringWithFormat:@"%lu", (unsigned long)[postData length]] forHTTPHeaderField:@"Content-Length"];
    [request setHTTPBody:postData];
    
    
    NSURLSessionConfiguration* sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
    sessionConfig.timeoutIntervalForRequest = 30;
    NSOperationQueue* delegateQueue = [[[NSOperationQueue alloc] init]autorelease];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:sessionConfig delegate:self delegateQueue:delegateQueue];
    
    __block __typeof(self) selfCopy = self;
   self.task = [session dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable anError) {
        if (anError.code==NSURLErrorCancelled){
            [session invalidateAndCancel];
            return;
        }
        [session finishTasksAndInvalidate];
        if (anError) {
            [selfCopy handleError:error];
        }
        else{
            if (data){
                NSError* error = nil;
                NSDictionary *jsonResponseDictionary = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
                NSString *status = jsonResponseDictionary[@"Status"];
                if (status && [status isEqualToString:@"SUCCESSFULLY"]){
                    [GAELog deleteFromList:logsList error:nil];
                }
            }
            dispatch_semaphore_signal(waitForRequestSemaphore);
        }
    }];
    
    [self.task resume];
    [request release];
    [logsDictionaryList release];
    [pool release];
}

- (void)cancel {
    [super cancel];
    if (self.task) {
        [self.task cancel];
        self.task = nil;
    }
}

#pragma mark -
#pragma mark NSURLSessionDelegate

- (void)URLSession:(NSURLSession *)session didReceiveChallenge:(NSURLAuthenticationChallenge *)challenge
 completionHandler:(void (^)(NSURLSessionAuthChallengeDisposition disposition, NSURLCredential * __nullable credential))completionHandler{
    [session defaultHandlerForDidReceiveChallenge:challenge completionHandler:completionHandler userName:nil password:nil errorHandler:^(NSError* authError){ [self handleError:authError]; }];
}

-(void)handleError:(NSError*)anError{

    NSMutableDictionary* userInfo = [[[NSMutableDictionary alloc]initWithObjectsAndKeys:self, SyncQueueCurrentOperationKey,parentQueue, SyncQueueOperationObject, nil] autorelease];
    userInfo[NSLocalizedDescriptionKey] = [anError localizedDescription];
    
    NSError *err = [NSError errorWithDomain:@"com.cloudworks.syncOperation" code:0 userInfo:userInfo];
    [self postError:[err localizedDescription] error:err];

    [self signalOperationDidComplete];

}

@end
