//
//  UploadTableOperation.h
//  ProductBuilder
//
//  Created by Виталий Гервазюк on 3/12/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "UploadOperation.h"

@interface UploadTableOperation : UploadOperation {
    
    int uploadTablesCnt;
}

-(SEL)uploadSelector;
-(NSString*)tableName;

-(NSString*)operationTitle;

@end
