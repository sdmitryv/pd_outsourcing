//
//  BackupDbOperation.h
//  ProductBuilder
//
//  Created by Valery Fomenko on 3/2/12.
//  Copyright (c) 2012 Retail Teamwork Ukraine. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SyncOperation.h"
#import "ASIDataCompressor.h"
#import "GoogleCloudStorageManager.h"

typedef enum : NSUInteger {
    PreparingBackUp,
    SendingBackUp,
} BackupDbOperationType;

@interface BackupDbOperation : SyncOperation<ASIDataCompressorDelegate, GoogleCloudStorageManagerDelegate>
@property BackupDbOperationType type;
@end
