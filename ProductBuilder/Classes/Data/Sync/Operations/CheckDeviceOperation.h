//
//  CheckDeviceOperation.h
//  DeviceAgent
//
//  Created by Alexander Martyshko on 11/21/13.
//  Copyright (c) 2013 Cloudworks. All rights reserved.
//

#import "SyncOperation.h"

// posted only in SYNC mode and only if value of activity (isDeactivated property) of the device was changed
extern NSString* const deviceStateChangedNotification;

@class Device, Location;

@interface CheckDeviceOperation : SyncOperation {

    Device *_device;
}

@property (nonatomic, readonly) Device *device;

@end
