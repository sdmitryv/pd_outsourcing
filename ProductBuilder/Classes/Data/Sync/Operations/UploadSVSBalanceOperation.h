//
//  UploadSVSBalanceOperation.h
//  ProductBuilder
//
//  Created by Vitaliy Gervazuk on 1/10/13.
//  Copyright (c) 2013 Cloudworks. All rights reserved.
//

#import "UploadTableOperation.h"

@interface UploadSVSBalanceOperation : UploadTableOperation

@end
