//
//  UpdateDeviceStatsOperation.m
//  DeviceAgent
//
//  Created by Alexander Martyshko on 11/21/13.
//  Copyright (c) 2013 Cloudworks. All rights reserved.
//

#import "UpdateDeviceStatsOperation.h"
#import "DeviceStats.h"
#import "AppSettingManager.h"


@implementation UpdateDeviceStatsOperation


+(id)secondSyncOperation{
    
    UpdateDeviceStatsOperation *operation = [[[UpdateDeviceStatsOperation alloc]init]autorelease];
    operation.isSecond = YES;
    return operation;
}

- (void)dealloc {
    
    [super dealloc];
}

-(BOOL)shouldCancelOnError:(NSError*)error{
    return NO;
}

- (NSString *)title {
    return NSLocalizedString(@"OPERATION_UPDATE_DEVICE_STATS", nil);
}


- (void) sync {
    
    if (self.mode == SyncOperationModeInitialization)
        return;

    
    NSDate *now = [NSDate date];

    if ([SyncOperationQueue partialSynchronization]) {
        
        NSDate *lastUpload = [[AppSettingManager instance] dateForKey:@"LastStatsUploadDate"];
        NSTimeInterval delta = [now timeIntervalSinceDate:lastUpload];
        if (fabs(delta) < 5*60)
            return;
    }

    result = FALSE;
    
    [syncManager updateDeviceStats:[DeviceStats getStatsList]];
    [self waitForRequest];
    
    if (result && _isSecond) {
        
        [[AppSettingManager instance] setDate:now forKey:@"LastStatsUploadDate"];
    }
}

@end
