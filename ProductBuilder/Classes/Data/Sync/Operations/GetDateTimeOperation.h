//
//  GetDateTimeOperation.h
//  ProductBuilder
//
//  Created by Viacheslav Lulakov on 6/12/14.
//  Copyright (c) 2014 Cloudworks. All rights reserved.
//

#import "SyncOperation.h"

@interface GetDateTimeOperation : SyncOperation

@property (nonatomic, readonly) NSDate * date;
@property (nonatomic, readonly) NSTimeZone * timeZone;

@end
