//
//  UploadExceptionLogOperation.m
//  ProductBuilder
//
//  Created by Alexander Martyshko on 10/3/13.
//  Copyright (c) 2013 Cloudworks. All rights reserved.
//

#import "UploadExceptionLogOperation.h"
#import "DataManager.h"

@implementation UploadExceptionLogOperation

-(id)init{
    if ((self =[super init])){
        self.name = self.title = NSLocalizedString(@"UPLOADING_EXEPTION_LOGS_TEXT", nil);
    }
    return self;
}

-(BOOL)shouldCancelOnError:(NSError*)error{
    return FALSE;
}

- (void) sync{
    @synchronized(self.class){
        if (self.isCancelled) return;
        DataManager *dataManager = [DataManager instance];
        self.updateItems = [dataManager getSyncRecordsFromTable:@"MobileExceptionLog" whereClause:@"1=1 limit 100"];
        
        for (NSInteger i=0;i<updateItems.count;i++){
            if (self.isCancelled) break;
            result = FALSE;
            NSDictionary *log = updateItems[i];
            [syncManager uploadExceptionLog:log];
            [self waitForRequest];
             if (self.isCancelled) break;
            if (result){
                id rowid = log[@"rowid"];
                //here we get version
                id syncState = log[@"SyncState"];
                if ([syncState isKindOfClass:[NSString class]]){
                    [[dataManager currentDatabase]beginTransaction];
                    NSNumber* newSyncState = [dataManager getSyncStateForRowInTable:@"MobileExceptionLog" rowId:rowid];
                     if (self.isCancelled) break;
                    if (syncState && newSyncState && [syncState intValue]==[newSyncState intValue]){
                        [dataManager commitSyncRecordFromTable:@"MobileExceptionLog" rownum:rowid];
                        [[dataManager currentDatabase]commit];
                    }
                    else{
                        [[dataManager currentDatabase]rollback];
                    }
                }
                else{ //we won't get here but just in case..
                    [dataManager commitSyncRecordFromTable:@"MobileExceptionLog" rownum:rowid];
                }
            }
            
            self.progress += 1.0/updateItems.count;
            if (self.isCancelled) break;
        }
        //save 100 last touched records only
        [[dataManager currentDatabase]executeQuery:@"delete from MobileExceptionLog where  SyncState is null and rowid not in (select rowid from MobileExceptionLog where SyncState is null order by rowid desc limit 100)"];

    }
}


@end
