//
//  UploadExceptionLogOperation.h
//  ProductBuilder
//
//  Created by Alexander Martyshko on 10/3/13.
//  Copyright (c) 2013 Cloudworks. All rights reserved.
//

#import "UploadOperation.h"

@interface UploadExceptionLogOperation : UploadOperation

@end
