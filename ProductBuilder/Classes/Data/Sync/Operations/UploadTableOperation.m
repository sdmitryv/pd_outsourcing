//
//  UploadTableOperation.m
//  ProductBuilder
//
//  Created by Виталий Гервазюк on 3/12/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "UploadTableOperation.h"
#import "DataManager.h"

@implementation UploadTableOperation

-(NSString *)operationTitle {
    return nil;
}

-(SEL)uploadSelector {
    return nil;
}

-(NSString *)tableName {
    return nil;
}


-(BOOL)shouldCancelOnError:(NSError*)error{
    return FALSE;
}

-(id)init{
    if ((self =[super init])){
        
        self.name = self.title = [self operationTitle];
        uploadTablesCnt = 10;
    }
    return self;
}

-(NSString*)syncColumnName{
    return @"SyncState";
}

- (void) sync{
    DataManager *dataManager = [DataManager instance];
    NSString* tblName = [self tableName];
    NSString* syncColumnName = [self syncColumnName];
    self.updateItems = [dataManager getSyncRecordsFromTable:tblName syncColumnName:syncColumnName];
    if (self.isCancelled || !self.updateItems.count) return;
    self.updateItems.name = tblName;
    
    if (![syncManager respondsToSelector:[self uploadSelector]]){
        return;
    }
    
    NSNamedMutableArray *objectsToSend = [[NSNamedMutableArray alloc] init];
    objectsToSend.name = tblName;
    
    if (uploadTablesCnt <= 0)
        uploadTablesCnt = 10;
    
    for (NSInteger i=1; i<=self.updateItems.count; i++) {
        [objectsToSend addObject:self.updateItems[i-1]];
        
        if (i % uploadTablesCnt == 0) {
            
            if (self.isCancelled)
                break;
            
            [self uploadObjects:objectsToSend];
            [objectsToSend release];
            objectsToSend = [[NSNamedMutableArray alloc] init];
            objectsToSend.name = tblName;
            
            self.progress += ((float)uploadTablesCnt)/self.updateItems.count;
        }
        
    }
    
    if (objectsToSend.count && !self.isCancelled)
        [self uploadObjects:objectsToSend];
    
    [objectsToSend release];
    
    self.progress = 1.0;
}

- (void)uploadObjects:(NSNamedMutableArray *)objects {
    DataManager *dataManager = [DataManager instance];
    NSString* tblName = [self tableName];
    NSString* syncColumnName = [self syncColumnName];
    
    result = FALSE;
    
    [syncManager performSelector:[self uploadSelector] withObject:objects];
    [self waitForRequest];
    if (result){
        for(NSDictionary* object in objects.data){
            id rowid = object[@"rowid"];
            //here we get version
            id syncState = object[syncColumnName];
            if ([syncState isKindOfClass:[NSString class]]){
                [[dataManager currentDatabase]beginTransaction];
                NSNumber* newSyncState = [dataManager getSyncStateForRowInTable:tblName rowId:rowid syncColumnName:syncColumnName];
                if (syncState && newSyncState && [syncState intValue]==[newSyncState intValue]){
                    [dataManager commitSyncRecordFromTable:tblName rownum:rowid syncColumnName:syncColumnName];
                    [[dataManager currentDatabase]commit];
                }
                else{
                    [[dataManager currentDatabase]rollback];
                }
                
            }
            else{ //we won't get here but just in case..
                [dataManager commitSyncRecordFromTable:tblName rownum:rowid syncColumnName:syncColumnName];
            }
        }
    }
}

@end
