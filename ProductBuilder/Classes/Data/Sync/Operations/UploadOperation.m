//
//  UploadOperation.m
//  ProductBuilder
//
//  Created by Valery Fomenko on 3/2/12.
//  Copyright (c) 2012 Retail Teamwork Ukraine. All rights reserved.
//

#import "UploadOperation.h"

@implementation UploadOperation

@synthesize updateItems;

-(void)dealloc{
    [updateItems release];
    [super dealloc];
}

@end
