//
//  SyncStartOperation.h
//  ProductBuilder
//
//  Created by Valery Fomenko on 3/21/12.
//  Copyright (c) 2012 Cloudworks. All rights reserved.
//

#import "SyncOperation.h"

@interface SyncStartOperation : SyncOperation

@end
