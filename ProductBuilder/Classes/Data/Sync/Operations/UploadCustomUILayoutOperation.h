//
//  UploadCustomUILayoutOperation.h
//  Shipments
//
//  Created by valery on 6/10/15.
//  Copyright (c) 2015 Cloudworks. All rights reserved.
//

#import "UploadOperation.h"
#import "SyncOperation.h"
#import "CustomUILayout.h"

@interface UploadCustomUILayoutOperation : SyncOperation{
    CustomUILayoutManager* _layoutManager;
}
-(id)initWithCustomUILayoutManager:(CustomUILayoutManager*)layoutManager;

@end
