//
//  SyncRecordStatusDeletedOperation.h
//  ProductBuilder
//
//  Created by valera on 3/6/13.
//  Copyright (c) 2013 Cloudworks. All rights reserved.
//

#import "SyncTableOperation.h"

@interface SyncRecordStatusDeletedOperation : SyncTableOperation

@property (nonatomic, copy) void(^beforeRowsWillDelete)(NSString* tableName);
+(id)syncTableOperation:(NSString *)table mode:(SyncOperationMode)mode beforeRowsWillDelete:(void (^)(NSString* tableName))beforeRowsWillDelete;
@end
