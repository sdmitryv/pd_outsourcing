//
//  UploadDeviceOperation.m
//  DeviceAgent
//
//  Created by Alexander Martyshko on 11/21/13.
//  Copyright (c) 2013 Cloudworks. All rights reserved.
//

#import "UploadDeviceOperation.h"
#import "Device.h"
#import "Settings.h"
#import "Location.h"
#import "DeviceAgentManager.h"
#import <CoreLocation/CoreLocation.h>
#import "AppSettingManager.h"

@implementation UploadDeviceOperation

@synthesize SVSAuthTokenForUpload;

- (void)dealloc {
    
    [SVSAuthTokenForUpload release];
    
    [super dealloc];
}

-(BOOL)shouldCancelOnError:(NSError*)error{
    return YES;
}

- (NSString *)title {
    return NSLocalizedString(@"OPERATION_CHEKING_DEVICE", nil);
}

- (void) sync {
    
    Device *_device = [[[DeviceAgentManager sharedInstance].currentDevice retain] autorelease];
    
    if (!_device)
        return;
    
    _device.SVSAuthToken = SVSAuthTokenForUpload;
    
    [syncManager updateDevice:[_device dictionaryRepresentation]];
    [self waitForRequest];
}



@end
