//
//  UploadSecurityLogOperation.m
//  ProductBuilder
//
//  Created by Vitaliy Gervazuk on 9/13/12.
//  Copyright (c) 2012 Cloudworks. All rights reserved.
//

#import "UploadSecurityLogOperation.h"

@implementation UploadSecurityLogOperation


-(NSString *)operationTitle {
    return NSLocalizedString(@"UPLOADING_SECURITY_TEXT", nil);
}


-(SEL)uploadSelector {
    return @selector(updateSecurityLogs:);
}

-(NSString*)tableName {
    return @"SecurityLog";
}

@end
