//
//  SyncStartInitOperation.m
//  ProductBuilder
//
//  Created by Valery Fomenko on 3/2/12.
//  Copyright (c) 2012 Retail Teamwork Ukraine. All rights reserved.
//

#import "SyncStartInitOperation.h"
#import "SyncAction.h"
#import "Settings.h"
#import "SyncTableHandler.h"
#import "AppSettingManager.h"

@implementation SyncStartInitOperation

-(id)init{
    if ((self =[super init])){
        self.title = NSLocalizedString(@"OPERATION_PREPARING_TEXT", nil);
    }
    return self;
}

-(void)sync{
    
    if (self.isCancelled) return;

    SyncAction *initAction = [SyncAction getSyncAction:@"Init.Start" target:@"System"];
    if (initAction != nil) {
        initAction.recModified = [NSDate date];
        [initAction markDirty];
    }
    else{
        initAction = [[[SyncAction alloc] init] autorelease];
        initAction.action = @"Init.Start";
        initAction.target = @"System";
    }
    [initAction save];
    DataManager* dm = [DataManager instance];
    [[dm currentDatabase] execute:@"PRAGMA foreign_keys = OFF;"];
    [[dm currentDatabase] beginTransaction];
    BOOL res = FALSE;
    @try{

        //do something
        res = TRUE;
    }
    @finally {
        [[dm currentDatabase] execute:@"PRAGMA foreign_keys = ON;"];
        if (res)
            [[dm currentDatabase]commit];
        else
            [[dm currentDatabase]rollback];
    }
}

@end
