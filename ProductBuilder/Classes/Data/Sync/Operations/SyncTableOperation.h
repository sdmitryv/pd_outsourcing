//
//  SyncTableOperation.h
//  ProductBuilder
//
//  Created by Valery Fomenko on 3/2/12.
//  Copyright (c) 2012 Retail Teamwork Ukraine. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SyncOperation.h"

@interface SyncTableOperation : SyncOperation{
    NSUInteger itemsCount;
    NSString    * tableName;
    BOOL recordsInserted;
    BOOL isMandatory;
    NSString *_whereCondition;
    void(^_completedSyncBlock)(SyncTableOperation* operation, NSInteger affectedRowsCount);
    BOOL _isNotRequiresTableCount;
}
@property (nonatomic, retain) NSString	* tableName;
@property (nonatomic, assign) NSUInteger itemsCount;
@property (nonatomic, assign) BOOL tableIsMissingIndexes;
@property (nonatomic, assign) BOOL ignoreMaxModifiedRecord;
@property (nonatomic, retain) NSString * whereCondition;
@property (nonatomic, retain) NSDictionary *conditionParams;
@property (nonatomic, copy) void(^completedSyncBlock)(SyncTableOperation* operation, NSInteger affectedRowsCount);
+(NSNumber*) serverVersion;
+(void)setServerVersion:(NSNumber*)version;
//- (NSThread *)threadForRequest;
-(id) initWithTable:(NSString *)table mode:(SyncOperationMode)aMode;
-(id) initWithTable:(NSString *)table title:(NSString*)titleVal mode:(SyncOperationMode)aMode;
-(id) initWithTable:(NSString *)table title:(NSString*)titleVal mode:(SyncOperationMode)aMode whereCondition:(NSString *)whereCondition conditionParams:(NSDictionary *)conditionParams completedSync:(void (^)(SyncTableOperation* operation, NSInteger affectedRowsCount))aCompletedSyncBlock;
+(id)syncTableOperation:(NSString *)table mode:(SyncOperationMode)mode;
+(id)syncTableOperation:(NSString *)table mode:(SyncOperationMode)mode isMandatory:(BOOL)isMandatory;
+(id)syncTableOperation:(NSString *)table title:(NSString*)titleVal mode:(SyncOperationMode)mode;
+(id)syncTableOperation:(NSString *)table title:(NSString*)titleVal mode:(SyncOperationMode)mode completedSync:(void (^)(SyncTableOperation* operation, NSInteger affectedRowsCount))aCompletedSyncBlock;
+(id)syncTableOperation:(NSString *)table mode:(SyncOperationMode)mode whereCondition:(NSString *)whereCondition;
+(id)syncTableOperation:(NSString *)table mode:(SyncOperationMode)mode completedSync:(void (^)(SyncTableOperation* operation, NSInteger affectedRowsCount))aCompletedSyncBlock;
+(id)syncTableOperation:(NSString *)table mode:(SyncOperationMode)mode whereCondition:(NSString *)whereCondition conditionParams:(NSDictionary *)conditionParams isNotRequiresTableCount:(BOOL)isNotRequiresTableCount;
@end
