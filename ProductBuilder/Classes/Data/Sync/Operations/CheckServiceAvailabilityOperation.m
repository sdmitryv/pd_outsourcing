//
//  CheckServiceAvailability.m
//  ProductBuilder
//
//  Created by Vitaliy Gervazuk on 10/17/13.
//  Copyright (c) 2013 Cloudworks. All rights reserved.
//

#import "CheckServiceAvailabilityOperation.h"

@implementation CheckServiceAvailabilityOperation

-(void)sync {
    syncManager.defaultTimeout = 5;
    [syncManager checkServiceAvailability];
    [self waitForRequest];
}

@end
