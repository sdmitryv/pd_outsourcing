//
//  CheckDeviceOperation.m
//  DeviceAgent
//
//  Created by Alexander Martyshko on 11/21/13.
//  Copyright (c) 2013 Cloudworks. All rights reserved.
//

#import "CheckDeviceOperation.h"
#import "Device.h"
#import "Settings.h"
#import "Location.h"
#import "DeviceAgentManager.h"
#import <CoreLocation/CoreLocation.h>
#import "AppSettingManager.h"
#import "BPUUID.h"
#import "Location.h"
#import "AppSettingManager.h"
#import "AppParameterManager.h"
#import "Workstation.h"
#import "Setting.h"

NSString* const deviceStateChangedNotification = @"deviceStateChangedNotification";

NSString* const NoSuchDeviceCode = @"NO_SUCH_DEVICE";
NSString* const NoSuchDeviceCodeSuffix = @":NO_SUCH_DEVICE";
NSString* const DeviceDeactivatedCode = @"DEVICE_DEACTIVATED";
NSString* const DeviceDeactivatedCodeSuffix = @":DEVICE_DEACTIVATED";
NSString* const AgentIdMismatchCode = @"AGENT_ID_MISMATCH";
NSString* const AgentIdMismatchCodeSuffix = @":AGENT_ID_MISMATCH";
NSString* const DeviceIdMismatchCode = @"DEVICE_SERVER_ID_MISMATCH";
NSString* const DeviceIdMismatchCodeSuffix = @":DEVICE_SERVER_ID_MISMATCH";
NSString* const NotAuthorizedAppCode = @"NOT_AUTHORIZED_APPLICATION";
NSString* const NotAuthorizedAppCodeSuffix = @":NOT_AUTHORIZED_APPLICATION";


@implementation CheckDeviceOperation

@synthesize device = _device;

- (void)dealloc {
    
    [_device release];
    
    [super dealloc];
}

-(BOOL)shouldCancelOnError:(NSError*)error{
    return YES;
}

- (NSString *)title {
    return NSLocalizedString(@"OPERATION_CHEKING_DEVICE", nil);
}


- (void) sync {
    
    if (AppParameterManager.instance.disableDeviceAgent) {
        
        return;
    }
    
    NSString *devName = AppParameterManager.instance.deviceName;
    if ([@"demo" isEqualToString:[[AppSettingManager instance].defaultServerCode lowercaseString]])
        devName = @"RTW_DEMO_DEVICE";
        
    [syncManager checkDeviceWithName:devName];
    [self waitForRequest];
    
    if (!result) {
        
        for (id bodyPart in syncManager.currentOperation.response.bodyParts) {
            
            if ([bodyPart isKindOfClass:[SOAPFault class]]) {
                
                SOAPFault* soapFault = (SOAPFault *)bodyPart;
                if ([soapFault.faultcode isEqualToString:NoSuchDeviceCode]
                    || [soapFault.faultcode hasSuffix:NoSuchDeviceCodeSuffix]
                    || [soapFault.faultsubcode isEqualToString:NoSuchDeviceCode])
                    [self posStateChangedNotificationWithState:DeviceAgentStateNoSuchDevice];
                else if ([soapFault.faultcode isEqualToString:DeviceDeactivatedCode]
                         || [soapFault.faultcode hasSuffix:DeviceDeactivatedCodeSuffix]
                         || [soapFault.faultsubcode isEqualToString:DeviceDeactivatedCode])
                    [self posStateChangedNotificationWithState:DeviceAgentStateDeactivated];
                else if ([soapFault.faultcode isEqualToString:AgentIdMismatchCode]
                         || [soapFault.faultcode hasSuffix:AgentIdMismatchCodeSuffix]
                         || [soapFault.faultsubcode isEqualToString:AgentIdMismatchCode])
                    [self posStateChangedNotificationWithState:DeviceAgentStateAgentIdMismatch];
                else if ([soapFault.faultcode isEqualToString:DeviceIdMismatchCode]
                         || [soapFault.faultcode hasSuffix:DeviceIdMismatchCodeSuffix]
                         || [soapFault.faultsubcode isEqualToString:DeviceIdMismatchCode])
                    [self posStateChangedNotificationWithState:DeviceAgentStateDeviceIdMismatch];
                else if ([soapFault.faultcode isEqualToString:NotAuthorizedAppCode]
                         || [soapFault.faultcode hasSuffix:NotAuthorizedAppCodeSuffix]
                         || [soapFault.faultsubcode isEqualToString:NotAuthorizedAppCode])
                    [self posStateChangedNotificationWithState:DeviceAgentStateNotAuthorizedApp];
                else if (mode == SyncOperationModeInitialization)
                    [self posStateChangedNotificationWithState:DeviceAgentStateUnknown];
            }
        }
        
        return;
    }


    XmlElement *xmlResult = (XmlElement *)operationResult;
    EGODatabaseRow * databaseRow = [xmlResult childrenToEGODatabaseRow];

    [_device release];
    _device = [[Device alloc] initWithRow:databaseRow];
    
    if (_device.isDeactivated) {
        
        [lastError release];
        lastError = [NSLocalizedString(@"DEVICE_AGENT_DEVICE_WAS_DEACTIVATED_MESSAGE", nil) retain];
        [self posStateChangedNotificationWithState:DeviceAgentStateDeactivated];
        return;
    }


    NSMutableArray * settings = [[[NSMutableArray alloc] init] autorelease];
    NSArray * xmlSettings = [xmlResult childrenWithNameByPath:@"DeviceSettings\\Setting"];
    if (xmlSettings.count > 0) {
        
        for (XmlElement * xmlSetting in xmlSettings) {
            
            databaseRow = [xmlSetting childrenToEGODatabaseRow];
            Setting * setting = [[Setting alloc] initWithRow:databaseRow];
            [settings addObject:setting];
            [setting release];
        }
    }

    
    XmlElement *xmlLocation = [xmlResult childWithName:@"Location"];
    databaseRow = [xmlLocation childrenToEGODatabaseRow];
    Location *initializedLocation = nil;
    if (self.mode == SyncOperationModeInitialization) {
        
        initializedLocation = [[[Location alloc] initWithRow:databaseRow] autorelease];
        
        if (!initializedLocation.id) {
            
            [lastError release];
            lastError = [NSLocalizedString(@"DEVICE_AGENT_LOCATION_ID_MISNATCH_DETAILED_MESSAGE", nil) retain];
            [self posStateChangedNotificationWithState:DeviceAgentStateLocationIdMismatch];
            return;
        }
    }

    
    // update device info on server
    _device.deviceAgentId = [AppSettingManager instance].deviceUniqueId;
    _device.deviceName = devName;
    _device.systemName = AppParameterManager.instance.systemName;
    _device.systemVersion = AppParameterManager.instance.deviceSystemVersion;
    _device.model = AppParameterManager.instance.deviceModel;
    _device.latitude = [DeviceAgentManager sharedInstance].currentLocation.coordinate.latitude;
    _device.longitude = [DeviceAgentManager sharedInstance].currentLocation.coordinate.longitude;
    
    [syncManager updateDevice:[_device dictionaryRepresentation]];
    [self waitForRequest];
    
    
    if (result) {
        
        if (self.mode == SyncOperationModeInitialization) {
            
            if (_device.lastTransactionNo) {
                
                long long dnt = _device.lastTransactionNo.longLongValue;
                [AppSettingManager instance].receiptLastNumber = dnt%1000000;
            }
            
            [AppSettingManager instance].initializedLocation = initializedLocation;
            [AppSettingManager instance].locationId = initializedLocation.id;
        }

        DeviceAgentState priorStatus = [DeviceAgentManager sharedInstance].deviceAgentState;
        
        _device.settings = [NSArray arrayWithArray:settings];
        [DeviceAgentManager sharedInstance].currentDevice = _device;
        
        if (mode == SyncOperationModeSynchronization && priorStatus != [DeviceAgentManager sharedInstance].deviceAgentState)
            [[NSNotificationCenter defaultCenter] postNotificationName:deviceStateChangedNotification object:nil];
    }
    else if (mode == SyncOperationModeInitialization)
        [self posStateChangedNotificationWithState:DeviceAgentStateUnknown];
}


- (void)posStateChangedNotificationWithState:(DeviceAgentState)state {
    
    DeviceAgentState prevState = [DeviceAgentManager sharedInstance].deviceAgentState;
    BOOL activeValueChanged = prevState != state;
    
    [DeviceAgentManager sharedInstance].deviceAgentState = state;
    
    dispatch_async(dispatch_get_main_queue(), ^{
    
        if (state != DeviceAgentStateOk)
            [parentQueue cancelAllOperationsAsync];
        
        if (mode == SyncOperationModeSynchronization && activeValueChanged)
            [[NSNotificationCenter defaultCenter] postNotificationName:deviceStateChangedNotification object:nil];
    });
}

@end
