//
//  ReserveReturnItemsOperation.h
//  ProductBuilder
//
//  Created by valera on 6/26/13.
//  Copyright (c) 2013 Cloudworks. All rights reserved.
//

#import "SyncOperation.h"
#import "BPUUID.h"

@interface ReserveReturnItemsOperation : SyncOperation

@property (nonatomic, retain)NSArray* reserveItems;
@end


@interface ReserveItem : NSObject

@property (nonatomic, retain)BPUUID* originalReceiptItemId;
@property (nonatomic, retain)BPUUID* originalReceiptId;
@property (nonatomic, assign)NSDecimal qtyAvaiable;
@property (nonatomic, assign)NSDecimal qtyReturned;
@property (nonatomic, retain)BPUUID* returnReceiptItemId;
@property (nonatomic, retain)BPUUID* returnReceiptId;

@end
