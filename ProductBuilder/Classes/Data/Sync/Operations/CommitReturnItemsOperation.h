//
//  CommitReturnItemsOperation.h
//  ProductBuilder
//
//  Created by valera on 6/26/13.
//  Copyright (c) 2013 Cloudworks. All rights reserved.
//

#import "SyncOperation.h"

@interface CommitReturnItemsOperation : SyncOperation
@property (nonatomic, retain)NSArray* commitItems;
@end
