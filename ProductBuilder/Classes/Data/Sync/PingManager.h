//
//  PingManager.h
//  ProductBuilder
//
//  Created by Valery Fomenko on 10/7/11.
//  Copyright 2011 Retail Teamwork Ukraine. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WebServiceResponse.h"
#import "SyncManager.h"
//#import "NetReachability.h"
extern NSString* const StatusViewConnectionStateChanged;

@interface PingManager : NSObject<WebServiceOperationDelegate/*, NetReachabilityDelegate*/>{
    BOOL status;
    BOOL firstRun;
    BOOL prevResult;
    NSTimeInterval lastCheckTime;
    dispatch_queue_t queue;
    SyncManager* _syncManager;
    dispatch_semaphore_t networkChangedSemaphore;
    dispatch_semaphore_t waitForRequestSemaphore;
}

+(PingManager *)instance;
-(BOOL)isOnline;
-(void)startPing;
-(void)checkStatus;

@property(nonatomic,readonly) SyncManager* syncManager;

@end
