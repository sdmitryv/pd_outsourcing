//
//  SyncTableHandler.h
//  ProductBuilder
//
//  Created by Valery Fomenko on 6/28/12.
//  Copyright (c) 2012 Cloudworks. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SyncOperationQueue.h"
#import "SyncTableOperation.h"

@interface SyncTableHandler : NSObject
+(BOOL)beforeClearTable:(SyncTableOperation*)operation;
+(BOOL)beforeTruncateTable:(SyncTableOperation*)operation;
+(void)beforeInsertIntoTable:(SyncTableOperation*)operation totalRows:(NSInteger)totalRows;
+(void)afterInsertIntoTable:(SyncTableOperation*)operation totalRows:(NSInteger)totalRows;
@end
