//
//  SyncDefaultOperations.m
//  CloudworksPOS
//
//  Created by valera on 7/21/11.
//  Copyright 2011 Retail Teamwork Ukraine. All rights reserved.
//

#import "SyncDefaultOperations.h"
#import "SyncTableOperation.h"
#import "SyncEndSyncOperation.h"
#import "SyncStartInitOperation.h"
#import "UploadQueryResultOperation.h"
#import "SyncStartOperation.h"
#import "UploadSecurityLogOperation.h"
#import "UploadActionTrackingsOperation.h"
#import "SyncRecordStatusDeletedOperation.h"
#import "UploadExceptionLogOperation.h"
#import "UploadSVSTransactionsOperation.h"
#import "CheckDeviceOperation.h"
#import "UploadGAELogOperation.h"
#import "Workstation.h"
#import "StatusViewController.h"
#import "UpdateDeviceStatsOperation.h"
#import "RealTimeQtyService.h"
#import "AppSettingManager.h"
#import "DownloadSVSSettingsOperation.h"
#import "Product_Builder-Swift.h"
#import "SecurityManager.h"

@implementation SyncDefaultOperations


BOOL needUpdateLocation = NO;
BOOL needUpdateTaxZone = NO;
BOOL needUpdateSettings = NO;
BOOL needUpdateCurrency = NO;
BOOL needUpdateRoleDiscounts = NO;


+(void)resetLocation{
    
    
    [Location resetCache];
    [Workstation resetCache];
    
    if (![NSThread isMainThread])
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [SuperViewController50.instance.statusViewController setStoreName:[Workstation currentWorkstation].description];
           // [[MainViewController50 sharedInstance].statusMessagesViewController updateFillFromLocationView];
        });
    else {
        [SuperViewController50.instance.statusViewController setStoreName:[Workstation currentWorkstation].description];
        //[[MainViewController50 sharedInstance].statusMessagesViewController updateFillFromLocationView];
    }
}



+(NSArray*)defaultDownloadOperations:(SyncOperationMode)mode {

    if ([SyncOperationQueue partialSynchronization]){
        return @[];
    }
    NSAutoreleasePool* pool = [[NSAutoreleasePool alloc]init];

    
    NSArray* arr =  [[NSArray alloc]initWithObjects:
                     [SyncTableOperation syncTableOperation:@"Workstation" mode:mode
                                              completedSync:^(SyncTableOperation *operation, NSInteger affectedRowsCount) {
                                                  
                                                  if (affectedRowsCount) {
                                                      
                                                      needUpdateLocation = YES;
                                                  }
                                                  
                                                  if (![SyncOperationQueue partialSynchronization] && needUpdateLocation) {
                                                      
                                                      needUpdateLocation = NO;
                                                      [SyncDefaultOperations resetLocation];
                                                  }
                                              }],
                    [SyncTableOperation syncTableOperation:@"Location"
                                                      mode:mode
                                             completedSync:^(SyncTableOperation *operation, NSInteger affectedRowsCount) {
                                                 
                                                 if (affectedRowsCount) {
                                                     
                                                     needUpdateLocation = YES;
                                                 }
                                                 
                                                 if (![SyncOperationQueue partialSynchronization] && needUpdateLocation) {
                                                     
                                                     needUpdateLocation = NO;
                                                     [SyncDefaultOperations resetLocation];
                                                 }
                                             }],
                    [SyncTableOperation syncTableOperation:@"InvenItem" mode:mode],
                    [SyncTableOperation syncTableOperation:@"InvenItemSerialNumber" mode:mode],
                    [SyncTableOperation syncTableOperation:@"InvenItemPrice" mode:mode],
                    [SyncTableOperation syncTableOperation:@"InvenItemPriceLocal" mode:mode],
                    [SyncTableOperation syncTableOperation:@"InvenStyle" mode:mode],
                    [SyncTableOperation syncTableOperation:@"InvenStyleExtended" mode:mode],
                    [SyncTableOperation syncTableOperation:@"CustomerPreset" mode:mode],
                    [SyncTableOperation syncTableOperation:@"InvenDepartment" mode:mode],
                    [SyncTableOperation syncTableOperation:@"InvenClass" mode:mode],
                    [SyncTableOperation syncTableOperation:@"InvenDeptSet" mode:mode],
                    [SyncTableOperation syncTableOperation:@"TaxDetail" mode:mode],
                    [SyncTableOperation syncTableOperation:@"TaxJurisdiction" mode:mode],
                    [SyncTableOperation syncTableOperation:@"TaxZoneLine" mode:mode],
                    [SyncTableOperation syncTableOperation:@"TaxCategory" mode:mode],
                    [SyncTableOperation syncTableOperation:@"Employee" mode:mode],
                    [SyncTableOperation syncTableOperation:@"Role"
                                                      mode:mode
                                             completedSync:^(SyncTableOperation *operation, NSInteger affectedRowsCount) {
                                                 
                                                 if (affectedRowsCount && ![SyncOperationQueue partialSynchronization]) {
                                                     
                                                     needUpdateRoleDiscounts = YES;
                                                 }
                                                 
                                                 if (![SyncOperationQueue partialSynchronization] && needUpdateRoleDiscounts) {
                                                     
                                                     needUpdateRoleDiscounts = NO;
                                                     
                                                     if ([SecurityManager currentEmployee]) {
                                                         
                                                         [[SecurityManager currentEmployee] resetDiscountLoadFlag];
                                                     }
                                                 }
                                             }],
                    [SyncTableOperation syncTableOperation:@"RoleEmployee"
                                                      mode:mode],
                    [SyncTableOperation syncTableOperation:@"RoleRight" mode:mode],
                    [SyncTableOperation syncTableOperation:@"Right" mode:mode],
                    [SyncTableOperation syncTableOperation:@"Setting"
                                                       mode:mode
                                             completedSync:^(SyncTableOperation *operation, NSInteger affectedRowsCount) {
                                
                                                 if (affectedRowsCount && ![SyncOperationQueue partialSynchronization]){
                                                     
                                                     needUpdateSettings = YES;
                                                 }
                                                 
                                                 if (![SyncOperationQueue partialSynchronization] && needUpdateSettings){
                                                     
                                                     needUpdateSettings = NO;
                                                     [StoredValuesService reset];
                                                     [RealTimeQtyService reset];
                                                 }
                                             }],
                    [SyncTableOperation syncTableOperation:@"ReturnReason" mode:mode],
                    [SyncTableOperation syncTableOperation:@"PaymentMethod" mode:mode],
                    [SyncTableOperation syncTableOperation:@"PaymentMethodAccount" mode:mode],
                    [SyncTableOperation syncTableOperation:@"DiscountReason" mode:mode],
                    [SyncTableOperation syncTableOperation:@"Country" title:@"Countries" mode:mode],
                    [SyncTableOperation syncTableOperation:@"State" mode:mode],
                    [SyncTableOperation syncTableOperation:@"Title" mode:mode],
                    [SyncTableOperation syncTableOperation:@"PriceLevel" mode:mode],
                    [SyncTableOperation syncTableOperation:@"PostalCode" mode:mode],
                    [SyncTableOperation syncTableOperation:@"ShippingMethod" mode:mode],
                    [SyncTableOperation syncTableOperation:@"CustomFieldsControl" mode:mode],
                    [SyncTableOperation syncTableOperation:@"CustomLookup" mode:mode],
                    [SyncTableOperation syncTableOperation:@"CurrencyExchangeRate" mode:mode],
                    [SyncTableOperation syncTableOperation:@"Currency"
                                                     title:@"Currencie"
                                                      mode:mode
                                             completedSync:^(SyncTableOperation *operation, NSInteger affectedRowsCount) {
                                                 
                                                 if (affectedRowsCount && ![SyncOperationQueue partialSynchronization]) {
                                                     
                                                     needUpdateCurrency = YES;
                                                 }
                                                 
                                                 if (![SyncOperationQueue partialSynchronization] && needUpdateCurrency) {
                                                     
                                                     needUpdateCurrency = NO;
                                                     [Currency resetCache];
                                                 }
                                             }],
                    [SyncTableOperation syncTableOperation:@"InvenStyleVendor" mode:mode],
                    [SyncTableOperation syncTableOperation:@"InvenStyleVendorItem" mode:mode],
                    [SyncTableOperation syncTableOperation:@"InvenItemIdentifier" mode:mode],
                    [SyncTableOperation syncTableOperation:@"CustomerRequiredField" mode:mode],
                    [SyncTableOperation syncTableOperation:@"CustomRequiredField" mode:mode],
                    [SyncTableOperation syncTableOperation:@"PosQuery" title:@"PosQueries" mode:mode],
                    [SyncTableOperation syncTableOperation:@"Certificate" mode:mode],
                    [SyncTableOperation syncTableOperation:@"ItemLabel" mode:mode],
                    [SyncTableOperation syncTableOperation:@"MobilePrintingDesign" mode:mode],
                    [SyncTableOperation syncTableOperation:@"MobilePrintingDesignArea" mode:mode],
                    [SyncTableOperation syncTableOperation:@"SalesReceiptPresetNote" mode:mode],
                    [SyncTableOperation syncTableOperation:@"TokenPrograms" title:@"TokenPrograms" mode:mode],
                    [SyncTableOperation syncTableOperation:@"MembershipLevel" mode:mode],
                    [SyncTableOperation syncTableOperation:@"LandedCost" mode:mode],
                    [SyncTableOperation syncTableOperation:@"AttributeSet" mode:mode],
                    [SyncTableOperation syncTableOperation:@"AttributeSetValue" mode:mode],
                    [SyncTableOperation syncTableOperation:@"CalendarStatus" mode:mode],
                    [SyncTableOperation syncTableOperation:@"DeviceReceiptCustomerField" mode:mode],
                    [SyncTableOperation syncTableOperation:@"FrequentBuyerProgram" mode:mode],
                    [SyncTableOperation syncTableOperation:@"MembershipLevelFrequentBuyerProgram" mode:mode],
                    [SyncTableOperation syncTableOperation:@"LocationAvailabilityGroup" mode:mode],
                    [SyncTableOperation syncTableOperation:@"LocationAvailabilityGroup_Location" mode:mode],
                    [SyncTableOperation syncTableOperation:@"CustomerStatus" mode:mode],
                    [SyncTableOperation syncTableOperation:@"InvenPreSetConfiguration" mode:mode],
                    [SyncTableOperation syncTableOperation:@"InvenPreSetCategory" mode:mode],
                    [SyncTableOperation syncTableOperation:@"InvenPreSetCategoryItem" mode:mode],
                    [SyncTableOperation syncTableOperation:@"DeliveryOrderTypeRange" mode:mode],
                    [SyncTableOperation syncTableOperation:@"SaleCreditLocation" mode:mode],
                    [SyncTableOperation syncTableOperation:@"SellFromLocation" mode:mode],
                    [SyncTableOperation syncTableOperation:@"SalesOrderType" mode:mode],
                    [SyncTableOperation syncTableOperation:@"TimeZone" mode:mode],
                    [SyncTableOperation syncTableOperation:@"ShipToAddress" mode:mode],
                    [SyncTableOperation syncTableOperation:@"AddressType" mode:mode],
                    [SyncTableOperation syncTableOperation:@"DirectPrinter" mode:mode],
                    [SyncTableOperation syncTableOperation:@"PrintDesignSetting" mode:mode],
                    [SyncTableOperation syncTableOperation:@"PrinterModel" mode:mode],
                    [SyncTableOperation syncTableOperation:@"PrinterModelPaper" mode:mode],
                    [SyncTableOperation syncTableOperation:@"PrinterModelOption" mode:mode],
                    [SyncTableOperation syncTableOperation:@"DirectPrinterOption" mode:mode],
                    [SyncTableOperation syncTableOperation:@"CustomerSession" mode:mode],
                    [SyncTableOperation syncTableOperation:@"SalesReturnRequiredField" mode:mode],
                    [SyncTableOperation syncTableOperation:@"TaxExemptReason" mode:mode],
                    [SyncTableOperation syncTableOperation:@"ECommerceChannel" mode:mode],
                    [SyncTableOperation syncTableOperation:@"ECommerceCategory" mode:mode],
                    [SyncTableOperation syncTableOperation:@"InvenItemEComCategory" mode:mode],
                    [SyncTableOperation syncTableOperation:@"InvenStyleEComCategory" mode:mode],
                    [SyncTableOperation syncTableOperation:@"InvenStyleRelated" mode:mode],
                    [SyncTableOperation syncTableOperation:@"InvenStyleRelationType" mode:mode],
                    [SyncTableOperation syncTableOperation:@"InvenStylePrice" mode:mode],
                    [SyncTableOperation syncTableOperation:@"PriceChangeReason" mode:mode],
                    [SyncRecordStatusDeletedOperation syncTableOperation:@"SyncRecordStatusDeleted" mode:mode beforeRowsWillDelete:^(NSString *tableName) {
                    }],
                    [SyncTableOperation syncTableOperation:@"InvenSeason" mode:mode],
                    [SyncTableOperation syncTableOperation:@"EmployeeNotification" mode:mode],
                    [SyncTableOperation syncTableOperation:@"Vendor" mode:mode],
                    [SyncTableOperation syncTableOperation:@"InvenBrand" mode:mode],
                    [SyncTableOperation syncTableOperation:@"RejectShipmentReason" mode:mode],
                    [SyncTableOperation syncTableOperation:@"PinPad" mode:mode],
                    [SyncTableOperation syncTableOperation:@"CustomUILayout" mode:mode],
                    [SyncTableOperation syncTableOperation:@"HiddenEntity" mode:mode],
                    [SyncTableOperation syncTableOperation:@"ServiceFee" mode:mode],
                    [SyncTableOperation syncTableOperation:@"ServiceFeeRange" mode:mode],
                    [SyncTableOperation syncTableOperation:@"ICMMediaContent" mode:mode],
                    [SyncTableOperation syncTableOperation:@"RcmIndex" mode:mode],
                    [SyncTableOperation syncTableOperation:@"RcmIndexGroup" mode:mode],
                    [SyncTableOperation syncTableOperation:@"Company" mode:mode],
                    [SyncTableOperation syncTableOperation:@"CurrencyDenomination" mode:mode],
                    [DownloadSVSSettingsOperation syncOperationWithMode:mode],
                    [SyncEndSyncOperation syncOperationWithMode:mode],
                    
    nil];
    NSMutableArray* ret =  [[NSMutableArray alloc ]initWithArray:arr];
    [arr release];
    if (mode == SyncOperationModeInitialization) {
        
        [ret insertObject:[SyncStartInitOperation syncOperationWithMode:mode] atIndex:0];
        [ret insertObject:[SyncStartOperation syncOperationWithMode:mode] atIndex:0];
    }
    else
        [ret addObject:[UpdateDeviceStatsOperation secondSyncOperation]];
    
    [pool release];
    return [ret autorelease];
}

+(NSArray*)defaultUploadOperations{
    
    if ([SyncOperationQueue partialSynchronization]){
        return @[[SyncStartOperation syncOperationWithMode:SyncOperationModeSynchronization],
                 [CheckDeviceOperation syncOperationWithMode:SyncOperationModeSynchronization],
                 [UpdateDeviceStatsOperation syncOperation],
                 [UploadSVSTransactionsOperation syncOperation],
                 [UploadExceptionLogOperation syncOperation],
                 [UploadGAELogOperation syncOperation],
                 [UpdateDeviceStatsOperation secondSyncOperation]];
    }
    return @[[SyncStartOperation syncOperationWithMode:SyncOperationModeSynchronization],
             [CheckDeviceOperation syncOperationWithMode:SyncOperationModeSynchronization],
             [UpdateDeviceStatsOperation syncOperation],
             [UploadQueryResultOperation syncOperation],
             [UploadSecurityLogOperation syncOperation],
             [UploadActionTrackingsOperation syncOperation],
             [UploadSVSTransactionsOperation syncOperation],
             [UploadExceptionLogOperation syncOperation],
             [UploadGAELogOperation syncOperation]];
}

@end

