//
//  MyClass.h
//  CloudworksPOS
//
//  Created by valera on 7/21/11.
//  Copyright 2011 Retail Teamwork Ukraine. All rights reserved.
//

#import <Foundation/Foundation.h>


typedef NS_ENUM(NSUInteger, SyncOperationMode) { SyncOperationModeSynchronization = 0, SyncOperationModeInitialization = 1 };
@class SyncOperation;

extern NSString* const SyncQueueDidErrorOccurNotification;
extern NSString* const SyncQueueDidStartOperationNotification;
extern NSString* const SyncQueueDidEndOperationNotification;
extern NSString* const SyncQueueDidChangeOperationTitleNotification;
extern NSString* const SyncQueueDidCompleteNotification;
//extern NSString* const SyncQueueDidCancelNotification;
extern NSString* const SyncQueueInProgressNotification;
extern NSString* const SyncQueueDidUpdateRecordsOperationNotification;
extern NSString* const SyncQueueErrorKey;
extern NSString* const SyncQueueErrorDescriptionKey;
extern NSString* const SyncQueueCurrentOperationKey;
extern NSString* const SyncQueueOperationObject;
extern NSString* const SyncQueueSyncLocationMistmatch;
extern NSString* const SyncQueueCurrentLocationKey;
extern NSString* const SyncQueueOpsLocationKey;
extern NSString* const SyncQueueIsCanceledKey;
extern NSString* const SyncQueueBackupOperationStateChanged;

@interface SyncOperationQueue : NSOperationQueue{
    BOOL breakOnError;
}
+(instancetype)sharedInstance;
-(void)cancelAllOperationsAsync;
-(void)addOperations:(NSArray *)ops waitUntilFinished:(BOOL)wait breakOnError:(BOOL)error;
-(void)postNotification:(NSString*)name;
-(void)postNotification:(NSString*)name userInfo:(id)userInfo;
-(void)postNotification:(NSString*)name userInfo:(id)userInfo sender:(SyncOperation*)syncOperation;

-(void)startDefaultInit;
-(void)startDefaultSync;
-(void)startDefaultSync:(BOOL)waitUntilFinished;
+(BOOL)partialSynchronization;
-(NSTimeInterval)lastTotalRuntime;
@end
