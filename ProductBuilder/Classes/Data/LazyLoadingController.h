//
//  LazyLoadingController.h
//  ProductBuilder
//
//  Created by Valera on 9/17/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ILazyLoadableList.h"
#import "SqlCommand.h"
#import "NSIntegerArray.h"

@interface LazyLoadingController : NSObject<NSFastEnumeration> {
	NSObject<ILazyLoadableList>* _obj;
	BOOL _prepared;
    NSIntegerArray *_listIds;
	NSMutableDictionary *_listValues;
	NSMutableArray *_listValuesIds;
}

-(id)initWithLazyLoadable:(NSObject<ILazyLoadableList>*) obj;
-(id)objectAtIndex:(NSUInteger)index;
-(void)replaceObject:(id)obj atIndex:(NSUInteger)index;
-(id)objectByRowid:(NSNumber *)rowid;
-(NSUInteger)indexOfObject:(NSObject<ILazyItem>*) anObject;
-(id)refreshObject:(NSObject<ILazyItem>*) anObject;
-(NSUInteger) count;
-(void) reset;
-(void) resetData;
-(BOOL) read:(NSUInteger)limit offset:(NSUInteger)offset;// use it to override, not to call
@property (nonatomic,readonly)BOOL prepared;
@property (nonatomic,readonly)NSIntegerArray * listIds;
extern NSUInteger const BUFFERSIZE;
extern NSUInteger const WINDOWSIZE;
@end
