//
//  ApppleTVmanager.m
//  ProductBuilder
//
//  Created by User on 5/7/12.
//  Copyright (c) 2012 Cloudworks. All rights reserved.
//

#import "ApppleTVmanager.h"
#import "CWMovieViewController.h"

static ApppleTVmanager *apppleTVmanagerInstance = nil;
@implementation ApppleTVmanager

@synthesize secondaryWindow, isDisplayed;

+(ApppleTVmanager *)sharedInstance {
    @synchronized(self){
        if (!apppleTVmanagerInstance) {
            apppleTVmanagerInstance = [[ApppleTVmanager alloc] init];
            [apppleTVmanagerInstance initScreens];
            [apppleTVmanagerInstance setIsDisplayed:NO];
        }
    }
	return apppleTVmanagerInstance;
}
-(void)setImage:(UIImage*)image{
    if (currentMovieController) {
        currentMovieController.appleTVActive = NO;
        [currentMovieController release];
        currentMovieController = nil;
    }
    
    CATransition *animation = [CATransition animation];
	[animation setDuration:0.3f];
	[animation setType:kCATransitionFade];
    [animation setValue:@"ImageView" forKey:@"showView"];
//    secondController.imageView.image=image;
//    [secondController.view bringSubviewToFront:secondController.imageView];
//	[[secondController.view layer] addAnimation:animation forKey:@"showView"];
}

-(void)setDefaultImage{    
    [self setImage:[UIImage imageNamed:@"Default-Landscape.png"]];
}
- (void)setMovieController:(CWMovieViewController *)movieController {
    if (currentMovieController) {
        [currentMovieController release];
        currentMovieController = nil;
    }
    
    currentMovieController = [movieController retain];
    currentMovieController.appleTVActive = YES;
//    currentMovieController.mPlaybackView.frame = CGRectMake(0, 0, secondController.view.frame.size.width, secondController.view.frame.size.height);
    [currentMovieController.mPlaybackView removeFromSuperview];
//    [secondController.view addSubview:currentMovieController.mPlaybackView];
//    [secondController.view bringSubviewToFront:currentMovieController.mPlaybackView];
}
- (void)initScreens{
    if ([[UIScreen screens] count] > 1)
    {
        UIScreen *screen = [[UIScreen screens] lastObject];
        
        if (!self.secondaryWindow)
        {
            self.secondaryWindow = [[[UIWindow alloc] initWithFrame:screen.bounds] autorelease];
            self.secondaryWindow.screen = screen;
            self.secondaryWindow.hidden = NO;
        }
        
        /*if (!self.secondController) {
            self.secondController = [[[SecondScreenViewController alloc] init] autorelease];
            CGRect rect = CGRectMake(0, 0, screen.bounds.size.width, screen.bounds.size.height);
            self.secondController.view.frame = rect;
            self.secondController.imageView.frame = rect;
            [self setDefaultImage];
            
        }*/
        
        //self.secondaryWindow.rootViewController = self.secondController;
    }
}

-(void)changeDisplayButtonState{
    if (isDisplayed==YES)
        [self setIsDisplayed:NO];
    else
       [self setIsDisplayed:YES];
}
    
-(void)dealloc{
    
    [secondaryWindow release];
    [currentMovieController release];
    [super dealloc];
}

@end
