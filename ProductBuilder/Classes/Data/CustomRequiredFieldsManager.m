//
//  CustomRequiredFieldsManager.m
//  ProductBuilder
//
//  Created by Viacheslav Lulakov on 11/16/16.
//  Copyright © 2016 Cloudworks. All rights reserved.
//

#import "CustomRequiredFieldsManager.h"

@interface CustomRequiredFieldsManager() {
    CustomRequiredFieldArea _area;
    NSMutableDictionary * _requiredFields;
    NSMutableDictionary * _requiredFieldsGroups;
    NSArray * _companyRequiredFields;
    NSArray * _personRequiredFields;
}

@property (nonatomic, readonly) NSMutableDictionary * requiredFields;

@end

@implementation CustomRequiredFieldsManager

#pragma mark - Life Cycle

- (id)initForArea:(CustomRequiredFieldArea)area delegate:(id<CustomRequiredFieldsManagerDelegate>) delegate {
    self = [super init];
    if (self) {
        _area = area;
        _delegate = delegate;
        _requiredFields = [[NSMutableDictionary alloc] init];
        _requiredFieldsGroups = [[NSMutableDictionary alloc] init];
        [self loadFieldsForArea:area];
    }
    return self;
}

- (void)dealloc {
    [_requiredFields release];
    [_requiredFieldsGroups release];
    [_companyRequiredFields release];
    [_personRequiredFields release];
    
    [super dealloc];
}

#pragma mark - Private Methods

- (void)loadFieldsForArea:(CustomRequiredFieldArea)area {
    NSArray *requiredFields = [CustomRequiredField getRequiredFieldsListForArea:area];
    if (requiredFields.count) {
        for (CustomRequiredField * field in requiredFields) {
            NSString * fieldName = [field.fieldName lowercaseString];
            NSString * reqSymbol = field.requireSymbol;
            if (![reqSymbol isEqualToString:@"1"]) {
                NSMutableSet * group = [_requiredFieldsGroups objectForKey:reqSymbol];
                if (group == nil) {
                    group = [[[NSMutableSet alloc] init] autorelease];
                    [_requiredFieldsGroups setObject:group forKey:reqSymbol];
                }
                [group addObject:fieldName];
            }
            [_requiredFields setObject:field forKey:fieldName];
        }
    }
    
    if (area == CustomRequiredFieldAreaCustomer) {
        CustomRequiredField * companyRequiredField = [[CustomRequiredField alloc] init];
        companyRequiredField.fieldName = @"organization";
        companyRequiredField.requireSymbol = @"1";
        _companyRequiredFields = [[NSArray alloc] initWithObjects:companyRequiredField, nil];
        [companyRequiredField release];
        CustomRequiredField * firstNameRequiredField = [[CustomRequiredField alloc] init];
        firstNameRequiredField.fieldName = @"firstName";
        firstNameRequiredField.requireSymbol = @"1";
        CustomRequiredField * lastNameRequiredField = [[CustomRequiredField alloc] init];
        lastNameRequiredField.fieldName = @"lastName";
        lastNameRequiredField.requireSymbol = @"1";
        _personRequiredFields = [[NSArray alloc] initWithObjects:firstNameRequiredField, lastNameRequiredField, nil];
        [firstNameRequiredField release];
        [lastNameRequiredField release];
    }
    else if(area == CustomRequiredFieldAreaShipToAddress) {
        CustomRequiredField * typeRequiredField = [[CustomRequiredField alloc] init];
        typeRequiredField.fieldName = @"addresstype";
        typeRequiredField.requireSymbol = @"1";
        [_requiredFields setObject:typeRequiredField forKey:@"addresstype"];
        [typeRequiredField release];
    }
}

#pragma mark - Public Methods

- (NSString *)checkRequiredFields:(NSArray *)fieldNames {
    NSString * requiredFieldName = nil; // name of invalid field
    
    // Find empty fields and not empty groups
    
    NSMutableArray * emptyFields = [[NSMutableArray alloc] init];
    NSMutableSet * notEmptyGroups = [[NSMutableSet alloc] init];
    NSMutableDictionary * requiredFields = [self requiredFields];
    for (NSString * fieldName in fieldNames) {
        CustomRequiredField * field = [requiredFields objectForKey:[fieldName lowercaseString]];
        NSString * symbol = field.requireSymbol;
        if (symbol.length > 0) {
            if ([_delegate isEmptyField:fieldName]) {
                [emptyFields addObject:field];
            }
            else {
                [notEmptyGroups addObject:symbol];
            }
        }
    }
    
    // Check required fields
    
    for (CustomRequiredField * field in emptyFields) {
        NSString * symbol = field.requireSymbol;
        if ([symbol isEqualToString:@"1"] || ![notEmptyGroups containsObject:symbol]) {
            requiredFieldName = field.fieldName;
            break;
        }
    }
    
    [emptyFields release];
    [notEmptyGroups release];
    
    return [requiredFieldName lowercaseString];
}

- (void)markRequiredFields:(NSArray *)fieldNames {
    NSMutableDictionary * counters = [[NSMutableDictionary alloc] init];
    NSMutableDictionary * requiredFields = [self requiredFields];
    for (NSString * fieldName in fieldNames) {
        CustomRequiredField * field = [requiredFields objectForKey:[fieldName lowercaseString]];
        NSString * requiredSymbol = field.requireSymbol;
        if (requiredSymbol.length > 0 && ![requiredSymbol isEqualToString:@"1"]) {
            NSNumber * counter = [counters valueForKey:requiredSymbol];
            if (counter == nil) {
                [counters setValue:[NSNumber numberWithInteger:1] forKey:requiredSymbol];
            }
            else {
                [counters setValue:[NSNumber numberWithInteger:counter.integerValue + 1] forKey:requiredSymbol];
            }
        }
    }
    
    for (NSString * fieldName in fieldNames) {
        CustomRequiredField * field = [requiredFields objectForKey:[fieldName lowercaseString]];
        NSString * requiredSymbol = field.requireSymbol;
        if (requiredSymbol.length > 0 && ![requiredSymbol isEqualToString:@"1"]) {
            if ([[counters objectForKey:requiredSymbol] integerValue] == 1) {
                requiredSymbol = nil;
            }
        }
        if ([requiredSymbol  isEqualToString:@"1"]) {
            requiredSymbol = @"*";
        }
        [_delegate markField:fieldName withSymbol:requiredSymbol];
    }
    [counters release];
}

- (NSArray *)requiredFieldsArray {
    return [self.requiredFields allValues];
}

#pragma mark - Properties

- (NSMutableDictionary *) requiredFields {
    if (_area != CustomRequiredFieldAreaCustomer) {
        return _requiredFields;
    }
    NSMutableDictionary * requiredFields = [_requiredFields mutableCopy];
    NSArray * extendedFields = nil;
    if (_delegate != nil && [_delegate respondsToSelector:@selector(isCompany)]) {
        extendedFields = [_delegate isCompany] ? _companyRequiredFields : _personRequiredFields;
    }
    
    for (CustomRequiredField * field in extendedFields) {
        [requiredFields setObject:field forKey:[field.fieldName lowercaseString]];
    }
    
    return [requiredFields autorelease];
}

@end
