//
//  EGOStatement.h
//  CloudworksPOS
//
//  Created by valera on 7/8/11.
//  Copyright 2011 Retail Teamwork Ukraine. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <sqlite3.h>

@class EGODatabase;

@interface EGOStatement : NSObject {
    EGODatabase* database;
    sqlite3_stmt *statement;
    NSString *query;
    long useCount;
}

- (id)initWithDatabase:(EGODatabase*) aDatabase stmt:(NSString*)sql;
- (BOOL) close;
- (BOOL) reset;
- (SQLITE_API int)execute;
- (sqlite3_stmt *)statement;
- (void)setStatement:(sqlite3_stmt *)value;

- (NSString *)query;
- (BOOL)isValid;
- (long)useCount;
- (void)setUseCount:(long)value;
- (BOOL)bind:(NSObject*)parametersList;
- (BOOL)bindToNamedParameters:(NSDictionary*)parameters;
- (BOOL)bindToParameters:(NSArray*)parameters;
@end
