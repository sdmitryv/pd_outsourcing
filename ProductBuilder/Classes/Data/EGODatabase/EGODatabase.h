//
//  EGODatabase.h
//  EGODatabase
//
//  Created by Shaun Harrison on 3/6/09.
//  Copyright (c) 2009 enormego
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//

#import <Foundation/Foundation.h>
#import "EGODatabaseRequest.h"
#import "EGODatabaseResult.h"
#import "EGODatabaseRow.h"
#import <sqlite3.h>
#import "NSIntegerArray.h"
#import <libkern/OSAtomic.h>
#import "os/lock.h"

//#define EGODatabaseRunQueryInBackgoundForMainThread
#define EGODatabaseDebugLog 1

#if EGODatabaseDebugLog
#define EGODBDebugLog(s,...) NSLog(s, ##__VA_ARGS__)
#else
#define EGODBDebugLog(s,...)
#endif

#ifdef EGODatabaseRunQueryInBackgoundForMainThread
extern NSString* const EGODatabaseStartExecutingMainThreadNotification;
extern NSString* const EGODatabaseEndExecutingMainThreadNotification;
#endif
extern NSString* const EGODatabaseStartLockNotification;
extern NSString* const EGODatabaseEndLockNotification;

@interface EGODatabase : NSObject {
@protected
	NSString* databasePath;
	//NSLock* executeLock;
	BOOL inTransaction;
    BOOL        shouldCacheStatements;
    NSMutableDictionary *cachedStatements;
	
@private
    //OSSpinLock spinlock;
    os_unfair_lock _lock;
	sqlite3* _handle;
    BOOL _inTransaction;
    BOOL _shuttingDown;
    BOOL _isOpened;
}

+ (id)databaseWithPath:(NSString*)aPath;
- (id)initWithPath:(NSString*)aPath;

- (BOOL)open;
- (void)close;
- (void)wal_checkpoint;

- (BOOL)execute:(NSString*)sql;
- (void)interrupt;

//cache
- (void)clearCachedStatements;
- (BOOL)shouldCacheStatements;
- (void)setShouldCacheStatements:(BOOL)value;

// Execute Updates
- (BOOL)executeUpdateWithParameters:(NSString*)sql, ... NS_REQUIRES_NIL_TERMINATION;
- (BOOL)executeUpdate:(NSString*)sql;
- (BOOL)executeUpdate:(NSString*)sql parameters:(NSArray*)parameters;
- (BOOL)executeUpdate:(NSString*)sql namedParameters:(NSDictionary*)parameters;

// Execute Query
- (EGODatabaseResult*)executeQueryWithParameters:(NSString*)sql, ... NS_REQUIRES_NIL_TERMINATION;
- (EGODatabaseResult*)executeQuery:(NSString*)sql;
- (EGODatabaseResult*)executeQuery:(NSString*)sql parameters:(NSArray*)parameters;
- (EGODatabaseResult*)executeQuery:(NSString*)sql namedParameters:(NSDictionary*)namedParameters;
//- (NSArray*)executeQueryFastArray:(NSString*)sql parametersList:(id)parametersList;
- (NSIntegerArray*)executeQueryFastArray:(NSString*)sql parametersList:(id)parametersList;
// Query request operation

- (EGODatabaseRequest*)requestWithQueryAndParameters:(NSString*)sql, ... NS_REQUIRES_NIL_TERMINATION;
- (EGODatabaseRequest*)requestWithQuery:(NSString*)sql;
- (EGODatabaseRequest*)requestWithQuery:(NSString*)sql parameters:(NSArray*)parameters;

- (NSInteger)changedRowsCount;

// Update request operation

- (EGODatabaseRequest*)requestWithUpdateAndParameters:(NSString*)sql, ... NS_REQUIRES_NIL_TERMINATION;
- (EGODatabaseRequest*)requestWithUpdate:(NSString*)sql;
- (EGODatabaseRequest*)requestWithUpdate:(NSString*)sql parameters:(NSArray*)parameters;

//transactions
- (BOOL)inTransaction;
- (BOOL) rollback;
- (BOOL) commit;
- (BOOL) beginDeferredTransaction;
- (BOOL) beginTransaction;


// Error methods

- (NSString*)lastErrorMessage;
- (BOOL)hadError;
- (int)lastErrorCode;
- (NSInteger)lastInsertRowid;
-(BOOL)alive;
@property(nonatomic,readonly) sqlite3* handle;
@property(nonatomic, readonly) BOOL isShuttingDown;
@end
