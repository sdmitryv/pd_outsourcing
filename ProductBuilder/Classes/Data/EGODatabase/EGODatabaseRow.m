//
//  EGODatabaseRow.m
//  EGODatabase
//
//  Created by Shaun Harrison on 3/6/09.
//  Copyright (c) 2009 enormego
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//

#import "EGODatabaseRow.h"
#import "EGODatabaseResult.h"
#import "DecimalHelper.h"
#import "NSDate+ISO8601Parsing.h"
#import "NSDictionary+CaseInsensitive.h"

@implementation EGODatabaseRow
@synthesize columnData;

- (id)initWithDatabaseResult:(EGODatabaseResult*)aResult {
	if((self = [super init])) {
		columnData = [[NSMutableArray alloc] init];
		result = aResult;
	}
	return self;
}

- (NSUInteger)columnIndexForName:(NSString*)columnName {
    return [result columnIndexForName:columnName];
}

-(id)objectForColumnIndex:(NSUInteger)index{
    return columnData[index];
}

-(id)objectForColumn:(NSString*)columnName{
    NSUInteger index = [self columnIndexForName:columnName];
	if(index == NSNotFound) return nil;
    return [self objectForColumnIndex:index];
}

-(int)intForValue:(id)value{
    return ISNULL(value) ? 0 : [value intValue];
}

-(long)longForValue:(id)value{
    return ISNULL(value) ? 0 : [value longValue];
}

-(BOOL)boolForValue:(id)value{
    if (ISNULL(value)) {
        return NO;
    }
    if ([value isKindOfClass:[NSString class]]){
        NSString *stringValue = [value lowercaseString];
        if ([stringValue isEqualToString:@"true"]) {
            return YES;
        }
        else if ([stringValue isEqualToString:@"false"]) {
            return NO;
        }
    }
	return [value intValue] != 0;
}

-(double)doubleForValue:(id)value{
    return ISNULL(value) ? 0 : [value doubleValue];
}

-(NSDecimal)decimalForValue:(id)value{
    if(ISNULL(value)) return D0;
    if ([value isKindOfClass:[NSNumber class]]){
        return [value decimalValue];
    }
    else if ([value isKindOfClass:[NSString class]]){
        NSScanner* scanner = [NSScanner scannerWithString:value];
        NSDecimal d;
        BOOL res = [scanner scanDecimal:&d];
        return res ? d : D0;
    }
    return D0;
}

-(NSDecimalNumber*)decimalNumberForValue:(id)value{
    if(ISNULL(value)) return nil;
    if ([value isKindOfClass:[NSDecimalNumber class]]){
        return value;
    }
    else if ([value isKindOfClass:[NSNumber class]]){
        return [NSDecimalNumber decimalNumberWithDecimal:[value decimalValue]];
    }
    else if ([value isKindOfClass:[NSString class]]){
        NSScanner* scanner = [NSScanner scannerWithString:value];
        NSDecimal d;
        BOOL res = [scanner scanDecimal:&d];
        return res ? [NSDecimalNumber decimalNumberWithDecimal:d] : nil;
    }
    return nil;
}

-(NSNumber*)numberForValue:(id)value{
    if (ISNULL(value)) return nil;
    if ([value isKindOfClass:[NSNumber class]]){
        return value;
    }
    else if ([value isKindOfClass:[NSString class]]){
        return @([value doubleValue]);
    }
    return nil;
}

-(NSData*)dataForValue:(id)value{
    if (ISNULL(value)) return nil;
    if ([value isKindOfClass:[NSData class]]){
        return value;
    }
    else if ([value isKindOfClass:[NSString class]]){
        return [value dataUsingEncoding:NSUTF8StringEncoding];
    }
    return nil;
}

-(NSDate*)dateForValue:(id)value{
    if (ISNULL_OR_EMPTY(value)) return nil;
    if ([value isKindOfClass:[NSString class]] || [value isKindOfClass:[NSNumber class]]){
        if ([value isKindOfClass:[NSString class]]){
            if ([value rangeOfString:@"-"].location!=NSNotFound){
                NSTimeInterval timeInterval;
                if ([NSDate parseISO8601String:value inTimeIntervalSince1970:&timeInterval]){
                    return [NSDate dateWithTimeIntervalSince1970:timeInterval];
                }
            }
        }
        return [NSDate dateWithTimeIntervalSince1970:[value doubleValue]];
    }
    return nil;
}

-(NSString*)stringForValue:(id)value{
    return ISNULL(value) ? nil : [value isKindOfClass:[NSString class]] ? value : [value description];
}

-(BPUUID*)UuidForValue:(id)value{
    return ISNULL_OR_EMPTY(value) ? nil : [BPUUID UUIDWithString:[value description]];
}

- (int)intForColumn:(NSString*)columnName {
    return [self intForValue:[self objectForColumn:columnName]];
}

- (int)intForColumnIndex:(NSInteger)index{
	return [self intForValue:[self objectForColumnIndex:index]];
}

- (long)longForColumn:(NSString*)columnName {
    return [self longForValue:[self objectForColumn:columnName]];
}

- (long)longForColumnIndex:(NSInteger)index{
	return [self longForValue:[self objectForColumnIndex:index]];
}

- (BOOL)boolForColumn:(NSString*)columnName {
   return [self boolForValue:[self objectForColumn:columnName]];
}

- (BOOL)boolForColumnIndex:(NSInteger)index{
	return [self boolForValue:[self objectForColumnIndex:index]];
}

- (double)doubleForColumn:(NSString*)columnName {
    return [self doubleForValue:[self objectForColumn:columnName]];
}

- (double)doubleForColumnIndex:(NSInteger)index{
	return [self doubleForValue:[self objectForColumnIndex:index]];
}

- (NSDecimal)decimalForColumn:(NSString*)columnName {
    return [self decimalForValue:[self objectForColumn:columnName]];
}

- (NSDecimal)decimalForColumnIndex:(NSInteger)index{
    return [self decimalForValue:[self objectForColumnIndex:index]];
}

- (NSDecimalNumber*)decimalNumberForColumn:(NSString*)columnName{
    return [self decimalNumberForValue:[self objectForColumn:columnName]];
}

- (NSDecimalNumber*)decimalNumberForColumnIndex:(NSInteger)index{
     return [self decimalNumberForValue:[self objectForColumnIndex:index]];
}

- (NSString*)stringForColumn:(NSString*)columnName {
    return [self stringForValue:[self objectForColumn:columnName]];
}

- (NSString*)stringForColumnIndex:(NSInteger)index{
	return [self stringForValue:[self objectForColumnIndex:index]];
}

- (NSDate*)dateForColumn:(NSString*)columnName {
    return [self dateForValue:[self objectForColumn:columnName]];
}

- (NSDate*)dateForColumnIndex:(NSInteger)index{
	return [self dateForValue:[self objectForColumnIndex:index]];
}

- (NSData*)dataForColumn:(NSString*)columnName {
   return [self dataForValue:[self objectForColumn:columnName]];
}

- (NSData*)dataForColumnIndex:(NSInteger)index {
	return [self dataForValue:[self objectForColumnIndex:index]];
}

- (NSNumber*)numberForColumn:(NSString*)columnName {
    return [self numberForValue:[self objectForColumn:columnName]];
}

- (NSNumber*)numberForColumnIndex:(NSInteger)index {
	return [self numberForValue:[self objectForColumnIndex:index]];
}

- (BPUUID*)UuidForColumn:(NSString*)columnName {
    return [self UuidForValue:[self objectForColumn:columnName]];
}

- (BPUUID*)UuidForColumnIndex:(NSInteger)index {
	return [self UuidForValue:[self objectForColumnIndex:index]];
}

- (void)dealloc {
	[columnData release];
	[super dealloc];
}

-(NSString*)description{
    NSMutableString* descr = [NSMutableString string];
    for (NSInteger i = 0; i < result.columnNames.count; i++){
        id value = columnData[i];
        [descr appendFormat:@"%@ = %@\n", (result.columnNames)[i], value];
    }
    return descr;
}

@end

@implementation EGODatabaseRowDictionary

-(id)initWithDictionary:(NSDictionary *)aDictionary{
    if (self = [super initWithDatabaseResult:nil]){
        dictionary = [[NSDictionary alloc] initWithDictionaryCaseInsensitive:aDictionary];
    }
    return self;
}

-(id)objectForColumn:(NSString*)columnName{
    return dictionary[columnName];
}

-(NSDate*)dateForValue:(id)value{
    return ISNULL_OR_EMPTY(value) ? nil : [NSDate dateWithTimeIntervalSince1970:[value doubleValue]/1000.0f];
}

-(void)dealloc{
    [dictionary release];
    [super dealloc];
}

@end
