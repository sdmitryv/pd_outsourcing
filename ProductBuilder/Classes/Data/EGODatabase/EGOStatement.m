//
//  EGOStatement.m
//  CloudworksPOS
//
//  Created by valera on 7/8/11.
//  Copyright 2011 Retail Teamwork Ukraine. All rights reserved.
//

#import "EGOStatement.h"
#import "EGODatabase.h"
#import "RTProtoDataReader.h"
#import <objc/runtime.h>
#import "DecimalHelper.h"

static inline void sqlite_desturctor(void* obj){
    if (obj)
        free(obj);
}

//static inline BOOL isSubclassOfClass(Class class, Class aClass){
//    for(Class candidate = class; candidate != nil; candidate = class_getSuperclass(candidate))
//        if (candidate == aClass)
//            return YES;
//    
//    return NO;
//}

@implementation EGOStatement

- (id)initWithDatabase:(EGODatabase*) aDatabase stmt:(NSString*)sql{
    if ((self=[super init])){
        if (!sql){
            [self release];
            return nil;
        }
        sqlite3_stmt* aStatement = NULL;
        database = aDatabase;
        self.query = sql;
        SQLITE_API int returnCode = sqlite3_prepare_v2(database.handle, [sql UTF8String], -1, &aStatement, 0);
        @try{
            if (SQLITE_OK != returnCode) {
                if (SQLITE_BUSY == returnCode)
                    EGODBDebugLog(@"[EGODatabase] Query Failed, Database Busy:\n%@\n\n", sql);
                else
                    EGODBDebugLog(@"[EGODatabase] sqlite3_prepare_v2 Failed, Error: %d \"%@\"\n%@\n\n", [database lastErrorCode], [database lastErrorMessage], sql);
            }
            else{
                statement = aStatement;
            }
        }
        @finally {
            if (SQLITE_OK != returnCode){
                [self close];
                [self release];
                return nil;
            }
        }
    }
    return self;
}

- (void)dealloc {
	[self close];
    [query release];
	[super dealloc];
}


- (BOOL) close {
    if (!statement || !database.alive) {
        return FALSE;
    }
    BOOL result = FALSE;
    SQLITE_API int returnCode = sqlite3_finalize(statement);
    if (returnCode != SQLITE_OK && [database hadError]){
        EGODBDebugLog(@"[EGODatabase] sqlite3_finalize Failed: (%d: %@)\n", returnCode, [database lastErrorMessage]);
    }
    statement = NULL;
    return result;
}

- (BOOL) reset {
    if (!statement || !database.alive) {
        return FALSE;
    }
    BOOL result = FALSE;
    SQLITE_API int returnCode = sqlite3_reset(statement);
    if (returnCode != SQLITE_OK){
        if ([database hadError])
            EGODBDebugLog(@"[EGODatabase] sqlite3_reset Failed: (%d: %@)\n", returnCode, [database lastErrorMessage]);
        [self close];
    }
    else{
        returnCode = sqlite3_clear_bindings(statement);
        if (returnCode != SQLITE_OK){
            if ([database hadError])
                EGODBDebugLog(@"[EGODatabase] sqlite3_clear_bindings Failed: (%d: %@)\n", returnCode, [database lastErrorMessage]);
            [self close];
        }
        else{
            result = TRUE;
        }
    }
    return result;
}

-(SQLITE_API int)execute{
    if (!statement || !database.alive) return SQLITE_ERROR;
    SQLITE_API int returnCode = sqlite3_step(statement);
    switch(returnCode){
        case SQLITE_OK:
        case SQLITE_DONE:
        case SQLITE_ROW:
            break;
        case SQLITE_BUSY:
            EGODBDebugLog(@"[EGODatabase] Update Failed, Database Busy:\n%@\n\n", query);
            break;
        case SQLITE_ERROR:
            EGODBDebugLog(@"[EGODatabase] sqlite3_step Failed: (%d: %@) SQLITE_ERROR\n%@\n\n", returnCode, [database lastErrorMessage], query);
            break;
        case SQLITE_MISUSE:
            EGODBDebugLog(@"[EGODatabase] sqlite3_step Failed: (%d: %@) SQLITE_MISUSE\n%@\n\n", returnCode, [database lastErrorMessage], query);
            break;
        case SQLITE_CONSTRAINT:
            EGODBDebugLog(@"[EGODatabase] sqlite3_step Failed: (%d: %@) SQLITE_CONSTRAINT\n%@\n\n", returnCode, [database lastErrorMessage], query);
            break;
        default:
            EGODBDebugLog(@"[EGODatabase] sqlite3_step Failed: (%d: %@) UNKNOWN_ERROR\n%@\n\n", returnCode, [database lastErrorMessage], query);
    }
    return returnCode;
}

- (sqlite3_stmt *) statement {
    return statement;
}

- (void)setStatement:(sqlite3_stmt *)value {
    statement = value;
}

- (BOOL)isValid{
    return statement!=0;
}

- (NSString *) query {
    return query;
}

- (void)setQuery:(NSString *)value {
    if (query != value) {
        [query release];
        query = [value retain];
    }
}

- (long)useCount {
    return useCount;
}

- (void)setUseCount:(long)value {
    if (useCount != value) {
        useCount = value;
    }
}

- (NSString*) description {
    return [NSString stringWithFormat:@"%@ %li hit(s) for query %@", [super description], useCount, query];
}

-(BOOL)bind:(NSObject*)parametersList{
    BOOL bindStmtResult = FALSE;
    if ([parametersList isKindOfClass:[NSArray class]]){
        bindStmtResult = [self bindToParameters:(id)parametersList];
    }
    else if ([parametersList isKindOfClass:[NSDictionary class]]){
        bindStmtResult = [self bindToNamedParameters:(id)parametersList];
    }
    return bindStmtResult;
}

- (BOOL)bindToParameters:(NSArray*)parameters {
//	int index = 0;
	int queryCount = sqlite3_bind_parameter_count(statement);
//	for(id obj in parameters) {
//		index++;
//		if (bindObject(statement, obj,index)!=SQLITE_OK){
//            return FALSE;
//        }
//	}
    void	 *(*f)();
    NSInteger  n;
    NSInteger  index;
    id   obj;
    f   =  (void *) CFArrayGetValueAtIndex;
    n   = [parameters count];
    for( index = 0; index < n; index++)
    {
        obj = (*f)( parameters, index);
        if (bindObject(statement, obj,(int)index + 1)!=SQLITE_OK){
            return FALSE;
        }
    }
	
	return index == queryCount;
}

//#DEFINE SHOW_MISSING_BINDING_PARAMS

- (BOOL)bindToNamedParameters:(NSDictionary*)parameters {
	__block int index = 0;
	int queryCount = sqlite3_bind_parameter_count(statement);
	
	for (id key in parameters) {
		NSMutableString* keyStr = [NSMutableString stringWithString:[key description]];
		if (![[keyStr substringToIndex:1] isEqualToString:@"@"]){
			[keyStr insertString:@"@" atIndex:0];
        }
		int idx = sqlite3_bind_parameter_index(statement, [keyStr UTF8String]);
		if (idx){
            id obj = parameters[key];
			if (bindObject(statement, obj,idx)!=SQLITE_OK){
                return FALSE;
            }
			index++;
		}else{
            EGODBDebugLog(@"orphaned parameter value for key=%@", key);
        }
	}
    
#ifdef SHOW_MISSING_BINDING_PARAMS
#ifdef DEBUG
    if (queryCount!=index){
        for (NSUInteger i = 1; i <= queryCount; i++){
            const char * paramNameUTF8 = sqlite3_bind_parameter_name(statement, i);
            NSString* paramName = [[NSString alloc]initWithUTF8String:paramNameUTF8];
            BOOL paramNotFound = FALSE;
            if (![parameters objectForKey:paramName]){
                paramNotFound = TRUE;
                if ([paramName hasPrefix:@"@"]){
                    //check again without @
                    if ([parameters objectForKey:[paramName substringFromIndex:1]]){
                        paramNotFound = FALSE;
                    }
                }
                if (paramNotFound){
                    EGODBDebugLog(@"No binding for param=%@", paramName);
                }
            }
            [paramName release];
        }
    }
#endif
#endif
	return index == queryCount;
}
NSNull* nullValue;

Class NSUTF8StringContainerClass;
Class NSDataClass;
Class NSDateClass;
Class NSDecimalNumberClass;
Class BPUUIDClass;
Class NSNumberClass;
Class NSStringClass;

+(void)initialize{
    NSUTF8StringContainerClass = [NSUTF8StringContainer class];
    NSDataClass = [NSData class];
    NSDateClass = [NSDate class];
    NSDecimalNumberClass = [NSDecimalNumber class];
    BPUUIDClass = [BPUUID class];
    NSNumberClass =[NSNumber class];
    NSStringClass = [NSString class];
}


int bindObject(sqlite3_stmt* statement, id obj, int idx) {

	if ((!obj) || ((NSNull *)obj == nullValue)) {
		return sqlite3_bind_null(statement, idx);
	}
    Class class = object_getClass(obj);
    
    for(Class candidate = class; candidate != nil; candidate = class_getSuperclass(candidate)){
        if (candidate==NSUTF8StringContainerClass) {
            const char* text = [[obj data] bytes];
            return sqlite3_bind_text(statement, idx, text ? text : "", (int)[[obj data] length], SQLITE_STATIC);
        }
        else if (candidate == NSDataClass) {
            return sqlite3_bind_blob(statement, idx, [obj bytes], (int)[obj length], SQLITE_STATIC);
        }
        else if (candidate == NSDateClass) {
            return sqlite3_bind_double(statement, idx, [obj timeIntervalSince1970]);
        }
        else if (candidate == NSDecimalNumberClass) {
            NSDecimal d = [obj decimalValue];
            return sqlite3_bind_text(statement, idx, [NSDecimalString(&d, nil) UTF8String], -1, SQLITE_STATIC);
        }
        else if (candidate == BPUUIDClass) {
            //return sqlite3_bind_text(statement, idx, [(BPUUID*)obj UTF8String], -1, SQLITE_STATIC);
            return sqlite3_bind_text(statement, idx, [(BPUUID*)obj UTF8String], -1, sqlite_desturctor);
        }
        else if (candidate == NSNumberClass) {
            const char * objCType = [obj objCType];
            switch (*objCType){
                case 'c':
                    return sqlite3_bind_int(statement, idx, [obj charValue]);
                case 'i':
                    return sqlite3_bind_int(statement, idx, [obj intValue]);
                case 's':
                    return sqlite3_bind_int(statement, idx, [obj shortValue]);
                case 'C':
                    return sqlite3_bind_int(statement, idx, [obj unsignedCharValue]);
                case 'I':
                    return sqlite3_bind_int(statement, idx, [obj unsignedIntValue]);
                case 'S':
                    return sqlite3_bind_int(statement, idx, [obj unsignedShortValue]);
                case 'f':
                    return sqlite3_bind_double(statement, idx, [obj floatValue]);
                case 'd':
                    return sqlite3_bind_double(statement, idx, [obj doubleValue]);
                case 'l':
                    return sqlite3_bind_int64(statement, idx, [obj longValue]);
                case 'q':
                    return sqlite3_bind_int64(statement, idx, [obj longLongValue]);
                case 'L':
                    return sqlite3_bind_int64(statement, idx, [obj unsignedLongValue]);
                case 'Q':
                   return sqlite3_bind_int64(statement, idx, [obj unsignedLongLongValue]);
                default:
                    return sqlite3_bind_text(statement, idx, [[obj description] UTF8String], -1, SQLITE_STATIC);
            }
        }
        else if (candidate == NSStringClass) {
            //else if ([obj isKindOfClass:NSStringClass]) {
            return sqlite3_bind_text(statement, idx, [obj UTF8String], -1, SQLITE_STATIC);
        }
    }
	return sqlite3_bind_text(statement, idx, [[obj description] UTF8String], -1, SQLITE_STATIC);

    
    /*
    if (isSubclassOfClass(class, NSUTF8StringContainerClass)) {
    //else if ([obj isKindOfClass:NSUTF8StringContainerClass]) {
        const char* text = [[obj data] bytes];
		return sqlite3_bind_text(statement, idx, text ? text : "", [[obj data] length], SQLITE_STATIC);
	}
    else if (isSubclassOfClass(class, NSDataClass)) {
    //else if ([obj isKindOfClass:NSDataClass]) {
		return sqlite3_bind_blob(statement, idx, [obj bytes], [obj length], SQLITE_STATIC);
	}
    else if (isSubclassOfClass(class, NSDateClass)) {
    //else if ([obj isKindOfClass:NSDateClass]) {
		return sqlite3_bind_double(statement, idx, [obj timeIntervalSince1970]);
	}
    else if (isSubclassOfClass(class, NSDecimalNumberClass)) {
    //else if ([obj isKindOfClass:NSDecimalNumberClass]) {
		//return sqlite3_bind_text(statement, idx, [[obj stringValue]UTF8String], -1, SQLITE_STATIC);
        NSDecimal d = [obj decimalValue];
        return sqlite3_bind_text(statement, idx, [NSDecimalString(&d, nil) UTF8String], -1, SQLITE_STATIC);
	}
    else if (isSubclassOfClass(class, BPUUIDClass)) {
    //else if ([obj isKindOfClass:BPUUIDClass]) {
		return sqlite3_bind_text(statement, idx, [(BPUUID*)obj UTF8String], -1, SQLITE_STATIC);
	}
    else if (isSubclassOfClass(class, NSNumberClass)) {
    //else if ([obj isKindOfClass:NSNumberClass]) {
        const char * objCType = [obj objCType];
		if (strcmp(objCType, @encode(BOOL)) == 0) {
			return sqlite3_bind_int(statement, idx, ([obj boolValue] ? 1 : 0));
		} else if (strcmp(objCType, @encode(int)) == 0) {
			return sqlite3_bind_int64(statement, idx, [obj longValue]);
		} else if (strcmp(objCType, @encode(long)) == 0) {
			return sqlite3_bind_int64(statement, idx, [obj longValue]);
		} else if (strcmp(objCType, @encode(float)) == 0) {
			return sqlite3_bind_double(statement, idx, [obj floatValue]);
		} else if (strcmp(objCType, @encode(double)) == 0) {
			return sqlite3_bind_double(statement, idx, [obj doubleValue]);
		} else {
			return sqlite3_bind_text(statement, idx, [[obj description] UTF8String], -1, SQLITE_STATIC);
		}
	}
    else if (isSubclassOfClass(class, NSStringClass)) {
    //else if ([obj isKindOfClass:NSStringClass]) {
        return sqlite3_bind_text(statement, idx, [obj UTF8String], -1, SQLITE_STATIC);
    }
    else {
		return sqlite3_bind_text(statement, idx, [[obj description] UTF8String], -1, SQLITE_STATIC);
	}
    */
}


@end