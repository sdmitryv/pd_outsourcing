//
//  SqlAndCondition.h
//  ProductBuilder
//
//  Created by Valera on 9/21/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SqlFilterCondition.h"

@interface SqlAndCondition : SqlFilterCondition<ISqlFilterConditionList>{
	NSMutableArray* _list;
}

-(instancetype) initWithList:(SqlFilterCondition*) condition,...;
-(instancetype) initWithConditions:(NSArray<SqlFilterCondition*>*) conditions;
+(instancetype) list:(SqlFilterCondition*) condition,...;
+(instancetype) conditions:(NSArray<SqlFilterCondition*>*) conditions;
-(void) add:(SqlFilterCondition*) condition;
-(NSString*)linkPerdicate;
@end
