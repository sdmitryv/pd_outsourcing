//
//  SqlFilterConditionRange.h
//  ProductBuilder
//
//  Created by Valera on 9/23/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SqlFilterCondition.h"

typedef NS_ENUM(NSUInteger, CompareOperatorRange)
{
	CompareOperatorRangeInRange,
    CompareOperatorRangeNotInRange,
	CompareOperatorRangeInList,
    CompareOperatorRangeNotInList
};

@interface SqlFieldConditionRange : SqlFilterCondition<ISqlFieldConditionRange> {
	NSString* _columnName;
	CompareOperatorRange _compareOperator;
	NSArray* _values;
	NSMutableDictionary *_parameters;
	BOOL _parametersGenerated;
}

-(id) init:(NSString*)columnName compareOperatorRange:(CompareOperatorRange)compareOperator values:(NSArray*)values;
+(id) condition:(NSString*)columnName compareOperatorRange:(CompareOperatorRange)compareOperator values:(NSArray*)values;

@property (nonatomic, readonly) NSString *columnName;
@property (nonatomic, readonly) NSArray *values;
@end
