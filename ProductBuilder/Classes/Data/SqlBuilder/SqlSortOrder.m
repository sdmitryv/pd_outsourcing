//
//  SqlSortOrder.m
//  ProductBuilder
//
//  Created by Valera on 9/21/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "SqlSortOrder.h"


@implementation SqlSortOrder
@synthesize columnName = _columnName;
@synthesize sortOrder = _sortOrder;

-(id) initWithColumnName:(NSString*)columnName order:(SortOrder)order
{
	if ((self = [super init])) {
		self.sortOrder = order;
		self.columnName = columnName;
	}
	return self;
}

+(id) orderBy:(NSString*)columnName order:(SortOrder)order{
	return [[[[self class] alloc]initWithColumnName:columnName order:order]autorelease];
}

-(NSString*) description
{
	return [NSString stringWithFormat:@"%@ %@",_columnName,_sortOrder==SqlSortOrderAscending ? @"asc" : @"desc"];
}

-(void)dealloc{
	self.columnName = nil;
	
	[super dealloc];
}

@end
