//
//  SqlFieldCondition.h
//  ProductBuilder
//
//  Created by Valera on 9/20/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SqlFilterCondition.h"

typedef NS_ENUM(NSUInteger, CompareOperator) {
	Equal,
	NonEqual,
	Greater,
	Less,
	GreaterOrEqual,
	LessOrEqual,
	Like,
	Null,
	NotNull
};

@interface SqlFieldCondition : SqlFilterCondition<ISqlFieldCondition> {

	NSString* _columnName;
	CompareOperator _operator;
	id _value;
}

-(nonnull instancetype) init:(nonnull NSString*)columnName compareOperator:(CompareOperator)compareOperator value:(nullable id)value;
+(nonnull instancetype) condition:(nonnull NSString*)columnName compareOperator:(CompareOperator)compareOperator value:(nullable id)value;

@property (nonatomic, readonly, nonnull) NSString *columnName;
@property (nonatomic, readonly, nullable) id value;
@property (nonatomic, readonly) CompareOperator compareOperator;
@end
