//
//  SqlFilterCondition.m
//  ProductBuilder
//
//  Created by Valera on 9/20/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "SqlFilterCondition.h"


@implementation SqlFilterCondition
@synthesize sqlCommand = _command;

-(void) dealloc
{
	self.sqlCommand = nil;
	
	[super dealloc];
}
@end
