//
//  WebServiceOperation.m
//  StockCount
//
//  Created by Lulakov Viacheslav on 12/17/10.
//  Copyright 2010 DBBest Technologies. All rights reserved.
//

#import "WebServiceOperation.h"
#import "RTMultipartInputStream.h"
#import "SOAPAttachment.h"
#import "WebServiceParameters.h"
#import "SOAPDOMResponseParser.h"
#import "MobileExceptionLog.h"
#import "NSURLSession+Extentions.h"

NSInteger const invalidMIMETypeError = 2;
NSInteger const kSOAPFaultError = 1001001;

@interface WebServiceOperation()

@property (atomic, retain) NSMutableData *responseData;
@property (atomic, retain) NSURLSessionTask * sessionTask;

@end

@implementation WebServiceOperation

@synthesize response;
@synthesize delegate;
@synthesize responseData;
@synthesize name;
@synthesize parameters, headerParameters, responseReceived;
@synthesize returnClass, useSaxParser ,saxParserPathToRecord, useMTOM;

@synthesize nameSpace;
@synthesize methodName;
@synthesize soapActionURL;

@synthesize address;
@synthesize defaultTimeout;
@synthesize logXMLInOut;
@synthesize cookies;
@synthesize authUsername;
@synthesize authPassword;


- (id)initWithDelegate:(id<WebServiceOperationDelegate>)aDelegate parameters:(WebServiceParameters *)aParameters{
	if ((self = [self initWithDelegate:aDelegate parameters:aParameters headerParameters:nil])) {
	}
	
	return self;
}

- (id)initWithDelegate:(id<WebServiceOperationDelegate>)aDelegate parameters:(WebServiceParameters *)aParameters headerParameters:(WebServiceParameters *)aHeaderParameters{
	if ((self = [super init])) {
		self.delegate = aDelegate;
		self.parameters = aParameters;
        self.headerParameters = aHeaderParameters;
        self.nameSpace = @"http://tempuri.org/";
        self.logXMLInOut = NO;
        self.sendErrorLog = YES;
	}
	
	return self;
}

-(id)retain{
    return [super retain];
}

-(void)start{
    [self main];
}

-(void)cancel{
    //[super cancel];
    delegate = nil;
    [self.sessionTask cancel];
    self.sessionTask = nil;
    [soapParser cancel];
    [xopParser finish];
}

- (void)dealloc {
    [self cancel];
    [_sessionTask release];
    [nameSpace release];
	[methodName release];
	[soapActionURL release];
	[address release];
	[cookies release];
    
	[response release];
	delegate = nil;
	[responseData release];
	[parameters release];
    [headerParameters release];
	[resultName release];
    [responseName release];
    
    [saxParserPathToRecord release];
    [soapParser cancel];
    [soapParser release];
    [xopParser finish];
    [xopParser release];
    
    [_requestBody release];
    [name release];
    [authUsername release];
    [authPassword release];
    [super dealloc];
}

- (NSString *)resultName {
    return resultName;
}

- (NSString *)responseName {
    return responseName;
}

- (void)main {
    
    //self.logXMLInOut = YES; //ALEX ADDED THIS LINE FOR DEBUGGING
    
    [resultName release];
    [responseName release];
    [response release];
	response = [[WebServiceResponse alloc]init];
    if (!self.address){
        responseReceived = TRUE;
        NSDictionary *userInfo = @{NSLocalizedDescriptionKey: @"Address URL is not defined"};
        NSError* error = [NSError errorWithDomain:@"NSURLErrorDomain" code:-1001 userInfo:userInfo];
        response.error = error;
        [self handleResponse];
        return;
    }
    resultName = [[NSString alloc]initWithFormat:@"%@Result", name];
    responseName = [[NSString alloc]initWithFormat:@"%@Response", name];

	NSMutableDictionary *bodyElements = [[NSMutableDictionary alloc]init];
	//if(parameters)
    //in case of empty parameters add fake one to force including soap action into the body
    bodyElements[name] = parameters ? parameters:[NSString string];
	
	NSString *operationXMLString = [self serializedFormUsingBodyElements:bodyElements];
	[bodyElements release];
    NSString* namespaceStr = [self.nameSpace hasSuffix:@"/"] ? self.nameSpace : [self.nameSpace stringByAppendingString:@"/"];
	NSString *actionName = [[NSString alloc]initWithFormat:@"%@%@", namespaceStr, name];
	[self sendHTTPCallUsingBody:operationXMLString soapAction:actionName];
    [actionName release];
}

- (void)addCookie:(NSHTTPCookie *)toAdd
{
	if(toAdd != nil) {
		if(cookies == nil) cookies = [[NSMutableArray alloc] init];
		[cookies addObject:toAdd];
	}
}

#pragma mark -
#pragma mark send stuff

- (NSString *)serializedFormUsingBodyElements:(NSDictionary *)bodyElements{
    xmlDocPtr doc;
	
	doc = xmlNewDoc((const xmlChar*)XML_DEFAULT_VERSION);
	if (!doc) {
		NSLog(@"Error creating the xml document tree");
		return @"";
	}
	
	xmlNodePtr root = xmlNewDocNode(doc, NULL, (const xmlChar*)"Envelope", NULL);
	xmlDocSetRootElement(doc, root);
	
	xmlNsPtr soapEnvelopeNs = xmlNewNs(root, (const xmlChar*)"http://schemas.xmlsoap.org/soap/envelope/", (const xmlChar*)"soap");
	xmlSetNs(root, soapEnvelopeNs);
    xmlNewProp(root, (const xmlChar*)"encodingStyle", (const xmlChar*)"http://schemas.xmlsoap.org/soap/encoding/");
    xmlNewNs(root, (const xmlChar*)[self.nameSpace UTF8String], (const xmlChar*)"tem");
    
    if(self.headerParameters.parameters.count){
        xmlNodePtr headerNode = xmlNewChild(root, soapEnvelopeNs, (const xmlChar*)"Header", NULL);
        //xmlAddChild(root, [self.headerParameters.parameters xmlNodeForDoc:doc elementName:@"Header" elementNSPrefix:@"tem"]);
        for(NSString *key in self.headerParameters.parameters) {
            id value = [self.headerParameters.parameters valueForKey:key];
            if ((value != nil) && ([value class] != [NSNull class])) {
                xmlAddChild(headerNode, [value xmlNodeForDoc:doc elementName:key elementNSPrefix:@"tem"]);
            }
        }
	}
	
	//place body everytime
    xmlNodePtr bodyNode = xmlNewChild(root, soapEnvelopeNs, (const xmlChar*)"Body", NULL);
    if(bodyElements.count){
        for(NSString *key in [bodyElements allKeys]) {
            id body = bodyElements[key];
            xmlAddChild(bodyNode, [body xmlNodeForDoc:doc elementName:key elementNSPrefix:@"tem"]);
        }
	}
    
	xmlChar *buf;
	int size;
	xmlDocDumpFormatMemory(doc, &buf, &size, 1);
	
	NSString *serializedForm = buf ? @((const char*)buf) : nil;
    if (buf){
        xmlFree(buf);
    }
	
	xmlFreeDoc(doc);
	return serializedForm;
}

- (void)sendHTTPCallUsingBody:(NSString *)outputBody soapAction:(NSString *)soapAction
{
	NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:self.address 
														   cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData
													   timeoutInterval:self.defaultTimeout];
	if(cookies) {
		[request setAllHTTPHeaderFields:[NSHTTPCookie requestHeaderFieldsWithCookies:cookies]];
	}
	[request setValue:@"wsdl2objc" forHTTPHeaderField:@"User-Agent"];
	[request setValue:soapAction forHTTPHeaderField:@"SOAPAction"];
    if (!self.useMTOM){
        NSData *bodyData = [outputBody dataUsingEncoding:NSUTF8StringEncoding];
        [request setValue:@"text/xml; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
        [request setValue:[NSString stringWithFormat:@"%lu", (unsigned long)[bodyData length]] forHTTPHeaderField:@"Content-Length"];
        // set version 1.1 - how?
        [request setHTTPBody: bodyData];
    }
    else{
        
        //        NSData* bodyData = [NSData dataWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"request_soapui_2" ofType:@"xml"]];
        //        [request setHTTPBody: bodyData];
        //        [request setValue:[NSString stringWithFormat:@"%u", [bodyData length]] forHTTPHeaderField:@"Content-Length"];
        
        NSString* bodyContentId =@"rootpart@cloudwk.com";//[NSString stringWithFormat:@"<%@>",[[NSProcessInfo processInfo] globallyUniqueString]];
        RTMultipartInputStream *bodyStream = [[RTMultipartInputStream alloc] init];
        outputBody = [outputBody stringByReplacingOccurrencesOfString:@"<?xml version=\"1.0\"?>" withString:@""];
        [bodyStream addPartWithName:@"body" string:outputBody contentId:bodyContentId];
        /*
         An HTTP Content-Type header should have a start parameter with the value of the Content-ID header of the MIME part that contains the SOAP 1.x Envelope, enclosed in double quotation marks. If the start parameter is omitted, the first MIME part must contain the SOAP 1.x Envelope
         */
        NSString* contentType = [NSString stringWithFormat:@"multipart/related; type=\"application/xop+xml\"; start=\"<%@>\"; start-info=\"application/soap+xml\"; boundary=\"%@\"",bodyContentId, bodyStream.boundary];
        [request setValue:contentType forHTTPHeaderField:@"Content-Type"]; 
        for (id object in [[self.parameters parameters] allValues]){
            if ([object isKindOfClass:[SOAPAttachment class]]){
                SOAPAttachment* attachment = (SOAPAttachment*)object;
                [bodyStream addPartWithName:attachment.name path:attachment.path contentId:attachment.contentId];
            }
        }
        
        [request setValue:[NSString stringWithFormat:@"%lu", (unsigned long)[bodyStream length]] forHTTPHeaderField:@"Content-Length"];
        [request setValue:@"1.0" forHTTPHeaderField:@"MIME-Version"];
        [request setHTTPBodyStream:bodyStream];
        [bodyStream release];
    }
    
    [request setValue:self.address.host forHTTPHeaderField:@"Host"];
	[request setHTTPMethod: @"POST"];
    
	if(self.logXMLInOut) {
        NSLog(@"OutputPath:\n%@", [request URL]);
		NSLog(@"OutputHeaders:\n%@", [request allHTTPHeaderFields]);
		NSLog(@"OutputBody:\n%@", outputBody);
	}
    
    if (self.sendErrorLog) {
        [_requestBody release];
        _requestBody = [outputBody retain];
    }
    
    NSURLSessionConfiguration* sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
    sessionConfig.timeoutIntervalForRequest = self.defaultTimeout;
    NSOperationQueue* delegateQueue = [[[NSOperationQueue alloc] init]autorelease];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:sessionConfig delegate:self delegateQueue:delegateQueue];
	self.sessionTask = [session dataTaskWithRequest:request];
    [self.sessionTask resume];
}

- (NSURLSessionTask*) sessionTask{
    @synchronized(self) {
        return [[_sessionTask retain] autorelease];
    }
}

- (void) setSessionTask:(NSURLSessionTask *)value{
    @synchronized(self) {
        [value retain];
        [_sessionTask release];
        _sessionTask = value;
    }
}

#pragma mark -
#pragma mark NSURSessionDelegate

- (void)URLSession:(NSURLSession *)session didReceiveChallenge:(NSURLAuthenticationChallenge *)challenge
 completionHandler:(void (^)(NSURLSessionAuthChallengeDisposition disposition, NSURLCredential * __nullable credential))completionHandler{
    
    [session defaultHandlerForDidReceiveChallenge:challenge completionHandler:completionHandler userName:self.authUsername password:self.authPassword errorHandler:^(NSError* authError){ [self handleError:authError]; }];

}

-(void)URLSession:(NSURLSession *)session task:(NSURLSessionTask *)task didSendBodyData:(int64_t)bytesSent totalBytesSent:(int64_t)totalBytesSent totalBytesExpectedToSend:(int64_t)totalBytesExpectedToSend{
    
    if (delegate && [delegate respondsToSelector:@selector(operation:completedUploadWithProgress:)]){
        [delegate operation:self completedUploadWithProgress:(float)totalBytesSent/(float)totalBytesExpectedToSend];
    }
}

-(void)URLSession:(NSURLSession *)session dataTask:(NSURLSessionDataTask *)dataTask didReceiveResponse:(NSURLResponse *)urlResponse completionHandler:(void (^)(NSURLSessionResponseDisposition))completionHandler{
    responseReceived = TRUE;
	NSHTTPURLResponse *httpResponse;
	if ([urlResponse isKindOfClass:[NSHTTPURLResponse class]]) {
		httpResponse = (NSHTTPURLResponse *) urlResponse;
	} else {
		httpResponse = nil;
	}
	
	if(self.logXMLInOut && httpResponse) {
		NSLog(@"ResponseStatusMessage: %@\n", [NSHTTPURLResponse localizedStringForStatusCode:[httpResponse statusCode]]);
        NSLog(@"ResponseStatus: %ld\n", (long)[httpResponse statusCode]);
		NSLog(@"ResponseHeaders:\n%@", [httpResponse allHeaderFields]);
	}
    
    NSError *error = nil;
    if ([httpResponse statusCode] >= 400 && [httpResponse statusCode] < 500) {
        NSDictionary *userInfo = @{NSLocalizedDescriptionKey: [NSHTTPURLResponse localizedStringForStatusCode:[httpResponse statusCode]]};
        
        error = [NSError errorWithDomain:@"WebServiceResponseHTTP" code:[httpResponse statusCode] userInfo:userInfo];

    }
	else
        if ([urlResponse.MIMEType rangeOfString:@"text/xml"].length == 0) {
        if ([urlResponse.MIMEType rangeOfString:@"multipart/related"].length == 0){
			NSDictionary *userInfo = @{NSLocalizedDescriptionKey: [NSString stringWithFormat: @"Unexpected response MIME type to SOAP call:%@", urlResponse.MIMEType]};
			error = [NSError errorWithDomain:@"WebServiceResponseHTTP" code:invalidMIMETypeError userInfo:userInfo];
        }
        else{
            xopParser = [[XopXmlResponseParser alloc]initWithResponseName:responseName headers:httpResponse.allHeaderFields error:&error];
            if (xopParser){
                xopParser.delegate  =self;
                responseIsXop = TRUE;
            }
        }
	}
    if (error){
        response.error = error;
        if (completionHandler){
            completionHandler(NSURLSessionResponseCancel);
        }
    }
    else{
        if (!responseIsXop && useSaxParser){
            soapParser = [[SOAPResponseParser alloc]initWithResponseName:responseName];
            soapParser.pathToObjectRecord = self.saxParserPathToRecord;
            soapParser.delegate = self;
        }
    }
    if (completionHandler){
        completionHandler(NSURLSessionResponseAllow);
    }
}

-(void)URLSession:(NSURLSession *)session dataTask:(NSURLSessionDataTask *)dataTask didReceiveData:(NSData *)data{
    responseReceived = TRUE;
    if (response.error && ((response.error.code>400 && response.error.code!=500) || response.error.code==invalidMIMETypeError)){
        return;
    }
    if (responseIsXop){
        [xopParser parseChunk:data];
    }
    else if (useSaxParser){
        // Process the downloaded chunk of data.
        [soapParser parseChunk:data];
        //NSLog(@"%@", [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]); //ALEX ADDED THIS LINE FOR DEBUGGING
    }
    else{
        if (responseData == nil) {
            responseData = [data mutableCopy];
        } else {
            [responseData appendData:data];
        }
    }
}

-(void)handleError:(NSError*)anError{
    if (anError.code==NSURLErrorCancelled) return;
    if (self.logXMLInOut) {
        NSLog(@"ResponseError:\n%@", anError);
    }
    response.error = anError;
    [self handleResponse];
    
}

-(void)URLSession:(NSURLSession *)session task:(NSURLSessionTask *)task didCompleteWithError:(NSError *)error{
    responseReceived = TRUE;
    @try{
        if (error.code==NSURLErrorCancelled){
            if (self.response.error){
                error = self.response.error;
            }
            else{
                [session invalidateAndCancel];
                return;
            }
        }
        [session finishTasksAndInvalidate];
        if (error){
            [self handleError:error];
            return;
        }
    }
    @finally{
        self.sessionTask = nil;
    }
    if (responseIsXop){
        [xopParser finish];
        [xopParser release];
        xopParser = nil;
    }
    else if (useSaxParser){
        // Signal the context that parsing is complete by passing "1" as the last parameter.
        if (soapParser.soapFault){
            NSMutableArray *responseBodyParts = [[NSMutableArray alloc]init];
            [responseBodyParts addObject:soapParser.soapFault];
            response.bodyParts = responseBodyParts;
            [responseBodyParts release];
            
            if (self.sendErrorLog) {
                [MobileExceptionLog postSoapFault:soapParser.soapFault originalRequest:_requestBody];
            }
        }
        [soapParser finish];
        [soapParser release];
        soapParser = nil;
        [self handleResponse];
        return;
    }
    
    if ( delegate){
        if (responseData)
        {
            @autoreleasepool {
                if (self.logXMLInOut) {
                    NSLog(@"ResponseBody:\n%@", [[[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding] autorelease]);
                }
                SOAPDOMResponseParser* domParser = [[SOAPDOMResponseParser alloc]initWithResponseName:responseName resultName:resultName returnClass:returnClass];
                [domParser parse:responseData response:response];
                
                if (self.sendErrorLog) {
                    for (NSObject *obj in response.bodyParts) {
                        if ([obj isKindOfClass:[SOAPFault class]]) {
                            [MobileExceptionLog postSoapFault:(SOAPFault *)obj originalRequest:_requestBody];
                            break;
                        }
                    }
                }
                
                [domParser release];
            }
        }
        [self handleResponse];
    }
}

-(NSString*)getRequestString {
    NSMutableDictionary *bodyElements = [[NSMutableDictionary alloc]init];
    //if(parameters)
    //in case of empty parameters add fake one to force including soap action into the body
    bodyElements[name] = parameters ? parameters:[NSString string];
    
    NSString *operationXMLString = [self serializedFormUsingBodyElements:bodyElements];
    [bodyElements release];
    
    return operationXMLString;
}

-(void)handleResponse{
    if (!response.error){
        for(id bodyPart in response.bodyParts) {
            if ([bodyPart isKindOfClass:[SOAPFault class]]) {
                SOAPFault* soapFault = (SOAPFault *)bodyPart;
                NSError* error = [NSError errorWithDomain:@"WebServiceResponseHTTP" code:kSOAPFaultError userInfo:@{NSLocalizedDescriptionKey: soapFault.simpleFaultString?:@"", NSLocalizedFailureReasonErrorKey:soapFault.faultcode?:@""}];
                    response.error = error;
                }
        }
    }
    [delegate retain];
    [delegate operation:self completedWithResponse:response];
    [delegate release];
}

#pragma mark -
#pragma mark SOAPResponseParserDelegate

-(void)parser:(SOAPResponseParser *)parser didReceiveRecord:(XmlElement*)object{
    if ([delegate respondsToSelector:@selector(operation:didReceiveRecord:)]){
        [delegate operation:self didReceiveRecord:object];
    }
}

-(void)parser:(SOAPResponseParser *)parser parsingError:(NSError*)error{
    response.error = error;
	[self handleResponse];
}

#pragma mark -
#pragma mark

-(void)xopXmlResponseParser:(XopXmlResponseParser *)parser didReceiveBinaryStream:(NSInputStream*)stream{
    if ([delegate respondsToSelector:@selector(operation:didReceiveBinaryStream:)]){
        [delegate operation:self didReceiveBinaryStream:stream];
    }
}

-(void)xopXmlResponseParser:(XopXmlResponseParser *)parser parsingError:(NSError*)error{
    response.error = error;
	[self handleResponse];
}

@end
