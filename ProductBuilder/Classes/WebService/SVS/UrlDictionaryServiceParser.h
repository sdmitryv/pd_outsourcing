//
//  UrlDictionaryServiceParser.h
//  ProductBuilder
//
//  Created by valera on 3/14/13.
//  Copyright (c) 2013 Cloudworks. All rights reserved.
//

typedef NS_ENUM(NSUInteger, UDSErrorCode){
    UDSInternalError = 1,	//Internal UDS error occured
    UDSBadInputParameters = 2, //Some of input parameters are invalid
    UDSURLNotFound = 3, //URL with specified label not found
};


@interface UrlDictionaryServiceParser : NSObject {
}

+(NSString*)localizedErrorMessageByErrorCode:(UDSErrorCode)code;
@end
