//
//  StoredValuesServiceLoaderHelper.m
//  ProductBuilder
//
//  Created by valery on 9/7/15.
//  Copyright (c) 2015 Cloudworks. All rights reserved.
//

#import "SVSLazyValue.h"

NSString* const SVSLazyValueDidChangeState = @"SVSLazyValueDidChangeState";

@interface SVSLazyValue(){
    dispatch_queue_t _sync_queue;
    BOOL _postingNotification;
    NSCondition* _runningCondition;
}

@property(atomic, retain) StoredValuesServiceRequest* request;
@property(atomic, retain) NSError* error;

@end

@implementation SVSLazyValue

-(instancetype)initWithRequest:(StoredValuesServiceRequest*)request{
    if ((self = [super init])){
        _sync_queue = dispatch_queue_create("com.sync.svsLazyValue", DISPATCH_QUEUE_CONCURRENT);
        if (request){
            self.state = SVSLazyValueStateInProgress;
            _request = [request retain];
            _request.delegate = self;
            _runningCondition = [[NSCondition alloc]init];
            [_request exec];
        }
        
        
    }
    return self;
}

-(void)dealloc{
    _request.delegate = nil;
    [_request cancel];
    dispatch_release(_sync_queue);
    [_runningCondition release];
    [_request release];
    [_error release];
    [_value release];
    [super dealloc];
}

-(NSObject*)value{
    __block NSObject* value = nil;
    dispatch_sync(_sync_queue, ^{
        value = [_value retain];
    });
    return [_value autorelease];
}

- (void) setValue:(NSObject*)value{
    dispatch_barrier_async(_sync_queue, ^{
        [value retain];
        [_value release];
        _value = value;
    });
}

-(void)signalStateChanged{
    dispatch_async(dispatch_get_main_queue(), ^{
        _postingNotification = YES;
        [[NSNotificationCenter defaultCenter]postNotificationName:SVSLazyValueDidChangeState object:self userInfo:nil];
        _postingNotification = NO;
    });
}

-(BOOL)postingNotification{
    return _postingNotification;
}

-(void)setState:(SVSLazyValueState)state{
    dispatch_barrier_async(_sync_queue, ^{
        _state = state;
        [self signalStateChanged];
    });
}
                  
-(SVSLazyValueState)state{
    __block SVSLazyValueState state;
     dispatch_sync(_sync_queue, ^{
         state = _state;
     });
    return state;
}

-(void)waitUntiInProgress{
    if (self.state==SVSLazyValueStateInProgress){
        [_runningCondition lock];
         if (self.state==SVSLazyValueStateInProgress){
            [_runningCondition wait];
        }
        [_runningCondition unlock];
    }
}


- (void)storedValuesServiceRequestDidComplete:(StoredValuesServiceRequest *)request withResult:(NSObject *)result {
    self.value = request.result;
    self.state = SVSLazyValueStateReady;
    [_runningCondition lock];
    [_runningCondition broadcast];
    [_runningCondition unlock];
}

- (void)storedValuesServiceRequestDidComplete:(StoredValuesServiceRequest *)request withError:(NSError *)error {
    self.error = error;
    self.state = SVSLazyValueStateFailed;
    [_runningCondition lock];
    [_runningCondition broadcast];
    [_runningCondition unlock];
}

- (void)storedValuesServiceRequestDidCancel:(StoredValuesServiceRequest *)request{
    self.state = SVSLazyValueStateCanceled;
    [_runningCondition lock];
    [_runningCondition broadcast];
    [_runningCondition unlock];
}

-(BOOL)isErrorConnectionRelated{
    if (self.state != SVSLazyValueStateFailed) return NO;
    switch (self.error.code) {
        case NSURLErrorNotConnectedToInternet:
        case NSURLErrorTimedOut:
        case NSURLErrorNetworkConnectionLost:
        case NSURLErrorCancelled:
            return YES;
        default:
            return NO;
    }
}

-(void)refresh{
    if (_state!=SVSLazyValueStateInProgress){
        dispatch_barrier_async(_sync_queue, ^{
            if (_state!=SVSLazyValueStateInProgress){
                _state = SVSLazyValueStateInProgress;
                [_value release];
                _value = nil;
                [self signalStateChanged];
                [_request exec];
                
            }
        });
    }
}


@end
