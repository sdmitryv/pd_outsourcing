//
//  StoredValuesServiceRequest.h
//  ProductBuilder
//
//  Created by valera on 3/14/13.
//  Copyright (c) 2013 Cloudworks. All rights reserved.
//

#import <Foundation/Foundation.h>


typedef NS_ENUM(NSUInteger, StoredValuesServiceRequestType) {
    NONEREQUESTTYPE = 0,
    UPDATECUSTOMER,
    GETBALANCE,
    AUTHORIZETRANSACTIONLP,
    COMMITTRANSACTIONLP,
    ISONLINE,
    RECALCULATEBALANCE,
    GETGCBALANCE,
    AUTHORIZEGCTRANSACTION,
    COMMITGCTRANSACTION,
    GIFTCARDEXISTS,
    FETCHLATESTGCTRANSACTION,
    REVERTGCTRANSACTION,
    GIFTCARDCREATE,
    GIFTCARDSFORCUSTOMER,
    GIFTCARDASSIGNCUSTOMER,
//    AUTHORIZEHCTRANSACTION,
//    COMMITHCTRANSACTION,
    GETHCBALANCE,
    UPDATEBALANCE,
    FETCHLATESTHCTRANSACTION,
    AUTHORIZEFBTRANSACTION,
    GETCUSTOMERLOYALTYPROGRAMS,
    GETLRPBALANCE,
    FETCHLATESLRPTRANSACTIONS,
    FETCHLATESLRPTRANSACTIONS2,
    LRPRESENDGCAWARDEMAIL,
    ASSIGNLRPTOCUSTOMER,
    REGISTERDEVICE,
    FINDCUSTOMER,
    REGISTERCUSTOMER,
    GETCUSTOMER,
    CHECKCUSTOMEREMAIL,
    CHECKCUSTOMERPHONE,
    VALIDATEEMAIL,
    CHECKCUSTOMERPASSWORD,
    GETSETTINGS,
    COUPONSCHECK,
    COUPONSLIST,
    COUPONSAUTHORIZE,
    COUPONSCAPTURE,
    COUPONSDISCARD,
    COUPONSACTIVECUSTOMER,
    COUPONSACTIVECUSTOMERCOUNT,
    GIFTCARDEXISTSMULTI,
    FETCHSCTRSANSACTION,
    AUTHORIZESCTRANSACTION,
    COMMITSCTRANSACTION,
    DISCARDSCTRANSACTION,
    GETSCBALANCE,
    AUTHORIZERECEIPTTRANSACTION,
    COMMITRECEIPTTRANSACTION,
    DISCARDRECEIPTTRANSACTION,
    FETCH_TOKEN_TRSANSACTION,
//    AUTHORIZE_TOKEN_TRANSACTION,
//    COMMIT_TOKEN_TRANSACTION,
//    DISCARD_TOKEN_TRANSACTION,
    GET_TOKEN_BALANCE,
    CARDONFILECREATEORUPDATE,
    CARDONFILEGETLIST,
    CARDONFILEDELETE
};


typedef NS_ENUM(NSUInteger, StoredValuesServiceRequestState) {
    StoredValuesServiceRequestPending = 0,
    StoredValuesServiceRequestStarted,
    StoredValuesServiceRequestCanceled,
    StoredValuesServiceRequestFinished
};

@class StoredValuesService;
@class StoredValuesServiceRequest;

@protocol StoredValuesServiceDelegate <NSObject>

@optional

- (void)storedValuesServiceRequestDidComplete:(StoredValuesServiceRequest *)request withResult:(NSObject *)result;
- (void)storedValuesServiceRequestDidComplete:(StoredValuesServiceRequest *)request withError:(NSError *)error;
- (void)storedValuesServiceRequestDidCancel:(StoredValuesServiceRequest *)request;

@end

typedef struct{
    BOOL didCompleteWithResult;
    BOOL didCompleteWithError;
    BOOL didCancel;
} StoredValuesServiceDelegateImplementedMethods;

@interface StoredValuesServiceRequest: NSObject<NSURLSessionDataDelegate> {
    NSTimeInterval defaultTimeout;
    BOOL logEnable;
    NSDictionary * cookies;
    NSString * authUsername;
	NSString * authPassword;
    NSURLResponse * urlResponse;
    NSString * authValue;
    NSMutableData * responseData;
    StoredValuesServiceRequestType requestType;
    NSDictionary * parameters;
    Class returnClass;
	id<StoredValuesServiceDelegate> delegate;
    NSError * error;
    NSObject * result;
    StoredValuesServiceRequestState state;
    NSURLSessionTask * sessionTask;
    NSURL * serviceURL;
    dispatch_semaphore_t waitForRequestSemaphore;
    NSOperationQueue* _delegateQueue;
    StoredValuesServiceDelegateImplementedMethods delegateMethods;
}
@property (nonatomic, retain) NSObject* context;
@property (nonatomic, assign) NSTimeInterval defaultTimeout;
@property (nonatomic, retain) NSDictionary * cookies;
@property (nonatomic, assign) BOOL logEnable;
@property (nonatomic, retain) NSString * authValue;
@property (nonatomic, retain) NSData * responseData;
@property (nonatomic, retain) NSString * authUsername;
@property (nonatomic, retain) NSString * authPassword;
@property (nonatomic, retain) NSURLResponse * urlResponse;
@property (nonatomic, assign) StoredValuesServiceRequestState state;
@property (nonatomic, assign) StoredValuesServiceRequestType requestType;
@property (nonatomic, readonly) BOOL isAuthorized;
@property (nonatomic, retain) NSDictionary * parameters;
@property (nonatomic, assign) Class returnClass;
@property (nonatomic, assign) id<StoredValuesServiceDelegate> delegate;
@property (nonatomic, retain) NSError * error;
@property (nonatomic, retain) NSObject * result;
@property (nonatomic, readonly) BOOL isCompleted;
@property (nonatomic, readonly) BOOL isSucceed;
@property (nonatomic, retain) NSURLSessionTask * sessionTask;
@property (nonatomic, retain) NSURL * serviceURL;
@property (nonatomic, assign) BOOL isTokenResult;

-(id)initWithType:(StoredValuesServiceRequestType)type url:(NSURL *)url params:(NSDictionary *)params delegate:(id<StoredValuesServiceDelegate>) delegate;
-(id)initWithType:(StoredValuesServiceRequestType)type url:(NSURL *)url params:(NSDictionary *)params;
-(void)exec;
-(void)execSync;
-(void)cancel;
-(void)waitUntilDelegateQueueCompleted;
@end
