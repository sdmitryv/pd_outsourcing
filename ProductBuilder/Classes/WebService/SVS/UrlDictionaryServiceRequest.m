//
//  UrlDictionaryServiceRequest.m
//  ProductBuilder
//
//  Created by valera on 3/14/13.
//  Copyright (c) 2013 Cloudworks. All rights reserved.
//

#import "UrlDictionaryServiceRequest.h"
#import "UrlDictionaryServiceParser.h"


@implementation UrlDictionaryServiceRequest


-(NSString *)localizedErrorByCode:(int)errorCode {
    
    return [UrlDictionaryServiceParser localizedErrorMessageByErrorCode:errorCode];
}

-(NSObject *)tryGetResultFromRequest:(NSDictionary *)transactionsDictionary requestType:(int)requestType {
    
    return transactionsDictionary[@"url"];
}

@end
