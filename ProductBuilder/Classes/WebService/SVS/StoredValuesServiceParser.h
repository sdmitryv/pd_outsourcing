//
//  StoredValuesServiceParser.h
//  ProductBuilder
//
//  Created by valera on 3/14/13.
//  Copyright (c) 2013 Cloudworks. All rights reserved.
//

#import "BPUUID.h"

typedef NS_ENUM(NSUInteger, SVSErrorCode){
    SVSErrorCodeInternalError = 1,	//Internal SVS error occured
    SVSErrorCodeBadRegistrationToken = 2, //Registration Token specified is not valid or was deauthorized
    SVSErrorCodeBadNamespace = 3, //Specified namespace extracted from Access Token cannot be used for given operation
    SVSErrorCodeBadInputParameters = 4, //Some of input parameters are invalid
    SVSErrorCodeAccessDenied = 5, //Access to specified operation was denied (e.g., the same registration token is used twice, etc.)
    SVSErrorCodeBadAccessToken = 6, //Access-Token header is invalid or missing
    SVSErrorCodeDuplicateEmail = 7,	//Customer with specified email already exists (e.g., on updating or creating a customer)
    SVSErrorCodeDuplicatePhone = 8,	//Customer with specified phone # already exists (e.g., on updating or creating a customer)
    SVSErrorCodeBadCredentials = 9,	//Invalid authentication data (email, phone, password, member code) provided on customer authentication
    SVSErrorCodeDuplicateGiftcard = 10,	//Attempting to create a Gift Card with number already present in SVS (use "Check" function in "giftcards" area before trying to create a GC to avoid this)
    SVSErrorCodeUnknownGiftcard = 11, //A Gift Card with specified number doesn't exist in SVS
    SVSErrorCodeNotEnoughBalance = 12,	//Balance of the stored value (e.g., Gift Card) is not enough to authorize a transaction
    SVSErrorCodeUnknownTransaction = 13,	//Transaction with specified ID doesn't exist (e.g., on Capture or Discard)
    SVSErrorCodeIllegalState = 14,	//Requested operation can't be performed in current state (e.g., trying to assign customer's gift card to another customer)
    SVSErrorCodeNotAuthenticated = 15,	//Current user is not authenticated
    SVSErrorCodeCorruptedData = 16,	//Received data doesn't correspond to declared structure (e.g., incomplete batch on Logs upload)
    SVSErrorCodeUnknownCustomer = 17,	//Customer with specified ID doesn't exist
    SVSErrorCodeUnknownDevice = 18,	//Device with specified ID doesn't exist
    SVSErrorCodeUnknownMessage = 19,	//Scheduled message with given ID doesn't exist
    SVSErrorCodeAuthorizedTransaction = 20,	//Operation is not allowed because specified transaction is in "AUTHORIZED" state. See message for expected state
    SVSErrorCodeDiscardedTransaction = 21,	//Operation is not allowed because specified transaction is in "DISCARDED" state. See message for expected state
    SVSErrorCodeCapturedTransaction = 22,	//Operation is not allowed because specified transaction is in "CAPTURED" state. See message for expected state
    SVSErrorCodeStatsServiceError = 23,	//Statistics service failed while processing current request
    SVSErrorCodeSearchServiceError = 24,	//Search failed while processing current request
    SVSErrorCodeUnknownProgram = 25,	//Stored value program with specified ID doesn't exist
    SVSErrorCodeDuplicateProgram = 26,	//Stored value program with specified ID already exists
    SVSErrorCodeDuplicateCoupon = 27,	//A coupon with specified customer ID and program ID already exists
    SVSErrorCodeDeadlineExceededError = 28,	//Deadline exceeded while processing data import
    SVSErrorCodeUnknownCoupon = 29,	//A coupon with specified customer ID and program ID doesn't exist
    SVSErrorCodeDuplicateMembercode = 30,	//Customer with specified membership code already exist
    SVSErrorCodeDuplicateCustomer = 31,	//Customer with specified ID already exists
    SVSErrorCodeInsecureProtocol = 32,	//Attempting to access secure API over insecure HTTP connection. HTTPS should be used instead
    SVSErrorCodeExpiredCoupon = 33,     //Attempting to redeem an expired coupon
    SVSErrorCodeInactiveGiftcard = 34,  //Attempting to use a deactivated gift card
    SVSErrorCodeMemberOnly = 35,         //Attempting to perform an operation allowed for MEMBER status customers only
    SVSErrorCodeDuplicateTransaction = 36,	//A transaction with specified reference number already exist
    SVSErrorCodeRechargeProhibited = 37,	//Specified stored value (e.g., Gift Card) recharge is not allowed
    SVSErrorCodeTransferProhibited = 38,	//Balance transfer from specified stored value (e.g., Gift Card) is not allowed
    SVSErrorCodeForeignGiftcard = 39,	//Attempting to perform an operation (e.g., balance transfer) between stored values belonging to different customers
    SVSErrorCodeSendGridServiceError = 40,
    SVSErrorCodeUnknownMarketingCampaign = 41,
    SVSErrorCodeSendGridTemplateEngineError = 42,
    SVSErrorCodeRelateNotResponding = 43, // Relate API is not responding
    SVSErrorCodeRelateResponseError = 44,  // Relate API response contains unexpected error
    SVSErrorCodeUnknownImport = 45, //Import with specified ID doesn't exist
    SVSErrorCodePostMarkServiceError = 46, //Postmark mailing service respond error
    SVSErrorCodeDuplicateExternalMailingSetting = 47, //External transactional mailing type with given name already exist
    SVSErrorCodeUnknownExternalMailingSetting = 48, //External transactional mailing types with given name doesn't exist
    SVSErrorCodeUnknownMessageAttachment = 49, //Message attachment with given name doesn't exist
    SVSErrorCodeBadTransactionType = 50, //Type of given transaction is bad for operation
    SVSErrorCodeMandrillServiceError = 51, //Mandrill mailing service respond error
    SVSErrorCodeUnknownEvent = 52, //SVS event with given ID doesn't exist
    SVSErrorCodeUnknownClient = 53, //Client with given ID doesn't exist
    SVSErrorCodeUnknownApplication = 54, //Application with given ID doesn't exist
    SVSErrorCodeUnknownRight = 55, //Right with given ID doesn't exist
    SVSErrorCodeUnknownRole = 56, //Role with given ID doesn't exist
    SVSErrorCodeDuplicateApplication = 57, //Application with given ID already exist
    SVSErrorCodeDuplicateClient = 58, //Client with given ID already exist
    SVSErrorCodeDuplicateFBProgram = 59, //Frequent buyer program with given ID already exist
    SVSErrorCodeUnknownFBProgram = 60, //Frequent buyer program with given ID doesn't exist
    SVSErrorCodeUnknownSalesReceipt = 61, //Sales receipt with given ID doesn't exist
    SVSErrorCodeUnknownSalesReceiptItem = 62, //Sales receipt item with given ID doesn't exist
    SVSErrorCodeHouseChargesNotEnabled = 63, //House charges are disabled in current namespace
    SVSErrorCodeUnknownHCAccount = 64, //House charge account with given ID doesn't exist
    SVSErrorCodeHCSettingsCanNotBeChanged = 65, //House charge settings can't be changed, because previously were performed house charge transactions by one of HC types (maintenance required)
    SVSErrorCodeDuplicateHCInvoice = 66, //House charge invoice with given ID already exist
    SVSErrorCodeUnknownSalesReceiptPayment = 67, //Sales receipt payment with given ID doesn't exist
    SVSErrorCodeUnknownSalesReceiptCharge = 68, //Sales receipt charge with given ID doesn't exist
    SVSErrorCodeUnknownHCInvoice = 69, //House charge invoice with given ID doesn't exist
    SVSErrorCodeWrongHouseChargesType = 70, //Attempt to perform operation by not selected house charge type
    SVSErrorCodeDuplicateMailchimpList = 71, //Mailchimp list with given ID already exist
    SVSErrorCodeUnknownMailchimpList = 72, //Mailchimp list with given ID doesn't exist
    SVSErrorCodeMailchimpServiceError = 73, //Mailchimp mailing service respond error
    SVSErrorCodeMailchimpNotAvailableSvsError = 74, //Mailchimp mailing service is not available at the moment
    SVSErrorCodeDuplicateMailchimpMarketingCampaign = 75, //Mailchimp marketing campaign with given ID already exist
    SVSErrorCodeUnknownMailchimpMarketingCampaign = 76, //Mailchimp marketing campaign with given ID doesn't exist
    SVSErrorCodeMailchimpDailySyncIsAlreadyInProgress = 77, //Daily synchronization with Mailchimp is already in progress
    SVSErrorCodeSVSErrorCodeUnknownStandardInternalMailingType = 78, //Standard transactional internal mailing type with given name doesn't exist
    SVSErrorCodeReservedAsStandardInternalMailingType = 79, //Trying to create custom internal mailing type with reserved by standard internal mailing type name
    SVSErrorCodeUnknownInternalMailingType = 80, //Internal mailing type with given name doesn't exist
    SVSErrorCodeDuplicateInternalMailingType = 81, //Internal mailing type with given name already exist
    SVSErrorCodeDisabledInternalMailingType = 82, //Internal mailing type with given name is disabled
    SVSErrorCodeTokensNotEnabled = 83, //Tokens are disabled in current namespace
    SVSErrorCodeUnknownCustomerTodo = 84, //Customer to do with given ID doesn't exist
    SVSErrorCodeUnknownCustomerActivity = 85, //    Customer activity with given ID doesn't exist
    SVSErrorCodeTryOperationLater = 86, //Operation can not be done now, try again later; see error message for details
    SVSErrorCodeRetryOperation = 87, //Re-call this API function to continue processing
    SVSErrorCodeNamespaceNotScheduledForPurge = 88, //Given namespace wasn't scheduled for purge
    SVSErrorCodeNamespaceScheduledForPurge = 89, //Given namespace was scheduled for purge
    SVSErrorCodeUnknownLRPPromotionalPeriod	= 90, //Given LRP Promotional period does not exist
    SVSErrorCodeUnknownMemberLevel = 91, //Member level with specified name does not exist
    SVSErrorCodeDuplicateCustomerNo	= 92, //Customer with specified CustomerNo already exists
    SVSErrorCodeDuplicateChatMailingType = 93, //Chat mailing type with specified name already exists
    SVSErrorCodeUnknownChatMailingType	= 94, //Chat mailing type with specified name does not exist
    SVSErrorCodeUnknownChatMailingTypePreSetMessage	= 95, //Specified preset message does not exist for for specified chat mailing type
    SVSErrorCodeUnknownWebhook	= 96, //Webhook with specified ID does not exist
    SVSErrorCodeUnknownDeviceCustomCommand	= 97, //Device custom command with specified name does not exist
    SVSErrorCodeCouponNotEnabled = 98 //Coupon functions are not enabled in given namespace
};

@interface ReceiptSVSBalancesResponse : NSObject
@property (nonatomic, retain)BPUUID* receiptId;
@property (nonatomic, retain)BPUUID* customerId;
@property (nonatomic, retain)NSDecimalNumber* lrp1Balance;
@property (nonatomic, retain)NSDecimalNumber* lrp1EarnedPoints;
@property (nonatomic, retain)NSDecimalNumber* lrp1Threshold;
@property (nonatomic, retain)NSDecimalNumber* lrp2Balance;
@property (nonatomic, retain)NSDecimalNumber* lrp2EarnedPoints;
@property (nonatomic, retain)NSDecimalNumber* lrp2Threshold;
@property (nonatomic, retain)NSArray* fbBalances;
@property (nonatomic, retain)NSArray* haBalances;
@property (nonatomic, retain)NSDecimalNumber* tokenBalance;
@property (nonatomic, retain)NSDecimalNumber* tokenEarnedPoints;
@end

@interface ReceiptSVSFrequentByuerBalancesResponse : NSObject
@property (nonatomic, retain)BPUUID* programId;
@property (nonatomic, retain)NSDecimalNumber* balanceAmount;
@property (nonatomic, retain)NSDecimalNumber* rate;
@property (nonatomic, retain)NSDecimalNumber* earnedAmount;
@end

@interface ReceiptSVSHABalancesResponse : NSObject
@property (nonatomic, retain)NSString* invoiceId;
@property (nonatomic, retain)NSDecimalNumber* balanceAmount;
@property (nonatomic, retain)NSDecimalNumber* earnedAmount;
@end

@interface ReceiptSVSAuthorizeEntityResponse : NSObject
@property (nonatomic, retain)NSString* transactionId;
@property (nonatomic, retain)NSDecimalNumber* balanceAmount;
@end

@class GiftCard;
@class EmailValidation;

@interface StoredValuesServiceParser : NSObject

+(NSError *)catchManualExceptions:(NSString *)text;
+(NSString*)localizedErrorMessageByErrorCode:(SVSErrorCode)code;
+(EmailValidation*)parseValidateEmailResponse:(NSDictionary*)dictionary;
+(ReceiptSVSBalancesResponse*)parseReceiptBalances:(NSDictionary *)dictionary;
@end

