//
//  UrlDictionaryServiceRequest.h
//  ProductBuilder
//
//  Created by valera on 3/14/13.
//  Copyright (c) 2013 Cloudworks. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "StoredValuesServiceRequest.h"

@interface UrlDictionaryServiceRequest: StoredValuesServiceRequest {
}
@end
