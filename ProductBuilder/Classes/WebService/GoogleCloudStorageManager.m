//
//  GoogleCloudStorageManager.m
//  ProductBuilder
//
//  Created by Viacheslav Lulakov on 12/11/15.
//  Copyright © 2015 Cloudworks. All rights reserved.
//

#import "GoogleCloudStorageManager.h"
#import "NSData+Base64.h"
#import "NSData+MD5.h"
#import "AppSettingManager.h"
#import "Location.h"

#define MAX_RETRY_COUNT 5

const NSString * kPrivateKeyTag = @"com.cloudwk.GoogleCloudStorage.privateKey";
const NSString * kClientEmail = @"158699161478-s3a658lthulq3agb95ia80ieb59t8u39@developer.gserviceaccount.com";
const NSString * kScope = @"https://www.googleapis.com/auth/devstorage.read_write";
const NSString * kTokenUrl = @"https://www.googleapis.com/oauth2/v3/token";
const NSString * kGrantType = @"urn\%3Aietf\%3Aparams\%3Aoauth\%3Agrant-type\%3Ajwt-bearer";
const NSString * kResumableUploadUrl = @"https://www.googleapis.com/upload/storage/v1/b/twr-mobile-backup/o?uploadType=resumable&name=";

@interface GoogleCloudStorageManager() <NSURLSessionDelegate> {
    SecKeyRef _privateKey;
    NSString * _filePath;
    dispatch_semaphore_t _waitSemaphore;
    NSUInteger _retryCount;
    NSInteger _fileSize;
    NSUInteger _uploadTaskIdentifier;
    NSInteger _offset;
}

@property(nonatomic, retain) NSInputStream  * dataStream;
@property(nonatomic, retain) NSURLSession   * session;
@property(nonatomic, retain) NSURLSessionDataTask * currentTask;
@property(nonatomic, retain) NSString       * accessToken;
@property(nonatomic, retain) NSString       * resumableSessionUrl;
@property(nonatomic, retain) NSError        * lastError;

@end

@implementation GoogleCloudStorageManager

#pragma mark - Life Cycle

- (id)initWithDelegate:(id<GoogleCloudStorageManagerDelegate>)delegate {
    self = [super init];
    if (self != nil) {
        if (![self getPrivateKey]) {
            [self savePrivateKey];
        }
        _waitSemaphore = dispatch_semaphore_create(0);
        _delegate = delegate;
    }

    return self;
}

- (void)dealloc {
    if (_privateKey != nil)
    {
        CFRelease(_privateKey);
    }
    
    [_session release];
    [_accessToken release];
    [_resumableSessionUrl release];
    [_lastError release];
    [_currentTask release];
    [_dataStream release];
    [super dealloc];
}

#pragma mark - Public Methods

- (void)uploadFile:(NSString *)filePath {
    _filePath = [filePath retain];
    self.lastError = nil;
    NSString * jwt = [self createJSONWebToken];
    self.accessToken = nil;
    if ([self getAccessToken:jwt]) {
        dispatch_semaphore_wait(_waitSemaphore, DISPATCH_TIME_FOREVER);
        if (_accessToken.length > 0) {
            [self startUpload];
            dispatch_semaphore_wait(_waitSemaphore, DISPATCH_TIME_FOREVER);
        }
    }
    else {
        if (_delegate != nil && [(id)_delegate respondsToSelector:@selector(manager:completedUploadWithError:)]) {
            [_delegate manager:self completedUploadWithError:_lastError];
        }
    }
}

- (void)cancelUpload {
    [_session invalidateAndCancel];
    [_session release];
    _session = nil;
    dispatch_semaphore_signal(_waitSemaphore);
}

#pragma mark - Private Methods

// Save private key to keychain

- (BOOL)savePrivateKey {
    NSString * privateKeyFileContents = [NSString stringWithContentsOfFile:[[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"gcs.pem"] encoding:NSUTF8StringEncoding error:nil];
    NSScanner * scanner = [NSScanner scannerWithString:privateKeyFileContents];
    NSString * privateKeyString = nil;
    if ([scanner scanString:@"-----BEGIN RSA PRIVATE KEY-----" intoString:nil] ) {
        [scanner scanUpToString:@"-----END RSA PRIVATE KEY-----" intoString:&privateKeyString];
    }

    NSData * privateKeyData = [NSData dataWithBase64EncodedString:privateKeyString];
    NSData * tagData = [kPrivateKeyTag dataUsingEncoding:NSUTF8StringEncoding];
    NSMutableDictionary * keyQueryDictionary = [[NSMutableDictionary alloc] init];
    [keyQueryDictionary setObject:(__bridge id)kSecClassKey forKey:(__bridge id)kSecClass];
    [keyQueryDictionary setObject:(__bridge id)kSecAttrKeyTypeRSA forKey:(__bridge id)kSecAttrKeyType];
    [keyQueryDictionary setObject:tagData forKey:(__bridge id)kSecAttrApplicationTag];
    [keyQueryDictionary setObject:(__bridge id)kSecAttrAccessibleWhenUnlocked forKey:(__bridge id)kSecAttrAccessible];
    [keyQueryDictionary setObject:privateKeyData forKey:(__bridge id)kSecValueData];
    [keyQueryDictionary setObject:(__bridge id)kSecAttrKeyClassPrivate forKey:(__bridge id)kSecAttrKeyClass];
    [keyQueryDictionary setObject:[NSNumber numberWithBool:YES] forKey:(__bridge id)kSecReturnRef];
    OSStatus secStatus = SecItemAdd((__bridge CFDictionaryRef)keyQueryDictionary, (CFTypeRef *)&_privateKey);
    [keyQueryDictionary release];
    if (secStatus == noErr || secStatus == errSecDuplicateItem)
    {
        return NO;
    }
    CFRetain(_privateKey);
    return YES;
}

// Get private key from keychain

- (BOOL)getPrivateKey {
    NSData * tagData = [kPrivateKeyTag dataUsingEncoding:NSUTF8StringEncoding];
    NSMutableDictionary * keyQueryDictionary = [[NSMutableDictionary alloc] init];
    [keyQueryDictionary setObject:(__bridge id)kSecClassKey forKey:(__bridge id)kSecClass];
    [keyQueryDictionary setObject:(__bridge id)kSecAttrKeyTypeRSA forKey:(__bridge id)kSecAttrKeyType];
    [keyQueryDictionary setObject:tagData forKey:(__bridge id)kSecAttrApplicationTag];
    [keyQueryDictionary setObject:(__bridge id)kSecAttrAccessibleWhenUnlocked forKey:(__bridge id)kSecAttrAccessible];
    [keyQueryDictionary setObject:[NSNumber numberWithBool:YES] forKey:(__bridge id)kSecReturnRef];
    _privateKey = nil;
    OSStatus err = SecItemCopyMatching((__bridge CFDictionaryRef)keyQueryDictionary, (CFTypeRef *)&_privateKey);
    [keyQueryDictionary release];
    if (err == noErr)
    {
        CFRetain(_privateKey);
        return YES;
    }
    return NO;
}

// Create JSON Web Token

- (NSString *)createJSONWebToken {
    
    // Forming the JWT header
    
    NSDictionary * headerJSON = @{@"alg":@"RS256",@"typ":@"JWT"};
    NSData * headerData = [NSJSONSerialization dataWithJSONObject:headerJSON options:0 error:nil];
    NSString * base64Header = [headerData base64urlEncodedString];
    
    // Create Claim set
    
    NSMutableDictionary * claimSet = [[NSMutableDictionary alloc] init];
    [claimSet setObject:kClientEmail forKey:@"iss"];
    [claimSet setObject:kScope forKey:@"scope"];
    [claimSet setObject:kTokenUrl forKey:@"aud"];
    NSDate * expirationDate = [NSDate date];
    NSInteger timeInterval = [expirationDate timeIntervalSince1970];
    [claimSet setObject:[NSNumber numberWithInteger:timeInterval] forKey:@"iat"];
    timeInterval += 3600;
    [claimSet setObject:[NSNumber numberWithInteger:timeInterval] forKey:@"exp"];
    NSData * claimSetData = [NSJSONSerialization dataWithJSONObject:claimSet options:0 error:nil];
    NSString * base64ClaimSet = [claimSetData base64urlEncodedString];
    
    // Computing the signature
    
    NSString * signString = [NSString stringWithFormat:@"%@.%@", base64Header, base64ClaimSet];
    NSData * signData = [signString dataUsingEncoding:NSUTF8StringEncoding];
    NSString * base64Signature = [signData sha256WithRSA:_privateKey];
    [claimSet release];
    return [NSString stringWithFormat:@"%@.%@.%@", base64Header, base64ClaimSet, base64Signature];
}

// OAuth2 Authorization on Google Cloud Storage Server

- (NSURLRequest *)getAccessTokenRequest:(NSString *)jwt {
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:(NSString *)kTokenUrl]];
    NSString *bodyString = [NSString stringWithFormat:@"grant_type=%@&assertion=%@", kGrantType, jwt];
    [request setHTTPBody:[bodyString dataUsingEncoding:NSUTF8StringEncoding]];
    [request setHTTPMethod:@"POST"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    return [request autorelease];
}

- (BOOL)getAccessToken:(NSString *)jwt {
    
    NSURLRequest * request = [[self getAccessTokenRequest:jwt] retain];
    
    [self logRequest:request withTitle:@"Get Access Token"];
    
    self.currentTask = [self.session dataTaskWithRequest:request completionHandler:^(NSData * data, NSURLResponse * response, NSError * error) {
        
        [self logResponse:response data:data error:error withTitle:@"Get Access Token"];
        
        if (error == nil) {
            NSDictionary * responseDictionary = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            self.accessToken = [responseDictionary objectForKey:@"access_token"];
            dispatch_semaphore_signal(_waitSemaphore);
        }
        else {
            self.lastError = error;
            [self finishUpload];
        }
    }];
    [request release];
    
    if (_currentTask == nil) {
        return NO;
    }
    [_currentTask resume];
    
    return YES;
}

- (void)startUpload {
    
    NSDateFormatter * dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyyMMddHHmmss"];
    NSString * dateString = [dateFormatter stringFromDate:[NSDate date]];
    [dateFormatter release];
    NSString * storageFilePath = [NSString stringWithFormat:@"%@/%@/%@_%@_%@_%@", [AppSettingManager instance].defaultServerCode, [Location localLocation].locationCode, APP_NAME, [AppSettingManager instance].iPadId, dateString, [_filePath lastPathComponent]];
    NSDictionary * attrs = [[NSFileManager defaultManager] attributesOfItemAtPath:_filePath error:nil];
    _fileSize = [attrs[NSFileSize] integerValue];
    
    NSString * urlString = [NSString stringWithFormat:@"%@%@", kResumableUploadUrl, [storageFilePath stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLHostAllowedCharacterSet]]];
    NSMutableURLRequest * request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:urlString]];
    [request setHTTPMethod:@"POST"];
    [request setValue:[NSString stringWithFormat:@"Bearer %@", _accessToken] forHTTPHeaderField:@"Authorization"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:@"0" forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"binary/octet-stream" forHTTPHeaderField:@"X-Upload-Content-Type"];
    [request setValue:[NSString stringWithFormat:@"%li", (long)_fileSize] forHTTPHeaderField:@"X-Upload-Content-Length"];
    
    [self logRequest:request withTitle:@"Start resumable session"];

    self.currentTask = [self.session dataTaskWithRequest:request completionHandler:^(NSData * data, NSURLResponse * response, NSError * error) {
        
        [self logResponse:response data:data error:error withTitle:@"Start resumable session"];
        
        if (error == nil) {
            self.resumableSessionUrl = [((NSHTTPURLResponse*)response).allHeaderFields objectForKey:@"Location"];
            [self uploadFileFromOffset:0];
        }
        else {
            self.lastError = error;
            [self finishUpload];
        }
    }];
    [request release];
    [self.currentTask resume];
}

- (void)finishUpload {
    if (_delegate != nil && [(id)_delegate respondsToSelector:@selector(manager:completedUploadWithError:)]) {
        [_delegate manager:self completedUploadWithError:_lastError];
    }
    dispatch_semaphore_signal(_waitSemaphore);
}

- (void)resume {
    
    NSMutableURLRequest * request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:_resumableSessionUrl]];
    [request setHTTPMethod:@"PUT"];
    [request setValue:[NSString stringWithFormat:@"Bearer %@", _accessToken] forHTTPHeaderField:@"Authorization"];
    [request setValue:@"0" forHTTPHeaderField:@"Content-Length"];
    [request setValue:[NSString stringWithFormat:@"bytes */%li", (long)_fileSize] forHTTPHeaderField:@"Content-Range"];
    
    [self logRequest:request withTitle:@"Get upload status"];
    
    self.currentTask = [self.session dataTaskWithRequest:request completionHandler:^(NSData * data, NSURLResponse * response, NSError * error) {
        
        [self logResponse:response data:data error:error withTitle:@"Get upload status"];
        
        if (error != nil) {
            self.lastError = error;
            if (_retryCount < 5) {
                NSTimeInterval timeInterval = pow(2.0, _retryCount) + (double)random() / LONG_MAX;
                [self performSelector:@selector(resume) withObject:nil afterDelay:timeInterval];
                _retryCount++;
                return;
            }
            [self finishUpload];
            return;
        }

        NSHTTPURLResponse * httpResponse = (NSHTTPURLResponse *)response;
        if (httpResponse.statusCode == 308) {
            BOOL rezult = NO;
            NSInteger stopPos = 0;
            NSString * range = [httpResponse.allHeaderFields objectForKey:@"Range"];
            if (range.length > 0) {
                NSScanner * scanner = [NSScanner scannerWithString:range];
                rezult = [scanner scanString:@"bytes=" intoString:nil];
                if (rezult) {
                    rezult = [scanner scanInteger:nil];
                    if (rezult) {
                        rezult = [scanner scanString:@"-" intoString:nil];
                        if (rezult) {
                            rezult = [scanner scanInteger:&stopPos];
                        }
                    }
                }
            }
            if (rezult) {
                [self uploadFileFromOffset:stopPos + 1];
                return;
            }
            else {
                if (_retryCount < 5) {
                    NSTimeInterval timeInterval = pow(2.0, _retryCount) + (double)random() / LONG_MAX;
                    [self performSelector:@selector(resume) withObject:nil afterDelay:timeInterval];
                    _retryCount++;
                    return;
                }
            }
        }
        else if (httpResponse.statusCode == 200 || httpResponse.statusCode == 201) {
            self.lastError = nil;
        }
        [self finishUpload];
    }];
    [request release];
    [_currentTask resume];
}

- (void)uploadFileFromOffset:(NSInteger)offset {
    _retryCount = 0;

    self.dataStream = [NSInputStream inputStreamWithFileAtPath:_filePath];
    if (_dataStream == nil) {
        [self finishUpload];
        return;
    }
    _offset = offset;
    [_dataStream setProperty:[NSNumber numberWithInteger:offset] forKey:NSStreamFileCurrentOffsetKey];
    NSMutableURLRequest * request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:_resumableSessionUrl]];
    [request setHTTPMethod:@"PUT"];
    [request setValue:[NSString stringWithFormat:@"Bearer %@", _accessToken] forHTTPHeaderField:@"Authorization"];
    [request setValue:@"binary/octet-stream" forHTTPHeaderField:@"Content-Type"];
    [request setValue:[NSString stringWithFormat:@"%li", (long)(_fileSize - offset)] forHTTPHeaderField:@"Content-Length"];
    [request setValue:[NSString stringWithFormat:@"bytes %li-%li/%li", (long)offset, (long)(_fileSize - 1), (long)_fileSize] forHTTPHeaderField:@"Content-Range"];
    [request setHTTPBodyStream:_dataStream];
    
    [self logRequest:request withTitle:@"Resumable upload"];
    
    self.currentTask = [self.session dataTaskWithRequest:request completionHandler:^(NSData * data, NSURLResponse * response, NSError * error) {
        
        [self logResponse:response data:data error:error withTitle:@"Resumable upload"];
        
        if (error != nil) {
            if (_retryCount < 5) {
                NSTimeInterval timeInterval = pow(2.0, _retryCount) + (double)random() / LONG_MAX;
                [self performSelector:@selector(resume) withObject:nil afterDelay:timeInterval];
                _retryCount++;
                return;
            }
        }
        self.lastError = error;
        [self finishUpload];
    }];
    [request release];
    _uploadTaskIdentifier = _currentTask.taskIdentifier;
    [_currentTask resume];
}

#pragma mark - Logging

- (void)logRequest:(NSURLRequest *)request withTitle:(NSString *)title {
    if (_logEnable) {
        NSLog(@"*** %@. Request URL: %@", title, request.URL);
        NSLog(@"*** %@. Request headers: \n%@", title, request.allHTTPHeaderFields.description);
        if (request.HTTPBody.length > 0) {
            NSString * bodyString = [[NSString alloc] initWithData:request.HTTPBody encoding:NSUTF8StringEncoding];
            NSLog(@"*** %@. Request body:\n%@", title, bodyString);
            [bodyString release];
        }
    }
}

- (void)logResponse:(NSURLResponse *)response data:(NSData *)data error:(NSError *)error withTitle:(NSString *)title {
    if (_logEnable) {
        NSHTTPURLResponse * httpResponse = (NSHTTPURLResponse *)response;
        NSLog(@"*** %@. Response status: %li", title, (long)httpResponse.statusCode);
        NSLog(@"*** %@. Response headers: %@", title, httpResponse.allHeaderFields.description);
        if (data.length > 0) {
            NSString * responseString = [[[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding] autorelease];
            NSLog(@"*** %@. Received data:\n%@", title, responseString);
        }
        if (error != nil) {
            NSLog(@"*** %@. Error: Code = %li. %@", title, (long)error.code, error.localizedDescription);
        }
    }
}

#pragma mark - Properties

- (NSURLSession *)session {
    if (_session == nil) {
        NSURLSessionConfiguration * configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
        _session = [[NSURLSession sessionWithConfiguration:configuration delegate:self delegateQueue:[NSOperationQueue mainQueue]] retain];
    }
    return _session;
}

#pragma mark - NSURLSessionTaskDelegate methods

- (void)URLSession:(NSURLSession *)session task:(NSURLSessionTask *)task
   didSendBodyData:(int64_t)bytesSent
    totalBytesSent:(int64_t)totalBytesSent
totalBytesExpectedToSend:(int64_t)totalBytesExpectedToSend {
    if (_logEnable) {
        NSLog(@"\ndidSendBodyData: send:%lli totalSend: %lli, expectedToSend: %lli", bytesSent, totalBytesSent, totalBytesExpectedToSend);
    }
    if (task.taskIdentifier == _uploadTaskIdentifier) {
        if (_delegate != nil) {
            if ([(id)_delegate respondsToSelector:@selector(manager:uploadProgress:)]){
                float progress = (float)(totalBytesSent + _offset) / (float)_fileSize;
                [_delegate manager:self uploadProgress:progress];
            }
        }
    }
}

@end
