//
//  XmlConvert.h
//  ProductBuilder
//
//  Created by Valery Fomenko on 3/23/12.
//  Copyright (c) 2012 Cloudworks. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface XmlConvert : NSObject
+(NSString*)decodeName:(NSString*)name;
+(NSString*)encodeName:(NSString*) name;
+(NSString*)decodeNameFromUTF8String:(const char*)name;
@end
