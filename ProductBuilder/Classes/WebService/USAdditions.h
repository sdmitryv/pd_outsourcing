//
//  USAdditions.h
//  WSDLParser
//
//  Created by John Ogle on 9/5/08.
//  Copyright 2008 LightSPEED Technologies. All rights reserved.
//  Modified by Matthew Faupel on 2009-05-06 to use NSDate instead of NSCalendarDate (for iPhone compatibility).
//  Modifications copyright (c) 2009 Micropraxis Ltd.
//  NSData (MBBase64) category taken from "MiloBird" at http://www.cocoadev.com/index.pl?BaseSixtyFour
//

#import <Foundation/Foundation.h>
#import <libxml/tree.h>
#import "NSNamedMutableArray.h"
#import "BPUUID.h"

@class XmlElement;

NSString* makeElementName(NSString * elName, NSString * elNSPrefix);

@interface NSString (USAdditions)
- (NSString*)stringByEscapingForURLArgument;
- (NSString*)stringByUnescapingFromURLArgument;
- (NSString *)stringByEscapingXML;
//- (NSString *)stringByUnescapingXML;
- (const xmlChar *)xmlString;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
+ (NSString *)deserializeNode:(xmlNodePtr)cur;
+ (NSString *)deserializeAttribute:(xmlAttrPtr)attr;

@end

@interface BPUUID (USAdditions)
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
@end

@interface NSNumber (USAdditions)

- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
+ (NSNumber *)deserializeNode:(xmlNodePtr)cur;

@end

@interface NSDate (USAdditions)

- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
+ (NSDate *)deserializeNode:(xmlNodePtr)cur;

@end

@interface NSData (USAdditions)

- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
+ (NSData *)deserializeNode:(xmlNodePtr)cur;

@end

@interface NSArray (USAdditions)

- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;

@end


@interface NSNamedMutableArray (USAdditions)

- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;

@end

@interface NSDictionary (USAdditions)

- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;

@end

@interface NSData (MBBase64)

+ (id)dataWithBase64EncodedString:(NSString *)string;     //  Padding '=' characters are optional. Whitespace is ignored.
- (NSString *)base64Encoding;
@end

@interface USBoolean : NSObject {
	BOOL value;
}

@property (assign) BOOL boolValue;

- (id)initWithBool:(BOOL)aValue;
- (NSString *)stringValue;

- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
+ (USBoolean *)deserializeNode:(xmlNodePtr)cur;

@end

@interface SOAPFault : NSObject {
    NSString *faultcode;
    NSString *faultsubcode;
    NSString *faultstring;
    NSString *faultactor;
    NSString *detail;
    XmlElement* detailElement;
}

@property (nonatomic, retain) NSString *faultcode;
@property (nonatomic, retain) NSString *faultsubcode;
@property (nonatomic, retain) NSString *faultstring;
@property (nonatomic, retain) NSString *faultactor;
@property (nonatomic, retain) NSString *detail;
@property (nonatomic, retain) XmlElement *detailElement;
@property (readonly) NSString *simpleFaultString;

+ (SOAPFault *)deserializeNode:(xmlNodePtr)cur;
+ (SOAPFault *)deserializeFromXmlElement_11:(XmlElement*)element;
+ (SOAPFault *)deserializeFromXmlElement_12:(XmlElement*)element;

@end

