//
//  RealTimeQtyService.m
//  ProductBuilder
//
//  Created by Lulakov Viacheslav on 1/3/13.
//  Copyright (c) 2013 Cloudworks. All rights reserved.
//

#import "RealTimeQtyService.h"
#import "DecimalHelper.h"
#import "SettingManager.h"
#import "NSURLSession+Extentions.h"
#import "GAELog.h"
#import "AppParameterManager.h"

#define DEFAULT_TIMEOUT     240

static RealTimeQtyService * realTimeQtyService;

@implementation RealTimeQtyServiceRequest

@synthesize requestType;
@synthesize parameters;
@synthesize returnClass;
@synthesize delegate;
@synthesize error;
@synthesize result;
@synthesize serviceURL;
@synthesize defaultTimeout;
@synthesize logEnable;
@synthesize cookies;
@synthesize accountUsername;
@synthesize accountPassword;
@synthesize authValue;
@synthesize httpResponse;
@synthesize responseData;

-(id)initWithType:(RealTimeQtyServiceRequestType)type url:(NSURL *)url params:(NSMutableDictionary *)params delegate:(id<RealTimeQtyServiceDelegate>) aDelegate {
    self = [super init];
    if (self != nil) {
        self.requestType = type;
        self.parameters = params;
        self.delegate = aDelegate;
        self.serviceURL = url;
        defaultTimeout = DEFAULT_TIMEOUT;
        logEnable = [AppParameterManager instance].enableLogs;
        waitForRequestSemaphore = dispatch_semaphore_create(0);
    }
    return self;
}

-(void)dealloc {
    [self cancel];
	[parameters release];
    [error release];
    [result release];
    [_sessionTask release];
    [cookies release];
    [accountPassword release];
    [accountUsername release];
    [authValue release];
    [httpResponse release];
    [responseData release];
    if (waitForRequestSemaphore)
        dispatch_release(waitForRequestSemaphore);
	[super dealloc];
}

-(NSURLSessionTask *)execURLRequest:(NSURL *)url withBody:(NSData *)body handleResults:(BOOL)handleResults{
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData
                                                       timeoutInterval:defaultTimeout];
    
    if(cookies != nil) {
        [request setAllHTTPHeaderFields:cookies];
    }
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:[NSString stringWithFormat:@"%lu", (unsigned long)[body length]] forHTTPHeaderField:@"Content-Length"];
    [request setValue:url.host forHTTPHeaderField:@"Host"];
    [request setHTTPMethod: @"POST"];
    [request setHTTPBody: body];
    if(self.logEnable) {
        NSLog(@"\nOutputPath:\n%@", [request.URL absoluteString]);
        NSLog(@"OutputHeaders:\n%@", [request allHTTPHeaderFields]);
        NSString * bodyText = [[NSString alloc] initWithData:body encoding:NSUTF8StringEncoding];
        NSLog(@"OutputBody:\n%@", bodyText);
        [bodyText release];
    }
    
    self.responseData = nil;
    NSURLSessionConfiguration* sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
    sessionConfig.timeoutIntervalForRequest = self.defaultTimeout;
    NSOperationQueue* delegateQueue = [[[NSOperationQueue alloc] init]autorelease];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:sessionConfig delegate:self delegateQueue:delegateQueue];
    
    __block __typeof(self) selfCopy = self;
    NSURLSessionDataTask* task = [session dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable anError) {
        
        @try {
            
            if (anError.code == NSURLErrorCancelled){
                
                [session invalidateAndCancel];
                return;
            }
            
            [session finishTasksAndInvalidate];
            selfCopy.responseData = data;
            selfCopy.httpResponse = [response isKindOfClass:[NSHTTPURLResponse class]] ? (NSHTTPURLResponse *)response : nil;
            
            if(logEnable) {
                NSLog(@"\nResponsePath: %@\n", [httpResponse.URL absoluteString]);
                NSLog(@"ResponseStatus: %ld\n", (long)[httpResponse statusCode]);
                NSLog(@"ResponseHeaders:\n%@", [httpResponse allHeaderFields]);
            }
            selfCopy.error = anError;
            if (handleResults){
                
                [selfCopy handleResults];
            }
        } @finally {
            
             dispatch_semaphore_signal(waitForRequestSemaphore);
        }
    }];
    
    [task resume];
    return task;
}

-(void)execWithHandlingResults:(BOOL)handleResults {
    NSData *bodyData = [NSJSONSerialization dataWithJSONObject:parameters options:NSJSONWritingPrettyPrinted error:nil];
    self.error = nil;
    self.sessionTask = [self execURLRequest:self.serviceURL withBody:bodyData handleResults:handleResults];
}

-(void)handleResults{
    
    if (!self.error){
        
        if (self.httpResponse.statusCode >= 400) {
            
            [self handleError:[NSError errorWithDomain:@"RealTimeQtyService" code:[httpResponse statusCode] userInfo:@{NSLocalizedDescriptionKey: [NSHTTPURLResponse localizedStringForStatusCode:[httpResponse statusCode]]}]];
        }
        else{
            
            if (logEnable && self.responseData) {
                
                NSLog(@"ResponseBody:\n%@", [[[NSString alloc] initWithData:self.responseData encoding:NSUTF8StringEncoding] autorelease]);
            }
            
            [self answerToRequest];
        }
    }
    else{
        
        [self handleError:self.error];
    }
}

-(void)exec{
    [self execWithHandlingResults:YES];
}

-(void)execSync{
    [self execWithHandlingResults:NO];
    dispatch_semaphore_wait(waitForRequestSemaphore, DISPATCH_TIME_FOREVER);
    [self handleResults];
}

-(void)cancel{
    [self.sessionTask cancel];
}

-(void)reportRequestWithResult:(NSObject*)aResult{
    self.result = aResult;
    if (self.delegate != nil && [self.delegate respondsToSelector:@selector(realTimeQtyServiceDidCompleteRequest:withResult:)]) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.delegate realTimeQtyServiceDidCompleteRequest:self withResult:aResult];
        });
    }
}

-(void)reportRequestWithError:(NSError*)anError{
    
    self.error = anError;
    
    [GAELog logfWithType:GAELogTypeError
                    area:@"RealTimeQtyService"
                 message:self.error.description
              stackTrace:[[[NSString alloc] initWithData:self.responseData encoding:NSUTF8StringEncoding] autorelease]];
    
    if (self.delegate != nil && [self.delegate respondsToSelector:@selector(realTimeQtyServiceDidCompleteRequest:withError:)]) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [self.delegate realTimeQtyServiceDidCompleteRequest:self withError:error];
        });
    }
}

-(void)answerToRequest {
    NSError * anError = nil;
    self.result = nil;
    if (responseData != nil) {
        NSString * dirtyText = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
        NSString * text = [dirtyText stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"\n\r ;"]];
        [dirtyText release];
        anError = [self catchManualExceptions:text];
        if (anError != nil) {
            [self reportRequestWithError:anError];
            return;
        }
        id object = [NSJSONSerialization JSONObjectWithData:[text dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingAllowFragments error:&anError];
        if (!anError) {
            if (![object isKindOfClass:[NSDictionary class]]) {
                NSDictionary *userInfo = @{NSLocalizedDescriptionKey: @"Enexpected response type"};
                anError = [NSError errorWithDomain:@"RealTimeQtyService" code:100 userInfo:userInfo];
            }
            else {
                NSDictionary * dictionary = (NSDictionary*)object;
                if (self.requestType == RealTimeQtyServiceQtyRequest) {
                    self.result = [self.class parseQtyResponse:dictionary];
                }
            }
        }
        else{
            if (logEnable) {
                NSLog(@"JSon Parser Error:\n%@\n", error);
            }
            [self reportRequestWithError:anError];
            return;
        }
    }
    [self reportRequestWithResult:result];
}


-(NSError *)catchManualExceptions:(NSString *)text {
    NSError *anError = nil;
    if ([text hasPrefix:@"Exception:"]) {
        NSArray * components = [text componentsSeparatedByString:@";"];
        NSDictionary *userInfo = @{NSLocalizedDescriptionKey: components[1]};
        int errorCode = [[components[0] stringByReplacingOccurrencesOfString:@"Exception:" withString:@""] intValue];
        anError = [NSError errorWithDomain:@"RealTimeQtyService" code:errorCode userInfo:userInfo];
    }
    return anError;
}

+(NSMutableDictionary *)parseQtyResponse:(NSDictionary *)dictionary {
    NSMutableDictionary * quantities = [[[NSMutableDictionary alloc] init] autorelease];
    NSArray *itemArray = dictionary[@"itemQuantities"];
    if (itemArray != nil) {
        for (NSDictionary * qtyDictionary in itemArray) {
            BPUUID * itemId = [BPUUID UUIDWithString:qtyDictionary[@"itemId"]];
            NSArray * qtyArray = qtyDictionary[@"quantities"];
            NSMutableDictionary * locationQty = [[NSMutableDictionary alloc] init];
            for (NSDictionary * locationDictionary in qtyArray) {
                NSNumber * valid = locationDictionary[@"valid"];
                
                BPUUID * locationId = [BPUUID UUIDWithString:locationDictionary[@"locationId"]];
                NSDecimalNumber *onHand = [valid boolValue] ? [self qtyFromObj:locationDictionary[@"onHand"]] : nil;
                NSDecimalNumber *committed = [self qtyFromObj:locationDictionary[@"committed"]];
                NSDecimalNumber *available = [self qtyFromObj:locationDictionary[@"available"]];
                NSDecimalNumber *incoming = [self qtyFromObj:locationDictionary[@"incoming"]];
                
                NSMutableDictionary *qtyDic = [[NSMutableDictionary alloc] init];
                
                if (onHand)
                    qtyDic[@"onHand"] = onHand;
                if (committed)
                    qtyDic[@"committed"] = committed;
                if (available)
                    qtyDic[@"available"] = available;
                if (incoming)
                    qtyDic[@"incoming"] = incoming;
                
                locationQty[locationId] = qtyDic;
                [qtyDic release];
            }
            quantities[itemId] = locationQty;
            [locationQty release];
        }
    }
    return quantities;
}


+(NSDecimalNumber *)qtyFromObj:(NSObject *)obj {
    
    if ([obj isKindOfClass:[NSString class]])
        return [NSDecimalNumber decimalNumberWithString:(NSString *)obj];
    
    if ([obj isKindOfClass:[NSDecimalNumber class]])
        return (NSDecimalNumber *)obj;
    
    if ([obj isKindOfClass:[NSNumber class]])
        return [NSDecimalNumber decimalNumberWithDecimal:CPDecimalFromDouble([((NSNumber *)obj) doubleValue])];
    
    return nil;
}


#pragma mark NSURLSessionDelegate
- (void)URLSession:(NSURLSession *)session didReceiveChallenge:(NSURLAuthenticationChallenge *)challenge
 completionHandler:(void (^)(NSURLSessionAuthChallengeDisposition disposition, NSURLCredential * __nullable credential))completionHandler{
    
    [session defaultHandlerForDidReceiveChallenge:challenge completionHandler:completionHandler userName:self.accountUsername password:self.accountPassword errorHandler:^(NSError* authError){ [self handleError:authError]; }];
}

-(void)handleError:(NSError*)anError{
    if (anError.code==NSURLErrorCancelled) return;
    if (logEnable) {
        NSLog(@"ResponseError:\n%@\n", anError);
    }
    [self reportRequestWithError:anError];
}

@end

@interface RealTimeQtyService()

@end

@implementation RealTimeQtyService

-(id)init {
    SettingManager * settingManager = [SettingManager instance];
    return [self initWithUsername:[settingManager svsAccountUserName] password:[settingManager svsAccountPassword]];
}

-(id)initWithUsername:(NSString *)name password:(NSString *)password {
    self = [super init];
	if(self != nil) {
        rtqServiceURL = [[NSURL alloc] initWithString:[[SettingManager instance] rtqServiceURL]];
        merchantId = [[[SettingManager instance] receiptTrackingMerchantId] retain];
        itemsQuantityURL = [[NSURL alloc] initWithString:[NSString stringWithFormat:@"/rest/v1/%@/itemsavailability/batchquantity", merchantId] relativeToURL:rtqServiceURL];
	}
	return self;
}

-(void)dealloc {
    [rtqServiceURL release];
    [itemsQuantityURL release];
    [merchantId release];
	
	[super dealloc];
}

+ (instancetype)sharedInstance {
    if (realTimeQtyService == nil) {
        realTimeQtyService = [[self.class alloc] init];
    }
    return realTimeQtyService;
}

+(void)reset {
    if (realTimeQtyService != nil) {
        [realTimeQtyService release];
        realTimeQtyService = nil;
    }
}

#pragma mark - Private method

-(RealTimeQtyServiceRequest *)qtyForItems:(NSArray *)items locations:(NSArray *)locations delegate:(id<RealTimeQtyServiceDelegate>)delegate{
    NSMutableDictionary * params = [[NSMutableDictionary alloc] init];
    [params setValue:items forKey:@"items"];
    [params setValue:locations forKey:@"locations"];
    RealTimeQtyServiceRequest * request = [[RealTimeQtyServiceRequest alloc] initWithType:RealTimeQtyServiceQtyRequest url:itemsQuantityURL params:params delegate:delegate];
    [params release];
    request.returnClass = nil;
    return [request autorelease];
}

-(RealTimeQtyServiceRequest *)syncQtyForItems:(NSArray *)items locations:(NSArray *)locations {
    RealTimeQtyServiceRequest * request = [self qtyForItems:items
                                                  locations:locations
                                                   delegate:nil];
    [request execSync];
    return request;
}

@end
