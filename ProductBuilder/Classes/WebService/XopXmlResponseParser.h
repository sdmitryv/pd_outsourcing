//
//  XopXmlResponseParser.h
//  ProductBuilder
//
//  Created by valera on 6/3/13.
//  Copyright (c) 2013 Cloudworks. All rights reserved.
//

#import <Foundation/Foundation.h>
//#import "CCMultipartReader.h"
#import "RTProxyStream.h"

typedef NS_ENUM(NSUInteger, MIMEType){ MIMETypeUnknown = 0,  MIMETypeXOPXml = 1, MIMETypeBinary = 2 };

@protocol XopXmlResponseParserDelegate;

@interface XopXmlResponseParser : NSObject{//<CCMultipartReaderDelegate>
    BOOL _closed;
}
-(id)initWithResponseName:(NSString*)aResponseName headers:(NSDictionary*)headers error:(NSError**)error;
-(void)parseChunk:(NSData*)data;
-(void)finish;
@property (nonatomic, assign)id<XopXmlResponseParserDelegate>delegate;
@end

@protocol XopXmlResponseParserDelegate<NSObject>

-(void)xopXmlResponseParser:(XopXmlResponseParser *)parser didReceiveBinaryStream:(NSInputStream*)stream;
-(void)xopXmlResponseParser:(XopXmlResponseParser *)parser parsingError:(NSError*)error;

@end
