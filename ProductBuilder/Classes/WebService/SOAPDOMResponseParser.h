//
//  SOAPDOMResponseParser.h
//  ProductBuilder
//
//  Created by valera on 6/4/13.
//  Copyright (c) 2013 Cloudworks. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WebServiceResponse.h"

@interface SOAPDOMResponseParser : NSObject

-(id)initWithResponseName:(NSString*)responseName resultName:(NSString*)resultName returnClass:(Class) returnClass;
-(BOOL)parse:(NSData*)responseData response:(WebServiceResponse*)response;
@end
