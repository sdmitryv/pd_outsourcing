//
//  RealTimeQtyService.h
//  ProductBuilder
//
//  Created by Lulakov Viacheslav on 1/3/13.
//  Copyright (c) 2013 Cloudworks. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BPUUID.h"

typedef NS_ENUM(NSUInteger, RealTimeQtyServiceRequestType) {
    RealTimeQtyServiceQtyRequest = 1
};

@class RealTimeQtyServiceRequest;
@class RealTimeQtyService;

@protocol RealTimeQtyServiceDelegate <NSObject>

@optional

- (void)realTimeQtyServiceDidCompleteRequest:(RealTimeQtyServiceRequest *)request withResult:(NSObject *)result;
- (void)realTimeQtyServiceDidCompleteRequest:(RealTimeQtyServiceRequest *)request withError:(NSError *)error;

@end

@interface RealTimeQtyServiceRequest: NSObject<NSURLSessionDataDelegate> {
    RealTimeQtyServiceRequestType requestType;
    NSMutableDictionary * parameters;
    Class returnClass;
	id<RealTimeQtyServiceDelegate> delegate;
    NSError * error;
    NSObject * result;
    NSURL * serviceURL;
    NSTimeInterval defaultTimeout;
    NSDictionary * cookies;
    BOOL logEnable;
    NSString * accountUsername;
    NSString * accountPassword;
    NSString * authValue;
    NSHTTPURLResponse * httpResponse;
    NSMutableData * responseData;
    dispatch_semaphore_t waitForRequestSemaphore;
}

@property (nonatomic, assign) RealTimeQtyServiceRequestType requestType;
@property (nonatomic, retain) NSMutableDictionary * parameters;
@property (nonatomic, assign) Class returnClass;
@property (nonatomic, assign) id<RealTimeQtyServiceDelegate> delegate;
@property (nonatomic, retain) NSError * error;
@property (nonatomic, retain) NSObject * result;
@property (atomic, retain) NSURLSessionTask * sessionTask;
@property (nonatomic, assign) NSURL * serviceURL;
@property (nonatomic, assign) NSTimeInterval defaultTimeout;
@property (nonatomic, retain) NSDictionary * cookies;
@property (nonatomic, assign) BOOL logEnable;
@property (nonatomic, retain) NSString * accountUsername;
@property (nonatomic, retain) NSString * accountPassword;
@property (nonatomic, retain) NSString * authValue;
@property (nonatomic, retain) NSHTTPURLResponse * httpResponse;
@property (nonatomic, retain) NSData * responseData;

-(id)initWithType:(RealTimeQtyServiceRequestType)type url:(NSURL *)url params:(NSMutableDictionary *)params delegate:(id<RealTimeQtyServiceDelegate>) delegate;
-(void)cancel;
@end

@interface RealTimeQtyService : NSObject {
    NSURL * rtqServiceURL;
    NSURL * itemsQuantityURL;
    NSString * merchantId;
}

-(id)initWithUsername:(NSString *)name password:(NSString *)password;

-(RealTimeQtyServiceRequest *)qtyForItems:(NSArray *)items locations:(NSArray *)locations delegate:(id<RealTimeQtyServiceDelegate>)delegate;
-(RealTimeQtyServiceRequest *)syncQtyForItems:(NSArray *)items locations:(NSArray *)locations;

+ (RealTimeQtyService *)sharedInstance;
+(void)reset;
@end
