//
//  SOAPResponseParser.h
//  ProductBuilder
//
//  Created by valera on 12/2/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

#import <libxml/parser.h>
#import <libxml/xmlstring.h>
#import "WebServiceResponse.h"

// define a struct to hold the attribute info
struct _xmlSAX2Attributes {
    const xmlChar* localname;
    const xmlChar* prefix;
    const xmlChar* uri;
    const xmlChar* value;
    const xmlChar* end;
};
typedef struct _xmlSAX2Attributes xmlSAX2Attributes;

@class SOAPResponseParser;

@protocol SOAPResponseParserDelegate <NSObject>
-(void)parser:(SOAPResponseParser *)parser didReceiveRecord:(XmlElement*)object;
-(void)parser:(SOAPResponseParser *)parser parsingError:(NSError*)error;
@end

typedef NS_ENUM(NSUInteger, SOAPActionParserState) {
    ACTION_START,
    ACTION_INSIDE_ENVELOPE,
    ACTION_INSIDE_BODY,
    ACTION_INSIDE_RESPONSE,
    ACTION_INSIDE_OBJECT_RECORD,
    ACTION_INSIDE_SOAP_FAULT,
    ACTION_FINISH,
};

@interface SOAPResponseParser : NSObject{
    id<SOAPResponseParserDelegate> delegate;
    //NSString* resultName;
    xmlSAXHandler saxHandler;
    NSString* responseName;
    NSUInteger responseElementNameLength;
    xmlParserCtxtPtr _xmlParserContext;
    SOAPActionParserState _state;
    //NSString* bodyNodePrefix;
    NSString* pathToObjectRecord;
    XmlElement* objectRootRecord;
    XmlElement* objectRecord;
    NSInteger levelFromObjectRecord;
    NSInteger levelFromResponse;
    NSInteger levelFromBody;
    NSInteger levelFromEnvelope;
    NSMutableArray* pathToObjectRecordArray;
    NSUInteger pathToObjectRecordIndex;
    SOAPFault* soapFault;
    NSString * soapFaultURI;
}

@property (nonatomic, retain)NSString* pathToObjectRecord;
@property (nonatomic, assign) id<SOAPResponseParserDelegate> delegate;
@property (nonatomic, readonly) SOAPFault* soapFault;
-(id)initWithResponseName:(NSString*)aResponseName;
-(void)parseChunk:(NSData*)data;
-(void)finish;
-(void)cancel;
@end
