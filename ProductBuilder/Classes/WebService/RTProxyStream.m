//
//  RTProxyStream.m
//  ProductBuilder
//
//  Created by valera on 6/4/13.
//  Copyright (c) 2013 Cloudworks. All rights reserved.
//

#import "RTProxyStream.h"

@interface RTProxyStream(){
    NSUInteger tail;
    dispatch_semaphore_t semaphoreSafeToRead;
    NSStreamStatus status;
    NSConditionLock* lock;
    const char* _data;
    size_t _length;
    dispatch_queue_t _readQueue;
    dispatch_queue_t _writeQueue;
}
@property (nonatomic, readonly)NSUInteger length;
@property (nonatomic, readonly)const char* data;
@end


@implementation RTProxyStream

-(id)init{
    if ((self=[super init])){
        semaphoreSafeToRead = dispatch_semaphore_create(0);
        lock = [[NSConditionLock alloc]initWithCondition:0];
        _writeQueue = dispatch_queue_create("com.RTProxy.writeQueue", DISPATCH_QUEUE_SERIAL);
        _readQueue = dispatch_queue_create("com.RTProxy.readQueue", DISPATCH_QUEUE_SERIAL);
    }
    return self;
}

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wobjc-designated-initializers"

-(id)initWithData:(char*)value length:(size_t)length{
    if ((self=[self init])){
        [self setData:value length:length];
    }
    return self;
}

#pragma clang diagnostic pop

-(void)dealloc{
    [self close];
    dispatch_sync(_readQueue, ^{});
    dispatch_sync(_writeQueue, ^{});
    [lock release];
    dispatch_release(semaphoreSafeToRead);
    dispatch_release(_readQueue);
    dispatch_release(_writeQueue);
    [super dealloc];
}

-(NSUInteger)length{
    NSInteger result = _length - tail;
    return result < 0 ? 0 : result;
}

-(void)setData:(const char *)value length:(size_t)length{
    tail = 0;
    _data = value;
    _length = length;
}

-(const char*)data{
    return _data;
}

- (NSInteger)read:(uint8_t *)buffer maxLength:(NSUInteger)maxLength{
    __block NSUInteger resultLen = 0;
    dispatch_sync(_readQueue , ^{
        resultLen = [self readInternal:buffer maxLength:maxLength];
    });
    return resultLen;
}

- (NSInteger)readInternal:(uint8_t *)buffer maxLength:(NSUInteger)maxLength{
        if (status!=NSStreamStatusOpen){
            return 0;
        }
        dispatch_semaphore_wait(semaphoreSafeToRead, DISPATCH_TIME_FOREVER);
        [lock lock];
            if (status==NSStreamStatusClosed || !self.length){
                [lock unlockWithCondition:0];
                return 0;
            }
            NSUInteger resultLen = 0;
            //read data that currently exists
            NSUInteger length = self.length;
            resultLen = length >= maxLength ? maxLength : length;
            if (resultLen){
                memcpy(buffer, _data + tail, resultLen);
                tail+=resultLen;
            }
            if (self.length)
                dispatch_semaphore_signal(semaphoreSafeToRead);
        [lock unlockWithCondition:self.length ? 1 : 0];
        if(status!=NSStreamStatusClosed && maxLength > resultLen){
            buffer+=resultLen;
            resultLen +=[self readInternal:buffer maxLength:maxLength - resultLen];
        }
        return resultLen;
}

- (BOOL)getBuffer:(uint8_t **)buffer length:(NSUInteger *)len{
    return NO;
}

- (BOOL)hasBytesAvailable{
    return status==NSStreamStatusOpen;
}

//appends data and waits until it will be completely read
-(void)append:(const char*)chunk length:(size_t) length{
    if (status!=NSStreamStatusOpen || !length){
        return;
    }
    dispatch_sync(_writeQueue , ^{
          if (status!=NSStreamStatusOpen || !length){
              return;
          }
          [lock lock];
          if (status==NSStreamStatusOpen){
              [self setData:chunk length:length];
              if (length){
                  dispatch_semaphore_signal(semaphoreSafeToRead);
                  [lock unlockWithCondition:1];
                  if (status!=NSStreamStatusOpen) return;
                  [lock lockWhenCondition:0];
              }
              [self setData:NULL length:0];
          }
          [lock unlockWithCondition:0];
    });
}

- (void)open{
    status = NSStreamStatusOpen;
}

- (void)close{
    if (status == NSStreamStatusClosed) return;
    [lock lock];
    status = NSStreamStatusClosed;
    dispatch_semaphore_signal(semaphoreSafeToRead);
    [lock unlockWithCondition:0];
}

- (NSStreamStatus)streamStatus
{
    return status;
}

- (void)_scheduleInCFRunLoop:(NSRunLoop *)runLoop forMode:(id)mode {}
- (void)_setCFClientFlags:(CFOptionFlags)flags callback:(CFReadStreamClientCallBack)callback context:(CFStreamClientContext)context {}

@end
