//
//  WebServiceParameters.h
//  StockCount
//
//  Created by Lulakov Viacheslav on 12/20/10.
//  Copyright 2010 DBBest Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <libxml/tree.h>

@interface WebServiceParameters : NSObject {
	NSMutableDictionary *parameters;
}

- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (WebServiceParameters *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;

- (void)addParameter:(NSString *)name value:(NSObject *)value;
//- (NSDictionary *)attributes;
- (NSDictionary *)parameters;

@end
