//
//  SOAPResponseParser.m
//  ProductBuilder
//
//  Created by valera on 12/2/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import "SOAPResponseParser.h"
#import "XmlConvert.h"

//static const xmlChar* kRootElementName = (xmlChar *)"root";
//static NSUInteger kRootElementNameLength = 4;
static const xmlChar *kEnvelopeElementName = (xmlChar *)"Envelope";
static uint kEnvelopeElementNameLength = 8;
static const xmlChar *kBodyElementName = (xmlChar *)"Body";
static uint kBodyElementNameLength = 4;
static const xmlChar *kFaultElementName = (xmlChar *)"Fault";
static uint kFaultElementNameLength = 5;


static void startElementSAX(void *ctx, const xmlChar *localname, const xmlChar *prefix,
                            const xmlChar *URI, int nb_namespaces, const xmlChar **namespaces,
                            int nb_attributes, int nb_defaulted, const xmlChar **attributes);
static void	endElementSAX(void *ctx, const xmlChar *localname, const xmlChar *prefix,
                          const xmlChar *URI);
static void	charactersFoundSAX(void *ctx, const xmlChar *ch, int len);
static void errorEncounteredSAX(void *ctx, const char *msg, ...);
static void endDocumentSAX(void *ctx);

//static NSData* lastParsedData;
//static NSString* lastParsedDataStr;

// Forward reference. The structure is defined in full at the end of the file.
//static xmlSAXHandler simpleSAXHandlerStruct;

@interface SOAPResponseParser(){
    BOOL isCanceled;
}

@end
@implementation SOAPResponseParser

@synthesize pathToObjectRecord, delegate, soapFault;

-(void)initializeSax{
    memset(&saxHandler, 0, sizeof(xmlSAXHandler));
    //xmlSAXVersion(&saxHandler, 2);
    saxHandler.initialized = XML_SAX2_MAGIC;  // so we do this to force parsing as SAX2.
    saxHandler.startElementNs = &startElementSAX;
    saxHandler.endElementNs = &endElementSAX;
    saxHandler.characters = &charactersFoundSAX;
    saxHandler.error = &errorEncounteredSAX;
    saxHandler.endDocument = &endDocumentSAX;
}

-(id)initWithResponseName:(NSString*)aResponseName{
    if ((self=[super init])){
        //xmlInitParser();
        [self initializeSax];
        //_xmlParserContext = xmlCreatePushParserCtxt(&simpleSAXHandlerStruct, self, NULL, 0, NULL);
        _xmlParserContext = xmlCreatePushParserCtxt(&saxHandler, self, NULL, 0, NULL);
        responseName = [aResponseName retain];
    }
    return self;
}

-(void)parseChunk:(NSData*)data{
//    [lastParsedData release];
//    lastParsedData = [data retain];
//    [lastParsedDataStr release];
//    lastParsedDataStr = [[NSString alloc]initWithData:lastParsedData encoding:NSUTF8StringEncoding];
    xmlParseChunk(_xmlParserContext, (const char *)[data bytes], (int)[data length], 0);
}

-(void)cancel{
    isCanceled = TRUE;
}

-(void)finish{
    // Signal the context that parsing is complete by passing "1" as the last parameter.
    xmlParseChunk(_xmlParserContext, NULL, 0, 1);
}

-(void)dealloc{
    if (_xmlParserContext){
        //[self finish];
        xmlDocPtr doc = _xmlParserContext->myDoc;
        
        if (doc!=NULL){
            xmlFreeDoc(doc);
            _xmlParserContext->myDoc = NULL;
        }
        
        xmlStopParser(_xmlParserContext);
        xmlFreeParserCtxt(_xmlParserContext);
    }

    //xmlCleanupParser();
   
    //[bodyNodePrefix release];
    [objectRecord release];
    [objectRootRecord release];
    [pathToObjectRecordArray release];
    [pathToObjectRecord release];
    [soapFault release];
    [soapFaultURI release];
    [responseName release];
    [super dealloc];
}

-(void)setPathToObjectRecord:(NSString *)value{
    [pathToObjectRecord release];
    pathToObjectRecord = [value retain];
    [pathToObjectRecordArray release];
    //pathToObjectRecordArray = [[pathToObjectRecord componentsSeparatedByString:@"\\"] retain];
    NSArray* pathToObjectRecordArrayTemp = [pathToObjectRecord componentsSeparatedByString:@"\\"];
    pathToObjectRecordArray  = [[NSMutableArray alloc]init];
    for(NSString* s in pathToObjectRecordArrayTemp){
        [pathToObjectRecordArray addObject:[XmlConvert encodeName:s]];
    }
}

-(void)resetCurrentcurrentObjectRecordElementName{
    pathToObjectRecordIndex = NSNotFound;
    if (pathToObjectRecordArray.count){
        pathToObjectRecordIndex = 0;
    }
}

#pragma mark SAX parser methods

- (void)elementFound:(const xmlChar *)localname prefix:(const xmlChar *)prefix 
                 uri:(const xmlChar *)URI namespaceCount:(int)namespaceCount
          namespaces:(const xmlChar **)namespaces attributeCount:(int)attributeCount 
defaultAttributeCount:(int)defaultAttributeCount attributes:(xmlSAX2Attributes *)attributes {
    if (isCanceled) return;
    NSAutoreleasePool* pool= [[NSAutoreleasePool alloc]init];
    if (_state>=ACTION_INSIDE_ENVELOPE){
        levelFromEnvelope++;
        if (_state>=ACTION_INSIDE_BODY){
            levelFromBody++;
            if (_state>=ACTION_INSIDE_RESPONSE){
                levelFromResponse++;
                if (_state >=ACTION_INSIDE_OBJECT_RECORD){
                    levelFromObjectRecord++;
                }
            }
        }
    }
    switch(_state){
        case ACTION_START:
            if(!xmlStrncmp(localname, kEnvelopeElementName, kEnvelopeElementNameLength)){
                _state = ACTION_INSIDE_ENVELOPE;
                levelFromEnvelope = 0;
            }
            break;
        case ACTION_INSIDE_ENVELOPE:
            if(!xmlStrncmp(localname, kBodyElementName, kBodyElementNameLength)){
                _state = ACTION_INSIDE_BODY;
            }
            break;
        case ACTION_FINISH:
            break;
        case ACTION_INSIDE_BODY:
            if (xmlStrEqual(localname, (const xmlChar *)[responseName UTF8String])){
                _state = ACTION_INSIDE_RESPONSE;
                levelFromResponse = 0;
//                [bodyNodePrefix release];
//                bodyNodePrefix = nil;
//                if (prefix){
//                    NSString* prefixStr = [[NSString alloc]initWithUTF8String:(const char*)prefix];
//                    bodyNodePrefix = [prefixStr retain];
//                    [prefixStr release];
//                }
            }
            else if (!xmlStrncmp(localname, kFaultElementName, kFaultElementNameLength)
                     /*&& !strcmp([bodyNodePrefix UTF8String],(const char*)prefix)*/){
                _state = ACTION_INSIDE_SOAP_FAULT;
                [soapFaultURI release];
                soapFaultURI = [@((char*)URI)retain];
                [objectRootRecord release];
                objectRootRecord = [[XmlElement alloc]init];
                //NSString* name =[[NSString alloc]initWithUTF8String:(char*)localname];
                NSString* name =[XmlConvert decodeNameFromUTF8String:(const char*)localname];
                objectRootRecord.name = name;
                //[name release];
                [objectRecord release];
                objectRecord = [objectRootRecord retain];
            }
            break;
        case ACTION_INSIDE_RESPONSE:
            if (pathToObjectRecordIndex!=NSNotFound && pathToObjectRecordArray.count > pathToObjectRecordIndex){
                NSString* currentObjectRecordElementName = pathToObjectRecordArray[pathToObjectRecordIndex];
                if (xmlStrEqual(localname , (const xmlChar*)[currentObjectRecordElementName UTF8String])){
                    pathToObjectRecordIndex++;
                    if (pathToObjectRecordArray.count <= pathToObjectRecordIndex){
                        _state = ACTION_INSIDE_OBJECT_RECORD;
                        levelFromObjectRecord = 0;
                        [objectRootRecord release];
                        objectRootRecord = [[XmlElement alloc]init];
                        //NSString* name =[[NSString alloc]initWithUTF8String:(char*)localname];
                        NSString* name =[XmlConvert decodeNameFromUTF8String:(const char*)localname];
                        objectRootRecord.name = name;
                        //[name release];
                        [objectRecord release];
                        objectRecord = [objectRootRecord retain];
                    }
                }
            }
            break;
        case ACTION_INSIDE_SOAP_FAULT:
        case ACTION_INSIDE_OBJECT_RECORD:
        {
            XmlElement* parent = [objectRecord retain];
            [objectRecord release];
            objectRecord = [[XmlElement alloc]init];
            //NSString* name =[[NSString alloc]initWithUTF8String:(char*)localname];
            NSString* name =[XmlConvert decodeNameFromUTF8String:(const char*)localname];
            objectRecord.name = name;
            //[name release];
            objectRecord.parent = parent;
            [parent addChild:objectRecord];
            [parent release];
        }
            break;
        default:
            break;
            
    }
    [pool release];
 }

- (void)endElement:(const xmlChar *)localname prefix:(const xmlChar *)prefix uri:(const xmlChar *)URI {
    if (isCanceled) return;
    if (_state>=ACTION_INSIDE_ENVELOPE){
        levelFromEnvelope--;
        if (_state>=ACTION_INSIDE_BODY){
            levelFromBody--;
            if (_state>=ACTION_INSIDE_RESPONSE){
                levelFromResponse--;
                if (_state >=ACTION_INSIDE_OBJECT_RECORD){
                    levelFromObjectRecord--;
                }
            }
        }
    }
    switch(_state){
        case ACTION_INSIDE_SOAP_FAULT:
        case ACTION_INSIDE_OBJECT_RECORD:
        {
            NSAutoreleasePool* pool = [[NSAutoreleasePool alloc]init];
            XmlElement* parentObjectRecord = [objectRecord.parent retain];
            if (_state == ACTION_INSIDE_OBJECT_RECORD){
                //if (levelFromObjectRecord<0){
                if (!parentObjectRecord){
                    //here objectRecord should equal to objectRootRecord
                    if ([delegate respondsToSelector:@selector(parser:didReceiveRecord:)]){
                        [delegate parser:self didReceiveRecord:objectRecord];
                    }
                    _state = ACTION_INSIDE_RESPONSE; //??
                    pathToObjectRecordIndex--;
                }
            }
            else if (_state == ACTION_INSIDE_SOAP_FAULT){
                if (!parentObjectRecord){
                    if ([soapFaultURI isEqualToString:@"http://schemas.xmlsoap.org/soap/envelope/"]){
                        soapFault = [[SOAPFault deserializeFromXmlElement_11:objectRecord] retain];
                    }
                    else if ([soapFaultURI isEqualToString:@"http://www.w3.org/2003/05/soap-envelope"]){
                        soapFault = [[SOAPFault deserializeFromXmlElement_12:objectRecord] retain];
                    }
                    _state = ACTION_INSIDE_BODY;
                }
            }
            [objectRecord release];
            objectRecord = [parentObjectRecord retain];
            [parentObjectRecord release];
            [pool release];
        }
            break;
        case ACTION_INSIDE_RESPONSE:
            pathToObjectRecordIndex--;
            if (levelFromResponse < 0){
                _state  = ACTION_INSIDE_BODY;
            }
            break;
        case ACTION_INSIDE_BODY:
            if (levelFromBody < 0){
                _state  = ACTION_INSIDE_ENVELOPE;
            }
            break;
        case ACTION_INSIDE_ENVELOPE:
            if (levelFromEnvelope< 0){
                _state  = ACTION_FINISH;
            }
            break;
        default:
            break;
    }
}

- (void)charactersFound:(const xmlChar *)characters length:(int)length {
    if (isCanceled) return;
    if (_state == ACTION_INSIDE_OBJECT_RECORD || _state == ACTION_INSIDE_SOAP_FAULT){
        if (length){
//            if (!objectRecord.dataCache){
//                NSMutableData* data = [[NSMutableData alloc]init];
//                objectRecord.dataCache = data;
//                [data release];
//            }
//            [objectRecord.dataCache appendBytes:characters length:length];
            NSMutableString* data = [[NSMutableString alloc]initWithBytes:characters length:length encoding:NSUTF8StringEncoding];
            if (!objectRecord.value){
                objectRecord.value = data;
            }
            else{
                [objectRecord.value appendString:data];
            }
            [data release];
        }
    }
}

- (void)parsingError:(NSString*)msg {
    NSDictionary *userInfo = [[NSDictionary alloc] initWithObjectsAndKeys:msg, NSLocalizedDescriptionKey, nil];
    [delegate parser:self parsingError:[NSError errorWithDomain:@"iPadServiceResponse" code:1 userInfo:userInfo]];
    [userInfo release];
}

-(void)endDocument {
    
}
@end


#pragma mark SAX Parsing Callbacks

static void startElementSAX(void *ctx, const xmlChar *localname, const xmlChar *prefix,
                            const xmlChar *URI, int nb_namespaces, const xmlChar **namespaces,
                            int nb_attributes, int nb_defaulted, const xmlChar **attributes) {
    [((SOAPResponseParser *)ctx) elementFound:localname prefix:prefix uri:URI 
                                namespaceCount:nb_namespaces namespaces:namespaces
                                attributeCount:nb_attributes defaultAttributeCount:nb_defaulted
                                    attributes:(xmlSAX2Attributes*)attributes];
}

static void	endElementSAX(void *ctx, const xmlChar *localname, const xmlChar *prefix,
                          const xmlChar *URI) {    
    [((SOAPResponseParser *)ctx) endElement:localname prefix:prefix uri:URI];
}

static void	charactersFoundSAX(void *ctx, const xmlChar *ch, int len) {
    [((SOAPResponseParser *)ctx) charactersFound:ch length:len];
}

static void errorEncounteredSAX(void *ctx, const char *msg, ...) {
    NSString *format = [[NSString alloc] initWithBytes:msg length:strlen(msg) encoding:NSUTF8StringEncoding];
    va_list argList;
    va_start(argList, msg);
    NSString* resultString = [[NSString alloc]initWithFormat:format arguments:argList];
    va_end(argList);
    
    [((SOAPResponseParser *)ctx) parsingError:resultString];
    [format release];
    [resultString release];
}

static void endDocumentSAX(void *ctx) {
    [((SOAPResponseParser *)ctx) endDocument];
}

//static xmlSAXHandler simpleSAXHandlerStruct = {
//    NULL,                       /* internalSubset */
//    NULL,                       /* isStandalone   */
//    NULL,                       /* hasInternalSubset */
//    NULL,                       /* hasExternalSubset */
//    NULL,                       /* resolveEntity */
//    NULL,                       /* getEntity */
//    NULL,                       /* entityDecl */
//    NULL,                       /* notationDecl */
//    NULL,                       /* attributeDecl */
//    NULL,                       /* elementDecl */
//    NULL,                       /* unparsedEntityDecl */
//    NULL,                       /* setDocumentLocator */
//    NULL,                       /* startDocument */
//    endDocumentSAX,             /* endDocument */
//    NULL,                       /* startElement*/
//    NULL,                       /* endElement */
//    NULL,                       /* reference */
//    charactersFoundSAX,         /* characters */
//    NULL,                       /* ignorableWhitespace */
//    NULL,                       /* processingInstruction */
//    NULL,                       /* comment */
//    NULL,                       /* warning */
//    errorEncounteredSAX,        /* error */
//    NULL,                       /* fatalError //: unused error() get all the errors */
//    NULL,                       /* getParameterEntity */
//    NULL,                       /* cdataBlock */
//    NULL,                       /* externalSubset */
//    XML_SAX2_MAGIC,             // initialized? not sure what it means just do it
//    NULL,                       // private
//    startElementSAX,            /* startElementNs */
//    endElementSAX,              /* endElementNs */
//    NULL,                       /* serror */
//};

