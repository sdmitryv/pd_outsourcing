//
//  RTMultipartInputStream.h
//  ProductBuilder
//
//  Created by valera on 12/27/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RTMultipartInputStream : NSInputStream
{
@private
    NSMutableArray *parts;
    NSString       *boundary;
    NSData         *footer;
    NSUInteger     footerLength, currentPart, length, delivered, status;
}
- (void)addPartWithName:(NSString *)name string:(NSString *)string contentId:(NSString*)contentId;
- (void)addPartWithName:(NSString *)name data:(NSData *)data contentId:(NSString*)contentId;
- (void)addPartWithName:(NSString *)name path:(NSString *)path contentId:(NSString*)contentId;
- (NSString *)boundary;
- (NSUInteger)length;
@end
