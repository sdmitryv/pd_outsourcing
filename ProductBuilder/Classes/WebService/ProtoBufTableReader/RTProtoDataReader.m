//
//  RTProtoDataReader.m
//  ProtoBufTableReader
//
//  Created by valera on 5/27/13.
//  Copyright (c) 2013 valera. All rights reserved.
//

#import "RTProtoDataReader.h"
#import "BPUUID.h"
#import "WireFormat.h"

@interface NSUTF8StringContainer()

@end

@implementation NSUTF8StringContainer

+(id)utf8StringContainerWithData:(NSData*)aData{
    NSUTF8StringContainer* var = [[NSUTF8StringContainer alloc]init];
    var->_data = [aData retain];
    return [var autorelease];
}

-(void)dealloc{
    self.data = nil;
    [super dealloc];
}

-(NSString*)description{
    return [[[NSString alloc]initWithBytesNoCopy:(void *)self.data.bytes length:self.data.length encoding:NSUTF8StringEncoding freeWhenDone:FALSE]autorelease];
}
@end

@interface PBCodedInputStream(ExtendedDataTypes)

-(NSDecimalNumber*)readDecimal;
-(NSTimeInterval)readTimeInterval;
-(BPUUID*)readGuid;
@end

@implementation PBCodedInputStream(ExtendedDataTypes)

-(BPUUID*)readGuid{
    int64_t low = 0UL;
    int64_t high = 0UL;
    int token = [self startSubItem];
    int fieldNumber;
    int tag;
    while ((fieldNumber = PBWireFormatGetTagFieldNumber(tag=[self readTag]))> 0){
        if (PBWireFormatGetTagWireType(tag)==PBWireFormatEndGroup) break;
        switch (fieldNumber)
        {
            case 1:
                low = [self readFixed64];
                break;
            case 2:
                high = [self readFixed64];
                break;
            default:
                [self skipField:tag];
                break;
        }
    }
    [self endSubItem:token];
    if(low == 0 && high == 0) return [BPUUID empty];
    uint
    a = (uint)low,
    b = (uint)(low >> 32),
    c= (uint)high,
    d = (uint)(high >> 32);
    uuid_t bytes = {a >> 24, a >> 16, a >>8, a,
        b >>8, b, b >> 24, b >> 16, c , c >> 8, c >> 16,c >> 24, d, d >>8, d >> 16, d >> 24};
    return [BPUUID UUIDWithUUIDBytes:bytes];
}

typedef NS_ENUM(NSUInteger, TimeSpanScale)
{
    TimeSpanScaleDays = 0,
    TimeSpanScaleHours = 1,
    TimeSpanScaleMinutes = 2,
    TimeSpanScaleSeconds = 3,
    TimeSpanScaleMilliseconds = 4,
    TimeSpanScaleTicks = 5,
    TimeSpanScaleMinMax = 15,
};

-(NSTimeInterval)readTimeInterval{
    switch (PBWireFormatGetTagWireType(lastTag))
    {
        case PBWireFormatLengthDelimited:
        case PBWireFormatStartGroup:
        {
            int token = [self startSubItem];
            int fieldNumber;
            TimeSpanScale scale = TimeSpanScaleDays;
            int64_t value = 0;
            int tag;
            while ((fieldNumber = PBWireFormatGetTagFieldNumber(tag=[self readTag]))> 0){
                if (PBWireFormatGetTagWireType(tag)==PBWireFormatEndGroup) break;
                switch (fieldNumber)
                {
                    case 1:
                        //source.Assert(WireType.SignedVariant);
                        value = [self readSInt64];
                        break;
                    case 2:
                        scale = (TimeSpanScale)[self readInt32];
                        break;
                    default:
                        [self skipField:tag];
                        break;
                }
            }
            [self endSubItem:token];
            switch (scale){
                case TimeSpanScaleDays:
                    return value * 86400.0;
                case TimeSpanScaleHours:
                    return value * 3600.0;
                case TimeSpanScaleMinutes:
                    return value * 60.0;
                case TimeSpanScaleSeconds:
                    return value;
                case TimeSpanScaleMilliseconds:
                    return value /1000.0;
                case TimeSpanScaleTicks:
                    return value/10000000.0;
                case TimeSpanScaleMinMax:
                    switch (value)
                {
                    case 1: return LONG_MAX/10000000.0;
                    case -1: return LONG_MIN/10000000.0f;
                    default:
                        @throw [NSException exceptionWithName:@"CodedInputStrean" reason:@"Unknown timespan min/max value" userInfo:nil];
                }
                default:
                    @throw [NSException exceptionWithName:@"CodedInputStrean" reason:@"Unknown timespan timescale" userInfo:nil];
                }
            break;
        case PBWireFormatFixed64:
            return [self readInt64]/10000000.0L;
        default:
            @throw [NSException exceptionWithName:@"CodedInputStrean" reason:@"Unexpected wire-type" userInfo:nil];
    }}
}

-(NSDecimalNumber*)readDecimal{
    uint64_t low = 0UL;
    uint high = 0U;
    uint flags = 0U;
    int token = [self startSubItem];
    int field;
    int tag;
    while ((field = PBWireFormatGetTagFieldNumber(tag = [self readTag])) > 0)
    {
        if (PBWireFormatGetTagWireType(tag)==PBWireFormatEndGroup) break;
        switch (field)
        {
            case 1:
                low = [self readUInt64];
                break;
            case 2:
                high = [self readUInt32];
                break;
            case 3:
                flags = [self readUInt32];
                break;
            default:
                [self skipField:tag];
                break;
        }
    }
    [self endSubItem:token];
    
    if (!low && !high)
        return [NSDecimalNumber zero];
    else{
        NSDecimal value = [[NSDecimalNumber zero] decimalValue];
        value._mantissa[0] = low &  0xFFFF;
        value._mantissa[1] = (low >> 16) &  0xFFFF;
        value._mantissa[2] = (low >> 32) &  0xFFFF;
        value._mantissa[3] = (low >> 48) &  0xFFFF;
        value._mantissa[4] = high &  0xFFFF;
        value._mantissa[5] = (high >> 16) &  0xFFFF;
        value._mantissa[6] = value._mantissa[7] = 0;
        if (value._mantissa[0] || value._mantissa[1] || value._mantissa[2] || value._mantissa[3] || value._mantissa[4] || value._mantissa[5]){
            value._length = 8;
        }
        
        value._isNegative = ((int) flags & 1) == 1;
        value._exponent = -((flags & 510U) >> 1);
        value._isCompact = NO;
        return [NSDecimalNumber decimalNumberWithDecimal:value];
    }
}

@end

@implementation RTProtoDataReader

-(id)initWithStream:(NSInputStream*)aStream{
    if ((self=[super init]))
    {
        //stream = [aStream retain];
        inputStream = [[PBCodedInputStream streamWithInputStream:aStream]retain];
        colReaders = [[NSMutableArray alloc]init];
        colNames = [[NSMutableArray alloc]init];
        colTypes = [[NSMutableArray alloc]init];
        [self advanceToNextField];
        if (currentField!=1){
            //invalid stream
            [self release];
            return nil;
        }
        [self readNextTableHeader];
    }
    return self;
}

-(void)dealloc{
    //[stream release];
    [currentRow release];
    [colNames release];
    [colTypes release];
    [colReaders release];
    [inputStream release];
    [super dealloc];
}

-(void)advanceToNextField{
    int32_t tag = [inputStream readTag];
    currentField = PBWireFormatGetTagWireType(tag)==PBWireFormatEndGroup ? 0 : PBWireFormatGetTagFieldNumber(tag);
}

-(void)readNextTableHeader{
    [currentRow release];
    currentRow = nil;
    currentTableToken = [inputStream startSubItem];
    [self advanceToNextField];
    if (!currentField){
        reachEndOfCurrentTable = TRUE;
        [inputStream endSubItem:currentTableToken];
    }
    if (currentField!=2){
        @throw [NSException exceptionWithName:@"InvalidProtoDataReader" reason:@"No header found" userInfo:nil];
    }
    [self readHeader];
}

-(void)readHeader{
    do{
        [self readColumn];
        [self advanceToNextField];
    }while(currentField==2);
}


-(void)readColumn{
    int token = [inputStream startSubItem];
    int field, tag;
    NSString* name = nil;
    ProtoDataType protoDataType = - 1;
    while ((field = PBWireFormatGetTagFieldNumber(tag=[inputStream readTag]))!=0){
        if (PBWireFormatGetTagWireType(tag)==PBWireFormatEndGroup) break;
        switch (field) {
            case 1:
                name = [inputStream readString];
                break;
            case 2:
                protoDataType = [inputStream readInt32];
                break;
            default:
                [inputStream skipField:tag];
                break;
        }
    }
    if (!name)
        @throw [NSException exceptionWithName:@"ProtoDataReader" reason:@"Column name is not defined" userInfo:nil];
    id(^object)(void) = nil;
    __block PBCodedInputStream* inputStreamP = inputStream;
    switch(protoDataType){
        case ProtoDataTypeInt:
            object = [^id(){return @([inputStreamP readInt32]);} copy];
            break;
        case ProtoDataTypeShort:
            object = [^id(){return @([inputStreamP readInt32]);} copy];
            break;
        case ProtoDataTypeDecimal:
            object = [^id(){return [inputStreamP readDecimal];} copy];
            break;
        case ProtoDataTypeString:
            //object = [^id(){return [inputStreamP readString];} copy];
            object = [^id(){return [NSUTF8StringContainer utf8StringContainerWithData:[inputStreamP readData]];} copy];
            break;
        case ProtoDataTypeGuid:
            object = [^id(){return [inputStreamP readGuid];} copy];
            break;
        case ProtoDataTypeDateTime:
            object = [^id(){return @([inputStreamP readTimeInterval]);} copy];
            break;
        case ProtoDataTypeBool:
            object = [^id(){return @([inputStreamP readBool]);} copy];
            break;
            
        case ProtoDataTypeByte:
            object = [^id(){return @([inputStreamP readRawByte]);} copy];
            break;
            
        case ProtoDataTypeChar:
            object = [^id(){return @([inputStreamP readInt32]);} copy];
            break;
            
        case ProtoDataTypeDouble:
            object = [^id(){return @([inputStreamP readDouble]);} copy];
            break;
            
        case ProtoDataTypeFloat:
            object = [^id(){return @([inputStreamP readFloat]);} copy];
            break;
            
        case ProtoDataTypeLong:
            object = [^id(){return @([inputStreamP readInt64]);} copy];
            break;
            
        case ProtoDataTypeByteArray:
            object = [^id(){return [inputStreamP readData];} copy];
            break;
            
        case ProtoDataTypeCharArray:
            object = [^id(){return [inputStreamP readString];} copy];
            break;
            
        default:
            @throw [NSException exceptionWithName:@"ProtoDataReader" reason:@"The type is not supported" userInfo:nil];
    }
    [colReaders addObject:object];
    [object release];
    [inputStream endSubItem:token];
    [colNames addObject:name];
    [colTypes addObject:@(protoDataType)];
}

-(void)readCurrentRow{
    @autoreleasepool {
        int field;
    //    if (!currentRow){
    //        currentRow = [[NSMutableArray alloc]initWithCapacity:colNames.count];
    //    }
    //    else{
    //        [currentRow removeAllObjects];
    //    }
        [currentRow release];
        currentRow = [[NSMutableArray alloc]initWithCapacity:colNames.count];
        int token = [inputStream startSubItem];
        int tag;
        while ((field = PBWireFormatGetTagFieldNumber(tag=[inputStream readTag]))!=0){
            if (PBWireFormatGetTagWireType(tag)==PBWireFormatEndGroup) break;
            if (field > colNames.count) [inputStream skipField:tag];
            else{
                int i = field - 1;
                id (^ readBlock)() = colReaders[i];
                while (i > currentRow.count){
                    [currentRow addObject:[NSNull null]];
                }
                id value = nil;
                @try{
                    value = readBlock();
                }
                @catch(NSException* exception) {
                    @throw [NSException exceptionWithName:exception.name reason:[NSString stringWithFormat:@"%@ [while reading [%@]column]", exception.description, colNames[i]] userInfo:nil];
                }
                if (!value){
                    value = [NSNull null];
                }
                currentRow[i] = value;
            }
        }
        for (NSInteger i = currentRow.count; i < colNames.count; i++){
            [currentRow addObject:[NSNull null]];
        }
        [inputStream endSubItem:token];
    }
}

-(NSString*)getName:(NSUInteger) index{
    return (NSString*)colNames[index];
}

-(id)getValue:(NSUInteger)index{
    return currentRow[index];
}

-(NSUInteger)getOrdinal:(NSString*)name{
    return [colNames indexOfObject:name];
}

-(NSUInteger)fieldCount{
    return colNames.count;
}

-(NSArray*)currentRow{
    return currentRow;
}

-(NSArray*)columnNames{
    return colNames;
}

-(BOOL)read{
    if (reachEndOfCurrentTable) return FALSE;
    if (!currentField){
        [inputStream endSubItem:currentTableToken];
        reachEndOfCurrentTable = TRUE;
        return FALSE;
    }
    [self readCurrentRow];
    [self advanceToNextField];
    return TRUE;
}

@end
