//
//  BackupManager.h
//  ProductBuilder
//
//  Created by Viacheslav Lulakov on 12/25/15.
//  Copyright © 2015 Cloudworks. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BackupManager : NSObject

+ (BackupManager *)sharedInstance;

- (void)backupDatabase;
- (void)cancel;

@property (nonatomic, assign) CGFloat     progress;
@property (nonatomic, retain) NSString  * message;
@property (nonatomic, assign) BOOL        isBusy;

@end
