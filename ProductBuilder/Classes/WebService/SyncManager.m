//
//  SyncManager.m
//  StockCount
//
//  Created by Lulakov Viacheslav on 12/15/10.
//  Copyright 2010 DBBest Technologies. All rights reserved.
//

#import "SyncManager.h"
#import "Settings.h"
#import "WebServiceOperation.h"
#import "WebServiceParameters.h"
#import "Workstation.h"
#import "SecurityManager.h"
#import "SOAPAttachment.h"
#import "Location.h"
#import "DeviceAgentManager.h"
#import "SettingManager.h"
#import "DataUtils.h"
#import "NSDate+System.h"
#import "AppSettingManager.h"
#import "AppParameterManager.h"
#import "DBAdditions.h"

NSString* const  addressFormat = @"http://%@";

@interface NSMutableDictionary(NullableExtentions)
-(void)setObjectDescriptionIfNotNil:(id)object forKey:(NSString*)key;
@end

@implementation NSMutableDictionary(NullableExtentions)

-(void)setObjectDescriptionIfNotNil:(id)object forKey:(NSString*)key{
    if (object && key){
        self[key] = [object description];
    }
}

@end

@interface SyncManager(){
    dispatch_queue_t _syncCurrentOperationQueue;
    dispatch_semaphore_t _waitResponseSemaphore;
}

//@property(atomic,retain)WebServiceOperation* currentOperation;
@property(nonatomic,readonly)NSDateFormatter *localDateFormatter;

-(NSMutableDictionary*)getRequestContext;

@end

@implementation SyncManager

@synthesize
syncDelegate,
logXML, defaultTimeout;

- (id)init {
	
	if ((self = [super init])){
        _syncCurrentOperationQueue = dispatch_queue_create("com.syncManager.self.currentOperation.syncQueue", DISPATCH_QUEUE_SERIAL);
        self.defaultTimeout = 120;
#ifdef DEBUG

        self.logXML = AppParameterManager.instance.enableLogs;
#else
        self.logXML = NO;
#endif
        [self readSettings];
        _waitResponseSemaphore = dispatch_semaphore_create(0);
    }
	return self;
}

-(void)readSettings{
    [opsAddress release];
    opsAddress = [[AppSettingManager instance].serverUrl retain];

    if (opsAddress && opsAddress.length)
        isOpsAddressValid = TRUE;
}

-(NSString*)address{

    if (isOpsAddressValid){
        
        return opsAddress;
    }
    return nil;
}

-(void)setCurrentOperation:(WebServiceOperation*)value{
    
    dispatch_barrier_sync(_syncCurrentOperationQueue, ^{
        
        _currentOperation.delegate = nil;
        [_currentOperation cancel];
        while (!dispatch_semaphore_wait(_waitResponseSemaphore, DISPATCH_TIME_NOW));
        
        [value retain];
        [_currentOperation release];
        _currentOperation = value;
        if (_currentOperation) {
            
            _currentOperation.logXMLInOut = self.logXML;
            _currentOperation.defaultTimeout = self.defaultTimeout;
            NSString* address = [self address];
            if (!address){
                
                [self.syncDelegate syncManager:self didOccurError:@"Op's URL not defined." error:nil];
            }
            
            NSString *fullAddress = [address rangeOfString:@"http" options: NSCaseInsensitiveSearch| NSAnchoredSearch].location != NSNotFound ? address : [NSString stringWithFormat:addressFormat, address];
            NSURL *fullAddressURL = [NSURL URLWithString:fullAddress];
            _currentOperation.address = fullAddressURL;
        }
    });
}

-(WebServiceOperation*)сurrentOperation{
    
    __block WebServiceOperation* value = nil;
    dispatch_barrier_sync(_syncCurrentOperationQueue, ^{
        
        value = [_currentOperation retain];
    });
    return [value autorelease];
}

- (void)dealloc {
    
    self.currentOperation = nil;
    [opsAddress release];
    [_localDateFormatter release];
    if (_syncCurrentOperationQueue){
        
        dispatch_sync(_syncCurrentOperationQueue, ^{});
        dispatch_release(_syncCurrentOperationQueue);
    }
    
    if (_waitResponseSemaphore){
        
        dispatch_release(_waitResponseSemaphore);
    }
	[super dealloc];
}

- (NSDateFormatter *)localDateFormatter {
    
    if (!_localDateFormatter) {
        
        _localDateFormatter = [[NSDateFormatter alloc] init];
        [_localDateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss"];
        [_localDateFormatter setLocale:[[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"] autorelease]];
    }
    return _localDateFormatter;
}

-(void)waitForResponse{

    dispatch_semaphore_wait(_waitResponseSemaphore, DISPATCH_TIME_FOREVER);
}

- (void)getIDs:(NSString *)tableName modifiedDate:(NSDate *)modifiedDate timestamp:(NSData *)timestamp{
    
	WebServiceParameters *params = [[WebServiceParameters alloc] init];
	[params addParameter:@"tableName" value:tableName];
    [params addParameter:@"modifiedDate" value:modifiedDate];
    [params addParameter:@"timestamp" value:timestamp];
    [params addParameter:@"deviceUniqueID" value:[[AppSettingManager instance].deviceUniqueId description]];
	self.currentOperation = [[[WebServiceOperation alloc] initWithDelegate:self parameters:params]autorelease];
	self.currentOperation.name = @"GetIDs";
	[self.currentOperation start];
    [params release];
}

- (void)getTable:(NSString *)tableName modifiedDate:(NSDate *)modifiedDate timestamp:(NSData *)timestamp{
    
    [self getTable:tableName ids:nil modifiedDate:modifiedDate timestamp:timestamp];
}

- (void)getTable:(NSString *)tableName ids:(NSArray *)ids {
    
	[self getTable:tableName ids:ids modifiedDate:nil timestamp:nil];
}

- (void)getTableCount:(NSString *)tableName modifiedDate:(NSDate *)modifiedDate whereCondition:(NSString *)whereCondition conditionParams:(NSDictionary *)conditionParams mode:(NSUInteger)syncMode timestamp:(NSData *)timestamp{
    
    return [self getTableCount:tableName ids:nil modifiedDate:modifiedDate whereCondition:whereCondition conditionParams:conditionParams mode:syncMode timestamp:timestamp];
}

- (void)getTableCount:(NSString *)tableName ids:(NSArray*) ids modifiedDate:(NSDate *)modifiedDate whereCondition:(NSString *)whereCondition conditionParams:(NSDictionary *)conditionParams mode:(NSUInteger)syncMode timestamp:(NSData *)timestamp {
    
    WebServiceParameters *params = [[WebServiceParameters alloc] init];
    NSMutableDictionary *tableParams = [NSMutableDictionary dictionary];
    if (conditionParams) {
        
        NSNamedMutableArray *queryArr = [[[NSNamedMutableArray alloc] init] autorelease];
        queryArr.name = @"QueryParam";
        for (NSString *key in conditionParams.allKeys) {
            [queryArr addObject:@{@"Name":key, @"Value":conditionParams[key]}];
        }
        if (queryArr.count > 0) {
            [tableParams setValue:queryArr forKey:@"QueryParams"];
        }
    }
    [tableParams setValue:tableName forKey:@"tableName"];
    [tableParams setValue:ids forKey:@"ids"];
    [tableParams setValue:modifiedDate forKey:@"modifiedDate"];
    [tableParams setValue:timestamp forKey:@"timestamp"];
    [tableParams setValue:whereCondition forKey:@"whereCondition"];
    [tableParams setBool:syncMode forKey:@"init"];
    [params addParameter:@"table" value:tableParams];
    [params addParameter:@"requestContext" value:[self getRequestContext]];

	self.currentOperation  = [[[WebServiceOperation alloc] initWithDelegate:self parameters:params]autorelease];
	self.currentOperation.name = @"GetTableCount";
    self.currentOperation.returnClass = [NSNumber class];
	[self.currentOperation start];
    [params release];
}

- (void)getTable:(NSString *)tableName ids:(NSArray *)ids modifiedDate:(NSDate *)modifiedDate timestamp:(NSData *)timestamp {
    
    WebServiceParameters *params = [[WebServiceParameters alloc] init];
    NSMutableDictionary *tableParams = [NSMutableDictionary dictionary];
    [tableParams setValue:tableName forKey:@"tableName"];
    [tableParams setValue:ids forKey:@"ids"];
    [tableParams setValue:modifiedDate forKey:@"modifiedDate"];
    [tableParams setValue:timestamp forKey:@"timestamp"];
    
    [params addParameter:@"table" value:tableParams];
    [params addParameter:@"requestContext" value:[self getRequestContext]];

	self.currentOperation  = [[[WebServiceOperation alloc] initWithDelegate:self parameters:params]autorelease];
	self.currentOperation.name = @"GetTable";
	[self.currentOperation start];
    [params release];
}

- (void)getTableSAX:(NSString *)tableName ids:(NSArray *)ids modifiedDate:(NSDate *)modifiedDate timestamp:(NSData *)timestamp {
    
    WebServiceParameters *params = [[WebServiceParameters alloc] init];
	NSMutableDictionary *tableParams = [NSMutableDictionary dictionary];
    [tableParams setValue:tableName forKey:@"tableName"];
    [tableParams setValue:ids forKey:@"ids"];
    [tableParams setValue:modifiedDate forKey:@"modifiedDate"];
    [tableParams setValue:timestamp forKey:@"timestamp"];
    
    [params addParameter:@"table" value:tableParams];
    [params addParameter:@"requestContext" value:[self getRequestContext]];

	self.currentOperation = [[[WebServiceOperation alloc] initWithDelegate:self parameters:params]autorelease];
    self.currentOperation.useSaxParser = TRUE;
    NSString* saxParserPathToRecord = [[NSString alloc] initWithFormat:@"root\\%@", tableName];
    self.currentOperation.saxParserPathToRecord = saxParserPathToRecord;
    [saxParserPathToRecord release];
	self.currentOperation.name = @"GetTable";
	[self.currentOperation start];
    [params release];
}

- (void)getTableAsBinary:(NSString *)tableName ids:(NSArray *)ids modifiedDate:(NSDate *)modifiedDate whereCondition:(NSString *)whereCondition conditionParams:(NSDictionary *)conditionParams mode:(NSUInteger)syncMode timestamp:(NSData *)timestamp retryCount:(NSUInteger)retryCount{
    
    WebServiceParameters *params = [[WebServiceParameters alloc] init];
    NSMutableDictionary *tableParams = [NSMutableDictionary dictionary];
    if (conditionParams) {
        
        NSNamedMutableArray *queryArr = [[[NSNamedMutableArray alloc] init] autorelease];
        queryArr.name = @"QueryParam";
        for (NSString *key in conditionParams.allKeys) {
            [queryArr addObject:@{@"Name":key, @"Value":conditionParams[key]}];
        }
        if (queryArr.count > 0) {
            [tableParams setValue:queryArr forKey:@"QueryParams"];
        }
    }
    
    [tableParams setValue:tableName forKey:@"tableName"];
    if (retryCount){
        [tableParams setValue:@(retryCount) forKey:@"retryCount"];
    }
    [tableParams setValue:ids forKey:@"ids"];
    [tableParams setValue:modifiedDate forKey:@"modifiedDate"];
    [tableParams setValue:timestamp forKey:@"timestamp"];
    [tableParams setValue:whereCondition forKey:@"whereCondition"];
    [tableParams setBool:syncMode forKey:@"init"];
    [params addParameter:@"table" value:tableParams];
    [params addParameter:@"requestContext" value:[self getRequestContext]];

	self.currentOperation = [[[WebServiceOperation alloc] initWithDelegate:self parameters:params]autorelease];
    self.currentOperation.useSaxParser = TRUE;
    NSString* saxParserPathToRecord = [[NSString alloc] initWithFormat:@"root\\%@", tableName];
    self.currentOperation.saxParserPathToRecord = saxParserPathToRecord;
    [saxParserPathToRecord release];
	self.currentOperation.name = @"GetTableAsBinary";
	[self.currentOperation start];
    [params release];
}

- (void)updateReceipt:(NSDictionary *)receipt items:(NSNamedMutableArray *)items itemsMembership:(NSNamedMutableArray*)itemsMembership payments:(NSNamedMutableArray *)payments charges:(NSNamedMutableArray *)charges globalCharges:(NSNamedMutableArray *)globalCharges
          itemCharges:(NSNamedMutableArray *)itemCharges taxes:(NSNamedMutableArray *)taxes creditAccounts:(NSNamedMutableArray *)creditAccounts discountCoupons:(NSNamedMutableArray*)discountCoupons onlineOMSTransaction:(NSDictionary *)onlineOMSTransaction receiptTaxExemptInfo:(NSNamedMutableArray*)receiptTaxExemptInfo taxJurisdictions:(NSNamedMutableArray*)taxJurisdictions persons:(NSNamedMutableArray*)persons receiptAccounts:(NSNamedMutableArray*)receiptAccounts itemPromos:(NSNamedMutableArray*)itemPromos receiptPromos:(NSNamedMutableArray*)receiptPromos{
    
    WebServiceParameters *params = [[[WebServiceParameters alloc] init] autorelease];
    [params addParameter:@"receipt" value:receipt];
    [params addParameter:@"receiptPromos" value:receiptPromos];
    [params addParameter:@"items" value:items];
    [params addParameter:@"itemPromos" value:itemPromos];
    [params addParameter:@"itemsMembership" value:itemsMembership];
    [params addParameter:@"payments" value:payments];
    [params addParameter:@"charges" value:charges];
    [params addParameter:@"globalCharges" value:globalCharges];
    [params addParameter:@"itemCharges" value:itemCharges];
    [params addParameter:@"taxes" value:taxes];
    [params addParameter:@"creditAccounts" value:creditAccounts];
    [params addParameter:@"discountCoupons" value:discountCoupons];
    if (onlineOMSTransaction) 
        [params addParameter:@"onlineOMSTransaction" value:onlineOMSTransaction];
    [params addParameter:@"taxExemptInfos" value:receiptTaxExemptInfo];
    [params addParameter:@"taxJurisdictions" value:taxJurisdictions];
    [params addParameter:@"salespersons" value:persons];
    [params addParameter:@"accounts" value:receiptAccounts];
    if (!AppParameterManager.instance.isUploadsDisabled)
        [params addParameter:@"requestContext" value:[self getRequestContext]];
	self.currentOperation  = [[[WebServiceOperation alloc] initWithDelegate:self parameters:params]autorelease];
	self.currentOperation.name = @"UpdateReceipt";
    self.currentOperation.returnClass = [USBoolean class];
	[self.currentOperation start];
}

- (void)updateCustomer:(NSDictionary *)customer {
    
    WebServiceParameters *params = [[[WebServiceParameters alloc] init] autorelease];
    [params addParameter:@"customer" value:customer];
    if (!AppParameterManager.instance.isUploadsDisabled)
        [params addParameter:@"requestContext" value:[self getRequestContext]];
	self.currentOperation = [[[WebServiceOperation alloc] initWithDelegate:self parameters:params]autorelease];
	self.currentOperation.name = @"UpdateCustomer";
    
	[self.currentOperation start];
}

- (void)updateEmployeeNotification:(NSDictionary *)employeeNotification {
    
    WebServiceParameters *params = [[[WebServiceParameters alloc] init] autorelease];
    [params addParameter:@"employeeNotification" value:employeeNotification];
    if (!AppParameterManager.instance.isUploadsDisabled)
        [params addParameter:@"requestContext" value:[self getRequestContext]];
    self.currentOperation = [[[WebServiceOperation alloc] initWithDelegate:self parameters:params]autorelease];
    self.currentOperation.name = @"UpdateEmployeeNotification";
    [self.currentOperation start];
}

- (void)updateSecurityLogs:(NSNamedMutableArray *)logs {
    
    WebServiceParameters *params = [[[WebServiceParameters alloc] init] autorelease];
    [params addParameter:@"logs" value:logs];
    if (!AppParameterManager.instance.isUploadsDisabled)
        [params addParameter:@"requestContext" value:[self getRequestContext]];
	self.currentOperation = [[[WebServiceOperation alloc] initWithDelegate:self parameters:params]autorelease];
	self.currentOperation.name = @"UpdateSecurityLogs";
	[self.currentOperation start];
}

- (void)updateSVSBalances:(NSNamedMutableArray *)balances {
    
    WebServiceParameters *params = [[[WebServiceParameters alloc] init] autorelease];
    [params addParameter:@"balances" value:balances];
    if (!AppParameterManager.instance.isUploadsDisabled)
        [params addParameter:@"requestContext" value:[self getRequestContext]];
	self.currentOperation = [[[WebServiceOperation alloc] initWithDelegate:self parameters:params]autorelease];
	self.currentOperation.name = @"UpdateSVSBalances";
	[self.currentOperation start];
}

- (void)updateSVSTransactions:(NSNamedMutableArray *)transactions {
    
    WebServiceParameters *params = [[[WebServiceParameters alloc] init] autorelease];
    [params addParameter:@"transactions" value:transactions];
    if (!AppParameterManager.instance.isUploadsDisabled)
        [params addParameter:@"requestContext" value:[self getRequestContext]];
	self.currentOperation = [[[WebServiceOperation alloc] initWithDelegate:self parameters:params]autorelease];
	self.currentOperation.name = @"UpdateSVSTransactions";
	[self.currentOperation start];
}

- (void)updateContacts:(NSNamedMutableArray *)contacts {
    
    WebServiceParameters *params = [[[WebServiceParameters alloc] init] autorelease];
    [params addParameter:@"contacts" value:contacts];
    if (!AppParameterManager.instance.isUploadsDisabled)
        [params addParameter:@"requestContext" value:[self getRequestContext]];
	self.currentOperation = [[[WebServiceOperation alloc] initWithDelegate:self parameters:params]autorelease];
	self.currentOperation.name = @"UpdateContacts";
	[self.currentOperation start];
}

- (void)updateShipToAddresses:(NSNamedMutableArray *)contacts {
    
    WebServiceParameters *params = [[[WebServiceParameters alloc] init] autorelease];
    [params addParameter:@"shipToAddresses" value:contacts];
    if (!AppParameterManager.instance.isUploadsDisabled)
        [params addParameter:@"requestContext" value:[self getRequestContext]];
	self.currentOperation = [[[WebServiceOperation alloc] initWithDelegate:self parameters:params]autorelease];
	self.currentOperation.name = @"UpdateShipToAddresses";
	[self.currentOperation start];
}

- (void)updateEmailTask:(NSNamedMutableArray *)emailTasksArr {
    
    WebServiceParameters *params = [[[WebServiceParameters alloc] init] autorelease];
    [params addParameter:@"emailTasks" value:emailTasksArr];
    if (!AppParameterManager.instance.isUploadsDisabled)
        [params addParameter:@"requestContext" value:[self getRequestContext]];
	self.currentOperation = [[[WebServiceOperation alloc] initWithDelegate:self parameters:params]autorelease];
	self.currentOperation.name = @"UpdateEmailTask";
	[self.currentOperation start];
}



-(NSMutableDictionary*)getRequestContext{
    
    NSMutableDictionary *context = [[[NSMutableDictionary alloc] init] autorelease];
    [context setValue:APP_NAME forKey:@"AppName"];
    [context setValue:AppParameterManager.instance.bundleId forKey:@"BundleId"];
    [context setValue:[DeviceAgentManager sharedInstance].appVersion forKey:@"AppVersion"];
    [context setValue:[self.localDateFormatter stringFromDate:[NSDate date]] forKey:@"DeviceLocalTime"];
    
    if ([AppSettingManager instance].deviceUniqueId){
        [context setValue:[AppSettingManager instance].deviceUniqueId.description forKey:@"DeviceAgentId"];
        [context setValue:[AppSettingManager instance].deviceUniqueId.description forKey:@"DeviceUniqueID"];
    }
    if ([AppSettingManager instance].deviceServerId)
        [context setValue:[AppSettingManager instance].deviceServerId.description forKey:@"DeviceServerId"];
    if ([AppSettingManager instance].locationId)
        [context setValue:[AppSettingManager instance].locationId.description forKey:@"LocationID"];
    if ([AppSettingManager instance].iPadId)
        [context setValue:[AppSettingManager instance].iPadId forKey:@"DeviceID"];
    if ([Workstation currentWorkstation])
        [context setValue:[Workstation currentWorkstation].id forKey:@"WorkstationID"];
    if ([SecurityManager currentEmployee])
        [context setValue:[SecurityManager currentEmployee].id forKey:@"EmployeeID"];
    if ([Location localLocation]) {
        [context setValue:@([Location localLocation].locationNum) forKey:@"LocationNum"];
        [context setValue:[Location localLocation].locationCode forKey:@"LocationCode"];
    }
    
    return context;
}

- (void)updateReceiptRemotePayment:(NSNamedMutableArray*)receiptRemotePayments{
   
    WebServiceParameters *params = [[[WebServiceParameters alloc] init] autorelease];
    if (!AppParameterManager.instance.isUploadsDisabled)
        [params addParameter:@"requestContext" value:[self getRequestContext]];
    [params addParameter:@"receiptRemotePayments" value:receiptRemotePayments];
	self.currentOperation = [[[WebServiceOperation alloc] initWithDelegate:self parameters:params]autorelease];
    self.currentOperation.name = @"UpdateReceiptRemotePayment";	
	[self.currentOperation start];
}

- (void)updateActionsTracking:(NSNamedMutableArray*)actionTrackings{

    for (NSMutableDictionary * dic in actionTrackings.data)
        dic[@"ActionDate"] = [NSDate dateWithTimeIntervalSince1970:((NSString *)dic[@"ActionDate"]).doubleValue];
     
    WebServiceParameters *params = [[[WebServiceParameters alloc] init] autorelease];
    if (!AppParameterManager.instance.isUploadsDisabled)
        [params addParameter:@"requestContext" value:[self getRequestContext]];
    [params addParameter:@"actionsTrackings" value:actionTrackings];
	self.currentOperation = [[[WebServiceOperation alloc] initWithDelegate:self parameters:params]autorelease];
    self.currentOperation.name = @"UpdateActionsTracking";
	[self.currentOperation start];
}

- (void)uploadQueryResults:(NSNamedMutableArray *)results {
    
    WebServiceParameters *params = [[[WebServiceParameters alloc] init] autorelease];
    if (!AppParameterManager.instance.isUploadsDisabled)
        [params addParameter:@"requestContext" value:[self getRequestContext]];
    [params addParameter:@"results" value:results];
	self.currentOperation = [[[WebServiceOperation alloc] initWithDelegate:self parameters:params]autorelease];
    self.currentOperation.name = @"UpdatePOSQueryResults";	
	[self.currentOperation start];
}

- (void)uploadExceptionLog:(NSDictionary *)log {

    WebServiceParameters *params = [[[WebServiceParameters alloc] init] autorelease];
    [params addParameter:@"mobileException" value:log];
    if (!AppParameterManager.instance.isUploadsDisabled)
        [params addParameter:@"requestContext" value:[self getRequestContext]];
	self.currentOperation = [[[WebServiceOperation alloc] initWithDelegate:self parameters:params]autorelease];
    self.currentOperation.sendErrorLog = FALSE;
	self.currentOperation.name = @"AddMobileException";
	[self.currentOperation start];
}

- (void)uploadCustomUILayouts:(NSNamedMutableArray *)transactions {
    
    WebServiceParameters *params = [[[WebServiceParameters alloc] init] autorelease];
    [params addParameter:@"customUILayouts" value:transactions];
    if (!AppParameterManager.instance.isUploadsDisabled)
        [params addParameter:@"requestContext" value:[self getRequestContext]];
    self.currentOperation = [[[WebServiceOperation alloc] initWithDelegate:self parameters:params]autorelease];
    self.currentOperation.name = @"UpdateCustomUILayouts";
    [self.currentOperation start];
}

- (void)saveCCLog:(NSString*)ccString{
    
    WebServiceParameters *params = [[[WebServiceParameters alloc] init] autorelease];
    [params addParameter:@"text" value:ccString];
    if (!AppParameterManager.instance.isUploadsDisabled)
        [params addParameter:@"requestContext" value:[self getRequestContext]];
	self.currentOperation = [[[WebServiceOperation alloc] initWithDelegate:self parameters:params]autorelease];
    self.currentOperation.name = @"SaveCCLog";	
	[self.currentOperation start];
}

- (void)test:(id<WebServiceOperationDelegate>)delegate {
    
    self.currentOperation = [[[WebServiceOperation alloc] initWithDelegate:delegate parameters:nil]autorelease];
	self.currentOperation.name = @"Test";
	[self.currentOperation start];
}

- (void)getVersion{
    
	self.currentOperation  = [[[WebServiceOperation alloc] initWithDelegate:self parameters:nil]autorelease];
	self.currentOperation.name = @"Test";
    self.currentOperation.returnClass = [NSNumber class];
	[self.currentOperation start];
}

- (void)getOriginalReceipt:(int)receiptNum locationId:(BPUUID*)locationId {
    
    WebServiceParameters *params = [[[WebServiceParameters alloc] init] autorelease];
    [params addParameter:@"receiptNo" value:@(receiptNum)];
    [params addParameter:@"locationId" value:[locationId description]];
    [params addParameter:@"requestContext" value:[self getRequestContext]];
	self.currentOperation = [[[WebServiceOperation alloc] initWithDelegate:self parameters:params]autorelease];
    self.currentOperation.name = @"GetOriginalReceipt";
	[self.currentOperation start];
}

- (void)getCustomerHistory:(BPUUID*)customerId locationId:(BPUUID*)locationId fromDate:(NSDate*)fromDate {
    
    WebServiceParameters *params = [[[WebServiceParameters alloc] init] autorelease];
    [params addParameter:@"customerId" value:customerId];
    [params addParameter:@"locationId" value:[locationId description]];
    [params addParameter:@"fromDate" value:fromDate];
    [params addParameter:@"requestContext" value:[self getRequestContext]];
	self.currentOperation = [[[WebServiceOperation alloc] initWithDelegate:self parameters:params]autorelease];
    self.currentOperation.name = @"GetCustomerHistory";
	[self.currentOperation start];
}

- (void)findReceiptForCode:(NSString*)receiptCode withMaxDays:(NSInteger)maxDays useLocationSearch:(BOOL)aUseLocationSearch useLocalSearch:(BOOL)useLocalSearch {
    
    WebServiceParameters *params = [[[WebServiceParameters alloc] init] autorelease];
    [params addParameter:@"receiptId" value:receiptCode];
    [params addParameter:@"requestContext" value:[self getRequestContext]];
    [params addParameter:@"source" value:@(useLocalSearch ? 1 : 0)];
    [params addParameter:@"maxDays" value:@(maxDays)];
    [params addParameter:@"useLocationSearch" value:@(aUseLocationSearch ? 1 : 0)];
	self.currentOperation = [[[WebServiceOperation alloc] initWithDelegate:self parameters:params]autorelease];
    self.currentOperation.name = @"FindReceipt";
	[self.currentOperation start];
}

- (void)unholdReceipt:(BPUUID *)receiptId {
    
    WebServiceParameters *params = [[[WebServiceParameters alloc] init] autorelease];
    [params addParameter:@"receiptId" value:receiptId];
    if (!AppParameterManager.instance.isUploadsDisabled)
        [params addParameter:@"requestContext" value:[self getRequestContext]];
    self.currentOperation = [[[WebServiceOperation alloc] initWithDelegate:self parameters:params]autorelease];
    self.currentOperation.name = @"UnholdReceipt";
    self.currentOperation.defaultTimeout = 120;
    [self.currentOperation start];
}

- (void)discardAllHeldReceipts {
    
    WebServiceParameters *params = [[[WebServiceParameters alloc] init] autorelease];
    if (!AppParameterManager.instance.isUploadsDisabled)
        [params addParameter:@"requestContext" value:[self getRequestContext]];
    self.currentOperation = [[[WebServiceOperation alloc] initWithDelegate:self parameters:params]autorelease];
    self.currentOperation.name = @"DiscardAllReceipts";
    [self.currentOperation start];
}

- (void)getItemsAvailableQty:(BPUUID*)receiptId{
    
    WebServiceParameters *params = [[[WebServiceParameters alloc] init] autorelease];
    [params addParameter:@"receiptID" value:receiptId];
    [params addParameter:@"requestContext" value:[self getRequestContext]];
	self.currentOperation = [[[WebServiceOperation alloc] initWithDelegate:self parameters:params]autorelease];
    self.currentOperation.name = @"GetItemsAvailableQty";
	[self.currentOperation start];
}

-(void)uploadBackup:(NSString*)filePath{
    
    WebServiceParameters *params = [[WebServiceParameters alloc] init];
    SOAPAttachment* attachment = [[SOAPAttachment alloc]init];
    attachment.path = filePath;
    attachment.name = [filePath lastPathComponent];
    attachment.contentId = [filePath lastPathComponent];
    [params addParameter:@"attachment" value:attachment];
    [attachment release];
    WebServiceParameters *headerParams = [[WebServiceParameters alloc] init];
    if (!AppParameterManager.instance.isUploadsDisabled)
        [headerParams addParameter:@"requestContext" value:[self getRequestContext]];
	self.currentOperation = [[[WebServiceOperation alloc] initWithDelegate:self parameters:params headerParameters:headerParams]autorelease];
    [params release];
    [headerParams release];
    self.currentOperation.name = @"UploadBackup2";
    self.currentOperation.useMTOM = TRUE;
	[self.currentOperation start];
}

- (void)updateReceiptTaxFree:(NSDictionary *)receiptTaxFree {
    
    WebServiceParameters *params = [[WebServiceParameters new] autorelease];
    [params addParameter:@"receiptTaxFree" value:receiptTaxFree];
    if (!AppParameterManager.instance.isUploadsDisabled)
        [params addParameter:@"requestContext" value:[self getRequestContext]];
    self.currentOperation = [[[WebServiceOperation alloc] initWithDelegate:self parameters:params] autorelease];
    self.currentOperation.name = @"UpdateReceiptTaxFree";
    [self.currentOperation start];
}

- (void)getCurrentLocation{
    
	self.currentOperation = [[[WebServiceOperation alloc] initWithDelegate:self parameters:nil]autorelease];
    self.currentOperation.name = @"GetCurrentLocation";	
	[self.currentOperation start];
}

- (void)getAppointments:(BPUUID*)customerId {
    
    WebServiceParameters *params = [[[WebServiceParameters alloc] init] autorelease];
    [params addParameter:@"customerId" value:[customerId description]];
	self.currentOperation = [[[WebServiceOperation alloc] initWithDelegate:self parameters:params]autorelease];
    self.currentOperation.name = @"GetAppointmentsByCustomer";
	[self.currentOperation start];
}

- (void)rejectAppointmentWithId:(BPUUID *)appointmentId {
    
    WebServiceParameters *params = [[[WebServiceParameters alloc] init] autorelease];
    [params addParameter:@"appointmentId" value:[appointmentId description]];
	self.currentOperation = [[[WebServiceOperation alloc] initWithDelegate:self parameters:params]autorelease];
    self.currentOperation.name = @"RejectAppointment";
	[self.currentOperation start];
}

- (void)getCustomersByOrderNum:(NSString*)orderNum{
    
    WebServiceParameters *params = [[[WebServiceParameters alloc] init] autorelease];
    [params addParameter:@"orderNum" value:orderNum];
    [params addParameter:@"limit" value:@(50)];
    [params addParameter:@"requestContext" value:[self getRequestContext]];
    self.currentOperation = [[[WebServiceOperation alloc] initWithDelegate:self parameters:params]autorelease];
    self.currentOperation.name = @"GetCustomersByOrderNum";
    [self.currentOperation start];
}


- (void)commitReturnItems:(NSArray*)returnReceiptItemIds{
    
    WebServiceParameters *params = [[WebServiceParameters alloc] init];
    NSNamedMutableArray* reserveItemsNamesArray = [[NSNamedMutableArray alloc]initWithArray:returnReceiptItemIds];
    reserveItemsNamesArray.name = @"Guid";
    [params addParameter:@"returnReceiptItemIDs" value:reserveItemsNamesArray];
    [reserveItemsNamesArray release];
    if (!AppParameterManager.instance.isUploadsDisabled)
        [params addParameter:@"requestContext" value:[self getRequestContext]];
	self.currentOperation = [[[WebServiceOperation alloc] initWithDelegate:self parameters:params]autorelease];
    [params release];
    self.currentOperation.name = @"CommitReturnItems";
	[self.currentOperation start];
}

-(NSString*)getRegNumber {
    
    return [NSString stringWithFormat:@"%@-%i-%@",
            [Location localLocation].locationCode,
            [Workstation currentWorkstation].wsNum,
            [AppSettingManager instance].iPadId];
}


-(void)getMWCardDetailsByCardToken:(NSString*)cardToken {
    
    WebServiceParameters * parameters = [[WebServiceParameters alloc] init];
    
    [parameters addParameter:@"merchantName" value:[SettingManager instance].MWMerchantName];
    [parameters addParameter:@"merchantSiteId" value:[SettingManager instance].MWMerchantSiteId];
    [parameters addParameter:@"merchantKey" value:[SettingManager instance].MWMerchantKey];
    [parameters addParameter:@"vaultToken" value:cardToken];
    
    self.currentOperation = [[[WebServiceOperation alloc] initWithDelegate:self parameters:parameters] autorelease];
    
    self.currentOperation.defaultTimeout = [SettingManager instance].MWOperationTimeout;
    
    self.currentOperation.name = @"VaultFindPaymentInfo";
    self.currentOperation.nameSpace = @"http://schemas.merchantwarehouse.com/merchantware/40/Credit/";
    self.currentOperation.address = [NSURL URLWithString:@"https://ps1.merchantware.net/Merchantware/ws/RetailTransaction/v4/Credit.asmx"];
    
    [parameters release];
    [self.currentOperation start];
}


-(NSString*)BPUUIDtoBase64String:(BPUUID*)value {
    
    uuid_t bytes;
    [value getUUIDBytes:bytes];
    NSData *data = [NSData dataWithBytes:&bytes length:sizeof(bytes)];
    //    NSString *base64 = [ObjectBase encodeBase64WithData:data];
    return [data base64Encoding];
}

-(void)processDataSets:(NSArray *)datasets withMetaData:(NSDictionary *)metaData {
    NSData *datasetsData = [NSJSONSerialization dataWithJSONObject:datasets options:0 error:nil];
    NSString *datasetsString = [[NSString alloc] initWithData:datasetsData encoding:NSUTF8StringEncoding];
    
    NSData *metaDataData = [NSJSONSerialization dataWithJSONObject:metaData options:0 error:nil];
    NSString *metaDataString = [[NSString alloc] initWithData:metaDataData encoding:NSUTF8StringEncoding];
    
    WebServiceParameters *params = [[WebServiceParameters new] autorelease];
    [params addParameter:@"requestContext" value:[self getRequestContext]];
    [params addParameter:@"dataSets" value:datasetsString];
    [params addParameter:@"metaData" value:metaDataString];
    [datasetsString release];
    [metaDataString release];
    self.currentOperation = [[[WebServiceOperation alloc] initWithDelegate:self parameters:params]autorelease];
    self.currentOperation.name = @"ProcessDataSets";
    [self.currentOperation start];
}

#pragma RTW Payment Service Methods

-(void)checkServiceAvailability {
    
    self.currentOperation = [[[WebServiceOperation alloc] initWithDelegate:self parameters:nil] autorelease];
    self.currentOperation.name = @"IsAvailable";
    self.currentOperation.address = [NSURL URLWithString:[NSString stringWithFormat:@"http://%@:%ld/PaymentProcessor", [AppSettingManager instance].printingServer, (long)[AppSettingManager instance].printingServerPort]];
    self.currentOperation.returnClass = [USBoolean class];
    [self.currentOperation start];
}

-(void)initProcessor:(int)paymentProcessingType {
    
    WebServiceParameters* parameters = [[WebServiceParameters alloc] init];
    
    if (!AppParameterManager.instance.isUploadsDisabled)
        [parameters addParameter:@"requestContext" value:[self getRequestContext]];
    [parameters addParameter:@"paymentProcessing" value:@(paymentProcessingType)];
    
    self.currentOperation = [[[WebServiceOperation alloc] initWithDelegate:self parameters:parameters] autorelease];
    [parameters release];
    self.currentOperation.name = @"InitProcessor";
    self.currentOperation.address = [NSURL URLWithString:[NSString stringWithFormat:@"http://%@:%ld/PaymentProcessor", [AppSettingManager instance].printingServer, (long)[AppSettingManager instance].printingServerPort]];
    self.currentOperation.returnClass = [NSString class];
    [self.currentOperation start];
}


- (void)checkDeviceWithName:(NSString *)name {
    
    WebServiceParameters *params = [[[WebServiceParameters alloc] init] autorelease];
    [params addParameter:@"deviceName" value:name];
    [params addParameter:@"requestContext" value:[self getRequestContext]];
    [params addParameter:@"getDeviceSettings" value:@NO];
	self.currentOperation = [[[WebServiceOperation alloc] initWithDelegate:self parameters:params]autorelease];
	self.currentOperation.name = @"CheckDevice";
    self.currentOperation.sendErrorLog = NO;
	[self.currentOperation start];
}

- (void)updateDevice:(NSDictionary *)device {
    
    WebServiceParameters *params = [[[WebServiceParameters alloc] init] autorelease];
    [params addParameter:@"device" value:device];
    if (!AppParameterManager.instance.isUploadsDisabled)
        [params addParameter:@"requestContext" value:[self getRequestContext]];
	self.currentOperation = [[[WebServiceOperation alloc] initWithDelegate:self parameters:params]autorelease];
	self.currentOperation.name = @"UpdateDevice";
	[self.currentOperation start];
}

- (void)updateDeviceStats:(NSDictionary *)deviceStats {
    
    WebServiceParameters *params = [[[WebServiceParameters alloc] init] autorelease];
    [params addParameter:@"deviceStats" value:deviceStats];
    if (!AppParameterManager.instance.isUploadsDisabled)
        [params addParameter:@"requestContext" value:[self getRequestContext]];
    self.currentOperation = [[[WebServiceOperation alloc] initWithDelegate:self parameters:params]autorelease];
    self.currentOperation.name = @"UpdateDeviceStats";
    [self.currentOperation start];
}

- (void)authorizeWithLogin:(NSString *)login password:(NSString *)password {
    
    WebServiceParameters *params = [[[WebServiceParameters alloc] init] autorelease];
    [params addParameter:@"login" value:login];
    [params addParameter:@"password" value:password];
    [params addParameter:@"requestContext" value:[self getRequestContext]];
	self.currentOperation = [[[WebServiceOperation alloc] initWithDelegate:self parameters:params]autorelease];
	self.currentOperation.name = @"Login";
	[self.currentOperation start];
}

-(void)updateSalesOrder:(NSDictionary *)salesOrder items:(NSNamedMutableArray *)items globalCharges:(NSNamedMutableArray *)globalCharges itemCharges:(NSNamedMutableArray *)itemCharges taxes:(NSNamedMutableArray *)taxes customers:(NSNamedMutableArray *)customers instruction:(NSDictionary *)instruction instructionKey:(NSString *)instructionKey  orderTaxExemptInfo:(NSNamedMutableArray *)salesOrderTaxExemptInfo taxJurisdictions:(NSNamedMutableArray*)taxJurisdictions persons:(NSNamedMutableArray*)persons {
    
    WebServiceParameters *params = [[[WebServiceParameters alloc] init] autorelease];
    [params addParameter:@"order" value:salesOrder];
    [params addParameter:@"items" value:items];
    [params addParameter:@"globalCharges" value:globalCharges];
    [params addParameter:@"itemCharges" value:itemCharges];
    [params addParameter:@"itemTaxes" value:taxes];
    [params addParameter:@"customers" value:customers];
    [params addParameter:@"taxExemptInfos" value:salesOrderTaxExemptInfo];
    [params addParameter:@"taxJurisdictions" value:taxJurisdictions];
    if (instructionKey) 
        [params addParameter:instructionKey value:instruction];
    [params addParameter:@"salespersons" value:persons];
    if (!AppParameterManager.instance.isUploadsDisabled)
        [params addParameter:@"requestContext" value:[self getRequestContext]];
	self.currentOperation  = [[[WebServiceOperation alloc] initWithDelegate:self parameters:params]autorelease];
	self.currentOperation.name = @"UpdateSalesOrder";
	[self.currentOperation start];
}

-(void)updateShipMemo:(NSDictionary *)shipMemo items:(NSNamedMutableArray *)items cartons:(NSNamedMutableArray *)cartons cartonItems:(NSNamedMutableArray *)cartonItems sendPickUpReadyEmail:(BOOL)sendPickUpReadyEmail{
    
    WebServiceParameters *params = [[[WebServiceParameters alloc] init] autorelease];
    [params addParameter:@"shipSalesOrder" value:shipMemo];
    [params addParameter:@"shipSalesOrderItems" value:items];
    [params addParameter:@"ssoCartons" value:cartons];
    [params addParameter:@"ssoCartonItems" value:cartonItems];
    if (sendPickUpReadyEmail)
            [params addParameter:@"sendPickUpReadyEmail" value:@(1)];
    if (!AppParameterManager.instance.isUploadsDisabled)
         [params addParameter:@"requestContext" value:[self getRequestContext]];
    self.currentOperation  = [[[WebServiceOperation alloc] initWithDelegate:self parameters:params]autorelease];
    self.currentOperation.name = @"UpdateShipMemo";
    self.currentOperation.returnClass = [USBoolean class];
    [self.currentOperation start];
}

-(void)getLocalDateTime {
    
    WebServiceParameters *params = [[[WebServiceParameters alloc] init] autorelease];
    [params addParameter:@"requestContext" value:[self getRequestContext]];
	self.currentOperation  = [[[WebServiceOperation alloc] initWithDelegate:self parameters:params]autorelease];
	self.currentOperation.name = @"GetLocalDateTime";
	[self.currentOperation start];
}

-(void)getEmployeeNotificationForEmployeeId:(BPUUID*)employeeId priorNotificationId:(BPUUID*)notificationId {
    
    WebServiceParameters *params = [[[WebServiceParameters alloc] init] autorelease];
    [params addParameter:@"requestContext" value:[self getRequestContext]];
    [params addParameter:@"employeeId" value:[employeeId description]];
    [params addParameter:@"priorEmployeeNotificationId" value:[notificationId description]];
    self.currentOperation  = [[[WebServiceOperation alloc] initWithDelegate:self parameters:params]autorelease];
    self.currentOperation.name = @"GetEmployeeNotofication";
    [self.currentOperation start];
}

-(void)checkPendingShipMemos{

    WebServiceParameters *params = [[[WebServiceParameters alloc] init] autorelease];
    [params addParameter:@"requestContext" value:[self getRequestContext]];
    
    self.currentOperation  = [[[WebServiceOperation alloc] initWithDelegate:self parameters:params]autorelease];
    self.currentOperation.name = @"CheckPendingShipMemos";
    [self.currentOperation start];
}

-(void)updateInvenPreSetCategories:(NSDictionary*) category{
    
    WebServiceParameters *params = [[[WebServiceParameters alloc] init] autorelease];
    [params addParameter:@"invenPreSetCategory" value:category];
    if (!AppParameterManager.instance.isUploadsDisabled)
        [params addParameter:@"requestContext" value:[self getRequestContext]];
	self.currentOperation  = [[[WebServiceOperation alloc] initWithDelegate:self parameters:params]autorelease];
	self.currentOperation.name = @"UpdateInvenPreSetCategory";
	[self.currentOperation start];
}

-(void)updateInvenPreSetCategoryItems:(NSDictionary*) item{
    
    WebServiceParameters *params = [[[WebServiceParameters alloc] init] autorelease];
    [params addParameter:@"invenPreSetCategoryItem" value:item];
    if (!AppParameterManager.instance.isUploadsDisabled)
        [params addParameter:@"requestContext" value:[self getRequestContext]];
	self.currentOperation  = [[[WebServiceOperation alloc] initWithDelegate:self parameters:params]autorelease];
	self.currentOperation.name = @"UpdateInvenPreSetCategoryItem";
	[self.currentOperation start];
}

-(void)getAccessToken {
    
    WebServiceParameters* params = [[[WebServiceParameters alloc] init] autorelease];
    [params addParameter:@"requestContext" value:[self getRequestContext]];
    
    self.currentOperation = [[[WebServiceOperation alloc] initWithDelegate:self parameters:params] autorelease];
    self.currentOperation.name = @"GetAuthorizationToken";
    self.currentOperation.returnClass = [NSString class];
    [self.currentOperation start];
}

// Brain Tree

-(void)updateEmployeePassword:(Employee *) employee password:(NSString *)password {
    
    WebServiceParameters* params = [[[WebServiceParameters alloc] init] autorelease];
    [params addParameter:@"employeeId" value:employee.id];
    [params addParameter:@"oldPassword" value:employee.password];
    [params addParameter:@"newPassword" value:password];
    if (!AppParameterManager.instance.isUploadsDisabled)
        [params addParameter:@"requestContext" value:[self getRequestContext]];
    self.currentOperation = [[[WebServiceOperation alloc] initWithDelegate:self parameters:params] autorelease];
    self.currentOperation.name = @"UpdateEmployeePassword";
    [self.currentOperation start];
}

-(void)getCCPaymentsBySalesOrder:(BPUUID*)salesOrderId{
    
    WebServiceParameters *params = [[[WebServiceParameters alloc] init] autorelease];
    [params addParameter:@"requestContext" value:[self getRequestContext]];
    [params addParameter:@"salesOrderId" value:salesOrderId];
    self.currentOperation  = [[[WebServiceOperation alloc] initWithDelegate:self parameters:params]autorelease];
    self.currentOperation.name = @"GetCCPaymentsBySalesOrder";
    [self.currentOperation start];
}

- (void)updateDirectPrinter:(NSDictionary *)directPrinter {
    NSNamedMutableArray *directPrinters = [[NSNamedMutableArray new] autorelease];
    [directPrinters addObject:directPrinter];
    directPrinters.name = [NSString stringWithFormat:@"DirectPrinter"];
    
    [self updateDirectPrinters:directPrinters];
}

- (void)updateDirectPrinters:(NSNamedMutableArray *)directPrinters {
    WebServiceParameters *params = [[WebServiceParameters new] autorelease];
    [params addParameter:@"directPrinters" value:directPrinters];
    [params addParameter:@"requestContext" value:[self getRequestContext]];
    self.currentOperation = [[[WebServiceOperation alloc] initWithDelegate:self parameters:params]autorelease];
    self.currentOperation.name = @"UpdateDirectPrinters";
    [self.currentOperation start];
}

- (void)updateDirectPrinterOption:(NSDictionary *)directPrinterOption {
    NSNamedMutableArray *directPrinterOptions = [[NSNamedMutableArray new] autorelease];
    [directPrinterOptions addObject:directPrinterOption];
    directPrinterOptions.name = [NSString stringWithFormat:@"DirectPrinterOption"];
    
    [self updateDirectPrinterOptions:directPrinterOptions];
}

- (void)updateDirectPrinterOptions:(NSNamedMutableArray *)directPrinterOptions {
    WebServiceParameters *params = [[[WebServiceParameters alloc] init] autorelease];
    [params addParameter:@"directPrinterOptions" value:directPrinterOptions];
    [params addParameter:@"requestContext" value:[self getRequestContext]];
    self.currentOperation = [[[WebServiceOperation alloc] initWithDelegate:self parameters:params]autorelease];
    self.currentOperation.name = @"UpdateDirectPrinterOptions";
    [self.currentOperation start];
}

#pragma SyncManagerDelegate methods

- (void) operation:(WebServiceOperation *)operation completedWithResponse:(WebServiceResponse *)response{
    
    [(id)syncDelegate retain];
    dispatch_semaphore_signal(_waitResponseSemaphore);
	if (syncDelegate) {
        
		if (!response.error) {
			[syncDelegate syncManager:self didCompleteOperation:operation.name withResult:response.result];
		}
		else {
			[syncDelegate syncManager:self didOccurError:[response.error localizedDescription] error:response.error];
		}
	}
    [(id)syncDelegate release];
}

-(void)operation:(WebServiceOperation *)operation didReceiveRecord:(id)object{
    
    if (syncDelegate) {
        
        if ([(id)syncDelegate respondsToSelector:@selector(syncManager:didReceiveRecord:)]){
            [syncDelegate syncManager:self didReceiveRecord:object];
        }
    }
}

-(void)operation:(WebServiceOperation *)operation didReceiveBinaryStream:(NSInputStream *)stream{
    if (syncDelegate) {
        if ([(id)syncDelegate respondsToSelector:@selector(syncManager:didReceiveBinaryStream:)]){
            [syncDelegate syncManager:self didReceiveBinaryStream:stream];
        }
    }
}

-(void)operation:(WebServiceOperation *)operation completedUploadWithProgress:(float)progress{
    if (syncDelegate) {
        if ([(id)syncDelegate respondsToSelector:@selector(syncManager:completedUploadWithProgress:)]){
            [syncDelegate syncManager:self completedUploadWithProgress:progress];
        }
    }
}

@end
