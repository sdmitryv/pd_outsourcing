//
//  XopXmlResponseParser.m
//  ProductBuilder
//
//  Created by valera on 6/3/13.
//  Copyright (c) 2013 Cloudworks. All rights reserved.
//

#import "XopXmlResponseParser.h"
#import "PKContentType.h"
#import "multipart_parser.h"

static int on_header_field(multipart_parser* parser, void* ctx, const char *at, size_t length);
static int on_header_value(multipart_parser* parser, void* ctx, const char *at, size_t length);
static int on_part_data(multipart_parser* parser, void* ctx, const char *at, size_t length);
static int on_part_data_begin(multipart_parser*, void* ctx);
static int on_headers_complete(multipart_parser*, void* ctx);
static int on_part_data_end(multipart_parser*, void* ctx);
static int on_body_end(multipart_parser*, void* ctx);
static void on_error(multipart_parser*, void* ctx, char*);

@interface XopXmlResponseParser(){
    NSString* responseName;
    NSMutableArray* contentTypes;
    NSUInteger currentContentIndex;
    //CCMultipartReader* multipartReader;
    NSUInteger dataLength;
    MIMEType currentMimeType;
    BOOL stopped;
    RTProxyStream* binaryStream;
    multipart_parser* mime_parser;
    multipart_parser_settings mime_settings;
}

@property (nonatomic, retain) NSString* boundary;
@property (nonatomic, retain)NSString* start;
@property (nonatomic, retain)NSString* startType;
@property (nonatomic, retain)NSMutableData* dataBuffer;
@property (nonatomic, retain)NSString* currentContentType;
@property (nonatomic, retain)NSString* currentHeaderValue;
@end

@implementation XopXmlResponseParser

+(BOOL)writeError:(NSError**)error description:(NSString*)description domain:(NSString*)domain{
    return [[self class] writeError:error description:description domain:domain code:100];
}

+(BOOL)writeError:(NSError**)error description:(NSString*)description domain:(NSString*)domain code:(NSInteger)code{
    if (error){
        NSMutableDictionary *errorDetail = [NSMutableDictionary dictionary];
        [errorDetail setValue:description forKey:NSLocalizedDescriptionKey];
        *error = [NSError errorWithDomain:domain code:code userInfo:errorDetail];
    }
    return error!=nil;
}

-(id)initWithResponseName:(NSString*)aResponseName headers:(NSDictionary*)headers error:(NSError**)error{
    if ((self=[super init])){
        stopped = FALSE;
        responseName = [aResponseName retain];
        NSString* contentTypeValue = headers[@"Content-Type"];
        if (!contentTypeValue.length){
            [[self class] writeError:error description:@"content type isn't presented" domain:@"XopParser"];
            [self release];
            return nil;
        }
        PKContentType* contentType = [PKContentType contentTypeFromString:contentTypeValue];
        self.boundary = (contentType.parameters)[@"boundary"];
        if (!self.boundary){
            [[self class] writeError:error description:@"boundary isn't defined" domain:@"XopParser"];
            [self release];
            return nil;
        }
        self.start = (contentType.parameters)[@"start"];
        self.startType = (contentType.parameters)[@"start-info"];
        contentTypes = [[NSMutableArray alloc]init];
        [contentTypes addObject:contentType];
        currentContentIndex = 0;
        self.dataBuffer = [NSMutableData data];
        
        //multipartReader = [[CCMultipartReader alloc]init];
        //multipartReader.boundary = self.boundary;
        //multipartReader.delegate = self;
        
        
        mime_settings.on_part_data_begin = &on_part_data_begin;
        mime_settings.on_header_field = &on_header_field;
        mime_settings.on_header_value = &on_header_value;
        mime_settings.on_part_data = &on_part_data;
        mime_settings.on_part_data_begin = &on_part_data_begin;
        mime_settings.on_headers_complete = &on_headers_complete;
        mime_settings.on_part_data_end = &on_part_data_end;
        mime_settings.on_body_end = &on_body_end;
        mime_settings.on_error = &on_error;
        mime_settings.context = self;
        mime_parser = multipart_parser_init([[NSString stringWithFormat:@"--%@", self.boundary] UTF8String], &mime_settings);
    }
    return self;
}

-(void)dealloc{
    multipart_parser_free(mime_parser);
    [self closeBinaryStream];
    //[multipartReader release];
    self.dataBuffer = nil;
    self.boundary = nil;
    self.start = nil;
    self.startType = nil;
    [responseName release];
    [contentTypes release];
    self.currentContentType = nil;
    self.currentHeaderValue = nil;
    [super dealloc];
}

-(void)parseChunk:(NSData*)data{
    //if (stopped || multipartReader.stopped) return;
    if (stopped || multipart_parser_has_error(mime_parser)) return;
    if (!dataLength){
        const void *dataBytes =[data bytes];
        if ([data length] > 2){
            NSString* string = [[NSString alloc]initWithBytes:dataBytes length:2 encoding:NSUTF8StringEncoding];
            if (!string || ![string isEqualToString:@"\r\n"]){
                stopped = TRUE;
                if (self.delegate){
                    NSError* error = nil;
                    [[self class]writeError:&error description:@"invalid symbols at begining of document" domain:@"XopParser"];
                    [self.delegate xopXmlResponseParser:self parsingError:error];
                }
            }
            [string release];
            if (stopped) return;
            //[multipartReader read:dataBytes+2 size:data.length - 2];
            multipart_parser_execute(mime_parser,data.bytes + 2, data.length - 2);
        }
    }
    else{
        //[multipartReader read:data];
        
        multipart_parser_execute(mime_parser,data.bytes, data.length);
    }
    dataLength += data.length;
//    if (multipartReader.hasError){
//        if (self.delegate){
//            NSError* error = nil;
//            [[self class]writeError:&error description:multipartReader.errorMessage domain:@"XopParser"];
//            [self.delegate xopXmlResponseParser:self parsingError:error];
//        }
//    }
    if(multipart_parser_has_error(mime_parser)){
        if (self.delegate){
            NSError* error = nil;
            [[self class]writeError:&error description:@(multipart_parser_get_error(mime_parser)) domain:@"XopParser"];
            [self.delegate xopXmlResponseParser:self parsingError:error];
        }
    }
}

-(void)finish{
    _closed = TRUE;
    [self closeBinaryStream];
}
/*

#pragma mark CCMultipartReaderDelegate methods

- (void)readerDidBeginPart:(CCMultipartReader *)reader headers:(NSDictionary *)headers{
    NSString* contentTypeValue = [headers objectForKey:@"Content-Type"];
    if (!contentTypeValue.length){
        NSError* error = nil;
        [[self class] writeError:&error description:@"content type isn't presented" domain:@"XopParser"];
        [self.delegate xopXmlResponseParser:self parsingError:error];
        [multipartReader reset];
        stopped = TRUE;
        return;
    }
    PKContentType* contentType = [PKContentType contentTypeFromString:contentTypeValue];
    if ([contentType.subType isEqualToString:@"xop+xml"]){
        currentMimeType = MIMETypeXOPXml;
    }
    else if ([contentType.subType isEqualToString:@"octet-stream"]){
        currentMimeType = MIMETypeBinary;
        [binaryStream release];
        binaryStream = nil;
    }
    else{
        currentMimeType = MIMETypeUnknown;
    }
    [contentTypes addObject:contentType];
    currentContentIndex++;
    self.dataBuffer  = [NSMutableData data];
}

- (void)reader:(CCMultipartReader *)reader didReadPartData:(NSData *)data{
    if (currentMimeType == MIMETypeXOPXml){
        [self.dataBuffer appendData:data];
    }
    else if (currentMimeType == MIMETypeBinary){
        if (self.delegate){
            if (!binaryStream){
                binaryStream = [[RTProxyStream alloc]init];
                //open stream to avoid skipping in future apeend method calls
                [binaryStream open];
                [self.delegate xopXmlResponseParser:self didReceiveBinaryStream:binaryStream];
            }
            [binaryStream append:data];
        }
    }
}

- (void)readerDidEndPart:(CCMultipartReader *)reader{
    if (currentMimeType == MIMETypeXOPXml){
//        WebServiceResponse* response = [[WebServiceResponse alloc]init];
//        SOAPDOMResponseParser* domParser = [[SOAPDOMResponseParser alloc]initWithResponseName:responseName resultName:nil returnClass:nil];
//        [domParser parse:self.dataBuffer response:response];
//        [domParser release];
//        self.dataBuffer  = nil;
//        [response release];
    }
    else if (currentMimeType == MIMETypeBinary){
        [self closeBinaryStream];
    }
    currentMimeType = MIMETypeUnknown;
}

- (void)readerDidFinishReading:(CCMultipartReader *)reader{
    [self closeBinaryStream];
}
*/

-(void)closeBinaryStream{
    if (binaryStream){
        [binaryStream close];
        [binaryStream release];
        binaryStream = nil;
    }
}

#pragma mark multipart_parser

-(int) multipart_parser:(multipart_parser*)parser on_header_field:(NSString*)value{
    self.currentHeaderValue = value;
    return 0;
}

-(int) multipart_parser:(multipart_parser*)parser on_header_value:(NSString*)value{
    if ([self.currentHeaderValue isEqualToString:@"Content-Type"]){
        self.currentContentType = value;
    }
    return 0;
}

-(int) multipart_parser_on_headers_complete:(multipart_parser*) parser{
    if (!self.currentContentType){
        //[[self class] writeError:error description:@"content type isn't presented" domain:@"XopParser"];
        stopped = TRUE;
        return 1;
    }
    PKContentType* contentType = [PKContentType contentTypeFromString:self.currentContentType];
    if ([contentType.subType isEqualToString:@"xop+xml"]){
        currentMimeType = MIMETypeXOPXml;
    }
    else if ([contentType.subType isEqualToString:@"octet-stream"]){
        currentMimeType = MIMETypeBinary;
        [binaryStream release];
        binaryStream = nil;
    }
    else{
        currentMimeType = MIMETypeUnknown;
    }
    [contentTypes addObject:contentType];
    currentContentIndex++;
    self.currentHeaderValue = nil;
    self.currentContentType = nil;
    return 0;
}

-(int) multipart_parser:(multipart_parser*)parser on_part_data:(const char *)data length:(size_t)length{
    if (currentMimeType == MIMETypeXOPXml){
        [self.dataBuffer appendData:[NSData dataWithBytesNoCopy:(void*)data length:length freeWhenDone:NO]];
    }
    else if (currentMimeType == MIMETypeBinary){
        if (self.delegate){
            if (!binaryStream && !_closed){
                binaryStream = [[RTProxyStream alloc]init];
                //open stream to avoid skipping in future apeend method calls
                [binaryStream open];
                [self.delegate xopXmlResponseParser:self didReceiveBinaryStream:binaryStream];
            }
            [binaryStream append:data length:length];
        }
    }
    return 0;
}

-(int) multipart_parser_on_part_data_begin:(multipart_parser*)parser{
    self.currentHeaderValue = nil;
    self.currentContentType = nil;
    currentMimeType = MIMETypeUnknown;
    self.dataBuffer  = [NSMutableData data];
    return 0;
}

-(int) multipart_parser_on_part_data_end:(multipart_parser*) parser{
    if (currentMimeType == MIMETypeXOPXml){
        //        WebServiceResponse* response = [[WebServiceResponse alloc]init];
        //        SOAPDOMResponseParser* domParser = [[SOAPDOMResponseParser alloc]initWithResponseName:responseName resultName:nil returnClass:nil];
        //        [domParser parse:self.dataBuffer response:response];
        //        [domParser release];
        //        self.dataBuffer  = nil;
        //        [response release];
    }
    else if (currentMimeType == MIMETypeBinary){
        [self closeBinaryStream];
    }
    currentMimeType = MIMETypeUnknown;
    return 0;
}

-(int) multipart_parser_on_body_end:(multipart_parser*)parser{
    [self closeBinaryStream];
    return 0;
}

-(void) multipart_parser:(multipart_parser*)parser on_error:(NSString*)value{

}

@end

static int on_header_field(multipart_parser* parser, void* ctx, const char *at, size_t length){
    return [((XopXmlResponseParser*)ctx) multipart_parser:parser on_header_field:[[[NSString alloc]initWithBytes:at length:length encoding:NSUTF8StringEncoding]autorelease]];
}

static int on_header_value(multipart_parser* parser, void* ctx, const char *at, size_t length){
    return [((XopXmlResponseParser*)ctx) multipart_parser:parser on_header_value:[[[NSString alloc]initWithBytes:at length:length encoding:NSUTF8StringEncoding]autorelease]];
}

static int on_part_data(multipart_parser* parser, void* ctx, const char *at, size_t length){
    return [((XopXmlResponseParser*)ctx) multipart_parser:parser on_part_data:at length:length];
}
            
static int on_part_data_begin(multipart_parser* parser, void* ctx){
    return [((XopXmlResponseParser*)ctx) multipart_parser_on_part_data_begin:parser];
}

static int on_headers_complete(multipart_parser* parser, void* ctx){
    return [((XopXmlResponseParser*)ctx) multipart_parser_on_headers_complete:parser];
}

static int on_part_data_end(multipart_parser* parser, void* ctx){
    return [((XopXmlResponseParser*)ctx) multipart_parser_on_part_data_end:parser];
}

static int on_body_end(multipart_parser* parser, void* ctx){
    return [((XopXmlResponseParser*)ctx) multipart_parser_on_body_end:parser];
}

static void on_error(multipart_parser* parser, void* ctx, char* error){
    return [((XopXmlResponseParser*)ctx) multipart_parser:parser on_error:@(error)];
}
