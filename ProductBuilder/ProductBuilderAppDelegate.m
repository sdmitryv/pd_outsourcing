//
//  ProductBuilderAppDelegate.m
//  ProductBuilder
//
//  Created by Lulakov Viacheslav on 6/22/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "ProductBuilderAppDelegate.h"
#import "Settings.h"
#import "SecurityManager.h"
#import "DataUtils.h"
#import "KeyboardHelper.h"
#import <UIKit/UIKit.h>
#import "UISearchBarTransparent.h"
#import "iMag.h"
#import "DeviceAgentManager.h"
#import "AppSettingManager.h"
#import "GAELog.h"
#import "MOGlassButton.h"
#import "Location.h"
#import "Workstation.h"
#import "Product_Builder-Swift.h"
#import "AppSettingManager.h"
#import "NSBundle+Language.h"
@import HockeySDK;
#import "Firebase.h"

@interface ProductBuilderAppDelegate(){
    NSString* _deviceLocation;
    NSString* _deviceAppId;
}

@end

@implementation ProductBuilderAppDelegate

@synthesize window=_window;
@synthesize superViewController=_superViewController;

-(UIInterfaceOrientationMask)application:(UIApplication *)application supportedInterfaceOrientationsForWindow:(UIWindow *)window {
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad) {
        return UIInterfaceOrientationMaskLandscape;
    }
    else {
        NSUInteger orientations = UIInterfaceOrientationMaskAllButUpsideDown;
        
        if(self.window.rootViewController){
            UIViewController *presentedViewController = [UIViewController topmostModalViewController];
            orientations = [presentedViewController supportedInterfaceOrientations];
        }
        
        return orientations;
    }
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
#if /*!defined(DEBUG) &&*/ !(TARGET_IPHONE_SIMULATOR)
    [BITHockeyManager.sharedHockeyManager configureWithIdentifier:@"e00945eeab8e4eaa9969b83c8c8ddd0d"];
    BITHockeyManager.sharedHockeyManager.crashManager.crashManagerStatus = BITCrashManagerStatusAutoSend;
    BITHockeyManager.sharedHockeyManager.disableUpdateManager = YES;
    [self updateHockeyUserName];
    [self updateHockeyUserEmail];
    [BITHockeyManager.sharedHockeyManager startManager];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleDatabaseIsReady) name:DataManagerDatabaseIsReady object:nil];
    
    NSURL *URL = [NSURL URLWithString:@"http://ip-api.com/json"];
    NSURLRequest *request = [NSURLRequest requestWithURL:URL];
    NSURLSessionDataTask* task = [[NSURLSession sharedSession] dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (!error && data.length) {
            NSError *error = nil;
            id response = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
            if (!error && response) {
                NSString * countryCode = response[@"countryCode"];
                NSString * city = response[@"city"];
                if (countryCode || countryCode){
                    @synchronized(self) {
                        [_deviceLocation release];
                        NSMutableString* value = [[NSMutableString alloc]initWithString:countryCode?:city];
                        if (countryCode && city){
                            [value appendFormat:@", %@", city];
                        }
                        _deviceLocation = [value retain];
                        [value release];
                        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                        [defaults setObject:_deviceLocation forKey:kLastKnownDeviceLocation];
                        [defaults synchronize];
                    }
                }
                [self updateHockeyUserName];
            }
        }
    }];
    [task resume];

#endif
    
#ifdef __IPHONE_7_0
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7")){
        [[UISegmentedControl appearance] setTintColor:[UIColor whiteColor]];
        [[UISearchBar appearance] setBarTintColor:[UIColor whiteColor]];
        [[UITextField appearance] setKeyboardAppearance:UIKeyboardAppearanceLight];
        if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.1")){
           // [[UITextView appearance] setKeyboardAppearance:UIKeyboardAppearanceDark];
        }
        [[UITextField appearance] setBackgroundColor:[UIColor whiteColor]];
        [[UITextField appearanceWhenContainedInInstancesOfClasses:@[UISearchBarTransparentBig.class]] setBackgroundColor:[UIColor clearColor]];
    }
    else{
        
    }
    [MOGlassButton appearance].style = MOGlassButtonStyleFlat;
    [[UISwitch appearance] setOnTintColor:[MOGlassButtonGreen backgroundColorForStyle:MOGlassButtonStyleFlat state:UIControlStateNormal]];
    //}
#endif
    
    [UITabBar appearance].tintColor = [UIColor whiteColor];
    [UIBarButtonItem appearance].tintColor = [UIColor whiteColor];
    [UINavigationBar appearance].tintColor = [UIColor whiteColor];
    if ([[UIDevice currentDevice] userInterfaceIdiom] != UIUserInterfaceIdiomPad) {

        [[UIToolbar appearance] setBarStyle:UIBarStyleBlack];
        [[UIToolbar appearance] setBackgroundColor:[UIColor blackColor]];
        [[UIToolbar appearance] setTintColor:[UIColor blackColor]];
    }
    
//    [self.window setRootViewController: self.superViewController];
    self.superViewController = (id) self.window.rootViewController;
    [self.window makeKeyAndVisible];

    [KeyboardHelper install];
    [iMag sharedInstance];
    
#if !(TARGET_IPHONE_SIMULATOR)
    [[DeviceAgentManager sharedInstance] writeAppVersion];
#endif
    
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"GoogleService-Info-test" ofType:@"plist"];
    FIROptions *options = [[FIROptions alloc] initWithContentsOfFile:filePath];
    [FIRApp configureWithOptions:options];
    
    [DataManager.instance enableDatabaseAccess];

    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT,0), ^{
        [GAELog logfWithType:GAELogTypeInfo area:AREA_APPLICATION_STATES message:@"applicationWillResignActive" stackTrace:@"applicationWillResignActive:"];
    });
    
    /*
     Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
     Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
     */
    [DataUtils resetFormatters];
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT,0), ^{
        [GAELog logfWithType:GAELogTypeInfo area:AREA_APPLICATION_STATES message:@"applicationDidEnterBackground" stackTrace:@"applicationDidEnterBackground:"];
    });
    /*
     Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
     If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
     */
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT,0), ^{
        [GAELog logfWithType:GAELogTypeInfo area:AREA_APPLICATION_STATES message:@"applicationWillEnterForeground" stackTrace:@"applicationWillEnterForeground:"];
    });
    /*
     Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
     */
    [Settings reset];
}

////////////////////////////////////////////////////////////
-(void)applicationDidBecomeActive:(UIApplication *)application{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT,0), ^{
        [GAELog logfWithType:GAELogTypeInfo area:AREA_APPLICATION_STATES message:@"applicationDidBecomeActive" stackTrace:@"applicationDidBecomeActive:"];
    });
    [self checkDeviceId];
    [self performSelector:@selector(checkDeviceId) withObject:nil afterDelay:1];
}


////////////////////////////////////////////////////////////
-(void)checkDeviceId {
    
    [Settings reset];
    
    [[StatusViewController sharedInstance] updateDeviceIdTitle];
}


////////////////////////////////////////////////////////////
- (void)applicationWillTerminate:(UIApplication *)application {
    /*
     Called when the application is about to terminate.
     Save data if appropriate.
     See also applicationDidEnterBackground:.
     */
    xmlCleanupParser();
}

- (void)dealloc
{
    [_window release];
    [_superViewController release];
    [super dealloc];
}	


////////////////////////////////////////////////////////////
- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {
    
    // <scheme>://<net_loc>/<path>;<params>?<query>#<fragment>
    if ([[url scheme] isEqualToString:[Settings sharedInstance].appURLScheme]) {

        [[NSNotificationCenter defaultCenter] postNotificationName:@"APPURLOPENTNOTIFICATION" object:application userInfo:@{@"url":url, @"sourceApp":sourceApplication}];
        
        return YES;
    }
    return NO;
}

#pragma mark hockey params

NSString* const kLastKnownDeviceAppId = @"lastKnownDeviceAppId";
NSString* const kLastKnownDeviceLocation = @"lastKnownDeviceLocation";

- (void)updateHockeyUserName{
    @synchronized(self) {
        if(!_deviceLocation){
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            [defaults synchronize];
            [_deviceLocation release];
            _deviceLocation = [[defaults objectForKey:kLastKnownDeviceLocation] retain];
        }
        BITHockeyManager.sharedHockeyManager.userName = [NSString stringWithFormat:@"%@, %@, %@", [UIDevice currentDevice].name?:@"N/A", _deviceLocation?:@"N/A", [[NSBundle mainBundle] infoDictionary][@"CFBundleVersion"]];
    }
}

- (void)updateHockeyUserEmail{
    @synchronized(self) {
        if(!_deviceAppId){
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            [defaults synchronize];
            [_deviceAppId release];
            _deviceAppId = [[defaults objectForKey:kLastKnownDeviceAppId] retain];
        }
        BITHockeyManager.sharedHockeyManager.userEmail = _deviceAppId;
    }
}

-(void)setAppLanguage{
    NSString *languageCode = [NSLocale currentLocale].languageCode;

    if([AppSettingManager instance].deviceLanguage == nil && ![languageCode isEqualToString:@"en"]){

        NSString* plistPath = [[NSBundle mainBundle] pathForResource:@"LanguageList" ofType:@"plist"];
        NSMutableArray* languageArray = [NSMutableArray arrayWithArray:[NSArray arrayWithContentsOfFile:plistPath]];
        
        for (NSDictionary * langDict in languageArray) {
            NSString* lang = [langDict valueForKey:@"Key"];
            if([lang rangeOfString:languageCode].location != NSNotFound){
                return;
            }
        }

        NSString * language = @"en";
        [NSBundle setLanguage:language];
    }
}

-(void)handleDatabaseIsReady{
    @synchronized(self) {
        [_deviceAppId release];
        _deviceAppId = [[NSString stringWithFormat:@"%@, %@/%@, %@", [AppSettingManager instance].defaultServerCode?:[AppSettingManager instance].serverUrl?:@"@N/A", Location.localLocation.name?:@"N/A", Workstation.currentWorkstation.name?:@"N/A", [AppSettingManager instance].iPadId?:@"N/A"]retain];
        
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults setObject:_deviceAppId forKey:kLastKnownDeviceAppId];
        [defaults synchronize];
    }
    [self setAppLanguage];
    [self updateHockeyUserEmail];
}

@end
